﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.FixedAsset.Model;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;

namespace Treemas.IFIN.Repository
{
    public class GLPeriodRepository : RepositoryControllerString<GLPeriod>, IGLPeriodRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public GLPeriodRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public GLPeriod GetGLPeriod(DateTime prdStart, DateTime prdEnd)
        {
            return transact(() => session.QueryOver<GLPeriod>()
                                                .Where(f => f.PeriodStart == prdStart && f.PeriodEnd == prdEnd).SingleOrDefault());
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
