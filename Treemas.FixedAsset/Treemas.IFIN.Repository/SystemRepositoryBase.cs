﻿using System.Data;
using System.Reflection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Connection;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Mapping.ByCode;
using Treemas.Base.Utilities;
using Treemas.Base.Repository.UnitOfWork;

namespace Treemas.IFIN.Repository
{
    public class SystemRepositoryBase
    {
        private volatile static SystemRepositoryBase uniqueInstance;
        private static object syncLock = new object();
        public static NHibernate.Cfg.Configuration configuration { get; set; }
        public static ISessionFactory sessionFactory { get; private set; }
        private static HbmMapping _mapping;
        private static IConnection _connection = null;

        private SystemRepositoryBase()
        {
            var conn = Connection.GetConnectionSetting();
            configuration = new NHibernate.Cfg.Configuration();
            configuration.DataBaseIntegration(dbi =>
            {
                dbi.Dialect<MsSql2012Dialect>();
                dbi.Driver<SqlClientDriver>();
                dbi.ConnectionProvider<DriverConnectionProvider>();
                dbi.ConnectionString = string.Format(@"Data Source={0};Database={1};Uid={2};Pwd={3};", conn.DBInstance, conn.DBName, conn.DBUser, conn.DBPassword);
                dbi.IsolationLevel = IsolationLevel.ReadCommitted;
                dbi.Timeout = 15;
            });
            configuration.AddDeserializedMapping(Mapping, null);
            sessionFactory = configuration.BuildSessionFactory();
        }

        private static SystemRepositoryBase instance()
        {
            if (uniqueInstance == null)
            {
                // Lock area where instance is created
                lock (syncLock)
                {
                    if (uniqueInstance == null)
                    {
                        uniqueInstance = new SystemRepositoryBase();
                    }
                }
            }
            return uniqueInstance;
        }

        public static SystemRepositoryBase Instance => instance();

        public ISession currentSession
        {
            get
            {
                return sessionFactory.OpenSession();
            }
        }

        public IStatelessSession statelessSession
        {
            get
            {
                return sessionFactory.OpenStatelessSession();
            }
        }
        public IUnitOfWorkFactory UnitOfWork
        {
            get
            {
                return new UnitOfWorkFactory(configuration, sessionFactory, currentSession);
            }
        }
        public IConnection Connection
        {
            get
            {
                if (_connection == null)
                {
                    _connection = new SystemConnection();
                }
                return _connection;
            }
        }
        public HbmMapping Mapping
        {
            get
            {
                if (_mapping == null)
                {
                    _mapping = CreateMapping();
                }
                return _mapping;
            }
        }
        private HbmMapping CreateMapping()
        {
            var mapper = new ModelMapper();
            //Add mapping classes to the model mapper
            var types = Assembly.Load("Treemas.IFIN.Repository").GetTypes();
            mapper.AddMappings(types);

            //Create and return a HbmMapping of the model mapping in code
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }
}
