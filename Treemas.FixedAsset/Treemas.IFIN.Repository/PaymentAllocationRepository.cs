﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository
{
    public class PaymentAllocationRepository : RepositoryControllerString<PaymentAllocation>, IPaymentAllocationRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public PaymentAllocationRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public PaymentAllocation getPaymentAllocation(string id)
        {
            return transact(() => session.QueryOver<PaymentAllocation>()
                                                .Where(f => f.PaymentAllocationID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<PaymentAllocation>()
                                                .Where(f => f.PaymentAllocationID == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<PaymentAllocation> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<PaymentAllocation> paged = new PagedResult<PaymentAllocation>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<PaymentAllocation>()
                                                     .OrderByDescending(GetOrderByExpression<PaymentAllocation>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<PaymentAllocation>()
                                                     .OrderByDescending(GetOrderByExpression<PaymentAllocation>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<PaymentAllocation> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<PaymentAllocation> specification;
            switch (searchColumn)
            {
                case "PaymentAllocationID":
                    specification = new AdHocSpecification<PaymentAllocation>(s => s.PaymentAllocationID.Equals(searchValue));
                    break;
                //    case "PaymentAllocationDescription":
                //        specification = new AdHocSpecification<PaymentAllocation>(s => s.PaymentAllocationDescription.Contains(searchValue));
                //        break;
                //    case "PaymentAllocationShortDesc":
                //        specification = new AdHocSpecification<PaymentAllocation>(s => s.PaymentAllocationShortDesc.Contains(searchValue));
                //        break;
                default:
                    specification = new AdHocSpecification<PaymentAllocation>(s => s.Description.Contains(searchValue));
                    break;
            }

            PagedResult<PaymentAllocation> paged = new PagedResult<PaymentAllocation>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<PaymentAllocation>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<PaymentAllocation>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<PaymentAllocation>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<PaymentAllocation>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
