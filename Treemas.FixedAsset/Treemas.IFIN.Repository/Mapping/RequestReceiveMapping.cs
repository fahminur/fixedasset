﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class RequestReceiveMapping : ClassMapping<RequestReceive>
    {
        public RequestReceiveMapping()
        {
            this.Table("FA_RequestReceive");
            Id<string>(x => x.AktivaId,
               map => { map.Column("AktivaId"); });

            Property<string>(x => x.COA,
               map => { map.Column("COA"); });

            Property<string>(x => x.Buyer,
               map => { map.Column("Buyer"); });
            Property<double>(x => x.Amount,
               map => { map.Column("Amount"); });

            Property<string>(x => x.ReffNo,
               map => { map.Column("ReffNo"); });
            Property<string>(x => x.InternalMemoNo,
               map => { map.Column("InternalMemoNo"); });
            Property<string>(x => x.Notes,
               map => { map.Column("Notes"); });
            Property<string>(x => x.Status,
               map => { map.Column("Status"); });
            Property<string>(x => x.BranchID,
              map => { map.Column("BranchID"); });
            //Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            //Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            //Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            //Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
