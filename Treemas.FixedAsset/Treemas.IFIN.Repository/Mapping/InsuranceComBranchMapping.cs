﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class InsuranceComBranchMapping : ClassMapping<InsuranceComBranch>
    {
        public InsuranceComBranchMapping()
        {
            this.Table("InsuranceComBranch");

            Id<string>(x => x.MaskAssBranchID,
               map => { map.Column("MaskAssBranchID"); });

            Property<string>(x => x.MaskAssID, map => { map.Column("MaskAssID"); });
            Property<string>(x => x.MaskAssBranchID, map => { map.Column("Description"); });
            Property<string>(x => x.BranchId, map => { map.Column("BranchId"); });
            Property<string>(x => x.MaskAssBranchName, map => { map.Column("MaskAssBranchName"); });
            Property<string>(x => x.Address, map => { map.Column("Address"); });
            Property<string>(x => x.City, map => { map.Column("City"); });
            Property<bool>(x => x.isActive, map => { map.Column("isActive"); });
        }
    }
}
