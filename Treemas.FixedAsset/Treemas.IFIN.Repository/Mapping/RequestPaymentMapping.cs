﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;


namespace Treemas.IFIN.Repository.Mapping
{
    public class RequestPaymentMapping : ClassMapping<RequestPayment>
    {
        public RequestPaymentMapping()
        {
            this.Table("FA_PaymentRequest");
            Id<string>(x => x.InvoiceNo,
               map => { map.Column("InvoiceNo"); });

            Property<string>(x => x.VendorId, map => { map.Column("VendorId"); });
            Property<string>(x => x.VendorBankId, map => { map.Column("VendorBankId"); });
            Property<string>(x => x.VendorBankBranch, map => { map.Column("VendorBankBranch"); });
            Property<string>(x => x.VendorBankAccountNo, map => { map.Column("VendorBankAccountNo"); });
            Property<string>(x => x.VendorBankAccountName, map => { map.Column("VendorBankAccountName"); });
            Property<string>(x => x.VendorInvoiceNo, map => { map.Column("VendorInvoiceNo"); });
            Property<DateTime?>(x => x.InvoiceDate, map => { map.Column("InvoiceDate"); });
            Property<DateTime?>(x => x.PaymentOrderDate, map => { map.Column("PaymentOrderDate"); });
            Property<string>(x => x.CaraBayar, map => { map.Column("CaraBayar"); });
            Property<double>(x => x.Jumlah, map => { map.Column("Jumlah"); });
            Property<string>(x => x.RefferenceNo, map => { map.Column("RefferenceNo"); });
            Property<string>(x => x.Notes, map => { map.Column("Notes"); });
            Property<string>(x => x.MemoNo, map => { map.Column("MemoNo"); });
            Property<string>(x => x.PaymentAllocationId, map => { map.Column("PaymentAllocationId"); });
            Property<DateTime?>(x => x.RequestDate, map => { map.Column("RequestDate"); });
            Property<string>(x => x.Status, map => { map.Column("Status"); });
        }
    }
}
