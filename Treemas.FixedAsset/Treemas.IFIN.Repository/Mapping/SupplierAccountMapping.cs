﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class SupplierAccountMapping : ClassMapping<VendorAccount>
    {
        public SupplierAccountMapping()
        {
            this.Table("FA_SuppAccView");
            Id<string>(x => x.SupplierAccID,
               map => { map.Column("SupplierAccID"); });

            Property<string>(x => x.SupplierID,
               map => { map.Column("SupplierID"); });

            Property<string>(x => x.SupplierBankID,
               map => { map.Column("SupplierBankID"); });

            Property<string>(x => x.SupplierBankBranch,
                map => { map.Column("SupplierBankBranch"); });

            Property<string>(x => x.SupplierAccountNo,
               map => { map.Column("SupplierAccountNo"); });

            Property<string>(x => x.SupplierAccountName,
               map => { map.Column("SupplierAccountName"); });

            Property<bool>(x => x.DefaultAccount,
              map => { map.Column("DefaultAccount"); });

            //Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            //Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            //Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            //Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
