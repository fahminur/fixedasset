﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class BankBranchMasterMapping : ClassMapping<BankBranchMaster>
    {
        public BankBranchMasterMapping()
        {
            this.Table("BankBranchMaster");
            Id<string>(x => x.BankBranchid, map => { map.Column("BankBranchid"); });

            Property<string>(x => x.BankBranchid, map => { map.Column("BankBranchid"); });
            Property<string>(x => x.BankCode, map => { map.Column("BankCode"); });
            Property<string>(x => x.BankBranchName, map => { map.Column("BankBranchName"); });
            Property<string>(x => x.BankID, map => { map.Column("BankID"); });
            Property<string>(x => x.City, map => { map.Column("City"); });
            Property<bool>(x => x.IsActive, map => { map.Column("IsActive"); });
        }
    }
}
