﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class SupplierMapping : ClassMapping<Vendor>
    {
        public SupplierMapping()
        {
            this.Table("Supplier");
            Id<string>(x => x.SupplierId,
               map => { map.Column("SupplierID"); });

            Property<string>(x => x.SupplierGroupID,
               map => { map.Column("SupplierGroupID"); });

            Property<string>(x => x.SupplierName,
               map => { map.Column("SupplierName"); });

            //Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            //Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            //Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            //Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
