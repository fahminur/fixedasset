﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class IfinsGLJournalDMapping : ClassMapping<IfinsGLJournalD>
    {
        public IfinsGLJournalDMapping()
        {
            this.Table("GLJournalD");
            Id<int>(x => x.SequenceNo, map => { map.Column("SequenceNo"); });
            Property<string>(x => x.CompanyID, map => { map.Column("CompanyID"); });
            Property<string>(x => x.BranchID, map => { map.Column("BranchID"); });
            Property<string>(x => x.Tr_Nomor, map => { map.Column("Tr_Nomor"); });
            Property<int>(x => x.SequenceNo, map => { map.Column("SequenceNo"); });
            Property<string>(x => x.CoaCo, map => { map.Column("CoaCo"); });
            Property<string>(x => x.CoaBranch, map => { map.Column("CoaBranch"); });
            Property<string>(x => x.CoaId, map => { map.Column("CoaId"); });
            Property<string>(x => x.TransactionID, map => { map.Column("TransactionID"); });
            Property<string>(x => x.Tr_Desc, map => { map.Column("Tr_Desc"); });
            Property<string>(x => x.Post, map => { map.Column("Post"); });
            Property<double>(x => x.Amount, map => { map.Column("Amount"); });
            Property<string>(x => x.CoaId_X, map => { map.Column("CoaId_X"); });
            Property<string>(x => x.PaymentAllocationId, map => { map.Column("PaymentAllocationId"); });
            Property<string>(x => x.ProductId, map => { map.Column("ProductId"); });
            Property<string>(x => x.UsrUpd, map => { map.Column("UsrUpd"); });
            Property<DateTime>(x => x.DtmUpd, map => { map.Column("DtmUpd"); });
        }
    }
}
