﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class AssetTypeMapping : ClassMapping<AssetType>
    {
        public AssetTypeMapping()
        {
            this.Table("AssetType");

            Id<string>(x => x.AssetTypeID, map => { map.Column("AssetTypeID"); });
            Property<string>(x => x.AssetTypeID, map => { map.Column("AssetTypeID"); });
            Property<string>(x => x.Description, map => { map.Column("Description"); });
            Property<string>(x => x.SerialNo1Label, map => { map.Column("SerialNo1Label"); });
            Property<string>(x => x.SerialNo2Label, map => { map.Column("SerialNo2Label"); });
            Property<string>(x => x.AgreementNoID, map => { map.Column("AgreementNoID"); });
            Property<string>(x => x.AssetCode, map => { map.Column("AssetCode"); });
        }
    }
}
