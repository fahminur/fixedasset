﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class InsuranceComHOMapping : ClassMapping<InsuranceComHO>
    {
        public InsuranceComHOMapping()
        {
            this.Table("InsuranceComHO");

            Id<string>(x => x.MaskAssID,
               map => { map.Column("MaskAssID"); });

            Property<string>(x => x.MaskAssID, map => { map.Column("MaskAssID"); });
            Property<string>(x => x.Description, map => { map.Column("Description"); });
            Property<string>(x => x.Address, map => { map.Column("Address"); });
        }
    }
}
