﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class IfinsGLJournalHMapping : ClassMapping<IfinsGLJournalH>
    {
        public IfinsGLJournalHMapping()
        {
            this.Table("GLJournalH");
            Id<string>(x => x.Tr_Nomor, map => { map.Column("Tr_Nomor"); });
            Property<string>(x => x.CompanyID, map => { map.Column("CompanyID"); });

            Property<string>(x => x.BranchID, map => { map.Column("BranchID "); });
            Property<string>(x => x.Tr_Nomor, map => { map.Column("Tr_Nomor "); });
            Property<string>(x => x.PeriodYear, map => { map.Column("PeriodYear "); });
            Property<string>(x => x.PeriodMonth, map => { map.Column("PeriodMonth"); });
            Property<string>(x => x.TransactionID, map => { map.Column("TransactionID"); });
            Property<DateTime?>(x => x.tr_Date1, map => { map.Column("tr_Date1 "); });
            Property<DateTime>(x => x.Tr_Date, map => { map.Column("Tr_Date"); });
            Property<string>(x => x.Reff_No, map => { map.Column("Reff_No"); });
            Property<DateTime>(x => x.Reff_Date, map => { map.Column("Reff_Date"); });
            Property<string>(x => x.Tr_Desc, map => { map.Column("Tr_Desc"); });
            Property<double>(x => x.JournalAmount, map => { map.Column("JournalAmount"); });
            Property<string>(x => x.BatchID, map => { map.Column("BatchID"); });
            Property<string>(x => x.SubSystem, map => { map.Column("SubSystem"); });
            Property<string>(x => x.Status_Tr, map => { map.Column("Status_Tr"); });
            Property<bool>(x => x.IsActive, map => { map.Column("IsActive "); });
            Property<string>(x => x.Flag, map => { map.Column("Flag "); });
            Property<string>(x => x.UsrUpd, map => { map.Column("UsrUpd "); });
            Property<DateTime>(x => x.DtmUpd, map => { map.Column("DtmUpd "); });
            Property<string>(x => x.Status, map => { map.Column("Status "); });
            Property<bool>(x => x.IsValid, map => { map.Column("IsValid"); });
            Property<int>(x => x.DepresiasiTenor, map => { map.Column("DepresiasiTenor"); });
            Property<int>(x => x.SisaTenor, map => { map.Column("SisaTenor"); });
            Property<DateTime?>(x => x.PrintDate, map => { map.Column("PrintDate"); });
            Property<string>(x => x.PrintBy, map => { map.Column("PrintBy"); });
            Property<bool>(x => x.IsPrint, map => { map.Column("IsPrint"); });
            Property<string>(x => x.Jenis_Reff, map => { map.Column("Jenis_Reff "); });
            Property<string>(x => x.PostingDate, map => { map.Column("PostingDate"); });
            Property<string>(x => x.PostingBy, map => { map.Column("PostingBy"); });
            Property<DateTime?>(x => x.LastRevisiPosting, map => { map.Column("LastRevisiPosting"); });
            Property<int>(x => x.CountRevisiPosting, map => { map.Column("CountRevisiPosting"); });
        }
    }
}
