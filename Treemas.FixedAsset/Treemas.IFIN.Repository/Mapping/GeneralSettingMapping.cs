﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class GeneralSettingMapping : ClassMapping<GeneralSetting>
    {
        public GeneralSettingMapping()
        {
            this.Table("GeneralSetting");
            Id<string>(x => x.GSID, map => { map.Column("GSID"); });
            Property<string>(x => x.ModuleID, map => { map.Column("ModuleID"); });
            Property<string>(x => x.GSName, map => { map.Column("GSName"); });
            Property<string>(x => x.GSValue, map => { map.Column("GSValue"); });
        }
    }
}
