﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class BankMasterMapping : ClassMapping<BankMaster>
    {
        public BankMasterMapping()
        {
            this.Table("BankMaster");
            Id<string>(x => x.BankID,
               map => { map.Column("BankID"); });

            Property<string>(x => x.BankName,
               map => { map.Column("BankName"); });

            Property<string>(x => x.ShortName,
               map => { map.Column("ShortName"); });
        }
    }
}
