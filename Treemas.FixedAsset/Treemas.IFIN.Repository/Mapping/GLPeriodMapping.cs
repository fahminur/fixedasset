﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository.Mapping
{
    public class GLPeriodMapping : ClassMapping<GLPeriod>
    {
        public GLPeriodMapping()
        {
            this.Table("GlPeriod");
            Id<string>(x => x.CompanyId, map => { map.Column("CompanyId"); });
            Property<string>(x => x.CompanyId, map => { map.Column("CompanyId"); });
            Property<string>(x => x.BranchId, map => { map.Column("BranchId"); });
            Property<string>(x => x.PeriodId, map => { map.Column("PeriodId"); });
            Property<string>(x => x.PeriodName, map => { map.Column("PeriodName"); });
            Property<DateTime>(x => x.PeriodStart, map => { map.Column("PeriodStart"); });
            Property<DateTime>(x => x.PeriodEnd, map => { map.Column("PeriodEnd"); });
            Property<int>(x => x.Year, map => { map.Column("Year"); });
            Property<int>(x => x.PeriodStatus, map => { map.Column("PeriodStatus"); });
            Property<DateTime>(x => x.PeriodClosedOn, map => { map.Column("PeriodClosedOn"); });
            Property<int>(x => x.PeriodEverClosed, map => { map.Column("PeriodEverClosed"); });
            Property<DateTime>(x => x.PeriodChangeActiveMonthOn, map => { map.Column("PeriodChangeActiveMonthOn"); });
            Property<int>(x => x.PeriodEverChangeActiveMonth, map => { map.Column("PeriodEverChangeActiveMonth"); });
        }
    }
}