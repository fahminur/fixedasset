﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository
{
    public class SupplierRepository : RepositoryControllerString<Vendor>, ISupplierRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SupplierRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public IEnumerable<Vendor> findActive()
        {
            Specification<Vendor> specification;
            specification = new AdHocSpecification<Vendor>(s => s.SupplierId == "true");

            return FindAll(specification);
        }

        public Vendor getSupplier(string id)
        {
            return transact(() => session.QueryOver<Vendor>()
                                                .Where(f => f.SupplierId == id).SingleOrDefault());
        }
        public bool IsDuplicate(string Supplierid)
        {
            int rowCount = transact(() => session.QueryOver<Vendor>()
                                                .Where(f => f.SupplierId == Supplierid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Vendor> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<Vendor> paged = new PagedResult<Vendor>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Vendor>()
                                                     .OrderByDescending(GetOrderByExpression<Vendor>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Vendor>()
                                                     .OrderBy(GetOrderByExpression<Vendor>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Vendor> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Vendor> specification;
            switch (searchColumn)
            {
                case "SupplierID":
                    specification = new AdHocSpecification<Vendor>(s => s.SupplierId.Equals(searchValue));
                    break;
                //case "SupplierDeskripsi":
                //    specification = new AdHocSpecification<Supplier>(s => s.SupplierDeskripsi.Contains(searchValue));
                //    break;
                //case "AssetAreaShortDesc":
                //    specification = new AdHocSpecification<Supplier>(s => s.AssetAreaShortDesc.Contains(searchValue));
                //    break;
                default:
                    specification = new AdHocSpecification<Vendor>(s => s.SupplierName.Contains(searchValue));
                    break;
            }

            PagedResult<Vendor> paged = new PagedResult<Vendor>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Vendor>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Vendor>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Vendor>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Vendor>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
