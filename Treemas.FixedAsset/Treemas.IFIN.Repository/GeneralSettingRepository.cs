﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.FixedAsset.Model;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;

namespace Treemas.IFIN.Repository
{
    public class GeneralSettingRepository: RepositoryControllerString<GeneralSetting>, IGeneralSettingRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public GeneralSettingRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public GeneralSetting GetGeneralSetting(string gsid, string modulid)
        {
            return transact(() => session.QueryOver<GeneralSetting>()
                                                .Where(f => f.GSID == gsid && f.ModuleID == modulid ).SingleOrDefault());
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
