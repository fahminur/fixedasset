﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository
{
    public class InsuranceComBranchRepository : RepositoryControllerString<InsuranceComBranch>, IInsuranceComBranchRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;

        public InsuranceComBranchRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public InsuranceComBranch getInsuranceComBranch(string id)
        {
            return transact(() => session.QueryOver<InsuranceComBranch>()
                                                .Where(f => f.MaskAssID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<InsuranceComBranch>()
                                                .Where(f => f.MaskAssID == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public IEnumerable<InsuranceComBranch> FindByMaskAssID(string MaskAssID)
        {
            Specification<InsuranceComBranch> specification;
            specification = new AdHocSpecification<InsuranceComBranch>(s => s.MaskAssID == MaskAssID);

            return FindAll(specification);
        }

        public PagedResult<InsuranceComBranch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<InsuranceComBranch> paged = new PagedResult<InsuranceComBranch>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<InsuranceComBranch>()
                                                     .OrderByDescending(GetOrderByExpression<InsuranceComBranch>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<InsuranceComBranch>()
                                                     .OrderByDescending(GetOrderByExpression<InsuranceComBranch>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<InsuranceComBranch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<InsuranceComBranch> specification;
            switch (searchColumn)
            {
                case "MaskAssBranchID":
                    specification = new AdHocSpecification<InsuranceComBranch>(s => s.MaskAssBranchID.Equals(searchValue));
                    break;
                //    case "InsuranceComBranchDescription":
                //        specification = new AdHocSpecification<InsuranceComBranch>(s => s.InsuranceComBranchDescription.Contains(searchValue));
                //        break;
                //    case "InsuranceComBranchShortDesc":
                //        specification = new AdHocSpecification<InsuranceComBranch>(s => s.InsuranceComBranchShortDesc.Contains(searchValue));
                //        break;
                default:
                    specification = new AdHocSpecification<InsuranceComBranch>(s => s.MaskAssBranchName.Contains(searchValue));
                    break;
            }

            PagedResult<InsuranceComBranch> paged = new PagedResult<InsuranceComBranch>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<InsuranceComBranch>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<InsuranceComBranch>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<InsuranceComBranch>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<InsuranceComBranch>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
