﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.IFIN.Repository
{
    public class IfinsGLJournalRepository : RepositoryControllerString<IfinsGLJournalH>, IIfinsGLJournalRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public IfinsGLJournalRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void AddGlJournalDtl(IfinsGLJournalD item)
        {
            transact(() => session.Save(item));
        }

        public bool IsDuplicate(string trno)
        {
            int rowCount = transact(() => session.QueryOver<IfinsGLJournalH>()
                                                .Where(f => f.Tr_Nomor == trno).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
