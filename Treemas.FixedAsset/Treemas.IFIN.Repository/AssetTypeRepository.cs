﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository
{
    public class AssetTypeRepository : RepositoryControllerString<AssetType>, IAssetTypeRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AssetTypeRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public PagedResult<AssetType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AssetType> paged = new PagedResult<AssetType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AssetType>()
                                                     .OrderByDescending(GetOrderByExpression<AssetType>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AssetType>()
                                                     .OrderByDescending(GetOrderByExpression<AssetType>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<AssetType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<AssetType> specification;
            switch (searchColumn)
            {
                case "MaskAssID":
                    specification = new AdHocSpecification<AssetType>(s => s.AssetTypeID.Equals(searchValue));
                    break;
                //    case "AssetTypeDescription":
                //        specification = new AdHocSpecification<AssetType>(s => s.AssetTypeDescription.Contains(searchValue));
                //        break;
                //    case "AssetTypeShortDesc":
                //        specification = new AdHocSpecification<AssetType>(s => s.AssetTypeShortDesc.Contains(searchValue));
                //        break;
                default:
                    specification = new AdHocSpecification<AssetType>(s => s.Description.Contains(searchValue));
                    break;
            }

            PagedResult<AssetType> paged = new PagedResult<AssetType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AssetType>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetType>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AssetType>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<AssetType>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        public AssetType getAssetType(string id)
        {
            return transact(() => session.QueryOver<AssetType>()
                                                .Where(f => f.AssetTypeID == id).SingleOrDefault());
        }

        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<AssetType>()
                                                 .Where(f => f.AssetTypeID == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
