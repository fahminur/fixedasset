﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using NHibernate.Criterion;
using Treemas.Base.Utilities;

namespace Treemas.IFIN.Repository
{
    public class RequestPaymentRepository : RepositoryControllerString<RequestPayment>, IRequestPaymentRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RequestPaymentRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

      
        public bool IsDuplicate(string invNo)
        {
            int rowCount = transact(() => session.QueryOver<RequestPayment>()
                                                .Where(f => f.InvoiceNo == invNo).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
     

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public RequestPayment getRequest(string id)
        {
            return transact(() => session.QueryOver<RequestPayment>()
                                                .Where(f => f.InvoiceNo == id).SingleOrDefault());
        }

        public PagedResult<RequestPayment> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, RequestPaymentFilter filter)
        {
            SimpleExpression VendorId = Restrictions.Eq("VendorId", filter.VendorId.Trim());
            SimpleExpression RequestDateStart = Restrictions.Ge("RequestDate", filter.RequestDateFrom);
            SimpleExpression RequestDateFinish = Restrictions.Le("RequestDate", filter.RequestDateTo);
            SimpleExpression VendorInvoiceNo = Restrictions.Eq("VendorInvoiceNo", filter.VendorInvoiceNo);
            SimpleExpression Status = Restrictions.Eq("Status", filter.InvoiceStatus);


            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.VendorId.Trim().IsNullOrEmpty()) conjuction.Add(VendorId);
            if (!filter.VendorInvoiceNo.Trim().IsNullOrEmpty()) conjuction.Add(VendorInvoiceNo);
            if (!filter.InvoiceStatus.Trim().IsNullOrEmpty()) conjuction.Add(Status);
            if (!filter.RequestDateFrom.IsNull()) conjuction.Add(RequestDateStart);
            if (!filter.RequestDateTo.IsNull()) conjuction.Add(RequestDateFinish);

            PagedResult<RequestPayment> paged = new PagedResult<RequestPayment>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<RequestPayment>()
                                 .Where(conjuction)
                                 //.And(s => s.Status == "N")
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<RequestPayment>()
                                 .Where(conjuction)
                                 //.And(s => s.Status == "N")
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<RequestPayment>()
                               .Where(conjuction)
                               //.And(s => s.Status == "N")
                               .RowCount());
            return paged;
        }
    }
}
