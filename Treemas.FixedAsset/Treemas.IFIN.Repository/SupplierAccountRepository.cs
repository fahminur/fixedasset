﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository
{
    public class SupplierAccountRepository : RepositoryControllerString<VendorAccount>, ISupplierAccountRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SupplierAccountRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public VendorAccount getSupplierAccount(string id)
        {
            return transact(() => session.QueryOver<VendorAccount>()
                                                .Where(f => f.SupplierAccID == id).SingleOrDefault());
        }
        public IList<VendorAccount> getSupplierAccbySupplier(string id)
        {
            return transact(() => session.QueryOver<VendorAccount>()
                                                .Where(f => f.SupplierID == id).List());
        }
        public bool IsDuplicate(string supplierId, string supplierAccId)
        {
            int rowCount = transact(() => session.QueryOver<VendorAccount>()
                                                .Where(f => f.SupplierID == supplierId && f.SupplierAccID == supplierAccId).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public IEnumerable<VendorAccount> getBankBySupplier(string supplierId)
        {
            Specification<VendorAccount> specification;
            specification = new AdHocSpecification<VendorAccount>(s => s.SupplierID == supplierId);
            return FindAll(specification);
        }
        public PagedResult<VendorAccount> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string supplierid)
        {
            PagedResult<VendorAccount> paged = new PagedResult<VendorAccount>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<VendorAccount>()
                                                     .Where(x => x.SupplierID == supplierid)
                                                     .OrderByDescending(GetOrderByExpression<VendorAccount>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<VendorAccount>()
                                                     .Where(x => x.SupplierID == supplierid)
                                                     .OrderBy(GetOrderByExpression<VendorAccount>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<VendorAccount> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string id)
        {
            Specification<VendorAccount> specification;

            switch (searchColumn)
            {
                case "Id":
                    specification = new AdHocSpecification<VendorAccount>(s => s.Id.Equals(searchValue) && s.SupplierID == id);
                    break;
                case "ContactName":
                    specification = new AdHocSpecification<VendorAccount>(s => s.SupplierAccountNo.Contains(searchValue) && s.SupplierID == id);
                    break;
                case "ContactJabatan":
                    specification = new AdHocSpecification<VendorAccount>(s => s.SupplierBankID.Contains(searchValue) && s.SupplierID == id);
                    break;
                case "ContactType":
                    specification = new AdHocSpecification<VendorAccount>(s => s.SupplierBankBranchID.Equals(searchValue) && s.SupplierID == id);
                    break;
                default:
                    specification = new AdHocSpecification<VendorAccount>(s => s.Id.Equals(searchValue) && s.SupplierID == id);
                    break;
            }

            PagedResult<VendorAccount> paged = new PagedResult<VendorAccount>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<VendorAccount>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<VendorAccount>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<VendorAccount>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<VendorAccount>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
