﻿using Treemas.Base.Utilities;
using Treemas.Base.Configuration;

namespace Treemas.IFIN.Repository
{ 
    public class SystemConnection : IConnection
    {
        private ConnectionParam connectionSetting;
        public SystemConnection()
        {
            connectionSetting = new ConnectionParam();
            IConfigurationBinder binder = SystemConfigurationCabinet.Instance.GetBinder("Database");
            CompositeConfigurationItem items = (CompositeConfigurationItem)binder.GetConfiguration("IFIN");

            ConfigurationItem item = items.GetItem("DBInstance");
            connectionSetting.DBInstance = item.Value;

            item = items.GetItem("DBUser");
            connectionSetting.DBUser = Encryption.Instance.DecryptText(item.Value);

            item = items.GetItem("DBPassword");
            connectionSetting.DBPassword = Encryption.Instance.DecryptText(item.Value);

            item = items.GetItem("DBName");
            connectionSetting.DBName = item.Value;
        }
        public ConnectionParam GetConnectionSetting()
        {
            return connectionSetting;
        }
    }
}
