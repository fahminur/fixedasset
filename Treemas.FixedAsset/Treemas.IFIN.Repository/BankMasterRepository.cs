﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.FixedAsset.Model;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;

namespace Treemas.IFIN.Repository
{
    public class BankMasterRepository : RepositoryControllerString<BankMaster>, IBankMasterRepository 
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public BankMasterRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public BankMaster getBankMaster(string id)
        {
            return transact(() => session.QueryOver<BankMaster>()
                                                .Where(f => f.BankID == id).SingleOrDefault());
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
