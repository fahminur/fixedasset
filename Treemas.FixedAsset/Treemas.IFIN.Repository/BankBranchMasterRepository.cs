﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.FixedAsset.Model;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;

namespace Treemas.IFIN.Repository
{
    public class BankBranchMasterRepository : RepositoryControllerString<BankBranchMaster>, IBankBranchMasterRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public BankBranchMasterRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public PagedResult<BankBranchMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<BankBranchMaster> paged = new PagedResult<BankBranchMaster>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<BankBranchMaster>()
                                                     .OrderByDescending(GetOrderByExpression<BankBranchMaster>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<BankBranchMaster>()
                                                     .OrderByDescending(GetOrderByExpression<BankBranchMaster>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<BankBranchMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<BankBranchMaster> specification;
            switch (searchColumn)
            {
                case "BankBranchid":
                    specification = new AdHocSpecification<BankBranchMaster>(s => s.BankBranchid.Equals(searchValue));
                    break;
                //    case "BankBranchMasterDescription":
                //        specification = new AdHocSpecification<BankBranchMaster>(s => s.BankBranchMasterDescription.Contains(searchValue));
                //        break;
                //    case "BankBranchMasterShortDesc":
                //        specification = new AdHocSpecification<BankBranchMaster>(s => s.BankBranchMasterShortDesc.Contains(searchValue));
                //        break;
                default:
                    specification = new AdHocSpecification<BankBranchMaster>(s => s.BankBranchName.Contains(searchValue));
                    break;
            }

            PagedResult<BankBranchMaster> paged = new PagedResult<BankBranchMaster>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<BankBranchMaster>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<BankBranchMaster>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<BankBranchMaster>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<BankBranchMaster>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        public IEnumerable<BankBranchMaster> getBankBranchByBankID(string id)
        {
            Specification<BankBranchMaster> specification;
            specification = new AdHocSpecification<BankBranchMaster>(s => s.BankID == id);

            return FindAll(specification);
        }

        public BankBranchMaster getBankBranchMaster(string id)
        {
            return transact(() => session.QueryOver<BankBranchMaster>()
                                                .Where(f => f.BankBranchid == id).SingleOrDefault());
        }

        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<InsuranceComBranch>()
                                                .Where(f => f.MaskAssID == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

    private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
