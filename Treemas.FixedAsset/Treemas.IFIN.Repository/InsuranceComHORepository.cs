﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository
{
    public class InsuranceComHORepository: RepositoryControllerString<InsuranceComHO>, IInsuranceComHORepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public InsuranceComHORepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public InsuranceComHO getInsuranceComHO(string id)
        {
            return transact(() => session.QueryOver<InsuranceComHO>()
                                                .Where(f => f.MaskAssID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<InsuranceComHO>()
                                                .Where(f => f.MaskAssID == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<InsuranceComHO> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<InsuranceComHO> paged = new PagedResult<InsuranceComHO>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<InsuranceComHO>()
                                                     .OrderByDescending(GetOrderByExpression<InsuranceComHO>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<InsuranceComHO>()
                                                     .OrderByDescending(GetOrderByExpression<InsuranceComHO>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<InsuranceComHO> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<InsuranceComHO> specification;
            switch (searchColumn)
            {
                case "MaskAssID":
                    specification = new AdHocSpecification<InsuranceComHO>(s => s.MaskAssID.Equals(searchValue));
                    break;
                //    case "InsuranceComHODescription":
                //        specification = new AdHocSpecification<InsuranceComHO>(s => s.InsuranceComHODescription.Contains(searchValue));
                //        break;
                //    case "InsuranceComHOShortDesc":
                //        specification = new AdHocSpecification<InsuranceComHO>(s => s.InsuranceComHOShortDesc.Contains(searchValue));
                //        break;
                default:
                    specification = new AdHocSpecification<InsuranceComHO>(s => s.Description.Contains(searchValue));
                    break;
            }

            PagedResult<InsuranceComHO> paged = new PagedResult<InsuranceComHO>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<InsuranceComHO>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<InsuranceComHO>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<InsuranceComHO>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<InsuranceComHO>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
