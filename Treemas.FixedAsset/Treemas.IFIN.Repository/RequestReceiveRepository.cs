﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.IFIN.Repository
{
    public class RequestReceiveRepository : RepositoryControllerString<RequestReceive>, IRequestReceiveRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RequestReceiveRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

      
        public bool IsDuplicate(string aktivaid)
        {
            int rowCount = transact(() => session.QueryOver<RequestReceive>()
                                                .Where(f => f.AktivaId == aktivaid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
     

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
