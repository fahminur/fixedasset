﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Service
{
    public class PenerimaanBarangServiceRepository : IPenerimaanBarangService
    {
        private IPenerimaanBarangRepository _penerimaanRepository;
        private IPenerimaanBarangSequence _penerimaanSequence;

        public PenerimaanBarangServiceRepository(IPenerimaanBarangRepository penerimaanRepository, IPenerimaanBarangSequence penerimaanSequence)
        {
            this._penerimaanRepository = penerimaanRepository;
            this._penerimaanSequence = penerimaanSequence;
        }

        public string SavePenerimaanBarang(PenerimaanBarangHdr GRH, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    GRH.GRNo = _penerimaanSequence.createTransactionNo("GR", year, month, user, company);

                    _penerimaanRepository.Add(GRH);
                    if (GRH.PenerimaanAktivaList != null)
                    {
                        foreach (var item in GRH.PenerimaanAktivaList)
                        {
                            item.GRNo = GRH.GRNo;
                            _penerimaanRepository.AddPenerimaanBarangAktiva(item);
                        }
                    }
                    if (GRH.PenerimaanNonAktivaList != null)
                    {
                        foreach (var item in GRH.PenerimaanNonAktivaList)
                        {
                            item.GRNo = GRH.GRNo;
                            _penerimaanRepository.AddPenerimaanBarangNonAktiva(item);
                        }
                    }
                    if (GRH.PenerimaanServiceList != null)
                    {
                        foreach (var item in GRH.PenerimaanServiceList)
                        {
                            item.GRNo = GRH.GRNo;
                            _penerimaanRepository.AddPenerimaanBarangService(item);
                        }
                    }
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string UpdatePenerimaanBarang(PenerimaanBarangHdr GRH, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _penerimaanRepository.Save(GRH);
                    _penerimaanRepository.DeletePenerimaanBarangActiva(GRH.GRNo.Trim());
                    _penerimaanRepository.DeletePenerimaanBarangNonActiva(GRH.GRNo.Trim());
                    _penerimaanRepository.DeletePenerimaanBarangService(GRH.GRNo.Trim());

                    if (GRH.PenerimaanAktivaList != null)
                    {
                        foreach (var item in GRH.PenerimaanAktivaList)
                        {
                            item.GRNo = GRH.GRNo;
                            _penerimaanRepository.AddPenerimaanBarangAktiva(item);
                        }
                    }
                    if (GRH.PenerimaanNonAktivaList != null)
                    {
                        foreach (var item in GRH.PenerimaanNonAktivaList)
                        {
                            item.GRNo = GRH.GRNo;
                            _penerimaanRepository.AddPenerimaanBarangNonAktiva(item);
                        }
                    }
                    if (GRH.PenerimaanServiceList != null)
                    {
                        foreach (var item in GRH.PenerimaanServiceList)
                        {
                            item.GRNo = GRH.GRNo;
                            _penerimaanRepository.AddPenerimaanBarangService(item);
                        }
                    }
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
    }
}
