﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Service;
using Treemas.Foundation.Model;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Service
{
   public class PenerimaanBarangSequenceService:IPenerimaanBarangSequence
    {
        private IPenerimaanBarangSequenceRepository _penerimaanbarangSeqRepository;
        public PenerimaanBarangSequenceService(IPenerimaanBarangSequenceRepository penerimaanbarangSeqRepository)
        {
            _penerimaanbarangSeqRepository = penerimaanbarangSeqRepository;
        }

        public string createTransactionNo(string SeqID, int year, int month, User user, Company company)
        {
            string newSeq = "";
            string transNo = "";
            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    PenerimaanBarangSequence sequence = _penerimaanbarangSeqRepository.getGRSeq(SeqID, year);

                    if (!sequence.IsNull())
                    {
                        switch (month)
                        {
                            case 1:
                                newSeq = sequence.Seq1.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq1 += 1;
                                break;
                            case 2:
                                newSeq = sequence.Seq2.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq2 += 1;
                                break;
                            case 3:
                                newSeq = sequence.Seq3.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq3 += 1;
                                break;
                            case 4:
                                newSeq = sequence.Seq4.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq4 += 1;
                                break;
                            case 5:
                                newSeq = sequence.Seq5.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq5 += 1;
                                break;
                            case 6:
                                newSeq = sequence.Seq6.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq6 += 1;
                                break;
                            case 7:
                                newSeq = sequence.Seq7.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq7 += 1;
                                break;
                            case 8:
                                newSeq = sequence.Seq8.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq8 += 1;
                                break;
                            case 9:
                                newSeq = sequence.Seq9.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq9 += 1;
                                break;
                            case 10:
                                newSeq = sequence.Seq10.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq10 += 1;
                                break;
                            case 11:
                                newSeq = sequence.Seq11.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq11 += 1;
                                break;
                            case 12:
                                newSeq = sequence.Seq12.ToString().PadLeft(sequence.LengthNo, '0');
                                sequence.Seq12 += 1;
                                break;
                        }

                        transNo += (sequence.Prefix.IsNullOrEmpty()) ? "" : sequence.Prefix.Trim();
                        //transNo += company.CompanyId.Trim();
                        //transNo += (sequence.IsSite) ? user.Site.SiteId.TrimEnd() : "";
                        transNo += (sequence.IsYear) ? year.ToString().Substring(2, 2) : "";
                        transNo += (sequence.IsMonth) ? month.ToString().PadLeft(2, '0') : "";
                        transNo += newSeq;
                        sequence.UpdateDate = user.BusinessDate.CurrentDate;
                        sequence.UpdateUser = user.FirstName;
                        _penerimaanbarangSeqRepository.Save(sequence);
                    }
                    if (!newSeq.IsNullOrEmpty()) break;
                }
                catch (Exception ex)
                {
                    if (i == 5) throw new Exception(ex.Message); else Thread.Sleep(10);
                }
            }

            return transNo;
        }
    }
}
