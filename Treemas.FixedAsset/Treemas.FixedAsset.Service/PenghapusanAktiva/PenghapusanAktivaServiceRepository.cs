﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Service
{
    public class PenghapusanAktivaServiceRepository : IPenghapusanAktivaService
    {
        private IPenghapusanAktivaRepository _PenghapusanRepository;
        private IPenghapusanAktivaSequence _PenghapusanSequence;

        public PenghapusanAktivaServiceRepository(IPenghapusanAktivaRepository PenghapusanRepository, IPenghapusanAktivaSequence PenghapusanSequence)
        {
            this._PenghapusanRepository = PenghapusanRepository;
            this._PenghapusanSequence = PenghapusanSequence;
        }

        public string SavePenghapusanAktiva(PenghapusanAktivaHeader paH, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    paH.DeleteNo = _PenghapusanSequence.createTransactionNo("DA", year, month, user, company);

                    _PenghapusanRepository.Add(paH);
                    if (paH.PenghapusanAktivaDetailList != null)
                    {
                        foreach (var item in paH.PenghapusanAktivaDetailList)
                        {
                            item.DeleteNo = paH.DeleteNo;
                            _PenghapusanRepository.AddPenghapusanDetail(item);
                        }
                    }
                    
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string UpdatePenghapusanAktiva(PenghapusanAktivaHeader paH, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _PenghapusanRepository.Save(paH);
                    _PenghapusanRepository.DeletePenghapusanDetail(paH.DeleteNo.Trim());

                    if (paH.PenghapusanAktivaDetailList != null)
                    {
                        foreach (var item in paH.PenghapusanAktivaDetailList)
                        {
                            item.DeleteNo = paH.DeleteNo;
                            _PenghapusanRepository.AddPenghapusanDetail(item);
                        }
                    }
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
    }
}
