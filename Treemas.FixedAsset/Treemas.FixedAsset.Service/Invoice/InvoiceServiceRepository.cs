﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Service
{
    public class InvoiceServiceRepository : IInvoiceService
    {
        private IInvoiceRepository _InvoiceRepository;
        private IInvoiceSequence _InvoiceSequence;

        public InvoiceServiceRepository(IInvoiceRepository InvoiceRepository, IInvoiceSequence InvoiceSequence)
        {
            this._InvoiceRepository = InvoiceRepository;
            this._InvoiceSequence = InvoiceSequence;
        }
        public string SaveInvoice(InvoiceH invH, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    invH.InvoiceNo = _InvoiceSequence.createTransactionNo("INV", year, month, user, company);

                    _InvoiceRepository.Add(invH);
                    if (invH.InvoiceAktivaList != null)
                    {
                        foreach (var item in invH.InvoiceAktivaList)
                        {
                            item.InvoiceNo = invH.InvoiceNo;
                            _InvoiceRepository.AddInvoiceAktiva(item);
                        }
                    }
                    if (invH.InvoiceNonAktivaList != null)
                    {
                        foreach (var item in invH.InvoiceNonAktivaList)
                        {
                            item.InvoiceNo = invH.InvoiceNo;
                            _InvoiceRepository.AddInvoiceNonAktiva(item);
                        }
                    }
                    if (invH.InvoiceServiceList != null)
                    {
                        foreach (var item in invH.InvoiceServiceList)
                        {
                            item.InvoiceNo = invH.InvoiceNo;
                            _InvoiceRepository.AddInvoiceService(item);
                        }
                    }
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string UpdateInvoice(InvoiceH invH, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _InvoiceRepository.Save(invH);
                    _InvoiceRepository.DeleteInvoiceActiva(invH.InvoiceNo.Trim());
                    _InvoiceRepository.DeleteInvoiceNonActiva(invH.InvoiceNo.Trim());
                    _InvoiceRepository.DeleteInvoiceService(invH.InvoiceNo.Trim());

                    if (invH.InvoiceAktivaList != null)
                    {
                        foreach (var item in invH.InvoiceAktivaList)
                        {
                            item.InvoiceNo = invH.InvoiceNo;
                            _InvoiceRepository.AddInvoiceAktiva(item);
                        }
                    }
                    if (invH.InvoiceNonAktivaList != null)
                    {
                        foreach (var item in invH.InvoiceNonAktivaList)
                        {
                            item.InvoiceNo = invH.InvoiceNo;
                            _InvoiceRepository.AddInvoiceNonAktiva(item);
                        }
                    }
                    if (invH.InvoiceServiceList != null)
                    {
                        foreach (var item in invH.InvoiceServiceList)
                        {
                            item.InvoiceNo = invH.InvoiceNo;
                            _InvoiceRepository.AddInvoiceService(item);
                        }
                    }
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
    }
}
