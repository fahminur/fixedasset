﻿using System;
using System.Transactions;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;


namespace Treemas.FixedAsset.Service
{
    public class GLJournalServiceRepository : IGLJournalService
    {
        private IGLJournalRepository _glJournalRepository;
        private IGLJournalSequence _glJournalSequence;

        public GLJournalServiceRepository(IGLJournalRepository glJournalRepository, IGLJournalSequence glJournalSequence)
        {
            _glJournalRepository = glJournalRepository;
            _glJournalSequence = glJournalSequence;
        }
        public string SaveJournal(GLJournalH journalHdr, User usr, Company comp)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    journalHdr.Tr_Nomor = _glJournalSequence.createTransactionNo("GLJ", year, month, journalHdr.BranchID.Trim(), usr, comp);

                    _glJournalRepository.Add(journalHdr);
                    if (journalHdr.GLJournalDtlList != null)
                    {
                        foreach (var item in journalHdr.GLJournalDtlList)
                        {
                            item.Tr_Nomor = journalHdr.Tr_Nomor;
                            _glJournalRepository.AddGlJournalDtl(item);
                        }
                    }
                    scope.Complete();
                    returnMessage = "Success - " + journalHdr.Tr_Nomor;
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
    }
}
