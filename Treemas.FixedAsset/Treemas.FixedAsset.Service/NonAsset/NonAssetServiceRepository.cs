﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Service
{
    public class NonAssetServiceRepository : INonAssetMasterService
    {
        private INonAssetMasterRepository _NonAssetMasterRepository;
        private INonAssetMasterSequence _NonAssetMasterSequence;

        public NonAssetServiceRepository(INonAssetMasterRepository NonAssetMasterRepository, INonAssetMasterSequence NonAssetMasterSequence)
        {
            this._NonAssetMasterRepository = NonAssetMasterRepository;
            this._NonAssetMasterSequence = NonAssetMasterSequence;
        }
        public string SaveNonAssetMaster(NonAssetMaster nam, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    nam.NonAktivaID = _NonAssetMasterSequence.createNonAssetMasterNo("NAM", nam.SiteID.Trim(), nam.GroupAssetID.Trim(), year, month, user, company);

                    _NonAssetMasterRepository.Add(nam);
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string UpdateNonAssetMaster(NonAssetMaster nam, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //FAM.AktivaID = _FixedAssetMasterSequence.createFixedAssetMasterNo("BNIMF00", FAM.AssetAreaID.Trim(), year, month, user, company);

                    _NonAssetMasterRepository.Save(nam);
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
    }
}
