﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Service
{
    public class PenjualanAktivaServiceRepository : IPenjualanAktivaService
    {
        private IPenjualanAktivaRepository _PenjualanRepository;
        private IPenjualanAktivaSequence _PenjualanSequence;

        public PenjualanAktivaServiceRepository(IPenjualanAktivaRepository PenjualanRepository, IPenjualanAktivaSequence PenjualanSequence)
        {
            this._PenjualanRepository = PenjualanRepository;
            this._PenjualanSequence = PenjualanSequence;
        }

        public string SavePenjualanAktiva(PenjualanAktivaHeader paH, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    paH.SellingNo = _PenjualanSequence.createTransactionNo("PA", year, month, user, company);

                    _PenjualanRepository.Add(paH);
                    if (paH.PenjualanAktivaDetailList != null)
                    {
                        foreach (var item in paH.PenjualanAktivaDetailList)
                        {
                            item.SellingNo = paH.SellingNo;
                            _PenjualanRepository.AddPenjualanDetail(item);
                        }
                    }
                    
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string UpdatePenjualanAktiva(PenjualanAktivaHeader paH, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _PenjualanRepository.Save(paH);
                    _PenjualanRepository.DeletePenjualanDetail(paH.SellingNo.Trim());

                    if (paH.PenjualanAktivaDetailList != null)
                    {
                        foreach (var item in paH.PenjualanAktivaDetailList)
                        {
                            item.SellingNo = paH.SellingNo;
                            _PenjualanRepository.AddPenjualanDetail(item);
                        }
                    }
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
    }
}
