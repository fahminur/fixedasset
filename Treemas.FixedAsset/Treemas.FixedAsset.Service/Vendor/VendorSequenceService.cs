﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Service;
using Treemas.Foundation.Model;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Service
{
    public class VendorSequenceService : IVendorSequence
    {
        private IVendorSequenceRepository _VndSeqRepository;
        public VendorSequenceService(IVendorSequenceRepository VndSeqRepository)
        {
            this._VndSeqRepository = VndSeqRepository;
        }

        public string createVendorID(string SeqID, string BranchID, int year, int month, User user, Company company)
        {
            string newSeq = "";
            string transNo = "";
            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    VendorSequence sequence = _VndSeqRepository.getVendorSeq(SeqID, year);

                    if (!sequence.IsNull())
                    {
                        newSeq = sequence.Seq1.ToString().PadLeft(sequence.LengthNo, '0');
                        sequence.Seq1 += 1;

                        transNo += BranchID.Trim() + newSeq;
                        sequence.UpdateDate = user.BusinessDate.CurrentDate;
                        sequence.UpdateUser = user.FirstName;
                        _VndSeqRepository.Save(sequence);
                    }
                    if (!newSeq.IsNullOrEmpty()) break;
                }
                catch (Exception ex)
                {
                    if (i == 5) throw new Exception(ex.Message); else Thread.Sleep(10);
                }
            }

            return transNo;
        }

    }
}
