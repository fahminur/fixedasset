﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Service
{
    public class VendorServiceRepository : IVendorService
    {
        private IVendorRepository _VendorRepository;
        private IVendorSequence _VendorSequence;

        public VendorServiceRepository(IVendorRepository VendorRepository, IVendorSequence VendorSequence)
        {
            this._VendorRepository = VendorRepository;
            this._VendorSequence = VendorSequence;
        }
        public string SaveVendor(Vendor vendor, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    vendor.VendorId = _VendorSequence.createVendorID("VND", user.Site.SiteID, year, month, user, company);

                    _VendorRepository.Add(vendor);
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string UpdateVendor(Vendor vendor, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _VendorRepository.Save(vendor);
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
    }
}
