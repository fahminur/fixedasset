﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Service
{
    public class FixedAssetMasterServiceRepository : IFixedAssetMasterService
    {
        private IFixedAssetMasterRepository _FixedAssetMasterRepository;
        private IFixedAssetMasterSequence _FixedAssetMasterSequence;
        private IInventoryRepository _inventoryRepository;
        public FixedAssetMasterServiceRepository(IFixedAssetMasterRepository FixedAssetMasterRepository, IFixedAssetMasterSequence FixedAssetMasterSequence, IInventoryRepository inventoryRepository)
        {
            _FixedAssetMasterRepository = FixedAssetMasterRepository;
            _FixedAssetMasterSequence = FixedAssetMasterSequence;
            _inventoryRepository = inventoryRepository;
        }

        public string SaveFixedAssetMaster(FixedAssetMaster FAM, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    FAM.AktivaID = _FixedAssetMasterSequence.createFixedAssetMasterNo("FAM", FAM.SiteID.Trim(), FAM.GroupAssetID.Trim(), year, month, user, company);
                    _FixedAssetMasterRepository.Add(FAM);

                    scope.Complete();
                }
                //if (FAM.IsOLS)
                //{
                //    GenerateOLSData(FAM, 1);
                //}

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string UpdateFixedAssetMaster(FixedAssetMaster FAM, User user, Company company)
        {
            string returnMessage = "";

            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //FAM.AktivaID = _FixedAssetMasterSequence.createFixedAssetMasterNo("BNIMF00", FAM.AssetAreaID.Trim(), year, month, user, company);

                    _FixedAssetMasterRepository.Save(FAM);
                    scope.Complete();
                }
                //if (FAM.IsOLS)
                //{
                //    if (_inventoryRepository.IsDuplicate(FAM.AktivaID))
                //    {
                //        GenerateOLSData(FAM, 2);
                //    }
                //    else
                //    {
                //        GenerateOLSData(FAM, 1);
                //    }
                //}

            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        void GenerateOLSData(FixedAssetMaster FAM, int State)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                //ols inventory data
                Inventory invt = new Inventory("");
                invt.UnitID = FAM.AktivaID;
                invt.AssetCode = FAM.AssetCode;
                invt.GRNCode = "GRN" + DateTime.Now.Year.ToString();
                invt.GRNDate = (DateTime?)null;
                invt.NoRangka = FAM.NoRangka == null ? "-" : FAM.NoRangka;
                invt.NoMesin = FAM.NoMesin;
                invt.NoPolisi = FAM.NoPol;
                invt.NewUsed = FAM.AssetConditionID == null ? "N" : FAM.AssetConditionID;
                invt.Color = "";
                invt.ManufacturingYear = FAM.TahunPembuatan.ToString() == null ? 0 : FAM.TahunPembuatan;
                invt.TotalOTR = Convert.ToDouble(FAM.HargaPerolehan);
                invt.SupplierID = FAM.VendorID;
                invt.ReceiveAt = "co";
                invt.UnitStatus = "RCV";
                invt.AgreementSeqNo = 0;
                invt.RentalType = "REN";
                invt.InProcessStatus = "NM";
                invt.AlocationUnitStatus = "RN";
                invt.OperationalStatus = "STB";
                invt.UnitCurrentStatus = "OHC";
                invt.PreDeliveryInspection = "1";
                invt.UserCreate = FAM.CreateUser;
                invt.DateCreate = DateTime.Now;
                invt.UserLastUpdate = FAM.CreateUser;
                invt.DateLastUpdate = DateTime.Now;

                if (State == 1)
                {
                    _inventoryRepository.Add(invt);
                    scope.Complete();
                }
                else if (State == 2)
                {
                    _inventoryRepository.Save(invt);
                    scope.Complete();
                }

            }

        }
    }
}
