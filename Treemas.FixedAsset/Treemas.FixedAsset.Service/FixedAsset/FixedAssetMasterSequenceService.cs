﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Service;
using Treemas.Foundation.Model;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Service
{
    public class FixedAssetMasterSequenceService : IFixedAssetMasterSequence
    {
        private IFixedAssetMasterSequenceRepository _FixedAssetMasterSequenceRepository;

        public FixedAssetMasterSequenceService(IFixedAssetMasterSequenceRepository FixedAssetMasterSequenceRepository)
        {
            _FixedAssetMasterSequenceRepository = FixedAssetMasterSequenceRepository;
        }

        public string createFixedAssetMasterNo(string SeqID, string BranchID, string GroupAsset, int year, int month, User user, Company company)
        {
            string newSeq = "";
            string transNo = "";
            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    FixedAssetSequence sequence = _FixedAssetMasterSequenceRepository.getFAMSeq(SeqID, year);

                    if (!sequence.IsNull())
                    {
                        newSeq = sequence.Seq1.ToString().PadLeft(sequence.LengthNo, '0');
                        sequence.Seq1 += 1;


                        transNo += (sequence.Prefix.IsNullOrEmpty()) ? "" : sequence.Prefix.Trim() + "-";
                        transNo += BranchID + "-";
                        transNo += GroupAsset + "-";
                        transNo += ((sequence.IsMonth) ? month.ToString().PadLeft(2, '0') : "") + "-";
                        transNo += ((sequence.IsYear) ? year.ToString().Substring(2, 2) : "") + "-";
                        transNo += newSeq;

                        sequence.UpdateDate = user.BusinessDate.CurrentDate;
                        sequence.UpdateUser = user.FirstName;
                        _FixedAssetMasterSequenceRepository.Save(sequence);
                    }
                    if (!newSeq.IsNullOrEmpty()) break;
                }
                catch (Exception ex)
                {
                    if (i == 5) throw new Exception(ex.Message); else Thread.Sleep(10);
                }
            }

            return transNo;
        }

        public string createNonAktivaNo(string SeqID, int year, int month, User user, Company company)
        {
            string newSeq = "";
            string transNo = "";
            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    FixedAssetSequence sequence = _FixedAssetMasterSequenceRepository.getNACSeq(SeqID, year);

                    if (!sequence.IsNull())
                    {
                        newSeq = sequence.Seq1.ToString().PadLeft(sequence.LengthNo, '0');
                        sequence.Seq1 += 1;

                        transNo += (sequence.Prefix.IsNullOrEmpty()) ? "" : sequence.Prefix.Trim() + "-";
                        transNo += ((sequence.IsMonth) ? month.ToString().PadLeft(2, '0') : "") + "-";
                        transNo += ((sequence.IsYear) ? year.ToString().Substring(2, 2) : "") + "-";
                        transNo += newSeq;

                        sequence.UpdateDate = user.BusinessDate.CurrentDate;
                        sequence.UpdateUser = user.FirstName;
                        _FixedAssetMasterSequenceRepository.Save(sequence);
                    }
                    if (!newSeq.IsNullOrEmpty()) break;
                }
                catch (Exception ex)
                {
                    if (i == 5) throw new Exception(ex.Message); else Thread.Sleep(10);
                }
            }

            return transNo;
        }
    }
}
