﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Controllers
{
    public class PenjualanAktivaController : PageController
    {
        private IPenjualanAktivaRepository _penjualanAktivaRepository;
        private IAuditTrailLog _auditRepository;
        private IPenjualanAktivaService _penjualanAktivaService;
        private IFixedAssetMasterRepository _fixedAssetMasterRepo;
        private ICurrencyRepository _currRepository;
        private IDepresiasiKomersialRepository _depresiasiKomersialRepositor;
        public PenjualanAktivaController(ISessionAuthentication sessionAuthentication, IPenjualanAktivaRepository penjualanAktivaRepository, IAuditTrailLog auditRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepo, ICurrencyRepository currRepository, IPenjualanAktivaService penjualanAktivaService, IDepresiasiKomersialRepository depresiasiKomersialRepositor)
            : base(sessionAuthentication)
        {
            this._penjualanAktivaRepository = penjualanAktivaRepository;
            this._auditRepository = auditRepository;
            this._penjualanAktivaService = penjualanAktivaService;
            this._fixedAssetMasterRepo = fixedAssetMasterRepo;
            this._currRepository = currRepository;
            this._depresiasiKomersialRepositor = depresiasiKomersialRepositor;

            Settings.ModuleName = "Penjualan Aktiva";
            Settings.Title = PenjualanAktivaResources.PageTitle;
        }
        protected override void Startup()
        {
            ViewData["PenjStatus"] = createPenjualanStatusSelect("N");
        }
        public ActionResult Search()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            JsonResult result = new JsonResult();
            try
            {
                PenjualanAktivaFilter filter = new PenjualanAktivaFilter();
                filter.InternalMemoNo = Request.QueryString["InternalMemoNo"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("InternalMemoNo")[0];
                filter.ReferenceNo = Request.QueryString["ReferenceNo"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ReferenceNo")[0];
                filter.TanggalPenjualanFrom = !Request.QueryString["TanggalPenjualanFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalPenjualanFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.TanggalPenjualanTo = !Request.QueryString["TanggalPenjualanTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalPenjualanTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.Status = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];

                // Initialization.   
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<PenjualanAktivaHeader> data;
                data = _penjualanAktivaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<PenjualanAktivaHeaderView>(new Converter<PenjualanAktivaHeader, PenjualanAktivaHeaderView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public PenjualanAktivaHeaderView ConvertFrom(PenjualanAktivaHeader item)
        {
            PenjualanAktivaHeaderView returnItem = new PenjualanAktivaHeaderView();

            returnItem.SellingNo = item.SellingNo;
            returnItem.SellingDate = Convert.ToDateTime(item.SellingDate).ToString("dd/MM/yyyy");
            returnItem.SellingDateString = string.Format("{0:dd/MM/yyyy}", item.SellingDate); //DateTime.ParseExact(item.SellingDate.ToString(), CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
            returnItem.ReferenceNo = item.ReferenceNo;
            returnItem.InternalMemoNo = item.InternalMemoNo;
            returnItem.Notes = item.Notes;
            returnItem.Status = item.Status;
            returnItem.StatusString = item.PenjualanStsEnum.ToDescription();
            returnItem.AllowEdit = item.AllowEdit;

            return returnItem;
        }

        public PenjualanAktivaDetailView ConvertFrom(PenjualanAktivaDetail item)
        {
            PenjualanAktivaDetailView returnItem = new PenjualanAktivaDetailView();
            FixedAssetMaster fam = _fixedAssetMasterRepo.getFixedAssetMaster(item.AktivaID);

            returnItem.SellingNo = item.SellingNo;
            returnItem.Seq = item.Seq;
            returnItem.AktivaID = item.AktivaID;
            returnItem.AktivaName = fam.IsNull() ? "" : fam.NamaAktiva.Trim();
            returnItem.SellingPrice = item.SellingPrice.ToString("N0");
            returnItem.NilaiAktiva = fam.IsNull() ? "0" : fam.SisaNilaiBukuKomersial.ToString("N0");
            returnItem.Buyer = item.Buyer;
            returnItem.StopDepreciationDate = Convert.ToDateTime(item.StopDepresiationDate).ToString("dd/MM/yyyy");
            returnItem.Notes = item.Notes;
            returnItem.DepreciationSeqNo = item.DepreciationSeqNo;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";

            PenjualanAktivaHeaderView data = new PenjualanAktivaHeaderView();
            data.Mode = "Create";
            data.PenjualanDetailList = CreateNewPenjualanDetail();
            data.SellingDate = DateTime.Now.ToString("dd/MM/yyyy");
            return CreateView(data);
        }

        private ViewResult CreateView(PenjualanAktivaHeaderView data)
        {
            ViewData["ActionName"] = data.Mode;

            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]

        public ActionResult Create(PenjualanAktivaHeaderView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();

            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    PenjualanAktivaHeader newData = new PenjualanAktivaHeader("");

                    newData.ReferenceNo = data.ReferenceNo;
                    newData.Notes = data.Notes;
                    newData.SellingDate = DateTime.ParseExact(data.SellingDate, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                    newData.InternalMemoNo = data.InternalMemoNo;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    newData.Status = "N";

                    newData.PenjualanAktivaDetailList = new List<PenjualanAktivaDetail>();
                    int i = 1;
                    foreach (PenjualanAktivaDetailView item in data.PenjualanDetailList)
                    {
                        PenjualanAktivaDetail newItem = new PenjualanAktivaDetail(i);
                        newItem.Seq = i;
                        newItem.AktivaID = item.AktivaID;
                        newItem.SellingPrice = double.Parse(item.SellingPrice);
                        newItem.Buyer = item.Buyer;
                        newItem.StopDepresiationDate = DateTime.ParseExact(item.StopDepreciationDate, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                        newItem.Notes = data.Notes;
                        newItem.DepreciationSeqNo = item.DepreciationSeqNo;
                        newData.PenjualanAktivaDetailList.Add(newItem);
                        i++;
                    }

                    _penjualanAktivaService.SavePenjualanAktiva(newData, user, company);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Save_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(PenjualanAktivaHeaderView data, string mode)
        {
            if (data.InternalMemoNo == "")
                return "Nomor internal memo harus di isi!";

            if (data.SellingDate.IsNull())
                return "Tanggal jual harus dipilih!";

            foreach(PenjualanAktivaDetailView item in data.PenjualanDetailList)
            {
                if (item.AktivaID.IsNullOrEmpty())
                    return "Kode aktiva harus di pilih!";
                if (item.SellingPrice.IsNullOrEmpty() || item.SellingPrice=="0")
                    return "Harga jual harus di isi!";
                if (item.Buyer.IsNullOrEmpty())
                    return "Pembeli harus di isi!";
            }

            if (mode == "Create")
            {
                if (_penjualanAktivaRepository.IsDuplicate(data.SellingNo))
                    return PenjualanAktivaResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            PenjualanAktivaHeader penjualan = _penjualanAktivaRepository.getPenjualanAktiva(id);
            PenjualanAktivaHeaderView data = ConvertFrom(penjualan);
            data.Mode = "Edit";
            IList<PenjualanAktivaDetail> penjualanDtl = _penjualanAktivaRepository.getPenjualanDetail(data.SellingNo);
            IList<PenjualanAktivaDetailView> penjualanDtlView = penjualanDtl.ToList().ConvertAll<PenjualanAktivaDetailView>(new Converter<PenjualanAktivaDetail, PenjualanAktivaDetailView>(ConvertFrom));
            data.PenjualanDetailList = penjualanDtlView.IsNullOrEmpty() ? CreateNewPenjualanDetail() : penjualanDtlView;

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Edit")]
        public ActionResult Edit(PenjualanAktivaHeaderView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();

            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    PenjualanAktivaHeader newData = _penjualanAktivaRepository.getPenjualanAktiva(data.SellingNo);

                    newData.ReferenceNo = data.ReferenceNo;
                    newData.Notes = data.Notes;
                    newData.SellingDate = DateTime.ParseExact(data.SellingDate, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                    newData.InternalMemoNo = data.InternalMemoNo;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    newData.Status = "N";

                    newData.PenjualanAktivaDetailList = new List<PenjualanAktivaDetail>();
                    int i = 1;
                    foreach (PenjualanAktivaDetailView item in data.PenjualanDetailList)
                    {
                        PenjualanAktivaDetail newItem = new PenjualanAktivaDetail(i);
                        newItem.AktivaID = item.AktivaID;
                        newItem.Seq = i;
                        newItem.SellingPrice = double.Parse(item.SellingPrice);
                        newItem.Buyer = item.Buyer;
                        newItem.StopDepresiationDate = DateTime.ParseExact(item.StopDepreciationDate, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                        newItem.Notes = data.Notes;
                        newItem.DepreciationSeqNo = item.DepreciationSeqNo;
                        newData.PenjualanAktivaDetailList.Add(newItem);
                        i++;
                    }
                    _penjualanAktivaService.UpdatePenjualanAktiva(newData, user, company);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Edit");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Update_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                PenjualanAktivaHeader newData = _penjualanAktivaRepository.getPenjualanAktiva(Id);
                _penjualanAktivaRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

        private IList<PenjualanAktivaDetailView> CreateNewPenjualanDetail()
        {
            IList<PenjualanAktivaDetailView> returnDetail = new List<PenjualanAktivaDetailView>();
            returnDetail.Add(AddNewAktivaRecord(returnDetail));
            return returnDetail;
        }
        private PenjualanAktivaDetailView AddNewAktivaRecord(IList<PenjualanAktivaDetailView> DetailList)
        {
            PenjualanAktivaDetailView detail = new PenjualanAktivaDetailView();
            detail.Seq = DetailList.IsNull() ? 1 : (DetailList.Count > 0) ? DetailList.LastOrDefault().Seq + 1 : 1;
            detail.StopDepreciationDate = DateTime.Now.ToString("dd/MM/yyyy");
            return detail;
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckAddPenjualanDetail")]
        public ActionResult NoAuthCheckAddPenjualanDetail(PenjualanAktivaHeaderView paH)
        {
            ViewData["ActionName"] = paH.Mode;
            paH.PenjualanDetailList.Add(AddNewAktivaRecord(paH.PenjualanDetailList));
            return CreateView(paH);
        }

        [HttpPost]
        public ActionResult NoAuthCheckDeletePenjualanDetail(PenjualanAktivaHeaderView paH, int RecordID)
        {
            ViewData["ActionName"] = paH.Mode;
            paH.PenjualanDetailList.Remove(paH.PenjualanDetailList.FindElement(d => d.Seq == RecordID));
            if (paH.PenjualanDetailList.Count == 0)
            {
                paH.PenjualanDetailList = CreateNewPenjualanDetail();
            }

            return CreateView(paH);
        }

        private IList<SelectListItem> createPenjualanStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(PenjualanStatusEnum)).Cast<PenjualanStatusEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NoAuthCheckGetNilaiAsset(string id, string SellingDate)
        {
            DateTime SellDate = DateTime.ParseExact(SellingDate,  "dd/MM/yyyy", CultureInfo.CurrentCulture);

            PenjualanAktivaDetailView returnItem = new PenjualanAktivaDetailView();
            FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(id);
            DepresiasiKomersial depKomr = _depresiasiKomersialRepositor.getSaldoDepresiasiKomersial(id, SellDate.Year, SellDate.Month);
            //DepresiasiKomersial depKomr = _depresiasiKomersialRepositor.getDepresiasiKomersial(id);

            double NilaiAktiva = depKomr.IsNull() ? fixedAsset.IsNull() ? 0 : fixedAsset.SisaNilaiBukuKomersial : depKomr.Saldo;
            int DeprSeqNo = depKomr.IsNull() ? 0 : depKomr.SeqNo;

            returnItem.AktivaName = fixedAsset.IsNull() ? "" : fixedAsset.NamaAktiva;
            returnItem.NilaiAktiva = NilaiAktiva.ToString("N0");
            returnItem.DepreciationSeqNo = DeprSeqNo;

            return Json(returnItem, JsonRequestBehavior.AllowGet);
        }
    }
}
