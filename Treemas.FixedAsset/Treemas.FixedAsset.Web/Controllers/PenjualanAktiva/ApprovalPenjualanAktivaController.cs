﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;
using System.Transactions;

namespace Treemas.FixedAsset.Controllers
{
    public class ApprovalPenjualanAktivaController : PageController
    {
        private IPenjualanAktivaRepository _penjualanAktivaRepository;
        private IAuditTrailLog _auditRepository;

        private IFixedAssetMasterRepository _fixedAssetMasterRepo;
        private IRequestReceiveRepository _receiveRepo;
        private ICurrencyRepository _currRepository;
        private IGroupAssetRepository _grpAsetRepo;
        private IPaymentAllocationRepository _paymentAllocationRepository;
        private IGLJournalService _glJournalService;
        private IIfinsGLJournalRepository _ifinsGLJournalRepository;

        public ApprovalPenjualanAktivaController(ISessionAuthentication sessionAuthentication, IPenjualanAktivaRepository penjualanAktivaRepository, IAuditTrailLog auditRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepo, ICurrencyRepository currRepository, IRequestReceiveRepository receiveRepo, IIfinsGLJournalRepository ifinsGLJournalRepository,
            IGroupAssetRepository grpAsetRepo, IPaymentAllocationRepository paymentAllocationRepository, IGLJournalService glJournalService)
            : base(sessionAuthentication)
        {
            this._penjualanAktivaRepository = penjualanAktivaRepository;
            this._auditRepository = auditRepository;

            this._fixedAssetMasterRepo = fixedAssetMasterRepo;
            this._currRepository = currRepository;
            this._receiveRepo = receiveRepo;
            this._grpAsetRepo = grpAsetRepo;
            this._paymentAllocationRepository = paymentAllocationRepository;
            this._glJournalService = glJournalService;
            this._ifinsGLJournalRepository = ifinsGLJournalRepository;
            Settings.ModuleName = "Approval Penjualan Aktiva";
            Settings.Title = "Approval " + PenjualanAktivaResources.PageTitle;
        }
        protected override void Startup()
        {
            ViewData["PenjStatus"] = createPenjualanStatusSelect("N");
        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            try
            {
                PenjualanAktivaFilter filter = new PenjualanAktivaFilter();
                filter.InternalMemoNo = Request.QueryString["InternalMemoNo"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("InternalMemoNo")[0];
                filter.ReferenceNo = Request.QueryString["ReferenceNo"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ReferenceNo")[0];
                filter.TanggalPenjualanFrom = !Request.QueryString["TanggalPenjualanFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalPenjualanFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.TanggalPenjualanTo = !Request.QueryString["TanggalPenjualanTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalPenjualanTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.Status = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];

                // Initialization.   
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<PenjualanAktivaHeader> data;
                data = _penjualanAktivaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<PenjualanAktivaHeaderView>(new Converter<PenjualanAktivaHeader, PenjualanAktivaHeaderView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public PenjualanAktivaHeaderView ConvertFrom(PenjualanAktivaHeader item)
        {
            PenjualanAktivaHeaderView returnItem = new PenjualanAktivaHeaderView();

            returnItem.SellingNo = item.SellingNo;
            returnItem.SellingDate = Convert.ToDateTime(item.SellingDate).ToString("dd/MM/yyyy");
            returnItem.ReferenceNo = item.ReferenceNo;
            returnItem.InternalMemoNo = item.InternalMemoNo;
            returnItem.Notes = item.Notes;
            returnItem.Status = item.Status;
            returnItem.StatusString = item.PenjualanStsEnum.ToDescription();
            returnItem.AllowEdit = item.AllowEdit;
            returnItem.AllowApprove = item.AllowApprove;
            returnItem.AllowReject = item.AllowAReject;
            return returnItem;
        }

        public PenjualanAktivaDetailView ConvertFrom(PenjualanAktivaDetail item)
        {
            PenjualanAktivaDetailView returnItem = new PenjualanAktivaDetailView();
            FixedAssetMaster fam = _fixedAssetMasterRepo.getFixedAssetMaster(item.AktivaID);

            returnItem.SellingNo = item.SellingNo;
            returnItem.AktivaID = item.AktivaID;
            returnItem.AktivaName = fam.IsNull() ? "" : fam.NamaAktiva.Trim();
            returnItem.SellingPrice = item.SellingPrice.ToString("N0");
            returnItem.NilaiAktiva = fam.IsNull() ? "0" : fam.SisaNilaiBukuKomersial.ToString("N0");
            returnItem.Buyer = item.Buyer;
            returnItem.StopDepreciationDate = Convert.ToDateTime(item.StopDepresiationDate).ToString("dd/MM/yyyy");
            returnItem.Notes = item.Notes;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            PenjualanAktivaHeaderView data = new PenjualanAktivaHeaderView();
            return CreateView(data);
        }

        private ViewResult CreateView(PenjualanAktivaHeaderView data)
        {
            return View("Detail", data);
        }

        private IList<PenjualanAktivaDetailView> CreateNewPenjualanDetail()
        {
            IList<PenjualanAktivaDetailView> returnDetail = new List<PenjualanAktivaDetailView>();
            returnDetail.Add(AddNewAktivaRecord(returnDetail));
            return returnDetail;
        }
        private PenjualanAktivaDetailView AddNewAktivaRecord(IList<PenjualanAktivaDetailView> DetailList)
        {
            PenjualanAktivaDetailView detail = new PenjualanAktivaDetailView();
            detail.Seq = DetailList.IsNull() ? 1 : (DetailList.Count > 0) ? DetailList.LastOrDefault().Seq + 1 : 1;
            detail.StopDepreciationDate = DateTime.Now.ToString("dd/MM/yyyy");
            return detail;
        }

        // GET: Role/Edit/5
        public ActionResult Approve(string id)
        {
            PenjualanAktivaHeader Penjualan = _penjualanAktivaRepository.getPenjualanAktiva(id);
            PenjualanAktivaHeaderView data = ConvertFrom(Penjualan);
            ViewData["ActionName"] = "Approve";
            data.Mode = "Approve";
            IList<PenjualanAktivaDetail> penjualanDtl = _penjualanAktivaRepository.getPenjualanDetail(data.SellingNo);
            IList<PenjualanAktivaDetailView> penjualanDtlView = penjualanDtl.ToList().ConvertAll<PenjualanAktivaDetailView>(new Converter<PenjualanAktivaDetail, PenjualanAktivaDetailView>(ConvertFrom));
            data.PenjualanDetailList = penjualanDtlView.IsNullOrEmpty() ? CreateNewPenjualanDetail() : penjualanDtlView;

            return CreateView(data);
        }


        [HttpPost]
        public ActionResult Approve(PenjualanAktivaHeaderView data)
        {
            ViewData["ActionName"] = "Approve";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            string message = "";
            try
            {
                if (!data.AllowApprove)
                {
                    message = "Anda tidak punya akses untuk menyetujui dokumen ini!";
                }
                else
                {
                    PenjualanAktivaHeader newData = _penjualanAktivaRepository.getPenjualanAktiva(data.SellingNo);
                    newData.PenjualanAktivaDetailList = _penjualanAktivaRepository.getPenjualanDetail(data.SellingNo);

                    newData.Status = PenjualanStatusEnum.A.ToString();
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _penjualanAktivaRepository.Save(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Approval");

                    //_penjualanAktivaRepository.CreateJournalPenjualan(newData.SellingNo.Trim());
                    //if (data.ApprovalStage.Trim() == "A3")
                    //{
                    foreach (PenjualanAktivaDetailView item in data.PenjualanDetailList)
                    {
                        FixedAssetMaster Fam = _fixedAssetMasterRepo.getFixedAssetMaster(item.AktivaID);

                        GroupAsset grpAset = _grpAsetRepo.getGroupAsset(Fam.GroupAssetID);
                        RequestReceive receive = new RequestReceive("");
                        receive.AktivaId = item.AktivaID;
                        receive.COA = Fam.IsOLS ? Fam.COAAktivaID : grpAset.CoaPerolehan;
                        receive.Buyer = item.Buyer;
                        receive.Amount = double.Parse(item.SellingPrice);
                        receive.ReffNo = data.ReferenceNo;
                        receive.InternalMemoNo = data.InternalMemoNo;
                        receive.Notes = item.Notes;
                        receive.Status = PenjualanStatusEnum.N.ToString();
                        receive.BranchID = Fam.SiteID.Trim();

                        _receiveRepo.Add(receive);

                        _auditRepository.SaveAuditTrail(Settings.ModuleName, receive, user.Username, "New Request Receive");

                        Fam.Status = AssetStatusEnum.S.ToString();

                        _fixedAssetMasterRepo.Save(Fam);

                        _auditRepository.SaveAuditTrail(Settings.ModuleName, Fam, user.Username, "Update Fixed Asset Sold Status");
                    }


                    //gl journal
                    //preparing gl 
                    List<string> SiteIDs = new List<string>();
                    IList<GLJournalH> jurnalHList = new List<GLJournalH>();
                    //IList<IfinsGLJournalH> ifinsjurnalHList = new List<IfinsGLJournalH>();
                    int year = int.Parse(DateTime.Now.ToString("yyyy"));
                    int month = int.Parse(DateTime.Now.ToString("MM"));
                    PaymentAllocation payallocAP = _paymentAllocationRepository.getPaymentAllocation("APVE");

                    foreach (var fa in data.PenjualanDetailList)
                    {
                        FixedAssetMaster famData = _fixedAssetMasterRepo.getFixedAssetMaster(fa.AktivaID);

                        if (!SiteIDs.Contains(famData.SiteID.Trim()))
                        {
                            SiteIDs.Add(famData.SiteID.Trim());
                        }
                    }

                    foreach (string SiteID in SiteIDs)
                    {
                        GLJournalH jurnalH = new GLJournalH("");
                        //IfinsGLJournalH ifinsJurnalH = new IfinsGLJournalH("");

                        #region Jurnal Hdr
                        jurnalH.CompanyID = "001";
                        jurnalH.BranchID = SiteID.Trim();
                        jurnalH.PeriodYear = year.ToString();
                        jurnalH.PeriodMonth = month.ToString();
                        jurnalH.TransactionID = "RPA";
                        jurnalH.Reff_No = newData.SellingNo.Trim();
                        jurnalH.Reff_Date = DateTime.Now;
                        jurnalH.Tr_Date = DateTime.Now;
                        jurnalH.tr_Date1 = (DateTime?)null;
                        jurnalH.PrintDate = (DateTime?)null;
                        jurnalH.LastRevisiPosting = (DateTime?)null;
                        jurnalH.Tr_Desc = "Terima Penjualan Asset";
                        jurnalH.JournalAmount = 0;// newData.InvoiceAmount;
                        jurnalH.BatchID = "RPA";
                        jurnalH.SubSystem = "FA";
                        jurnalH.Status_Tr = "OP";
                        jurnalH.IsActive = true;
                        jurnalH.IsValid = true;
                        jurnalH.Flag = "R";
                        jurnalH.DtmUpd = DateTime.Now;
                        jurnalH.UsrUpd = user.Username;

                        //ifinsJurnalH.CompanyID = "001";
                        //ifinsJurnalH.BranchID = SiteID.Trim();
                        //ifinsJurnalH.PeriodYear = year.ToString();
                        //ifinsJurnalH.PeriodMonth = month.ToString();
                        //ifinsJurnalH.TransactionID = "RPA";
                        //ifinsJurnalH.Reff_No = newData.SellingNo.Trim();
                        //ifinsJurnalH.Reff_Date = DateTime.Now;
                        //ifinsJurnalH.Tr_Date = DateTime.Now;
                        //ifinsJurnalH.tr_Date1 = (DateTime?)null;
                        //ifinsJurnalH.PrintDate = (DateTime?)null;
                        //ifinsJurnalH.LastRevisiPosting = (DateTime?)null;
                        //ifinsJurnalH.Tr_Desc = "Terima Penjualan Asset";
                        //ifinsJurnalH.JournalAmount = 0;// newData.InvoiceAmount;
                        //ifinsJurnalH.BatchID = "RPA";
                        //ifinsJurnalH.SubSystem = "FA";
                        //ifinsJurnalH.Status_Tr = "OP";
                        //ifinsJurnalH.IsActive = true;
                        //ifinsJurnalH.IsValid = true;
                        //ifinsJurnalH.Flag = "R";
                        //ifinsJurnalH.DtmUpd = DateTime.Now;
                        //ifinsJurnalH.UsrUpd = user.Username;
                        #endregion

                        int i = 0;
                        jurnalH.GLJournalDtlList = new List<GLJournalD>();
                        //ifinsJurnalH.GLJournalDtlList = new List<IfinsGLJournalD>();
                        i = 0;
                        double totalPrice = 0;

                        #region Jurnal Dtl
                        foreach (PenjualanAktivaDetail item in newData.PenjualanAktivaDetailList)
                        {

                            bool isSurplus = false;

                            //DEBT
                            FixedAssetMaster famData = _fixedAssetMasterRepo.getFixedAssetMaster(item.AktivaID);
                            if (famData.SiteID.Trim() != SiteID.Trim()) { continue; }

                            if (famData.SisaNilaiBukuKomersial < item.SellingPrice)
                            {
                                isSurplus = true;
                            }
                            else
                            {
                                isSurplus = false;
                            }

                            GLJournalD jurnalD = new GLJournalD(i);
                            GroupAsset GA = _grpAsetRepo.getGroupAsset(famData.GroupAssetID);

                            if (isSurplus)
                            {
                                jurnalD.CompanyID = "001";
                                jurnalD.BranchID = famData.SiteID.Trim();
                                jurnalD.SequenceNo = i + 1;
                                jurnalD.CoaCo = "001";
                                jurnalD.CoaBranch = famData.SiteID.Trim();
                                jurnalD.CoaId = payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                                jurnalD.TransactionID = "RPA";
                                jurnalD.Tr_Desc = "AR Penjualan Asset";
                                jurnalD.Post = "D";
                                jurnalD.Amount = item.SellingPrice;
                                jurnalD.CoaId_X = "-";
                                jurnalD.PaymentAllocationId = "FA";
                                jurnalD.ProductId = "-";
                                jurnalD.DtmUpd = jurnalH.DtmUpd;
                                jurnalD.UsrUpd = jurnalH.UsrUpd;

                                jurnalH.JournalAmount += jurnalD.Amount;
                                jurnalH.GLJournalDtlList.Add(jurnalD);

                                //ifinsJurnalH.JournalAmount += jurnalD.Amount;
                                //ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                                totalPrice += jurnalD.Amount;

                                i++;

                                //credit 1
                                GLJournalD jurnalC = new GLJournalD(i);

                                jurnalC.CompanyID = "001";
                                jurnalC.BranchID = jurnalH.BranchID;
                                jurnalC.SequenceNo = i + 1;
                                jurnalC.CoaCo = "001";
                                jurnalC.CoaBranch = jurnalH.BranchID;
                                jurnalC.CoaId = famData.IsOLS ? famData.COAAktivaID.Trim() + "-" + jurnalH.BranchID : GA.CoaPerolehan.Trim() + "-" + jurnalH.BranchID;
                                jurnalC.TransactionID = "RPA";
                                jurnalC.Tr_Desc = "Penjualan Asset";
                                jurnalC.Post = "C";
                                jurnalC.Amount = famData.SisaNilaiBukuKomersial;
                                jurnalC.CoaId_X = "-";
                                jurnalC.PaymentAllocationId = "AR";
                                jurnalC.ProductId = "-";
                                jurnalC.DtmUpd = jurnalH.DtmUpd;
                                jurnalC.UsrUpd = jurnalH.UsrUpd;

                                jurnalH.GLJournalDtlList.Add(jurnalC);

                                //ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));

                                i++;

                                //credit 2
                                i++;
                                GLJournalD jurnalCLaba = new GLJournalD(i);

                                jurnalCLaba.CompanyID = "001";
                                jurnalCLaba.BranchID = jurnalH.BranchID;
                                jurnalCLaba.SequenceNo = i + 1;
                                jurnalCLaba.CoaCo = "001";
                                jurnalCLaba.CoaBranch = jurnalH.BranchID;
                                jurnalCLaba.CoaId = famData.IsOLS ? famData.COAAktivaID.Trim() + "-" + jurnalH.BranchID : GA.CoaPerolehan.Trim() + "-" + jurnalH.BranchID;
                                jurnalCLaba.TransactionID = "RPA";
                                jurnalCLaba.Tr_Desc = "Laba Penjualan Asset";
                                jurnalCLaba.Post = "C";
                                jurnalCLaba.Amount = totalPrice - famData.SisaNilaiBukuKomersial;
                                jurnalCLaba.CoaId_X = "-";
                                jurnalCLaba.PaymentAllocationId = "AR";
                                jurnalCLaba.ProductId = "-";
                                jurnalCLaba.DtmUpd = jurnalH.DtmUpd;
                                jurnalCLaba.UsrUpd = jurnalH.UsrUpd;

                                jurnalH.JournalAmount += jurnalCLaba.Amount;
                                jurnalH.GLJournalDtlList.Add(jurnalCLaba);

                                //ifinsJurnalH.JournalAmount += jurnalCLaba.Amount;
                                //ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalCLaba));
                            }

                            else
                            {
                                jurnalD.CompanyID = "001";
                                jurnalD.BranchID = famData.SiteID.Trim();
                                jurnalD.SequenceNo = i + 1;
                                jurnalD.CoaCo = "001";
                                jurnalD.CoaBranch = famData.SiteID.Trim();
                                jurnalD.CoaId = payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                                jurnalD.TransactionID = "RPA";
                                jurnalD.Tr_Desc = "AR Penjualan Asset";
                                jurnalD.Post = "C";
                                jurnalD.Amount = item.SellingPrice;
                                jurnalD.CoaId_X = "-";
                                jurnalD.PaymentAllocationId = "FA";
                                jurnalD.ProductId = "-";
                                jurnalD.DtmUpd = jurnalH.DtmUpd;
                                jurnalD.UsrUpd = jurnalH.UsrUpd;

                                jurnalH.JournalAmount += jurnalD.Amount;
                                jurnalH.GLJournalDtlList.Add(jurnalD);

                                //ifinsJurnalH.JournalAmount += jurnalD.Amount;
                                //ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                                totalPrice += jurnalD.Amount;

                                i++;

                                //CREDIT 
                                if (totalPrice > 0)
                                {
                                    //credit 1
                                    GLJournalD jurnalCRugi = new GLJournalD(i);

                                    jurnalCRugi.CompanyID = "001";
                                    jurnalCRugi.BranchID = jurnalH.BranchID;
                                    jurnalCRugi.SequenceNo = i + 1;
                                    jurnalCRugi.CoaCo = "001";
                                    jurnalCRugi.CoaBranch = jurnalH.BranchID;
                                    jurnalCRugi.CoaId = famData.IsOLS ? famData.COAAktivaID.Trim() + "-" + jurnalH.BranchID : GA.CoaPerolehan.Trim() + "-" + jurnalH.BranchID;
                                    jurnalCRugi.TransactionID = "RPA";
                                    jurnalCRugi.Tr_Desc = "Rugi Penjualan Asset";
                                    jurnalCRugi.Post = "C";
                                    jurnalCRugi.Amount = famData.SisaNilaiBukuKomersial - totalPrice;
                                    jurnalCRugi.CoaId_X = "-";
                                    jurnalCRugi.PaymentAllocationId = "AR";
                                    jurnalCRugi.ProductId = "-";
                                    jurnalCRugi.DtmUpd = jurnalH.DtmUpd;
                                    jurnalCRugi.UsrUpd = jurnalH.UsrUpd;

                                    jurnalH.JournalAmount += jurnalCRugi.Amount;
                                    jurnalH.GLJournalDtlList.Add(jurnalCRugi);

                                    //ifinsJurnalH.JournalAmount += jurnalCRugi.Amount;
                                    //ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalCRugi));

                                    //Debt
                                    i++;
                                    GLJournalD jurnalC = new GLJournalD(i);

                                    jurnalC.CompanyID = "001";
                                    jurnalC.BranchID = jurnalH.BranchID;
                                    jurnalC.SequenceNo = i + 1;
                                    jurnalC.CoaCo = "001";
                                    jurnalC.CoaBranch = jurnalH.BranchID;
                                    jurnalC.CoaId = famData.IsOLS ? famData.COAAktivaID.Trim() + "-" + jurnalH.BranchID : GA.CoaPerolehan.Trim() + "-" + jurnalH.BranchID;
                                    jurnalC.TransactionID = "RPA";
                                    jurnalC.Tr_Desc = "Fixed Asset";
                                    jurnalC.Post = "D";
                                    jurnalC.Amount = famData.SisaNilaiBukuKomersial;
                                    jurnalC.CoaId_X = "-";
                                    jurnalC.PaymentAllocationId = "AR";
                                    jurnalC.ProductId = "-";
                                    jurnalC.DtmUpd = jurnalH.DtmUpd;
                                    jurnalC.UsrUpd = jurnalH.UsrUpd;

                                    jurnalH.GLJournalDtlList.Add(jurnalC);

                                    //ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));
                                }
                            }
                        }
                        #endregion

                        jurnalHList.Add(jurnalH);
                        //ifinsjurnalHList.Add(ifinsJurnalH);
                    }

                    for (int j = 0; j <= jurnalHList.Count - 1; j++)
                    {
                        message = _glJournalService.SaveJournal(jurnalHList[j], user, company);
                        string[] msgResult = message.Split('-');
                        if (msgResult.Length > 0)
                        {
                            if (msgResult[0].Trim() == "Success")
                            {
                                //ifinsjurnalHList[j].Tr_Nomor = msgResult[1].Trim();

                                //_ifinsGLJournalRepository.Add(ifinsjurnalHList[j]);

                                //if (ifinsjurnalHList[j].GLJournalDtlList != null)
                                //{
                                //    foreach (var item in ifinsjurnalHList[j].GLJournalDtlList)
                                //    {
                                //        item.Tr_Nomor = ifinsjurnalHList[j].Tr_Nomor;
                                //        _ifinsGLJournalRepository.AddGlJournalDtl(item);
                                //    }
                                //}

                                _auditRepository.SaveAuditTrail(Settings.ModuleName, jurnalHList[j], user.Username, "Create");
                            }
                        }
                    }
                    message = "";
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Approve_succes));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private IfinsGLJournalD ConvertForm(GLJournalD item)
        {
            IfinsGLJournalD returnItem = new IfinsGLJournalD(Convert.ToInt32(item.Id));

            returnItem.CompanyID = item.CompanyID;
            returnItem.BranchID = item.BranchID;
            returnItem.SequenceNo = item.SequenceNo;
            returnItem.CoaCo = item.CoaCo;
            returnItem.CoaBranch = item.CoaBranch;
            returnItem.CoaId = item.CoaId;
            returnItem.TransactionID = item.TransactionID;
            returnItem.Tr_Desc = item.Tr_Desc;
            returnItem.Post = item.Post;
            returnItem.Amount = item.Amount;
            returnItem.CoaId_X = item.CoaId_X;
            returnItem.PaymentAllocationId = item.PaymentAllocationId;
            returnItem.ProductId = item.ProductId;
            returnItem.DtmUpd = item.DtmUpd;
            returnItem.UsrUpd = item.UsrUpd;

            return returnItem;
        }

        public ActionResult Reject(string id)
        {
            ViewData["ActionName"] = "Reject";
            User user = Lookup.Get<User>();
            string message = "";
            PenjualanAktivaHeader Penjualan = _penjualanAktivaRepository.getPenjualanAktiva(id);
            PenjualanAktivaHeaderView data = ConvertFrom(Penjualan);

            try
            {
                if (!data.AllowReject)
                {
                    message = "Anda tidak punya akses untuk menolak dokumen ini!";
                }
                else
                {
                    Penjualan.Status = PenjualanStatusEnum.R.ToString();
                    Penjualan.UpdateUser = user.Username;
                    Penjualan.UpdateDate = DateTime.Now;
                    //switch (Stage.Trim())
                    //{
                    //    case "A1":
                    //newData.IsApproved1 = false;
                    //newData.Approved1User = user.Username.Trim();
                    //newData.Approved1Date = DateTime.Now;
                    //break;
                    //case "A2":
                    //    newData.IsApproved2 = false;
                    //    newData.Approved2User = user.Username.Trim();
                    //    newData.Approved2Date = DateTime.Now;
                    //    break;
                    //case "A3":
                    //    newData.IsApproved3 = false;
                    //    newData.Approved3User = user.Username.Trim();
                    //    newData.Approved3Date = DateTime.Now;
                    //    break;
                    //    default:
                    //        break;
                    //}


                    _penjualanAktivaRepository.Save(Penjualan);

                    //if (Stage.Trim() == "A1")
                    //{
                    foreach (PenjualanAktivaDetailView item in data.PenjualanDetailList)
                    {
                        FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(item.AktivaID);
                        fixedAsset.Status = AssetStatusEnum.A.ToString();
                        _fixedAssetMasterRepo.Save(fixedAsset);
                    }
                    //}

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, Penjualan, user.Username, "Reject");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Reject_success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private IList<SelectListItem> createPenjualanStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(PenjualanStatusEnum)).Cast<PenjualanStatusEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }
    }
}
