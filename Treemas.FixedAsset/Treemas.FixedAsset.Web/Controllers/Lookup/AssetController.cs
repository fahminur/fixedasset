﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.FixedAsset.Web.Controllers
{
    public class AssetController : PageController
    {
        IAssetRepository _asssetRepository;
        public AssetController(ISessionAuthentication sessionAuthentication, IAssetRepository asssetRepository) : base(sessionAuthentication)
        {
            _asssetRepository = asssetRepository;
            Settings.ModuleName = "OLS Asset Lookup";
            Settings.Title = GroupAssetResources.PageTitle;
        }

        public ActionResult NoAuthCheckSearch()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                User user = Lookup.Get<User>();
                //var company = Lookup.Get<Company>();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                //string siteId = user.Site.SiteId;
                //string companyId = company.CompanyId;

                // Loading.   
                PagedResult<Asset> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _asssetRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _asssetRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<AssetView>(new Converter<Asset, AssetView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public AssetView ConvertFrom(Asset item)
        {
            AssetView returnItem = new AssetView();

            returnItem.AssetCode = item.AssetCode;
            returnItem.AssetName = item.AssetName;
            returnItem.AssetTypeID = item.AssetTypeID;

            return returnItem;
        }
    }
}
