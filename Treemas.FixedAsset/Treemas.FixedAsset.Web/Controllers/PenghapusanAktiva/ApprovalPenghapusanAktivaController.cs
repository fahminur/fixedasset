﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Controllers
{
    public class ApprovalPenghapusanAktivaController : PageController
    {
        private IPenghapusanAktivaRepository _penghapusanAktivaRepository;
        private IAuditTrailLog _auditRepository;
        private IFixedAssetMasterRepository _fixedAssetMasterRepo;

        public ApprovalPenghapusanAktivaController(ISessionAuthentication sessionAuthentication, IPenghapusanAktivaRepository penghapusanAktivaRepository, IAuditTrailLog auditRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepo) : base(sessionAuthentication)
        {
            this._penghapusanAktivaRepository = penghapusanAktivaRepository;
            this._auditRepository = auditRepository;
            this._fixedAssetMasterRepo = fixedAssetMasterRepo;

            Settings.ModuleName = "Approval Penghapusan Aktiva";
            Settings.Title = "Approval Penghapusan Aktiva";
        }

        protected override void Startup()
        {
            ViewData["PenghapusanStatus"] = createPenghapusanStatusSelect("N");
        }
        public ActionResult Search()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            JsonResult result = new JsonResult();
            try
            {
                PenghapusanAktivaFilter filter = new PenghapusanAktivaFilter();
                filter.InternalMemoNo = Request.QueryString["InternalMemoNo"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("InternalMemoNo")[0];
                filter.DeleteDateFrom = !Request.QueryString["DeleteDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("DeleteDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.DeleteDateTo = !Request.QueryString["DeleteDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("DeleteDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.Status = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];

                // Initialization.   
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<PenghapusanAktivaHeader> data;
                data = _penghapusanAktivaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<PenghapusanAktivaHeaderView>(new Converter<PenghapusanAktivaHeader, PenghapusanAktivaHeaderView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public PenghapusanAktivaHeaderView ConvertFrom(PenghapusanAktivaHeader item)
        {
            PenghapusanAktivaHeaderView returnItem = new PenghapusanAktivaHeaderView();
            FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(item.DeleteNo);

            returnItem.DeleteNo = item.DeleteNo;
            returnItem.DeleteDate = item.DeleteDate.ToString("dd/MM/yyyy");
            returnItem.InternalMemoNo = item.InternalMemoNo;
            returnItem.AllowApprove = item.AllowApprove;
            returnItem.AllowReject = item.AllowReject;
            returnItem.Status = item.Status;
            returnItem.Notes = item.Notes;
            returnItem.StatusString = item.PenghapusanStsEnum.ToDescription();

            return returnItem;
        }

        public PenghapusanAktivaDetailView ConvertFrom(PenghapusanAktivaDetail item)
        {
            PenghapusanAktivaDetailView returnItem = new PenghapusanAktivaDetailView();
            FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(item.AktivaID);

            returnItem.DeleteNo = item.DeleteNo;
            returnItem.Seq = item.Seq;
            returnItem.AktivaID = item.AktivaID;
            returnItem.AktivaName = fixedAsset.IsNull() ? "" : fixedAsset.NamaAktiva.Trim();
            returnItem.DeleteReason = item.DeleteReason;
            returnItem.SisaNilaiBukuKomersial = fixedAsset.IsNull() ? "0" : fixedAsset.SisaNilaiBukuKomersial.ToString("N0");
            returnItem.Notes = item.Notes;

            return returnItem;
        }

        private IList<PenghapusanAktivaDetailView> CreateNewPenghapusanDetail()
        {
            IList<PenghapusanAktivaDetailView> returnDetail = new List<PenghapusanAktivaDetailView>();
            returnDetail.Add(AddNewAktivaRecord(returnDetail));
            return returnDetail;
        }
        private PenghapusanAktivaDetailView AddNewAktivaRecord(IList<PenghapusanAktivaDetailView> DetailList)
        {
            PenghapusanAktivaDetailView detail = new PenghapusanAktivaDetailView();
            detail.Seq = DetailList.IsNull() ? 1 : (DetailList.Count > 0) ? DetailList.LastOrDefault().Seq + 1 : 1;
            return detail;
        }

        private ViewResult CreateView(PenghapusanAktivaHeaderView data)
        {
            return View("Detail", data);
        }

        public ActionResult Reject(string id)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            PenghapusanAktivaHeader newData = _penghapusanAktivaRepository.getPenghapusanAktiva(id);
            PenghapusanAktivaHeaderView data = ConvertFrom(newData);

            try
            {
                if (!newData.AllowReject)
                {
                    message = "Anda tidak punya akses untuk menolak dokumen ini!";
                }
                else
                {
                    newData.Status = PenjualanStatusEnum.R.ToString();
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    //switch (Stage.Trim())
                    //{
                    //    case "A1":
                    //        newData.IsApproved1 = false;
                    //        newData.Approved1User = user.Username.Trim();
                    //        newData.Approved1Date = DateTime.Now;
                    //        break;
                    //case "A2":
                    //    newData.IsApproved2 = false;
                    //    newData.Approved2User = user.Username.Trim();
                    //    newData.Approved2Date = DateTime.Now;
                    //    break;
                    //case "A3":
                    //    newData.IsApproved3 = false;
                    //    newData.Approved3User = user.Username.Trim();
                    //    newData.Approved3Date = DateTime.Now;
                    //    break;
                    //    default:
                    //        break;
                    //}

                    _penghapusanAktivaRepository.Save(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Reject");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Save_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public ActionResult Approve(string id)
        {
            ViewData["ActionName"] = "Approve";
            PenghapusanAktivaHeader penghapusan = _penghapusanAktivaRepository.getPenghapusanAktiva(id);
            PenghapusanAktivaHeaderView data = ConvertFrom(penghapusan);
            data.Mode = "Approve";
            IList<PenghapusanAktivaDetail> PenghapusanDtl = _penghapusanAktivaRepository.getPenghapusanDetail(data.DeleteNo);
            IList<PenghapusanAktivaDetailView> PenghapusanDtlView = PenghapusanDtl.ToList().ConvertAll<PenghapusanAktivaDetailView>(new Converter<PenghapusanAktivaDetail, PenghapusanAktivaDetailView>(ConvertFrom));
            data.PenghapusanAktivaDetailList = PenghapusanDtlView.IsNullOrEmpty() ? CreateNewPenghapusanDetail() : PenghapusanDtlView;

            return CreateView(data);
        }

        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Approve(PenghapusanAktivaHeaderView data)
        {
            ViewData["ActionName"] = "Approve";
            User user = Lookup.Get<User>();
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            try
            {
                if (!data.AllowApprove)
                {
                    message = "Anda tidak punya akses untuk menyetujui dokumen ini!";
                }
                else
                {
                    PenghapusanAktivaHeader newData = _penghapusanAktivaRepository.getPenghapusanAktiva(data.DeleteNo);

                    newData.Status = PenjualanStatusEnum.A.ToString();

                    //switch (data.ApprovalStage.Trim())
                    //{
                    //    case "A1":
                    //        newData.IsApproved1 = true;
                    //        newData.Approved1User = user.Username.Trim();
                    //        newData.Approved1Date = DateTime.Now;
                    //        break;
                    //case "A2":
                    //    newData.IsApproved2 = true;
                    //    newData.Approved2User = user.Username.Trim();
                    //    newData.Approved2Date = DateTime.Now;
                    //    break;
                    //case "A3":
                    //    newData.IsApproved3 = true;
                    //    newData.Approved3User = user.Username.Trim();
                    //    newData.Approved3Date = DateTime.Now;
                    //    break;
                    //    default:
                    //        break;
                    //}

                    _penghapusanAktivaRepository.Save(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Approval");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Update_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult NoAuthCheckGetNilaiAsset(string id)
        //{
        //    PenghapusanAktivaHeaderView returnItem = new PenghapusanAktivaHeaderView();
        //    FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(id);

        //    returnItem.AktivaName = fixedAsset.IsNull() ? "" : fixedAsset.NamaAktiva;
        //    returnItem.SisaNilaiBukuKomersial = fixedAsset.IsNull() ? "" : fixedAsset.SisaNilaiBukuKomersial.ToString("N0");

        //    return Json(returnItem, JsonRequestBehavior.AllowGet);
        //}

        //private string validateData(PenghapusanAktivaHeaderView data, string mode)
        //{
        //    if (data.AktivaID == "")
        //        return PenghapusanAktivaResources.Validation_AktivaID;

        //    if (data.DeleteDate.IsNull())
        //        return PenghapusanAktivaResources.Validation_DeleteDate;

        //    if (data.DeleteReason.IsNullOrEmpty())
        //        return PenghapusanAktivaResources.Validation_DeleteReason;

        //    if (mode == "Create")
        //    {
        //        if (_penghapusanAktivaRepository.IsDuplicate(data.AktivaID))
        //            return PenghapusanAktivaResources.Validation_DuplicateData;
        //    }
        //    return "";
        //}

        private IList<SelectListItem> createPenghapusanStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(PenjualanStatusEnum)).Cast<PenjualanStatusEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }
    }
}