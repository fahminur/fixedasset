﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Controllers
{
    public class PenghapusanAktivaController : PageController
    {
        private IPenghapusanAktivaRepository _penghapusanAktivaRepository;
        private IAuditTrailLog _auditRepository;
        private IFixedAssetMasterRepository _fixedAssetMasterRepo;
        private IPenghapusanAktivaService _pengenghapusanAktivaService;
        public PenghapusanAktivaController(ISessionAuthentication sessionAuthentication, IPenghapusanAktivaRepository penghapusanAktivaRepository, IAuditTrailLog auditRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepo, IPenghapusanAktivaService pengenghapusanAktivaService) : base(sessionAuthentication)
        {
            this._penghapusanAktivaRepository = penghapusanAktivaRepository;
            this._auditRepository = auditRepository;
            this._fixedAssetMasterRepo = fixedAssetMasterRepo;
            this._pengenghapusanAktivaService = pengenghapusanAktivaService;

            Settings.ModuleName = "Penghapusan Aktiva";
            Settings.Title = PenghapusanAktivaResources.PageTitle;
        }

        protected override void Startup()
        {
            ViewData["PenghapusanStatus"] = createPenghapusanStatusSelect("N");
        }
        public ActionResult Search()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            JsonResult result = new JsonResult();
            try
            {
                PenghapusanAktivaFilter filter = new PenghapusanAktivaFilter();
                filter.InternalMemoNo = Request.QueryString["InternalMemoNo"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("InternalMemoNo")[0];
                filter.DeleteDateFrom = !Request.QueryString["DeleteDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("DeleteDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.DeleteDateTo = !Request.QueryString["DeleteDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("DeleteDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.Status = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];

                // Initialization.   
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<PenghapusanAktivaHeader> data;
                data = _penghapusanAktivaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<PenghapusanAktivaHeaderView>(new Converter<PenghapusanAktivaHeader, PenghapusanAktivaHeaderView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public PenghapusanAktivaHeaderView ConvertFrom(PenghapusanAktivaHeader item)
        {
            PenghapusanAktivaHeaderView returnItem = new PenghapusanAktivaHeaderView();
            FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(item.DeleteNo);

            returnItem.DeleteNo = item.DeleteNo;
            returnItem.DeleteDate = item.DeleteDate.ToString("dd/MM/yyyy");
            returnItem.InternalMemoNo = item.InternalMemoNo;
            returnItem.AllowEdit = item.AllowEdit;
            returnItem.Status = item.Status;
            returnItem.Notes = item.Notes;
            returnItem.StatusString = item.PenghapusanStsEnum.ToDescription();

            return returnItem;
        }

        public PenghapusanAktivaDetailView ConvertFrom(PenghapusanAktivaDetail item)
        {
            PenghapusanAktivaDetailView returnItem = new PenghapusanAktivaDetailView();
            FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(item.AktivaID);

            returnItem.DeleteNo = item.DeleteNo;
            returnItem.Seq = item.Seq;
            returnItem.AktivaID = item.AktivaID;
            returnItem.AktivaName = fixedAsset.IsNull() ? "" : fixedAsset.NamaAktiva.Trim();
            returnItem.DeleteReason = item.DeleteReason;
            returnItem.SisaNilaiBukuKomersial = fixedAsset.IsNull() ? "0" : fixedAsset.SisaNilaiBukuKomersial.ToString("N0");
            returnItem.Notes = item.Notes;

            return returnItem;
        }

        private IList<PenghapusanAktivaDetailView> CreateNewPenghapusanDetail()
        {
            IList<PenghapusanAktivaDetailView> returnDetail = new List<PenghapusanAktivaDetailView>();
            returnDetail.Add(AddNewAktivaRecord(returnDetail));
            return returnDetail;
        }
        private PenghapusanAktivaDetailView AddNewAktivaRecord(IList<PenghapusanAktivaDetailView> DetailList)
        {
            PenghapusanAktivaDetailView detail = new PenghapusanAktivaDetailView();
            detail.Seq = DetailList.IsNull() ? 1 : (DetailList.Count > 0) ? DetailList.LastOrDefault().Seq + 1 : 1;
            return detail;
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckAddPenghapusanDetail")]
        public ActionResult NoAuthCheckAddPenghapusanDetail(PenghapusanAktivaHeaderView paH)
        {
            ViewData["ActionName"] = paH.Mode;
            paH.PenghapusanAktivaDetailList.Add(AddNewAktivaRecord(paH.PenghapusanAktivaDetailList));
            return CreateView(paH);
        }

        [HttpPost]
        public ActionResult NoAuthCheckDeletePenghapusanDetail(PenghapusanAktivaHeaderView paH, int RecordID)
        {
            ViewData["ActionName"] = paH.Mode;
            paH.PenghapusanAktivaDetailList.Remove(paH.PenghapusanAktivaDetailList.FindElement(d => d.Seq == RecordID));
            if (paH.PenghapusanAktivaDetailList.Count == 0)
            {
                paH.PenghapusanAktivaDetailList = CreateNewPenghapusanDetail();
            }

            return CreateView(paH);
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            PenghapusanAktivaHeaderView data = new PenghapusanAktivaHeaderView();
            data.PenghapusanAktivaDetailList = CreateNewPenghapusanDetail();
            data.Mode = "Create";
            data.DeleteDate = DateTime.Now.ToString("dd/MM/yyyy");
            return CreateView(data);
        }

        private ViewResult CreateView(PenghapusanAktivaHeaderView data)
        {
            ViewData["ActionName"] = data.Mode;

            return View("Detail", data);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult Create(PenghapusanAktivaHeaderView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();

            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    PenghapusanAktivaHeader newData = new PenghapusanAktivaHeader("");

                    newData.DeleteDate = DateTime.ParseExact(data.DeleteDate, "d/M/yyyy", culture);
                    newData.InternalMemoNo = data.InternalMemoNo;
                    newData.Notes = data.Notes;
                    newData.Status = PenjualanStatusEnum.N.ToString();

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;

                    newData.PenghapusanAktivaDetailList = new List<PenghapusanAktivaDetail>();
                    int i = 1;
                    foreach (PenghapusanAktivaDetailView item in data.PenghapusanAktivaDetailList)
                    {
                        PenghapusanAktivaDetail newItem = new PenghapusanAktivaDetail(i);
                        newItem.Seq = i;
                        newItem.AktivaID = item.AktivaID;
                        newItem.DeleteReason = item.DeleteReason;
                        newItem.Notes = item.Notes;
                        newData.PenghapusanAktivaDetailList.Add(newItem);
                        i++;
                    }

                    _pengenghapusanAktivaService.SavePenghapusanAktiva(newData, user, company);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");

                    //FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(data.AktivaID);
                    //fixedAsset.Status = AssetStatusEnum.D.ToString();
                    //_fixedAssetMasterRepo.Save(fixedAsset);

                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, fixedAsset, user.Username, "Update Asset");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Save_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            PenghapusanAktivaHeader penghapusan = _penghapusanAktivaRepository.getPenghapusanAktiva(id);
            PenghapusanAktivaHeaderView data = ConvertFrom(penghapusan);
            data.Mode = "Edit";
            IList<PenghapusanAktivaDetail> PenghapusanDtl = _penghapusanAktivaRepository.getPenghapusanDetail(data.DeleteNo);
            IList<PenghapusanAktivaDetailView> PenghapusanDtlView = PenghapusanDtl.ToList().ConvertAll<PenghapusanAktivaDetailView>(new Converter<PenghapusanAktivaDetail, PenghapusanAktivaDetailView>(ConvertFrom));
            data.PenghapusanAktivaDetailList = PenghapusanDtlView.IsNullOrEmpty() ? CreateNewPenghapusanDetail() : PenghapusanDtlView;

            return CreateView(data);
        }

        // POST: Role/Edit/5
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Edit")]
        public ActionResult Edit(PenghapusanAktivaHeaderView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();

            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    PenghapusanAktivaHeader newData = new PenghapusanAktivaHeader("");

                    newData.DeleteNo = data.DeleteNo;
                    newData.DeleteDate = DateTime.ParseExact(data.DeleteDate, "d/M/yyyy", culture);
                    newData.InternalMemoNo = data.InternalMemoNo;
                    newData.Notes = data.Notes;
                    newData.Status = PenjualanStatusEnum.N.ToString();

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    newData.PenghapusanAktivaDetailList = new List<PenghapusanAktivaDetail>();
                    int i = 1;
                    foreach (PenghapusanAktivaDetailView item in data.PenghapusanAktivaDetailList)
                    {
                        PenghapusanAktivaDetail newItem = new PenghapusanAktivaDetail(i);
                        newItem.Seq = i;
                        newItem.AktivaID = item.AktivaID;
                        newItem.DeleteReason = item.DeleteReason;
                        newItem.Notes = item.Notes;
                        newData.PenghapusanAktivaDetailList.Add(newItem);
                        i++;
                    }

                    _pengenghapusanAktivaService.UpdatePenghapusanAktiva(newData, user, company);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Edit");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Update_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NoAuthCheckGetNilaiAsset(string id)
        {
            PenghapusanAktivaDetailView returnItem = new PenghapusanAktivaDetailView();
            FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(id);

            returnItem.AktivaName = fixedAsset.IsNull() ? "" : fixedAsset.NamaAktiva;
            returnItem.SisaNilaiBukuKomersial = fixedAsset.IsNull() ? "" : fixedAsset.SisaNilaiBukuKomersial.ToString("N0");

            return Json(returnItem, JsonRequestBehavior.AllowGet);
        }

        private string validateData(PenghapusanAktivaHeaderView data, string mode)
        {
            if (data.DeleteDate.IsNull())
                return PenghapusanAktivaResources.Validation_DeleteDate;

            if (data.InternalMemoNo == "")
                return "Nomor internal memo harus di isi!";

            if (mode == "Create")
            {
                if (_penghapusanAktivaRepository.IsDuplicate(data.DeleteNo))
                    return PenghapusanAktivaResources.Validation_DuplicateData;
            }
            return "";
        }
        private IList<SelectListItem> createPenghapusanStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(PenjualanStatusEnum)).Cast<PenjualanStatusEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }
    }
}