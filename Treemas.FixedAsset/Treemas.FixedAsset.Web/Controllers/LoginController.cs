﻿using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;

namespace Treemas.FixedAsset.Web.Controllers
{
    public class LoginController : LoginPageController
    {
        private IUserAccount _userAccount;
        private ISSOSessionStorage _ssoSessionStorage;
        public LoginController(IUserAccount userAccount, ISessionAuthentication sessionAuthentication,
            ISSOSessionStorage ssoSessionStorage) : base(userAccount, sessionAuthentication, ssoSessionStorage)
        {
            this._userAccount = userAccount;
            this._ssoSessionStorage = ssoSessionStorage;
        }

        protected override void Startup()
        {
            if (!SystemSettings.Instance.Security.EnableAuthentication)
            {
                User user = _userAccount.IsUserAuthentic(SystemSettings.Instance.Security.SimulatedAuthenticatedUser.Username, SystemSettings.Instance.Security.SimulatedAuthenticatedUser.Password);
                base.Lookup.Add(user);
            }
        }


    }
}