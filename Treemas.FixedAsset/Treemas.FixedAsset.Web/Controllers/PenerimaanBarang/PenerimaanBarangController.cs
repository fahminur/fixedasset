﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using Treemas.IFIN.Repository;
using System.Transactions;
using System.Globalization;

namespace Treemas.FixedAsset.Controllers
{
    public class PenerimaanBarangController : PageController
    {
        private IPenerimaanBarangRepository _penerimaanBarangRepository;
        private IGroupAssetRepository _groupAssetRepository;
        private ICurrencyRepository _currencyRepository;
        private IVendorAccountRepository _VendorAccountRepository;
        private IVendorRepository _VendorRepository;
        private ITaxRepository _TaxRepository;
        private IGRTypeRepository _grTypeRepository;
        private IGeneralSettingRepository _generalSettingRepository;
        private IGLPeriodRepository _glPeriodRepository;
        private IFixedAssetMasterRepository _fixedAssetMasterRepository;
        private IFixedAssetMasterSequence _fixedAssetMasterSequence;
        private IPenerimaanBarangService _penerimaanBarangService;
        private INonAssetMasterRepository _nonAssetMasterRepository;
        private string _invoiceTypeID;

        private IAuditTrailLog _auditRepository;

        public PenerimaanBarangController(IPenerimaanBarangRepository penerimaanBarangRepository, ISessionAuthentication sessionAuthentication, IGroupAssetRepository groupAssetRepository,
            ITaxRepository TaxRepository, IVendorAccountRepository VendorAccountRepository, ICurrencyRepository currencyRepository, IGRTypeRepository grTypeRepository,
            IGeneralSettingRepository generalSettingRepository, IGLPeriodRepository glPeriodRepository, IVendorRepository VendorRepository, IAuditTrailLog auditRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepository, IFixedAssetMasterSequence fixedAssetMasterSequence, IPenerimaanBarangService penerimaanBarangService,
            INonAssetMasterRepository nonAssetMasterRepository) : base(sessionAuthentication)
        {
            this._groupAssetRepository = groupAssetRepository;
            this._currencyRepository = currencyRepository;
            this._VendorAccountRepository = VendorAccountRepository;
            this._TaxRepository = TaxRepository;
            this._auditRepository = auditRepository;
            this._grTypeRepository = grTypeRepository;
            this._generalSettingRepository = generalSettingRepository;
            this._glPeriodRepository = glPeriodRepository;
            this._VendorRepository = VendorRepository;
            this._fixedAssetMasterRepository = fixedAssetMasterRepository;
            this._fixedAssetMasterSequence = fixedAssetMasterSequence;
            this._penerimaanBarangService = penerimaanBarangService;
            this._penerimaanBarangRepository = penerimaanBarangRepository;
            this._nonAssetMasterRepository = nonAssetMasterRepository;

            Settings.ModuleName = "Penerimaan Barang";
            Settings.Title = "Penerimaan Barang";
        }

        protected override void Startup()
        {
            ViewData["PenerimaanStatus"] = createPenerimaanStatusSelect(" ");
        }

        public ActionResult Search()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            JsonResult result = new JsonResult();
            try
            {
                PenerimaanBarangFilter filter = new PenerimaanBarangFilter();
                filter.GRDateFrom = !Request.QueryString["GRDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("GRDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.GRDateTo = !Request.QueryString["GRDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("GRDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.DeliveryOrderDateFrom = !Request.QueryString["DeliveryOrderDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("DeliveryOrderDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.DeliveryOrderDateTo = !Request.QueryString["DeliveryOrderDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("DeliveryOrderDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.VendorID = Request.QueryString.GetValues("VendorID")[0];
                filter.DeliveryOrderNo = Request.QueryString.GetValues("DeliveryOrderNo")[0];
                filter.Status = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<PenerimaanBarangHdr> data;
                data = _penerimaanBarangRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<PenerimaanBarangHdrView>(new Converter<PenerimaanBarangHdr, PenerimaanBarangHdrView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        private PenerimaanBarangHdrView ConvertFrom(PenerimaanBarangHdr input)
        {
            PenerimaanBarangHdrView output = new PenerimaanBarangHdrView();
            Vendor Vendor = _VendorRepository.getVendor(input.VendorID.Trim());

            output.GRNo = input.GRNo.Trim();
            output.VendorID = input.VendorID.Trim();
            output.VendorName = Vendor == null ? "" : Vendor.VendorName.Trim();
            output.DeliveryOrderNo = input.DeliveryOrderNo.Trim();
            output.Notes = input.Notes.IsNull() ? "" : input.Notes.Trim();
            output.GRType = input.GRType.Trim();
            output.Status = input.Status.Trim();
            output.StatusString = input.penerimaanStatusEnum.ToDescription();
            output.DeliveryOrderDate = input.DeliveryOrderDate.ToString("dd/MM/yyyy");
            output.GRDate = input.GRDate.ToString("dd/MM/yyyy");
            output.AllowEdit = input.AllowEdit;
            return output;
        }

        private IList<SelectListItem> createPenerimaanStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(PenerimaanStatusEnum)).Cast<PenerimaanStatusEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            PenerimaanBarangHdrView data = new PenerimaanBarangHdrView();

            data.Mode = "Create";
            data.PenerimaanAktivaList = CreateNewAktivaDetail();
            data.PenerimaanNonAktivaList = CreateNewNonAktivaDetail();
            //data.PenerimaanServiceList = CreateNewserviceDetail();
            data.DeliveryOrderDate = DateTime.Now.ToString("dd/MM/yyyy"); ;
            data.GRDate = DateTime.Now.ToString("dd/MM/yyyy"); ;

            return CreateView(data);
        }

        private IList<PenerimaanBarangAktivaView> CreateNewAktivaDetail()
        {
            IList<PenerimaanBarangAktivaView> returnDetail = new List<PenerimaanBarangAktivaView>();
            returnDetail.Add(AddNewAktivaRecord(returnDetail));
            return returnDetail;
        }

        private PenerimaanBarangAktivaView AddNewAktivaRecord(IList<PenerimaanBarangAktivaView> AktivaDetail)
        {
            PenerimaanBarangAktivaView detail = new PenerimaanBarangAktivaView();
            detail.Seq = AktivaDetail.IsNull() ? 1 : (AktivaDetail.Count > 0) ? AktivaDetail.LastOrDefault().Seq + 1 : 1;
            detail.Quantity = 1;
            return detail;
        }

        private IList<PenerimaanBarangNonAktivaView> CreateNewNonAktivaDetail()
        {
            IList<PenerimaanBarangNonAktivaView> returnDetail = new List<PenerimaanBarangNonAktivaView>();
            returnDetail.Add(AddNewNonAktivaRecord(returnDetail));
            return returnDetail;
        }

        private PenerimaanBarangNonAktivaView AddNewNonAktivaRecord(IList<PenerimaanBarangNonAktivaView> NonAktivaDetail)
        {
            PenerimaanBarangNonAktivaView detail = new PenerimaanBarangNonAktivaView();
            detail.Seq = NonAktivaDetail.IsNull() ? 1 : (NonAktivaDetail.Count > 0) ? NonAktivaDetail.LastOrDefault().Seq + 1 : 1;
            detail.Quantity = 1;
            return detail;
        }

        private IList<PenerimaanBarangServiceView> CreateNewserviceDetail()
        {
            IList<PenerimaanBarangServiceView> returnDetail = new List<PenerimaanBarangServiceView>();
            returnDetail.Add(AddNewServiceRecord(returnDetail));
            return returnDetail;
        }

        private PenerimaanBarangServiceView AddNewServiceRecord(IList<PenerimaanBarangServiceView> ServiceDetail)
        {
            PenerimaanBarangServiceView detail = new PenerimaanBarangServiceView();
            detail.Seq = ServiceDetail.IsNull() ? 1 : (ServiceDetail.Count > 0) ? ServiceDetail.LastOrDefault().Seq + 1 : 1;
            return detail;
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckAddAccountAktiva")]
        public ActionResult NoAuthCheckAddAccountAktiva(PenerimaanBarangHdrView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.PenerimaanAktivaList.Add(AddNewAktivaRecord(invH.PenerimaanAktivaList));
            //PenerimaanBarangHdrView InvH = CountAmount(invH);

            return CreateView(invH);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckAddAccountNonAktiva")]
        public ActionResult NoAuthCheckAddAccountNonAktiva(PenerimaanBarangHdrView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.PenerimaanNonAktivaList.Add(AddNewNonAktivaRecord(invH.PenerimaanNonAktivaList));
            //PenerimaanBarangHdrView InvH = CountAmount(invH);

            return CreateView(invH);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckAddAccountService")]
        public ActionResult NoAuthCheckAddAccountService(PenerimaanBarangHdrView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.PenerimaanServiceList.Add(AddNewServiceRecord(invH.PenerimaanServiceList));
            //PenerimaanBarangHdrView InvH = CountAmount(invH);
            return CreateView(invH);
        }

        [HttpPost]
        public ActionResult NoAuthCheckDeleteAktivaRow(PenerimaanBarangHdrView invH, int RecordID)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.PenerimaanAktivaList.Remove(invH.PenerimaanAktivaList.FindElement(d => d.Seq == RecordID));
            if (invH.PenerimaanAktivaList.Count == 0)
            {
                invH.PenerimaanAktivaList = CreateNewAktivaDetail();
            }

            PenerimaanBarangHdrView InvH = CountAmount(invH);
            return CreateView(InvH);
        }
        [HttpPost]
        public ActionResult NoAuthCheckDeleteNonAktivaRow(PenerimaanBarangHdrView invH, int RecordID)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.PenerimaanNonAktivaList.Remove(invH.PenerimaanNonAktivaList.FindElement(d => d.Seq == RecordID));
            if (invH.PenerimaanNonAktivaList.Count == 0)
            {
                invH.PenerimaanNonAktivaList = CreateNewNonAktivaDetail();
            }

            PenerimaanBarangHdrView InvH = CountAmount(invH);
            return CreateView(InvH);
        }
        [HttpPost]
        public ActionResult NoAuthCheckDeleteServiceRow(PenerimaanBarangHdrView invH, int RecordID)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.PenerimaanServiceList.Remove(invH.PenerimaanServiceList.FindElement(d => d.Seq == RecordID));
            if (invH.PenerimaanServiceList.Count == 0)
            {
                invH.PenerimaanServiceList = CreateNewserviceDetail();
            }

            PenerimaanBarangHdrView InvH = CountAmount(invH);
            return CreateView(InvH);
        }


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckCalculateActivaAmount")]
        public ActionResult NoAuthCheckCalculateActivaAmount(PenerimaanBarangHdrView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            GeneralSetting GS = _generalSettingRepository.GetGeneralSetting("FANONACTIVA", "FA");
            string message = "";

            try
            {
                for (int i = 0; i <= invH.PenerimaanAktivaList.Count - 1; i++)
                {
                    double CurValue = Convert.ToDouble(invH.PenerimaanAktivaList[i].Price);
                    double CurLimit = Convert.ToDouble(GS.GSValue);

                    if (CurValue <= CurLimit)
                    {
                        message = "Minimum nilai Aktiva adalah " + CurLimit.ToString("N0") + "!";
                        invH.PenerimaanAktivaList[i].Price = "0";
                    }
                }
                CountAmount(invH);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (!message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                CollectScreenMessages();
                return CreateView(invH);
            }
            else
            {
                return CreateView(invH);
            }
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckValidateNonActivaAmount")]
        public ActionResult NoAuthCheckValidateNonActivaAmount(PenerimaanBarangHdrView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            string message = "";
            GeneralSetting GS = _generalSettingRepository.GetGeneralSetting("FANONACTIVA", "FA");

            try
            {
                for (int i = 0; i <= invH.PenerimaanNonAktivaList.Count - 1; i++)
                {
                    double CurValue = Convert.ToDouble(invH.PenerimaanNonAktivaList[i].Price);
                    double CurLimit = Convert.ToDouble(GS.GSValue);

                    if (CurValue > CurLimit)
                    {
                        message = "Maksimum nilai Non-Aktiva adalah " + CurLimit.ToString("N0") + "!";
                        invH.PenerimaanNonAktivaList[i].Price = "0";
                    }
                }
                //CountNonActivaAmortize(invH);
                CountAmount(invH);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (!message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                CollectScreenMessages();
                return CreateView(invH);
            }
            else
            {
                return CreateView(invH);
            }
        }


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckCalculateNonActivaAmount")]
        public ActionResult NoAuthCheckCalculateNonActivaAmount(PenerimaanBarangHdrView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            PenerimaanBarangHdrView InvH = CountAmount(invH);
            //PenerimaanBarangHdrView invh = CountNonActivaAmortize(InvH);
            return CreateView(InvH);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckCalculateServiceAmount")]
        public ActionResult NoAuthCheckCalculateServiceAmount(PenerimaanBarangHdrView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            PenerimaanBarangHdrView InvH = CountAmount(invH);
            return CreateView(InvH);
        }


        private PenerimaanBarangHdrView CountAmount(PenerimaanBarangHdrView invH)
        {
            try
            {
                double GRAmount = 0;
                foreach (var item in invH.PenerimaanAktivaList)
                {
                    item.TotalPrice = (Convert.ToDouble(item.Price) * Convert.ToDouble(item.Quantity)).ToString("N0");
                    item.Price = Convert.ToDouble(item.Price).ToString("N0");
                    GRAmount += Convert.ToDouble(item.TotalPrice);
                    invH.GRAmount = GRAmount.ToString("N0");
                }

                foreach (var item in invH.PenerimaanNonAktivaList)
                {
                    item.TotalPrice = (Convert.ToDouble(item.Quantity) * Convert.ToDouble(item.Price)).ToString("N0");
                    item.Price = Convert.ToDouble(item.Price).ToString("N0");
                    GRAmount += Convert.ToDouble(item.TotalPrice);
                    invH.GRAmount = GRAmount.ToString("N0");
                }

                foreach (var item in invH.PenerimaanServiceList)
                {
                    item.Amount = Convert.ToDouble(item.Amount).ToString("N0");
                    GRAmount += Convert.ToDouble(item.Amount);
                    invH.GRAmount = GRAmount.ToString("N0");
                }
            }
            catch (Exception e)
            {
                ScreenMessages.Submit(ScreenMessage.Error(e.Message));
                CollectScreenMessages();
            }
            return invH;
        }


        private ViewResult CreateView(PenerimaanBarangHdrView data)
        {
            ViewData["PenerimaanType"] = PenerimaanTypeSelect(data.GRType == null ? " " : data.GRType);

            generateViewDataGroupAssetSelect(data.PenerimaanNonAktivaList);

            return View("Detail", data);
        }

        private IList<SelectListItem> PenerimaanTypeSelect(string selected)
        {
            List<GRType> itemList = Lookup.Get<List<GRType>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _grTypeRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GRType, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _invoiceTypeID = itemList.FirstOrDefault().GRTypeID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString())).Selected = true;
                _invoiceTypeID = selected;
            }

            return dataList;
        }

        public SelectListItem ConvertForm(GRType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GRTypeName.Trim();
            returnItem.Value = item.GRTypeID.Trim();
            return returnItem;
        }

        private void generateViewDataGroupAssetSelect(IList<PenerimaanBarangNonAktivaView> NonActivaList)
        {
            int index = 0;
            if (!NonActivaList.IsNullOrEmpty())
            {
                foreach (PenerimaanBarangNonAktivaView item in NonActivaList)
                {
                    ViewData["PenerimaanNonAktivaList[" + (index).ToString() + "].GroupAssetIDSelect"] = createGroupAssetSelect(item.GroupAssetID);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }

        private IList<SelectListItem> createGroupAssetSelect(string selected)
        {
            List<GroupAsset> itemList = Lookup.Get<List<GroupAsset>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _groupAssetRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(GroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GroupAssetDescription;
            returnItem.Value = item.GroupAssetID.Trim();
            return returnItem;
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult Create(PenerimaanBarangHdrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            string message = "";
            bool isActiva = false;
            bool isNonActiva = false;
            //bool isService = false;
            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");


            switch (data.GRType)
            {
                case "AC":
                    isActiva = true;
                    break;
                //case "ANA":
                //    isNonActiva = true;
                //    isActiva = true;
                //    break;
                //case "APA":
                //    isActiva = true;
                //    isService = true;
                //    break;
                case "NA":
                    isNonActiva = true;
                    break;
                //case "NPA":
                //    isNonActiva = true;
                //    isService = true;
                //    break;
                //case "PA":
                //    isService = true;
                //    break;
                case "ST":
                    isActiva = true;
                    isNonActiva = true;
                    //isService = true;
                    break;
                default:
                    break;
            }


            try
            {
                message = validateData(data, "Create");

                if (message.IsNullOrEmpty())
                {
                    PenerimaanBarangHdr newData = new PenerimaanBarangHdr("");
                    List<FixedAssetMaster> famDatas = new List<FixedAssetMaster>();

                    newData.GRNo = "";
                    newData.DeliveryOrderNo = data.DeliveryOrderNo;
                    newData.VendorID = data.VendorID;
                    newData.GRDate = DateTime.ParseExact(data.GRDate, "d/M/yyyy", culture);
                    newData.DeliveryOrderDate = DateTime.ParseExact(data.DeliveryOrderDate, "d/M/yyyy", culture);
                    newData.Notes = data.Notes;
                    newData.GRType = data.GRType;
                    newData.GRAmount = Convert.ToDouble(data.GRAmount);
                    newData.Status = PenerimaanStatusEnum.N.ToString();

                    //aktiva
                    if (isActiva)
                    {
                        newData.PenerimaanAktivaList = new List<PenerimaanBarangAktiva>();
                        foreach (var item in data.PenerimaanAktivaList)
                        {
                            PenerimaanBarangAktiva newItem = new PenerimaanBarangAktiva(item.Seq);
                            newItem.GRNo = data.GRNo;
                            newItem.Seq = item.Seq;
                            newItem.AktivaId = item.AktivaId;
                            newItem.Quantity = 1;
                            newItem.Price = Convert.ToDouble(item.Price);
                            newItem.TotalPrice = Convert.ToDouble(item.TotalPrice);
                            newItem.Notes = item.Notes;
                            newData.GRAmount += Convert.ToDouble(item.TotalPrice);
                            newData.PenerimaanAktivaList.Add(newItem);
                        }
                    }
                    if (isNonActiva)
                    {
                        //nonAktiva
                        newData.PenerimaanNonAktivaList = new List<PenerimaanBarangNonAktiva>();
                        foreach (var item in data.PenerimaanNonAktivaList)
                        {
                            PenerimaanBarangNonAktiva newItem = new PenerimaanBarangNonAktiva(item.Seq);
                            //string ReffNo = _fixedAssetMasterSequence.createNonAktivaNo("NAC", year, month, user, company);

                            newItem.GRNo = data.GRNo;
                            newItem.Seq = item.Seq;
                            newItem.NonAktivaId = item.NonAktivaId.Trim();
                            newItem.RefferenceNo = null;
                            newItem.GroupAssetID = item.GroupAssetID;
                            newItem.ItemName = item.ItemName;
                            newItem.Price = Convert.ToDouble(item.Price);
                            newItem.Quantity = Convert.ToInt32(item.Quantity);
                            newItem.TotalPrice = Convert.ToDouble(item.TotalPrice);
                            newItem.Notes = item.Notes;
                            newData.GRAmount += Convert.ToDouble(item.TotalPrice);
                            newData.PenerimaanNonAktivaList.Add(newItem);
                        }
                    }
                    //if (isService)
                    //{
                    //    //Service
                    //    newData.PenerimaanServiceList = new List<PenerimaanBarangService>();
                    //    foreach (var item in data.PenerimaanServiceList)
                    //    {
                    //        PenerimaanBarangService newItem = new PenerimaanBarangService(item.Seq);
                    //        newItem.GRNo = data.GRNo;
                    //        newItem.Seq = item.Seq;
                    //        newItem.AktivaId = item.AktivaId;
                    //        newItem.Amount = Convert.ToDouble(item.Amount);
                    //        newItem.Notes = item.Notes;
                    //        newData.GRAmount += Convert.ToDouble(item.Amount);
                    //        newData.PenerimaanServiceList.Add(newItem);
                    //    }
                    //}
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;

                    message = _penerimaanBarangService.SavePenerimaanBarang(newData, user, company);
                    if (message == "")
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");


                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Save_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            PenerimaanBarangHdr penerimanItem = _penerimaanBarangRepository.getPenerimaanBarang(id);
            PenerimaanBarangHdrView data = ConvertFrom(penerimanItem);
            data.Mode = "Edit";

            IList<PenerimaanBarangAktiva> GRA = _penerimaanBarangRepository.getPenerimaanBarangAktiva(data.GRNo);
            IList<PenerimaanBarangAktivaView> GRAView = GRA.ToList().ConvertAll<PenerimaanBarangAktivaView>(new Converter<PenerimaanBarangAktiva, PenerimaanBarangAktivaView>(ConvertFrom));
            data.PenerimaanAktivaList = GRAView.IsNullOrEmpty() ? CreateNewAktivaDetail() : GRAView;
            IList<PenerimaanBarangNonAktiva> GRNA = _penerimaanBarangRepository.getPenerimaanBarangNonAktiva(data.GRNo);
            IList<PenerimaanBarangNonAktivaView> GRNAView = GRNA.ToList().ConvertAll<PenerimaanBarangNonAktivaView>(new Converter<PenerimaanBarangNonAktiva, PenerimaanBarangNonAktivaView>(ConvertFrom));
            data.PenerimaanNonAktivaList = GRNAView.IsNullOrEmpty() ? CreateNewNonAktivaDetail() : GRNAView;
            //IList<PenerimaanBarangService> GRS = _penerimaanBarangRepository.getPenerimaanBarangService(data.GRNo);
            //IList<PenerimaanBarangServiceView> GRSView = GRS.ToList().ConvertAll<PenerimaanBarangServiceView>(new Converter<PenerimaanBarangService, PenerimaanBarangServiceView>(ConvertFrom));
            //data.PenerimaanServiceList = GRSView.IsNullOrEmpty() ? CreateNewserviceDetail() : GRSView;
            return CreateView(data);
        }

        public PenerimaanBarangAktivaView ConvertFrom(PenerimaanBarangAktiva item)
        {
            PenerimaanBarangAktivaView returnItem = new PenerimaanBarangAktivaView();
            FixedAssetMaster FA = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);

            returnItem.GRNo = item.GRNo;
            returnItem.Seq = item.Seq;
            returnItem.AktivaId = item.AktivaId;
            returnItem.AktivaName = FA.IsNull() ? "" : FA.NamaAktiva.Trim();
            returnItem.Quantity = item.Quantity;
            returnItem.Price = item.Price.ToString("N0");
            returnItem.TotalPrice = item.TotalPrice.ToString("N0");
            returnItem.Notes = item.Notes;

            return returnItem;
        }

        public PenerimaanBarangNonAktivaView ConvertFrom(PenerimaanBarangNonAktiva item)
        {
            PenerimaanBarangNonAktivaView returnItem = new PenerimaanBarangNonAktivaView();
            NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);
            returnItem.GRNo = item.GRNo;
            returnItem.Seq = item.Seq;
            returnItem.NonAktivaId = item.NonAktivaId;
            returnItem.NonAktivaName = nam.IsNull() ? "" : nam.NamaNonAktiva.Trim();
            returnItem.RefferenceNo = item.RefferenceNo;
            returnItem.GroupAssetID = item.GroupAssetID;
            returnItem.ItemName = item.ItemName;
            returnItem.Price = item.Price.ToString("N0");
            returnItem.Quantity = item.Quantity;
            returnItem.TotalPrice = item.TotalPrice.ToString("N0");
            returnItem.Notes = item.Notes;
            return returnItem;
        }

        public PenerimaanBarangServiceView ConvertFrom(PenerimaanBarangService item)
        {
            PenerimaanBarangServiceView returnItem = new PenerimaanBarangServiceView();
            FixedAssetMaster FA = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);

            returnItem.GRNo = item.GRNo;
            returnItem.Seq = item.Seq;
            returnItem.AktivaId = item.AktivaId;
            returnItem.AktivaName = FA.IsNull() ? "" : FA.NamaAktiva.Trim();
            returnItem.Notes = item.Notes;
            returnItem.Amount = item.Amount.ToString("N0");
            returnItem.Notes = item.Notes;

            return returnItem;
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Edit")]
        public ActionResult Edit(PenerimaanBarangHdrView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();

            string message = "";
            bool isActiva = false;
            bool isNonActiva = false;
            //bool isService = false;

            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            switch (data.GRType)
            {
                case "AC":
                    isActiva = true;
                    break;
                //case "ANA":
                //    isNonActiva = true;
                //    isActiva = true;
                //    break;
                //case "APA":
                //    isActiva = true;
                //    isService = true;
                //    break;
                case "NA":
                    isNonActiva = true;
                    break;
                //case "NPA":
                //    isNonActiva = true;
                //    isService = true;
                //    break;
                //case "PA":
                //    isService = true;
                //    break;
                case "ST":
                    isActiva = true;
                    isNonActiva = true;
                    //isService = true;
                    break;
                default:
                    break;
            }

            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    PenerimaanBarangHdr newData = _penerimaanBarangRepository.getPenerimaanBarang(data.GRNo);

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    if (message.IsNullOrEmpty())
                    {
                        newData.DeliveryOrderNo = data.DeliveryOrderNo;
                        newData.VendorID = data.VendorID;
                        newData.GRDate = DateTime.ParseExact(data.GRDate, "d/M/yyyy", culture);
                        newData.DeliveryOrderDate = DateTime.ParseExact(data.DeliveryOrderDate, "d/M/yyyy", culture);
                        newData.Notes = data.Notes;
                        newData.GRType = data.GRType;
                        newData.GRAmount = Convert.ToDouble(data.GRAmount);
                        newData.Status = PenerimaanStatusEnum.N.ToString();

                        //aktiva
                        if (isActiva)
                        {
                            newData.PenerimaanAktivaList = new List<PenerimaanBarangAktiva>();
                            foreach (var item in data.PenerimaanAktivaList)
                            {
                                PenerimaanBarangAktiva newItem = new PenerimaanBarangAktiva(item.Seq);
                                newItem.GRNo = data.GRNo;
                                newItem.Seq = item.Seq;
                                newItem.AktivaId = item.AktivaId;
                                newItem.Quantity = 1;
                                newItem.Price = Convert.ToDouble(item.Price);
                                newItem.TotalPrice = Convert.ToDouble(item.TotalPrice);
                                newItem.Notes = item.Notes;
                                newData.GRAmount += Convert.ToDouble(item.TotalPrice);
                                newData.PenerimaanAktivaList.Add(newItem);
                            }
                        }
                        if (isNonActiva)
                        {
                            //nonAktiva
                            newData.PenerimaanNonAktivaList = new List<PenerimaanBarangNonAktiva>();
                            foreach (var item in data.PenerimaanNonAktivaList)
                            {
                                PenerimaanBarangNonAktiva newItem = new PenerimaanBarangNonAktiva(item.Seq);

                                newItem.GRNo = data.GRNo;
                                newItem.Seq = item.Seq;
                                newItem.NonAktivaId = item.NonAktivaId.Trim();
                                newItem.RefferenceNo = item.RefferenceNo;
                                newItem.GroupAssetID = item.GroupAssetID;
                                newItem.ItemName = item.ItemName;
                                newItem.Price = Convert.ToDouble(item.Price);
                                newItem.Quantity = Convert.ToInt32(item.Quantity);
                                newItem.TotalPrice = Convert.ToDouble(item.TotalPrice);
                                newItem.Notes = item.Notes;
                                newData.GRAmount += Convert.ToDouble(item.TotalPrice);
                                newData.PenerimaanNonAktivaList.Add(newItem);
                            }
                        }
                        //if (isService)
                        //{
                        //    //Service
                        //    newData.PenerimaanServiceList = new List<PenerimaanBarangService>();
                        //    foreach (var item in data.PenerimaanServiceList)
                        //    {
                        //        PenerimaanBarangService newItem = new PenerimaanBarangService(item.Seq);
                        //        newItem.GRNo = data.GRNo;
                        //        newItem.Seq = item.Seq;
                        //        newItem.AktivaId = item.AktivaId;
                        //        newItem.Amount = Convert.ToDouble(item.Amount);
                        //        newItem.Notes = item.Notes;
                        //        newData.GRAmount += Convert.ToDouble(item.Amount);
                        //        newData.PenerimaanServiceList.Add(newItem);
                        //    }
                        //}
                        newData.UpdateUser = user.Username;
                        newData.UpdateDate = DateTime.Now;

                        message = _penerimaanBarangService.UpdatePenerimaanBarang(newData, user, company);
                        
                        if (message == "")
                            _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Update_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(PenerimaanBarangHdrView data, string mode)
        {
            bool isActiva = false;
            bool isNonActiva = false;
            //bool isService = false;

            switch (data.GRType)
            {
                case "AC":
                    isActiva = true;
                    break;
                //case "ANA":
                //    isNonActiva = true;
                //    isActiva = true;
                //    break;
                //case "APA":
                //    isActiva = true;
                //    isService = true;
                //    break;
                case "NA":
                    isNonActiva = true;
                    break;
                //case "NPA":
                //    isNonActiva = true;
                //    isService = true;
                //    break;
                //case "PA":
                //    isService = true;
                //    break;
                case "ST":
                    isActiva = true;
                    isNonActiva = true;
                    //isService = true;
                    break;
                default:
                    break;
            }


            if (data.VendorName == "" || data.VendorName.IsNull())
                return "Vendor harus di pilih!";

            if (data.DeliveryOrderNo == "" || data.DeliveryOrderNo.IsNull())
                return "Nomor surat jalan/DO harus di isi!";

            if (data.GRDate.IsNull())
                return "Tanggal penerimaan barang tidak valid!";

            if (data.DeliveryOrderDate.IsNull())
                return "Tanggal surat jalan/DO tidak valid!";

            if (data.GRType == " " || data.GRType.IsNull())
                return "Jenis penrimaaan barang harus dipilih!";

            if (isActiva)
            {
                foreach (PenerimaanBarangAktivaView item in data.PenerimaanAktivaList)
                {
                    if (item.AktivaId == "" || item.AktivaId.IsNull())
                        return "Kode aktiva harus di isi!";

                    //if (item.Price.IsNull() || Convert.ToDouble(item.Price) == 0)
                    //    return "Harga Aktiva harus di isi!";

                }
            }

            if (isNonActiva)
            {
                foreach (PenerimaanBarangNonAktivaView item in data.PenerimaanNonAktivaList)
                {
                    if (item.NonAktivaId == "" || item.NonAktivaId.IsNull())
                        return "Kode Non Aktiva harus di isi!";

                    //if (item.GroupAssetID == "" || item.GroupAssetID.IsNull())
                    //    return "Group Asset non aktiva harus di pilih!";
                    //if (item.ItemName == "" || item.ItemName.IsNull())
                    //    return "Nama Barang/Jasa non aktiva harus di isi!";
                    //if (item.Price.IsNull() || Convert.ToDouble(item.Price) == 0)
                    //    return "Harga non aktiva harus di isi!";
                }
            }

            //if (isService)
            //{
            //    foreach (PenerimaanBarangServiceView item in data.PenerimaanServiceList)
            //    {
            //        if (item.AktivaId == "" || item.AktivaId.IsNull())
            //            return "Kode Aktiva perawatan aktiva harus di isi!";
            //        if (item.Amount == "0" || item.Amount.IsNull())
            //            return "Biaya perawatan aktiva harus di isi!";
            //    }
            //}

            if (mode == "Create")
            {
                if (_penerimaanBarangRepository.IsDuplicate(data.GRNo))
                    return "Nomor Penerimaan Barang sudah ada!";
            }
            return "";
        }
    }
}