﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using Treemas.IFIN.Repository;
using System.Transactions;
using System.Globalization;

namespace Treemas.FixedAsset.Web.Controllers.PenerimaanBarang
{
    public class ApprovalPenerimaanBarangController : PageController
    {
        private IPenerimaanBarangRepository _penerimaanBarangRepository;
        private IGroupAssetRepository _groupAssetRepository;
        private ICurrencyRepository _currencyRepository;
        private IVendorAccountRepository _VendorAccountRepository;
        private IVendorRepository _VendorRepository;
        private ITaxRepository _TaxRepository;
        private IGRTypeRepository _grTypeRepository;
        private IGeneralSettingRepository _generalSettingRepository;
        private IGLPeriodRepository _glPeriodRepository;
        private IFixedAssetMasterRepository _fixedAssetMasterRepository;
        private IFixedAssetMasterSequence _fixedAssetMasterSequence;
        private IPenerimaanBarangService _penerimaanBarangService;
        private string _invoiceTypeID;
        private IAuditTrailLog _auditRepository;
        private IGLJournalRepository _glJournalRepository;
        private IGLJournalService _glJournalService;
        private INonAssetMasterRepository _nonAssetMasterRepository;
        private IInventoryRepository _inventoryRepository;

        public ApprovalPenerimaanBarangController(IPenerimaanBarangRepository penerimaanBarangRepository, ISessionAuthentication sessionAuthentication, IGroupAssetRepository groupAssetRepository, IAssetUserRepository assetUserRepository,
            ITaxRepository TaxRepository, IVendorAccountRepository VendorAccountRepository, ICurrencyRepository currencyRepository, IGRTypeRepository grTypeRepository,
            IGeneralSettingRepository generalSettingRepository, IGLPeriodRepository glPeriodRepository, IVendorRepository VendorRepository, IAuditTrailLog auditRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepository, IFixedAssetMasterSequence fixedAssetMasterSequence, IPenerimaanBarangService penerimaanBarangService,
            IGLJournalRepository glJournalRepository, IGLJournalService glJournalService, INonAssetMasterRepository nonAssetMasterRepository, IInventoryRepository inventoryRepository) : base(sessionAuthentication)
        {
            this._groupAssetRepository = groupAssetRepository;
            this._currencyRepository = currencyRepository;
            this._VendorAccountRepository = VendorAccountRepository;
            this._TaxRepository = TaxRepository;
            this._auditRepository = auditRepository;
            this._grTypeRepository = grTypeRepository;
            this._generalSettingRepository = generalSettingRepository;
            this._glPeriodRepository = glPeriodRepository;
            this._VendorRepository = VendorRepository;
            this._fixedAssetMasterRepository = fixedAssetMasterRepository;
            this._fixedAssetMasterSequence = fixedAssetMasterSequence;
            this._penerimaanBarangService = penerimaanBarangService;
            this._penerimaanBarangRepository = penerimaanBarangRepository;
            this._glJournalRepository = glJournalRepository;
            this._glJournalService = glJournalService;
            this._nonAssetMasterRepository = nonAssetMasterRepository;
            this._inventoryRepository = inventoryRepository;

            Settings.ModuleName = "Approval Penerimaan Barang";
            Settings.Title = "Approval Penerimaan Barang";
        }

        protected override void Startup()
        {
            ViewData["PenerimaanStatus"] = createPenerimaanStatusSelect(" ");
        }

        public ActionResult Search()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            JsonResult result = new JsonResult();
            try
            {
                PenerimaanBarangFilter filter = new PenerimaanBarangFilter();
                filter.GRDateFrom = !Request.QueryString["GRDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("GRDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.GRDateTo = !Request.QueryString["GRDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("GRDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.DeliveryOrderDateFrom = !Request.QueryString["DeliveryOrderDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("DeliveryOrderDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.DeliveryOrderDateTo = !Request.QueryString["DeliveryOrderDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("DeliveryOrderDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.VendorID = Request.QueryString.GetValues("VendorID")[0];
                filter.DeliveryOrderNo = Request.QueryString.GetValues("DeliveryOrderNo")[0];
                filter.Status = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<PenerimaanBarangHdr> data;
                data = _penerimaanBarangRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<PenerimaanBarangHdrView>(new Converter<PenerimaanBarangHdr, PenerimaanBarangHdrView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        private PenerimaanBarangHdrView ConvertFrom(PenerimaanBarangHdr input)
        {
            PenerimaanBarangHdrView output = new PenerimaanBarangHdrView();
            Vendor Vendor = _VendorRepository.getVendor(input.VendorID.Trim());

            output.GRNo = input.GRNo.Trim();
            output.VendorID = input.VendorID.Trim();
            output.VendorName = Vendor == null ? "" : Vendor.VendorName.Trim();
            output.DeliveryOrderNo = input.DeliveryOrderNo.Trim();
            output.Notes = input.Notes.IsNull() ? "" : input.Notes.Trim();
            output.GRType = input.GRType.Trim();
            output.Status = input.Status.Trim();
            output.StatusString = input.penerimaanStatusEnum.ToDescription();
            output.DeliveryOrderDate = input.DeliveryOrderDate.ToString("dd/MM/yyyy");
            output.GRDate = input.GRDate.ToString("dd/MM/yyyy");
            output.AllowApprove = input.AllowApprove;
            output.AllowReject = input.AllowReject;
            return output;
        }

        private IList<SelectListItem> createPenerimaanStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(PenerimaanStatusEnum)).Cast<PenerimaanStatusEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }

        public ActionResult Approve(string id)
        {
            ViewData["ActionName"] = "Approve";
            PenerimaanBarangHdr penerimanItem = _penerimaanBarangRepository.getPenerimaanBarang(id);
            PenerimaanBarangHdrView data = ConvertFrom(penerimanItem);
            data.Mode = "Approve";

            IList<PenerimaanBarangAktiva> GRA = _penerimaanBarangRepository.getPenerimaanBarangAktiva(data.GRNo);
            IList<PenerimaanBarangAktivaView> GRAView = GRA.ToList().ConvertAll<PenerimaanBarangAktivaView>(new Converter<PenerimaanBarangAktiva, PenerimaanBarangAktivaView>(ConvertFrom));
            data.PenerimaanAktivaList = GRAView;//.IsNullOrEmpty() ? CreateNewAktivaDetail() : GRAView;
            IList<PenerimaanBarangNonAktiva> GRNA = _penerimaanBarangRepository.getPenerimaanBarangNonAktiva(data.GRNo);
            IList<PenerimaanBarangNonAktivaView> GRNAView = GRNA.ToList().ConvertAll<PenerimaanBarangNonAktivaView>(new Converter<PenerimaanBarangNonAktiva, PenerimaanBarangNonAktivaView>(ConvertFrom));
            data.PenerimaanNonAktivaList = GRNAView;//.IsNullOrEmpty() ? CreateNewNonAktivaDetail() : GRNAView;
            //IList<PenerimaanBarangService> GRS = _penerimaanBarangRepository.getPenerimaanBarangService(data.GRNo);
            //IList<PenerimaanBarangServiceView> GRSView = GRS.ToList().ConvertAll<PenerimaanBarangServiceView>(new Converter<PenerimaanBarangService, PenerimaanBarangServiceView>(ConvertFrom));
            //data.PenerimaanServiceList = GRSView;//.IsNullOrEmpty() ? CreateNewserviceDetail() : GRSView;
            return CreateView(data);
        }

        public PenerimaanBarangAktivaView ConvertFrom(PenerimaanBarangAktiva item)
        {
            PenerimaanBarangAktivaView returnItem = new PenerimaanBarangAktivaView();
            FixedAssetMaster FA = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);

            returnItem.GRNo = item.GRNo;
            returnItem.Seq = item.Seq;
            returnItem.AktivaId = item.AktivaId;
            returnItem.AktivaName = FA.IsNull() ? "" : FA.NamaAktiva.Trim();
            returnItem.Quantity = item.Quantity;
            returnItem.Price = item.Price.ToString("N0");
            returnItem.TotalPrice = item.TotalPrice.ToString("N0");
            returnItem.Notes = item.Notes;

            return returnItem;
        }

        public PenerimaanBarangNonAktivaView ConvertFrom(PenerimaanBarangNonAktiva item)
        {
            PenerimaanBarangNonAktivaView returnItem = new PenerimaanBarangNonAktivaView();
            NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);

            returnItem.GRNo = item.GRNo;
            returnItem.Seq = item.Seq;
            returnItem.NonAktivaId = item.NonAktivaId;
            returnItem.NonAktivaName = nam.IsNull() ? "" : nam.NamaNonAktiva.Trim();
            returnItem.RefferenceNo = item.RefferenceNo;
            returnItem.GroupAssetID = item.GroupAssetID;
            returnItem.ItemName = item.ItemName;
            returnItem.Price = item.Price.ToString("N0");
            returnItem.Quantity = item.Quantity;
            returnItem.TotalPrice = item.TotalPrice.ToString("N0");
            returnItem.Notes = item.Notes;
            return returnItem;
        }

        public PenerimaanBarangServiceView ConvertFrom(PenerimaanBarangService item)
        {
            PenerimaanBarangServiceView returnItem = new PenerimaanBarangServiceView();
            FixedAssetMaster FA = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);

            returnItem.GRNo = item.GRNo;
            returnItem.Seq = item.Seq;
            returnItem.AktivaId = item.AktivaId;
            returnItem.AktivaName = FA.IsNull() ? "" : FA.NamaAktiva.Trim();
            returnItem.Notes = item.Notes;
            returnItem.Amount = item.Amount.ToString("N0");
            returnItem.Notes = item.Notes;

            return returnItem;
        }

        [HttpPost]
        public ActionResult Approve(PenerimaanBarangHdrView data)
        {
            ViewData["ActionName"] = "Approve";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            try
            {
                PenerimaanBarangHdr newData = _penerimaanBarangRepository.getPenerimaanBarang(data.GRNo);
                List<FixedAssetMaster> famDatas = new List<FixedAssetMaster>();
                List<Inventory> invtDatas = new List<Inventory>();

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeApprove");

                newData.Status = PenerimaanStatusEnum.A.ToString();

                newData.UpdateUser = user.Username;
                newData.UpdateDate = DateTime.Now;

                _penerimaanBarangRepository.Save(newData);

                if (message == "")
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterApprove");

                if (newData.GRType.Trim() == "AC")
                {
                    foreach (var item in data.PenerimaanAktivaList)
                    {
                        FixedAssetMaster fam = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);
                        fam.Status = AssetStatusEnum.R.ToString();
                        fam.TanggalTerimaAsset = DateTime.ParseExact(data.GRDate, "d/M/yyyy", culture);
                        famDatas.Add(fam);
                        //if (fam.IsOLS)
                        //{
                        Inventory invt = _inventoryRepository.getInventory(item.AktivaId);
                        if (invt != null)
                        {
                            invt.GRNDate = newData.GRDate;
                            invt.SupplierID = newData.VendorID;
                            invt.UserLastUpdate = user.Username;
                            invt.DateLastUpdate = DateTime.Now;

                            invtDatas.Add(invt);
                        }
                        //}
                    }

                    using (TransactionScope scope = new TransactionScope())
                    {
                        foreach (FixedAssetMaster famupd in famDatas)
                        {
                            _fixedAssetMasterRepository.Save(famupd);
                        }
                        scope.Complete();
                    }
                    using (TransactionScope scope = new TransactionScope())
                    {
                        foreach (Inventory invt in invtDatas)
                        {
                            _inventoryRepository.Save(invt);
                        }
                        scope.Complete();
                    }
                }

                ////create GL Journal
                //int year = int.Parse(DateTime.Now.ToString("yyyy"));
                //int month = int.Parse(DateTime.Now.ToString("MM"));

                ////header
                //GLJournalH jurnalH = new GLJournalH("");
                //jurnalH.CompanyID = "001";
                //jurnalH.BranchID = user.Site.SiteID.ToString();
                //jurnalH.PeriodYear = year.ToString();
                //jurnalH.PeriodMonth = month.ToString();
                //jurnalH.TransactionID = "RFA";
                //jurnalH.Reff_No = newData.GRNo;
                //jurnalH.Reff_Date = DateTime.Now;
                //jurnalH.Tr_Date = DateTime.Now;
                //jurnalH.tr_Date1 = (DateTime?)null;
                //jurnalH.PrintDate = (DateTime?)null;
                //jurnalH.LastRevisiPosting = (DateTime?)null;
                //jurnalH.Tr_Desc = "Terima Fixed Asset";
                //jurnalH.JournalAmount = newData.GRAmount;
                //jurnalH.BatchID = "RFA";
                //jurnalH.SubSystem = "FA";
                //jurnalH.Status_Tr = "OP";
                //jurnalH.IsActive = true;
                //jurnalH.IsValid = true;
                //jurnalH.Flag = "R";
                //jurnalH.DtmUpd = DateTime.Now;
                //jurnalH.UsrUpd = user.Username;

                ////detail
                //IList<PenerimaanBarangAktiva> GRA = _penerimaanBarangRepository.getPenerimaanBarangAktiva(data.GRNo);
                //IList<PenerimaanBarangNonAktiva> GRNA = _penerimaanBarangRepository.getPenerimaanBarangNonAktiva(data.GRNo);
                //IList<PenerimaanBarangService> GRS = _penerimaanBarangRepository.getPenerimaanBarangService(data.GRNo);
                //int i = 0;
                //jurnalH.GLJournalDtlList = new List<GLJournalD>();

                ////D
                //if (!GRA.IsNull())
                //{
                //    foreach (var item in GRA)
                //    {
                //        GLJournalD jurnalD = new GLJournalD(i);
                //        FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);
                //        GroupAsset GA = _groupAssetRepository.getGroupAsset(famData.IsNull() ? "" : famData.GroupAssetID);

                //        jurnalD.CompanyID = "001";
                //        jurnalD.BranchID = user.Site.SiteID.ToString().Trim();
                //        jurnalD.SequenceNo = i + 1;
                //        jurnalD.CoaCo = "001";
                //        jurnalD.CoaBranch = user.Site.SiteID.ToString().Trim();
                //        jurnalD.CoaId = GA.IsNull() ? "" : GA.CoaBeban;
                //        jurnalD.TransactionID = "RFA";
                //        jurnalD.Tr_Desc = "Terima " + famData.NamaAktiva.Trim() + "|" + item.AktivaId;
                //        jurnalD.Post = "D";
                //        jurnalD.Amount = item.TotalPrice;
                //        jurnalD.CoaId_X = "-";
                //        jurnalD.PaymentAllocationId = "RFA";
                //        jurnalD.ProductId = "-";
                //        jurnalD.DtmUpd = jurnalH.DtmUpd;
                //        jurnalD.UsrUpd = jurnalH.UsrUpd;

                //        jurnalH.GLJournalDtlList.Add(jurnalD);

                //        i++;
                //    }
                //}
                //if (!GRNA.IsNull())
                //{
                //    foreach (var item in GRNA)
                //    {
                //        GLJournalD jurnalD = new GLJournalD(i);
                //        GroupAsset GA = _groupAssetRepository.getGroupAsset(item.GroupAssetID);

                //        jurnalD.CompanyID = "001";
                //        jurnalD.BranchID = user.Site.SiteID.ToString().Trim();
                //        jurnalD.SequenceNo = i + 1;
                //        jurnalD.CoaCo = "001";
                //        jurnalD.CoaBranch = user.Site.SiteID.ToString().Trim();
                //        jurnalD.CoaId = GA.IsNull() ? "" : GA.CoaBeban;
                //        jurnalD.TransactionID = "RFA";
                //        jurnalD.Tr_Desc = "Terima " + item.ItemName.Trim() + "|" + item.RefferenceNo;
                //        jurnalD.Post = "D";
                //        jurnalD.Amount = item.TotalPrice;
                //        jurnalD.CoaId_X = "-";
                //        jurnalD.PaymentAllocationId = "RFA";
                //        jurnalD.ProductId = "-";
                //        jurnalD.DtmUpd = jurnalH.DtmUpd;
                //        jurnalD.UsrUpd = jurnalH.UsrUpd;

                //        jurnalH.GLJournalDtlList.Add(jurnalD);

                //        i++;
                //    }
                //}
                //if (!GRS.IsNull())
                //{
                //    foreach (var item in GRS)
                //    {
                //        GLJournalD jurnalD = new GLJournalD(i);
                //        FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);
                //        GroupAsset GA = _groupAssetRepository.getGroupAsset(famData.IsNull() ? "" : famData.GroupAssetID);

                //        jurnalD.CompanyID = "001";
                //        jurnalD.BranchID = user.Site.SiteID.ToString().Trim();
                //        jurnalD.SequenceNo = i + 1;
                //        jurnalD.CoaCo = "001";
                //        jurnalD.CoaBranch = user.Site.SiteID.ToString().Trim();
                //        jurnalD.CoaId = GA.IsNull() ? "" : GA.CoaBeban;
                //        jurnalD.TransactionID = "RFA";
                //        jurnalD.Tr_Desc = "Terima " + famData.NamaAktiva.Trim() + "|" + item.AktivaId;
                //        jurnalD.Post = "D";
                //        jurnalD.Amount = item.Amount;
                //        jurnalD.CoaId_X = "-";
                //        jurnalD.PaymentAllocationId = "RFA";
                //        jurnalD.ProductId = item.AktivaId;
                //        jurnalD.DtmUpd = jurnalH.DtmUpd;
                //        jurnalD.UsrUpd = jurnalH.UsrUpd;

                //        jurnalH.GLJournalDtlList.Add(jurnalD);
                //        i++;
                //    }
                //}

                ////C
                //if (i > 0)
                //{
                //    GeneralSetting GS = _generalSettingRepository.GetGeneralSetting("FACOAGR", "FA");

                //    GLJournalD jurnalD = new GLJournalD(i);
                //    jurnalD.CompanyID = "001";
                //    jurnalD.BranchID = user.Site.SiteID.ToString().Trim(); ;
                //    jurnalD.SequenceNo = i + 1;
                //    jurnalD.CoaCo = "001";
                //    jurnalD.CoaBranch = user.Site.SiteID.ToString().Trim(); ;
                //    jurnalD.CoaId = GS.IsNull() ? "" : GS.GSValue.Trim() + "-" + user.Site.SiteID.ToString().Trim(); ;
                //    jurnalD.TransactionID = "RNI";
                //    jurnalD.Tr_Desc = "Receive Not Invoice";
                //    jurnalD.Post = "C";
                //    jurnalD.Amount = jurnalH.JournalAmount;
                //    jurnalD.CoaId_X = "-";
                //    jurnalD.PaymentAllocationId = "RNI";
                //    jurnalD.ProductId = "-";
                //    jurnalD.DtmUpd = jurnalH.DtmUpd;
                //    jurnalD.UsrUpd = jurnalH.UsrUpd;

                //    jurnalH.GLJournalDtlList.Add(jurnalD);
                //}

                //message = _glJournalService.SaveJournal(jurnalH, user, company);
                //if (message == "")
                //    _auditRepository.SaveAuditTrail(Settings.ModuleName, jurnalH, user.Username, "Create");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Approve_succes));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public ActionResult Reject(string id)
        {
            ViewData["ActionName"] = "Reject";
            User user = Lookup.Get<User>();
            string message = "";
            PenerimaanBarangHdr hdr = _penerimaanBarangRepository.getPenerimaanBarang(id);
            PenerimaanBarangHdrView data = ConvertFrom(hdr);

            try
            {
                PenerimaanBarangHdr newData = _penerimaanBarangRepository.getPenerimaanBarang(data.GRNo);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeReject");

                newData.Status = PenerimaanStatusEnum.R.ToString();

                newData.UpdateUser = user.Username;
                newData.UpdateDate = DateTime.Now;

                _penerimaanBarangRepository.Save(newData);

                if (message == "")
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterReject");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Reject_success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private ViewResult CreateView(PenerimaanBarangHdrView data)
        {
            if (data.PenerimaanAktivaList.IsNull())
            {
                data.PenerimaanAktivaList = CreateNewAktivaDetail();
            }
            if (data.PenerimaanNonAktivaList.IsNull())
            {
                data.PenerimaanNonAktivaList = CreateNewNonAktivaDetail();
            }
            ViewData["PenerimaanType"] = PenerimaanTypeSelect(data.GRType == null ? " " : data.GRType);

            generateViewDataGroupAssetSelect(data.PenerimaanNonAktivaList);

            return View("Detail", data);
        }

        private IList<PenerimaanBarangAktivaView> CreateNewAktivaDetail()
        {
            IList<PenerimaanBarangAktivaView> returnDetail = new List<PenerimaanBarangAktivaView>();
            returnDetail.Add(AddNewAktivaRecord(returnDetail));
            return returnDetail;
        }

        private PenerimaanBarangAktivaView AddNewAktivaRecord(IList<PenerimaanBarangAktivaView> AktivaDetail)
        {
            PenerimaanBarangAktivaView detail = new PenerimaanBarangAktivaView();
            detail.Seq = AktivaDetail.IsNull() ? 1 : (AktivaDetail.Count > 0) ? AktivaDetail.LastOrDefault().Seq + 1 : 1;
            detail.Quantity = 1;
            return detail;
        }

        private IList<PenerimaanBarangNonAktivaView> CreateNewNonAktivaDetail()
        {
            IList<PenerimaanBarangNonAktivaView> returnDetail = new List<PenerimaanBarangNonAktivaView>();
            returnDetail.Add(AddNewNonAktivaRecord(returnDetail));
            return returnDetail;
        }

        private PenerimaanBarangNonAktivaView AddNewNonAktivaRecord(IList<PenerimaanBarangNonAktivaView> NonAktivaDetail)
        {
            PenerimaanBarangNonAktivaView detail = new PenerimaanBarangNonAktivaView();
            detail.Seq = NonAktivaDetail.IsNull() ? 1 : (NonAktivaDetail.Count > 0) ? NonAktivaDetail.LastOrDefault().Seq + 1 : 1;
            detail.Quantity = 1;
            return detail;
        }


        private IList<SelectListItem> PenerimaanTypeSelect(string selected)
        {
            List<GRType> itemList = Lookup.Get<List<GRType>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _grTypeRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GRType, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _invoiceTypeID = itemList.FirstOrDefault().GRTypeID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString())).Selected = true;
                _invoiceTypeID = selected;
            }

            return dataList;
        }

        public SelectListItem ConvertForm(GRType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GRTypeName.Trim();
            returnItem.Value = item.GRTypeID.Trim();
            return returnItem;
        }

        private void generateViewDataGroupAssetSelect(IList<PenerimaanBarangNonAktivaView> NonActivaList)
        {
            int index = 0;
            if (!NonActivaList.IsNullOrEmpty())
            {
                foreach (PenerimaanBarangNonAktivaView item in NonActivaList)
                {
                    ViewData["PenerimaanNonAktivaList[" + (index).ToString() + "].GroupAssetIDSelect"] = createGroupAssetSelect(item.GroupAssetID);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }

        private IList<SelectListItem> createGroupAssetSelect(string selected)
        {
            List<GroupAsset> itemList = Lookup.Get<List<GroupAsset>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _groupAssetRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(GroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GroupAssetDescription;
            returnItem.Value = item.GroupAssetID.Trim();
            return returnItem;
        }
    }
}