﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;
using Treemas.Foundation.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.FixedAsset.Web.Resources;

namespace Treemas.Foundation.Controllers
{
    public class KelurahanController : PageController
    {
        private IKelurahanRepository _KelurahanRepository;
        private IAuditTrailLog _auditRepository;

        public KelurahanController(ISessionAuthentication sessionAuthentication, IKelurahanRepository KelurahanRepository,
               IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._KelurahanRepository = KelurahanRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Kelurahan";
            Settings.Title = KelurahanResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                    string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Foundation_Kelurahan> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _KelurahanRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _KelurahanRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<KelurahanView>(new Converter<Foundation_Kelurahan, KelurahanView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public ActionResult NoAuthCheckSearch()
        {
            return Search();
        }

        public KelurahanView ConvertFrom(Foundation_Kelurahan item)
        {
            KelurahanView returnItem = new KelurahanView();
            returnItem.KelurahanID = item.KelurahanID;
            returnItem.Kelurahan = item.Kelurahan;
            returnItem.Kecamatan = item.Kecamatan;
            returnItem.Kota = item.Kota;
            returnItem.ZipCode = item.ZipCode;
            returnItem.AreaCode = item.AreaCode;
            returnItem.Provinsi = item.Provinsi;
            return returnItem;
        }


        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            KelurahanView data = new KelurahanView();
            return CreateView(data);
        }

        private ViewResult CreateView(KelurahanView data)
        {
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(KelurahanView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Foundation_Kelurahan newData = new Foundation_Kelurahan("");
                    newData.KelurahanID = data.KelurahanID;
                    newData.Kelurahan = data.Kelurahan;
                    newData.Kecamatan = data.Kecamatan;
                    newData.Kota = data.Kota;
                    newData.ZipCode = data.ZipCode;
                    newData.AreaCode = data.AreaCode;
                    newData.Provinsi = data.Provinsi;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _KelurahanRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(KelurahanView data, string mode)
        {
            if (data.KelurahanID == "")
                return KelurahanResources.Validation_ID;

            if (data.Kelurahan == "")
                return KelurahanResources.Validation_Kelurahan;

            if (data.Kecamatan == "")
                return KelurahanResources.Validation_Kecamatan;
            if (data.Kota == "")
                return KelurahanResources.Validation_Kota;
            if (data.ZipCode == "")
                return KelurahanResources.Validation_ZipCode;

            if (mode == "Create")
            {
                if (_KelurahanRepository.IsDuplicate(data.KelurahanID))
                    return GeneralResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            Foundation_Kelurahan Kelurahan = _KelurahanRepository.getKelurahanSingle(id);
            KelurahanView data = ConvertFrom(Kelurahan);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(KelurahanView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Foundation_Kelurahan newData = _KelurahanRepository.getKelurahanSingle(data.KelurahanID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.KelurahanID = data.KelurahanID;
                    newData.Kelurahan = data.Kelurahan;
                    newData.Kecamatan = data.Kecamatan;
                    newData.Kota = data.Kota;
                    newData.ZipCode = data.ZipCode;
                    newData.AreaCode = data.AreaCode;
                    newData.Provinsi = data.Provinsi;
                    newData.CreateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _KelurahanRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Foundation_Kelurahan newData = _KelurahanRepository.getKelurahanSingle(Id);
                _KelurahanRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}