﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.FixedAsset.Controllers
{
    public class NamaHartaController : PageController
    {
        private INamaHartaRepository _namaHartaRepository;

        private IAuditTrailLog _auditRepository;

        public NamaHartaController(ISessionAuthentication sessionAuthentication, INamaHartaRepository namaHartaRepository,
            IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._namaHartaRepository = namaHartaRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Nama Harta";
            Settings.Title = NamaHartaResources.PageTitle;
        }
        // GET: NamaHarta
        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<NamaHarta> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _namaHartaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _namaHartaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<NamaHartaView>(new Converter<NamaHarta, NamaHartaView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public NamaHartaView ConvertFrom(NamaHarta item)
        {
            NamaHartaView returnItem = new NamaHartaView();
            //  GroupAsset groupAsset = _groupAssetRepository.getGroupAsset(item.JenisHartaID);

            returnItem.NamaHartaID = item.NamaHartaID;
            returnItem.NamaHartaDeskripsi = item.NamaHartaDeskripsi;
            returnItem.JenisHartaID = item.JenisHartaEnums;
            returnItem.JenisHartaString = item.JenisHartaEnums.ToDescription();
            returnItem.Status = item.Status;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            NamaHartaView data = new NamaHartaView();
            return CreateView(data);
        }

        private IList<SelectListItem> createJenisHarta(string selected)
        {

            List<SelectListItem> dataList = Enum.GetValues(typeof(JenisHartaEnum)).Cast<JenisHartaEnum>().Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();

            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                //dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
                //_AccPeriodId = selected;
            }
            return dataList;
        }
        public SelectListItem Convert1(NamaHarta item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.JenisHartaID;
            returnItem.Value = item.JenisHartaID;
            return returnItem;
        }

        private ViewResult CreateView(NamaHartaView data)
        {
            ViewData["JenisHartaList"] = createJenisHarta(data.JenisHartaValue);
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(NamaHartaView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    NamaHarta newData = new NamaHarta("");
                    newData.NamaHartaID = data.NamaHartaID;
                    newData.NamaHartaDeskripsi = data.NamaHartaDeskripsi;
                    newData.JenisHartaID = data.JenisHartaValue;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _namaHartaRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(NamaHartaView data, string mode)
        {
            if (data.NamaHartaID == "")
                return NamaHartaResources.Validation_Id;

            if (data.NamaHartaDeskripsi == "")
                return NamaHartaResources.Validation_Description;

            if (data.JenisHartaID == 0)
                return NamaHartaResources.Validation_JenisHarta;

            if (mode == "Create")
            {
                if (_namaHartaRepository.IsDuplicate(data.NamaHartaID))
                    return NamaHartaResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            NamaHarta jenisusaha = _namaHartaRepository.getNamaHarta(id);
            NamaHartaView data = ConvertFrom(jenisusaha);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(NamaHartaView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    NamaHarta newData = _namaHartaRepository.getNamaHarta(data.NamaHartaID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.NamaHartaID = data.NamaHartaID;
                    newData.NamaHartaDeskripsi = data.NamaHartaDeskripsi;
                    newData.JenisHartaID = data.JenisHartaID.ToString();

                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _namaHartaRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                NamaHarta newData = _namaHartaRepository.getNamaHarta(Id);
                _namaHartaRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}