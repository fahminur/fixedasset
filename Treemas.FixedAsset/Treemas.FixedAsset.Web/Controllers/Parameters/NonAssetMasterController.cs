﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using System.Globalization;

namespace Treemas.FixedAsset.Controllers
{
    public class NonAssetMasterController : PageController
    {
        private INonAssetMasterRepository _nonAssetMasterRepository;
        private IGroupAssetRepository _groupAssetRepository;
        private ISubGroupAssetRepository _subGroupAssetRepository;
        private IAssetBranchRepository _branchRepository;
        private IAssetAreaRepository _assetAreaRepository;
        private IAssetDeptRepository _assetDeptRepository;
        private IAssetUserRepository _assetUserRepository;
        private IAssetConditionRepository _assetConditionRepository;
        private INonAssetMasterService _nonAssetMasterService;
        private IVendorRepository _VendorRepository;
        private IInsuranceComHORepository _insuranceComHORepository;
        private IInsuranceComBranchRepository _insuranceComBranchRepository;
        private IAuditTrailLog _auditRepository;
        public NonAssetMasterController(INonAssetMasterRepository nonAssetMasterRepository, ISessionAuthentication sessionAuthentication, IGroupAssetRepository groupAssetRepository, ISubGroupAssetRepository subGroupAssetRepository,
            IAssetBranchRepository branchRepository, IAssetAreaRepository assetAreaRepository, IAssetDeptRepository assetDeptRepository, IAssetUserRepository assetUserRepository,
            IAssetConditionRepository assetConditionRepository, INonAssetMasterService nonAssetMasterService, IVendorRepository VendorRepository, IInsuranceComHORepository insuranceComHORepository,
            IInsuranceComBranchRepository insuranceComBranchRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._nonAssetMasterRepository = nonAssetMasterRepository;
            this._groupAssetRepository = groupAssetRepository;
            this._subGroupAssetRepository = subGroupAssetRepository;
            this._branchRepository = branchRepository;
            this._assetAreaRepository = assetAreaRepository;
            this._assetDeptRepository = assetDeptRepository;
            this._assetUserRepository = assetUserRepository;
            this._assetConditionRepository = assetConditionRepository;
            this._nonAssetMasterService = nonAssetMasterService;
            this._VendorRepository = VendorRepository;
            this._insuranceComHORepository = insuranceComHORepository;
            this._insuranceComBranchRepository = insuranceComBranchRepository;
            this._auditRepository = auditRepository;

            Settings.ModuleName = "Registrasi Non Aktiva";
            Settings.Title = NonAssetMasterResources.PageTitle;
        }

        protected override void Startup()
        {
            ViewData["GroupAssetList"] = createGroupAssetSelect(" ");
            ViewData["SubGroupAssetList"] = createSubGroupAssetSelect(" ");
            ViewData["SiteList"] = createSitetSelect(" ");
            ViewData["AssetDeptList"] = createAssetDeptSelect(" ");
            ViewData["AssetUsertList"] = createAssetUserSelect("");
            ViewData["AssetCondList"] = createAssetConditionSelect("");
            ViewData["AsuransiHOList"] = createInsuranceComHOSelect("");
            ViewData["AsuransiBranchList"] = createInsuranceComBranchSelect("");
            ViewData["AssetStatusList"] = createAssetStatusSelect("");
        }

        public ActionResult Search()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                NonAssetFilter filter = new NonAssetFilter();
                filter.NonAktivaID = Request.QueryString.GetValues("NonAktivaID")[0];
                filter.NamaNonAktiva = Request.QueryString.GetValues("NamaNonAktiva")[0];
                filter.GroupAssetID = Request.QueryString["GroupAssetID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("GroupAssetID")[0];
                filter.SubGroupAssetID = Request.QueryString["SubGroupAssetID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("SubGroupAssetID")[0];
                filter.SiteID = Request.QueryString["SiteID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("SiteID")[0];
                filter.AssetDepID = Request.QueryString["AssetDepID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("AssetDepID")[0];
                filter.TanggalPerolehanStart = !Request.QueryString["TanggalPerolehanStart"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalPerolehanStart")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.TanggalPerolehanEnd = !Request.QueryString["TanggalPerolehanEnd"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalPerolehanEnd")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.Status = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<NonAssetMaster> data;
                data = _nonAssetMasterRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<NonAssetMasterView>(new Converter<NonAssetMaster, NonAssetMasterView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public ActionResult NoAuthCheckSearch()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                //User user = Lookup.Get<User>();
                //var company = Lookup.Get<Company>();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                //string siteId = user.Site.SiteId;
                //string companyId = company.CompanyId;

                // Loading.   
                PagedResult<NonAssetMaster> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _nonAssetMasterRepository.FindAllPagedLookup(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _nonAssetMasterRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<NonAssetMasterView>(new Converter<NonAssetMaster, NonAssetMasterView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        public NonAssetMasterView ConvertFrom(NonAssetMaster item)
        {
            NonAssetMasterView returnItem = new NonAssetMasterView();
            GroupAsset groupasset = _groupAssetRepository.getGroupAsset(item.GroupAssetID);
            SubGroupAsset subgroupasset = _subGroupAssetRepository.getSubGroupAsset(item.SubGroupAssetID);
            AssetBranch site = _branchRepository.getAssetBranch(item.SiteID);
            AssetDept assetdept = _assetDeptRepository.getAssetDept(item.AssetDepID);
            AssetUser assetuser = _assetUserRepository.getAssetUser(item.AssetUserID);
            AssetCondition assetcondition = _assetConditionRepository.getAssetCondition(item.AssetConditionID);
            Vendor Vendor = _VendorRepository.getVendor(item.VendorID);

            //tab aktiva
            returnItem.NonAktivaID = item.NonAktivaID;
            returnItem.NamaNonAktiva = item.NamaNonAktiva;
            returnItem.GroupAssetID = item.GroupAssetID.IsNull() ? "" : item.GroupAssetID.Trim();
            returnItem.GroupAssetString = groupasset.IsNull() ? "" : groupasset.GroupAssetDescription;
            returnItem.SubGroupAssetID = item.SubGroupAssetID.IsNull() ? "" : item.SubGroupAssetID.Trim();
            returnItem.SubGroupAssetString = subgroupasset.IsNull() ? "" : subgroupasset.SubGroupAssetDescription;
            returnItem.SiteID = item.SiteID.IsNull() ? "" : item.SiteID.Trim();
            returnItem.SiteString = site.IsNull() ? "" : site.AssetBranchDescription;
            returnItem.Lokasi = item.Lokasi.IsNullOrEmpty() ? "" : item.Lokasi.Trim();
            returnItem.AssetAreaID = item.AssetAreaID.IsNull() ? "" : item.AssetAreaID.Trim();
            returnItem.AssetDepID = item.AssetDepID.IsNull() ? "" : item.AssetDepID.Trim();
            returnItem.AssetDepString = assetdept.IsNull() ? "" : assetdept.AssetDeptDescription;
            returnItem.AssetUserID = item.AssetUserID.IsNull() ? "" : item.AssetUserID.Trim();
            returnItem.AssetUserString = assetuser.IsNull() ? "" : assetuser.AssetUserName;
            returnItem.AssetConditionID = item.AssetConditionID.IsNull() ? "" : item.AssetConditionID.Trim();
            returnItem.Merek = item.Merek;
            returnItem.Model = item.Model;
            returnItem.SerialNo1 = item.SerialNo1;
            returnItem.SerialNo2 = item.SerialNo2;
            returnItem.MasaGaransi = item.MasaGaransi.IsNull() ? null : Convert.ToDateTime(item.MasaGaransi).ToString("dd/MM/yyyy");
            returnItem.KeteranganAktiva = item.KeteranganAktiva;
            returnItem.NoPolis = item.NoPolis;
            returnItem.TglPolis = item.TglPolis.IsNull() ? null : Convert.ToDateTime(item.TglPolis).ToString("dd/MM/yyyy");
            returnItem.InsuranceComID = item.InsuranceComID.IsNull() ? "" : item.InsuranceComID.Trim();
            returnItem.InsuranceComBranchID = item.InsuranceComBranchID.IsNull() ? "" : item.InsuranceComBranchID.Trim();
            returnItem.TanggalTerimaAsset = item.TanggalTerimaAsset.IsNull() ? null : Convert.ToDateTime(item.TanggalTerimaAsset).ToString("dd/MM/yyyy");

            //tab perolehan
            returnItem.VendorID = item.VendorID.IsNull() ? "" : item.VendorID.Trim();
            returnItem.VendorName = Vendor.IsNull() ? "" : Vendor.VendorName.Trim();
            returnItem.NoPO = item.NoPO;
            returnItem.NoDO = item.NoDO;
            returnItem.NoGSRN = item.NoGSRN;
            returnItem.NoInvoice = item.NoInvoice;
            returnItem.TanggalPerolehan = item.TanggalPerolehan.IsNull() ? null : Convert.ToDateTime(item.TanggalPerolehan).ToString("dd/MM/yyyy");
            returnItem.NilaiOriginal = item.NilaiOriginal.ToString("N0");
            returnItem.Kurs = item.Kurs.ToString("N0");
            returnItem.HargaPerolehan = item.HargaPerolehan.ToString("N0");
            returnItem.KeteranganPerolehan = item.KeteranganPerolehan;
            returnItem.Status = item.Status.Trim();// ? "Aktif" : "Non-Aktif";
            returnItem.StatusString = item.NAStatusEnum.ToDescription();

            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            NonAssetMasterView data = new NonAssetMasterView();
            return CreateView(data);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult Create(NonAssetMasterView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            string message = "";

            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    NonAssetMaster newData = new NonAssetMaster("");

                    //newData.NonAktivaID = data.NonAktivaID;
                    newData.NamaNonAktiva = data.NamaNonAktiva;
                    newData.GroupAssetID = data.GroupAssetID;
                    newData.SubGroupAssetID = data.SubGroupAssetID;
                    newData.SiteID = data.SiteID;
                    newData.Lokasi = data.Lokasi;
                    newData.AssetDepID = data.AssetDepID;
                    newData.AssetUserID = data.AssetUserID;
                    newData.AssetConditionID = data.AssetConditionID;
                    newData.Merek = data.Merek;
                    newData.Model = data.Model;
                    newData.SerialNo1 = data.SerialNo1;
                    newData.InsuranceComID = data.InsuranceComID;
                    newData.InsuranceComBranchID = data.InsuranceComBranchID;
                    newData.SerialNo2 = data.SerialNo2;
                    newData.MasaGaransi = !data.MasaGaransi.IsNullOrEmpty() ? DateTime.ParseExact(data.MasaGaransi, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.KeteranganAktiva = data.KeteranganAktiva;
                    newData.VendorID = data.VendorID;
                    newData.NoPO = data.NoPO;
                    newData.NoDO = data.NoDO;
                    newData.NoGSRN = data.NoGSRN;
                    newData.NoInvoice = data.NoInvoice;
                    newData.TanggalPerolehan = !data.TanggalPerolehan.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPerolehan, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.NilaiOriginal = Convert.ToDouble(data.NilaiOriginal);
                    newData.Kurs = Convert.ToDouble(data.Kurs);
                    newData.HargaPerolehan = Convert.ToDouble(data.HargaPerolehan);
                    newData.KeteranganPerolehan = data.KeteranganPerolehan;
                    newData.TanggalTerimaAsset = !data.TanggalTerimaAsset.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalTerimaAsset, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.Status = AssetStatusEnum.R.ToString();
                    newData.NoPolis = data.NoPolis;
                    newData.TglPolis = !data.TglPolis.IsNullOrEmpty() ? DateTime.ParseExact(data.TglPolis, "d/M/yyyy", culture) : (DateTime?)null;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;

                    message = _nonAssetMasterService.SaveNonAssetMaster(newData, user, company);
                    if (message == "")
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Save_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            NonAssetMaster NonAssetmaster = _nonAssetMasterRepository.getNonAssetMaster(id);
            NonAssetMasterView data = ConvertFrom(NonAssetmaster);

            return CreateView(data);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Edit")]

        public ActionResult Edit(NonAssetMasterView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            string message = "";

            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    NonAssetMaster newData = _nonAssetMasterRepository.getNonAssetMaster(data.NonAktivaID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.NamaNonAktiva = data.NamaNonAktiva;
                    newData.GroupAssetID = data.GroupAssetID;
                    newData.SubGroupAssetID = data.SubGroupAssetID;
                    newData.SiteID = data.SiteID;
                    newData.Lokasi = data.Lokasi;
                    newData.AssetDepID = data.AssetDepID;
                    newData.AssetUserID = data.AssetUserID;
                    newData.AssetConditionID = data.AssetConditionID;
                    newData.Merek = data.Merek;
                    newData.Model = data.Model;
                    newData.SerialNo1 = data.SerialNo1;
                    newData.InsuranceComID = data.InsuranceComID;
                    newData.InsuranceComBranchID = data.InsuranceComBranchID;
                    newData.SerialNo2 = data.SerialNo2;
                    newData.MasaGaransi = !data.MasaGaransi.IsNullOrEmpty() ? DateTime.ParseExact(data.MasaGaransi, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.KeteranganAktiva = data.KeteranganAktiva;
                    newData.VendorID = data.VendorID;
                    newData.NoPO = data.NoPO;
                    newData.NoDO = data.NoDO;
                    newData.NoGSRN = data.NoGSRN;
                    newData.NoInvoice = data.NoInvoice;
                    newData.TanggalPerolehan = !data.TanggalPerolehan.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPerolehan, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.NilaiOriginal = Convert.ToDouble(data.NilaiOriginal);
                    newData.Kurs = Convert.ToDouble(data.Kurs);
                    newData.HargaPerolehan = Convert.ToDouble(data.HargaPerolehan);
                    newData.KeteranganPerolehan = data.KeteranganPerolehan;
                    newData.TanggalTerimaAsset = !data.TanggalTerimaAsset.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalTerimaAsset, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.Status = data.Status;
                    newData.NoPolis = data.NoPolis;
                    newData.TglPolis = !data.TglPolis.IsNullOrEmpty() ? DateTime.ParseExact(data.TglPolis, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    message = _nonAssetMasterService.UpdateNonAssetMaster(newData, user, company);
                    if (message == "")
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Update_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                NonAssetMaster newData = _nonAssetMasterRepository.getNonAssetMaster(Id);

                newData.Status = AssetStatusEnum.D.ToString();
                newData.UpdateUser = user.Username;
                newData.UpdateDate = DateTime.Now;
                _nonAssetMasterRepository.Save(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

        private ViewResult CreateView(NonAssetMasterView data)
        {
            ViewData["GroupAssetList"] = createGroupAssetSelect(data.GroupAssetID);
            ViewData["SubGroupAssetList"] = createSubGroupAssetSelect(data.SubGroupAssetID);
            ViewData["SiteList"] = createSitetSelect(data.SiteID);
            ViewData["AssetDeptList"] = createAssetDeptSelect(data.AssetDepID);
            ViewData["AssetUsertList"] = createAssetUserSelect(data.AssetUserID);
            ViewData["AssetCondList"] = createAssetConditionSelect(data.AssetConditionID);
            ViewData["AsuransiHOList"] = createInsuranceComHOSelect(data.InsuranceComID);
            ViewData["AsuransiBranchList"] = createInsuranceComBranchSelect(data.InsuranceComBranchID);

            return View("Detail", data);
        }

        private IList<SelectListItem> createAssetStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(AssetStatusEnum)).Cast<AssetStatusEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }

        private IList<SelectListItem> createGroupAssetSelect(string selected)
        {
            List<GroupAsset> itemList = Lookup.Get<List<GroupAsset>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _groupAssetRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(GroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GroupAssetDescription;
            returnItem.Value = item.GroupAssetID.Trim();// + ',' + item.CoaPerolehan.Trim() + ',' + item.CoaAkumulasi.Trim() + ',' + item.CoaBeban.Trim();
            return returnItem;
        }

        //================================================================end====================================================================

        //==================================================== Sub Group Asset ==================================================================
        private IList<SelectListItem> createSubGroupAssetSelect(string selected)
        {
            List<SubGroupAsset> itemList = Lookup.Get<List<SubGroupAsset>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _subGroupAssetRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<SubGroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(SubGroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.SubGroupAssetDescription;
            returnItem.Value = item.SubGroupAssetID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================

        //==================================================== Site ==================================================================
        private IList<SelectListItem> createSitetSelect(string selected)
        {
            List<AssetBranch> itemList = null;// Lookup.Get<List<AssetBranch>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _branchRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetBranch, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetBranch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetBranchDescription;
            returnItem.Value = item.AssetBranchID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //====================================================  Asset Area ==================================================================
        private IList<SelectListItem> createAssetAreaSelect(string selected)
        {
            List<AssetArea> itemList = Lookup.Get<List<AssetArea>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetAreaRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetArea, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetArea item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetAreaDescription;
            returnItem.Value = item.AssetAreaID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //====================================================  Asset Dept ==================================================================
        private IList<SelectListItem> createAssetDeptSelect(string selected)
        {
            List<AssetDept> itemList = null;// Lookup.Get<List<AssetDept>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetDeptRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetDept, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetDept item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetDeptDescription;
            returnItem.Value = item.AssetDeptID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //==================================================== Asset User ==================================================================
        private IList<SelectListItem> createAssetUserSelect(string selected)
        {
            List<AssetUser> itemList = null;// Lookup.Get<List<AssetUser>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetUserRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetUser, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetUser item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetUserName;
            returnItem.Value = item.AssetUserID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //====================================================  Asset Condition ==================================================================
        private IList<SelectListItem> createAssetConditionSelect(string selected)
        {
            List<AssetCondition> itemList = null; //Lookup.Get<List<AssetCondition>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetConditionRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetCondition, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetCondition item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetConditionDescription;
            returnItem.Value = item.AssetConditionID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================
        //==================================================== Insurance HO ==================================================================
        private IList<SelectListItem> createInsuranceComHOSelect(string selected)
        {
            List<InsuranceComHO> itemList = Lookup.Get<List<InsuranceComHO>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _insuranceComHORepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<InsuranceComHO, SelectListItem>(ConvertFromParent));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFromParent(InsuranceComHO item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description.Trim();
            returnItem.Value = item.MaskAssID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================

        //==================================================== Insurance HO ==================================================================
        private IList<SelectListItem> createInsuranceComBranchSelect(string selected)
        {
            List<InsuranceComBranch> itemList = Lookup.Get<List<InsuranceComBranch>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _insuranceComBranchRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<InsuranceComBranch, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(InsuranceComBranch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.MaskAssBranchName.Trim();
            returnItem.Value = item.MaskAssBranchID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================

        private string validateData(NonAssetMasterView data, string mode)
        {
            if (mode == "Create")
            {
                if (data.NamaNonAktiva == "" || data.NamaNonAktiva.IsNull())
                    return NonAssetMasterResources.Validation_Nama;

                if (data.GroupAssetID == "" || data.GroupAssetID.IsNull())
                    return NonAssetMasterResources.Validation_GroupAssetID;

                if (data.SubGroupAssetID == "" || data.SubGroupAssetID.IsNull())
                    return NonAssetMasterResources.Validation_subassetid;

                //if (data.NamaHartaID == "" || data.NamaHartaID.IsNull())
                //    return NonAssetMasterResources.Validation_Namaharta;

                if (data.SiteID == "" || data.SiteID.IsNull())
                    return NonAssetMasterResources.Validation_site;

                //if (data.AssetLocationID == "" || data.AssetLocationID.IsNull())
                //    return NonAssetMasterResources.Validation_lokasi;

                //if (data.AssetAreaID == "" || data.AssetAreaID.IsNull())
                //    return NonAssetMasterResources.Validation_area;

                //if (data.AssetDepID == "" || data.AssetDepID.IsNull())
                //    return NonAssetMasterResources.Validation_dept;
            }

            else if (mode == "Edit")
            {
                if (data.NamaNonAktiva == "" || data.NamaNonAktiva.IsNull())
                    return NonAssetMasterResources.Validation_Nama;

                if (data.GroupAssetID == "" || data.GroupAssetID.IsNull())
                    return NonAssetMasterResources.Validation_GroupAssetID;

                if (data.SubGroupAssetID == "" || data.SubGroupAssetID.IsNull())
                    return NonAssetMasterResources.Validation_subassetid;

                //if (data.NamaHartaID == "" || data.NamaHartaID.IsNull())
                //    return NonAssetMasterResources.Validation_Namaharta;

                if (data.SiteID == "" || data.SiteID.IsNull())
                    return NonAssetMasterResources.Validation_site;

                //if (data.AssetLocationID == "" || data.AssetLocationID.IsNull())
                //    return NonAssetMasterResources.Validation_lokasi;

                //if (data.AssetAreaID == "" || data.AssetAreaID.IsNull())
                //    return NonAssetMasterResources.Validation_area;

                //if (data.AssetDepID == "" || data.AssetDepID.IsNull())
                //    return NonAssetMasterResources.Validation_dept;

                //if (data.VendorID.IsNull() || data.VendorID == "")
                //    return NonAssetMasterResources.Validation_vendor;

                //if (data.NoPO == "" || data.NoPO.IsNull())
                //    return NonAssetMasterResources.Validation_nopo;

                //if (data.NoDO == "" || data.NoDO.IsNull())
                //    return NonAssetMasterResources.Validation_nodo;

                //if (data.NoGSRN == "" || data.NoGSRN.IsNull())
                //    return NonAssetMasterResources.Validation_nogsrn;

                //if (data.NoInvoice == "" || data.NoInvoice.IsNull())
                //    return NonAssetMasterResources.Validation_invoice;

                //if (data.NoPaymentVoucher == "" || data.NoPaymentVoucher.IsNull())
                //    return NonAssetMasterResources.Validation_payment;

                //if (data.TanggalPerolehan.IsNull())
                //    return NonAssetMasterResources.Validation_tglperolehan;

                //if (data.CurrencyID == "" || data.CurrencyID.IsNull())
                //    return NonAssetMasterResources.Validation_currency;

                //if (data.NilaiOriginal == 0 || data.NilaiOriginal.IsNull() || data.NilaiOriginal.ToString() == "")
                //    return NonAssetMasterResources.Validation_NilaiOriginal;

                //if (data.Kurs == 0 || data.Kurs.IsNull() || data.Kurs.ToString() == "")
                //    return NonAssetMasterResources.Validation_Kurs;

                //if (data.HargaPerolehan == 0 || data.HargaPerolehan.IsNull() || data.HargaPerolehan.ToString() == "")
                //    return NonAssetMasterResources.HargaPerolehan;

                //if (data.MetodeDepresiasiKomersial == "" || data.MetodeDepresiasiKomersial.IsNull())
                //    return NonAssetMasterResources.Validation_MetodeDepriasi;

                //if (data.MasaManfaatKomersial == 0 || data.MasaManfaatKomersial.IsNull() || data.MasaManfaatKomersial.ToString() == "")
                //    return NonAssetMasterResources.Validation_MasaManfaatKom;

                //if (data.PeriodeAwal.IsNull() || data.PeriodeAwal == "")
                //    return NonAssetMasterResources.Validation_PeriodeAwal;

                //if (data.PeriodeAkhir.IsNull() || data.PeriodeAkhir == "")
                //    return NonAssetMasterResources.Validation_PeriodeAkhir;

                //if (data.Tarif == 0 || data.Tarif.IsNull() || data.Tarif.ToString() == "")
                //    return NonAssetMasterResources.Validation_Tarif;

                //if (data.ResiduKomersial == 0 || data.ResiduKomersial.IsNull() || data.ResiduKomersial.ToString() == "")
                //    return NonAssetMasterResources.Validation_Residu;

                //if (data.GolonganPajakID == "" || data.GolonganPajakID.IsNull())
                //    return NonAssetMasterResources.Validation_Golpajak;

                //if (data.MetodeDepresiasiFiskal == "" || data.MetodeDepresiasiFiskal.IsNull())
                //    return NonAssetMasterResources.Validation_MetodeDepriasifis;

                //if (data.MasaManfaatFiskal == 0 || data.MasaManfaatFiskal.IsNull() || data.MasaManfaatFiskal.ToString() == "")
                //    return NonAssetMasterResources.Validation_MasaManfaatfis;

                //if (data.PeriodeMulaiFiskal.IsNull() || data.PeriodeMulaiFiskal == "")
                //    return NonAssetMasterResources.Validation_PeriodeAwalfis;

                //if (data.PeriodeAkhirFiskal.IsNull() || data.PeriodeAkhirFiskal == "")
                //    return NonAssetMasterResources.Validation_PeriodeAkhirfis;

                //if (data.TarifFiskal == 0 || data.TarifFiskal.IsNull() || data.TarifFiskal.ToString() == "")
                //    return NonAssetMasterResources.Validation_Tariffis;

                //if (data.Pembebanan == 0 || data.Pembebanan.IsNull() || data.Pembebanan.ToString() == "")
                //    return NonAssetMasterResources.Validation_Pembebananfis;

                //if (data.ResiduFiskal == 0 || data.ResiduFiskal.IsNull() || data.ResiduFiskal.ToString() == "")
                //    return NonAssetMasterResources.Validation_Residufis;


                //if (data.TanggalPenghapusan.IsNull() || data.TanggalPenghapusan.IsNull() || data.TanggalPenghapusan.ToString() == "")
                //    return NonAssetMasterResources.Validation_TglPenghapusan;

                //if (data.NomorReferensi == "" || data.NomorReferensi.IsNull())
                //    return NonAssetMasterResources.Validation_Noref;

                //if (data.HargaJual == 0 || data.HargaJual.IsNull() || data.HargaJual.ToString() == "")
                //    return NonAssetMasterResources.Validation_Hargaju;

                //if (data.Penyusutandihitung.IsNull())
                //    return NonAssetMasterResources.Validation_penyusutandihit;

                //if (data.AlasanPengapusan == "" || data.AlasanPengapusan.IsNull())
                //    return NonAssetMasterResources.Validation_penghapusfis;

                //if (data.TanggalPenghapusanFiskal.IsNull())
                //    return NonAssetMasterResources.Validation_TglPenghapusanfis;

                //if (data.NomorReferensiFiskal == "" || data.NomorReferensiFiskal.IsNull())
                //    return NonAssetMasterResources.Validation_Noreffis;

                //if (data.HargaJualFiskal == 0 || data.HargaJualFiskal.IsNull() || data.HargaJualFiskal.ToString() == "")
                //    return NonAssetMasterResources.Validation_Hargajufis;

                //if (data.AlasanFiskal.IsNull() || data.AlasanFiskal == "")
                //    return NonAssetMasterResources.Validation_penghapusfis;
            }

            if (mode == "Create")
            {
                if (_nonAssetMasterRepository.IsDuplicate(data.NonAktivaID))
                    return NonAssetMasterResources.Validation_DuplicateData;
            }
            return "";
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckSubGroupAsset(string GroupAssetID)
        {
            List<SubGroupAsset> itemList = null;
            itemList = _subGroupAssetRepository.FindByGroupAsset(GroupAssetID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<SubGroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetUserByBranch(string BrachID)
        {
            List<AssetUser> itemList = null;
            itemList = _assetUserRepository.FindByBranchID(BrachID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetUser, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetUserByDept(string BrachID, string DeptID)
        {
            List<AssetUser> itemList = null;
            itemList = _assetUserRepository.FindByDeptID(BrachID, DeptID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetUser, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }   
    }
}