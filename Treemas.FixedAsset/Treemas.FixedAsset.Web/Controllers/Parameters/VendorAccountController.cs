﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;

using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using System.Globalization;

namespace Treemas.FixedAsset.Web.Controllers
{
    public class VendorAccountController : PageController
    {
        private IBankBranchMasterRepository _bankBranchMasterRepository;
        private IBankMasterRepository _bankMasterRepository;
        private IVendorAccountRepository _vendorAccountRepository;
        private IVendorRepository _vendorRepository;
        private IAuditTrailLog _auditRepository;

        public VendorAccountController(ISessionAuthentication sessionAuthentication, IBankBranchMasterRepository bankBranchMasterRepository, IBankMasterRepository bankMasterRepository, IVendorAccountRepository vendorAccountRepository,
            IVendorRepository vendorRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._bankBranchMasterRepository = bankBranchMasterRepository;
            this._bankMasterRepository = bankMasterRepository;
            this._vendorAccountRepository = vendorAccountRepository;
            this._vendorRepository = vendorRepository;
            this._auditRepository = auditRepository;

            Settings.ModuleName = "Vendor Bank Account";
            Settings.Title = VendorResources.PageTitle;
        }


        protected override void Startup()
        {
            ViewData["BankList"] = createBankListSelect("");
            ViewData["BankBranchList"] = createBankBranchListSelect("", "");

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                VendorBankFilter filter = new VendorBankFilter();
                filter.VendorID = Request.QueryString.GetValues("VendorID")[0];
                filter.VendorAccountName = Request.QueryString.GetValues("VendorAccountName")[0];
                filter.VendorBankBranchID = Request.QueryString.GetValues("VendorBankBranchID")[0];
                filter.VendorBankID = Request.QueryString.GetValues("VendorBankID")[0];

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<VendorAccount> data;
                data = _vendorAccountRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<VendorAccountView>(new Converter<VendorAccount, VendorAccountView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        private VendorAccountView ConvertFrom(VendorAccount input)
        {
            VendorAccountView returnItem = new VendorAccountView();
            BankMaster bank = _bankMasterRepository.getBankMaster(input.VendorBankID);
            Vendor vendor = _vendorRepository.getVendor(input.VendorID);

            returnItem.VendorID = input.VendorID;
            returnItem.VendorName = vendor.IsNull() ? "" : vendor.VendorName.Trim();
            returnItem.VendorAccID = input.VendorAccID.Trim();
            returnItem.VendorBankID = input.VendorBankID.Trim();
            returnItem.VendorBankName = bank.IsNull() ? "" : bank.BankName.Trim();
            returnItem.VendorBankBranch = input.VendorBankBranch;
            returnItem.VendorAccountNo = input.VendorAccountNo;
            returnItem.VendorAccountName = input.VendorAccountName;
            returnItem.VendorBankBranchID = input.VendorBankBranchID;
            returnItem.DefaultAccount = input.DefaultAccount;
            returnItem.UntukBayar = input.UntukBayar;
            returnItem.TanggalEfektif = input.TanggalEfektif.IsNull() ? null : Convert.ToDateTime(input.TanggalEfektif).ToString("dd/MM/yyyy");

            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            VendorAccountView data = new VendorAccountView();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Create(VendorAccountView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    List<VendorAccount> count = _vendorAccountRepository.getVendorAccbyVendor(data.VendorID).ToList();
                    BankBranchMaster branch = _bankBranchMasterRepository.getBankBranchMaster(data.VendorBankBranchID.ToString());
                    VendorAccount newData = new VendorAccount("");
                    
                    newData.VendorID = data.VendorID;
                    newData.VendorAccID = data.VendorID.Trim()+data.VendorBankID.Trim() + (count.IsNull() ? 1 : count.Count + 1);
                    newData.VendorBankID = data.VendorBankID.Trim();
                    newData.VendorBankBranch = branch.IsNull() ? "" : branch.BankBranchName.Trim();
                    newData.VendorAccountNo = data.VendorAccountNo;
                    newData.VendorAccountName = data.VendorAccountName;
                    newData.VendorBankBranchID = data.VendorBankBranchID;
                    newData.DefaultAccount = data.DefaultAccount;
                    newData.UntukBayar = data.UntukBayar;
                    newData.TanggalEfektif = !data.TanggalEfektif.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalEfektif, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.UsrUpd = user.Username;
                    newData.DtmUpd = DateTime.Now;

                    _vendorAccountRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        //[HttpPost]
        //public ActionResult Delete(string Id)
        //{
            //User user = Lookup.Get<User>();
            //string message = "";
            //try
            //{
            //    VendorAccount newData = _vendorAccountRepository.getVendorAccount(Id);

            //    newData. = false;
            //    newData.UsrUpd = user.Username;
            //    newData.DtmUpd = DateTime.Now;

            //    _VendorRepository.Save(newData);
            //    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");

            //}
            //catch (Exception e)
            //{
            //    message = e.Message;
            //}

            //if (message.IsNullOrEmpty())
            //{
            //    ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
            //    return RedirectToAction("Index");
            //}
            //ScreenMessages.Submit(ScreenMessage.Error(message));
            //CollectScreenMessages();
            //return Edit(Id);
        //}

        private ViewResult CreateView(VendorAccountView data)
        {
            ViewData["BankList"] = createBankListSelect(data.VendorBankID);
            ViewData["BankBranchList"] = createBankBranchListSelect(data.VendorBankID, data.VendorBankBranchID.ToString());
            return View("Detail", data);
        }

        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            VendorAccount Vendor = _vendorAccountRepository.getVendorAccount(id);
            VendorAccountView data = ConvertFrom(Vendor);

            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(VendorAccountView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    VendorAccount newData = _vendorAccountRepository.getVendorAccount(data.VendorAccID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");
                    BankBranchMaster branch = _bankBranchMasterRepository.getBankBranchMaster(data.VendorBankBranchID.ToString());

                    newData.VendorID = data.VendorID;
                    newData.VendorAccID = data.VendorAccID;
                    newData.VendorBankID = data.VendorBankID.Trim();
                    newData.VendorBankBranch = branch.IsNull() ? "" : branch.BankBranchName.Trim();
                    newData.VendorAccountNo = data.VendorAccountNo;
                    newData.VendorAccountName = data.VendorAccountName;
                    newData.VendorBankBranchID = data.VendorBankBranchID;
                    newData.DefaultAccount = data.DefaultAccount;
                    newData.UntukBayar = data.UntukBayar;
                    newData.TanggalEfektif = !data.TanggalEfektif.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalEfektif, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.UsrUpd = user.Username;
                    newData.DtmUpd = DateTime.Now;

                    _vendorAccountRepository.Save(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private IList<SelectListItem> createBankListSelect(string selected)
        {
            List<BankMaster> itemList = Lookup.Get<List<BankMaster>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _bankMasterRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<BankMaster, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(BankMaster item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BankName.Trim();
            returnItem.Value = item.BankID.Trim();
            return returnItem;
        }

        private IList<SelectListItem> createBankBranchListSelect(string BankID, string selected)
        {
            List<BankBranchMaster> itemList = null;// Lookup.Get<List<BankBranchMaster>>();

            if (itemList.IsNullOrEmpty())
            {
                if (BankID.IsNullOrEmpty())
                {
                    itemList = _bankBranchMasterRepository.FindAll().ToList();
                }
                else
                {
                    itemList = _bankBranchMasterRepository.getBankBranchByBankID(BankID).ToList();
                }
                Lookup.Remove(itemList);
                Lookup.Add(itemList);

            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<BankBranchMaster, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty() && selected != "0")
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetBankBranch(string BankID)
        {
            List<BankBranchMaster> itemList = null;
            itemList = _bankBranchMasterRepository.getBankBranchByBankID(BankID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<BankBranchMaster, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public SelectListItem ConvertFrom(BankBranchMaster item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BankBranchName.Trim();
            returnItem.Value = item.BankBranchid.Trim();
            return returnItem;
        }

        private string validateData(VendorAccountView data, string mode)
        {
            if (data.VendorID.IsNullOrEmpty())
                return "Nama vendor harus di pilih!";

            if (data.VendorBankID.IsNullOrEmpty())
                return "Nama Bank harus di pilih!";

            if (data.VendorBankBranchID.IsNull())
                return "Kantor Cabang Bank harus di pilih!";

            if (data.VendorAccountNo.IsNullOrEmpty())
                return "Nomor Rekening harus di isi!";

            if (data.VendorAccountName.IsNullOrEmpty())
                return "Atas Nama Rekening harus di isi!";

            if (data.UntukBayar.IsNullOrEmpty())
                return "Keterangan Untuk Bayar harus di isi!";

            if (data.TanggalEfektif.IsNullOrEmpty())
                return "Efektif Tanggal harus di pilih!";
            
            if (mode == "Create")
            {
                if (_vendorAccountRepository.IsDuplicate(data.VendorID, data.VendorAccID))
                    return VendorResources.Validation_DuplicateData;
            }
            return "";
        }

    }
}