﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Controllers
{
    public class AssetTidakSusutController : PageController
    {
        private IAssetTidakSusutRepository _AssetTidakSusutRepository;
        private IFixedAssetMasterRepository _FixedAssetMasterRepository;


        private IAuditTrailLog _auditRepository;
        public IList<AssetTidakSusut> AssetTidakSusutList;
        int i = 0;

        public AssetTidakSusutController(ISessionAuthentication sessionAuthentication, IFixedAssetMasterRepository FixedAssetMasterRepository, IAssetTidakSusutRepository AssetTidakSusutRepository,
          IAuditTrailLog auditRepository)
            : base(sessionAuthentication)
        {
            this._AssetTidakSusutRepository = AssetTidakSusutRepository;
            this._FixedAssetMasterRepository = FixedAssetMasterRepository;
            this._auditRepository = auditRepository;
            //Settings.Title = AssetTidakSusutResources.PageTitleRoleFunction;
            Settings.ModuleName = "FixedAssetMaster";

        }
        protected override void Startup()
        {
            var returnpage = new { controller = "FixedAssetMaster", action = "Index" };
            try
            {
                string Aktivaid = Request.RequestContext.RouteData.Values["Id"].ToString();
                FixedAssetMaster fixedassetmaster = null;
                if (Aktivaid.IsNullOrEmpty())
                    Response.RedirectToRoute(returnpage);
                fixedassetmaster = _FixedAssetMasterRepository.getFixedAssetMaster(Aktivaid);



                FixedAssetMasterView FixedAssetMasterView = ConvertFromViewData(fixedassetmaster);
                Lookup.Remove<FixedAssetMasterView>();
                Lookup.Add(FixedAssetMasterView);
                ViewData["FixedAssetMaster"] = FixedAssetMasterView;
            }
            catch
            {
                Response.RedirectToRoute(returnpage);
            }

        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                FixedAssetMasterView FixedAssetMasterView = Lookup.Get<FixedAssetMasterView>();
                if (FixedAssetMasterView.IsNull())
                {
                    if (Request.QueryString.GetValues("Id").IsNullOrEmpty())
                        return null;
                    string Id = Request.QueryString.GetValues("Id")[0].ToString();
                    FixedAssetMaster FixedAssetMaster = _FixedAssetMasterRepository.getFixedAssetMaster(Id);

                    FixedAssetMasterView = ConvertFromViewData(FixedAssetMaster);
                    Lookup.Remove<FixedAssetMasterView>();
                    Lookup.Add(FixedAssetMasterView);
                }

                //   ViewData["FixedAssetMaster"] = FixedAssetMasterView;
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // AssetTidakSusutList = _AssetTidakSusutRepository.getAssetTidakSusut(FixedAssetMasterView.Id);

                // Loading.   
                PagedResult<AssetTidakSusut> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _AssetTidakSusutRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, FixedAssetMasterView.AktivaID);
                }
                else
                {
                    data = _AssetTidakSusutRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, FixedAssetMasterView.AktivaID);
                }

                var items = data.Items.ToList().ConvertAll<AssetTidakSusutView>(new Converter<AssetTidakSusut, AssetTidakSusutView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public ActionResult NoAuthCheckSearch()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string FixedAssetMasterId = Request.QueryString.GetValues("FixedAssetMasterIdLookup").FirstOrDefault().ToString();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<AssetTidakSusut> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _AssetTidakSusutRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, FixedAssetMasterId);

                }
                else
                {
                    data = _AssetTidakSusutRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, FixedAssetMasterId);
                }

                var items = data.Items.ToList().ConvertAll<AssetTidakSusutView>(new Converter<AssetTidakSusut, AssetTidakSusutView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public AssetTidakSusutView ConvertFrom(AssetTidakSusut item)
        {
            AssetTidakSusutView returnItem = new AssetTidakSusutView();
            FixedAssetMasterView FixedAssetMasterView = Lookup.Get<FixedAssetMasterView>();

            returnItem.Id = item.Id;
            returnItem.RowID = item.Id;
            returnItem.AktivaId = item.AktivaId;
            returnItem.Bulan = item.Bulan;
            returnItem.Tahun = item.Tahun;

            ViewData["FixedAssetMaster"] = FixedAssetMasterView;
            return returnItem;
        }
        public FixedAssetMasterView ConvertFromViewData(FixedAssetMaster item)
        {
            FixedAssetMasterView returnItem = new FixedAssetMasterView();


            returnItem.AktivaID = item.AktivaID;
            returnItem.NamaAktiva = item.NamaAktiva;


            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            AssetTidakSusutView data = new AssetTidakSusutView();
            data.AktivaId = Lookup.Get<FixedAssetMasterView>().AktivaID;
            return CreateView(data);
        }

        private ViewResult CreateView(AssetTidakSusutView data)
        {

            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(AssetTidakSusutView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            FixedAssetMasterView FixedAssetMaster = Lookup.Get<FixedAssetMasterView>();

            var returnpage = new { controller = "AssetTidakSusut", action = "Index", id = FixedAssetMaster.AktivaID.Trim() };

            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    AssetTidakSusut newData = new AssetTidakSusut(0L);
                    // newData.FixedAssetMasterID = data.FixedAssetMasterID;
                    newData.Bulan = data.Bulan;
                    newData.Tahun = data.Tahun;
                    newData.AktivaId = FixedAssetMaster.AktivaID;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _AssetTidakSusutRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToRoute(returnpage);
                //RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(AssetTidakSusutView data, string mode)
        {
            //if (data.FixedAssetMasterID == "")
            //    return FixedAssetMasterResources.Validation_Id;

            if (data.Tahun == "")
                return AssetTidakSusutResources.Validation_Tahun;

            if (data.Bulan == "")
                return AssetTidakSusutResources.Validation_Bulan;


            if (mode == "Create")
            {
                if (_AssetTidakSusutRepository.IsDuplicate(data.RowID))
                    return AssetTidakSusutResources.Validation_DuplicateData;
            }
            return "";
        }



        // GET: Role/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            AssetTidakSusut AssetTidakSusut = _AssetTidakSusutRepository.getAssetTidakSusut(id);
            AssetTidakSusutView data = ConvertFrom(AssetTidakSusut);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(AssetTidakSusutView data)
        {



            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            FixedAssetMasterView FixedAssetMaster = Lookup.Get<FixedAssetMasterView>();

            var returnpage = new { controller = "AssetTidakSusut", action = "Index", id = FixedAssetMaster.AktivaID };


            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    AssetTidakSusut newData = _AssetTidakSusutRepository.getAssetTidakSusut(data.Id);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    // newData.FixedAssetMasterID = data.FixedAssetMasterID;
                    newData.Tahun = data.Tahun;
                    newData.Bulan = data.Bulan;
                    newData.AktivaId = FixedAssetMaster.AktivaID;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _AssetTidakSusutRepository.Save(newData);
                    ViewData["FixedAssetMaster"] = FixedAssetMaster;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                //  return RedirectToRoute(returnpage);
                return RedirectToRoute(returnpage);
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            FixedAssetMasterView FixedAssetMaster = Lookup.Get<FixedAssetMasterView>();

            var returnpage = new { controller = "AssetTidakSusut", action = "Index", id = FixedAssetMaster.AktivaID };

            string message = "";
            try
            {
                AssetTidakSusut newData = _AssetTidakSusutRepository.getAssetTidakSusut(Id);
                // AssetTidakSusut newData = new AssetTidakSusut(Id);  

                _AssetTidakSusutRepository.Remove(newData);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToRoute(returnpage);
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }


    }
}