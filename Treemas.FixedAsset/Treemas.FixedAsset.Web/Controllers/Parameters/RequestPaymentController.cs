﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using System.Globalization;
using System.Transactions;
using System.Web.UI.WebControls;

namespace Treemas.FixedAsset.Controllers
{
    public class RequestPaymentController : PageController
    {

        private IInvoiceRepository _invoiceRepository;
        private IVendorRepository _suppRepository;
        private IVendorAccountRepository _suppAccRepository;
        private IRequestPaymentRepository _reqPaymentRepo;
        private IGroupAssetRepository _astGroupRepo;
        private ISubGroupAssetRepository _subGrpRepo;
        private IBankMasterRepository _bankMasterRepo;
        private IPaymentTypeRepository _paymentTypeRepository;
        private IAuditTrailLog _auditRepository;
        private IFixedAssetMasterRepository _fixedAssetMasterRepository;
        private string _paymentTypeID;

        public RequestPaymentController(ISessionAuthentication sessionAuthentication, IInvoiceRepository invoiceRepository,
            IVendorRepository suppRepository, IVendorAccountRepository suppAccRepository, IRequestPaymentRepository reqPaymentRepo,
            IAuditTrailLog auditRepository, IGroupAssetRepository astGrouprepo, ISubGroupAssetRepository subGrpRepo, IPaymentTypeRepository paymentTypeRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepository, IBankMasterRepository bankMasterRepo) : base(sessionAuthentication)
        {
            this._invoiceRepository = invoiceRepository;
            this._suppRepository = suppRepository;
            this._suppAccRepository = suppAccRepository;
            this._reqPaymentRepo = reqPaymentRepo;
            this._astGroupRepo = astGrouprepo;
            this._subGrpRepo = subGrpRepo;
            this._bankMasterRepo = bankMasterRepo;
            this._paymentTypeRepository = paymentTypeRepository;
            this._auditRepository = auditRepository;
            this._fixedAssetMasterRepository = fixedAssetMasterRepository;

            Settings.ModuleName = "Pengajuan Pembayaran";
            Settings.Title = RequestPaymentResources.PageTitle;
        }

        protected override void Startup()
        {
            ViewData["InvStatus"] = createRequestStatusSelect("N");
        }

        public ActionResult Search()
        {
            string Mode = Request.QueryString.GetValues("Mode")[0];
            string SearchType = Mode == "" ? "Request" : Mode;
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            JsonResult result = new JsonResult();
            try
            {
                if (SearchType == "Request")
                {
                    // Initialization.   
                    RequestPaymentFilter filter = new RequestPaymentFilter();
                    filter.RequestDateFrom = !Request.QueryString["RequestDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("RequestDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.RequestDateTo = !Request.QueryString["RequestDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("RequestDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.VendorId = Request.QueryString.GetValues("VendorId")[0];
                    filter.VendorInvoiceNo = Request.QueryString.GetValues("VendorInvoiceNo")[0];
                    filter.InvoiceStatus = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];

                    string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                    var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                    int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                    int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                    int pageNumber = (startRec + pageSize - 1) / pageSize;

                    // Loading.   
                    PagedResult<RequestPayment> data;
                    data = _reqPaymentRepo.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                    var items = data.Items.ToList().ConvertAll<RequestPaymentView>(new Converter<RequestPayment, RequestPaymentView>(ConvertFrom));

                    result = this.Json(new
                    {
                        draw = Convert.ToInt32(draw),
                        recordsTotal = data.TotalItems,
                        recordsFiltered = data.TotalItems,
                        data = items
                    }, JsonRequestBehavior.AllowGet);
                }
                else if (SearchType == "Invoice")
                {
                    // Initialization.   
                    InvoiceFilter filter = new InvoiceFilter();
                    filter.PlanDisburseDateFrom = !Request.QueryString["PlanDisburseDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("PlanDisburseDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.PlanDisburseDateTo = !Request.QueryString["PlanDisburseDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("PlanDisburseDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    string vendorid = Request.QueryString.GetValues("VendorId")[0];
                    string VendorInvoiceNo = Request.QueryString.GetValues("VendorInvoiceNo")[0];
                    string InvoiceStatus = "A";
                    filter.VendorId = vendorid;
                    filter.VendorInvoiceNo = VendorInvoiceNo;
                    filter.InvoiceStatus = InvoiceStatus;

                    string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                    var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                    int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                    int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                    int pageNumber = (startRec + pageSize - 1) / pageSize;

                    // Loading.   
                    PagedResult<InvoiceH> data;
                    data = _invoiceRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                    var items = data.Items.ToList().ConvertAll<RequestPaymentView>(new Converter<InvoiceH, RequestPaymentView>(ConvertFrom));

                    result = this.Json(new
                    {
                        draw = Convert.ToInt32(draw),
                        recordsTotal = data.TotalItems,
                        recordsFiltered = data.TotalItems,
                        data = items
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public ActionResult SearchInvoice()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                InvoiceFilter filter = new InvoiceFilter();
                filter.PlanDisburseDateFrom = Request.QueryString.GetValues("PlanDisburseDateFrom")[0].ToDateTime();
                filter.PlanDisburseDateTo = Request.QueryString.GetValues("PlanDisburseDateTo")[0].ToDateTime();
                string vendorid = Request.QueryString.GetValues("VendorId")[0];
                string VendorInvoiceNo = Request.QueryString.GetValues("VendorInvoiceNo")[0];
                //string InvoiceStatus = Request.QueryString.GetValues("Status")[0];
                filter.VendorId = vendorid;
                filter.VendorInvoiceNo = VendorInvoiceNo;
                //filter.InvoiceStatus = InvoiceStatus;

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<InvoiceH> data;
                data = _invoiceRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<RequestPaymentView>(new Converter<InvoiceH, RequestPaymentView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        private ViewResult CreateView(InvoiceHView data)
        {
            ViewData["ActionName"] = "Generate";
            return View("InvoiceList", data);
        }

        public RequestPaymentView ConvertFrom(RequestPayment item)
        {
            RequestPaymentView returnItem = new RequestPaymentView();
            PaymentType paytype = _paymentTypeRepository.GetPaymentType(item.CaraBayar);

            returnItem.VendorId = item.VendorId;
            returnItem.VendorName = _suppRepository.getVendor(item.VendorId).VendorName;
            returnItem.InvoiceNo = item.InvoiceNo;
            returnItem.TanggalInvoice = Convert.ToDateTime(item.InvoiceDate).ToString("dd/MM/yyyy");
            returnItem.RequestDate = Convert.ToDateTime(item.RequestDate).ToString("dd/MM/yyyy");
            returnItem.TanggalRencanaBayar = Convert.ToDateTime(item.PaymentOrderDate).ToString("dd/MM/yyyy");
            returnItem.RequestDateString = Convert.ToDateTime(item.RequestDate).ToString("dd/MM/yyyy");
            returnItem.InvoiceDateString = Convert.ToDateTime(item.InvoiceDate).ToString("dd/MM/yyyy");
            returnItem.TanggalJatuhTempo = Convert.ToDateTime(item.PaymentOrderDate).ToString("dd/MM/yyyy");
            returnItem.TanggalJatuhTempoString = Convert.ToDateTime(item.PaymentOrderDate).ToString("dd/MM/yyyy");
            returnItem.VendorInvoiceNo = item.VendorInvoiceNo;
            returnItem.RefferenceNo = item.RefferenceNo;
            returnItem.Notes = item.Notes;
            returnItem.MemoNo = item.MemoNo;
            returnItem.CaraBayar = item.CaraBayar;
            returnItem.CaraBayarString = paytype.IsNull() ? "" : paytype.PaymentTypeName.Trim();
            returnItem.Status = item.Status;
            returnItem.StatusString = item.RequestStatusEnum.ToDescription();
            returnItem.Jumlah = item.Jumlah.ToString("N0");
            returnItem.VendorBankId = item.VendorBankId;
            returnItem.VendorBankAccountName = item.VendorBankAccountName;
            returnItem.VendorBankAccountNo = item.VendorBankAccountNo;
            returnItem.VendorBankBranch = item.VendorBankBranch;
            returnItem.AllowEdit = item.AllowEdit;
            return returnItem;
        }
        public RequestPaymentView ConvertFrom(InvoiceH item)
        {
            RequestPaymentView returnItem = new RequestPaymentView();

            returnItem.VendorId = item.VendorID;
            returnItem.VendorName = _suppRepository.getVendor(item.VendorID).VendorName;

            returnItem.InvoiceNo = item.InvoiceNo;
            returnItem.TanggalInvoice = item.InvoiceDate.ToString("dd/MM/yyyy");
            returnItem.InvoiceDateString = item.InvoiceDate.ToString("dd/MM/yyyy");
            returnItem.TanggalJatuhTempo = item.InvoiceDueDate.ToString("dd/MM/yyyy");
            returnItem.TanggalJatuhTempoString = item.InvoiceDueDate.ToString("dd/MM/yyyy");
            returnItem.TanggalRencanaBayar = item.InvoiceOrderDate.ToString("dd/MM/yyyy");
            returnItem.PaymentOrderDateString = item.InvoiceOrderDate.ToString("dd/MM/yyyy");
            returnItem.VendorInvoiceNo = item.VendorInvoiceNo;
            returnItem.Status = item.Status;
            returnItem.StatusString = item.InvStatusEnum.ToDescription();
            VendorAccount suppAcc = _suppAccRepository.getVendorAccount(item.VendorBankId);
            BankMaster bank = _bankMasterRepo.getBankMaster(suppAcc.VendorBankID);
            returnItem.VendorBankId = bank.ShortName;
            returnItem.VendorBankAccountName = suppAcc.VendorAccountName;
            returnItem.VendorBankAccountNo = suppAcc.VendorAccountNo;
            returnItem.VendorBankBranch = suppAcc.VendorBankBranch;
            returnItem.Jumlah = item.InvoiceAmount.ToString("N0");

            return returnItem;
        }

        private IList<SelectListItem> createPaymentTypeSelect(string selected)
        {
            List<PaymentType> itemList = _paymentTypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<PaymentType, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _paymentTypeID = itemList.FirstOrDefault().PaymentTypeID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString())).Selected = true;
                _paymentTypeID = selected;
            }

            return dataList;
        }

        public SelectListItem ConvertForm(PaymentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.PaymentTypeName.Trim();
            returnItem.Value = item.PaymentTypeID.ToString().Trim();
            return returnItem;
        }

        private IList<SelectListItem> createRequestStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(RequestPaymentEnum)).Cast<RequestPaymentEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }

        private ViewResult CreateView(RequestPaymentView data)
        {
            ViewData["PaymentType"] = createPaymentTypeSelect(data.CaraBayar);
            return View("Request", data);
        }

        // GET: Role/Edit/5
        //[HttpPost]
        public ActionResult Create(string InvoiceNos, string VendorNames)
        {
            ViewData["ActionName"] = "Create";
            string message = "";

            if (InvoiceNos == "")
            {
                message = "Silahkan pilih nomor invoice!";
            }
            else
            {
                //VendorNames = VendorNames.Remove(VendorNames.Length - 1, 1);
                //InvoiceNos = InvoiceNos.Remove(InvoiceNos.Length - 1, 1);
                string[] vendors = VendorNames.Remove(VendorNames.Length - 1, 1).Split(';');
                string[] invoices = InvoiceNos.Remove(InvoiceNos.Length - 1, 1).Split(';');

                var vnd = vendors.Distinct();

                if (vnd.Count() > 1)
                {
                    message = "Hanya invoice dengan vendor yang sama yang bisa dipilih!";
                }
            }

            if (message.IsNullOrEmpty())
            {
                string[] invoices = InvoiceNos.Remove(InvoiceNos.Length - 1, 1).Split(';');
                string VendorInvoiceNos = "";
                double InvoiceAmount = 0;

                foreach (string invoice in invoices)
                {
                    InvoiceH invh = _invoiceRepository.getInvoice(invoice);
                    if (!invh.IsNull())
                    {
                        VendorInvoiceNos += invh.VendorInvoiceNo.Trim() + ";";
                        InvoiceAmount += invh.InvoiceAmount;
                    }
                }

                InvoiceH requestPayment = _invoiceRepository.getInvoice(InvoiceNos.Split(';')[0]);

                RequestPaymentView data = ConvertFrom(requestPayment);
                data.RefferenceNo = InvoiceNos.Remove(InvoiceNos.Length - 1, 1);
                data.Notes = VendorInvoiceNos.Remove(VendorInvoiceNos.Length - 1, 1);
                data.Jumlah = InvoiceAmount.ToString("N0");
                return CreateView(data);
            }

            ScreenMessages.Submit(ScreenMessage.Error(message));
            return RedirectToAction("Generate");
        }

        public ActionResult Generate()
        {
            CollectScreenMessages();
            InvoiceHView data = new InvoiceHView();
            return CreateView(data);
        }


        [HttpPost]
        public ActionResult Create(RequestPaymentView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";

            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    List<RequestPayment> newData = new List<RequestPayment>();
                    List<InvoiceH> newInv = new List<InvoiceH>();
                    
                    foreach (string invoice in data.RefferenceNo.Split(';'))
                    {
                        InvoiceH invoiceH = _invoiceRepository.getInvoice(invoice);
                        List<InvoiceAktiva> invoiceA = _invoiceRepository.getInvoiceAktiva(invoice).ToList();
                        List<InvoiceNonAktiva> invoiceNA = _invoiceRepository.getInvoiceNonAktiva(invoice).ToList();
                        List<InvoiceService> invoiceSrv = _invoiceRepository.getInvoiceService(invoice).ToList();
                        VendorAccount suppAcc = _suppAccRepository.getVendorAccount(invoiceH.VendorBankId);

                        RequestPayment reqAktiva = new RequestPayment("");
                        reqAktiva.InvoiceNo = invoiceH.InvoiceNo.Trim();
                        reqAktiva.VendorInvoiceNo = invoiceH.VendorInvoiceNo.Trim();
                        reqAktiva.VendorId = invoiceH.VendorID;

                        reqAktiva.VendorBankId = suppAcc.VendorBankID;
                        reqAktiva.VendorBankAccountNo = suppAcc.VendorAccountNo;
                        reqAktiva.VendorBankAccountName = suppAcc.VendorAccountName;
                        reqAktiva.VendorBankBranch = suppAcc.VendorBankBranch;
                        reqAktiva.InvoiceDate = invoiceH.InvoiceDate;
                        reqAktiva.PaymentOrderDate = invoiceH.InvoiceOrderDate;
                        reqAktiva.CaraBayar = data.CaraBayar;
                        reqAktiva.Jumlah = invoiceH.InvoiceAmount;
                        reqAktiva.RefferenceNo = invoiceH.InvoiceNo.Trim();
                        reqAktiva.Notes = data.Notes;
                        reqAktiva.MemoNo = data.MemoNo;
                        reqAktiva.RequestDate = DateTime.Now;
                        reqAktiva.PaymentAllocationId = "APVENDOR";
                        reqAktiva.Status = RequestPaymentEnum.N.ToString();
                        newData.Add(reqAktiva);

                        invoiceH.Status = InvoiceStatusEnum.Q.ToString();
                        newInv.Add(invoiceH);

                        //if (invoiceA.Count > 0)
                        //{
                        //    foreach (InvoiceAktiva invA in invoiceA)
                        //    {
                        //        RequestPayment reqAktiva = new RequestPayment("");
                        //        reqAktiva.InvoiceNo = data.InvoiceNo;
                        //        reqAktiva.VendorInvoiceNo = invoiceH.VendorInvoiceNo;
                        //        reqAktiva.VendorId = invoiceH.VendorID;

                        //        reqAktiva.VendorBankId = suppAcc.VendorBankID;
                        //        reqAktiva.VendorBankAccountNo = suppAcc.VendorAccountNo;
                        //        reqAktiva.VendorBankAccountName = suppAcc.VendorAccountName;
                        //        reqAktiva.VendorBankBranch = suppAcc.VendorBankBranch;
                        //        reqAktiva.InvoiceDate = invoiceH.InvoiceDate;
                        //        reqAktiva.PaymentOrderDate = invoiceH.InvoiceOrderDate;
                        //        reqAktiva.CaraBayar = data.CaraBayar;
                        //        reqAktiva.Jumlah = invA.TotalAmount;
                        //        reqAktiva.RefferenceNo = data.RefferenceNo;
                        //        reqAktiva.Notes = data.Notes;
                        //        reqAktiva.RequestDate = DateTime.Now;
                        //        FixedAssetMaster Fam = _fixedAssetMasterRepository.getFixedAssetMaster(invA.StartingAktivaId);
                        //        SubGroupAsset subGrp = _subGrpRepo.getSubGroupAsset(Fam.SubGroupAssetID);
                        //        GroupAsset group = _astGroupRepo.getGroupAsset(Fam.GroupAssetID);
                        //        reqAktiva.PaymentAllocationId = "APVENDOR";
                        //        reqAktiva.Status = RequestPaymentEnum.N.ToString();
                        //        newData.Add(reqAktiva);
                        //    }
                        //}

                        //if (invoiceNA.Count > 0)
                        //{
                        //    foreach (InvoiceNonAktiva invNA in invoiceNA)
                        //    {
                        //        RequestPayment reqNonAktiva = new RequestPayment("");
                        //        reqNonAktiva.InvoiceNo = data.InvoiceNo;
                        //        reqNonAktiva.VendorInvoiceNo = invoiceH.VendorInvoiceNo;
                        //        reqNonAktiva.VendorId = invoiceH.VendorID;

                        //        reqNonAktiva.VendorBankId = suppAcc.VendorBankID;
                        //        reqNonAktiva.VendorBankAccountNo = suppAcc.VendorAccountNo;
                        //        reqNonAktiva.VendorBankAccountName = suppAcc.VendorAccountName;
                        //        reqNonAktiva.VendorBankBranch = suppAcc.VendorBankBranch;
                        //        reqNonAktiva.InvoiceDate = invoiceH.InvoiceDate;
                        //        reqNonAktiva.PaymentOrderDate = invoiceH.InvoiceOrderDate;
                        //        reqNonAktiva.CaraBayar = data.CaraBayar;
                        //        reqNonAktiva.Jumlah = invNA.TotalAmount;
                        //        reqNonAktiva.RefferenceNo = data.RefferenceNo;
                        //        reqNonAktiva.RequestDate = DateTime.Now;
                        //        reqNonAktiva.Notes = data.Notes;
                        //        if (invNA.ExpenseType == "L")
                        //        {
                        //            reqNonAktiva.PaymentAllocationId = invNA.ExpensePA;
                        //        }
                        //        if (invNA.ExpenseType == "A")
                        //        {
                        //            reqNonAktiva.PaymentAllocationId = invNA.PrepaidPA;
                        //        }
                        //        reqNonAktiva.Status = RequestPaymentEnum.N.ToString();
                        //        newData.Add(reqNonAktiva);
                        //    }
                        //}

                        //if (invoiceSrv.Count > 0)
                        //{
                        //    foreach (InvoiceService invSrv in invoiceSrv)
                        //    {
                        //        RequestPayment reqSrv = new RequestPayment("");
                        //        reqSrv.InvoiceNo = data.InvoiceNo;
                        //        reqSrv.VendorInvoiceNo = invoiceH.VendorInvoiceNo;
                        //        reqSrv.VendorId = invoiceH.VendorID;

                        //        reqSrv.VendorBankId = suppAcc.VendorBankID;
                        //        reqSrv.VendorBankAccountNo = suppAcc.VendorAccountNo;
                        //        reqSrv.VendorBankAccountName = suppAcc.VendorAccountName;
                        //        reqSrv.VendorBankBranch = suppAcc.VendorBankBranch;
                        //        reqSrv.InvoiceDate = invoiceH.InvoiceDate;
                        //        reqSrv.PaymentOrderDate = invoiceH.InvoiceOrderDate;
                        //        reqSrv.CaraBayar = data.CaraBayar;
                        //        reqSrv.Jumlah = invSrv.TotalAmount;
                        //        reqSrv.RefferenceNo = data.RefferenceNo;
                        //        reqSrv.Notes = data.Notes;
                        //        reqSrv.PaymentAllocationId = "APVENDOR";
                        //        reqSrv.Status = RequestPaymentEnum.N.ToString();
                        //        newData.Add(reqSrv);
                        //    }
                        //}
                    }


                    using (TransactionScope scope = new TransactionScope())
                    {
                        foreach (RequestPayment req in newData)
                        {
                            _reqPaymentRepo.Add(req);
                        }
                        foreach(InvoiceH inv in newInv)
                        {
                            _invoiceRepository.Save(inv);
                        }
                        scope.Complete();
                    }
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Save_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            RequestPayment Item = _reqPaymentRepo.getRequest(id);
            RequestPaymentView data = ConvertFrom(Item);

            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(RequestPaymentView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";

            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    foreach (string invoice in data.RefferenceNo.Split(';'))
                    {
                        //RequestPayment newData = _reqPaymentRepo.getRequest(invoice);
                        InvoiceH invoiceH = _invoiceRepository.getInvoice(invoice);
                        List<InvoiceAktiva> invoiceA = _invoiceRepository.getInvoiceAktiva(invoice).ToList();
                        List<InvoiceNonAktiva> invoiceNA = _invoiceRepository.getInvoiceNonAktiva(invoice).ToList();
                        List<InvoiceService> invoiceSrv = _invoiceRepository.getInvoiceService(invoice).ToList();
                        VendorAccount suppAcc = _suppAccRepository.getVendorAccount(invoiceH.VendorBankId);
                        List<RequestPayment> newData = new List<RequestPayment>();

                        RequestPayment reqAktiva = new RequestPayment("");
                        reqAktiva.InvoiceNo = invoiceH.InvoiceNo.Trim();
                        reqAktiva.VendorInvoiceNo = invoiceH.VendorInvoiceNo.Trim();
                        reqAktiva.VendorId = invoiceH.VendorID;

                        reqAktiva.VendorBankId = suppAcc.VendorBankID;
                        reqAktiva.VendorBankAccountNo = suppAcc.VendorAccountNo;
                        reqAktiva.VendorBankAccountName = suppAcc.VendorAccountName;
                        reqAktiva.VendorBankBranch = suppAcc.VendorBankBranch;
                        reqAktiva.InvoiceDate = invoiceH.InvoiceDate;
                        reqAktiva.PaymentOrderDate = invoiceH.InvoiceOrderDate;
                        reqAktiva.CaraBayar = data.CaraBayar;
                        reqAktiva.Jumlah = invoiceH.InvoiceAmount;
                        reqAktiva.RefferenceNo = invoiceH.InvoiceNo.Trim();
                        reqAktiva.Notes = invoiceH.Notes;
                        reqAktiva.MemoNo = data.MemoNo;
                        reqAktiva.RequestDate = DateTime.Now;
                        reqAktiva.PaymentAllocationId = "APVENDOR";
                        reqAktiva.Status = RequestPaymentEnum.N.ToString();
                        newData.Add(reqAktiva);

                        //if (invoiceA.Count > 0)
                        //{
                        //    foreach (InvoiceAktiva invA in invoiceA)
                        //    {
                        //        RequestPayment reqAktiva = new RequestPayment("");
                        //        reqAktiva.InvoiceNo = data.InvoiceNo;
                        //        reqAktiva.VendorInvoiceNo = invoiceH.VendorInvoiceNo;
                        //        reqAktiva.VendorId = invoiceH.VendorID;

                        //        reqAktiva.VendorBankId = suppAcc.VendorBankID;
                        //        reqAktiva.VendorBankAccountNo = suppAcc.VendorAccountNo;
                        //        reqAktiva.VendorBankAccountName = suppAcc.VendorAccountName;
                        //        reqAktiva.VendorBankBranch = suppAcc.VendorBankBranch;
                        //        reqAktiva.InvoiceDate = invoiceH.InvoiceDate;
                        //        reqAktiva.PaymentOrderDate = invoiceH.InvoiceOrderDate;
                        //        reqAktiva.CaraBayar = data.CaraBayar;
                        //        reqAktiva.Jumlah = invA.TotalAmount;
                        //        reqAktiva.RefferenceNo = data.RefferenceNo;
                        //        reqAktiva.Notes = data.Notes;
                        //        reqAktiva.RequestDate = DateTime.Now;
                        //        FixedAssetMaster Fam = _fixedAssetMasterRepository.getFixedAssetMaster(invA.StartingAktivaId);
                        //        SubGroupAsset subGrp = _subGrpRepo.getSubGroupAsset(Fam.SubGroupAssetID);
                        //        GroupAsset group = _astGroupRepo.getGroupAsset(Fam.GroupAssetID);
                        //        reqAktiva.PaymentAllocationId = group.PaymentAllocation;
                        //        reqAktiva.Status = RequestPaymentEnum.N.ToString();
                        //        newData.Add(reqAktiva);
                        //    }
                        //}

                        //if (invoiceNA.Count > 0)
                        //{
                        //    foreach (InvoiceNonAktiva invNA in invoiceNA)
                        //    {
                        //        RequestPayment reqNonAktiva = new RequestPayment("");
                        //        reqNonAktiva.InvoiceNo = data.InvoiceNo;
                        //        reqNonAktiva.VendorInvoiceNo = invoiceH.VendorInvoiceNo;
                        //        reqNonAktiva.VendorId = invoiceH.VendorID;

                        //        reqNonAktiva.VendorBankId = suppAcc.VendorBankID;
                        //        reqNonAktiva.VendorBankAccountNo = suppAcc.VendorAccountNo;
                        //        reqNonAktiva.VendorBankAccountName = suppAcc.VendorAccountName;
                        //        reqNonAktiva.VendorBankBranch = suppAcc.VendorBankBranch;
                        //        reqNonAktiva.InvoiceDate = invoiceH.InvoiceDate;
                        //        reqNonAktiva.PaymentOrderDate = invoiceH.InvoiceOrderDate;
                        //        reqNonAktiva.CaraBayar = data.CaraBayar;
                        //        reqNonAktiva.Jumlah = invNA.TotalAmount;
                        //        reqNonAktiva.RefferenceNo = data.RefferenceNo;
                        //        reqNonAktiva.RequestDate = DateTime.Now;
                        //        reqNonAktiva.Notes = data.Notes;
                        //        if (invNA.ExpenseType == "L")
                        //        {
                        //            reqNonAktiva.PaymentAllocationId = invNA.ExpensePA;
                        //        }
                        //        if (invNA.ExpenseType == "A")
                        //        {
                        //            reqNonAktiva.PaymentAllocationId = invNA.PrepaidPA;
                        //        }
                        //        reqNonAktiva.Status = RequestPaymentEnum.N.ToString();
                        //        newData.Add(reqNonAktiva);
                        //    }
                        //}

                        //if (invoiceSrv.Count > 0)
                        //{
                        //    foreach (InvoiceService invSrv in invoiceSrv)
                        //    {
                        //        RequestPayment reqSrv = new RequestPayment("");
                        //        reqSrv.InvoiceNo = data.InvoiceNo;
                        //        reqSrv.VendorInvoiceNo = invoiceH.VendorInvoiceNo;
                        //        reqSrv.VendorId = invoiceH.VendorID;

                        //        reqSrv.VendorBankId = suppAcc.VendorBankID;
                        //        reqSrv.VendorBankAccountNo = suppAcc.VendorAccountNo;
                        //        reqSrv.VendorBankAccountName = suppAcc.VendorAccountName;
                        //        reqSrv.VendorBankBranch = suppAcc.VendorBankBranch;
                        //        reqSrv.InvoiceDate = invoiceH.InvoiceDate;
                        //        reqSrv.PaymentOrderDate = invoiceH.InvoiceOrderDate;
                        //        reqSrv.CaraBayar = data.CaraBayar;
                        //        reqSrv.Jumlah = invSrv.TotalAmount;
                        //        reqSrv.RefferenceNo = data.RefferenceNo;
                        //        reqSrv.Notes = data.Notes;
                        //        reqSrv.PaymentAllocationId = invSrv.ExpensePA;
                        //        reqSrv.Status = RequestPaymentEnum.N.ToString();
                        //        newData.Add(reqSrv);
                        //    }
                        //}

                        using (TransactionScope scope = new TransactionScope())
                        {
                            foreach (RequestPayment req in newData)
                            {
                                _reqPaymentRepo.Save(req);
                            }
                            scope.Complete();
                        }
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Edit");
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Update_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }
        private string validateData(RequestPaymentView data, string mode)
        {
            if (data.VendorId == "")
                return FixedAssetMasterResources.Validation_Id;

            if (data.NoInvoice == "")
                return FixedAssetMasterResources.Validation_Nama;
            if (data.CaraBayar.IsNull() || data.CaraBayar == "")
                return "Cara Bayar harus dipilih!";

            if (mode == "Create")
            {
                if (_reqPaymentRepo.IsDuplicate(data.InvoiceNo))
                    return "Invoice No Is Already Exist";
            }
            return "";
        }

    }
}