﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.FixedAsset.Controllers
{
    public class GroupAssetController : PageController
    {
       
        private IGroupAssetRepository _groupAssetRepository;
        private IGolonganPajakRepository _golonganPajakRepository;

        private IAuditTrailLog _auditRepository;
        public GroupAssetController(ISessionAuthentication sessionAuthentication, 
            IGroupAssetRepository groupAssetRepository,IGolonganPajakRepository golonganPajakRepository,IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._golonganPajakRepository = golonganPajakRepository;
            this._groupAssetRepository = groupAssetRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Group Asset";
            Settings.Title = GroupAssetResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<GroupAsset> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _groupAssetRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _groupAssetRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<GroupAssetView>(new Converter<GroupAsset, GroupAssetView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public ActionResult NoAuthCheckSearch()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                //User user = Lookup.Get<User>();
                //var company = Lookup.Get<Company>();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                //string siteId = user.Site.SiteId;
                //string companyId = company.CompanyId;

                // Loading.   
                PagedResult<GroupAsset> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _groupAssetRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _groupAssetRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<GroupAssetView>(new Converter<GroupAsset, GroupAssetView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public GroupAssetView ConvertFrom(GroupAsset item)
        {
            GroupAssetView returnItem = new GroupAssetView();

            returnItem.GroupAssetID = item.GroupAssetID;
            returnItem.GroupAssetDescription = item.GroupAssetDescription;
            returnItem.GroupAssetShortDesc = item.GroupAssetShortDesc;
            returnItem.CoaPerolehan = item.CoaPerolehan;
            returnItem.CoaAkumulasi = item.CoaAkumulasi;
            returnItem.CoaBeban = item.CoaBeban;
            returnItem.PaymentAllocation = item.PaymentAllocation;         
            returnItem.Status = item.Status;
            return returnItem;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            GroupAssetView data = new GroupAssetView();
            return CreateView(data);
        }

        private ViewResult CreateView(GroupAssetView data)
        {
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(GroupAssetView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    GroupAsset newData = new GroupAsset("");

                    newData.GroupAssetID = data.GroupAssetID;
                    newData.GroupAssetDescription = data.GroupAssetDescription;
                    newData.GroupAssetShortDesc = data.GroupAssetShortDesc;
                    newData.CoaPerolehan = data.CoaPerolehan;
                    newData.CoaAkumulasi = data.CoaAkumulasi;
                    newData.CoaBeban = data.CoaBeban;
                    newData.PaymentAllocation = data.PaymentAllocation;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _groupAssetRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(GroupAssetView data, string mode)
        {
            if (data.GroupAssetID == "")
                return GroupAssetResources.Validation_Id;

            if (data.GroupAssetDescription == "")
                return GroupAssetResources.Validation_Description;

            //if (data.CoaPerolehan == "")
            //    return GroupAssetResources.Validation_Perolehan;

            //if (data.CoaAkumulasi.Trim().IsNullOrEmpty())
            //    return GroupAssetResources.Validation_Akumulasi;

            //if (data.CoaBeban.Trim().IsNullOrEmpty())
            //    return GroupAssetResources.Validation_Beban;

            if (data.Status.IsNull())
                return GroupAssetResources.Validation_Status;

            if (mode == "Create")
            {
                if (_groupAssetRepository.IsDuplicate(data.GroupAssetID))
                    return GroupAssetResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            GroupAsset subgroup = _groupAssetRepository.getGroupAsset(id);
            GroupAssetView data = ConvertFrom(subgroup);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(GroupAssetView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    GroupAsset newData = _groupAssetRepository.getGroupAsset(data.GroupAssetID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.GroupAssetID = data.GroupAssetID;
                    newData.GroupAssetDescription = data.GroupAssetDescription;
                    newData.GroupAssetShortDesc = data.GroupAssetID;
                    newData.CoaPerolehan = data.CoaPerolehan;
                    newData.CoaAkumulasi = data.CoaAkumulasi;                
                    newData.CoaBeban = data.CoaBeban;
                    newData.PaymentAllocation = data.PaymentAllocation;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _groupAssetRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                GroupAsset newData = _groupAssetRepository.getGroupAsset(Id);
                _groupAssetRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}