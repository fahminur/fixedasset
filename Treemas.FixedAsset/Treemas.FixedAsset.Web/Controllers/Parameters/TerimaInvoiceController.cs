﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;

using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using System.Globalization;

namespace Treemas.FixedAsset.Controllers
{
    public class TerimaInvoiceController : PageController
    {

        private ITerimaInvoiceHRepository _terimaInvoiceHRepository;
        private IVendorBankRepository _vendorBankRepository;
        private IVendorRepository _vendorRepository;

        private IAuditTrailLog _auditRepository;
        public TerimaInvoiceController(ISessionAuthentication sessionAuthentication, ITerimaInvoiceHRepository terimaInvoiceHRepository, IVendorBankRepository vendorBankRepository, IVendorRepository vendorRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._terimaInvoiceHRepository = terimaInvoiceHRepository;
            this._vendorBankRepository = vendorBankRepository;
            this._vendorRepository = vendorRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Terima Invoice";
            Settings.Title = TerimaInvoiceHResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<TerimaInvoiceH> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _terimaInvoiceHRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _terimaInvoiceHRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<TerimaInvoiceHView>(new Converter<TerimaInvoiceH, TerimaInvoiceHView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public TerimaInvoiceHView ConvertFrom(TerimaInvoiceH item)
        {
            TerimaInvoiceHView returnItem = new TerimaInvoiceHView();

            returnItem.Id = item.Id;
            returnItem.VendorId = item.VendorId;
            returnItem.NoInvoice = item.NoInvoice;
            returnItem.TanggalInvoice = item.TanggalInvoice;
            returnItem.TanggalJatuhTempo = item.TanggalJatuhTempo;
            returnItem.TanggalRencanaBayar = item.TanggalRencanaBayar;
            returnItem.VendorBankId = item.VendorBankId;
            returnItem.TotalInvoice = item.TotalInvoice;
            returnItem.PPn = item.PPn;
            returnItem.PPh23 = item.PPh23;
            returnItem.TotalBayar = item.TotalBayar;
            returnItem.Catatan = item.Catatan;
            returnItem.Status = item.Status;
            returnItem.CreateDate = item.CreateDate;
            returnItem.TanggalInvoiceString = DateTime.Parse(item.TanggalInvoice.ToString(), CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
            returnItem.TanggalJatuhTempoString = DateTime.Parse(item.TanggalJatuhTempo.ToString(), CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
            returnItem.CreateDateString = DateTime.Parse(item.CreateDate.ToString(), CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
            return returnItem;
        }

        public JsonResult NoAuthCheckVendor(long vendorId)
        {
            List<VendorBank> itemList = null;
            itemList = _vendorBankRepository.getBankByVendor(vendorId).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<VendorBank, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public IList<SelectListItem> createJenisInvoiceSelect()
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(JenisInvoiceEnum)).Cast<JenisInvoiceEnum>().Select(x => new SelectListItem { Text = x.ToDescription(), Value = ((int)x).ToString() }).ToList();

            return dataList;
        }

        public JsonResult NoAuthCheckVendorBank(long VendorBankId)
        {
            VendorBank vendorBank = _vendorBankRepository.getVendorBank(VendorBankId);

            List<object> objectList = new List<object>();

            objectList.Add(new { BankAccountNo = vendorBank.BankAccountNo, BankAccountName = vendorBank.BankAccountName, BankBranch = vendorBank.BankBranch });

            return Json(objectList, JsonRequestBehavior.AllowGet);
        }

        //================================================Vendor Bank==============================================================
        private IList<SelectListItem> createVendorBankSelect(long selected)
        {
            List<VendorBank> itemList = Lookup.Get<List<VendorBank>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _vendorBankRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<VendorBank, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });
            if (selected != 0)
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(VendorBank item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BankName;
            returnItem.Value = item.Id.ToString();
            return returnItem;
        }
        //=======================================================end==============================================================

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            ViewData["JenisInvoiceList"] = createJenisInvoiceSelect();
            TerimaInvoiceHView data = new TerimaInvoiceHView();
            Lookup.Remove<TerimaInvoiceHView>();
            return CreateView(data);
        }

        private ViewResult CreateView(TerimaInvoiceHView data)
        {
            ViewData["VendorBankList"] = createVendorBankSelect(data.VendorBankId);

            if (Lookup.Get<TerimaInvoiceHView>() != null)
            {
                data = Lookup.Get<TerimaInvoiceHView>();
                if (!data.Detail.IsNullOrEmpty())
                {
                    var newlist = data.Detail.OrderBy(x => x.SeqNo);
                    int count = newlist.Count();
                    int lastSeq = newlist.Last().SeqNo;
                    ViewData["Seq"] = lastSeq + 1;
                }
                else
                {
                    ViewData["Seq"] = data.Detail.Count;
                }
            }
            else
            {
                ViewData["Seq"] = data.Detail.Count;
            }

            return View("Detail", data);
        }

        public ActionResult Add(TerimaInvoiceHView invoiceH, int index)
        {
            ViewData["ActionName"] = "NoAuthCheckAdd";
            ViewData["JenisInvoiceList"] = createJenisInvoiceSelect();
            ViewData["SeqNo"] = index;
            TerimaInvoiceDView data = new TerimaInvoiceDView();
            if (index == 0)
            {
                Lookup.Remove<TerimaInvoiceHView>();
                Lookup.Add(invoiceH);
            }
            else
            {
                TerimaInvoiceHView lookupPOH = Lookup.Get<TerimaInvoiceHView>();
                TerimaInvoiceHView poH = new TerimaInvoiceHView();
                poH = invoiceH;
                poH.Detail = lookupPOH.Detail;
                poH.Aktiva = lookupPOH.Aktiva;
                poH.NonAktiva = lookupPOH.NonAktiva;
                poH.Perawatan = lookupPOH.Perawatan;

                Lookup.Remove<TerimaInvoiceHView>();
                Lookup.Add(invoiceH);
            }
            //data.DeliveryDate = DateTime.Now;
            return CreateViewDetail(data);
        }

        private ViewResult CreateViewDetail(TerimaInvoiceDView data)
        {
            return View("DetailInvoice", data);
        }

        [HttpPost]
        public ActionResult NoAuthCheckAddAktiva(TerimaInvoiceAktivaView detail)
        {
            TerimaInvoiceHView invoiceView = Lookup.Get<TerimaInvoiceHView>();
            TerimaInvoiceDView invoiceDView = new TerimaInvoiceDView();
            invoiceDView.VendorId = invoiceView.VendorId;
            invoiceDView.NoInvoice = invoiceView.NoInvoice;
            invoiceDView.SeqNo = detail.ASeqNo;
            invoiceDView.AktivaIDNoReff = detail.AktivaID;
            invoiceDView.JenisInvoice = "Aktiva";
            invoiceDView.NamaBarangJasa = detail.AktivaName;
            invoiceDView.Jumlah = detail.AktivaQty;
            invoiceDView.Satuan = detail.AktivaUOM;
            invoiceDView.MataUang = detail.MataUang;
            invoiceDView.HargaSatuan = detail.HargaPerolehan;
            invoiceDView.Nilai = invoiceDView.HargaSatuan * invoiceDView.Jumlah;
            invoiceDView.PPn = detail.AktivaPPn;
            invoiceDView.PPh23 = detail.AktivaPPh23;

            ViewData["Seq"] = invoiceDView.SeqNo + 1;
            ViewData["ActionName"] = "Create";
            invoiceView.Aktiva.Add(detail);
            invoiceView.Detail.Add(invoiceDView);
            return CreateView(invoiceView);
        }

        [HttpPost]
        public ActionResult NoAuthCheckAddNonAktiva(TerimaInvoiceNonAktivaView detail)
        {
            TerimaInvoiceHView invoiceView = Lookup.Get<TerimaInvoiceHView>();
            TerimaInvoiceDView invoiceDView = new TerimaInvoiceDView();
            invoiceDView.VendorId = invoiceView.VendorId;
            invoiceDView.NoInvoice = invoiceView.NoInvoice;
            invoiceDView.SeqNo = detail.NSeqNo;
            invoiceDView.AktivaIDNoReff = detail.NoRefference;
            invoiceDView.JenisInvoice = "NonAktiva";
            invoiceDView.NamaBarangJasa = detail.NamaBarangJasa;
            invoiceDView.Jumlah = 0;
            invoiceDView.Satuan = "";
            invoiceDView.MataUang = "";
            invoiceDView.HargaSatuan = detail.BebanPerBulan;
            invoiceDView.Nilai = detail.BebanPerBulan;
            invoiceDView.PPn = 10;
            invoiceDView.PPh23 = 2;

            ViewData["Seq"] = invoiceDView.SeqNo++;
            ViewData["ActionName"] = "Create";
            invoiceView.NonAktiva.Add(detail);
            invoiceView.Detail.Add(invoiceDView);
            return CreateView(invoiceView);
        }

        [HttpPost]
        public ActionResult NoAuthCheckAddPerawatan(TerimaInvoicePerawatanView detail)
        {
            TerimaInvoiceHView invoiceView = Lookup.Get<TerimaInvoiceHView>();
            TerimaInvoiceDView invoiceDView = new TerimaInvoiceDView();
            invoiceDView.VendorId = invoiceView.VendorId;
            invoiceDView.NoInvoice = invoiceView.NoInvoice;
            invoiceDView.SeqNo = detail.PSeqNo;
            invoiceDView.AktivaIDNoReff = detail.PAktivaID;
            invoiceDView.JenisInvoice = "Perawatan";
            invoiceDView.NamaBarangJasa = detail.PAktivaName;
            invoiceDView.Jumlah = detail.PerawatanQty;
            invoiceDView.Satuan = detail.PerawatanUOM;
            invoiceDView.MataUang = detail.MataUang;
            invoiceDView.HargaSatuan = detail.NilaiPerawatan;
            invoiceDView.Nilai = invoiceDView.HargaSatuan * invoiceDView.Jumlah;
            invoiceDView.PPn = detail.PerawatanPPn;
            invoiceDView.PPh23 = detail.PerawatanPPh23;

            ViewData["Seq"] = invoiceDView.SeqNo++;
            ViewData["ActionName"] = "Create";
            invoiceView.Perawatan.Add(detail);
            invoiceView.Detail.Add(invoiceDView);
            return CreateView(invoiceView);
        }

        [HttpPost]
        public ActionResult NoAuthCheckDeleteRow(int seqno)
        {
            TerimaInvoiceHView invoiceView = Lookup.Get<TerimaInvoiceHView>();
            ViewData["ActionName"] = "Create";

            invoiceView.Detail.Remove(invoiceView.Detail.FindElement(x => x.SeqNo == seqno));
            invoiceView.Aktiva.Remove(invoiceView.Aktiva.FindElement(x => x.ASeqNo == seqno));
            invoiceView.NonAktiva.Remove(invoiceView.NonAktiva.FindElement(x => x.NSeqNo == seqno));
            invoiceView.Perawatan.Remove(invoiceView.Perawatan.FindElement(x => x.PSeqNo == seqno));

            Lookup.Remove<TerimaInvoiceHView>();
            Lookup.Add(invoiceView);

            return CreateView(invoiceView);
        }

        public ActionResult NoAuthCheckBack()
        {
            TerimaInvoiceHView invoiceView = Lookup.Get<TerimaInvoiceHView>();
            ViewData["ActionName"] = "Create";

            return CreateView(invoiceView);
        }

        [HttpPost]
        public ActionResult Create(TerimaInvoiceHView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            TerimaInvoiceHView invoiceView = Lookup.Get<TerimaInvoiceHView>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    TerimaInvoiceH newData = new TerimaInvoiceH(0L);
                    newData.VendorId = data.VendorId;
                    newData.NoInvoice = data.NoInvoice;
                    newData.TanggalInvoice = data.TanggalInvoice;
                    newData.TanggalJatuhTempo = data.TanggalJatuhTempo;
                    newData.TanggalRencanaBayar = data.TanggalRencanaBayar;
                    newData.Catatan = data.Catatan;
                    newData.VendorBankId = data.VendorBankId;
                    newData.TotalInvoice = data.TotalInvoice;
                    newData.PPh23 = data.PPh23;
                    newData.PPn = data.PPn;
                    newData.TotalBayar = data.TotalBayar;
                    newData.Status = true;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;

                    newData.Detail = new List<TerimaInvoiceD>();
                    newData.Aktiva = new List<TerimaInvoiceAktiva>();
                    newData.NonAktiva = new List<TerimaInvoiceNonAktiva>();
                    newData.Perawatan = new List<TerimaInvoicePerawatan>();

                    foreach (TerimaInvoiceAktivaView item in invoiceView.Aktiva)
                    {
                        TerimaInvoiceAktiva newItem = new TerimaInvoiceAktiva(0L);

                        newItem.VendorId = newData.VendorId;
                        newItem.NoInvoice = newData.NoInvoice;
                        newItem.SeqNo = item.ASeqNo;
                        newItem.AktivaID = item.AktivaID;
                        newItem.NoPO = item.NoPO;
                        newItem.NoDO = item.NoDO;
                        newItem.NoPenerimaan = item.NoPenerimaan;
                        newItem.MataUang = item.MataUang;
                        newItem.NilaiOriginal = item.NilaiOriginal;
                        newItem.Kurs = item.Kurs;
                        newItem.HargaPerolehan = item.HargaPerolehan;
                        newItem.Keterangan = item.Keterangan;

                        newItem.UpdateUser = user.Username;
                        newItem.UpdateDate = DateTime.Now;
                        newItem.CreateUser = user.Username;
                        newItem.CreateDate = DateTime.Now;
                        newData.Aktiva.Add(newItem);
                    }

                    foreach (TerimaInvoiceNonAktivaView item in invoiceView.NonAktiva)
                    {
                        TerimaInvoiceNonAktiva newItem = new TerimaInvoiceNonAktiva(0L);

                        newItem.VendorId = newData.VendorId;
                        newItem.NoInvoice = newData.NoInvoice;
                        newItem.SeqNo = item.NSeqNo;
                        newItem.NoRefference = item.NoRefference;
                        newItem.NamaBarangJasa = item.NamaBarangJasa;
                        newItem.Nilai = item.Nilai;
                        newItem.CaraPembebanan = item.CaraPembebanan;
                        newItem.JangkaWaktu = item.JangkaWaktu;
                        newItem.StartDate = item.StartDate;
                        newItem.EndDate = item.EndDate;
                        newItem.BebanPerBulan = item.BebanPerBulan;
                        newItem.COABiaya = item.COABiaya;
                        newItem.COAPrepaid = item.COAPrepaid;
                        newItem.NoPO = item.NoPO;
                        newItem.NoDO = item.NoDO;
                        newItem.NoPenerimaan = item.NoPenerimaan;
                        newItem.Keterangan = item.Keterangan;

                        newItem.UpdateUser = user.Username;
                        newItem.UpdateDate = DateTime.Now;
                        newItem.CreateUser = user.Username;
                        newItem.CreateDate = DateTime.Now;
                        newData.NonAktiva.Add(newItem);
                    }

                    foreach (TerimaInvoicePerawatanView item in invoiceView.Perawatan)
                    {
                        TerimaInvoicePerawatan newItem = new TerimaInvoicePerawatan(0L);

                        newItem.VendorId = newData.VendorId;
                        newItem.NoInvoice = newData.NoInvoice;
                        newItem.SeqNo = item.PSeqNo;
                        newItem.AktivaID = item.PAktivaID;
                        newItem.COABiaya = item.COABiaya;
                        newItem.NoPO = item.NoPO;
                        newItem.NoSPK = item.NoSPK;
                        newItem.TanggalPerawatan = item.TanggalPerawatan;
                        newItem.MataUang = item.MataUang;
                        newItem.NilaiOriginal = item.NilaiOriginal;
                        newItem.Kurs = item.Kurs;
                        newItem.NilaiPerawatan = item.NilaiPerawatan;
                        newItem.Keterangan = item.Keterangan;

                        newItem.UpdateUser = user.Username;
                        newItem.UpdateDate = DateTime.Now;
                        newItem.CreateUser = user.Username;
                        newItem.CreateDate = DateTime.Now;
                        newData.Perawatan.Add(newItem);
                    }

                    foreach (TerimaInvoiceDView item in invoiceView.Detail)
                    {
                        TerimaInvoiceD newItem = new TerimaInvoiceD(0L);

                        newItem.VendorId = newData.VendorId;
                        newItem.NoInvoice = newData.NoInvoice;
                        newItem.AktivaIDNoReff = item.AktivaIDNoReff;
                        newItem.SeqNo = item.SeqNo;
                        newItem.JenisInvoice = item.JenisInvoice;
                        newItem.NamaBarangJasa = item.NamaBarangJasa;
                        newItem.Jumlah = item.Jumlah;
                        newItem.Satuan = item.Satuan;
                        newItem.MataUang = item.MataUang;
                        newItem.HargaSatuan = item.HargaSatuan;
                        newItem.Nilai = item.Nilai;
                        newItem.PPh23 = item.PPh23;
                        newItem.PPn = item.PPn;

                        newItem.UpdateUser = user.Username;
                        newItem.UpdateDate = DateTime.Now;
                        newItem.CreateUser = user.Username;
                        newItem.CreateDate = DateTime.Now;
                        newData.Detail.Add(newItem);
                    }

                    _terimaInvoiceHRepository.Add(newData);
                    foreach (TerimaInvoiceAktiva item in newData.Aktiva)
                    {
                        _terimaInvoiceHRepository.AddAktiva(item);
                    }
                    foreach (TerimaInvoiceNonAktiva item in newData.NonAktiva)
                    {
                        _terimaInvoiceHRepository.AddNonAktiva(item);
                    }
                    foreach (TerimaInvoicePerawatan item in newData.Perawatan)
                    {
                        _terimaInvoiceHRepository.AddPerawatan(item);
                    }
                    //if (message.Trim().IsNullOrEmpty())
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();

            return CreateView(data);
        }

        private string validateData(TerimaInvoiceHView data, string mode)
        {
            return string.Empty;
        }
    }
}