﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.FixedAsset.Web.Controllers
{
    public class GeneralReferenceController : PageController
    {
        private IAssetBranchRepository _assetBranchRepository;
        private IAssetConditionRepository _assetConditionRepository;
        private IAssetDeptRepository _assetDeptRepository;
        private IAssetLocationRepository _assetLocationRepository;
        private IAssetUserRepository _assetUserRepository;
        private IJenisUsahaRepository _jenisUsahaRepository;


        private IAuditTrailLog _auditRepository;
        public GeneralReferenceController(ISessionAuthentication sessionAuthentication,
            IAssetBranchRepository assetBranchRepository, IAssetConditionRepository assetConditionRepository, IAssetDeptRepository assetDeptRepository,
            IAssetLocationRepository assetLocationRepository, IAssetUserRepository assetUserRepository, IJenisUsahaRepository jenisUsahaRepository,
            IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._assetBranchRepository = assetBranchRepository;
            this._assetConditionRepository = assetConditionRepository;
            this._assetDeptRepository = assetDeptRepository;
            this._assetLocationRepository = assetLocationRepository;
            this._assetUserRepository = assetUserRepository;
            this._jenisUsahaRepository = jenisUsahaRepository;


            this._auditRepository = auditRepository;
            Settings.ModuleName = "GeneralReference";
            Settings.Title = GeneralReferenceResources.PageTitle;
        }

        protected override void Startup()
        {
            ViewData["RefTableList"] = createRefTableSelect();
        }

        private IList<SelectListItem> createRefTableSelect()
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(ReferenceTable)).Cast<ReferenceTable>().Select(x => new SelectListItem { Text = x.ToDescription(), Value = ((int)x).ToString() }).ToList();

            return dataList;
        }

        //GET: Function/Show
        public ActionResult Show(string tablename)
        {
            var returnpage = new { controller = "GeneralReference", action = "Index" };
            TableHelperView refTable = Lookup.Get<TableHelperView>();
            if (tablename.IsNullOrEmpty() && !refTable.Table.IsNullOrEmpty())
            {
                tablename = refTable.Table;
            }
            try
            {
                TableHelperView tableHelper = null;
                if (tablename.IsNullOrEmpty())
                    Response.RedirectToRoute(returnpage);

                tableHelper = ConvertFromTableHelper(tablename);
                if (tableHelper.IsNull())
                    Response.RedirectToRoute(returnpage);


                Lookup.Remove<TableHelperView>();
                Lookup.Add(tableHelper);
                ViewData["targetTable"] = tableHelper;
            }
            catch
            {
                CollectScreenMessages();
                Response.RedirectToRoute(returnpage);
            }

            CollectScreenMessages();
            return ShowTable();
        }

        private ViewResult ShowTable()
        {
            return View("GeneralReferenceView");
        }

        public TableHelperView ConvertFromTableHelper(string tableName)
        {
            TableHelperView returnItem = new TableHelperView();
            returnItem.Table = tableName;
            return returnItem;
        }
        public ActionResult Search()
        {
            JsonResult result = null;
            TableHelperView refTableName = Lookup.Get<TableHelperView>();
            try
            {
                // Initialization.
                string refTable = refTableName.Table;
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.
                switch (refTable)
                {
                    case "Asset Branch":
                        result = SearchAssetBranch(pageSize, sortColumn, sortColumnDir, pageNumber, searchColumn, searchValue, draw);
                        break;
                    case "Asset Condition":
                        result = SearchAssetCondition(pageSize, sortColumn, sortColumnDir, pageNumber, searchColumn, searchValue, draw);
                        break;
                    case "Asset Dept":
                        result = SearchAssetDept(pageSize, sortColumn, sortColumnDir, pageNumber, searchColumn, searchValue, draw);
                        break;
                    case "Asset Location":
                        result = SearchAssetLocation(pageSize, sortColumn, sortColumnDir, pageNumber, searchColumn, searchValue, draw);
                        break;
                    case "Asset User":
                        result = SearchAssetUser(pageSize, sortColumn, sortColumnDir, pageNumber, searchColumn, searchValue, draw);
                        break;
                    case "Jenis Usaha":
                        result = SearchJenisUsaha(pageSize, sortColumn, sortColumnDir, pageNumber, searchColumn, searchValue, draw);
                        break;

                    default:
                        refTable = "";
                        break;
                }

            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            TableHelperView refTable = Lookup.Get<TableHelperView>();
            ViewData["ActionName"] = "Create";
            ViewData["TableName"] = refTable.Table;

            switch (refTable.Table)
            {
                case "Asset User":
                    ViewData["ActionName"] = "NoAuthCheckSaveAssetUser";
                    AssetUserView userdata = new AssetUserView();
                    return CreateView(userdata);
                default:
                    GeneralReferenceView data = new GeneralReferenceView();
                    return CreateView(data);
            }
        }

        private ViewResult CreateView(GeneralReferenceView data)
        {
            return View("Detail", data);
        }

        // POST: application/Create
        [HttpPost]
        public ActionResult Create(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            TableHelperView refTable = Lookup.Get<TableHelperView>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    switch (refTable.Table)
                    {
                        case "Asset Branch":
                            message = SaveAssetBranch(data);
                            break;
                        case "Asset Condition":
                            message = SaveAssetCondition(data);
                            break;
                        case "Asset Dept":
                            message = SaveAssetDept(data);
                            break;
                        case "Asset Location":
                            message = SaveAssetLocation(data);
                            break;
                        //case "Asset User":
                        //    message = SaveAssetUser(data);
                        //    break;
                        case "Jenis Usaha":
                            message = SaveJenisUsaha(data);
                            break;

                        default:
                            message = "Table Not Found";
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Show");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(GeneralReferenceView data, string mode)
        {
            if (data.DataId == "")
                return PaymentTermResources.Validation_Id;

            if (data.Description == "")
                return PaymentTermResources.Validation_PaymentTermDescription;

            if (data.ShortDesc == "")
                return PaymentTermResources.Validation_Days;

            if (data.Status.IsNull())
                return PaymentTermResources.Validation_Days;

            return "";
        }

        // GET: application/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            TableHelperView refTable = Lookup.Get<TableHelperView>();

            GeneralReferenceView data = null;

            switch (refTable.Table)
            {
                case "Asset Branch":
                    AssetBranch assetBranch = _assetBranchRepository.getAssetBranch(id);
                    data = ConvertFromAssetBranch(assetBranch);
                    ViewData["TableName"] = "Asset Branch";
                    break;
                case "Asset Condition":
                    AssetCondition assetCondition = _assetConditionRepository.getAssetCondition(id);
                    data = ConvertFromAssetCondition(assetCondition);
                    ViewData["TableName"] = "Asset Condition";
                    break;
                case "Asset Dept":
                    AssetDept assetDept = _assetDeptRepository.getAssetDept(id);
                    data = ConvertFromAssetDept(assetDept);
                    ViewData["TableName"] = "Asset Dept";
                    break;
                case "Asset Location":
                    AssetLocation assetLocation = _assetLocationRepository.getAssetLocation(id);
                    data = ConvertFromAssetLocation(assetLocation);
                    ViewData["TableName"] = "Asset Location";
                    break;
                case "Asset User":
                    AssetUser assetUser = _assetUserRepository.getAssetUser(id);
                    AssetUserView dataAUV = ConvertFrom(assetUser);
                    ViewData["TableName"] = "Asset User";
                    ViewData["ActionName"] = "NoAuthCheckEditAssetUser";
                    return CreateView(dataAUV);
                case "Jenis Usaha":
                    JenisUsaha jenisUsaha = _jenisUsahaRepository.getJenisUsaha(id);
                    data = ConvertFromJenisUsaha(jenisUsaha);
                    ViewData["TableName"] = "Jenis Usaha";
                    break;

            }
            return CreateView(data);
        }

        private ViewResult CreateView(AssetUserView data)
        {
            ViewData["DeptList"] = createDeptSelect(data.AssetDeptID);
            ViewData["BranchList"] = createBranchSelect(data.AssetBranchID);
            return View("DetailAssetUser", data);
        }

        //POST: application/Edit/5
        [HttpPost]
        public ActionResult Edit(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Edit";
            TableHelperView refTable = Lookup.Get<TableHelperView>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    switch (refTable.Table)
                    {

                        case "Asset Branch":
                            message = EditAssetBranch(data);
                            break;
                        case "Asset Condition":
                            message = EditAssetCondition(data);
                            break;
                        case "Asset Dept":
                            message = EditAssetDept(data);
                            break;
                        case "Asset Location":
                            message = EditAssetLocation(data);
                            break;
                        //case "Asset User":
                        //    message = EditAssetUser(data);
                        //    break;
                        case "Jenis Usaha":
                            message = EditJenisUsaha(data);
                            break;

                        default:
                            message = "Table Not Found";
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Show");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        // POST: application/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            TableHelperView refTable = Lookup.Get<TableHelperView>();
            string message = "";
            try
            {
                switch (refTable.Table)
                {
                    case "Asset Branch":
                        message = DeleteAssetBranch(Id);
                        break;
                    case "Asset Condition":
                        message = DeleteAssetCondition(Id);
                        break;
                    case "Asset Dept":
                        message = DeleteAssetDept(Id);
                        break;
                    case "Asset Location":
                        message = DeleteAssetLocation(Id);
                        break;
                    case "Asset User":
                        message = DeleteAssetUser(Id);
                        break;
                    case "Jenis Usaha":
                        message = DeleteJenisUsaha(Id);
                        break;

                    default:
                        message = "Table Not Found";
                        break;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Show");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }


        //-------------------------------&& */     InvoicingRule   /* && ------------------------------------------------------------------------------------
        public JsonResult SearchAssetBranch(int pageSize, string sortColumn, string sortColumnDir, int pageNumber, string searchColumn, string searchValue, string draw)
        {
            switch (sortColumn)
            {
                case "DataId":
                    sortColumn = "Id";
                    break;
                default:
                    sortColumn = "Id";
                    break;
            }
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   

                PagedResult<AssetBranch> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _assetBranchRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _assetBranchRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<GeneralReferenceView>(new Converter<AssetBranch, GeneralReferenceView>(ConvertFromAssetBranch));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public GeneralReferenceView ConvertFromAssetBranch(AssetBranch item)
        {
            GeneralReferenceView returnItem = new GeneralReferenceView();
            returnItem.DataId = item.AssetBranchID;
            returnItem.Description = item.AssetBranchDescription;
            returnItem.ShortDesc = item.AssetBranchShortDesc;
            returnItem.Status = item.Status;
            return returnItem;
        }

        public string SaveAssetBranch(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (_assetBranchRepository.IsDuplicate(data.DataId))
                    message = GeneralResources.Validation_DuplicateData;

                if (message.IsNullOrEmpty())
                {
                    AssetBranch newData = new AssetBranch("");
                    newData.AssetBranchID = data.DataId;
                    newData.AssetBranchDescription = data.Description;
                    newData.AssetBranchShortDesc = data.ShortDesc;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _assetBranchRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }

        [HttpPost]
        public string EditAssetBranch(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (message.IsNullOrEmpty())
                {
                    AssetBranch newData = _assetBranchRepository.getAssetBranch(data.DataId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.AssetBranchID = data.DataId;
                    newData.AssetBranchDescription = data.Description;
                    newData.AssetBranchShortDesc = data.ShortDesc;
                    newData.Status = data.Status;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _assetBranchRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteAssetBranch(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                AssetBranch newData = _assetBranchRepository.getAssetBranch(Id);
                _assetBranchRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }
        //===========================================================InvoicingRule===========================================================================


        //-------------------------------&& */     PurchaseCatalogClas   /* && ------------------------------------------------------------------------------------
        public JsonResult SearchAssetCondition(int pageSize, string sortColumn, string sortColumnDir, int pageNumber, string searchColumn, string searchValue, string draw)
        {
            switch (sortColumn)
            {
                case "DataId":
                    sortColumn = "Id";
                    break;
                default:
                    sortColumn = "Id";
                    break;
            }
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   

                PagedResult<AssetCondition> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _assetConditionRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _assetConditionRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<GeneralReferenceView>(new Converter<AssetCondition, GeneralReferenceView>(ConvertFromAssetCondition));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public GeneralReferenceView ConvertFromAssetCondition(AssetCondition item)
        {
            GeneralReferenceView returnItem = new GeneralReferenceView();
            returnItem.DataId = item.AssetConditionID;
            returnItem.Description = item.AssetConditionDescription;
            returnItem.ShortDesc = item.AssetConditionShortDesc;
            returnItem.Status = item.Status;
            return returnItem;
        }

        public string SaveAssetCondition(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (_assetConditionRepository.IsDuplicate(data.DataId))
                    message = GeneralResources.Validation_DuplicateData;

                if (message.IsNullOrEmpty())
                {
                    AssetCondition newData = new AssetCondition("");
                    newData.AssetConditionID = data.DataId;
                    newData.AssetConditionDescription = data.Description;
                    newData.AssetConditionShortDesc = data.ShortDesc;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _assetConditionRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }

        [HttpPost]
        public string EditAssetCondition(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (message.IsNullOrEmpty())
                {
                    AssetCondition newData = _assetConditionRepository.getAssetCondition(data.DataId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.AssetConditionID = data.DataId;
                    newData.AssetConditionDescription = data.Description;
                    newData.AssetConditionShortDesc = data.ShortDesc;
                    newData.Status = data.Status;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _assetConditionRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteAssetCondition(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                AssetCondition newData = _assetConditionRepository.getAssetCondition(Id);
                _assetConditionRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }
        //===========================================================InvoicingRule===========================================================================

        //-------------------------------&& */     PurchaseCatalogClas   /* && ------------------------------------------------------------------------------------
        public JsonResult SearchAssetDept(int pageSize, string sortColumn, string sortColumnDir, int pageNumber, string searchColumn, string searchValue, string draw)
        {
            switch (sortColumn)
            {
                case "DataId":
                    sortColumn = "Id";
                    break;
                default:
                    sortColumn = "Id";
                    break;
            }
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   

                PagedResult<AssetDept> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _assetDeptRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _assetDeptRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<GeneralReferenceView>(new Converter<AssetDept, GeneralReferenceView>(ConvertFromAssetDept));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public GeneralReferenceView ConvertFromAssetDept(AssetDept item)
        {
            GeneralReferenceView returnItem = new GeneralReferenceView();
            returnItem.DataId = item.AssetDeptID;
            returnItem.Description = item.AssetDeptDescription;
            returnItem.ShortDesc = item.AssetDeptShortDesc;
            returnItem.Status = item.Status;
            return returnItem;
        }

        public string SaveAssetDept(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (_assetDeptRepository.IsDuplicate(data.DataId))
                    message = GeneralResources.Validation_DuplicateData;

                if (message.IsNullOrEmpty())
                {
                    AssetDept newData = new AssetDept("");
                    newData.AssetDeptID = data.DataId;
                    newData.AssetDeptDescription = data.Description;
                    newData.AssetDeptShortDesc = data.ShortDesc;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _assetDeptRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }

        [HttpPost]
        public string EditAssetDept(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (message.IsNullOrEmpty())
                {
                    AssetDept newData = _assetDeptRepository.getAssetDept(data.DataId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.AssetDeptID = data.DataId;
                    newData.AssetDeptDescription = data.Description;
                    newData.AssetDeptShortDesc = data.ShortDesc;
                    newData.Status = data.Status;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _assetDeptRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteAssetDept(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                AssetDept newData = _assetDeptRepository.getAssetDept(Id);
                _assetDeptRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }
        //===========================================================InvoicingRule===========================================================================

        //-------------------------------&& */     PurchaseCatalogClas   /* && ------------------------------------------------------------------------------------
        public JsonResult SearchAssetLocation(int pageSize, string sortColumn, string sortColumnDir, int pageNumber, string searchColumn, string searchValue, string draw)
        {
            switch (sortColumn)
            {
                case "DataId":
                    sortColumn = "Id";
                    break;
                default:
                    sortColumn = "Id";
                    break;
            }
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   

                PagedResult<AssetLocation> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _assetLocationRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _assetLocationRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<GeneralReferenceView>(new Converter<AssetLocation, GeneralReferenceView>(ConvertFromAssetLocation));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public GeneralReferenceView ConvertFromAssetLocation(AssetLocation item)
        {
            GeneralReferenceView returnItem = new GeneralReferenceView();
            returnItem.DataId = item.AssetLocationID;
            returnItem.Description = item.AssetLocationDescription;
            returnItem.ShortDesc = item.AssetLocationShortDesc;
            returnItem.Status = item.Status;
            return returnItem;
        }

        public string SaveAssetLocation(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (_assetLocationRepository.IsDuplicate(data.DataId))
                    message = GeneralResources.Validation_DuplicateData;

                if (message.IsNullOrEmpty())
                {
                    AssetLocation newData = new AssetLocation("");
                    newData.AssetLocationID = data.DataId;
                    newData.AssetLocationDescription = data.Description;
                    newData.AssetLocationShortDesc = data.ShortDesc;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _assetLocationRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }

        [HttpPost]
        public string EditAssetLocation(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (message.IsNullOrEmpty())
                {
                    AssetLocation newData = _assetLocationRepository.getAssetLocation(data.DataId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.AssetLocationID = data.DataId;
                    newData.AssetLocationDescription = data.Description;
                    newData.AssetLocationShortDesc = data.ShortDesc;
                    newData.Status = data.Status;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _assetLocationRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteAssetLocation(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                AssetLocation newData = _assetLocationRepository.getAssetLocation(Id);
                _assetLocationRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }
        //===========================================================InvoicingRule===========================================================================

        //-------------------------------&& */     PurchaseCatalogClas   /* && ------------------------------------------------------------------------------------
        public JsonResult SearchAssetUser(int pageSize, string sortColumn, string sortColumnDir, int pageNumber, string searchColumn, string searchValue, string draw)
        {
            switch (sortColumn)
            {
                case "DataId":
                    sortColumn = "Id";
                    break;
                default:
                    sortColumn = "Id";
                    break;
            }
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   

                PagedResult<AssetUser> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _assetUserRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _assetUserRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<GeneralReferenceView>(new Converter<AssetUser, GeneralReferenceView>(ConvertFromAssetUser));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public GeneralReferenceView ConvertFromAssetUser(AssetUser item)
        {
            GeneralReferenceView returnItem = new GeneralReferenceView();
            returnItem.DataId = item.AssetUserID;
            returnItem.Description = item.AssetUserName;
            //returnItem.as = item.AssetUserShortDesc;
            returnItem.Status = item.Status;
            return returnItem;
        }

        public AssetUserView ConvertFrom(AssetUser item)
        {
            AssetUserView returnItem = new AssetUserView();
            returnItem.AssetUserID = item.AssetUserID.Trim();
            returnItem.AssetUserName = item.AssetUserName.Trim();
            returnItem.AssetBranchID = item.AssetBranchID.IsNull() ? "" : item.AssetBranchID.Trim();
            returnItem.AssetDeptID = item.AssetDeptID.IsNull() ? "" : item.AssetDeptID.Trim();
            returnItem.Status = item.Status;
            return returnItem;
        }

        [HttpPost]
        public ActionResult NoAuthCheckSaveAssetUser(AssetUserView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (_assetUserRepository.IsDuplicate(data.AssetUserID))
                    message = GeneralResources.Validation_DuplicateData;

                if (message.IsNullOrEmpty())
                {
                    AssetUser newData = new AssetUser("");
                    newData.AssetUserID = data.AssetUserID;
                    newData.AssetUserName = data.AssetUserName;
                    newData.AssetBranchID = data.AssetBranchID;
                    newData.AssetDeptID = data.AssetDeptID;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _assetUserRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Show");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult NoAuthCheckEditAssetUser(AssetUserView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (message.IsNullOrEmpty())
                {
                    AssetUser newData = _assetUserRepository.getAssetUser(data.AssetUserID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.AssetUserID = data.AssetUserID;
                    newData.AssetUserName = data.AssetUserName;
                    newData.AssetBranchID = data.AssetBranchID;
                    newData.AssetDeptID = data.AssetDeptID;
                    newData.Status = data.Status;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _assetUserRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Show");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public string DeleteAssetUser(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                AssetUser newData = _assetUserRepository.getAssetUser(Id);
                _assetUserRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }
        //===========================================================InvoicingRule===========================================================================
        //-------------------------------&& */     PurchaseCatalogClas   /* && ------------------------------------------------------------------------------------
        public JsonResult SearchJenisUsaha(int pageSize, string sortColumn, string sortColumnDir, int pageNumber, string searchColumn, string searchValue, string draw)
        {
            switch (sortColumn)
            {
                case "DataId":
                    sortColumn = "Id";
                    break;
                default:
                    sortColumn = "Id";
                    break;
            }
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   

                PagedResult<JenisUsaha> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _jenisUsahaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _jenisUsahaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<GeneralReferenceView>(new Converter<JenisUsaha, GeneralReferenceView>(ConvertFromJenisUsaha));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public GeneralReferenceView ConvertFromJenisUsaha(JenisUsaha item)
        {
            GeneralReferenceView returnItem = new GeneralReferenceView();
            returnItem.DataId = item.JenisUsahaID;
            returnItem.Description = item.NamaJenisUsaha;
            returnItem.ShortDesc = item.JenisUsahaShortDesc;
            returnItem.Status = item.Status;
            return returnItem;
        }

        public string SaveJenisUsaha(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (_jenisUsahaRepository.IsDuplicate(data.DataId))
                    message = GeneralResources.Validation_DuplicateData;

                if (message.IsNullOrEmpty())
                {
                    JenisUsaha newData = new JenisUsaha("");
                    newData.JenisUsahaID = data.DataId;
                    newData.NamaJenisUsaha = data.Description;
                    newData.JenisUsahaShortDesc = data.ShortDesc;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _jenisUsahaRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }

        [HttpPost]
        public string EditJenisUsaha(GeneralReferenceView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (message.IsNullOrEmpty())
                {
                    JenisUsaha newData = _jenisUsahaRepository.getJenisUsaha(data.DataId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.JenisUsahaID = data.DataId;
                    newData.NamaJenisUsaha = data.Description;
                    newData.JenisUsahaShortDesc = data.ShortDesc;
                    newData.Status = data.Status;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _jenisUsahaRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteJenisUsaha(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                JenisUsaha newData = _jenisUsahaRepository.getJenisUsaha(Id);
                _jenisUsahaRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }
        //===========================================================InvoicingRule===========================================================================

        //====================================================  Dept ==================================================================
        private IList<SelectListItem> createDeptSelect(string selected)
        {
            List<AssetDept> itemList = null;// Lookup.Get<List<AssetDept>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetDeptRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetDept, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetDept item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetDeptDescription;
            returnItem.Value = item.AssetDeptID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================

        //====================================================  Branch ==================================================================
        private IList<SelectListItem> createBranchSelect(string selected)
        {
            List<AssetBranch> itemList = null;// Lookup.Get<List<AssetDept>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetBranchRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetBranch, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetBranch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetBranchDescription;
            returnItem.Value = item.AssetBranchID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================

    }
}