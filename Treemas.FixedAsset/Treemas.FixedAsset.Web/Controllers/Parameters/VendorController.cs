﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;

using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using System.Globalization;

namespace Treemas.FixedAsset.Controllers
{
    public class VendorController : PageController
    {

        private IVendorRepository _VendorRepository;
        private IAssetTypeRepository _AssetTypeRepository;
        private IAuditTrailLog _auditRepository;
        private IKelurahanRepository _kelurahanRepository;
        private IVendorService _vendorService;
        public VendorController(ISessionAuthentication sessionAuthentication, IVendorRepository VendorRepository, IAuditTrailLog auditRepository,
            IAssetTypeRepository assetTypeRepository, IKelurahanRepository kelurahanRepository, IVendorService vendorService) : base(sessionAuthentication)
        {
            this._VendorRepository = VendorRepository;
            this._AssetTypeRepository = assetTypeRepository;
            this._auditRepository = auditRepository;
            this._kelurahanRepository = kelurahanRepository;
            this._vendorService = vendorService;

            Settings.ModuleName = "Vendor";
            Settings.Title = VendorResources.PageTitle;
        }

        protected override void Startup()
        {
            ViewData["AssetTypeList"] = createAssetTypeSelect("");
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                VendorFilter filter = new VendorFilter();
                filter.VendorName = Request.QueryString.GetValues("VendorName")[0];
                filter.ContactPersonName = Request.QueryString.GetValues("ContactPersonName")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Vendor> data;
                data = _VendorRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<VendorView>(new Converter<Vendor, VendorView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public ActionResult NoAuthCheckSearch()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Vendor> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _VendorRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _VendorRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<VendorView>(new Converter<Vendor, VendorView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public VendorView ConvertFrom(Vendor item)
        {
            VendorView returnItem = new VendorView();
            AssetType assetType = _AssetTypeRepository.getAssetType(item.AssetTypeID);
            Vendor vendor = _VendorRepository.getVendor(item.VendorGroupID);

            returnItem.Id = item.Id;
            returnItem.VendorId = item.VendorId;
            returnItem.VendorName = item.VendorName;
            returnItem.VendorGroupID = item.VendorGroupID;
            returnItem.VendorGroupName = vendor.IsNull() ? "" : vendor.VendorName.Trim();
            returnItem.GroupCompanyID = item.GroupCompanyID;
            returnItem.NPWP = item.NPWP;
            returnItem.TDP = item.TDP;
            returnItem.SIUP = item.SIUP;
            returnItem.VendorAddress = item.VendorAddress;
            returnItem.VendorRT = item.VendorRT;
            returnItem.VendorRW = item.VendorRW;
            returnItem.VendorKelurahan = item.VendorKelurahan;
            returnItem.VendorKecamatan = item.VendorKecamatan;
            returnItem.VendorCity = item.VendorCity;
            returnItem.VendorZipCode = item.VendorZipCode;
            returnItem.VendorAreaPhone1 = item.VendorAreaPhone1;
            returnItem.VendorPhone1 = item.VendorPhone1;
            returnItem.VendorAreaPhone2 = item.VendorAreaPhone2;
            returnItem.VendorPhone2 = item.VendorPhone2;
            returnItem.VendorAreaFax = item.VendorAreaFax;
            returnItem.VendorFax = item.VendorFax;
            returnItem.ContactPersonName = item.ContactPersonName;
            returnItem.ContactPersonJobTitle = item.ContactPersonJobTitle;
            returnItem.ContactPersonEmail = item.ContactPersonEmail;
            returnItem.ContactPersonHP = item.ContactPersonHP;
            returnItem.VendorStatus = item.VendorStatus;
            returnItem.VendorStartDate = item.VendorStartDate.IsNull() ? null : Convert.ToDateTime(item.VendorStartDate).ToString("dd/MM/yyyy");
            returnItem.VendorBadStatus = item.VendorBadStatus;
            returnItem.VendorLevelStatus = item.VendorLevelStatus;
            returnItem.LastCalculationDate = item.LastCalculationDate.IsNull() ? null : Convert.ToDateTime(item.LastCalculationDate).ToString("dd/MM/yyyy");
            returnItem.IsAutomotive = item.IsAutomotive;
            returnItem.IsUrusBPKB = item.IsUrusBPKB;
            returnItem.VendorCategory = item.VendorCategory;
            returnItem.VendorAssetStatus = item.VendorAssetStatus;
            returnItem.VendorIncentiveIDNew = item.VendorIncentiveIDNew;
            returnItem.VendorIncentiveIDUsed = item.VendorIncentiveIDUsed;
            returnItem.VendorIncentiveIDRO = item.VendorIncentiveIDRO;
            returnItem.IsActive = item.IsActive;
            returnItem.IsActiveString = item.IsActive ? "Aktif" : "Non-Aktif";
            returnItem.VendorPointId = item.VendorPointId;
            returnItem.PF = item.PF;
            returnItem.NoPKS = item.NoPKS;
            returnItem.PenerapanTVC = item.PenerapanTVC;
            returnItem.isPerseorangan = item.isPerseorangan;
            returnItem.PKSDate = item.PKSDate.IsNull() ? null : Convert.ToDateTime(item.PKSDate).ToString("dd/MM/yyyy");
            returnItem.AssetTypeID = item.AssetTypeID.IsNullOrEmpty() ? "" : item.AssetTypeID.Trim();
            returnItem.AssetTypeDesc = assetType.IsNull() ? "" : assetType.Description.Trim();
            returnItem.VendorInitialName = item.VendorInitialName;
            //returnItem.AttachedFile = item.AttachedFile;
            returnItem.SKBP = item.SKBP;
            returnItem.NPWPVendorAddress = item.NPWPVendorAddress;
            returnItem.NPWPVendorRT = item.NPWPVendorRT;
            returnItem.NPWPVendorRW = item.NPWPVendorRW;
            returnItem.NPWPVendorKelurahan = item.NPWPVendorKelurahan;
            returnItem.NPWPVendorKecamatan = item.NPWPVendorKecamatan;
            returnItem.NPWPVendorCity = item.NPWPVendorCity;
            returnItem.NPWPVendorZipCode = item.NPWPVendorZipCode;
            returnItem.NPWPVendorAreaPhone1 = item.NPWPVendorAreaPhone1;
            returnItem.NPWPVendorPhone1 = item.NPWPVendorPhone1;
            returnItem.NPWPVendorAreaPhone2 = item.NPWPVendorAreaPhone2;
            returnItem.NPWPVendorPhone2 = item.NPWPVendorPhone2;
            returnItem.NPWPVendorAreaFax = item.NPWPVendorAreaFax;
            returnItem.NPWPVendorFax = item.NPWPVendorFax;

            return returnItem;
        }

        //GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            VendorView data = new VendorView();
            return CreateView(data);
        }

        private ViewResult CreateView(VendorView data)
        {
            ViewData["AssetTypeList"] = createAssetTypeSelect(data.AssetTypeID);
            //ViewData["VendorKotaList"] = createVendorKotaSelect(data.VendorCity);
            //ViewData["VendorKecamatanList"] = createVendorKecamatanSelect(data.VendorKecamatan);
            //ViewData["VendorKelurahanList"] = createVendorKelurahanSelect(data.VendorKelurahan);
            //ViewData["NPWPKotaList"] = createNPWPKotaSelect(data.NPWPVendorCity);
            //ViewData["NPWPKecamatanList"] = createNPWPKecamatanSelect(data.NPWPVendorKecamatan);
            //ViewData["NPWPKelurahanList"] = createNPWPKelurahanSelect(data.NPWPVendorKelurahan);

            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(VendorView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Vendor newData = new Vendor("");
                    //newData.VendorID = data.VendorID;
                    newData.VendorName = data.VendorName;
                    newData.VendorGroupID = data.VendorGroupID;
                    newData.GroupCompanyID = data.GroupCompanyID;
                    newData.NPWP = data.NPWP;
                    newData.TDP = data.TDP;
                    newData.SIUP = data.SIUP;
                    newData.VendorAddress = data.VendorAddress;
                    newData.VendorRT = data.VendorRT;
                    newData.VendorRW = data.VendorRW;
                    newData.VendorKelurahan = data.VendorKelurahan;
                    newData.VendorKecamatan = data.VendorKecamatan;
                    newData.VendorCity = data.VendorCity;
                    newData.VendorZipCode = data.VendorZipCode;
                    newData.VendorAreaPhone1 = data.VendorAreaPhone1;
                    newData.VendorPhone1 = data.VendorPhone1;
                    newData.VendorAreaPhone2 = data.VendorAreaPhone2;
                    newData.VendorPhone2 = data.VendorPhone2;
                    newData.VendorAreaFax = data.VendorAreaFax;
                    newData.VendorFax = data.VendorFax;
                    newData.ContactPersonName = data.ContactPersonName;
                    newData.ContactPersonJobTitle = data.ContactPersonJobTitle;
                    newData.ContactPersonEmail = data.ContactPersonEmail;
                    newData.ContactPersonHP = data.ContactPersonHP;
                    newData.VendorStatus = "N";
                    newData.VendorStartDate = !data.VendorStartDate.IsNullOrEmpty() ? DateTime.ParseExact(data.VendorStartDate, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.VendorBadStatus = data.VendorBadStatus;
                    newData.VendorLevelStatus = data.VendorLevelStatus;
                    newData.LastCalculationDate = !data.LastCalculationDate.IsNullOrEmpty() ? DateTime.ParseExact(data.LastCalculationDate, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.IsAutomotive = data.IsAutomotive;
                    newData.IsUrusBPKB = data.IsUrusBPKB;
                    newData.VendorCategory = data.VendorCategory;
                    newData.VendorAssetStatus = data.VendorAssetStatus;
                    newData.VendorIncentiveIDNew = data.VendorIncentiveIDNew;
                    newData.VendorIncentiveIDUsed = data.VendorIncentiveIDUsed;
                    newData.VendorIncentiveIDRO = data.VendorIncentiveIDRO;
                    newData.VendorPointId = data.VendorPointId;
                    newData.PF = data.PF;
                    newData.NoPKS = data.NoPKS;
                    newData.PenerapanTVC = data.PenerapanTVC;
                    newData.isPerseorangan = data.isPerseorangan;
                    newData.PKSDate = !data.PKSDate.IsNullOrEmpty() ? DateTime.ParseExact(data.PKSDate, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.AssetTypeID = data.AssetTypeID.IsNullOrEmpty() ? null : data.AssetTypeID.Trim();
                    newData.VendorInitialName = data.VendorInitialName;
                    //newData.AttachedFile = data.AttachedFile;
                    newData.SKBP = data.SKBP;
                    newData.NPWPVendorAddress = data.NPWPVendorAddress;
                    newData.NPWPVendorRT = data.NPWPVendorRT;
                    newData.NPWPVendorRW = data.NPWPVendorRW;
                    newData.NPWPVendorKelurahan = data.NPWPVendorKelurahan;
                    newData.NPWPVendorKecamatan = data.NPWPVendorKecamatan;
                    newData.NPWPVendorCity = data.NPWPVendorCity;
                    newData.NPWPVendorZipCode = data.NPWPVendorZipCode;
                    newData.NPWPVendorAreaPhone1 = data.NPWPVendorAreaPhone1;
                    newData.NPWPVendorPhone1 = data.NPWPVendorPhone1;
                    newData.NPWPVendorAreaPhone2 = data.NPWPVendorAreaPhone2;
                    newData.NPWPVendorPhone2 = data.NPWPVendorPhone2;
                    newData.NPWPVendorAreaFax = data.NPWPVendorAreaFax;
                    newData.NPWPVendorFax = data.NPWPVendorFax;
                    newData.IsActive = true;
                    newData.UsrUpd = user.Username;
                    newData.DtmUpd = DateTime.Now;

                    message = _vendorService.SaveVendor(newData, user, company);
                    if (message == "")
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(VendorView data, string mode)
        {
            //if (data.VendorID == "")
            //    return VendorResources.Validation_Id;

            if (data.VendorName.IsNullOrEmpty())
                return VendorResources.Validation_Name;

            if (data.NPWP.IsNullOrEmpty())
                return VendorResources.Validation_NPWP;

            //if (data.isPerseorangan.IsNull())
            //    return "";

            if (data.ContactPersonName.IsNullOrEmpty())
                return VendorResources.Validation_ContactPersonName;

            if (data.VendorAddress.IsNullOrEmpty())
                return VendorResources.Validation_VendorAddress;


            //if (data.VendorKelurahan == "")
            //    return VendorResources.Validation_VendorKelurahan;

            //if (data.VendorKecamatan == "")
            //    return VendorResources.Validation_VendorKecamatan;

            //if (data.VendorCity == "")
            //    return VendorResources.Validation_VendorCity;

            if (data.VendorZipCode.IsNullOrEmpty())
                return VendorResources.Validation_VendorZipCode;

            if (data.VendorAreaPhone1.IsNullOrEmpty())
                return "Silahkan masukkan kode area Vendor Phone 1!";

            if (data.VendorPhone1.IsNullOrEmpty())
                return VendorResources.Validation_VendorPhone;

            if (data.NPWPVendorAddress.IsNullOrEmpty())
                return VendorResources.Validation_VendorAddress;

            if (data.NPWPVendorZipCode.IsNullOrEmpty())
                return VendorResources.Validation_VendorZipCode;

            if (data.NPWPVendorAreaPhone1.IsNullOrEmpty())
                return "Silahkan masukkan kode area NPWP Phone 1!";

            if (data.NPWPVendorPhone1.IsNullOrEmpty())
                return VendorResources.Validation_VendorPhone;
            //if (data.VendorFax == "")
            //    return VendorResources.Validation_VendorFax;

            //if (data.VendorWebsite == "")
            //    return VendorResources.Validation_VendorWebsite;

            //if (data.VendorTypeID.IsNullOrEmpty())
            //    return VendorResources.Validation_VendorTypeID;

            //if (data.Currency == "")
            //    return VendorResources.Validation_Currency;

            //if (data.Buyer == "")
            //    return VendorResources.Validation_Buyer;

            //if (data.PaymentTerm == "")
            //    return VendorResources.Validation_PaymentTerm;

            //if (data.FlagAutoUpdateResource.IsNull())
            //    return VendorResources.Validation_FlagAutoUpdateResource;


            if (data.VendorCategory.IsNullOrEmpty())
                return "Silahkan pilih kategori vendor!";

            if (data.VendorAssetStatus.IsNullOrEmpty())
                return "Silahkan pilih vendor jual aset!";

            if (mode == "Create")
            {
                if (_VendorRepository.IsDuplicate(data.VendorId))
                    return VendorResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            Vendor Vendor = _VendorRepository.getVendor(id);
            VendorView data = ConvertFrom(Vendor);

            return CreateView(data);
        }

        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(VendorView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();

            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Vendor newData = _VendorRepository.getVendor(data.VendorId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    //newData.VendorID = data.VendorID;
                    newData.VendorName = data.VendorName;
                    newData.VendorGroupID = data.VendorGroupID;
                    newData.GroupCompanyID = data.GroupCompanyID;
                    newData.NPWP = data.NPWP;
                    newData.TDP = data.TDP;
                    newData.SIUP = data.SIUP;
                    newData.VendorAddress = data.VendorAddress;
                    newData.VendorRT = data.VendorRT;
                    newData.VendorRW = data.VendorRW;
                    newData.VendorKelurahan = data.VendorKelurahan;
                    newData.VendorKecamatan = data.VendorKecamatan;
                    newData.VendorCity = data.VendorCity;
                    newData.VendorZipCode = data.VendorZipCode;
                    newData.VendorAreaPhone1 = data.VendorAreaPhone1;
                    newData.VendorPhone1 = data.VendorPhone1;
                    newData.VendorAreaPhone2 = data.VendorAreaPhone2;
                    newData.VendorPhone2 = data.VendorPhone2;
                    newData.VendorAreaFax = data.VendorAreaFax;
                    newData.VendorFax = data.VendorFax;
                    newData.ContactPersonName = data.ContactPersonName;
                    newData.ContactPersonJobTitle = data.ContactPersonJobTitle;
                    newData.ContactPersonEmail = data.ContactPersonEmail;
                    newData.ContactPersonHP = data.ContactPersonHP;
                    newData.VendorStatus = "N";
                    newData.VendorStartDate = !data.VendorStartDate.IsNullOrEmpty() ? DateTime.ParseExact(data.VendorStartDate, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.VendorBadStatus = data.VendorBadStatus;
                    newData.VendorLevelStatus = data.VendorLevelStatus;
                    newData.LastCalculationDate = !data.LastCalculationDate.IsNullOrEmpty() ? DateTime.ParseExact(data.LastCalculationDate, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.IsAutomotive = data.IsAutomotive;
                    newData.IsUrusBPKB = data.IsUrusBPKB;
                    newData.VendorCategory = data.VendorCategory;
                    newData.VendorAssetStatus = data.VendorAssetStatus;
                    newData.VendorIncentiveIDNew = data.VendorIncentiveIDNew;
                    newData.VendorIncentiveIDUsed = data.VendorIncentiveIDUsed;
                    newData.VendorIncentiveIDRO = data.VendorIncentiveIDRO;
                    newData.VendorPointId = data.VendorPointId;
                    newData.PF = data.PF;
                    newData.NoPKS = data.NoPKS;
                    newData.PenerapanTVC = data.PenerapanTVC;
                    newData.isPerseorangan = data.isPerseorangan;
                    newData.PKSDate = !data.PKSDate.IsNullOrEmpty() ? DateTime.ParseExact(data.PKSDate, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.AssetTypeID = data.AssetTypeID.IsNullOrEmpty() ? null : data.AssetTypeID.Trim();
                    newData.VendorInitialName = data.VendorInitialName;
                    //newData.AttachedFile = data.AttachedFile;
                    newData.SKBP = data.SKBP;
                    newData.NPWPVendorAddress = data.NPWPVendorAddress;
                    newData.NPWPVendorRT = data.NPWPVendorRT;
                    newData.NPWPVendorRW = data.NPWPVendorRW;
                    newData.NPWPVendorKelurahan = data.NPWPVendorKelurahan;
                    newData.NPWPVendorKecamatan = data.NPWPVendorKecamatan;
                    newData.NPWPVendorCity = data.NPWPVendorCity;
                    newData.NPWPVendorZipCode = data.NPWPVendorZipCode;
                    newData.NPWPVendorAreaPhone1 = data.NPWPVendorAreaPhone1;
                    newData.NPWPVendorPhone1 = data.NPWPVendorPhone1;
                    newData.NPWPVendorAreaPhone2 = data.NPWPVendorAreaPhone2;
                    newData.NPWPVendorPhone2 = data.NPWPVendorPhone2;
                    newData.NPWPVendorAreaFax = data.NPWPVendorAreaFax;
                    newData.NPWPVendorFax = data.NPWPVendorFax;
                    newData.IsActive = true;

                    newData.UsrUpd = user.Username;
                    newData.DtmUpd = DateTime.Now;

                    message = _vendorService.UpdateVendor(newData, user, company);
                    if (message == "")
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Vendor newData = _VendorRepository.getVendor(Id);

                newData.IsActive = false;
                newData.UsrUpd = user.Username;
                newData.DtmUpd = DateTime.Now;

                _VendorRepository.Save(newData);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");

            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

        private IList<SelectListItem> createAssetTypeSelect(string selected)
        {
            List<AssetType> itemList = null;// Lookup.Get<List<AssetLocation>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _AssetTypeRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetType, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description.Trim();
            returnItem.Value = item.AssetTypeID.Trim();
            return returnItem;
        }

        //-----------------------------------------------------------------------------------------------------------------
        private IList<SelectListItem> createVendorKotaSelect(string selected)
        {
            List<Foundation_Kelurahan> KelurahanList = Lookup.Get<List<Foundation_Kelurahan>>();
            if (KelurahanList.IsNullOrEmpty())
            {
                KelurahanList = _kelurahanRepository.FindAll().ToList();
                Lookup.Remove(KelurahanList);
                Lookup.Add(KelurahanList);
            }

            List<Foundation_Kelurahan> itemList = KelurahanList.GroupBy(k => new { Kota = k.Kota.Trim() }).Select(c => new Foundation_Kelurahan("K") { Kota = c.Key.Kota.Trim() }).ToList();

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Foundation_Kelurahan, SelectListItem>(ConvertFromKota));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createNPWPKotaSelect(string selected)
        {
            List<Foundation_Kelurahan> KelurahanList = Lookup.Get<List<Foundation_Kelurahan>>();
            if (KelurahanList.IsNullOrEmpty())
            {
                KelurahanList = _kelurahanRepository.FindAll().ToList();
                Lookup.Remove(KelurahanList);
                Lookup.Add(KelurahanList);
            }

            List<Foundation_Kelurahan> itemList = KelurahanList.GroupBy(k => new { Kota = k.Kota.Trim() }).Select(c => new Foundation_Kelurahan("K") { Kota = c.Key.Kota.Trim() }).ToList();

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Foundation_Kelurahan, SelectListItem>(ConvertFromKota));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFromKota(Foundation_Kelurahan item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Kota.Trim();
            returnItem.Value = item.Kota.Trim();
            return returnItem;
        }

        //-----------------------------------------------------------------------------------------------------------------
        private IList<SelectListItem> createVendorKecamatanSelect(string selected)
        {
            List<Foundation_Kelurahan> KelurahanList = Lookup.Get<List<Foundation_Kelurahan>>();
            if (KelurahanList.IsNullOrEmpty())
            {
                KelurahanList = _kelurahanRepository.FindAll().ToList();
                Lookup.Remove(KelurahanList);
                Lookup.Add(KelurahanList);
            }

            List<Foundation_Kelurahan> itemList = KelurahanList.GroupBy(k => new { Kecamatan = k.Kecamatan.Trim() }).Select(c => new Foundation_Kelurahan("C") { Kecamatan = c.Key.Kecamatan.Trim() }).ToList();

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Foundation_Kelurahan, SelectListItem>(ConvertFromKecamatan));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createNPWPKecamatanSelect(string selected)
        {
            List<Foundation_Kelurahan> KelurahanList = Lookup.Get<List<Foundation_Kelurahan>>();
            if (KelurahanList.IsNullOrEmpty())
            {
                KelurahanList = _kelurahanRepository.FindAll().ToList();
                Lookup.Remove(KelurahanList);
                Lookup.Add(KelurahanList);
            }

            List<Foundation_Kelurahan> itemList = KelurahanList.GroupBy(k => new { Kecamatan = k.Kecamatan.Trim() }).Select(c => new Foundation_Kelurahan("C") { Kecamatan = c.Key.Kecamatan.Trim() }).ToList();

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Foundation_Kelurahan, SelectListItem>(ConvertFromKecamatan));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFromKecamatan(Foundation_Kelurahan item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Kecamatan.Trim();
            returnItem.Value = item.Kecamatan.Trim();
            return returnItem;
        }

        //-----------------------------------------------------------------------------------------------------------------
        private IList<SelectListItem> createVendorKelurahanSelect(string selected)
        {
            List<Foundation_Kelurahan> KelurahanList = Lookup.Get<List<Foundation_Kelurahan>>();
            if (KelurahanList.IsNullOrEmpty())
            {
                KelurahanList = _kelurahanRepository.FindAll().ToList();
                Lookup.Remove(KelurahanList);
                Lookup.Add(KelurahanList);
            }

            List<Foundation_Kelurahan> itemList = KelurahanList.GroupBy(k => new { KelurahanID = k.KelurahanID, Kelurahan = k.Kelurahan.Trim() })
                .Select(c => new Foundation_Kelurahan(c.Key.KelurahanID) { KelurahanID = c.Key.KelurahanID, Kelurahan = c.Key.Kelurahan.Trim() }).ToList();

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Foundation_Kelurahan, SelectListItem>(ConvertFromKelurahan));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Text == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createNPWPKelurahanSelect(string selected)
        {
            List<Foundation_Kelurahan> KelurahanList = Lookup.Get<List<Foundation_Kelurahan>>();
            if (KelurahanList.IsNullOrEmpty())
            {
                KelurahanList = _kelurahanRepository.FindAll().ToList();
                Lookup.Remove(KelurahanList);
                Lookup.Add(KelurahanList);
            }

            List<Foundation_Kelurahan> itemList = KelurahanList.GroupBy(k => new { KelurahanID = k.KelurahanID, Kelurahan = k.Kelurahan.Trim() })
                .Select(c => new Foundation_Kelurahan(c.Key.KelurahanID) { KelurahanID = c.Key.KelurahanID, Kelurahan = c.Key.Kelurahan.Trim() }).ToList();

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Foundation_Kelurahan, SelectListItem>(ConvertFromKelurahan));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Text == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFromKelurahan(Foundation_Kelurahan item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Kelurahan.Trim();
            returnItem.Value = item.KelurahanID.Trim();
            return returnItem;
        }

        //-----------------------------------------------------------------------------------------------------------------

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetKecamatanByKota(string Kota)
        {
            List<Foundation_Kelurahan> itemList = null;
            itemList = _kelurahanRepository.getKecamatan(Kota).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Foundation_Kelurahan, SelectListItem>(ConvertFromKecamatan));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetKelurahaByKotaKecamatan(string Kota, string Kecamatan)
        {
            List<Foundation_Kelurahan> itemList = null;
            itemList = _kelurahanRepository.getKelurahan(Kota, Kecamatan).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Foundation_Kelurahan, SelectListItem>(ConvertFromKelurahan));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetZipCode(string ID)
        {
            Foundation_Kelurahan zip = _kelurahanRepository.GetZipCode(ID);
            List<object> objectList = new List<object>();

            objectList.Add(new { ZipCode = zip.ZipCode });

            return Json(objectList, JsonRequestBehavior.AllowGet);
        }
    }
}