﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using System.Globalization;
using System.Transactions;
using System.Web.UI.WebControls;
using System.Linq;

namespace Treemas.FixedAsset.Controllers
{
    public class ApprovalRequestPaymentController : PageController
    {
        private IInvoiceRepository _invoiceRepository;
        private IVendorRepository _suppRepository;
        private IVendorAccountRepository _suppAccRepository;
        private IRequestPaymentRepository _reqPaymentRepo;
        private IGroupAssetRepository _astGroupRepo;
        private ISubGroupAssetRepository _subGrpRepo;
        private IBankMasterRepository _bankMasterRepo;
        private IPaymentTypeRepository _paymentTypeRepository;
        private IAuditTrailLog _auditRepository;
        private IFixedAssetMasterRepository _fixedAssetMasterRepository;
        private string _paymentTypeID;

        public ApprovalRequestPaymentController(ISessionAuthentication sessionAuthentication, IInvoiceRepository invoiceRepository,
            IVendorRepository suppRepository, IVendorAccountRepository suppAccRepository, IRequestPaymentRepository reqPaymentRepo,
            IAuditTrailLog auditRepository, IGroupAssetRepository astGrouprepo, ISubGroupAssetRepository subGrpRepo, IPaymentTypeRepository paymentTypeRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepository, IBankMasterRepository bankMasterRepo) : base(sessionAuthentication)
        {
            this._invoiceRepository = invoiceRepository;
            this._suppRepository = suppRepository;
            this._suppAccRepository = suppAccRepository;
            this._reqPaymentRepo = reqPaymentRepo;
            this._astGroupRepo = astGrouprepo;
            this._subGrpRepo = subGrpRepo;
            this._bankMasterRepo = bankMasterRepo;
            this._paymentTypeRepository = paymentTypeRepository;
            this._auditRepository = auditRepository;
            this._fixedAssetMasterRepository = fixedAssetMasterRepository;

            Settings.ModuleName = "Approval Pengajuan Pembayaran";
            Settings.Title = "Approval "+RequestPaymentResources.PageTitle;
        }

        protected override void Startup()
        {
            ViewData["InvStatus"] = createRequestStatusSelect("N");
        }

        public ActionResult Search()
        {
            string Mode = Request.QueryString.GetValues("Mode")[0];
            string SearchType = Mode == "" ? "Request" : Mode;
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            JsonResult result = new JsonResult();
            try
            {
                if (SearchType == "Request")
                {
                    // Initialization.   
                    RequestPaymentFilter filter = new RequestPaymentFilter();
                    filter.RequestDateFrom = !Request.QueryString["RequestDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("RequestDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.RequestDateTo = !Request.QueryString["RequestDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("RequestDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.VendorId = Request.QueryString.GetValues("VendorId")[0];
                    filter.VendorInvoiceNo = Request.QueryString.GetValues("VendorInvoiceNo")[0];
                    filter.InvoiceStatus = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];

                    string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                    var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                    int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                    int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                    int pageNumber = (startRec + pageSize - 1) / pageSize;

                    // Loading.   
                    PagedResult<RequestPayment> data;
                    data = _reqPaymentRepo.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                    var items = data.Items.ToList().ConvertAll<RequestPaymentView>(new Converter<RequestPayment, RequestPaymentView>(ConvertFrom));

                    result = this.Json(new
                    {
                        draw = Convert.ToInt32(draw),
                        recordsTotal = data.TotalItems,
                        recordsFiltered = data.TotalItems,
                        data = items
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public RequestPaymentView ConvertFrom(RequestPayment item)
        {
            RequestPaymentView returnItem = new RequestPaymentView();
            PaymentType paytype = _paymentTypeRepository.GetPaymentType(item.CaraBayar);

            returnItem.VendorId = item.VendorId;
            returnItem.VendorName = _suppRepository.getVendor(item.VendorId).VendorName;
            returnItem.InvoiceNo = item.InvoiceNo;
            returnItem.TanggalInvoice = Convert.ToDateTime(item.InvoiceDate).ToString("dd/MM/yyyy");
            returnItem.RequestDate = Convert.ToDateTime(item.RequestDate).ToString("dd/MM/yyyy");
            returnItem.TanggalRencanaBayar = Convert.ToDateTime(item.PaymentOrderDate).ToString("dd/MM/yyyy");
            returnItem.TanggalRencanaBayarString = Convert.ToDateTime(item.PaymentOrderDate).ToString("dd/MM/yyyy");
            returnItem.RequestDateString = Convert.ToDateTime(item.RequestDate).ToString("dd/MM/yyyy");
            returnItem.InvoiceDateString = Convert.ToDateTime(item.InvoiceDate).ToString("dd/MM/yyyy");
            returnItem.TanggalJatuhTempo = Convert.ToDateTime(item.PaymentOrderDate).ToString("dd/MM/yyyy");
            returnItem.TanggalJatuhTempoString = Convert.ToDateTime(item.PaymentOrderDate).ToString("dd/MM/yyyy");
            returnItem.VendorInvoiceNo = item.VendorInvoiceNo;
            returnItem.RefferenceNo = item.RefferenceNo;
            returnItem.Notes = item.Notes;
            returnItem.CaraBayar = item.CaraBayar;
            returnItem.CaraBayarString = paytype.IsNull() ? "" : paytype.PaymentTypeName.Trim();
            returnItem.Status = item.Status;
            returnItem.StatusString = item.RequestStatusEnum.ToDescription();
            returnItem.Jumlah = item.Jumlah.ToString("N0");
            returnItem.VendorBankId = item.VendorBankId;
            returnItem.VendorBankAccountName = item.VendorBankAccountName;
            returnItem.VendorBankAccountNo = item.VendorBankAccountNo;
            returnItem.VendorBankBranch = item.VendorBankBranch;
            //returnItem.AllowEdit = item.AllowEdit;
            returnItem.AllowApprove = item.AllowApprove;
            return returnItem;
        }

        private IList<SelectListItem> createPaymentTypeSelect(string selected)
        {
            List<PaymentType> itemList = _paymentTypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<PaymentType, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _paymentTypeID = itemList.FirstOrDefault().PaymentTypeID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString())).Selected = true;
                _paymentTypeID = selected;
            }

            return dataList;
        }

        public SelectListItem ConvertForm(PaymentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.PaymentTypeName.Trim();
            returnItem.Value = item.PaymentTypeID.ToString().Trim();
            return returnItem;
        }

        private IList<SelectListItem> createRequestStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(RequestPaymentEnum)).Cast<RequestPaymentEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }

        private ViewResult CreateView(RequestPaymentView data)
        {
            ViewData["PaymentType"] = createPaymentTypeSelect(data.CaraBayar);
            return View("Detail", data);
        }

        public ActionResult Approve(string id)
        {
            ViewData["ActionName"] = "Approve";
            RequestPayment Item = _reqPaymentRepo.getRequest(id);
            RequestPaymentView data = ConvertFrom(Item);

            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Approve(RequestPaymentView data)
        {
            ViewData["ActionName"] = "Approve";
            User user = Lookup.Get<User>();
            string message = "";

            try
            {
                foreach (string invoice in data.RefferenceNo.Split(';'))
                {
                    RequestPayment updData = _reqPaymentRepo.getRequest(invoice);

                    updData.Status = RequestPaymentEnum.A.ToString();

                    _reqPaymentRepo.Save(updData);

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, updData, user.Username, "Approve");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Approve_succes));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public ActionResult Reject(string id)
        {
            ViewData["ActionName"] = "Approve";
            User user = Lookup.Get<User>();
            string message = "";
            string invNo = "";

            try
            {
                foreach (string invoice in id.Split(';'))
                {
                    invNo = invoice;
                    RequestPayment updData = _reqPaymentRepo.getRequest(invoice);

                    updData.Status = RequestPaymentEnum.R.ToString();

                    _reqPaymentRepo.Save(updData);

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, updData, user.Username, "Reject");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Reject_success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            RequestPayment pdata = _reqPaymentRepo.getRequest(invNo);
            RequestPaymentView data = ConvertFrom(pdata);
            return CreateView(data);
        }
    }
}