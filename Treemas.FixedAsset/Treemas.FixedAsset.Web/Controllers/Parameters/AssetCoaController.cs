﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.FixedAsset.Controllers
{
    public class AssetCoaController : PageController
    {
        private IAssetCoaRepository _assetCoaRepository;
        private IAuditTrailLog _auditRepository;
        public AssetCoaController(ISessionAuthentication sessionAuthentication, IAssetCoaRepository assetCoaRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._assetCoaRepository = assetCoaRepository;
            this._auditRepository = auditRepository;

            Settings.ModuleName = "Asset COA";
            Settings.Title = AssetCoaResources.PageTitle;
        }
        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<AssetCoa> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _assetCoaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _assetCoaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<AssetCoaView>(new Converter<AssetCoa, AssetCoaView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public AssetCoaView ConvertFrom(AssetCoa item)
        {
            AssetCoaView returnItem = new AssetCoaView();

            returnItem.COAID = item.COAID;
            returnItem.COADescription = item.COADescription;
            returnItem.CoaType = item.CoaTypeEnums;
            returnItem.CoaTypeString = item.CoaTypeEnums.ToDescription();
            returnItem.Status = item.Status;
            return returnItem;
        }

        private IList<SelectListItem> createCoaType(int selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(CoaTypeEnum)).Cast<CoaTypeEnum>().Select(x => new SelectListItem { Text = x.ToDescription(), Value = ((int)x).ToString() }).ToList();
            if (selected > 0)
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetBranch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetBranchDescription;
            returnItem.Value = item.AssetBranchID;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            AssetCoaView data = new AssetCoaView();
            return CreateView(data);
        }

        private ViewResult CreateView(AssetCoaView data)
        {
            ViewData["CoaTypeList"] = createCoaType((int)data.CoaType);
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(AssetCoaView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    AssetCoa newData = new AssetCoa("");
                    newData.COAID = data.COAID;
                    newData.COADescription = data.COADescription;
                    newData.CoaType = data.CoaType.GetHashCode().ToString();
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _assetCoaRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(AssetCoaView data, string mode)
        {
            if (data.COAID == "")
                return AssetCoaResources.Validation_Id;

            if (data.COADescription == "")
                return AssetCoaResources.Validation_Deskripsi;

            if (data.CoaTypeString == "")
                return AssetCoaResources.Validation_CoaType;

            if (data.Status.IsNull())
                return AssetCoaResources.Validation_Status;

            if (mode == "Create")
            {
                if (_assetCoaRepository.IsDuplicate(data.COAID))
                    return AssetCoaResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            AssetCoa assetarea = _assetCoaRepository.getAssetCoa(id);
            AssetCoaView data = ConvertFrom(assetarea);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(AssetCoaView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    AssetCoa newData = _assetCoaRepository.getAssetCoa(data.COAID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.COAID = data.COAID;
                    newData.COADescription = data.COADescription;
                    newData.CoaType = data.CoaTypeString;

                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _assetCoaRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                AssetCoa newData = _assetCoaRepository.getAssetCoa(Id);
                _assetCoaRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

    }
}