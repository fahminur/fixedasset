﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.FixedAsset.Controllers
{
    public class JenisUsahaController : PageController
    {
       
        private IJenisUsahaRepository _jenisUsahaRepository;
       

        private IAuditTrailLog _auditRepository;
        public JenisUsahaController(ISessionAuthentication sessionAuthentication, IJenisUsahaRepository jenisUsahaRepository, 
            IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._jenisUsahaRepository = jenisUsahaRepository;
            //this._groupAssetRepository = groupAssetRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Jenis Usaha";
            Settings.Title = JenisUsahaResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<JenisUsaha> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _jenisUsahaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _jenisUsahaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<JenisUsahaView>(new Converter<JenisUsaha, JenisUsahaView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public JenisUsahaView ConvertFrom(JenisUsaha item)
        {
            JenisUsahaView returnItem = new JenisUsahaView();
          //  GroupAsset groupAsset = _groupAssetRepository.getGroupAsset(item.JenisHartaID);

            returnItem.JenisUsahaID = item.JenisUsahaID;
            returnItem.NamaJenisUsaha = item.NamaJenisUsaha;
            //returnItem.JenisHartaID = item.JenisHartaID;
            //returnItem.JenisHartaString = groupAsset.GroupAssetDescription;
            returnItem.JenisUsahaShortDesc = item.JenisUsahaShortDesc;
            returnItem.Status = item.Status;
            return returnItem;
        }


        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            JenisUsahaView data = new JenisUsahaView();
            return CreateView(data);
        }

        private ViewResult CreateView(JenisUsahaView data)
        {
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(JenisUsahaView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    JenisUsaha newData = new JenisUsaha("");
                    newData.JenisUsahaID = data.JenisUsahaID;
                    newData.NamaJenisUsaha = data.NamaJenisUsaha;              
                    newData.JenisUsahaShortDesc = data.JenisUsahaShortDesc;                   
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _jenisUsahaRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(JenisUsahaView data, string mode)
        {
            if (data.JenisUsahaID == "")
                return JenisUsahaResources.Validation_Id;

            if (data.NamaJenisUsaha == "")
                return JenisUsahaResources.Validation_nama;
            
            if (data.JenisUsahaShortDesc == "")
                return JenisUsahaResources.Validation_shortdesc;

            if (data.Status.IsNull())
                return JenisUsahaResources.Validation_Status;

            if (mode == "Create")
            {
                if (_jenisUsahaRepository.IsDuplicate(data.JenisUsahaID))
                    return JenisUsahaResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            JenisUsaha jenisusaha = _jenisUsahaRepository.getJenisUsaha(id);
            JenisUsahaView data = ConvertFrom(jenisusaha);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(JenisUsahaView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    JenisUsaha newData = _jenisUsahaRepository.getJenisUsaha(data.JenisUsahaID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.JenisUsahaID = data.JenisUsahaID;
                    newData.NamaJenisUsaha = data.NamaJenisUsaha;
                    newData.JenisUsahaShortDesc = data.JenisUsahaShortDesc;

                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _jenisUsahaRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                JenisUsaha newData = _jenisUsahaRepository.getJenisUsaha(Id);
                _jenisUsahaRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}