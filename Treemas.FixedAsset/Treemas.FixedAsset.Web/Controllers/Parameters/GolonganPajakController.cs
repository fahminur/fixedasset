﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.FixedAsset.Controllers
{
    public class GolonganPajakController : PageController
    {
        private IGolonganPajakRepository _golonganPajakRepository;
        private IAuditTrailLog _auditRepository;

        public GolonganPajakController(ISessionAuthentication sessionAuthentication, IGolonganPajakRepository golonganPajakRepository,
               IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._golonganPajakRepository = golonganPajakRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Golongan Pajak";
            Settings.Title = GolonganPajakResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<GolonganPajak> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _golonganPajakRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _golonganPajakRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<GolonganPajakView>(new Converter<GolonganPajak, GolonganPajakView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public GolonganPajakView ConvertFrom(GolonganPajak item)
        {
            GolonganPajakView returnItem = new GolonganPajakView();
            returnItem.GolonganPajakID = item.GolonganPajakID;
            returnItem.GolonganPajakDescription = item.GolonganPajakDescription;
            returnItem.SLPercentage = item.SLPercentage;
            returnItem.DDPercentage = item.DDPercentage;
            returnItem.MasaManfaat = item.MasaManfaat;
            returnItem.Status = item.Status;
            return returnItem;
        }


        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            GolonganPajakView data = new GolonganPajakView();
            return CreateView(data);
        }

        private ViewResult CreateView(GolonganPajakView data)
        {
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(GolonganPajakView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    GolonganPajak newData = new GolonganPajak("");

                    newData.GolonganPajakID = data.GolonganPajakID;
                    newData.GolonganPajakDescription = data.GolonganPajakDescription;
                    newData.SLPercentage = data.SLPercentage;
                    newData.DDPercentage = data.DDPercentage;
                    newData.MasaManfaat = data.MasaManfaat;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _golonganPajakRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(GolonganPajakView data, string mode)
        {
            if (data.GolonganPajakID == "")
                return GolonganPajakResources.Validation_Id;

            if (data.GolonganPajakDescription == "")
                return GolonganPajakResources.Validation_Description;

            if (data.SLPercentage == 0)
                return GolonganPajakResources.Validation_SL;

            if (data.DDPercentage == 0)
                return GolonganPajakResources.Validation_DD;

            if (data.MasaManfaat == 0)
                return GolonganPajakResources.Validation_MasaManfaat;

            if (data.Status.IsNull())
                return GolonganPajakResources.Validation_Status;

            if (mode == "Create")
            {
                if (_golonganPajakRepository.IsDuplicate(data.GolonganPajakID))
                    return GolonganPajakResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            GolonganPajak golonganPajak = _golonganPajakRepository.getGolonganPajak(id);
            GolonganPajakView data = ConvertFrom(golonganPajak);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(GolonganPajakView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    GolonganPajak newData = _golonganPajakRepository.getGolonganPajak(data.GolonganPajakID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.GolonganPajakID = data.GolonganPajakID;
                    newData.GolonganPajakDescription = data.GolonganPajakDescription;
                    newData.SLPercentage = data.SLPercentage;
                    newData.DDPercentage = data.DDPercentage;
                    newData.MasaManfaat = data.MasaManfaat;
                    newData.Status = data.Status;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _golonganPajakRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                GolonganPajak newData = _golonganPajakRepository.getGolonganPajak(Id);
                _golonganPajakRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}