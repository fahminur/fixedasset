﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using System.Globalization;

namespace Treemas.FixedAsset.Controllers
{
    public class MutasiAktivaController : PageController
    {

        private IMutasiAktivaRepository _mutasiAktivaRepository;
        private IAuditTrailLog _auditRepository;

        private IGroupAssetRepository _groupAssetRepository;
        private ISubGroupAssetRepository _subGroupAssetRepository;
        private IAssetBranchRepository _assetBranchRepository;
        private IAssetLocationRepository _assetLocationRepository;
        private IAssetAreaRepository _assetAreaRepository;
        private IAssetDeptRepository _assetDeptRepository;
        private IAssetUserRepository _assetUserRepository;
        private IAssetConditionRepository _assetCondRepository;

        private IFixedAssetMasterRepository _fixedAssetMasterRepo;

        public MutasiAktivaController(ISessionAuthentication sessionAuthentication, IMutasiAktivaRepository mutasiAktivaRepository, IAuditTrailLog auditRepository,
            IGroupAssetRepository groupAssetRepository, ISubGroupAssetRepository subGroupAssetRepository, IAssetBranchRepository assetBranchRepository,
            IAssetLocationRepository assetLocationRepository, IAssetAreaRepository assetAreaRepository, IAssetDeptRepository assetDeptRepository, IAssetUserRepository assetUserRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepo, IAssetConditionRepository assetCondRepository)
            : base(sessionAuthentication)
        {
            this._mutasiAktivaRepository = mutasiAktivaRepository;
            this._auditRepository = auditRepository;

            this._groupAssetRepository = groupAssetRepository;
            this._subGroupAssetRepository = subGroupAssetRepository;
            this._assetBranchRepository = assetBranchRepository;
            this._assetLocationRepository = assetLocationRepository;
            this._assetAreaRepository = assetAreaRepository;
            this._assetDeptRepository = assetDeptRepository;
            this._assetUserRepository = assetUserRepository;
            this._assetCondRepository = assetCondRepository;

            this._fixedAssetMasterRepo = fixedAssetMasterRepo;

            Settings.ModuleName = "Mutasi Aktiva";
            Settings.Title = MutasiAktivaResources.PageTitle;
        }
        protected override void Startup()
        {
            ViewData["GroupAssetList"] = createGroupAssetSelect("");
            ViewData["SubGroupAssetList"] = createSubGroupAssetSelect("");
            ViewData["AssetBranchList"] = createAssetBranchSelect("");
            ViewData["AssetLokasiList"] = createAssetLocationSelect("");
            ViewData["AssetAreaList"] = createAssetAreaSelect("");
            ViewData["AssetDeptList"] = createAssetDeptSelect("");
            ViewData["AssetUserList"] = createAssetUserSelect("");
        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            try
            {
                MutasiAktivaFilter filter = new MutasiAktivaFilter();
                filter.AktivaID = Request.QueryString.GetValues("AktivaID")[0];
                //filter.NamaAktiva = Request.QueryString.GetValues("NamaAktiva")[0];
                filter.GroupAssetID = Request.QueryString["GroupAssetID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("GroupAssetID")[0];
                filter.SubGroupAssetID = Request.QueryString["SubGroupAssetID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("SubGroupAssetID")[0];
                filter.TanggalMutasiFrom = !Request.QueryString["TanggalMutasiFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalMutasiFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.TanggalMutasiTo = !Request.QueryString["TanggalMutasiTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalMutasiTo")[0], "d/M/yyyy", culture) : (DateTime?)null;

                // Initialization.   
                //string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                //string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<MutasiAktiva> data;
                data = _mutasiAktivaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                //}
                //else
                //{
                //    data = _mutasiAktivaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                //}

                var items = data.Items.ToList().ConvertAll<MutasiAktivaView>(new Converter<MutasiAktiva, MutasiAktivaView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckSubGroupAsset(string GroupAssetID)
        {
            List<SubGroupAsset> itemList = null;
            itemList = _subGroupAssetRepository.FindByGroupAsset(GroupAssetID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<SubGroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetActivaDetail(string AktivaID)
        {
            MutasiAktivaView returnItem = new MutasiAktivaView();
            FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(AktivaID);
            AssetBranch cabangDari = _assetBranchRepository.getAssetBranch(fixedAsset.SiteID);
            AssetLocation lokasiDari = _assetLocationRepository.getAssetLocation(fixedAsset.AssetLocationID);
            AssetArea areaDari = _assetAreaRepository.getAssetArea(fixedAsset.AssetAreaID);
            AssetDept deptDari = _assetDeptRepository.getAssetDept(fixedAsset.AssetDepID);
            AssetUser pemakaiDari = _assetUserRepository.getAssetUser(fixedAsset.AssetUserID);
            AssetCondition kondisiDari = _assetCondRepository.getAssetCondition(fixedAsset.AssetConditionID);

            returnItem.NamaAktiva = fixedAsset.NamaAktiva;
            returnItem.GroupAssetID = fixedAsset.GroupAssetID;
            returnItem.SubGroupAssetID = fixedAsset.SubGroupAssetID;
            returnItem.DariCabangID = fixedAsset.SiteID;
            returnItem.DariLokasi = fixedAsset.Lokasi.IsNull() ? "" : fixedAsset.Lokasi.Trim();
            //returnItem.DariLokasiID = fixedAsset.AssetLocationID;
            //returnItem.DariAreaID = fixedAsset.AssetAreaID;
            returnItem.DariDepartmentID = fixedAsset.AssetDepID;
            returnItem.DariPemakaiID = fixedAsset.AssetUserID;
            returnItem.DariKondisiID = fixedAsset.AssetConditionID;
            returnItem.DariCabangName = cabangDari.IsNull() ? "" : cabangDari.AssetBranchDescription;
            returnItem.DariLokasiName = lokasiDari.IsNull() ? "" : lokasiDari.AssetLocationDescription.Trim();
            returnItem.DariAreaName = areaDari.IsNull() ? "" : areaDari.AssetAreaDescription.Trim();
            returnItem.DariDepartmentName = deptDari.IsNull() ? "" : deptDari.AssetDeptDescription.Trim();
            returnItem.DariPemakaiName = pemakaiDari.IsNull() ? "" : pemakaiDari.AssetUserName.Trim();
            returnItem.DariKondisiName = kondisiDari.IsNull() ? "" : kondisiDari.AssetConditionDescription.Trim();
            returnItem.DariKodeIndukAktivaID = fixedAsset.ParentAktivaID;

            return Json(returnItem, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetUserByBranch(string BrachID)
        {
            List<AssetUser> itemList = null;
            itemList = _assetUserRepository.FindByBranchID(BrachID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetUser, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetUserByDept(string BrachID, string DeptID)
        {
            List<AssetUser> itemList = null;
            itemList = _assetUserRepository.FindByDeptID(BrachID, DeptID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetUser, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        public MutasiAktivaView ConvertFrom(MutasiAktiva item)
        {
            MutasiAktivaView returnItem = new MutasiAktivaView();
            FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(item.AktivaID);
            AssetBranch cabangDari = _assetBranchRepository.getAssetBranch(item.DariCabangID);
            AssetBranch cabangKe = _assetBranchRepository.getAssetBranch(item.KeCabangID);
            //AssetLocation lokasiDari = _assetLocationRepository.getAssetLocation(item.DariLokasiID);
            //AssetLocation lokasiKe = _assetLocationRepository.getAssetLocation(item.KeLokasiID);
            //AssetArea areaDari = _assetAreaRepository.getAssetArea(item.DariAreaID);
            //AssetArea areaKe = _assetAreaRepository.getAssetArea(item.KeAreaID);
            AssetDept deptDari = _assetDeptRepository.getAssetDept(item.DariDepartmentID);
            AssetDept deptKe = _assetDeptRepository.getAssetDept(item.KeDepartmentID);
            AssetUser pemakaiDari = _assetUserRepository.getAssetUser(item.DariPemakaiID);
            AssetUser pemakaiKe = _assetUserRepository.getAssetUser(item.KePemakaiID);
            AssetCondition kondisiDari = _assetCondRepository.getAssetCondition(item.DariKondisiID);
            AssetCondition kondisiKe = _assetCondRepository.getAssetCondition(item.KeKondisiID);

            returnItem.AktivaID = item.AktivaID;
            returnItem.NamaAktiva = item.NamaAktiva;
            returnItem.TanggalMutasi = item.TanggalMutasi.ToString("dd/MM/yyyy");
            returnItem.TanggalMutasiString = item.TanggalMutasi.ToString("dd/MM/yyyy");
            returnItem.Periode = item.Periode;

            returnItem.DariCabangID = item.DariCabangID;
            returnItem.DariLokasi = item.DariLokasi;
            //returnItem.DariLokasiID = item.DariLokasiID;
            //returnItem.DariAreaID = item.DariAreaID;
            returnItem.DariDepartmentID = item.DariDepartmentID;
            returnItem.DariPemakaiID = item.DariPemakaiID;
            returnItem.DariKondisiID = item.DariKondisiID;

            returnItem.DariKodeIndukAktivaID = item.DariKodeIndukAktivaID;

            returnItem.DariCabangName = cabangDari.IsNull() ? "" : cabangDari.AssetBranchDescription.Trim();
            //returnItem.DariLokasiName = lokasiDari.IsNull() ? "" : lokasiDari.AssetLocationDescription.Trim();
            //returnItem.DariAreaName = areaDari.IsNull() ? "" : areaDari.AssetAreaDescription.Trim();
            returnItem.DariDepartmentName = deptDari.IsNull() ? "" : deptDari.AssetDeptDescription.Trim();
            returnItem.DariPemakaiName = pemakaiDari.IsNull() ? "" : pemakaiDari.AssetUserName.Trim();
            returnItem.DariKondisiName = kondisiDari.IsNull() ? "" : kondisiDari.AssetConditionDescription.Trim();

            returnItem.KeAktivaID = item.KeAktivaID;
            returnItem.KeCabangID = item.KeCabangID;
            returnItem.KeLokasi = item.KeLokasi;
            //returnItem.KeLokasiID = item.KeLokasiID;
            //returnItem.KeAreaID = item.KeAreaID;
            returnItem.KeDepartmentID = item.KeDepartmentID;
            returnItem.KePemakaiID = item.KePemakaiID;
            returnItem.KeKondisiID = item.KeKondisiID;

            returnItem.KeCabangName = cabangKe.IsNull() ? "" : cabangKe.AssetBranchDescription.Trim();
            //returnItem.KeLokasiName = lokasiKe.IsNull() ? "" : lokasiKe.AssetLocationDescription.Trim();
            //returnItem.KeAreaName = areaKe.IsNull() ? "" : areaKe.AssetAreaDescription.Trim();
            returnItem.KeDepartmentName = deptKe.IsNull() ? "" : deptKe.AssetDeptDescription.Trim();
            returnItem.KePemakaiName = pemakaiKe.IsNull() ? "" : pemakaiKe.AssetUserName.Trim();
            returnItem.KeKondisiName = kondisiKe.IsNull() ? "" : kondisiKe.AssetConditionDescription.Trim();

            returnItem.KeKodeIndukAktivaID = item.KeKodeIndukAktivaID;

            returnItem.NoReferensi = item.NoReferensi;
            returnItem.Keterangan = item.Keterangan;

            if (!fixedAsset.IsNull())
            {
                returnItem.GroupAssetID = fixedAsset.GroupAssetID;
                returnItem.SubGroupAssetID = fixedAsset.SubGroupAssetID;
            }
            return returnItem;
        }

        //==================================================== Group Asset ==================================================================
        private IList<SelectListItem> createGroupAssetSelect(string selected)
        {
            List<GroupAsset> itemList = Lookup.Get<List<GroupAsset>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _groupAssetRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(GroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GroupAssetDescription;
            returnItem.Value = item.GroupAssetID;
            return returnItem;
        }

        //================================================================end====================================================================

        //==================================================== Sub Group Asset ==================================================================
        private IList<SelectListItem> createSubGroupAssetSelect(string selected)
        {
            List<SubGroupAsset> itemList = Lookup.Get<List<SubGroupAsset>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _subGroupAssetRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<SubGroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(SubGroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.SubGroupAssetDescription;
            returnItem.Value = item.SubGroupAssetID;
            return returnItem;
        }

        //================================================================end====================================================================

        //====================================================  Asset  Branch  ==================================================================
        private IList<SelectListItem> createAssetBranchSelect(string selected)
        {
            List<AssetBranch> itemList = null;// Lookup.Get<List<AssetBranch>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetBranchRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetBranch, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetBranch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetBranchDescription;
            returnItem.Value = item.AssetBranchID;
            return returnItem;
        }

        //================================================================end====================================================================

        //====================================================  Asset  Location==================================================================
        private IList<SelectListItem> createAssetLocationSelect(string selected)
        {
            List<AssetLocation> itemList = null;// Lookup.Get<List<AssetLocation>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetLocationRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetLocation, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetLocation item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetLocationDescription;
            returnItem.Value = item.AssetLocationID;
            return returnItem;
        }

        //================================================================end====================================================================

        //====================================================  Asset Area ==================================================================
        private IList<SelectListItem> createAssetAreaSelect(string selected)
        {
            List<AssetArea> itemList = Lookup.Get<List<AssetArea>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetAreaRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetArea, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetArea item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetAreaDescription;
            returnItem.Value = item.AssetAreaID;
            return returnItem;
        }

        //================================================================end====================================================================


        //====================================================  Asset Dept ==================================================================
        private IList<SelectListItem> createAssetDeptSelect(string selected)
        {
            List<AssetDept> itemList = null;// Lookup.Get<List<AssetDept>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetDeptRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetDept, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetDept item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetDeptDescription;
            returnItem.Value = item.AssetDeptID;
            return returnItem;
        }

        //================================================================end====================================================================


        //==================================================== Asset User ==================================================================
        private IList<SelectListItem> createAssetUserSelect(string selected)
        {
            List<AssetUser> itemList = null;// Lookup.Get<List<AssetUser>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetUserRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetUser, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetUser item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetUserName;
            returnItem.Value = item.AssetUserID;
            return returnItem;
        }

        //================================================================end====================================================================

        //==================================================== Asset Condition ==================================================================
        private IList<SelectListItem> createAssetCondSelect(string selected)
        {
            List<AssetCondition> itemList = null;// Lookup.Get<List<AssetCondition>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetCondRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetCondition, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetCondition item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetConditionDescription;
            returnItem.Value = item.AssetConditionID;
            return returnItem;
        }

        //================================================================end====================================================================

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            MutasiAktivaView data = new MutasiAktivaView();
            data.TanggalMutasi = DateTime.Now.ToString("dd/MM/yyyy"); ;
            return CreateView(data);
        }

        private ViewResult CreateView(MutasiAktivaView data)
        {
            ViewData["AssetBranchToList"] = createAssetBranchSelect(data.KeCabangID);
            //ViewData["AssetLokasiToList"] = createAssetLocationSelect(data.KeLokasiID);
            //ViewData["AssetAreaToList"] = createAssetAreaSelect(data.KeAreaID);
            ViewData["AssetDeptToList"] = createAssetDeptSelect(data.KeDepartmentID);
            ViewData["AssetUserToList"] = createAssetUserSelect(data.KePemakaiID);
            ViewData["AssetConditionToList"] = createAssetCondSelect(data.KeKondisiID);
            //ViewData["AssetLokasiFromList"] = createAssetLocationSelect(data.DariLokasiID);
            //ViewData["AssetAreaFromList"] = createAssetAreaSelect(data.DariAreaID);
            ViewData["AssetDeptFromList"] = createAssetDeptSelect(data.DariDepartmentID);
            ViewData["AssetUserFromList"] = createAssetUserSelect(data.DariPemakaiID);
            ViewData["AssetConditionFromList"] = createAssetCondSelect(data.DariKondisiID);
            ViewData["AssetBranchFromList"] = createAssetBranchSelect(data.DariCabangID);

            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(MutasiAktivaView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    MutasiAktiva newData = new MutasiAktiva("");
                    FixedAssetMaster newAsset = _fixedAssetMasterRepo.getFixedAssetMaster(data.AktivaID);

                    string[] CurAktiva = data.AktivaID.Trim().Split('-');
                    CurAktiva[1] = data.KeCabangID.Trim();
                    string NewAktivaID = string.Join("-", CurAktiva);

                    newData.AktivaID = data.AktivaID;
                    newData.NamaAktiva = data.NamaAktiva;
                    newData.TanggalMutasi = DateTime.ParseExact(data.TanggalMutasi, "d/M/yyyy", culture);
                    newData.Periode = data.Periode;
                    newData.DariCabangID = data.DariCabangID;
                    newData.DariLokasi = data.DariLokasi;
                    //newData.DariLokasiID = data.DariLokasiID;
                    //newData.DariAreaID = data.DariAreaID;
                    newData.DariDepartmentID = data.DariDepartmentID;
                    newData.DariPemakaiID = data.DariPemakaiID;
                    newData.DariKondisiID = data.DariKondisiID;
                    //newData.DariKodeIndukAktivaID = data.DariKodeIndukAktivaID;
                    newData.KeAktivaID = NewAktivaID;
                    newData.KeCabangID = data.KeCabangID;
                    newData.KeLokasi = data.KeLokasi;
                    //newData.KeLokasiID = data.KeLokasiID;
                    //newData.KeAreaID = data.KeAreaID;
                    newData.KeDepartmentID = data.KeDepartmentID;
                    newData.KePemakaiID = data.KePemakaiID;
                    newData.KeKondisiID = data.KeKondisiID;
                    //newData.KeKodeIndukAktivaID = data.KeKodeIndukAktivaID;
                    newData.NoReferensi = data.NoReferensi;
                    newData.Keterangan = data.Keterangan;
                    newData.GroupAssetID = newAsset.GroupAssetID;
                    newData.SubGroupAssetID = newAsset.SubGroupAssetID;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;


                    _mutasiAktivaRepository.Add(newData);
                    _fixedAssetMasterRepo.Remove(newAsset);

                    newAsset.AktivaID = NewAktivaID;
                    newAsset.SiteID = data.KeCabangID;
                    newAsset.Lokasi = data.KeLokasi;
                    //newAsset.AssetLocationID = data.KeLokasiID;
                    //newAsset.AssetAreaID = data.KeAreaID;
                    newAsset.AssetDepID = data.KeDepartmentID;
                    newAsset.AssetUserID = data.KePemakaiID;
                    newAsset.AssetConditionID = data.KeKondisiID;
                    newAsset.UpdateUser = user.Username;
                    newAsset.UpdateDate = DateTime.Now;
                    _fixedAssetMasterRepo.Add(newAsset);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Save_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(MutasiAktivaView data, string mode)
        {
            if (data.AktivaID.IsNullOrEmpty())
                return MutasiAktivaResources.Validation_Id;

            if (data.Periode.IsNullOrEmpty())
                return MutasiAktivaResources.Validation_Periode;

            if (data.TanggalMutasi.IsNull())
                return MutasiAktivaResources.Validation_TanggalMutasi;

            if (mode == "Create")
            {
                if (_mutasiAktivaRepository.IsDuplicate(data.AktivaID))
                    return MutasiAktivaResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            MutasiAktiva mutasi = _mutasiAktivaRepository.getMutasiAktiva(id);
            MutasiAktivaView data = ConvertFrom(mutasi);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(MutasiAktivaView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    MutasiAktiva newData = _mutasiAktivaRepository.getMutasiAktiva(data.AktivaID);
                    FixedAssetMaster newAsset = _fixedAssetMasterRepo.getFixedAssetMaster(data.AktivaID);

                    string[] CurAktiva = data.AktivaID.Trim().Split('-');
                    CurAktiva[1] = data.KeCabangID.Trim();
                    string NewAktivaID = string.Join("-", CurAktiva);

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.AktivaID = data.AktivaID;
                    newData.NamaAktiva = data.NamaAktiva;
                    newData.TanggalMutasi = DateTime.ParseExact(data.TanggalMutasi, "d/M/yyyy", culture);
                    newData.Periode = data.Periode;
                    newData.DariCabangID = data.DariCabangID;
                    newData.DariLokasi = data.DariLokasi;
                    //newData.DariLokasiID = data.DariLokasiID;
                    //newData.DariAreaID = data.DariAreaID;
                    newData.DariDepartmentID = data.DariDepartmentID;
                    newData.DariPemakaiID = data.DariPemakaiID;
                    newData.DariKondisiID = data.DariKondisiID;
                    //newData.DariKodeIndukAktivaID = data.DariKodeIndukAktivaID;
                    newData.KeAktivaID = NewAktivaID;
                    newData.KeCabangID = data.KeCabangID;
                    newData.KeLokasi = data.KeLokasi;
                    //newData.KeLokasiID = data.KeLokasiID;
                    //newData.KeAreaID = data.KeAreaID;
                    newData.KeDepartmentID = data.KeDepartmentID;
                    newData.KePemakaiID = data.KePemakaiID;
                    newData.KeKondisiID = data.KeKondisiID;
                    //newData.KeKodeIndukAktivaID = data.KeKodeIndukAktivaID;
                    newData.NoReferensi = data.NoReferensi;
                    newData.Keterangan = data.Keterangan;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _mutasiAktivaRepository.Save(newData);
                    _fixedAssetMasterRepo.Remove(newAsset);

                    newAsset.AktivaID = NewAktivaID;
                    newAsset.SiteID = data.KeCabangID;
                    newAsset.Lokasi = data.KeLokasi;
                    newAsset.AssetDepID = data.KeDepartmentID;
                    newAsset.AssetUserID = data.KePemakaiID;
                    newAsset.AssetConditionID = data.KeKondisiID;
                    newAsset.UpdateUser = user.Username;
                    newAsset.UpdateDate = DateTime.Now;

                    _fixedAssetMasterRepo.Add(newAsset);

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Update_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                MutasiAktiva newData = _mutasiAktivaRepository.getMutasiAktiva(Id);
                _mutasiAktivaRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

    }
}