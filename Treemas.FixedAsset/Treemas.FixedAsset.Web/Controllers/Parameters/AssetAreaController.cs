﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.FixedAsset.Controllers
{
    public class AssetAreaController : PageController
    {
       
        private IAssetAreaRepository _assetAreaRepository;
        private IAssetBranchRepository _assetBranchRepository;

        private IAuditTrailLog _auditRepository;
        public AssetAreaController(ISessionAuthentication sessionAuthentication, IAssetAreaRepository assetAreaRepository, 
            IAssetBranchRepository assetBranchRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._assetAreaRepository = assetAreaRepository;
            this._assetBranchRepository = assetBranchRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Asset Area";
            Settings.Title = AssetAreaResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<AssetArea> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _assetAreaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _assetAreaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<AssetAreaView>(new Converter<AssetArea, AssetAreaView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public AssetAreaView ConvertFrom(AssetArea item)
        {
            AssetAreaView returnItem = new AssetAreaView();
            AssetBranch assetbranch = _assetBranchRepository.getAssetBranch(item.AssetBranchID);

            returnItem.AssetAreaID = item.AssetAreaID;
            returnItem.AssetAreaDescription = item.AssetAreaDescription;
            returnItem.AssetAreaShortDesc = item.AssetAreaShortDesc;
            returnItem.AssetBranchID = item.AssetBranchID;
            returnItem.AssetBranchString = assetbranch.AssetBranchDescription;           
            returnItem.Status = item.Status;
            return returnItem;
        }

        private IList<SelectListItem> createAssetBranch(string selected)
        {
            List<AssetBranch> itemList = null;//Lookup.Get<List<AssetBranch>>();
            
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetBranchRepository.findActive().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }
         

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetBranch, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
           
               
            if (!selected.IsNullOrEmpty())
              {
                 dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
              }
              return dataList;
                       
        }

        public SelectListItem ConvertFrom(AssetBranch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetBranchDescription;
            returnItem.Value = item.AssetBranchID;
            return returnItem;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            AssetAreaView data = new AssetAreaView();
            return CreateView(data);
        }

        private ViewResult CreateView(AssetAreaView data)
        {
            ViewData["AssetAreaList"] = createAssetBranch(data.AssetBranchID);
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(AssetAreaView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    AssetArea newData = new AssetArea("");
                    newData.AssetAreaID = data.AssetAreaID;
                    newData.AssetAreaDescription = data.AssetAreaDescription;
                    newData.AssetAreaShortDesc = data.AssetAreaShortDesc;
                    newData.AssetBranchID = data.AssetBranchID;                    
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _assetAreaRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(AssetAreaView data, string mode)
        {
            if (data.AssetAreaID == "")
                return AssetAreaResources.Validation_Id;

            if (data.AssetAreaDescription == "")
                return AssetAreaResources.Validation_Description;

            if (data.AssetAreaShortDesc == "")
                return AssetAreaResources.Validation_ShortDesc;

            if (data.AssetBranchID.Trim().IsNullOrEmpty())
                return AssetAreaResources.Validation_CoaClass;
       
            if (data.Status.IsNull())
                return AssetAreaResources.Validation_Status;

            if (mode == "Create")
            {
                if (_assetAreaRepository.IsDuplicate(data.AssetAreaID))
                    return AssetAreaResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            AssetArea assetarea = _assetAreaRepository.getAssetArea(id);
            AssetAreaView data = ConvertFrom(assetarea);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(AssetAreaView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    AssetArea newData = _assetAreaRepository.getAssetArea(data.AssetAreaID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.AssetAreaID = data.AssetAreaID;
                    newData.AssetAreaDescription = data.AssetAreaDescription;
                    newData.AssetAreaShortDesc = data.AssetAreaShortDesc;
                    newData.AssetBranchID = data.AssetBranchID;                

                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _assetAreaRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                AssetArea newData = _assetAreaRepository.getAssetArea(Id);
                _assetAreaRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}