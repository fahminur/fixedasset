﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;

using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using System.Globalization;


namespace Treemas.FixedAsset.Parameters
{
    public class GLJournalController : PageController
    {
        private IGLJournalRepository _gljRepo;
        private IGLJournalTypeRepository _gljTypeRepo;
        private IAssetBranchRepository _branchRepository;

        private string _gljTypeID;

        public GLJournalController(ISessionAuthentication sessionAuthentication, IGLJournalRepository gljRepo, IGLJournalTypeRepository gljTypeRepo,
            IAssetBranchRepository branchRepository) : base(sessionAuthentication)
        {
            _gljRepo = gljRepo;
            _gljTypeRepo = gljTypeRepo;
            _branchRepository = branchRepository;

            Settings.ModuleName = "GL Journal";
            Settings.Title = "GL Journal";
        }
        protected override void Startup()
        {
            ViewData["GlJournalType"] = GlJournalTypeSelect("");
            ViewData["SiteList"] = createSitetSelect(" ");

        }

        public ActionResult Search()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                GLJFilter filter = new GLJFilter();
                filter.BranchID = Request.QueryString.GetValues("BranchID")[0];
                filter.TransactionID = Request.QueryString["TransactionID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("TransactionID")[0];
                filter.Tr_Nomor = Request.QueryString.GetValues("Tr_Nomor")[0];
                filter.Tr_DateFrom = !Request.QueryString["Tr_DateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("Tr_DateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.Tr_DateTo = !Request.QueryString["Tr_DateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("Tr_DateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<GLJournalH> data;
                data = _gljRepo.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<GLJournalHView>(new Converter<GLJournalH, GLJournalHView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        private GLJournalHView ConvertFrom(GLJournalH input)
        {
            GLJournalHView gljH = new GLJournalHView();
            AssetBranch branch = _branchRepository.getAssetBranch(input.BranchID);

            string strLink = "";
            switch (input.TransactionID.Trim())
            {
                case "RIV":
                case "RAP":
                    strLink = "Invoice/Edit/" + input.Reff_No;
                    break;
                case "RPA":
                    strLink = "PenjualanAktiva/Edit/" + input.Reff_No;
                    break;
                default:
                    strLink = "#";
                    break;
            }

            gljH.CompanyID = input.CompanyID;
            gljH.BranchID = input.BranchID;
            gljH.BranchName = branch.IsNull() ? input.BranchID : branch.AssetBranchDescription;
            gljH.Tr_Nomor = input.Tr_Nomor;
            gljH.PeriodYear = input.PeriodYear;
            gljH.PeriodMonth = input.PeriodMonth;
            gljH.TransactionID = input.TransactionID;
            gljH.tr_Date1 = Convert.ToDateTime(input.tr_Date1).ToString("dd/MM/yyyy");
            gljH.Tr_Date = input.Tr_Date.ToString("dd/MM/yyyy");
            gljH.Reff_No = input.Reff_No;
            gljH.Reff_Link = strLink;
            gljH.Reff_Date = input.Reff_Date.ToString("dd/MM/yyyy");
            gljH.Tr_Desc = input.Tr_Desc;
            gljH.JournalAmount = input.JournalAmount.ToString("N0");
            gljH.BatchID = input.BatchID;
            gljH.SubSystem = input.SubSystem;
            gljH.Status_Tr = input.Status_Tr;
            gljH.IsActive = input.IsActive;
            gljH.Flag = input.Flag;
            gljH.IsValid = input.IsValid;
            gljH.DepresiasiTenor = input.DepresiasiTenor;
            gljH.SisaTenor = input.SisaTenor;
            gljH.PrintBy = input.PrintBy;
            gljH.IsPrint = input.IsPrint;
            gljH.Jenis_Reff = input.Jenis_Reff;
            gljH.PostingDate = input.PostingDate;
            gljH.PostingBy = input.PostingBy;
            gljH.LastRevisiPosting = Convert.ToDateTime(input.LastRevisiPosting).ToString("dd/MM/yyyy");
            gljH.CountRevisiPosting = input.CountRevisiPosting;
            return gljH;
        }

        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            GLJournalH gljH = _gljRepo.GetJournalHdr(id);
            GLJournalHView data = ConvertFrom(gljH);

            IList<GLJournalD> gljDs = _gljRepo.getGLJournalDtl(data.Tr_Nomor);
            IList<GLJournalDView> gljDsView = gljDs.ToList().ConvertAll<GLJournalDView>(new Converter<GLJournalD, GLJournalDView>(ConvertFrom));
            data.GLJournalDtlList = gljDsView;

            return CreateView(data);
        }

        public GLJournalDView ConvertFrom(GLJournalD item)
        {
            GLJournalDView returnItem = new GLJournalDView();

            returnItem.CompanyID = item.CompanyID;
            returnItem.BranchID = item.BranchID;
            returnItem.Tr_Nomor = item.Tr_Nomor;
            returnItem.SequenceNo = item.SequenceNo;
            returnItem.CoaCo = item.CoaCo;
            returnItem.CoaBranch = item.CoaBranch;
            returnItem.CoaId = item.CoaId;
            returnItem.TransactionID = item.TransactionID;
            returnItem.Tr_Desc = item.Tr_Desc;
            returnItem.Post = item.Post;
            returnItem.PostDesc = item.Post == "D" ? "Debet" : "Kredit";
            returnItem.Amount = item.Amount.ToString("N0");
            returnItem.CoaId_X = item.CoaId_X;
            returnItem.PaymentAllocationId = item.PaymentAllocationId;
            returnItem.ProductId = item.ProductId;

            return returnItem;
        }

        private ActionResult CreateView(GLJournalHView data)
        {
            return View("Detail", data);
        }

        private IList<SelectListItem> GlJournalTypeSelect(string selected)
        {
            List<GLJournalType> itemList = Lookup.Get<List<GLJournalType>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _gljTypeRepo.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GLJournalType, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (itemList.Count > 0)
                _gljTypeID = itemList.FirstOrDefault().TransactionID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString())).Selected = true;
                _gljTypeID = selected;
            }

            return dataList;
        }

        public SelectListItem ConvertForm(GLJournalType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.TransactionDesc.Trim();
            returnItem.Value = item.TransactionID.ToString().Trim();
            return returnItem;
        }

        private IList<SelectListItem> createSitetSelect(string selected)
        {
            List<AssetBranch> itemList = null;// Lookup.Get<List<AssetBranch>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _branchRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetBranch, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetBranch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetBranchDescription;
            returnItem.Value = item.AssetBranchID.Trim();
            return returnItem;
        }

    }
}