﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.FixedAsset.Controllers
{
    public class SubGroupAssetController : PageController
    {

        private ISubGroupAssetRepository _subGroupAssetRepository;
        private IGroupAssetRepository _groupAssetRepository;
        private IGolonganPajakRepository _golonganPajakRepository;

        private IAuditTrailLog _auditRepository;
        public SubGroupAssetController(ISessionAuthentication sessionAuthentication, ISubGroupAssetRepository subGroupAssetRepository,
            IGroupAssetRepository groupAssetRepository, IGolonganPajakRepository golonganPajakRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._golonganPajakRepository = golonganPajakRepository;
            this._groupAssetRepository = groupAssetRepository;
            this._subGroupAssetRepository = subGroupAssetRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Sub Group Asset";
            Settings.Title = SubGroupAssetResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<SubGroupAsset> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _subGroupAssetRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _subGroupAssetRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<SubGroupAssetView>(new Converter<SubGroupAsset, SubGroupAssetView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SubGroupAssetView ConvertFrom(SubGroupAsset item)
        {
            SubGroupAssetView returnItem = new SubGroupAssetView();
            GroupAsset groupAsset = _groupAssetRepository.getGroupAsset(item.GroupAssetID);
            GolonganPajak golonganPajak = _golonganPajakRepository.getGolonganPajak(item.GolonganPajakID);

            returnItem.SubGroupAssetID = item.SubGroupAssetID;
            returnItem.SubGroupAssetDescription = item.SubGroupAssetDescription;
            returnItem.GroupAssetID = item.GroupAssetID;
            returnItem.GroupAssetString = groupAsset.GroupAssetDescription;
            returnItem.MasaManfaat = item.MasaManfaat;
            returnItem.GolonganPajakID = item.GolonganPajakID;
            if (!golonganPajak.IsNull())
                returnItem.GolonganPajakString = golonganPajak.GolonganPajakDescription;
            returnItem.Status = item.Status;
            return returnItem;
        }
        //================================================== GroupAsset  =============================================================================
        private IList<SelectListItem> createGroupAsset(string selected)
        {
            List<GroupAsset> itemList = Lookup.Get<List<GroupAsset>>();

            if (itemList.IsNullOrEmpty())
            {
                itemList = _groupAssetRepository.findActive().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }


            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });


            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;

        }

        public SelectListItem ConvertFrom(GroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GroupAssetDescription;
            returnItem.Value = item.GroupAssetID;
            return returnItem;
        }
        //===================================================== end Group Asset =======================================================================

        //==================================================== Golongan Pajak ==================================================================
        private IList<SelectListItem> createGolonganPajakSelect(string selected)
        {
            List<GolonganPajak> itemList = Lookup.Get<List<GolonganPajak>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _golonganPajakRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GolonganPajak, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(GolonganPajak item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GolonganPajakDescription;
            returnItem.Value = item.GolonganPajakID;
            return returnItem;
        }

        //================================================================end====================================================================



        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            SubGroupAssetView data = new SubGroupAssetView();
            return CreateView(data);
        }

        private ViewResult CreateView(SubGroupAssetView data)
        {
            ViewData["GroupAssetList"] = createGroupAsset(data.GroupAssetID);
            ViewData["GolonganPajakList"] = createGolonganPajakSelect(data.GolonganPajakID);
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(SubGroupAssetView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    SubGroupAsset newData = new SubGroupAsset("");

                    newData.SubGroupAssetID = data.SubGroupAssetID;
                    newData.SubGroupAssetDescription = data.SubGroupAssetDescription;
                    newData.GroupAssetID = data.GroupAssetID;
                    newData.MasaManfaat = data.MasaManfaat;
                    newData.GolonganPajakID = data.GolonganPajakID;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _subGroupAssetRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(SubGroupAssetView data, string mode)
        {
            if (data.SubGroupAssetID == "")
                return SubGroupAssetResources.Validation_Id;

            if (data.SubGroupAssetDescription == "")
                return SubGroupAssetResources.Validation_Description;

            if (data.GroupAssetID.Trim().IsNullOrEmpty())
                return SubGroupAssetResources.Validation_GroupAsset;

            if (data.MasaManfaat == 0)
                return SubGroupAssetResources.Validation_Masa;

            if (data.GolonganPajakID.Trim().IsNullOrEmpty())
                return SubGroupAssetResources.Validation_Pajak;

            if (data.Status.IsNull())
                return SubGroupAssetResources.Validation_Status;

            if (mode == "Create")
            {
                if (_subGroupAssetRepository.IsDuplicate(data.SubGroupAssetID))
                    return SubGroupAssetResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            SubGroupAsset subgroup = _subGroupAssetRepository.getSubGroupAsset(id);
            SubGroupAssetView data = ConvertFrom(subgroup);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(SubGroupAssetView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    SubGroupAsset newData = _subGroupAssetRepository.getSubGroupAsset(data.SubGroupAssetID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.SubGroupAssetID = data.SubGroupAssetID;
                    newData.SubGroupAssetDescription = data.SubGroupAssetDescription;
                    newData.GroupAssetID = data.GroupAssetID;
                    newData.MasaManfaat = data.MasaManfaat;
                    newData.GolonganPajakID = data.GolonganPajakID;

                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _subGroupAssetRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                SubGroupAsset newData = _subGroupAssetRepository.getSubGroupAsset(Id);
                _subGroupAssetRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}