﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Controllers
{
    public class PerawatanAktivaController : PageController
    {
        private IPerawatanAktivaRepository _perawatanAktivaRepository;
        private IAuditTrailLog _auditRepository;
        private IInvoiceRepository _invoiceRepository;
        private IFixedAssetMasterRepository _fixedAssetMasterRepo;
        private ICurrencyRepository _currRepository;
        private IVendorRepository _VendorRepository;
        private IGroupAssetRepository _groupAssetRepository;

        public PerawatanAktivaController(ISessionAuthentication sessionAuthentication, IPerawatanAktivaRepository perawatanAktivaRepository, IAuditTrailLog auditRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepo, ICurrencyRepository currRepository, IInvoiceRepository invoiceRepository, IVendorRepository VendorRepository,
            IGroupAssetRepository groupAssetRepository)
            : base(sessionAuthentication)
        {
            this._perawatanAktivaRepository = perawatanAktivaRepository;
            this._auditRepository = auditRepository;
            this._invoiceRepository = invoiceRepository;
            this._fixedAssetMasterRepo = fixedAssetMasterRepo;
            this._currRepository = currRepository;
            this._VendorRepository = VendorRepository;
            this._groupAssetRepository = groupAssetRepository;
            Settings.ModuleName = "Perawatan Aktiva";
            Settings.Title = PerawatanAktivaResources.PageTitle;
        }
        protected override void Startup()
        {
        }
        public ActionResult Search()
        {
            string Mode = Request.QueryString.GetValues("Mode")[0];
            string SearchType = Mode == "" ? "Perawatan" : Mode;
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            JsonResult result = new JsonResult();
            try
            {
                if (SearchType == "Perawatan")
                {
                    PerawatanAktivaFilter filter = new PerawatanAktivaFilter();
                    filter.AktivaID = Request.QueryString.GetValues("AktivaID")[0];
                    filter.VendorID = Request.QueryString.GetValues("VendorID")[0];
                    filter.NoReferensi = Request.QueryString.GetValues("NoReferensi")[0];
                    filter.TanggalPerawatanFrom = !Request.QueryString["TanggalPerawatanFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalPerawatanFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.TanggalPerawatanTo = !Request.QueryString["TanggalPerawatanTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalPerawatanTo")[0], "d/M/yyyy", culture) : (DateTime?)null; 

                    // Initialization.   
                    string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                    var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                    int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                    int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                    int pageNumber = (startRec + pageSize - 1) / pageSize;

                    // Loading.   
                    PagedResult<PerawatanAktiva> data;
                    data = _perawatanAktivaRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                    var items = data.Items.ToList().ConvertAll<PerawatanAktivaView>(new Converter<PerawatanAktiva, PerawatanAktivaView>(ConvertFrom));

                    result = this.Json(new
                    {
                        draw = Convert.ToInt32(draw),
                        recordsTotal = data.TotalItems,
                        recordsFiltered = data.TotalItems,
                        data = items
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public PerawatanAktivaView ConvertFrom(PerawatanAktiva item)
        {
            PerawatanAktivaView returnItem = new PerawatanAktivaView();
            FixedAssetMaster fixedAsset = _fixedAssetMasterRepo.getFixedAssetMaster(item.AktivaID);
            Currency currency = _currRepository.getCurrency(item.CurrencyID);
            Vendor sup = _VendorRepository.getVendor(item.VendorID);

            returnItem.AktivaID = item.AktivaID;
            returnItem.NamaAktiva = item.NamaAktiva;
            returnItem.TanggalPerawatan = item.TanggalPerawatan.ToString("dd/MM/yyyy");
            returnItem.TanggalPerawatanString = item.TanggalPerawatan.ToString("dd/MM/yyyy");
            returnItem.NoReferensi = item.NoReferensi;
            returnItem.Keterangan = item.Keterangan;
            returnItem.CurrencyID = item.CurrencyID;
            returnItem.CurrencyName = currency.IsNull() ? "" : currency.CurrencyDescription;
            returnItem.Kurs = item.Kurs.ToString("N0");
            returnItem.Biaya = item.Biaya.ToString("N0");
            returnItem.VendorID = item.VendorID;
            returnItem.VendorName = sup.IsNull() ? "" : sup.VendorName;
            returnItem.NoInvoice = item.NoInvoice;
            returnItem.MasaGaransi = item.MasaGaransi.ToString("dd/MM/yyyy");
            returnItem.MasaGaransiString = item.MasaGaransi.ToString("dd/MM/yyyy");
            returnItem.NilaiOriginal = item.NilaiOriginal.ToString("N0");
            returnItem.COA = item.COA;

            return returnItem;
        }

        public PerawatanAktivaDetailView ConvertFrom(PerawatanAktivaDetail item)
        {
            PerawatanAktivaDetailView returnItem = new PerawatanAktivaDetailView();

            returnItem.AktivaID = item.AktivaID;
            returnItem.NamaAktiva = item.NamaAktiva;
            returnItem.TanggalPerawatan = item.TanggalPerawatan.ToString("dd/MM/yyyy");
            returnItem.TanggalPerawatanString = item.TanggalPerawatan.ToString("dd/MM/yyyy");
            returnItem.Biaya = item.Biaya.ToString("N0");
            returnItem.VendorID = item.VendorID;
            returnItem.VendorName = item.VendorName;
            returnItem.NoInvoice = item.NoInvoice;
            returnItem.MasaGaransi = item.MasaGaransi.ToString("dd/MM/yyyy");
            returnItem.MasaGaransiString = item.MasaGaransi.ToString("dd/MM/yyyy");
            returnItem.NilaiOriginal = item.NilaiOriginal.ToString("N0");
            returnItem.COAPerawatan = item.COAPerawatan;

            return returnItem;
        }


        //========================================================= Currency ==================================================================
        private IList<SelectListItem> createCurrencySelect(string selected)
        {
            List<Currency> itemList = Lookup.Get<List<Currency>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _currRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Currency, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(Currency item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.CurrencyDescription;
            returnItem.Value = item.CurrencyId;
            return returnItem;
        }

        //================================================================end====================================================================
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckGetInvoiceList")]
        public ActionResult NoAuthCheckGetInvoiceList(PerawatanAktivaView perawatan)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            ViewData["ActionName"] = "Generate";
            InvoiceFilter filter = new InvoiceFilter();
            string VendorInvoiceNo = perawatan.VendorInvoiceNo;
            filter.InvoiceDateFrom = DateTime.ParseExact(perawatan.InvoiceDateFrom, "d/M/yyyy", culture);
            filter.InvoiceDateTo = DateTime.ParseExact(perawatan.InvoiceDateTo, "d/M/yyyy", culture);
            filter.VendorInvoiceNo = perawatan.VendorInvoiceNo;
            filter.VendorId = perawatan.VendorID;
            filter.AktivaID = perawatan.AktivaID;
            ViewData["CurrencyList"] = createCurrencySelect(perawatan.CurrencyID);

            IList<PerawatanAktivaDetailView> item = GetPerawatanDetail(filter);
            perawatan.PerawatanAktivaDetailList = item;
            return View("PerawatanList", perawatan);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckCalculateBiaya")]
        public ActionResult NoAuthCheckCalculateBiaya(PerawatanAktivaView perawatan)
        {
            ViewData["ActionName"] = "Generate";
            ViewData["CurrencyList"] = createCurrencySelect(perawatan.CurrencyID);

            try
            {
                foreach (var item in perawatan.PerawatanAktivaDetailList)
                {
                    if (item.Selected)
                        item.Biaya = (Convert.ToDouble(item.NilaiOriginal) * Convert.ToDouble(perawatan.Kurs)).ToString("N0");
                }
            }
            catch (Exception e)
            {
                ScreenMessages.Submit(ScreenMessage.Error(e.Message));
                CollectScreenMessages();
            }

            return View("PerawatanList", perawatan);
        }

        private IList<PerawatanAktivaDetailView> GetPerawatanDetail(InvoiceFilter filter)
        {
            IList<InvoiceH> data = _invoiceRepository.getInvoiceList(filter);
            List<PerawatanAktivaDetail> PerawatanList = new List<PerawatanAktivaDetail>();

            foreach (InvoiceH InvH in data)
            {
                IList<InvoiceService> InvS = _invoiceRepository.getInvoiceService(InvH.InvoiceNo);
                foreach (InvoiceService InvoiceService in InvS)
                {
                    PerawatanAktiva currdata = _perawatanAktivaRepository.getPerawatanAktiva(InvoiceService.AktivaId);

                    if (currdata.IsNull())
                    {
                        if (filter.AktivaID.IsNullOrEmpty())
                        {
                            PerawatanAktivaDetail newItem = new PerawatanAktivaDetail("");
                            FixedAssetMaster fam = _fixedAssetMasterRepo.getFixedAssetMaster(InvoiceService.AktivaId);
                            Vendor sup = _VendorRepository.getVendor(InvH.VendorID);
                            GroupAsset ga = _groupAssetRepository.getGroupAsset(fam.IsNull() ? "" : fam.SubGroupAssetID);

                            newItem.AktivaID = InvoiceService.AktivaId;
                            newItem.NamaAktiva = fam.IsNull() ? "" : fam.NamaAktiva;
                            newItem.TanggalPerawatan = InvH.InvoiceDate;
                            newItem.VendorID = InvH.VendorID;
                            newItem.VendorName = sup.IsNull() ? "" : sup.VendorName;
                            newItem.NoInvoice = InvH.VendorInvoiceNo;
                            newItem.COAPerawatan = ga.IsNull() ? "" : ga.CoaBeban;
                            newItem.NilaiOriginal = InvoiceService.TotalAmount;
                            newItem.Biaya = 1 * InvoiceService.TotalAmount;
                            newItem.MasaGaransi = InvoiceService.ServiceGuaranteeDate;
                            PerawatanList.Add(newItem);
                        }
                        else
                        {
                            if (InvoiceService.AktivaId == filter.AktivaID)
                            {
                                PerawatanAktivaDetail newItem = new PerawatanAktivaDetail("");
                                FixedAssetMaster fam = _fixedAssetMasterRepo.getFixedAssetMaster(InvoiceService.AktivaId);
                                Vendor sup = _VendorRepository.getVendor(InvH.VendorID);
                                GroupAsset ga = _groupAssetRepository.getGroupAsset(fam.GroupAssetID);

                                newItem.AktivaID = InvoiceService.AktivaId;
                                newItem.NamaAktiva = fam.IsNull() ? "" : fam.NamaAktiva;
                                newItem.TanggalPerawatan = InvH.InvoiceDate;
                                newItem.VendorID = InvH.VendorID;
                                newItem.VendorName = sup.IsNull() ? "" : sup.VendorName;
                                newItem.NoInvoice = InvH.VendorInvoiceNo;
                                newItem.COAPerawatan = ga.IsNull() ? "" : ga.CoaBeban;
                                newItem.NilaiOriginal = InvoiceService.TotalAmount;
                                newItem.Biaya = 1 * InvoiceService.TotalAmount;
                                newItem.MasaGaransi = InvoiceService.ServiceGuaranteeDate;
                                PerawatanList.Add(newItem);
                            }
                        }
                    }
                }
            }

            IList<PerawatanAktivaDetailView> items = PerawatanList.ToList().ConvertAll<PerawatanAktivaDetailView>(new Converter<PerawatanAktivaDetail, PerawatanAktivaDetailView>(ConvertFrom));
            return items;
        }

        public IList<PerawatanAktivaDetailView> GeneratePerawatanDetail(PerawatanAktivaView data)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            InvoiceFilter filter = new InvoiceFilter();
            string VendorInvoiceNo = data.VendorInvoiceNo;
            filter.InvoiceDateFrom = DateTime.ParseExact(data.InvoiceDateFrom, "d/M/yyyy", culture);
            filter.InvoiceDateTo = DateTime.ParseExact(data.InvoiceDateTo, "d/M/yyyy", culture);
            filter.VendorInvoiceNo = data.VendorInvoiceNo;
            filter.VendorId = data.VendorID;
            filter.AktivaID = data.AktivaID;

            IList<PerawatanAktivaDetailView> item = GetPerawatanDetail(filter);

            return item;
        }

        public ActionResult Generate()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            CollectScreenMessages();
            ViewData["ActionName"] = "Generate";
            PerawatanAktivaView data = new PerawatanAktivaView();
            data.InvoiceDateFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            data.InvoiceDateTo = DateTime.ParseExact(data.InvoiceDateFrom, "d/M/yyyy", culture).AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
            data.PerawatanAktivaDetailList = GeneratePerawatanDetail(data);
            ViewData["CurrencyList"] = createCurrencySelect(data.CurrencyID);

            return View("PerawatanList", data);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Generate")]
        public ActionResult Generate(PerawatanAktivaView data)
        {
            CollectScreenMessages();
            ViewData["ActionName"] = "Generate";
            User user = Lookup.Get<User>();
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    foreach (PerawatanAktivaDetailView dtl in data.PerawatanAktivaDetailList)
                    {
                        PerawatanAktiva newData = new PerawatanAktiva("");
                        if (dtl.Selected)
                        {
                            newData.AktivaID = dtl.AktivaID;
                            newData.NamaAktiva = dtl.NamaAktiva;
                            newData.NoReferensi = data.NoReferensi;
                            newData.Keterangan = data.Keterangan;
                            newData.TanggalPerawatan = DateTime.ParseExact(dtl.TanggalPerawatan, "d/M/yyyy", culture);
                            newData.CurrencyID = data.CurrencyID;
                            newData.NilaiOriginal = Convert.ToDouble(dtl.NilaiOriginal);
                            newData.COA = dtl.COAPerawatan;
                            newData.Kurs = Convert.ToInt32(data.Kurs);
                            newData.Biaya = Convert.ToDouble(dtl.Biaya);
                            newData.VendorID = dtl.VendorID;
                            newData.NoInvoice = dtl.NoInvoice;
                            newData.MasaGaransi = DateTime.ParseExact(dtl.MasaGaransi, "d/M/yyyy", culture);

                            newData.UpdateUser = user.Username;
                            newData.UpdateDate = DateTime.Now;
                            newData.CreateUser = user.Username;
                            newData.CreateDate = DateTime.Now;

                            _perawatanAktivaRepository.Add(newData);

                            _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Save_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            ViewData["CurrencyList"] = createCurrencySelect(data.CurrencyID);
            return View("PerawatanList", data);
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            PerawatanAktivaView data = new PerawatanAktivaView();
            return CreateView(data);
        }

        private ViewResult CreateView(PerawatanAktivaView data)
        {
            ViewData["CurrencyList"] = createCurrencySelect(data.CurrencyID);

            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(PerawatanAktivaView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    PerawatanAktiva newData = new PerawatanAktiva("");

                    newData.AktivaID = data.AktivaID;
                    newData.NamaAktiva = data.NamaAktiva;
                    newData.NoReferensi = data.NoReferensi;
                    newData.Keterangan = data.Keterangan;
                    newData.TanggalPerawatan = DateTime.ParseExact(data.TanggalPerawatan, "d/M/yyyy", culture);
                    newData.CurrencyID = data.CurrencyID;
                    newData.NilaiOriginal = Convert.ToDouble(data.NilaiOriginal);
                    newData.COA = data.COA;
                    newData.Kurs = Convert.ToInt32(data.Kurs);
                    newData.Biaya = Convert.ToDouble(data.Biaya);
                    newData.VendorID = data.VendorID;
                    newData.NoInvoice = data.NoInvoice;
                    newData.MasaGaransi = DateTime.ParseExact(data.MasaGaransi, "d/M/yyyy", culture);

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _perawatanAktivaRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(PerawatanAktivaView data, string mode)
        {
            if (mode == "Create")
            {
                if (data.NoReferensi.IsNullOrEmpty())
                    return PerawatanAktivaResources.Validation_NoReferensi;

                if (data.CurrencyID.IsNull() || data.CurrencyID == "")
                    return PerawatanAktivaResources.Validation_CurrencyID;

                if (data.Kurs.IsNull() || data.Kurs == "0")
                    return PerawatanAktivaResources.Validation_Kurs;

                var dtl = data.PerawatanAktivaDetailList.FindElement(d => d.Selected == true);
                if (dtl.IsNull())
                    return "Silahkan pilih salah satu aktiva!";

                if (_perawatanAktivaRepository.IsDuplicate(data.AktivaID))
                    return PerawatanAktivaResources.Validation_DuplicateData;
            }
            else if (mode == "Edit")
            {
                if (data.TanggalPerawatan.IsNull())
                    return PerawatanAktivaResources.Validation_TanggalPerawatan;

                if (data.NoReferensi.IsNullOrEmpty())
                    return PerawatanAktivaResources.Validation_NoReferensi;

                if (data.CurrencyID.IsNull() || data.CurrencyID == "")
                    return PerawatanAktivaResources.Validation_CurrencyID;

                if (data.Kurs.IsNull() || data.Kurs == "0")
                    return PerawatanAktivaResources.Validation_Kurs;

                if (data.VendorID.IsNullOrEmpty())
                    return PerawatanAktivaResources.Validation_Kurs;

                if (data.NoInvoice.IsNullOrEmpty())
                    return PerawatanAktivaResources.Validation_NoInvoice;

                if (data.MasaGaransi.IsNull())
                    return PerawatanAktivaResources.Validation_MasaGaransi;

            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            PerawatanAktiva jenisusaha = _perawatanAktivaRepository.getPerawatanAktiva(id);
            PerawatanAktivaView data = ConvertFrom(jenisusaha);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(PerawatanAktivaView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    PerawatanAktiva newData = _perawatanAktivaRepository.getPerawatanAktiva(data.AktivaID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.AktivaID = data.AktivaID;
                    newData.NamaAktiva = data.NamaAktiva;
                    newData.NoReferensi = data.NoReferensi;
                    newData.Keterangan = data.Keterangan;
                    newData.TanggalPerawatan = DateTime.ParseExact(data.TanggalPerawatan, "d/M/yyyy", culture);
                    newData.CurrencyID = data.CurrencyID;
                    newData.NilaiOriginal = Convert.ToDouble(data.NilaiOriginal);
                    newData.COA = data.COA;
                    newData.Kurs = Convert.ToInt32(data.Kurs);
                    newData.Biaya = Convert.ToDouble(data.Biaya);
                    newData.VendorID = data.VendorID;
                    newData.NoInvoice = data.NoInvoice;
                    newData.MasaGaransi = DateTime.ParseExact(data.MasaGaransi, "d/M/yyyy", culture);
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _perawatanAktivaRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                PerawatanAktiva newData = _perawatanAktivaRepository.getPerawatanAktiva(Id);
                _perawatanAktivaRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

    }
}
