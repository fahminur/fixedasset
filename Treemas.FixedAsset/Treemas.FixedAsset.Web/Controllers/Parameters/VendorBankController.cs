﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Procurement.Interface;
using Treemas.Procurement.Model;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Procurement.Repository;




namespace Treemas.FixedAsset.Controllers
{
    public class VendorBankController : PageController
    {
        private IVendorBankRepository _VendorBankRepository;
        private IVendorRepository _vendorRepository;
        private IRfqSendDocRepository _rfqSendDocRepository;
        private IBankMasterRepository _bankMasterRepository;

        private IAuditTrailLog _auditRepository;
        public IList<VendorBank> VendorBankList;
        int i = 0;

        public VendorBankController(ISessionAuthentication sessionAuthentication, IVendorRepository vendorRepository, IVendorBankRepository VendorBankRepository,
          IRfqSendDocRepository rfqSendDocRepository, IAuditTrailLog auditRepository, IBankMasterRepository bankMasterRepository)
            : base(sessionAuthentication)
        {
            this._VendorBankRepository = VendorBankRepository;
            this._vendorRepository = vendorRepository;
            this._rfqSendDocRepository = rfqSendDocRepository;
            this._bankMasterRepository = bankMasterRepository;

            this._auditRepository = auditRepository;
            Settings.Title = VendorBankResources.PageTitle;
            Settings.ModuleName = "Vendor";

        }
        protected override void Startup()
        {
            var returnpage = new { controller = "Vendor", action = "Index" };
            try
            {
                int vendorid = Convert.ToInt32(Request.RequestContext.RouteData.Values["Id"]);
                Vendor vendor = null;
                if (vendorid < 1)
                    Response.RedirectToRoute(returnpage);
                vendor = _vendorRepository.getVendor(vendorid);



                VendorView vendorView = ConvertFromViewData(vendor);
                Lookup.Remove<VendorView>();
                Lookup.Add(vendorView);
                ViewData["Vendor"] = vendorView;
            }
            catch
            {
                Response.RedirectToRoute(returnpage);
            }

        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                VendorView vendorView = Lookup.Get<VendorView>();
                if (vendorView.IsNull())
                {
                    if (Request.QueryString.GetValues("Id").IsNullOrEmpty())
                        return null;
                    long Id = long.Parse(Request.QueryString.GetValues("Id")[0]);
                    Vendor vendor = _vendorRepository.getVendor(Id);

                    vendorView = ConvertFromViewData(vendor);
                    Lookup.Remove<VendorView>();
                    Lookup.Add(vendorView);
                }

                //   ViewData["Vendor"] = vendorView;
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // VendorBankList = _VendorBankRepository.getVendorBank(vendorView.Id);

                // Loading.   
                PagedResult<VendorBank> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _VendorBankRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, vendorView.Id);
                }
                else
                {
                    data = _VendorBankRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, vendorView.Id);
                }

                var items = data.Items.ToList().ConvertAll<VendorBankView>(new Converter<VendorBank, VendorBankView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public ActionResult NoAuthCheckSearch()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                long vendorId = Convert.ToInt64(Request.QueryString.GetValues("vendorIdLookup").FirstOrDefault());
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<VendorBank> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _VendorBankRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, vendorId);
                }
                else
                {
                    data = _VendorBankRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, vendorId);
                }

                var items = data.Items.ToList().ConvertAll<VendorBankView>(new Converter<VendorBank, VendorBankView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public VendorBankView ConvertFrom(VendorBank item)
        {
            VendorBankView returnItem = new VendorBankView();
            VendorView vendorView = Lookup.Get<VendorView>();

            returnItem.Id = item.Id;
            returnItem.BankMasterId = item.BankMasterId;
            returnItem.BankAccountNo = item.BankAccountNo;
            returnItem.BankAccountName = item.BankAccountName;
            returnItem.BankBranch = item.BankBranch;
            returnItem.BankName = item.BankName;
            returnItem.IsDefault = item.IsDefault;
            returnItem.VendorId = item.VendorId;

            ViewData["Vendor"] = vendorView;
            return returnItem;
        }
        //=============================================================send doc-=================-=====================================


        private IList<SelectListItem> createBankMasterSelect(string selected)
        {
            List<BankMaster> itemList = Lookup.Get<List<BankMaster>>();

            if (itemList.IsNullOrEmpty())
            {
                itemList = _bankMasterRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }


            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<BankMaster, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });


            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;

        }

        public SelectListItem ConvertFrom(BankMaster item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BankName;
            returnItem.Value = item.BankMasterId;
            return returnItem;
        }

        //=================================================================batas======================================================
        public VendorView ConvertFromViewData(Vendor item)
        {
            VendorView returnItem = new VendorView();

            returnItem.Id = item.Id;
            returnItem.VendorID = item.Id;
            returnItem.VendorName = item.VendorName;


            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            VendorBankView data = new VendorBankView();
            return CreateView(data);
        }

        private ViewResult CreateView(VendorBankView data)
        {
            ViewData["BankMaster"] = createBankMasterSelect(data.BankMasterId);
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(VendorBankView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            VendorView vendor = Lookup.Get<VendorView>();

            var returnpage = new { controller = "VendorBank", action = "Index", id = vendor.Id };

            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    VendorBank newData = new VendorBank(0L);
                    newData.BankMasterId = data.BankMasterId;
                    newData.BankName = data.BankName;
                    newData.BankBranch = data.BankBranch;
                    newData.BankAccountNo = data.BankAccountNo;
                    newData.IsDefault = data.IsDefault;
                    newData.BankAccountName = data.BankAccountName;
                    newData.VendorId = vendor.VendorID;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _VendorBankRepository.Add(newData);
                    //_vendorRepository.UpdateApprove(data.VendorId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {

                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToRoute(returnpage);
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(VendorBankView data, string mode)
        {
            //if (data.VendorID == "")
            //    return VendorResources.Validation_Id;

            if (data.BankName == "")
                return VendorBankResources.Validation_Name;

            if (data.BankAccountNo == "")
                return VendorBankResources.Validation_Jabatan;

            if (data.BankBranch == "")
                return VendorBankResources.Validation_ContactType;
            if (mode == "Create")
            {
                if (_VendorBankRepository.IsDuplicate(data.BankMasterId, data.BankAccountNo))
                    return VendorBankResources.Validation_DuplicateData;
            }
            return "";
        }



        // GET: Role/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            VendorBank VendorBank = _VendorBankRepository.getVendorBank(id);
            VendorBankView data = ConvertFrom(VendorBank);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(VendorBankView data)
        {



            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            VendorView vendor = Lookup.Get<VendorView>();

            var returnpage = new { controller = "VendorBank", action = "Index", id = vendor.Id };


            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    VendorBank newData = _VendorBankRepository.getVendorBank(data.Id);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    // newData.VendorID = data.VendorID;
                    newData.BankName = data.BankName;
                    newData.BankBranch = data.BankBranch;
                    newData.BankAccountNo = data.BankAccountNo;
                    newData.BankAccountName = data.BankAccountName;
                    newData.IsDefault = data.IsDefault;
                    newData.VendorId = vendor.VendorID;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _VendorBankRepository.Save(newData);
                    ViewData["Vendor"] = vendor;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                //  return RedirectToRoute(returnpage);
                return RedirectToRoute(returnpage);
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            VendorView vendor = Lookup.Get<VendorView>();

            var returnpage = new { controller = "VendorBank", action = "Index", id = vendor.Id };

            string message = "";
            try
            {
                VendorBank newData = _VendorBankRepository.getVendorBank(Id);
                // VendorBank newData = new VendorBank(Id);  

                _VendorBankRepository.Remove(newData);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToRoute(returnpage);
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }


    }
}