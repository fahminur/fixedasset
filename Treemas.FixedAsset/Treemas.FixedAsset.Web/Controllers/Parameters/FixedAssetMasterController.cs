﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using System.Globalization;

namespace Treemas.FixedAsset.Controllers
{
    public class FixedAssetMasterController : PageController
    {
        private IFixedAssetMasterRepository _fixedAssetMasterRepository;
        private IGroupAssetRepository _groupAssetRepository;
        private ISubGroupAssetRepository _subGroupAssetRepository;
        private INamaHartaRepository _namaHartaRepository;
        private IAssetCoaRepository _assetCoaRepository;
        private IAssetBranchRepository _branchRepository;
        private IAssetLocationRepository _assetLocationRepository;
        private IAssetAreaRepository _assetAreaRepository;
        private IAssetDeptRepository _assetDeptRepository;
        private IAssetUserRepository _assetUserRepository;
        private IAssetConditionRepository _assetConditionRepository;
        private ICurrencyRepository _currencyRepository;
        private IGolonganPajakRepository _golonganPajakRepository;
        private IFixedAssetMasterService _fixedAssetMasterService;
        private IVendorRepository _VendorRepository;
        private IMetodeDepresiasiRepository _metodeDepresiasiRepository;
        private IInsuranceComHORepository _insuranceComHORepository;
        private IInsuranceComBranchRepository _insuranceComBranchRepository;
        private IAuditTrailLog _auditRepository;
        private IPenjualanAktivaRepository _penjualanAktivaRepository;
        private IPenghapusanAktivaRepository _penghapusanAktivaRepository;
        private IInventoryRepository _inventoryRepository;
        private IPenerimaanBarangRepository _penerimaanBarangRepository;
        private IInvoiceRepository _invoiceRepository;
        public FixedAssetMasterController(ISessionAuthentication sessionAuthentication, IFixedAssetMasterRepository fixedAssetMasterRepository, IGroupAssetRepository groupAssetRepository,
            ISubGroupAssetRepository subGroupAssetRepository, INamaHartaRepository namaHartaRepository, IAssetCoaRepository assetCoaRepository, IAssetBranchRepository branchRepository,
            IAssetLocationRepository assetLocationRepository, IAssetAreaRepository assetAreaRepository, IAssetDeptRepository assetDeptRepository, IAssetUserRepository assetUserRepository,
            IAssetConditionRepository assetConditionRepository, ICurrencyRepository currencyRepository, IFixedAssetMasterService fixedAssetMasterService, IVendorRepository VendorRepository,
            IGolonganPajakRepository golonganPajakRepository, IMetodeDepresiasiRepository metodeDepresiasiRepository, IAuditTrailLog auditRepository, IInsuranceComHORepository insuranceComHORepository,
            IInsuranceComBranchRepository insuranceComBranchRepository, IPenjualanAktivaRepository penjualanAktivaRepository, IPenghapusanAktivaRepository penghapusanAktivaRepository,
            IInventoryRepository inventoryRepository, IPenerimaanBarangRepository penerimaanBarangRepository, IInvoiceRepository invoiceRepository) : base(sessionAuthentication)
        {
            this._fixedAssetMasterRepository = fixedAssetMasterRepository;
            this._groupAssetRepository = groupAssetRepository;
            this._subGroupAssetRepository = subGroupAssetRepository;
            this._namaHartaRepository = namaHartaRepository;
            this._assetCoaRepository = assetCoaRepository;
            this._branchRepository = branchRepository;
            this._assetLocationRepository = assetLocationRepository;
            this._assetAreaRepository = assetAreaRepository;
            this._assetDeptRepository = assetDeptRepository;
            this._assetUserRepository = assetUserRepository;
            this._assetConditionRepository = assetConditionRepository;
            this._currencyRepository = currencyRepository;
            this._golonganPajakRepository = golonganPajakRepository;
            this._fixedAssetMasterService = fixedAssetMasterService;
            this._VendorRepository = VendorRepository;
            this._metodeDepresiasiRepository = metodeDepresiasiRepository;
            this._auditRepository = auditRepository;
            this._insuranceComHORepository = insuranceComHORepository;
            this._insuranceComBranchRepository = insuranceComBranchRepository;
            this._penghapusanAktivaRepository = penghapusanAktivaRepository;
            this._penjualanAktivaRepository = penjualanAktivaRepository;
            this._inventoryRepository = inventoryRepository;
            this._penerimaanBarangRepository = penerimaanBarangRepository;
            this._invoiceRepository = invoiceRepository;

            Settings.ModuleName = "Registrasi Aktiva";
            Settings.Title = FixedAssetMasterResources.PageTitle;
        }

        protected override void Startup()
        {
            ViewData["GroupAssetList"] = createGroupAssetSelect(" ");
            ViewData["SubGroupAssetList"] = createSubGroupAssetSelect(" ");
            ViewData["SiteList"] = createSitetSelect(" ");
            //ViewData["AssetLokasitList"] = createAssetLocationSelect("");
            //ViewData["AssetAreaList"] = createAssetAreaSelect(" ");
            ViewData["AssetDeptList"] = createAssetDeptSelect(" ");
            ViewData["AssetUsertList"] = createAssetUserSelect("");
            ViewData["AssetCondList"] = createAssetConditionSelect("");
            ViewData["GolonganPajakList"] = createGolonganPajakSelect("");
            ViewData["NamaHartaList"] = createNamaHartaSelect("");
            ViewData["AsuransiHOList"] = createInsuranceComHOSelect("");
            ViewData["AsuransiBranchList"] = createInsuranceComBranchSelect("");
            ViewData["AssetStatusList"] = createAssetStatusSelect("");
        }

        public ActionResult Search()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                User user = Lookup.Get<User>();
                FixedAssetFilter filter = new FixedAssetFilter();
                filter.AktivaID = Request.QueryString.GetValues("AktivaID")[0];
                filter.NamaAktiva = Request.QueryString.GetValues("NamaAktiva")[0];
                filter.GroupAssetID = Request.QueryString["GroupAssetID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("GroupAssetID")[0];
                filter.SubGroupAssetID = Request.QueryString["SubGroupAssetID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("SubGroupAssetID")[0];
                filter.SiteID = Request.QueryString["SiteID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("SiteID")[0];
                //filter.AssetAreaID = Request.QueryString["AssetAreaID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("AssetAreaID")[0];
                filter.AssetDepID = Request.QueryString["AssetDepID"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("AssetDepID")[0];
                filter.TanggalPerolehanStart = !Request.QueryString["TanggalPerolehanStart"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalPerolehanStart")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.TanggalPerolehanEnd = !Request.QueryString["TanggalPerolehanEnd"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("TanggalPerolehanEnd")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.Status = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];
                filter.IsOls = Request.QueryString["IsOls"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("IsOls")[0];
                filter.NoMesin = Request.QueryString["NoMesin"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("NoMesin")[0];
                filter.NoRangka = Request.QueryString["NoRangka"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("NoRangka")[0];

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<FixedAssetMaster> data;
                data = _fixedAssetMasterRepository.FindAllPaged(user, pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<FixedAssetMasterView>(new Converter<FixedAssetMaster, FixedAssetMasterView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }


        public ActionResult NoAuthCheckSearch()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                User user = Lookup.Get<User>();
                //var company = Lookup.Get<Company>();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;
                string SearchType = Request.QueryString.GetValues("SearchType")[0];

                //string siteId = user.Site.SiteId;
                //string companyId = company.CompanyId;

                // Loading.   
                PagedResult<FixedAssetMaster> data = new PagedResult<FixedAssetMaster>(pageNumber, pageSize);
                PagedResult<FixedAssetMaster> searchData;

                if (searchValue.IsNullOrEmpty())
                {
                    searchData = _fixedAssetMasterRepository.FindAllPagedLookup(user, pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    searchData = _fixedAssetMasterRepository.FindAllPaged(user, pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                switch (SearchType)
                {
                    case "Sales":
                    case "Delete":
                    case "InvoiceSvc":
                        List<FixedAssetMaster> activeData = searchData.Items.AsEnumerable().Where(f => f.Status.Trim() == "A")
                            .Skip((pageNumber) * pageSize)
                            .Take(pageSize).ToList();

                        data.Items = activeData;
                        data.PageNumber = activeData.Count;
                        data.TotalItems = searchData.Items.AsEnumerable().Where(f => f.Status.Trim() == "A").Count();
                        break;
                    case "Invoice":
                        List<FixedAssetMaster> rcvData = searchData.Items.AsEnumerable().Where(f => f.Status.Trim() == "R")
                            .Skip((pageNumber) * pageSize)
                            .Take(pageSize).ToList();

                        data.Items = rcvData;
                        data.PageNumber = rcvData.Count;
                        data.TotalItems= searchData.Items.AsEnumerable().Where(f => f.Status.Trim() == "R").Count(); 
                        break;
                    case "GR":
                        List<FixedAssetMaster> newData = searchData.Items.AsEnumerable().Where(f => f.IsOLS ? f.Status.Trim() == "A" : f.Status.Trim() == "N")
                            .Skip((pageNumber) * pageSize)
                            .Take(pageSize).ToList();

                        data.Items = newData;
                        data.PageNumber = newData.Count;
                        data.TotalItems = searchData.Items.AsEnumerable().Where(f => f.IsOLS ? f.Status.Trim() == "A" : f.Status.Trim() == "N").Count();
                        break;
                    default:
                        data = searchData;
                        break;
                }

                var items = data.Items.ToList().ConvertAll<FixedAssetMasterView>(new Converter<FixedAssetMaster, FixedAssetMasterView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckSubGroupAsset(string GroupAssetID)
        {
            List<SubGroupAsset> itemList = null;
            itemList = _subGroupAssetRepository.FindByGroupAsset(GroupAssetID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<SubGroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGolonganPajakDetail(string GolonganPajakID)
        {
            GolonganPajak GP = _golonganPajakRepository.getGolonganPajak(GolonganPajakID);

            return Json(GP, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckCOA(string GroupAssetID, string CoaType)
        {
            //List<AssetCoa> itemList = null;
            //string CoaId = null;
            //if (CoaType == "A")
            //    CoaId = _groupAssetRepository.getGroupAsset(GroupAssetID).CoaPerolehan;
            //else if (CoaType == "P")
            //    CoaId = _groupAssetRepository.getGroupAsset(GroupAssetID).CoaAkumulasi;
            //else if (CoaType == "B")
            //    CoaId = _groupAssetRepository.getGroupAsset(GroupAssetID).CoaBeban;
            //itemList = _assetCoaRepository.findByGroupAsset(CoaId).ToList();
            //IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetCoa, SelectListItem>(ConvertFrom));
            //dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            GroupAsset groupAsset = _groupAssetRepository.getGroupAsset(GroupAssetID);

            List<object> objectList = new List<object>();

            objectList.Add(new { COAAktiva = groupAsset.CoaPerolehan, COAAkumulasi = groupAsset.CoaAkumulasi, COAPenyusutan = groupAsset.CoaBeban });

            return Json(objectList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckInsuranceComBranch(string MaskAssID)
        {
            List<InsuranceComBranch> itemList = null;
            itemList = _insuranceComBranchRepository.FindByMaskAssID(MaskAssID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<InsuranceComBranch, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetUserByBranch(string BrachID)
        {
            List<AssetUser> itemList = null;
            itemList = _assetUserRepository.FindByBranchID(BrachID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetUser, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetUserByDept(string BrachID, string DeptID)
        {
            List<AssetUser> itemList = null;
            itemList = _assetUserRepository.FindByDeptID(BrachID, DeptID).ToList();
            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetUser, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public FixedAssetMasterView ConvertFrom(FixedAssetMaster item)
        {
            FixedAssetMasterView returnItem = new FixedAssetMasterView();
            GroupAsset groupasset = _groupAssetRepository.getGroupAsset(item.GroupAssetID);
            SubGroupAsset subgroupasset = _subGroupAssetRepository.getSubGroupAsset(item.SubGroupAssetID);
            NamaHarta namaharta = _namaHartaRepository.getNamaHarta(item.NamaHartaID);
            AssetCoa aktiva = _assetCoaRepository.getAssetCoa(item.COAAktivaID);
            AssetCoa akumulasi = _assetCoaRepository.getAssetCoa(item.COAAkumulasiID);
            AssetCoa penyusutan = _assetCoaRepository.getAssetCoa(item.COAPenyusutanID);
            AssetBranch site = _branchRepository.getAssetBranch(item.SiteID);
            AssetDept assetdept = _assetDeptRepository.getAssetDept(item.AssetDepID);
            AssetUser assetuser = _assetUserRepository.getAssetUser(item.AssetUserID);
            AssetCondition assetcondition = _assetConditionRepository.getAssetCondition(item.AssetConditionID);
            Vendor Vendor = _VendorRepository.getVendor(item.VendorID);
            Currency currency = _currencyRepository.getCurrency(item.CurrencyID);
            GolonganPajak golonganPajak = _golonganPajakRepository.getGolonganPajak(item.GolonganPajakID);
            Inventory inventory = _inventoryRepository.getInventory(item.AktivaID);
            PenerimaanBarangAktiva GRaktiva = _penerimaanBarangRepository.getSinglePenerimaanBarangAktiva(item.AktivaID);
            PenjualanAktivaDetail penjualanDtl = _penjualanAktivaRepository.getSinglePenjualanDetail(item.AktivaID);
            InvoiceAktiva invAktiva = _invoiceRepository.getSingleInvoiceAktiva(item.AktivaID);
            PenghapusanAktivaDetail HapusAktiva = _penghapusanAktivaRepository.getSinglePenghapusanDetail(item.AktivaID);

            InvoiceH invHdr = null;
            PenerimaanBarangHdr GRHdr = null;
            PenghapusanAktivaHeader HapusHdr = null;

            if (!invAktiva.IsNull())
            {
                invHdr = _invoiceRepository.getInvoice(invAktiva.InvoiceNo);
            }
            if (!GRaktiva.IsNull())
            {
                GRHdr = _penerimaanBarangRepository.getPenerimaanBarang(GRaktiva.GRNo);
            }
            if (!HapusAktiva.IsNull())
            {
                HapusHdr = _penghapusanAktivaRepository.getPenghapusanAktiva(HapusAktiva.DeleteNo);
            }

            //tab aktiva
            returnItem.AktivaID = item.AktivaID;
            returnItem.AssetCode = inventory == null ? "" : inventory.AssetCode;
            returnItem.NamaAktiva = item.NamaAktiva;
            returnItem.GroupAssetID = item.GroupAssetID.IsNull() ? "" : item.GroupAssetID.Trim();
            returnItem.GroupAssetString = groupasset.IsNull() ? "" : groupasset.GroupAssetDescription;
            returnItem.SubGroupAssetID = item.SubGroupAssetID.IsNull() ? "" : item.SubGroupAssetID.Trim();
            returnItem.SubGroupAssetString = subgroupasset.IsNull() ? "" : subgroupasset.SubGroupAssetDescription;
            returnItem.NamaHartaID = item.NamaHartaID.IsNull() ? "" : item.NamaHartaID.Trim();
            returnItem.NamaHartaString = namaharta.IsNull() ? "" : namaharta.NamaHartaDeskripsi;
            returnItem.COAAktivaID = item.COAAktivaID;
            returnItem.COAAkumulasiID = item.COAAkumulasiID;
            returnItem.COAPenyusutanID = item.COAPenyusutanID;
            returnItem.ParentAktivaID = item.ParentAktivaID.IsNull() ? "" : item.ParentAktivaID.Trim();
            returnItem.SiteID = item.SiteID.IsNull() ? "" : item.SiteID.Trim();
            returnItem.SiteString = site.IsNull() ? "" : site.AssetBranchDescription;
            returnItem.AssetLocationID = item.AssetLocationID.IsNull() ? "" : item.AssetLocationID.Trim();
            returnItem.Lokasi = item.Lokasi.IsNullOrEmpty() ? "" : item.Lokasi.Trim();
            //returnItem.AssetLocationString = assetlocation.IsNull() ? "" : assetlocation.AssetLocationDescription;
            returnItem.AssetAreaID = item.AssetAreaID.IsNull() ? "" : item.AssetAreaID.Trim();
            //returnItem.AssetAreaString = assetarea.IsNull() ? "" : assetarea.AssetAreaDescription;
            returnItem.AssetDepID = item.AssetDepID.IsNull() ? "" : item.AssetDepID.Trim();
            returnItem.AssetDepString = assetdept.IsNull() ? "" : assetdept.AssetDeptDescription;
            returnItem.AssetUserID = item.AssetUserID.IsNull() ? "" : item.AssetUserID.Trim();
            returnItem.AssetUserString = assetuser.IsNull() ? "" : assetuser.AssetUserName;
            returnItem.AssetConditionID = item.AssetConditionID.IsNull() ? "" : item.AssetConditionID.Trim();
            returnItem.Merek = item.Merek;
            returnItem.Model = item.Model;
            returnItem.SerialNo1 = item.SerialNo1;
            returnItem.SerialNo2 = item.SerialNo2;
            returnItem.MasaGaransi = item.MasaGaransi.IsNull() ? null : Convert.ToDateTime(item.MasaGaransi).ToString("dd/MM/yyyy");
            returnItem.KeteranganAktiva = item.KeteranganAktiva;
            returnItem.NoPol = item.NoPol;
            returnItem.STNK = item.STNK;
            returnItem.BPKB = item.BPKB;
            returnItem.NoRangka = item.NoRangka;
            returnItem.NoMesin = item.NoMesin;
            returnItem.FormANo = item.FormANo;
            returnItem.NomorFaktur = item.NomorFaktur;
            returnItem.NomorNIKVIN = item.NomorNIKVIN;
            returnItem.TahunPembuatan = item.TahunPembuatan.ToString();
            returnItem.NoPolis = item.NoPolis;
            returnItem.TglPolis = item.TglPolis.IsNull() ? null : Convert.ToDateTime(item.TglPolis).ToString("dd/MM/yyyy");
            returnItem.InsuranceComID = item.InsuranceComID.IsNull() ? "" : item.InsuranceComID.Trim();
            returnItem.InsuranceComBranchID = item.InsuranceComBranchID.IsNull() ? "" : item.InsuranceComBranchID.Trim();
            returnItem.TanggalTerimaAsset = item.TanggalTerimaAsset.IsNull() ? null : Convert.ToDateTime(item.TanggalTerimaAsset).ToString("dd/MM/yyyy");
            returnItem.IsOLS = item.IsOLS;
            returnItem.JenisAsset = item.IsOLS.IsNull() ? "Fixed Asset" : item.IsOLS ? "Operating Lease" : "Fixed Asset";

            //tab penyusutan
            returnItem.TanggalPerolehanKomersial = item.TanggalPerolehanKomersial.IsNull() ? item.TanggalPerolehan.IsNull() ? null : Convert.ToDateTime(item.TanggalPerolehan).ToString("dd/MM/yyyy") : Convert.ToDateTime(item.TanggalPerolehanKomersial).ToString("dd/MM/yyyy");
            returnItem.TanggalPerolehanFiskal = item.TanggalPerolehanFiskal.IsNull() ? item.TanggalPerolehan.IsNull() ? null : Convert.ToDateTime(item.TanggalPerolehan).ToString("dd/MM/yyyy") : Convert.ToDateTime(item.TanggalPerolehanFiskal).ToString("dd/MM/yyyy");
            returnItem.HargaPerolehanKomersial = item.HargaPerolehanKomersial.ToString("N0");
            returnItem.HargaPerolehanFiskal = item.HargaPerolehanFiskal.ToString("N0");
            returnItem.KalkulasiOtomatis = item.KalkulasiOtomatis;
            returnItem.KalkulasiOtomatisFiskal = item.KalkulasiOtomatisFiskal;
            returnItem.TanggalCatat = item.TanggalCatat.IsNull() ? null : Convert.ToDateTime(item.TanggalCatat).ToString("dd/MM/yyyy");
            returnItem.TanggalCatatFiskal = item.TanggalCatatFiskal.IsNull() ? null : Convert.ToDateTime(item.TanggalCatatFiskal).ToString("dd/MM/yyyy");
            returnItem.MetodeDepresiasiKomersial = item.MetodeDepresiasiKomersial.IsNull() ? "" : item.MetodeDepresiasiKomersial.Trim();
            returnItem.GolonganPajakID = item.GolonganPajakID.IsNull() ? "" : item.GolonganPajakID.Trim();
            returnItem.GolonganPajakString = golonganPajak.IsNull() ? "" : golonganPajak.GolonganPajakDescription;
            returnItem.MasaManfaatKomersial = item.MasaManfaatKomersial.ToString();
            returnItem.MetodeDepresiasiFiskal = item.MetodeDepresiasiFiskal.IsNull() ? "" : item.MetodeDepresiasiFiskal.Trim();
            returnItem.PeriodeAwal = item.PeriodeAwal;
            returnItem.MasaManfaatFiskal = item.MasaManfaatFiskal.ToString();
            returnItem.PeriodeAkhir = item.PeriodeAkhir;
            returnItem.PeriodeMulaiFiskal = item.PeriodeMulaiFiskal;
            returnItem.Tarif = item.Tarif.ToString("N0");
            returnItem.PeriodeAkhirFiskal = item.PeriodeAkhirFiskal;
            returnItem.ResiduKomersial = item.ResiduKomersial.ToString("N0");
            returnItem.TarifFiskal = item.TarifFiskal.ToString("N0");
            returnItem.Pembebanan = item.Pembebanan.ToString("N0");
            returnItem.ResiduFiskal = item.ResiduFiskal.ToString("N0");
            returnItem.TanggalPenghapusan = item.TanggalPenghapusan.IsNull() ? HapusHdr.IsNull() ? null : Convert.ToDateTime(HapusHdr.DeleteDate).ToString("dd/MM/yyyy") : Convert.ToDateTime(item.TanggalPenghapusan).ToString("dd/MM/yyyy");
            returnItem.TanggalPenghapusanFiskal = item.TanggalPenghapusanFiskal.IsNull() ? null : Convert.ToDateTime(item.TanggalPenghapusanFiskal).ToString("dd/MM/yyyy");
            returnItem.NomorReferensi = HapusHdr.IsNull() ? item.NomorReferensi: HapusHdr.InternalMemoNo;
            returnItem.NomorReferensiFiskal = item.NomorReferensiFiskal;
            returnItem.HargaJual = penjualanDtl.IsNull() ? item.HargaJual.ToString("N0") : penjualanDtl.SellingPrice.ToString("N0");
            returnItem.HargaJualFiskal = item.HargaJualFiskal.ToString("N0");
            returnItem.Penyusutandihitung = item.Penyusutandihitung;
            returnItem.PenyusutanFiskal = item.PenyusutanFiskal;
            returnItem.AlasanPengapusan = HapusAktiva.IsNull() ? item.AlasanPengapusan : HapusAktiva.DeleteReason ;
            returnItem.AlasanFiskal = item.AlasanFiskal;

            //tab perolehan
            returnItem.VendorID = item.VendorID.IsNull() ? "" : item.VendorID.Trim();
            returnItem.VendorName = Vendor.IsNull() ? "" : Vendor.VendorName.Trim();
            returnItem.NoPO = invHdr.IsNull() ? item.NoPO : invHdr.PONo;
            returnItem.NoDO = GRHdr.IsNull() ? item.NoDO : GRHdr.DeliveryOrderNo;
            returnItem.NoGSRN = GRHdr.IsNull() ? item.NoGSRN : GRHdr.GRNo;
            returnItem.NoInvoice = invHdr.IsNull() ? item.NoInvoice : invHdr.InvoiceNo;
            returnItem.NoPaymentVoucher = item.NoPaymentVoucher;
            returnItem.TanggalPerolehan = item.TanggalPerolehan.IsNull() ? null : Convert.ToDateTime(item.TanggalPerolehan).ToString("dd/MM/yyyy");
            returnItem.TanggalPerolehanString = item.TanggalPerolehan.IsNull() ? null : Convert.ToDateTime(item.TanggalPerolehan).ToString("dd/MM/yyyy");
            returnItem.CurrencyID = item.CurrencyID.IsNull() ? "" : item.CurrencyID.Trim();
            returnItem.NilaiOriginal = item.NilaiOriginal.ToString("N0");
            returnItem.Kurs = item.Kurs.IsNull() ? "1" : item.Kurs.ToString("N0");
            returnItem.HargaPerolehan = item.HargaPerolehan.ToString("N0");
            returnItem.KeteranganPerolehan = item.KeteranganPerolehan;
            returnItem.AkumulasiPenyusutanKomersial = item.AkumulasiPenyusutanKomersial.ToString("N0");
            returnItem.AkumulasiPenyusutanFiskal = item.AkumulasiPenyusutanFiskal.ToString("N0");
            returnItem.SisaNilaiBukuKomersial = item.SisaNilaiBukuKomersial.ToString("N0");
            returnItem.SisaNilaiBukuFiskal = item.SisaNilaiBukuFiskal.ToString("N0");
            returnItem.Status = item.Status.Trim();// ? "Aktif" : "Non-Aktif";
            returnItem.StatusString = item.FAStatusEnum.ToDescription();

            //PenjualanAktiva jual = _penjualanAktivaRepository.getPenjualanAktiva(item.AktivaID);
            //PenghapusanAktiva hapus = _penghapusanAktivaRepository.getPenghapusanAktiva(item.AktivaID);

            //returnItem.StatusString = item.Status ? "Aktif" : !jual.IsNull() ? "Terjual" : !hapus.IsNull() ? "Dihapus" : "Tidak Aktif";

            return returnItem;
        }
        //----------------------------------------------------- status ----------------------------------------------------------------
        private IList<SelectListItem> createAssetStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(AssetStatusEnum)).Cast<AssetStatusEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }

        //==================================================== Group Asset ==================================================================
        private IList<SelectListItem> createGroupAssetSelect(string selected)
        {
            List<GroupAsset> itemList = Lookup.Get<List<GroupAsset>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _groupAssetRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(GroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GroupAssetDescription;
            returnItem.Value = item.GroupAssetID.Trim();// + ',' + item.CoaPerolehan.Trim() + ',' + item.CoaAkumulasi.Trim() + ',' + item.CoaBeban.Trim();
            return returnItem;
        }

        //================================================================end====================================================================

        //==================================================== Sub Group Asset ==================================================================
        private IList<SelectListItem> createSubGroupAssetSelect(string selected)
        {
            List<SubGroupAsset> itemList = Lookup.Get<List<SubGroupAsset>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _subGroupAssetRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<SubGroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(SubGroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.SubGroupAssetDescription;
            returnItem.Value = item.SubGroupAssetID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================

        //==================================================== Nama Harta ==================================================================
        private IList<SelectListItem> createNamaHartaSelect(string selected)
        {
            List<NamaHarta> itemList = Lookup.Get<List<NamaHarta>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _namaHartaRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<NamaHarta, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(NamaHarta item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.NamaHartaDeskripsi;
            returnItem.Value = item.NamaHartaID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //==================================================== Asset COA ==================================================================
        //private IList<SelectListItem> createAssetCoaSelect(string selected)
        //{
        //    List<AssetCoa> itemList = Lookup.Get<List<AssetCoa>>(); ;
        //    if (itemList.IsNullOrEmpty())
        //    {
        //        itemList = _assetCoaRepository.FindAll().ToList();
        //        Lookup.Remove(itemList);
        //        Lookup.Add(itemList);
        //    }

        //    IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetCoa, SelectListItem>(ConvertFrom));
        //    dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

        //    if (!selected.IsNullOrEmpty())
        //    {
        //        dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
        //    }
        //    return dataList;
        //}

        //public SelectListItem ConvertFrom(AssetCoa item)
        //{
        //    SelectListItem returnItem = new SelectListItem();
        //    returnItem.Text = item.COADescription;
        //    returnItem.Value = item.COAID;
        //    return returnItem;
        //}

        //================================================================end====================================================================


        //==================================================== Site ==================================================================
        private IList<SelectListItem> createSitetSelect(string selected)
        {
            List<AssetBranch> itemList = null;// Lookup.Get<List<AssetBranch>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _branchRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetBranch, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetBranch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetBranchDescription;
            returnItem.Value = item.AssetBranchID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //====================================================  Asset  Location==================================================================
        private IList<SelectListItem> createAssetLocationSelect(string selected)
        {
            List<AssetLocation> itemList = null;// Lookup.Get<List<AssetLocation>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetLocationRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetLocation, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetLocation item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetLocationDescription;
            returnItem.Value = item.AssetLocationID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //====================================================  Asset Area ==================================================================
        private IList<SelectListItem> createAssetAreaSelect(string selected)
        {
            List<AssetArea> itemList = Lookup.Get<List<AssetArea>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetAreaRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetArea, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetArea item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetAreaDescription;
            returnItem.Value = item.AssetAreaID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //====================================================  Asset Dept ==================================================================
        private IList<SelectListItem> createAssetDeptSelect(string selected)
        {
            List<AssetDept> itemList = null;// Lookup.Get<List<AssetDept>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetDeptRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetDept, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetDept item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetDeptDescription;
            returnItem.Value = item.AssetDeptID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //==================================================== Asset User ==================================================================
        private IList<SelectListItem> createAssetUserSelect(string selected)
        {
            List<AssetUser> itemList = null;// Lookup.Get<List<AssetUser>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetUserRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetUser, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetUser item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetUserName;
            returnItem.Value = item.AssetUserID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //====================================================  Asset Condition ==================================================================
        private IList<SelectListItem> createAssetConditionSelect(string selected)
        {
            List<AssetCondition> itemList = null; //Lookup.Get<List<AssetCondition>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _assetConditionRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<AssetCondition, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(AssetCondition item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.AssetConditionDescription;
            returnItem.Value = item.AssetConditionID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================


        //==================================================== Vendor ==================================================================
        //private IList<SelectListItem> createVendorSelect(int selected)
        //{
        //    List<Vendor> itemList = Lookup.Get<List<Vendor>>(); ;
        //    if (itemList.IsNullOrEmpty())
        //    {
        //        itemList = _vendorRepository.FindAll().ToList();
        //        Lookup.Remove(itemList);
        //        Lookup.Add(itemList);
        //    }

        //    IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Vendor, SelectListItem>(ConvertFrom));
        //    dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

        //    if (selected > 0)
        //    {
        //        dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
        //    }
        //    return dataList;
        //}

        //public SelectListItem ConvertFrom(Vendor item)
        //{
        //    SelectListItem returnItem = new SelectListItem();
        //    returnItem.Text = item.VendorName;
        //    returnItem.Value = item.Id.ToString();
        //    return returnItem;
        //}

        //================================================================end====================================================================

        //==================================================== Currency ==================================================================
        private IList<SelectListItem> createCurrencySelect(string selected)
        {
            List<Currency> itemList = Lookup.Get<List<Currency>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _currencyRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Currency, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(Currency item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.CurrencyDescription;
            returnItem.Value = item.CurrencyId.Trim();
            return returnItem;
        }

        //================================================================end====================================================================
        //==================================================== Currency ==================================================================
        private IList<SelectListItem> createGolonganPajakSelect(string selected)
        {
            List<GolonganPajak> itemList = Lookup.Get<List<GolonganPajak>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _golonganPajakRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GolonganPajak, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(GolonganPajak item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GolonganPajakDescription;
            returnItem.Value = item.GolonganPajakID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================
        //==================================================== aktiva ID ==================================================================
        //private IList<SelectListItem> createAktivaIdSelect(string selected)
        //{
        //    List<FixedAssetMaster> itemList = Lookup.Get<List<FixedAssetMaster>>(); ;
        //    if (itemList.IsNullOrEmpty())
        //    {
        //        itemList = _fixedAssetMasterRepository.FindAll().ToList();
        //        Lookup.Remove(itemList);
        //        Lookup.Add(itemList);
        //    }

        //    IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<FixedAssetMaster, SelectListItem>(ConvertFromParent));
        //    dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

        //    if (!selected.IsNullOrEmpty())
        //    {
        //        dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
        //    }
        //    return dataList;
        //}

        public SelectListItem ConvertFromParent(FixedAssetMaster item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.NamaAktiva;
            returnItem.Value = item.AktivaID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================
        //==================================================== metode depresiasi ID ==================================================================
        private IList<SelectListItem> createMetodeDepresiasiSelect(string selected)
        {
            List<MetodeDepresiasi> itemList = Lookup.Get<List<MetodeDepresiasi>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _metodeDepresiasiRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<MetodeDepresiasi, SelectListItem>(ConvertFromParent));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFromParent(MetodeDepresiasi item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.MetodeDepresiasiName;
            returnItem.Value = item.MetodeDepresiasiID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================

        //==================================================== Insurance HO ==================================================================
        private IList<SelectListItem> createInsuranceComHOSelect(string selected)
        {
            List<InsuranceComHO> itemList = Lookup.Get<List<InsuranceComHO>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _insuranceComHORepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<InsuranceComHO, SelectListItem>(ConvertFromParent));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFromParent(InsuranceComHO item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description.Trim();
            returnItem.Value = item.MaskAssID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================

        //==================================================== Insurance HO ==================================================================
        private IList<SelectListItem> createInsuranceComBranchSelect(string selected)
        {
            List<InsuranceComBranch> itemList = Lookup.Get<List<InsuranceComBranch>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _insuranceComBranchRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<InsuranceComBranch, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(InsuranceComBranch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.MaskAssBranchName.Trim();
            returnItem.Value = item.MaskAssBranchID.Trim();
            return returnItem;
        }

        //================================================================end====================================================================
        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            FixedAssetMasterView data = new FixedAssetMasterView();
            return CreateView(data);
        }

        private ViewResult CreateView(FixedAssetMasterView data)
        {
            ViewData["GroupAssetList"] = createGroupAssetSelect(data.GroupAssetID);// + ',' + data.COAAktivaID.Trim() + ',' + data.COAAkumulasiID.Trim() + ',' + data.COAPenyusutanID.Trim());
            ViewData["SubGroupAssetList"] = createSubGroupAssetSelect(data.SubGroupAssetID);
            ViewData["NamaHartaList"] = createNamaHartaSelect(data.NamaHartaID);
            //ViewData["AssetCoaAktivaList"] = createAssetCoaSelect(data.COAAktivaID);
            //ViewData["AssetCoaAkumulasiList"] = createAssetCoaSelect(data.COAAkumulasiID);
            //ViewData["AssetCoaPenyusutanList"] = createAssetCoaSelect(data.COAPenyusutanID);
            ViewData["SiteList"] = createSitetSelect(data.SiteID);
            //ViewData["AssetLokasitList"] = createAssetLocationSelect(data.AssetLocationID);
            //ViewData["AssetAreaList"] = createAssetAreaSelect(data.AssetAreaID);
            ViewData["AssetDeptList"] = createAssetDeptSelect(data.AssetDepID);
            ViewData["AssetUsertList"] = createAssetUserSelect(data.AssetUserID);
            ViewData["AssetCondList"] = createAssetConditionSelect(data.AssetConditionID);
            ViewData["CurrencyList"] = createCurrencySelect(data.CurrencyID);
            ViewData["GolonganPajakList"] = createGolonganPajakSelect(data.GolonganPajakID);
            //ViewData["ParentAktivaList"] = createAktivaIdSelect(data.ParentAktivaID);
            ViewData["MetodeDepresiasiFiskalList"] = createMetodeDepresiasiSelect(data.MetodeDepresiasiFiskal);
            ViewData["MetodeDepresiasiKomersialList"] = createMetodeDepresiasiSelect(data.MetodeDepresiasiKomersial);
            ViewData["AsuransiHOList"] = createInsuranceComHOSelect(data.InsuranceComID);
            ViewData["AsuransiBranchList"] = createInsuranceComBranchSelect(data.InsuranceComBranchID);

            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]

        public ActionResult Create(FixedAssetMasterView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            string message = "";

            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    FixedAssetMaster newData = new FixedAssetMaster("");

                    //newData.AktivaID = data.AktivaID;
                    newData.AssetCode = data.AssetCode;
                    newData.NamaAktiva = data.NamaAktiva;
                    newData.GroupAssetID = data.GroupAssetID;
                    newData.SubGroupAssetID = data.SubGroupAssetID;
                    //newData.NamaHartaID = data.NamaHartaID;
                    newData.COAAktivaID = data.COAAktivaID;
                    newData.COAAkumulasiID = data.COAAkumulasiID;
                    newData.COAPenyusutanID = data.COAPenyusutanID;
                    //newData.ParentAktivaID = data.ParentAktivaID;
                    newData.SiteID = data.SiteID;
                    newData.Lokasi = data.Lokasi;
                    //newData.AssetLocationID = data.AssetLocationID;
                    //newData.AssetAreaID = data.AssetAreaID;
                    newData.AssetDepID = data.AssetDepID;
                    newData.AssetUserID = data.AssetUserID;
                    newData.AssetConditionID = data.AssetConditionID;
                    newData.Merek = data.Merek;
                    newData.Model = data.Model;
                    newData.SerialNo1 = data.SerialNo1;
                    newData.InsuranceComID = data.InsuranceComID;
                    newData.InsuranceComBranchID = data.InsuranceComBranchID;
                    newData.SerialNo2 = data.SerialNo2;
                    newData.MasaGaransi = !data.MasaGaransi.IsNullOrEmpty() ? DateTime.ParseExact(data.MasaGaransi, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.KeteranganAktiva = data.KeteranganAktiva;
                    newData.VendorID = data.VendorID;
                    newData.NoPO = data.NoPO;
                    newData.NoDO = data.NoDO;
                    newData.NoGSRN = data.NoGSRN;
                    newData.NoInvoice = data.NoInvoice;
                    newData.NoPaymentVoucher = data.NoPaymentVoucher;
                    newData.TanggalPerolehan = !data.TanggalPerolehan.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPerolehan, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.CurrencyID = data.CurrencyID;
                    newData.NilaiOriginal = Convert.ToDouble(data.NilaiOriginal);
                    newData.Kurs = Convert.ToDouble(data.Kurs);
                    newData.HargaPerolehan = Convert.ToDouble(data.HargaPerolehan);
                    newData.KeteranganPerolehan = data.KeteranganPerolehan;
                    newData.TanggalPerolehanKomersial = !data.TanggalPerolehanKomersial.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPerolehanKomersial, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.KalkulasiOtomatis = data.KalkulasiOtomatis;
                    newData.TanggalTerimaAsset = !data.TanggalTerimaAsset.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalTerimaAsset, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.IsOLS = data.IsOLS;
                    newData.Status = newData.IsOLS ? AssetStatusEnum.A.ToString() : AssetStatusEnum.N.ToString();

                    newData.TanggalCatat = !data.TanggalCatat.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalCatat, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.MetodeDepresiasiKomersial = data.MetodeDepresiasiKomersial;
                    newData.MasaManfaatKomersial = Convert.ToDouble(data.MasaManfaatKomersial);
                    newData.PeriodeAwal = data.PeriodeAwal;
                    newData.PeriodeAkhir = data.PeriodeAkhir;
                    newData.Tarif = Convert.ToDouble(data.Tarif);
                    newData.ResiduKomersial = Convert.ToDouble(data.ResiduKomersial);
                    newData.TanggalPenghapusan = !data.TanggalPenghapusan.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPenghapusan, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.NomorReferensi = data.NomorReferensi;
                    newData.HargaJual = Convert.ToDouble(data.HargaJual);
                    newData.Penyusutandihitung = data.Penyusutandihitung;
                    newData.AlasanPengapusan = data.AlasanPengapusan;
                    newData.TanggalPenghapusanFiskal = !data.TanggalPenghapusanFiskal.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPenghapusanFiskal, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.HargaPerolehanFiskal = Convert.ToDouble(data.HargaPerolehanFiskal);
                    newData.KalkulasiOtomatisFiskal = data.KalkulasiOtomatisFiskal;
                    newData.TanggalCatatFiskal = !data.TanggalCatatFiskal.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalCatatFiskal, "d/M/yyyy", culture) : (DateTime?)null;

                    newData.GolonganPajakID = data.GolonganPajakID;
                    newData.MetodeDepresiasiFiskal = data.MetodeDepresiasiFiskal;
                    newData.MasaManfaatFiskal = Convert.ToDouble(data.MasaManfaatFiskal);
                    newData.PeriodeMulaiFiskal = data.PeriodeMulaiFiskal;
                    newData.PeriodeAkhirFiskal = data.PeriodeAkhirFiskal;
                    newData.TarifFiskal = Convert.ToDouble(data.TarifFiskal);
                    newData.Pembebanan = Convert.ToDouble(data.Pembebanan);
                    newData.ResiduFiskal = Convert.ToDouble(data.ResiduFiskal);
                    newData.TanggalPerolehanFiskal = !data.TanggalPerolehanFiskal.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPerolehanFiskal, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.NomorReferensiFiskal = data.NomorReferensiFiskal;
                    newData.HargaJualFiskal = Convert.ToDouble(data.HargaJualFiskal);
                    newData.PenyusutanFiskal = data.PenyusutanFiskal;
                    newData.AlasanFiskal = data.AlasanFiskal;
                    //newData.Status = AssetStatusEnum.N.ToString();
                    newData.NoPol = data.NoPol;
                    newData.STNK = data.STNK;
                    newData.BPKB = data.BPKB;
                    newData.NoRangka = data.NoRangka;
                    newData.NoMesin = data.NoMesin;
                    newData.NomorFaktur = data.NomorFaktur;
                    newData.NomorNIKVIN = data.NomorNIKVIN;
                    newData.FormANo = data.FormANo;
                    newData.TahunPembuatan = data.TahunPembuatan.IsNullOrEmpty() ? 0 : Convert.ToInt32(data.TahunPembuatan);

                    newData.NoPolis = data.NoPolis;
                    newData.TglPolis = !data.TglPolis.IsNullOrEmpty() ? DateTime.ParseExact(data.TglPolis, "d/M/yyyy", culture) : (DateTime?)null;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;

                    message = _fixedAssetMasterService.SaveFixedAssetMaster(newData, user, company);
                    if (message == "")
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Save_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(FixedAssetMasterView data, string mode)
        {
            if (data.IsOLS)
            {
                if (data.AssetCode == "" || data.AssetCode.IsNullOrEmpty())
                {
                    return "Pilih nama asset dari data asset Operating Lease!";
                }
            }

            if (mode == "Create")
            {
                if (data.IsOLS.IsNull())
                    return "Jenis Asset harus di pilih!";

                if (data.NamaAktiva == "" || data.NamaAktiva.IsNull())
                    return FixedAssetMasterResources.Validation_Nama;

                if (data.GroupAssetID == "" || data.GroupAssetID.IsNull())
                    return FixedAssetMasterResources.Validation_GroupAssetID;

                if (data.SubGroupAssetID == "" || data.SubGroupAssetID.IsNull())
                    return FixedAssetMasterResources.Validation_subassetid;

                if (data.SiteID == "" || data.SiteID.IsNull())
                {
                    return FixedAssetMasterResources.Validation_site;
                }
                else if (data.SiteID == "099")
                {
                    if (data.AssetDepID == "" || data.AssetDepID.IsNull())
                        return FixedAssetMasterResources.Validation_dept;
                }

                if (data.Lokasi == "" || data.Lokasi.IsNull())
                    return FixedAssetMasterResources.Validation_lokasi;

            }

            else if (mode == "Edit")
            {
                if (data.NamaAktiva == "" || data.NamaAktiva.IsNull())
                    return FixedAssetMasterResources.Validation_Nama;

                if (data.GroupAssetID == "" || data.GroupAssetID.IsNull())
                    return FixedAssetMasterResources.Validation_GroupAssetID;

                if (data.SubGroupAssetID == "" || data.SubGroupAssetID.IsNull())
                    return FixedAssetMasterResources.Validation_subassetid;

                if (data.SiteID == "" || data.SiteID.IsNull())
                {
                    return FixedAssetMasterResources.Validation_site;
                }
                else if (data.SiteID == "099")
                {
                    if (data.AssetDepID == "" || data.AssetDepID.IsNull())
                        return FixedAssetMasterResources.Validation_dept;
                }

                if (data.IsOLS.IsNull())
                    return "Jenis Asset harus di pilih!";

                if (data.Lokasi == "" || data.Lokasi.IsNull())
                    return FixedAssetMasterResources.Validation_lokasi;
                            
                if (data.VendorID.IsNull() || data.VendorID == "")
                    return FixedAssetMasterResources.Validation_vendor;

                if (data.NoPO == "" || data.NoPO.IsNull())
                    return FixedAssetMasterResources.Validation_nopo;

                if (data.NoDO == "" || data.NoDO.IsNull())
                    return FixedAssetMasterResources.Validation_nodo;

                if (data.NoGSRN == "" || data.NoGSRN.IsNull())
                    return FixedAssetMasterResources.Validation_nogsrn;

                if (data.NoInvoice == "" || data.NoInvoice.IsNull())
                    return FixedAssetMasterResources.Validation_invoice;

                if (data.NoPaymentVoucher == "" || data.NoPaymentVoucher.IsNull())
                    return FixedAssetMasterResources.Validation_payment;

                if (data.TanggalPerolehan.IsNull())
                    return FixedAssetMasterResources.Validation_tglperolehan;

                if (data.CurrencyID == "" || data.CurrencyID.IsNull())
                    return FixedAssetMasterResources.Validation_currency;

                if (data.NilaiOriginal == "0" || data.NilaiOriginal.IsNull() || data.NilaiOriginal.ToString() == "")
                    return FixedAssetMasterResources.Validation_NilaiOriginal;

                if (data.Kurs == "0" || data.Kurs.IsNull() || data.Kurs.ToString() == "")
                    return FixedAssetMasterResources.Validation_Kurs;

                if (data.HargaPerolehan == "0" || data.HargaPerolehan.IsNull() || data.HargaPerolehan.ToString() == "")
                    return FixedAssetMasterResources.HargaPerolehan;

                if (data.MetodeDepresiasiKomersial == "" || data.MetodeDepresiasiKomersial.IsNull())
                    return FixedAssetMasterResources.Validation_MetodeDepriasi;

                if (data.MasaManfaatKomersial == "0" || data.MasaManfaatKomersial.IsNull() || data.MasaManfaatKomersial.ToString() == "")
                    return FixedAssetMasterResources.Validation_MasaManfaatKom;

                if (data.PeriodeAwal.IsNull() || data.PeriodeAwal == "")
                    return FixedAssetMasterResources.Validation_PeriodeAwal;

                if (data.PeriodeAkhir.IsNull() || data.PeriodeAkhir == "")
                    return FixedAssetMasterResources.Validation_PeriodeAkhir;

                if (data.Tarif == "0" || data.Tarif.IsNull() || data.Tarif.ToString() == "")
                    return FixedAssetMasterResources.Validation_Tarif;

                if (data.ResiduKomersial == "0" || data.ResiduKomersial.IsNull() || data.ResiduKomersial.ToString() == "")
                    return FixedAssetMasterResources.Validation_Residu;

                if (data.GolonganPajakID == "" || data.GolonganPajakID.IsNull())
                    return FixedAssetMasterResources.Validation_Golpajak;

                if (data.MetodeDepresiasiFiskal == "" || data.MetodeDepresiasiFiskal.IsNull())
                    return FixedAssetMasterResources.Validation_MetodeDepriasifis;

                if (data.MasaManfaatFiskal == "0" || data.MasaManfaatFiskal.IsNull() || data.MasaManfaatFiskal.ToString() == "")
                    return FixedAssetMasterResources.Validation_MasaManfaatfis;

                if (data.PeriodeMulaiFiskal.IsNull() || data.PeriodeMulaiFiskal == "")
                    return FixedAssetMasterResources.Validation_PeriodeAwalfis;

                if (data.PeriodeAkhirFiskal.IsNull() || data.PeriodeAkhirFiskal == "")
                    return FixedAssetMasterResources.Validation_PeriodeAkhirfis;

                if (data.TarifFiskal == "0" || data.TarifFiskal.IsNull() || data.TarifFiskal.ToString() == "")
                    return FixedAssetMasterResources.Validation_Tariffis;

                if (data.Pembebanan == "0" || data.Pembebanan.IsNull() || data.Pembebanan.ToString() == "")
                    return FixedAssetMasterResources.Validation_Pembebananfis;

                if (data.ResiduFiskal == "0" || data.ResiduFiskal.IsNull() || data.ResiduFiskal.ToString() == "")
                    return FixedAssetMasterResources.Validation_Residufis;


                //if (data.TanggalPenghapusan.IsNull() || data.TanggalPenghapusan.IsNull() || data.TanggalPenghapusan.ToString() == "")
                //    return FixedAssetMasterResources.Validation_TglPenghapusan;

                //if (data.NomorReferensi == "" || data.NomorReferensi.IsNull())
                //    return FixedAssetMasterResources.Validation_Noref;

                //if (data.HargaJual == "0" || data.HargaJual.IsNull() || data.HargaJual.ToString() == "")
                //    return FixedAssetMasterResources.Validation_Hargaju;

                if (data.Penyusutandihitung.IsNull())
                    return FixedAssetMasterResources.Validation_penyusutandihit;

                //if (data.AlasanPengapusan == "" || data.AlasanPengapusan.IsNull())
                //    return FixedAssetMasterResources.Validation_penghapusfis;

                //if (data.TanggalPenghapusanFiskal.IsNull())
                //    return FixedAssetMasterResources.Validation_TglPenghapusanfis;

                //if (data.NomorReferensiFiskal == "" || data.NomorReferensiFiskal.IsNull())
                //    return FixedAssetMasterResources.Validation_Noreffis;

                //if (data.HargaJualFiskal == "0" || data.HargaJualFiskal.IsNull() || data.HargaJualFiskal.ToString() == "")
                //    return FixedAssetMasterResources.Validation_Hargajufis;

                //if (data.AlasanFiskal.IsNull() || data.AlasanFiskal == "")
                //    return FixedAssetMasterResources.Validation_penghapusfis;
            }

            if (mode == "Create")
            {
                if (_fixedAssetMasterRepository.IsDuplicate(data.AktivaID))
                    return FixedAssetMasterResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            FixedAssetMaster fixedmasterid = _fixedAssetMasterRepository.getFixedAssetMaster(id);
            FixedAssetMasterView data = ConvertFrom(fixedmasterid);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Edit")]

        public ActionResult Edit(FixedAssetMasterView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            string message = "";

            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    FixedAssetMaster newData = _fixedAssetMasterRepository.getFixedAssetMaster(data.AktivaID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    //newData.AktivaID = data.AktivaID;
                    newData.AssetCode = data.AssetCode;
                    newData.NamaAktiva = data.NamaAktiva;
                    newData.GroupAssetID = data.GroupAssetID;
                    newData.SubGroupAssetID = data.SubGroupAssetID;
                    //newData.NamaHartaID = data.NamaHartaID;
                    newData.COAAktivaID = data.COAAktivaID;
                    newData.COAAkumulasiID = data.COAAkumulasiID;
                    newData.COAPenyusutanID = data.COAPenyusutanID;
                    //newData.ParentAktivaID = data.ParentAktivaID;
                    newData.SiteID = data.SiteID;
                    newData.Lokasi = data.Lokasi;
                    //newData.AssetLocationID = data.AssetLocationID;
                    //newData.AssetAreaID = data.AssetAreaID;
                    newData.AssetDepID = data.AssetDepID;
                    newData.AssetUserID = data.AssetUserID;
                    newData.AssetConditionID = data.AssetConditionID;
                    newData.Merek = data.Merek;
                    newData.Model = data.Model;
                    newData.SerialNo1 = data.SerialNo1;
                    newData.InsuranceComID = data.InsuranceComID;
                    newData.InsuranceComBranchID = data.InsuranceComBranchID;
                    newData.SerialNo2 = data.SerialNo2;
                    newData.MasaGaransi = !data.MasaGaransi.IsNullOrEmpty() ? DateTime.ParseExact(data.MasaGaransi, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.KeteranganAktiva = data.KeteranganAktiva;
                    newData.VendorID = data.VendorID;
                    newData.NoPO = data.NoPO;
                    newData.NoDO = data.NoDO;
                    newData.NoGSRN = data.NoGSRN;
                    newData.NoInvoice = data.NoInvoice;
                    newData.NoPaymentVoucher = data.NoPaymentVoucher;
                    newData.TanggalPerolehan = !data.TanggalPerolehan.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPerolehan, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.CurrencyID = data.CurrencyID;
                    newData.NilaiOriginal = Convert.ToDouble(data.NilaiOriginal);
                    newData.Kurs = Convert.ToDouble(data.Kurs);
                    newData.HargaPerolehan = Convert.ToDouble(data.HargaPerolehan);
                    newData.KeteranganPerolehan = data.KeteranganPerolehan;
                    newData.TanggalPerolehanKomersial = !data.TanggalPerolehanKomersial.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPerolehanKomersial, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.KalkulasiOtomatis = data.KalkulasiOtomatis;
                    newData.TanggalTerimaAsset = !data.TanggalTerimaAsset.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalTerimaAsset, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.IsOLS = data.IsOLS;
                    newData.Status = newData.IsOLS ? AssetStatusEnum.A.ToString() : AssetStatusEnum.N.ToString();

                    newData.TanggalCatat = !data.TanggalCatat.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalCatat, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.MetodeDepresiasiKomersial = data.MetodeDepresiasiKomersial;
                    newData.MasaManfaatKomersial = Convert.ToDouble(data.MasaManfaatKomersial);
                    newData.PeriodeAwal = data.PeriodeAwal;
                    newData.PeriodeAkhir = data.PeriodeAkhir;
                    newData.Tarif = Convert.ToDouble(data.Tarif);
                    newData.ResiduKomersial = Convert.ToDouble(data.ResiduKomersial);
                    newData.TanggalPenghapusan = !data.TanggalPenghapusan.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPenghapusan, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.NomorReferensi = data.NomorReferensi;
                    newData.HargaJual = Convert.ToDouble(data.HargaJual);
                    newData.Penyusutandihitung = data.Penyusutandihitung;
                    newData.AlasanPengapusan = data.AlasanPengapusan;
                    newData.TanggalPenghapusanFiskal = !data.TanggalPenghapusanFiskal.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPenghapusanFiskal, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.HargaPerolehanFiskal = Convert.ToDouble(data.HargaPerolehanFiskal);
                    newData.KalkulasiOtomatisFiskal = data.KalkulasiOtomatisFiskal;
                    newData.TanggalCatatFiskal = !data.TanggalCatatFiskal.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalCatatFiskal, "d/M/yyyy", culture) : (DateTime?)null;

                    newData.GolonganPajakID = data.GolonganPajakID;
                    newData.MetodeDepresiasiFiskal = data.MetodeDepresiasiFiskal;
                    newData.MasaManfaatFiskal = Convert.ToDouble(data.MasaManfaatFiskal);
                    newData.PeriodeMulaiFiskal = data.PeriodeMulaiFiskal;
                    newData.PeriodeAkhirFiskal = data.PeriodeAkhirFiskal;
                    newData.TarifFiskal = Convert.ToDouble(data.TarifFiskal);
                    newData.Pembebanan = Convert.ToDouble(data.Pembebanan);
                    newData.ResiduFiskal = Convert.ToDouble(data.ResiduFiskal);
                    newData.TanggalPerolehanFiskal = !data.TanggalPerolehanFiskal.IsNullOrEmpty() ? DateTime.ParseExact(data.TanggalPerolehanFiskal, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.NomorReferensiFiskal = data.NomorReferensiFiskal;
                    newData.HargaJualFiskal = Convert.ToDouble(data.HargaJualFiskal);
                    newData.PenyusutanFiskal = data.PenyusutanFiskal;
                    newData.AlasanFiskal = data.AlasanFiskal;
                    //newData.Status = AssetStatusEnum.A.ToString();
                    newData.NoPol = data.NoPol;
                    newData.STNK = data.STNK;
                    newData.BPKB = data.BPKB;
                    newData.NoRangka = data.NoRangka;
                    newData.NoMesin = data.NoMesin;
                    newData.NoPolis = data.NoPolis;
                    newData.NomorFaktur = data.NomorFaktur;
                    newData.NomorNIKVIN = data.NomorNIKVIN;
                    newData.FormANo = data.FormANo;
                    newData.TahunPembuatan = data.TahunPembuatan.IsNullOrEmpty() ? 0 : Convert.ToInt32(data.TahunPembuatan);

                    newData.TglPolis = !data.TglPolis.IsNullOrEmpty() ? DateTime.ParseExact(data.TglPolis, "d/M/yyyy", culture) : (DateTime?)null;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    Inventory invt = _inventoryRepository.getInventory(newData.AktivaID);
                    if (invt != null)
                    {
                        invt.AssetCode = data.AssetCode;
                        invt.GRNCode = "GRN" + DateTime.Now.Year.ToString();
                        invt.ManufacturingYear = data.TahunPembuatan.ToString() == null ? 0 : Convert.ToInt32(data.TahunPembuatan);
                        invt.GRNDate = (DateTime?)null;
                        invt.NoRangka = data.NoRangka;
                        invt.NoMesin = data.NoMesin;
                        invt.NoPolisi = data.NoPol;
                        invt.NewUsed = data.AssetConditionID == null ? "N" : data.AssetConditionID;
                        invt.TotalOTR = Convert.ToDouble(data.HargaPerolehan);
                        invt.SupplierID = data.VendorID;
                        invt.UserLastUpdate = user.Username;
                        invt.DateLastUpdate = DateTime.Now;

                        _inventoryRepository.Save(invt);
                    }
                    message = _fixedAssetMasterService.UpdateFixedAssetMaster(newData, user, company);
                    if (message == "")
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");


                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Update_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                FixedAssetMaster newData = _fixedAssetMasterRepository.getFixedAssetMaster(Id);
                _fixedAssetMasterRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}