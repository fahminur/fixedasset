﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Utilities;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Web.Platform;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Web.Controllers
{
    public class HomeController : PageController
    {
        private IUserRepository _userRepository;
        private IBusinessDateRepository _businessDateRepository;
        private ICompanyRepository _companyRepository;
        public HomeController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IBusinessDateRepository businessDateRepository, ICompanyRepository companyRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._businessDateRepository = businessDateRepository;
            this._companyRepository = companyRepository;
        }

        // GET: Home
        protected override void Startup()
        {
            Settings.Title = "Treemas ERP System - Fixed Asset Module";
            User user = Lookup.Get<User>();

            // Get Business Date;
            if (user.BusinessDate.IsNull())
            {
                user.BusinessDate = _businessDateRepository.getBusinessDate();
                Lookup.Remove<User>();
                Lookup.Add(user);
            }

            ViewData["User"] = user;

            Company company = _companyRepository.getCompany("001");
            Lookup.Add(company);
        }

        public User GetUserInfo(string username)
        {
            var userInfo = _userRepository.GetUser(username);
            return userInfo;
        }


    }
}