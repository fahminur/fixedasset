﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using Treemas.IFIN.Repository;
using System.Transactions;
using System.Globalization;

namespace Treemas.FixedAsset.Web.Controllers.Invoice
{
    public class ApprovalInvoiceController : PageController
    {
        private IInvoiceRepository _InvoiceRepository;
        private IGroupAssetRepository _groupAssetRepository;
        private ISubGroupAssetRepository _subGroupAssetRepository;
        private INamaHartaRepository _namaHartaRepository;
        private IAssetCoaRepository _assetCoaRepository;
        private IAssetBranchRepository _branchRepository;
        private IAssetLocationRepository _assetLocationRepository;
        private IAssetAreaRepository _assetAreaRepository;
        private IAssetDeptRepository _assetDeptRepository;
        private IAssetUserRepository _assetUserRepository;
        private IAssetConditionRepository _assetConditionRepository;
        private ICurrencyRepository _currencyRepository;
        private IGolonganPajakRepository _golonganPajakRepository;
        private IVendorAccountRepository _VendorAccountRepository;
        private IVendorRepository _VendorRepository;
        private ITaxRepository _TaxRepository;
        private IInvoiceService _InvoiceService;
        private IInvoiceTypeRepository _invoiceTypeRepository;
        private IGeneralSettingRepository _generalSettingRepository;
        private IGLPeriodRepository _glPeriodRepository;
        private IExpenceTypeRepository _expenceTypeRepository;
        private IFixedAssetMasterRepository _fixedAssetMasterRepository;
        private IFixedAssetMasterSequence _fixedAssetMasterSequence;
        private IAuditTrailLog _auditRepository;
        private IGLJournalRepository _glJournalRepository;
        private IGLJournalService _glJournalService;
        private IPaymentAllocationRepository _paymentAllocationRepository;
        private IMateraiRepository _materaiRepository;
        private INonAssetMasterRepository _nonAssetMasterRepository;
        private IInventoryRepository _inventoryRepository;
        private IIfinsGLJournalRepository _ifinsGLJournalRepository;

        private string _invoiceTypeID;
        private string _VendorAccID;
        private string _expenceTypeID;

        public ApprovalInvoiceController(ISessionAuthentication sessionAuthentication, IInvoiceRepository InvoiceRepository, IGroupAssetRepository groupAssetRepository,
            ISubGroupAssetRepository subGroupAssetRepository, INamaHartaRepository namaHartaRepository, IAssetCoaRepository assetCoaRepository, IAssetBranchRepository branchRepository,
            IAssetLocationRepository assetLocationRepository, IAssetAreaRepository assetAreaRepository, IAssetDeptRepository assetDeptRepository, IAssetUserRepository assetUserRepository,
            ITaxRepository TaxRepository, IVendorAccountRepository VendorAccountRepository, IAssetConditionRepository assetConditionRepository, ICurrencyRepository currencyRepository,
            IInvoiceService InvoiceService, IGolonganPajakRepository golonganPajakRepository, IInvoiceTypeRepository invoiceTypeRepository, IGeneralSettingRepository generalSettingRepository,
            IGLPeriodRepository glPeriodRepository, IVendorRepository VendorRepository, IExpenceTypeRepository expenceTypeRepository, IAuditTrailLog auditRepository, IMateraiRepository materaiRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepository, IFixedAssetMasterSequence fixedAssetMasterSequence, IGLJournalRepository glJournalRepository, IGLJournalService glJournalService,
            IPaymentAllocationRepository paymentAllocationRepository, INonAssetMasterRepository nonAssetMasterRepository, IInventoryRepository inventoryRepository,
            IIfinsGLJournalRepository ifinsGLJournalRepository) : base(sessionAuthentication)
        {
            this._InvoiceRepository = InvoiceRepository;
            this._groupAssetRepository = groupAssetRepository;
            this._subGroupAssetRepository = subGroupAssetRepository;
            this._namaHartaRepository = namaHartaRepository;
            this._assetCoaRepository = assetCoaRepository;
            this._branchRepository = branchRepository;
            this._assetLocationRepository = assetLocationRepository;
            this._assetAreaRepository = assetAreaRepository;
            this._assetDeptRepository = assetDeptRepository;
            this._assetUserRepository = assetUserRepository;
            this._assetConditionRepository = assetConditionRepository;
            this._currencyRepository = currencyRepository;
            this._golonganPajakRepository = golonganPajakRepository;
            this._VendorAccountRepository = VendorAccountRepository;
            this._TaxRepository = TaxRepository;
            this._auditRepository = auditRepository;
            this._InvoiceService = InvoiceService;
            this._invoiceTypeRepository = invoiceTypeRepository;
            this._generalSettingRepository = generalSettingRepository;
            this._glPeriodRepository = glPeriodRepository;
            this._VendorRepository = VendorRepository;
            this._expenceTypeRepository = expenceTypeRepository;
            this._fixedAssetMasterRepository = fixedAssetMasterRepository;
            this._fixedAssetMasterSequence = fixedAssetMasterSequence;
            this._glJournalRepository = glJournalRepository;
            this._glJournalService = glJournalService;
            this._paymentAllocationRepository = paymentAllocationRepository;
            this._materaiRepository = materaiRepository;
            this._nonAssetMasterRepository = nonAssetMasterRepository;
            this._inventoryRepository = inventoryRepository;
            this._ifinsGLJournalRepository = ifinsGLJournalRepository;

            Settings.ModuleName = "Approval Invoice";
            Settings.Title = "Approval Invoice";
        }

        protected override void Startup()
        {
            ViewData["InvStatus"] = createInvoiceStatusSelect("N");
        }

        public ActionResult Search()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            JsonResult result = new JsonResult();
            try
            {
                InvoiceFilter filter = new InvoiceFilter();
                string vendorid = Request.QueryString.GetValues("VendorID")[0];
                string VendorInvoiceNo = Request.QueryString.GetValues("VendorInvoiceNo")[0];
                string InvoiceStatus = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];
                filter.InvoiceDateFrom = !Request.QueryString["InvoiceDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("InvoiceDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.InvoiceDateTo = !Request.QueryString["InvoiceDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("InvoiceDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                filter.VendorId = vendorid;
                filter.VendorInvoiceNo = VendorInvoiceNo;
                filter.InvoiceStatus = InvoiceStatus;

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<InvoiceH> data;
                data = _InvoiceRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<InvoiceHView>(new Converter<InvoiceH, InvoiceHView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public InvoiceHView ConvertFrom(InvoiceH item)
        {
            InvoiceHView returnItem = new InvoiceHView();
            returnItem.InvoiceNo = item.InvoiceNo;
            returnItem.VendorInvoiceNo = item.VendorInvoiceNo;
            returnItem.VendorID = item.VendorID;
            returnItem.GRNo = item.GRNo;
            returnItem.DeliveryOrderNo = item.DeliveryOrderNo;
            returnItem.PONo = item.PONo;
            Vendor Vendor = _VendorRepository.getVendor(item.VendorID.Trim());
            returnItem.VendorName = Vendor == null ? "" : Vendor.VendorName.Trim();
            returnItem.VendorBankId = item.VendorBankId;
            returnItem.InvoiceDate = item.InvoiceDate.ToString("dd/MM/yyyy");
            returnItem.InvoiceDueDate = item.InvoiceDueDate.ToString("dd/MM/yyyy");
            returnItem.InvoiceOrderDate = item.InvoiceOrderDate.ToString("dd/MM/yyyy");
            //returnItem.InvoiceDateString = item.InvoiceDate.ToString("dd/MM/yyyy");
            //returnItem.InvoiceDueDateString = item.InvoiceDueDate.ToString("dd/MM/yyyy");
            //returnItem.InvoiceOrderDateString = item.InvoiceOrderDate.ToString("dd/MM/yyyy");
            returnItem.Status = item.Status;
            returnItem.StatusString = item.InvStatusEnum.ToDescription();
            returnItem.Notes = item.Notes;
            returnItem.InvoiceTypeID = item.InvoiceTypeID.Trim();
            returnItem.NoFakturPajak = item.NoFakturPajak;
            returnItem.TglFakturPajak = item.TglFakturPajak.ToString("dd/MM/yyyy");
            returnItem.InvoiceAmount = item.InvoiceAmount.ToString("N0");
            returnItem.AllowEdit = item.AllowEdit;
            returnItem.AllowApprove = item.AllowApprove;
            returnItem.AllowReject = item.AllowReject;
            return returnItem;
        }

        private IList<SelectListItem> createInvoiceStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(InvoiceStatusEnum)).Cast<InvoiceStatusEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }

        [HttpPost]
        public ActionResult Approve(InvoiceHView inhv)
        {
            ViewData["ActionName"] = "Approve";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            string message = "";
            bool isActiva = false;
            bool isNonActiva = false;
            bool isService = false;

            try
            {
                InvoiceH newData = _InvoiceRepository.getInvoice(inhv.InvoiceNo);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeApprove");

                newData.Status = InvoiceStatusEnum.A.ToString();
                newData.UpdateUser = user.Username;
                newData.UpdateDate = DateTime.Now;

                _InvoiceRepository.Save(newData);

                if (message == "")
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterApprove");

                //create GL Journal
                int year = int.Parse(DateTime.Now.ToString("yyyy"));
                int month = int.Parse(DateTime.Now.ToString("MM"));

                switch (newData.InvoiceTypeID.Trim())
                {
                    case "AC":
                        isActiva = true;
                        break;
                    case "ANA":
                        isNonActiva = true;
                        isActiva = true;
                        break;
                    case "APA":
                        isActiva = true;
                        isService = true;
                        break;
                    case "NA":
                        isNonActiva = true;
                        break;
                    case "NPA":
                        isNonActiva = true;
                        isService = true;
                        break;
                    case "PA":
                        isService = true;
                        break;
                    case "ST":
                        isActiva = true;
                        isNonActiva = true;
                        isService = true;
                        break;
                    default:
                        break;
                }

                PaymentAllocation payallocAP = _paymentAllocationRepository.getPaymentAllocation("APVE");
                PaymentAllocation payallocDlvryCost = _paymentAllocationRepository.getPaymentAllocation("E-POS");
                PaymentAllocation paPph = _paymentAllocationRepository.getPaymentAllocation("APPPH23");
                PaymentAllocation paOtherCost = _paymentAllocationRepository.getPaymentAllocation("OTHERPAY");
                PaymentAllocation paService = _paymentAllocationRepository.getPaymentAllocation("E-MNTEQP");

                IList<InvoiceAktiva> InvA = _InvoiceRepository.getInvoiceAktiva(newData.InvoiceNo);
                IList<InvoiceNonAktiva> InvNA = _InvoiceRepository.getInvoiceNonAktiva(newData.InvoiceNo);
                IList<InvoiceService> InvS = _InvoiceRepository.getInvoiceService(newData.InvoiceNo);

                //preparing gl 
                List<string> SiteIDs = new List<string>();
                IList<GLJournalH> jurnalHList = new List<GLJournalH>();
                IList<IfinsGLJournalH> ifinsjurnalHList = new List<IfinsGLJournalH>();

                foreach (var fa in InvA)
                {
                    FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(fa.StartingAktivaId);
                    if (!SiteIDs.Contains(famData.SiteID.Trim()))
                    {
                        SiteIDs.Add(famData.SiteID.Trim());
                    }
                }

                foreach (var na in InvNA)
                {
                    NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(na.NonAktivaId);
                    if (!SiteIDs.Contains(nam.SiteID.Trim()))
                    {
                        SiteIDs.Add(nam.SiteID.Trim());
                    }
                }

                foreach (var fa in InvS)
                {
                    FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(fa.AktivaId);
                    if (!SiteIDs.Contains(famData.SiteID.Trim()))
                    {
                        SiteIDs.Add(famData.SiteID.Trim());
                    }
                }

                foreach (string SiteID in SiteIDs)
                {
                    GLJournalH jurnalH = new GLJournalH("");
                    IfinsGLJournalH ifinsJurnalH = new IfinsGLJournalH("");

                    #region Jurnal Hdr
                    jurnalH.CompanyID = "001";
                    jurnalH.BranchID = SiteID.Trim();
                    jurnalH.PeriodYear = year.ToString();
                    jurnalH.PeriodMonth = month.ToString();
                    jurnalH.TransactionID = "RIV";
                    jurnalH.Reff_No = newData.InvoiceNo.Trim();
                    jurnalH.Reff_Date = DateTime.Now;
                    jurnalH.Tr_Date = DateTime.Now;
                    jurnalH.tr_Date1 = (DateTime?)null;
                    jurnalH.PrintDate = (DateTime?)null;
                    jurnalH.LastRevisiPosting = (DateTime?)null;
                    jurnalH.Tr_Desc = "Terima Invoice Vendor";
                    jurnalH.JournalAmount = 0;// newData.InvoiceAmount;
                    jurnalH.BatchID = "RIV";
                    jurnalH.SubSystem = "FA";
                    jurnalH.Status_Tr = "OP";
                    jurnalH.IsActive = true;
                    jurnalH.IsValid = true;
                    jurnalH.Flag = "R";
                    jurnalH.DtmUpd = DateTime.Now;
                    jurnalH.UsrUpd = user.Username;

                    ifinsJurnalH.CompanyID = "001";
                    ifinsJurnalH.BranchID = SiteID.Trim();
                    ifinsJurnalH.PeriodYear = year.ToString();
                    ifinsJurnalH.PeriodMonth = month.ToString();
                    ifinsJurnalH.TransactionID = "RIV";
                    ifinsJurnalH.Reff_No = newData.InvoiceNo.Trim();
                    ifinsJurnalH.Reff_Date = DateTime.Now;
                    ifinsJurnalH.Tr_Date = DateTime.Now;
                    ifinsJurnalH.tr_Date1 = (DateTime?)null;
                    ifinsJurnalH.PrintDate = (DateTime?)null;
                    ifinsJurnalH.LastRevisiPosting = (DateTime?)null;
                    ifinsJurnalH.Tr_Desc = "Terima Invoice Vendor";
                    ifinsJurnalH.JournalAmount = 0;// newData.InvoiceAmount;
                    ifinsJurnalH.BatchID = "RIV";
                    ifinsJurnalH.SubSystem = "FA";
                    ifinsJurnalH.Status_Tr = "OP";
                    ifinsJurnalH.IsActive = true;
                    ifinsJurnalH.IsValid = true;
                    ifinsJurnalH.Flag = "R";
                    ifinsJurnalH.DtmUpd = DateTime.Now;
                    ifinsJurnalH.UsrUpd = user.Username;
                    #endregion

                    int i = 0;
                    jurnalH.GLJournalDtlList = new List<GLJournalD>();
                    ifinsJurnalH.GLJournalDtlList = new List<IfinsGLJournalD>();

                    i = 0;
                    double totalPrice = 0;

                    #region Jurnal Dtl AKTIVA
                    if (isActiva)
                    {
                        foreach (InvoiceAktiva item in InvA)
                        {
                            //DEBT
                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.StartingAktivaId);
                            if (famData.SiteID.Trim() != SiteID.Trim()) { continue; }

                            GLJournalD jurnalD = new GLJournalD(i);
                            GroupAsset GA = _groupAssetRepository.getGroupAsset(famData.GroupAssetID);

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = famData.SiteID.Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = famData.SiteID.Trim();
                            jurnalD.CoaId = GA.IsNull() ? "" : GA.CoaPerolehan.Trim() + "-" + jurnalH.BranchID;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Aktiva - " + famData.NamaAktiva.Trim();
                            jurnalD.Post = "D";
                            jurnalD.Amount = item.Price + item.PPnAmount;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalPrice += jurnalD.Amount;

                            i++;

                            if (famData.IsOLS)
                            {
                                Inventory invt = _inventoryRepository.getInventory(item.StartingAktivaId);
                                if (invt != null)
                                {
                                    invt.GRNDate = invt.GRNDate.IsNull() ? (DateTime?)null : invt.GRNDate;
                                    invt.TotalOTR = Convert.ToDouble(item.TotalAmount);
                                    invt.SupplierID = newData.VendorID;
                                    invt.UserLastUpdate = user.Username;
                                    invt.DateLastUpdate = DateTime.Now;

                                    _inventoryRepository.Save(invt);
                                }
                            }
                        }

                        //CREDIT
                        if (totalPrice > 0)
                        {
                            GLJournalD jurnalC = new GLJournalD(i);

                            jurnalC.CompanyID = "001";
                            jurnalC.BranchID = jurnalH.BranchID;
                            jurnalC.SequenceNo = i + 1;
                            jurnalC.CoaCo = "001";
                            jurnalC.CoaBranch = jurnalH.BranchID;
                            jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalC.TransactionID = "RIV";
                            jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalC.Post = "C";
                            jurnalC.Amount = totalPrice;
                            jurnalC.CoaId_X = "-";
                            jurnalC.PaymentAllocationId = "AP";
                            jurnalC.ProductId = "-";
                            jurnalC.DtmUpd = jurnalH.DtmUpd;
                            jurnalC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalC);

                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));

                            i++;
                        }
                    }
                    #endregion

                    #region Jurnal Dtl NON AKTIVA
                    if (isNonActiva)
                    {
                        totalPrice = 0;
                        foreach (var item in InvNA)
                        {
                            //DEBT
                            NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);
                            if (nam.SiteID.Trim() != SiteID.Trim()) { continue; }

                            GLJournalD jurnalD = new GLJournalD(i);
                            GroupAsset GA = _groupAssetRepository.getGroupAsset(item.GroupAssetID);
                            PaymentAllocation payallocPrepaid = _paymentAllocationRepository.getPaymentAllocation(item.PrepaidPA);
                            PaymentAllocation payallocExpence = _paymentAllocationRepository.getPaymentAllocation(item.ExpensePA);

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = nam.SiteID.Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = nam.SiteID.Trim();
                            jurnalD.CoaId = item.ExpenseType == "L" ? (payallocExpence.IsNull() ? "" : payallocExpence.COA.Trim() + "-" + jurnalD.CoaBranch) : payallocPrepaid.IsNull() ? "" : payallocPrepaid.COA.Trim() + "-" + jurnalD.CoaBranch;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Non Aktiva " + (nam.IsNull() ? "-" : nam.NamaNonAktiva.Trim());
                            jurnalD.Post = "D";
                            jurnalD.Amount = item.Price + item.PPnAmount;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalPrice += jurnalD.Amount;
                            i++;
                        }

                        //CREDIT
                        if (totalPrice > 0)
                        {
                            GLJournalD jurnalC = new GLJournalD(i);

                            jurnalC.CompanyID = "001";
                            jurnalC.BranchID = jurnalH.BranchID.Trim();
                            jurnalC.SequenceNo = i + 1;
                            jurnalC.CoaCo = "001";
                            jurnalC.CoaBranch = jurnalH.BranchID.Trim();
                            jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID.Trim();
                            jurnalC.TransactionID = "RIV";
                            jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalC.Post = "C";
                            jurnalC.Amount = totalPrice;
                            jurnalC.CoaId_X = "-";
                            jurnalC.PaymentAllocationId = "AP";
                            jurnalC.ProductId = "-";
                            jurnalC.DtmUpd = jurnalH.DtmUpd;
                            jurnalC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalC);

                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));

                            i++;
                        }

                    }
                    #endregion

                    #region Jurnal Dtl SERVICE
                    if (isService)
                    {
                        totalPrice = 0;
                        foreach (var item in InvS)
                        {
                            //DEBT
                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);
                            if (famData.SiteID.Trim() != SiteID.Trim()) { continue; }

                            GLJournalD jurnalD = new GLJournalD(i);

                            GroupAsset GA = _groupAssetRepository.getGroupAsset(famData.IsNull() ? "" : famData.GroupAssetID);
                            PaymentAllocation paExpense = _paymentAllocationRepository.getPaymentAllocation(item.ExpensePA.Trim());

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = famData.SiteID.Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = famData.SiteID.Trim();
                            jurnalD.CoaId = item.ExpensePA.IsNull() ? paService.COA.Trim() + "-" + jurnalD.CoaBranch : paExpense.COA.Trim() + "-" + jurnalD.CoaBranch;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Biaya Perawatan Aktiva";
                            jurnalD.Post = "D";
                            jurnalD.Amount = item.ServiceAmount + item.PPnAmount + item.ItemAmount;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalPrice += jurnalD.Amount;
                            i++;
                        }

                        //CREDIT
                        if (totalPrice > 0)
                        {
                            GLJournalD jurnalC = new GLJournalD(i);

                            jurnalC.CompanyID = "001";
                            jurnalC.BranchID = jurnalH.BranchID.Trim();
                            jurnalC.SequenceNo = i + 1;
                            jurnalC.CoaCo = "001";
                            jurnalC.CoaBranch = jurnalH.BranchID.Trim();
                            jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID.Trim();
                            jurnalC.TransactionID = "RIV";
                            jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalC.Post = "C";
                            jurnalC.Amount = totalPrice;
                            jurnalC.CoaId_X = "-";
                            jurnalC.PaymentAllocationId = "AP";
                            jurnalC.ProductId = "-";
                            jurnalC.DtmUpd = jurnalH.DtmUpd;
                            jurnalC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalC);

                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));

                            i++;
                        }
                    }
                    #endregion

                    #region Jurnal Dtl PPh

                    #region AKTIVA
                    if (isActiva)
                    {
                        double totalPph = 0;
                        foreach (var item in InvA)
                        {
                            //DEBT
                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.StartingAktivaId);
                            if (famData.SiteID.Trim() != SiteID.Trim()) { continue; }

                            GLJournalD jurnalPph = new GLJournalD(i);

                            if (item.PPhAmount > 0)
                            {
                                jurnalPph.CompanyID = "001";
                                jurnalPph.BranchID = famData.SiteID.Trim();
                                jurnalPph.SequenceNo = i + 1;
                                jurnalPph.CoaCo = "001";
                                jurnalPph.CoaBranch = famData.SiteID.Trim();
                                jurnalPph.CoaId = paPph.IsNull() ? "" : paPph.COA.Trim() + "-" + jurnalH.BranchID;
                                jurnalPph.TransactionID = "RIV";
                                jurnalPph.Tr_Desc = "Hutang PPH - " + famData.NamaAktiva.Trim();
                                jurnalPph.Post = "D";
                                jurnalPph.Amount = item.PPhAmount;
                                jurnalPph.CoaId_X = "-";
                                jurnalPph.PaymentAllocationId = "FA";
                                jurnalPph.ProductId = "-";
                                jurnalPph.DtmUpd = jurnalH.DtmUpd;
                                jurnalPph.UsrUpd = jurnalH.UsrUpd;

                                jurnalH.JournalAmount += jurnalPph.Amount;
                                jurnalH.GLJournalDtlList.Add(jurnalPph);

                                ifinsJurnalH.JournalAmount += jurnalPph.Amount;
                                ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalPph));

                                totalPph += jurnalPph.Amount;
                                i++;
                            }
                        }

                        //CREDIT
                        if (totalPph > 0)
                        {
                            GLJournalD jurnalPphC = new GLJournalD(i);
                            jurnalPphC.CompanyID = "001";
                            jurnalPphC.BranchID = jurnalH.BranchID.Trim();
                            jurnalPphC.SequenceNo = i + 1;
                            jurnalPphC.CoaCo = "001";
                            jurnalPphC.CoaBranch = jurnalH.BranchID.Trim();
                            jurnalPphC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalPphC.TransactionID = "RIV";
                            jurnalPphC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalPphC.Post = "C";
                            jurnalPphC.Amount = totalPph;
                            jurnalPphC.CoaId_X = "-";
                            jurnalPphC.PaymentAllocationId = "AP";
                            jurnalPphC.ProductId = "-";
                            jurnalPphC.DtmUpd = jurnalH.DtmUpd;
                            jurnalPphC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalPphC);
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalPphC));
                            i++;
                        }
                    }
                    #endregion

                    #region NON AKTIVA
                    if (isNonActiva)
                    {
                        double totalPph = 0;
                        foreach (var item in InvNA)
                        {
                            //DEBT
                            NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);
                            if (nam.SiteID.Trim() != SiteID.Trim()) { continue; }

                            GLJournalD jurnalD = new GLJournalD(i);
                            GroupAsset GA = _groupAssetRepository.getGroupAsset(item.GroupAssetID);

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = nam.SiteID.Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = nam.SiteID.Trim();
                            jurnalD.CoaId = paPph.IsNull() ? "" : paPph.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Hutang PPH - " + (nam.IsNull() ? "-" : nam.NamaNonAktiva.Trim());
                            jurnalD.Post = "D";
                            jurnalD.Amount = item.PPhAmount;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalPph += jurnalD.Amount;
                            i++;
                        }

                        //CREDIT
                        if (totalPph > 0)
                        {
                            GLJournalD jurnalC = new GLJournalD(i);
                            jurnalC.CompanyID = "001";
                            jurnalC.BranchID = jurnalH.BranchID.Trim();
                            jurnalC.SequenceNo = i + 1;
                            jurnalC.CoaCo = "001";
                            jurnalC.CoaBranch = jurnalH.BranchID.Trim();
                            jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalC.TransactionID = "RIV";
                            jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalC.Post = "C";
                            jurnalC.Amount = totalPph;
                            jurnalC.CoaId_X = "-";
                            jurnalC.PaymentAllocationId = "AP";
                            jurnalC.ProductId = "-";
                            jurnalC.DtmUpd = jurnalH.DtmUpd;
                            jurnalC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalC);
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));
                            i++;
                        }
                    }
                    #endregion

                    #region SERVICE
                    if (isService)
                    {
                        double totalPph = 0;
                        foreach (var item in InvS)
                        {
                            //DEBT
                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);
                            if (famData.SiteID.Trim() != SiteID.Trim()) { continue; }

                            GLJournalD jurnalD = new GLJournalD(i);

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = famData.SiteID.Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = famData.SiteID.Trim();
                            jurnalD.CoaId = paPph.IsNull() ? "" : paPph.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Hutang PPH - Biaya Perawatan Aktiva";
                            jurnalD.Post = "D";
                            jurnalD.Amount = item.PPhAmount;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalPph += jurnalD.Amount;
                            i++;
                        }

                        //CREDIT
                        if (totalPph > 0)
                        {
                            GLJournalD jurnalC = new GLJournalD(i);
                            jurnalC.CompanyID = "001";
                            jurnalC.BranchID = jurnalH.BranchID.Trim();
                            jurnalC.SequenceNo = i + 1;
                            jurnalC.CoaCo = "001";
                            jurnalC.CoaBranch = jurnalH.BranchID.Trim();
                            jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalC.TransactionID = "RIV";
                            jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalC.Post = "C";
                            jurnalC.Amount = totalPph;
                            jurnalC.CoaId_X = "-";
                            jurnalC.PaymentAllocationId = "AP";
                            jurnalC.ProductId = "-";
                            jurnalC.DtmUpd = jurnalH.DtmUpd;
                            jurnalC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalC);

                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));
                            i++;
                        }
                    }
                    #endregion

                    #endregion

                    #region Jurnal Dtl DeliveryCost
                    #region AKTIVA
                    if (isActiva)
                    {
                        double totalDeliveryCost = 0;
                        foreach (var item in InvA)
                        {
                            //DEBT
                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.StartingAktivaId);
                            if (famData.SiteID.Trim() != SiteID.Trim()) { continue; }

                            GLJournalD jurnalDlvry = new GLJournalD(i);

                            if (item.PPhAmount > 0)
                            {
                                jurnalDlvry.CompanyID = "001";
                                jurnalDlvry.BranchID = famData.SiteID.ToString().Trim();
                                jurnalDlvry.SequenceNo = i + 1;
                                jurnalDlvry.CoaCo = "001";
                                jurnalDlvry.CoaBranch = famData.SiteID.ToString().Trim();
                                jurnalDlvry.CoaId = paPph.IsNull() ? "" : paPph.COA.Trim() + "-" + jurnalH.BranchID;
                                jurnalDlvry.TransactionID = "RIV";
                                jurnalDlvry.Tr_Desc = "Biaya Kirim - " + famData.NamaAktiva.Trim();
                                jurnalDlvry.Post = "D";
                                jurnalDlvry.Amount = item.ExpedCost;
                                jurnalDlvry.CoaId_X = "-";
                                jurnalDlvry.PaymentAllocationId = "FA";
                                jurnalDlvry.ProductId = "-";
                                jurnalDlvry.DtmUpd = jurnalH.DtmUpd;
                                jurnalDlvry.UsrUpd = jurnalH.UsrUpd;

                                jurnalH.JournalAmount += jurnalDlvry.Amount;
                                jurnalH.GLJournalDtlList.Add(jurnalDlvry);

                                ifinsJurnalH.JournalAmount += jurnalDlvry.Amount;
                                ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalDlvry));

                                totalDeliveryCost += jurnalDlvry.Amount;
                                i++;
                            }
                        }

                        //CREDIT
                        if (totalDeliveryCost > 0)
                        {
                            GLJournalD jurnalDlvryC = new GLJournalD(i);
                            jurnalDlvryC.CompanyID = "001";
                            jurnalDlvryC.BranchID = jurnalH.BranchID;
                            jurnalDlvryC.SequenceNo = i + 1;
                            jurnalDlvryC.CoaCo = "001";
                            jurnalDlvryC.CoaBranch = jurnalH.BranchID;
                            jurnalDlvryC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalDlvryC.TransactionID = "RIV";
                            jurnalDlvryC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalDlvryC.Post = "C";
                            jurnalDlvryC.Amount = totalDeliveryCost;
                            jurnalDlvryC.CoaId_X = "-";
                            jurnalDlvryC.PaymentAllocationId = "AP";
                            jurnalDlvryC.ProductId = "-";
                            jurnalDlvryC.DtmUpd = jurnalH.DtmUpd;
                            jurnalDlvryC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalDlvryC);
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalDlvryC));
                            i++;
                        }
                    }
                    #endregion

                    #region NON AKTIVA
                    if (isNonActiva)
                    {
                        double totalDeliveryCost = 0;
                        foreach (var item in InvNA)
                        {
                            //DEBT
                            NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);
                            if (nam.SiteID.Trim() != SiteID.Trim()) { continue; }

                            GLJournalD jurnalD = new GLJournalD(i);
                            GroupAsset GA = _groupAssetRepository.getGroupAsset(item.GroupAssetID);


                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = nam.SiteID.ToString().Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = nam.SiteID.ToString().Trim();
                            jurnalD.CoaId = paPph.IsNull() ? "" : paPph.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Biaya Kirim - " + (nam.IsNull() ? "-" : nam.NamaNonAktiva.Trim());
                            jurnalD.Post = "D";
                            jurnalD.Amount = item.ExpedCost;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalDeliveryCost += jurnalD.Amount;
                            i++;
                        }

                        //CREDIT
                        if (totalDeliveryCost > 0)
                        {
                            GLJournalD jurnalC = new GLJournalD(i);
                            jurnalC.CompanyID = "001";
                            jurnalC.BranchID = jurnalH.BranchID.Trim();
                            jurnalC.SequenceNo = i + 1;
                            jurnalC.CoaCo = "001";
                            jurnalC.CoaBranch = jurnalH.BranchID.Trim();
                            jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalC.TransactionID = "RIV";
                            jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalC.Post = "C";
                            jurnalC.Amount = totalDeliveryCost;
                            jurnalC.CoaId_X = "-";
                            jurnalC.PaymentAllocationId = "AP";
                            jurnalC.ProductId = "-";
                            jurnalC.DtmUpd = jurnalH.DtmUpd;
                            jurnalC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalC);
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));
                            i++;
                        }
                    }
                    #endregion
                    #endregion

                    #region Jurnal Dtl OtherCost
                    #region AKTIVA
                    if (isActiva)
                    {
                        double totalOtherCost = 0;
                        foreach (var item in InvA)
                        {
                            //DEBT
                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.StartingAktivaId);
                            if (famData.SiteID.Trim() != SiteID.Trim()) { continue; }

                            GLJournalD jurnalPph = new GLJournalD(i);
                            GLJournalD jurnalDlvry = new GLJournalD(i);

                            if (item.OtherCost > 0)
                            {
                                jurnalPph.CompanyID = "001";
                                jurnalPph.BranchID = famData.SiteID.ToString().Trim();
                                jurnalPph.SequenceNo = i + 1;
                                jurnalPph.CoaCo = "001";
                                jurnalPph.CoaBranch = famData.SiteID.ToString().Trim();
                                jurnalPph.CoaId = paOtherCost.IsNull() ? "" : paOtherCost.COA.Trim() + "-" + jurnalH.BranchID;
                                jurnalPph.TransactionID = "RIV";
                                jurnalPph.Tr_Desc = "Biaya Lain - " + famData.NamaAktiva.Trim();
                                jurnalPph.Post = "D";
                                jurnalPph.Amount = item.OtherCost;
                                jurnalPph.CoaId_X = "-";
                                jurnalPph.PaymentAllocationId = "FA";
                                jurnalPph.ProductId = "-";
                                jurnalPph.DtmUpd = jurnalH.DtmUpd;
                                jurnalPph.UsrUpd = jurnalH.UsrUpd;

                                jurnalH.JournalAmount += jurnalPph.Amount;
                                jurnalH.GLJournalDtlList.Add(jurnalPph);

                                ifinsJurnalH.JournalAmount += jurnalPph.Amount;
                                ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalPph));

                                totalOtherCost += jurnalPph.Amount;
                                i++;
                            }
                        }

                        //CREDIT
                        if (totalOtherCost > 0)
                        {
                            GLJournalD jurnalPphC = new GLJournalD(i);
                            jurnalPphC.CompanyID = "001";
                            jurnalPphC.BranchID = jurnalH.BranchID;
                            jurnalPphC.SequenceNo = i + 1;
                            jurnalPphC.CoaCo = "001";
                            jurnalPphC.CoaBranch = jurnalH.BranchID;
                            jurnalPphC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalPphC.TransactionID = "RIV";
                            jurnalPphC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalPphC.Post = "C";
                            jurnalPphC.Amount = totalOtherCost;
                            jurnalPphC.CoaId_X = "-";
                            jurnalPphC.PaymentAllocationId = "AP";
                            jurnalPphC.ProductId = "-";
                            jurnalPphC.DtmUpd = jurnalH.DtmUpd;
                            jurnalPphC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalPphC);
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalPphC));
                            i++;
                        }
                    }
                    #endregion
                    #endregion

                    jurnalHList.Add(jurnalH);
                    ifinsjurnalHList.Add(ifinsJurnalH);
                }


                for (int j = 0; j <= jurnalHList.Count-1; j++)
                {
                    message = _glJournalService.SaveJournal(jurnalHList[j], user, company);
                    string[] msgResult = message.Split('-');
                    if (msgResult.Length > 0)
                    {
                        if (msgResult[0].Trim() == "Success")
                        {
                            ifinsjurnalHList[j].Tr_Nomor = msgResult[1].Trim();

                            _ifinsGLJournalRepository.Add(ifinsjurnalHList[j]);

                            if (ifinsjurnalHList[j].GLJournalDtlList != null)
                            {
                                foreach (var item in ifinsjurnalHList[j].GLJournalDtlList)
                                {
                                    item.Tr_Nomor = ifinsjurnalHList[j].Tr_Nomor;
                                    _ifinsGLJournalRepository.AddGlJournalDtl(item);
                                }
                            }

                            _auditRepository.SaveAuditTrail(Settings.ModuleName, jurnalHList[j], user.Username, "Create");
                        }
                    }
                }

                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Approve_succes));
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                message = e.Message;
            }

            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(inhv);
        }

        public ActionResult Reject(string id)
        {
            ViewData["ActionName"] = "Reject";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            string message = "";
            bool isActiva = false;
            bool isNonActiva = false;
            bool isService = false;

            InvoiceH newData = _InvoiceRepository.getInvoice(id);
            InvoiceHView inhv = ConvertFrom(newData);

            try
            {

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeApprove");

                bool isApproved = newData.Status.Trim() == "A" ? true : false;

                newData.Status = InvoiceStatusEnum.X.ToString();
                newData.UpdateUser = user.Username;
                newData.UpdateDate = DateTime.Now;

                _InvoiceRepository.Save(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterApprove");

                if (isApproved)
                {
                    //create GL Journal reversal
                    int year = int.Parse(DateTime.Now.ToString("yyyy"));
                    int month = int.Parse(DateTime.Now.ToString("MM"));

                    switch (newData.InvoiceTypeID.Trim())
                    {
                        case "AC":
                            isActiva = true;
                            break;
                        case "ANA":
                            isNonActiva = true;
                            isActiva = true;
                            break;
                        case "APA":
                            isActiva = true;
                            isService = true;
                            break;
                        case "NA":
                            isNonActiva = true;
                            break;
                        case "NPA":
                            isNonActiva = true;
                            isService = true;
                            break;
                        case "PA":
                            isService = true;
                            break;
                        case "ST":
                            isActiva = true;
                            isNonActiva = true;
                            isService = true;
                            break;
                        default:
                            break;
                    }

                    IList<InvoiceAktiva> InvA = _InvoiceRepository.getInvoiceAktiva(newData.InvoiceNo);
                    IList<InvoiceNonAktiva> InvNA = _InvoiceRepository.getInvoiceNonAktiva(newData.InvoiceNo);
                    IList<InvoiceService> InvS = _InvoiceRepository.getInvoiceService(newData.InvoiceNo);

                    PaymentAllocation payallocAP = _paymentAllocationRepository.getPaymentAllocation("APVE");
                    PaymentAllocation payallocDlvryCost = _paymentAllocationRepository.getPaymentAllocation("E-POS");
                    PaymentAllocation paPph = _paymentAllocationRepository.getPaymentAllocation("APPPH23");
                    PaymentAllocation paOtherCost = _paymentAllocationRepository.getPaymentAllocation("OTHERPAY");

                    #region Jurnal Hdr
                    GLJournalH jurnalH = new GLJournalH("");
                    IfinsGLJournalH ifinsJurnalH = new IfinsGLJournalH("");

                    jurnalH.CompanyID = "001";
                    jurnalH.BranchID = user.Site.SiteID.ToString();
                    jurnalH.PeriodYear = year.ToString();
                    jurnalH.PeriodMonth = month.ToString();
                    jurnalH.TransactionID = "RIV";
                    jurnalH.Reff_No = newData.InvoiceNo.Trim();
                    jurnalH.Reff_Date = DateTime.Now;
                    jurnalH.Tr_Date = DateTime.Now;
                    jurnalH.tr_Date1 = (DateTime?)null;
                    jurnalH.PrintDate = (DateTime?)null;
                    jurnalH.LastRevisiPosting = (DateTime?)null;
                    jurnalH.Tr_Desc = "Reversal Terima Invoice Vendor";
                    jurnalH.JournalAmount = 0;// newData.InvoiceAmount;
                    jurnalH.BatchID = "RIV";
                    jurnalH.SubSystem = "FA";
                    jurnalH.Status_Tr = "OP";
                    jurnalH.IsActive = true;
                    jurnalH.IsValid = true;
                    jurnalH.Flag = "R";
                    jurnalH.DtmUpd = DateTime.Now;
                    jurnalH.UsrUpd = user.Username;

                    ifinsJurnalH.CompanyID = "001";
                    ifinsJurnalH.BranchID = user.Site.SiteID.ToString();
                    ifinsJurnalH.PeriodYear = year.ToString();
                    ifinsJurnalH.PeriodMonth = month.ToString();
                    ifinsJurnalH.TransactionID = "RIV";
                    ifinsJurnalH.Reff_No = newData.InvoiceNo.Trim();
                    ifinsJurnalH.Reff_Date = DateTime.Now;
                    ifinsJurnalH.Tr_Date = DateTime.Now;
                    ifinsJurnalH.tr_Date1 = (DateTime?)null;
                    ifinsJurnalH.PrintDate = (DateTime?)null;
                    ifinsJurnalH.LastRevisiPosting = (DateTime?)null;
                    ifinsJurnalH.Tr_Desc = "Reversal Invoice Vendor";
                    ifinsJurnalH.JournalAmount = 0;// newData.InvoiceAmount;
                    ifinsJurnalH.BatchID = "RIV";
                    ifinsJurnalH.SubSystem = "FA";
                    ifinsJurnalH.Status_Tr = "OP";
                    ifinsJurnalH.IsActive = true;
                    ifinsJurnalH.IsValid = true;
                    ifinsJurnalH.Flag = "R";
                    ifinsJurnalH.DtmUpd = DateTime.Now;
                    ifinsJurnalH.UsrUpd = user.Username;
                    #endregion

                    int i = 0;
                    jurnalH.GLJournalDtlList = new List<GLJournalD>();
                    ifinsJurnalH.GLJournalDtlList = new List<IfinsGLJournalD>();

                    #region Jurnal Dtl AKTIVA
                    if (isActiva)
                    {
                        i = 0;
                        double totalPrice = 0;

                        foreach (var item in InvA)
                        {
                            //CREDIT
                            GLJournalD jurnalD = new GLJournalD(i);

                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.StartingAktivaId);
                            GroupAsset GA = _groupAssetRepository.getGroupAsset(famData.IsNull() ? "" : famData.GroupAssetID);

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = user.Site.SiteID.ToString().Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = user.Site.SiteID.ToString().Trim();
                            jurnalD.CoaId = GA.IsNull() ? "" : GA.CoaPerolehan.Trim() + "-" + jurnalH.BranchID;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Aktiva - " + famData.NamaAktiva.Trim();
                            jurnalD.Post = "C";
                            jurnalD.Amount = item.Price + item.PPnAmount;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalPrice += jurnalD.Amount;

                            i++;

                            if (famData.IsOLS)
                            {
                                Inventory invt = _inventoryRepository.getInventory(item.StartingAktivaId);
                                if (invt != null)
                                {
                                    invt.GRNDate = invt.GRNDate.IsNull() ? (DateTime?)null : invt.GRNDate;
                                    invt.TotalOTR = Convert.ToDouble(item.TotalAmount);
                                    invt.SupplierID = newData.VendorID;
                                    invt.UserLastUpdate = user.Username;
                                    invt.DateLastUpdate = DateTime.Now;

                                    _inventoryRepository.Save(invt);
                                }
                            }
                        }

                        //DEBT
                        if (totalPrice > 0)
                        {
                            GLJournalD jurnalC = new GLJournalD(i);

                            jurnalC.CompanyID = "001";
                            jurnalC.BranchID = user.Site.SiteID.ToString().Trim(); ;
                            jurnalC.SequenceNo = i + 1;
                            jurnalC.CoaCo = "001";
                            jurnalC.CoaBranch = user.Site.SiteID.ToString().Trim(); ;
                            jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalC.TransactionID = "RIV";
                            jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalC.Post = "D";
                            jurnalC.Amount = totalPrice;
                            jurnalC.CoaId_X = "-";
                            jurnalC.PaymentAllocationId = "AP";
                            jurnalC.ProductId = "-";
                            jurnalC.DtmUpd = jurnalH.DtmUpd;
                            jurnalC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalC);

                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));

                            i++;
                        }
                    }
                    #endregion

                    #region Jurnal Dtl NON AKTIVA
                    if (isNonActiva)
                    {
                        double totalPrice = 0;
                        foreach (var item in InvNA)
                        {
                            //CREDIT
                            GLJournalD jurnalD = new GLJournalD(i);
                            GroupAsset GA = _groupAssetRepository.getGroupAsset(item.GroupAssetID);
                            PaymentAllocation payallocPrepaid = _paymentAllocationRepository.getPaymentAllocation(item.PrepaidPA);
                            PaymentAllocation payallocExpence = _paymentAllocationRepository.getPaymentAllocation(item.ExpensePA);
                            NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = user.Site.SiteID.ToString().Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = user.Site.SiteID.ToString().Trim();
                            jurnalD.CoaId = item.ExpenseType == "L" ? (payallocExpence.IsNull() ? "" : payallocExpence.COA + "-" + jurnalD.CoaBranch) : payallocPrepaid.IsNull() ? "" : payallocPrepaid.COA + "-" + jurnalD.CoaBranch;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Non Aktiva " + (nam.IsNull() ? "-" : nam.NamaNonAktiva.Trim());
                            jurnalD.Post = "C";
                            jurnalD.Amount = item.Price + item.PPnAmount;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalPrice += jurnalD.Amount;
                            i++;
                        }

                        //DEBT
                        if (totalPrice > 0)
                        {
                            GLJournalD jurnalC = new GLJournalD(i);

                            jurnalC.CompanyID = "001";
                            jurnalC.BranchID = user.Site.SiteID.ToString().Trim(); ;
                            jurnalC.SequenceNo = i + 1;
                            jurnalC.CoaCo = "001";
                            jurnalC.CoaBranch = user.Site.SiteID.ToString().Trim(); ;
                            jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalC.TransactionID = "RIV";
                            jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalC.Post = "C";
                            jurnalC.Amount = totalPrice;
                            jurnalC.CoaId_X = "-";
                            jurnalC.PaymentAllocationId = "AP";
                            jurnalC.ProductId = "-";
                            jurnalC.DtmUpd = jurnalH.DtmUpd;
                            jurnalC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalC);

                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));

                            i++;
                        }
                    }
                    #endregion

                    #region Jurnal Dtl SERVICE
                    if (isService)
                    {
                        double totalPrice = 0;
                        foreach (var item in InvS)
                        {
                            //CREDIT
                            GLJournalD jurnalD = new GLJournalD(i);

                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);
                            GroupAsset GA = _groupAssetRepository.getGroupAsset(famData.IsNull() ? "" : famData.GroupAssetID);

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = user.Site.SiteID.ToString().Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = user.Site.SiteID.ToString().Trim();
                            jurnalD.CoaId = GA.IsNull() ? "" : GA.CoaBeban.Trim() + "-" + jurnalD.CoaBranch;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Biaya Perawatan Aktiva";
                            jurnalD.Post = "C";
                            jurnalD.Amount = item.ServiceAmount + item.PPnAmount + item.ItemAmount;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalPrice += jurnalD.Amount;
                            i++;
                        }

                        //DEBT
                        if (totalPrice > 0)
                        {
                            GLJournalD jurnalC = new GLJournalD(i);

                            jurnalC.CompanyID = "001";
                            jurnalC.BranchID = user.Site.SiteID.ToString().Trim();
                            jurnalC.SequenceNo = i + 1;
                            jurnalC.CoaCo = "001";
                            jurnalC.CoaBranch = user.Site.SiteID.ToString().Trim();
                            jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalC.TransactionID = "RIV";
                            jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalC.Post = "D";
                            jurnalC.Amount = totalPrice;
                            jurnalC.CoaId_X = "-";
                            jurnalC.PaymentAllocationId = "AP";
                            jurnalC.ProductId = "-";
                            jurnalC.DtmUpd = jurnalH.DtmUpd;
                            jurnalC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalC);

                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));

                            i++;
                        }
                    }
                    #endregion

                    #region Jurnal Dtl PPh

                    #region AKTIVA
                    if (isActiva)
                    {
                        double totalPph = 0;
                        foreach (var item in InvA)
                        {
                            //CREDIT
                            GLJournalD jurnalPph = new GLJournalD(i);
                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.StartingAktivaId);

                            if (item.PPhAmount > 0)
                            {
                                jurnalPph.CompanyID = "001";
                                jurnalPph.BranchID = user.Site.SiteID.ToString().Trim();
                                jurnalPph.SequenceNo = i + 1;
                                jurnalPph.CoaCo = "001";
                                jurnalPph.CoaBranch = user.Site.SiteID.ToString().Trim();
                                jurnalPph.CoaId = paPph.IsNull() ? "" : paPph.COA.Trim() + "-" + jurnalH.BranchID;
                                jurnalPph.TransactionID = "RIV";
                                jurnalPph.Tr_Desc = "Piutang PPH - " + famData.NamaAktiva.Trim();
                                jurnalPph.Post = "C";
                                jurnalPph.Amount = item.PPhAmount;
                                jurnalPph.CoaId_X = "-";
                                jurnalPph.PaymentAllocationId = "FA";
                                jurnalPph.ProductId = "-";
                                jurnalPph.DtmUpd = jurnalH.DtmUpd;
                                jurnalPph.UsrUpd = jurnalH.UsrUpd;

                                jurnalH.JournalAmount += jurnalPph.Amount;
                                jurnalH.GLJournalDtlList.Add(jurnalPph);

                                ifinsJurnalH.JournalAmount += jurnalPph.Amount;
                                ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalPph));

                                totalPph += jurnalPph.Amount;
                                i++;
                            }
                        }

                        //DEBT
                        if (totalPph > 0)
                        {
                            GLJournalD jurnalPphC = new GLJournalD(i);
                            jurnalPphC.CompanyID = "001";
                            jurnalPphC.BranchID = user.Site.SiteID.ToString().Trim(); ;
                            jurnalPphC.SequenceNo = i + 1;
                            jurnalPphC.CoaCo = "001";
                            jurnalPphC.CoaBranch = user.Site.SiteID.ToString().Trim(); ;
                            jurnalPphC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalPphC.TransactionID = "RIV";
                            jurnalPphC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalPphC.Post = "D";
                            jurnalPphC.Amount = totalPph;
                            jurnalPphC.CoaId_X = "-";
                            jurnalPphC.PaymentAllocationId = "AP";
                            jurnalPphC.ProductId = "-";
                            jurnalPphC.DtmUpd = jurnalH.DtmUpd;
                            jurnalPphC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalPphC);
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalPphC));
                            i++;
                        }
                    }
                    #endregion

                    #region NON AKTIVA
                    if (isNonActiva)
                    {
                        double totalPph = 0;
                        foreach (var item in InvNA)
                        {
                            //CREDIT
                            GLJournalD jurnalD = new GLJournalD(i);
                            GroupAsset GA = _groupAssetRepository.getGroupAsset(item.GroupAssetID);
                            NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = user.Site.SiteID.ToString().Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = user.Site.SiteID.ToString().Trim();
                            jurnalD.CoaId = paPph.IsNull() ? "" : paPph.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Piutang PPH - " + (nam.IsNull() ? "-" : nam.NamaNonAktiva.Trim());
                            jurnalD.Post = "C";
                            jurnalD.Amount = item.PPhAmount;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalPph += jurnalD.Amount;
                            i++;
                        }

                        //DEBT
                        GLJournalD jurnalC = new GLJournalD(i);
                        jurnalC.CompanyID = "001";
                        jurnalC.BranchID = user.Site.SiteID.ToString().Trim(); ;
                        jurnalC.SequenceNo = i + 1;
                        jurnalC.CoaCo = "001";
                        jurnalC.CoaBranch = user.Site.SiteID.ToString().Trim(); ;
                        jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                        jurnalC.TransactionID = "RIV";
                        jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                        jurnalC.Post = "C";
                        jurnalC.Amount = totalPph;
                        jurnalC.CoaId_X = "-";
                        jurnalC.PaymentAllocationId = "AP";
                        jurnalC.ProductId = "-";
                        jurnalC.DtmUpd = jurnalH.DtmUpd;
                        jurnalC.UsrUpd = jurnalH.UsrUpd;

                        jurnalH.GLJournalDtlList.Add(jurnalC);
                        ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));
                        i++;
                    }
                    #endregion

                    #region SERVICE
                    if (isService)
                    {
                        double totalPph = 0;
                        foreach (var item in InvS)
                        {
                            //DEBT
                            GLJournalD jurnalD = new GLJournalD(i);

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = user.Site.SiteID.ToString().Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = user.Site.SiteID.ToString().Trim();
                            jurnalD.CoaId = paPph.IsNull() ? "" : paPph.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Piutang PPH - Biaya Perawatan Aktiva";
                            jurnalD.Post = "D";
                            jurnalD.Amount = item.PPhAmount;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalPph += jurnalD.Amount;
                            i++;
                        }

                        //CREDIT
                        GLJournalD jurnalC = new GLJournalD(i);
                        jurnalC.CompanyID = "001";
                        jurnalC.BranchID = user.Site.SiteID.ToString().Trim();
                        jurnalC.SequenceNo = i + 1;
                        jurnalC.CoaCo = "001";
                        jurnalC.CoaBranch = user.Site.SiteID.ToString().Trim();
                        jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                        jurnalC.TransactionID = "RIV";
                        jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                        jurnalC.Post = "C";
                        jurnalC.Amount = totalPph;
                        jurnalC.CoaId_X = "-";
                        jurnalC.PaymentAllocationId = "AP";
                        jurnalC.ProductId = "-";
                        jurnalC.DtmUpd = jurnalH.DtmUpd;
                        jurnalC.UsrUpd = jurnalH.UsrUpd;

                        jurnalH.GLJournalDtlList.Add(jurnalC);

                        ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));
                        i++;
                    }
                    #endregion

                    #endregion

                    #region Jurnal Dtl DeliveryCost
                    #region AKTIVA
                    if (isActiva)
                    {
                        double totalDeliveryCost = 0;
                        foreach (var item in InvA)
                        {
                            //CREDIT
                            GLJournalD jurnalDlvry = new GLJournalD(i);
                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.StartingAktivaId);

                            if (item.PPhAmount > 0)
                            {
                                jurnalDlvry.CompanyID = "001";
                                jurnalDlvry.BranchID = user.Site.SiteID.ToString().Trim();
                                jurnalDlvry.SequenceNo = i + 1;
                                jurnalDlvry.CoaCo = "001";
                                jurnalDlvry.CoaBranch = user.Site.SiteID.ToString().Trim();
                                jurnalDlvry.CoaId = paPph.IsNull() ? "" : paPph.COA.Trim() + "-" + jurnalH.BranchID;
                                jurnalDlvry.TransactionID = "RIV";
                                jurnalDlvry.Tr_Desc = "Biaya Kirim - " + famData.NamaAktiva.Trim();
                                jurnalDlvry.Post = "C";
                                jurnalDlvry.Amount = item.ExpedCost;
                                jurnalDlvry.CoaId_X = "-";
                                jurnalDlvry.PaymentAllocationId = "FA";
                                jurnalDlvry.ProductId = "-";
                                jurnalDlvry.DtmUpd = jurnalH.DtmUpd;
                                jurnalDlvry.UsrUpd = jurnalH.UsrUpd;

                                jurnalH.JournalAmount += jurnalDlvry.Amount;
                                jurnalH.GLJournalDtlList.Add(jurnalDlvry);

                                ifinsJurnalH.JournalAmount += jurnalDlvry.Amount;
                                ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalDlvry));

                                totalDeliveryCost += jurnalDlvry.Amount;
                                i++;
                            }
                        }

                        //DEBT
                        if (totalDeliveryCost > 0)
                        {
                            GLJournalD jurnalDlvryC = new GLJournalD(i);
                            jurnalDlvryC.CompanyID = "001";
                            jurnalDlvryC.BranchID = user.Site.SiteID.ToString().Trim(); ;
                            jurnalDlvryC.SequenceNo = i + 1;
                            jurnalDlvryC.CoaCo = "001";
                            jurnalDlvryC.CoaBranch = user.Site.SiteID.ToString().Trim(); ;
                            jurnalDlvryC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalDlvryC.TransactionID = "RIV";
                            jurnalDlvryC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalDlvryC.Post = "D";
                            jurnalDlvryC.Amount = totalDeliveryCost;
                            jurnalDlvryC.CoaId_X = "-";
                            jurnalDlvryC.PaymentAllocationId = "AP";
                            jurnalDlvryC.ProductId = "-";
                            jurnalDlvryC.DtmUpd = jurnalH.DtmUpd;
                            jurnalDlvryC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalDlvryC);
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalDlvryC));
                            i++;
                        }
                    }
                    #endregion

                    #region NON AKTIVA
                    if (isNonActiva)
                    {
                        double totalDeliveryCost = 0;
                        foreach (var item in InvNA)
                        {
                            //CREDIT
                            GLJournalD jurnalD = new GLJournalD(i);
                            GroupAsset GA = _groupAssetRepository.getGroupAsset(item.GroupAssetID);
                            NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);

                            jurnalD.CompanyID = "001";
                            jurnalD.BranchID = user.Site.SiteID.ToString().Trim();
                            jurnalD.SequenceNo = i + 1;
                            jurnalD.CoaCo = "001";
                            jurnalD.CoaBranch = user.Site.SiteID.ToString().Trim();
                            jurnalD.CoaId = paPph.IsNull() ? "" : paPph.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalD.TransactionID = "RIV";
                            jurnalD.Tr_Desc = "Biaya Kirim - " + (nam.IsNull() ? "-" : nam.NamaNonAktiva.Trim());
                            jurnalD.Post = "C";
                            jurnalD.Amount = item.ExpedCost;
                            jurnalD.CoaId_X = "-";
                            jurnalD.PaymentAllocationId = "FA";
                            jurnalD.ProductId = "-";
                            jurnalD.DtmUpd = jurnalH.DtmUpd;
                            jurnalD.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.JournalAmount += jurnalD.Amount;
                            jurnalH.GLJournalDtlList.Add(jurnalD);

                            ifinsJurnalH.JournalAmount += jurnalD.Amount;
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalD));

                            totalDeliveryCost += jurnalD.Amount;
                            i++;
                        }

                        //DEMT
                        if (totalDeliveryCost > 0)
                        {
                            GLJournalD jurnalC = new GLJournalD(i);
                            jurnalC.CompanyID = "001";
                            jurnalC.BranchID = user.Site.SiteID.ToString().Trim(); ;
                            jurnalC.SequenceNo = i + 1;
                            jurnalC.CoaCo = "001";
                            jurnalC.CoaBranch = user.Site.SiteID.ToString().Trim(); ;
                            jurnalC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalC.TransactionID = "RIV";
                            jurnalC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalC.Post = "D";
                            jurnalC.Amount = totalDeliveryCost;
                            jurnalC.CoaId_X = "-";
                            jurnalC.PaymentAllocationId = "AP";
                            jurnalC.ProductId = "-";
                            jurnalC.DtmUpd = jurnalH.DtmUpd;
                            jurnalC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalC);
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalC));
                            i++;
                        }
                    }
                    #endregion
                    #endregion

                    #region Jurnal Dtl OtherCost
                    #region AKTIVA
                    if (isActiva)
                    {
                        double totalOtherCost = 0;
                        foreach (var item in InvA)
                        {
                            //credit
                            GLJournalD jurnalPph = new GLJournalD(i);
                            GLJournalD jurnalDlvry = new GLJournalD(i);
                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(item.StartingAktivaId);

                            if (item.OtherCost > 0)
                            {
                                jurnalPph.CompanyID = "001";
                                jurnalPph.BranchID = user.Site.SiteID.ToString().Trim();
                                jurnalPph.SequenceNo = i + 1;
                                jurnalPph.CoaCo = "001";
                                jurnalPph.CoaBranch = user.Site.SiteID.ToString().Trim();
                                jurnalPph.CoaId = paOtherCost.IsNull() ? "" : paOtherCost.COA.Trim() + "-" + jurnalH.BranchID;
                                jurnalPph.TransactionID = "RIV";
                                jurnalPph.Tr_Desc = "Biaya Lain - " + famData.NamaAktiva.Trim();
                                jurnalPph.Post = "C";
                                jurnalPph.Amount = item.OtherCost;
                                jurnalPph.CoaId_X = "-";
                                jurnalPph.PaymentAllocationId = "FA";
                                jurnalPph.ProductId = "-";
                                jurnalPph.DtmUpd = jurnalH.DtmUpd;
                                jurnalPph.UsrUpd = jurnalH.UsrUpd;

                                jurnalH.JournalAmount += jurnalPph.Amount;
                                jurnalH.GLJournalDtlList.Add(jurnalPph);

                                ifinsJurnalH.JournalAmount += jurnalPph.Amount;
                                ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalPph));

                                totalOtherCost += jurnalPph.Amount;
                                i++;
                            }
                        }

                        //DEBT
                        if (totalOtherCost > 0)
                        {
                            GLJournalD jurnalPphC = new GLJournalD(i);
                            jurnalPphC.CompanyID = "001";
                            jurnalPphC.BranchID = user.Site.SiteID.ToString().Trim(); ;
                            jurnalPphC.SequenceNo = i + 1;
                            jurnalPphC.CoaCo = "001";
                            jurnalPphC.CoaBranch = user.Site.SiteID.ToString().Trim(); ;
                            jurnalPphC.CoaId = payallocAP.IsNull() ? "" : payallocAP.COA.Trim() + "-" + jurnalH.BranchID;
                            jurnalPphC.TransactionID = "RIV";
                            jurnalPphC.Tr_Desc = "AP - " + inhv.VendorName.Trim();
                            jurnalPphC.Post = "D";
                            jurnalPphC.Amount = totalOtherCost;
                            jurnalPphC.CoaId_X = "-";
                            jurnalPphC.PaymentAllocationId = "AP";
                            jurnalPphC.ProductId = "-";
                            jurnalPphC.DtmUpd = jurnalH.DtmUpd;
                            jurnalPphC.UsrUpd = jurnalH.UsrUpd;

                            jurnalH.GLJournalDtlList.Add(jurnalPphC);
                            ifinsJurnalH.GLJournalDtlList.Add(ConvertForm(jurnalPphC));
                            i++;
                        }
                    }
                    #endregion

                    #endregion


                    message = _glJournalService.SaveJournal(jurnalH, user, company);
                    string[] msgResult = message.Split('-');
                    if (msgResult.Length > 0)
                    {
                        if (msgResult[0].Trim() == "Success")
                        {
                            ifinsJurnalH.Tr_Nomor = msgResult[1].Trim();

                            _ifinsGLJournalRepository.Add(ifinsJurnalH);

                            if (ifinsJurnalH.GLJournalDtlList != null)
                            {
                                foreach (var item in ifinsJurnalH.GLJournalDtlList)
                                {
                                    item.Tr_Nomor = ifinsJurnalH.Tr_Nomor;
                                    _ifinsGLJournalRepository.AddGlJournalDtl(item);
                                }
                            }

                            _auditRepository.SaveAuditTrail(Settings.ModuleName, jurnalH, user.Username, "Create Jurnal Reversal");
                            ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Reject_success));
                            return RedirectToAction("Index");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(inhv);
        }

        public ActionResult Approve(string id)
        {
            ViewData["ActionName"] = "Approve";
            InvoiceH InvoiceItem = _InvoiceRepository.getInvoice(id);
            InvoiceHView data = ConvertFrom(InvoiceItem);
            VendorAccount VendorAccount = _VendorAccountRepository.getVendorAccount(data.VendorBankId.Trim());
            data.Mode = "Approve";

            if (VendorAccount != null)
            {
                data.VendorAccountName = VendorAccount.VendorAccountName;
                data.VendorAccountNo = VendorAccount.VendorAccountNo;
                data.VendorBankBranch = VendorAccount.VendorBankBranch;
            }

            IList<InvoiceAktiva> InvA = _InvoiceRepository.getInvoiceAktiva(data.InvoiceNo);
            IList<InvoiceAktivaView> InvAView = InvA.ToList().ConvertAll<InvoiceAktivaView>(new Converter<InvoiceAktiva, InvoiceAktivaView>(ConvertFrom));
            data.InvoiceAktivaList = InvAView.IsNullOrEmpty() ? CreateNewAktivaDetail() : InvAView;
            IList<InvoiceNonAktiva> InvNA = _InvoiceRepository.getInvoiceNonAktiva(data.InvoiceNo);
            IList<InvoiceNonAktivaView> InvNAView = InvNA.ToList().ConvertAll<InvoiceNonAktivaView>(new Converter<InvoiceNonAktiva, InvoiceNonAktivaView>(ConvertFrom));
            data.InvoiceNonAktivaList = InvNAView.IsNullOrEmpty() ? CreateNewNonAktivaDetail() : InvNAView;
            IList<InvoiceService> InvS = _InvoiceRepository.getInvoiceService(data.InvoiceNo);
            IList<InvoiceServiceView> InvSView = InvS.ToList().ConvertAll<InvoiceServiceView>(new Converter<InvoiceService, InvoiceServiceView>(ConvertFrom));
            data.InvoiceServiceList = InvSView.IsNullOrEmpty() ? CreateNewserviceDetail() : InvSView;
            return CreateView(data);

        }

        public InvoiceAktivaView ConvertFrom(InvoiceAktiva item)
        {
            InvoiceAktivaView returnItem = new InvoiceAktivaView();
            FixedAssetMaster FA = _fixedAssetMasterRepository.getFixedAssetMaster(item.StartingAktivaId);
            GroupAsset GA = _groupAssetRepository.getGroupAsset(FA.IsNull() ? "" : FA.GroupAssetID);

            returnItem.InvoiceNo = item.InvoiceNo.Trim();
            returnItem.Seq = item.Seq;
            returnItem.StartingAktivaId = item.StartingAktivaId.Trim();
            returnItem.AktivaName = FA.IsNull() ? "" : FA.NamaAktiva.Trim();
            returnItem.ID = item.ID;
            returnItem.COABeban = GA.IsNull() ? "" : GA.CoaBeban;
            //returnItem.PurchaseOrderNo = item.PurchaseOrderNo.Trim();
            //returnItem.DeliveryOrderNo = item.DeliveryOrderNo.Trim();
            //returnItem.GRNo = item.GRNo.Trim();
            returnItem.TanggalPerolehan = item.TanggalPerolehan.ToString("dd/MM/yyyy");
            returnItem.Quantity = item.Quantity;
            returnItem.Price = item.Price.ToString("N0");
            returnItem.TotalPrice = item.TotalPrice.ToString("N0");
            returnItem.OtherCost = item.OtherCost.ToString("N0");
            returnItem.ExpedCost = item.ExpedCost.ToString("N0");
            //returnItem.PPnId = item.PPnId.ToString().Trim();
            returnItem.PPnAmount = item.PPnAmount.ToString("N0");
            //returnItem.PPhId = item.PPhId.ToString().Trim();
            returnItem.PPhAmount = item.PPhAmount.ToString("N0");
            returnItem.Notes = item.Notes;
            returnItem.TotalAmount = item.TotalAmount.ToString("N0");

            return returnItem;
        }
        public InvoiceNonAktivaView ConvertFrom(InvoiceNonAktiva item)
        {
            //GroupAsset GA = _groupAssetRepository.getGroupAsset(item.GroupAssetID);
            NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);

            InvoiceNonAktivaView returnItem = new InvoiceNonAktivaView();
            returnItem.ID = item.ID;
            returnItem.InvoiceNo = item.InvoiceNo;
            returnItem.Seq = item.Seq;
            returnItem.NonAktivaId = item.NonAktivaId;
            returnItem.NonAktivaName = nam.IsNull() ? "" : nam.NamaNonAktiva.Trim();
            //returnItem.RefferenceNo = item.RefferenceNo;
            //returnItem.GroupAssetID = item.GroupAssetID;
            //returnItem.ItemName = item.ItemName;

            returnItem.Price = item.Price.ToString("N0");
            returnItem.OtherCost = item.OtherCost.ToString("N0");
            returnItem.ExpedCost = item.ExpedCost.ToString("N0");

            //returnItem.Qty = item.Qty;
            //returnItem.TotalPrice = item.TotalPrice.ToString("N0");
            //returnItem.COABeban = GA.IsNull() ? "" : GA.CoaBeban;
            returnItem.ExpenseType = item.ExpenseType;
            returnItem.TimePeriod = item.TimePeriod;
            returnItem.StartDate = item.StartDate.IsNull() ? null : Convert.ToDateTime(item.StartDate).ToString("dd/MM/yyyy");
            returnItem.EndDate = item.EndDate.IsNull() ? null : Convert.ToDateTime(item.EndDate).ToString("dd/MM/yyyy");
            returnItem.MonthlyExpense = item.MonthlyExpense.ToString("N0");
            returnItem.ExpensePA = item.ExpensePA;
            returnItem.PrepaidPA = item.PrepaidPA;
            //returnItem.PONo = item.PONo;
            //returnItem.DeliveryOrderNo = item.DeliveryOrderNo;
            //returnItem.GRNo = item.GRNo;
            returnItem.Notes = item.Notes;
            returnItem.DepreciatedTime = item.DepreciatedTime;
            //returnItem.PPnId = item.PPnId.ToString().Trim();
            returnItem.PPnAmount = item.PPnAmount.ToString("N0");
            //returnItem.PPhId = item.PPhId.ToString().Trim();
            returnItem.PPhAmount = item.PPhAmount.ToString("N0");
            returnItem.TotalAmount = item.TotalAmount.ToString("N0");
            return returnItem;
        }
        public InvoiceServiceView ConvertFrom(InvoiceService item)
        {
            InvoiceServiceView returnItem = new InvoiceServiceView();
            FixedAssetMaster FA = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);
            GroupAsset GA = _groupAssetRepository.getGroupAsset(FA.IsNull() ? "" : FA.GroupAssetID);
            Materai materai = _materaiRepository.GetMaterai(item.MateraiID);

            returnItem.ID = item.ID;
            returnItem.InvoiceNo = item.InvoiceNo;
            returnItem.Seq = item.Seq;
            returnItem.AktivaId = item.AktivaId;
            returnItem.COABeban = GA.IsNull() ? "" : GA.CoaBeban;
            returnItem.AktivaName = FA.IsNull() ? "" : FA.NamaAktiva.Trim();
            returnItem.OtherCost = item.OtherCost.ToString("N0");
            //returnItem.PONo = item.PONo;
            returnItem.WorkOrderNo = item.WorkOrderNo;
            returnItem.ExpensePA = item.ExpensePA;
            returnItem.ServiceDate = item.ServiceDate.ToString("dd/MM/yyyy");
            returnItem.ServiceGuaranteeDate = item.ServiceGuaranteeDate.ToString("dd/MM/yyyy");
            returnItem.ServiceAmount = item.ServiceAmount.ToString("N0");
            returnItem.ItemAmount = item.ItemAmount.ToString("N0");
            returnItem.Notes = item.Notes;
            //returnItem.PPnId = item.PPnId.ToString().Trim();
            returnItem.PPnAmount = item.PPnAmount.ToString("N0");
            //returnItem.PPhId = item.PPhId.ToString().Trim();
            returnItem.PPhAmount = item.PPhAmount.ToString("N0");
            returnItem.MateraiID = item.MateraiID;
            returnItem.MateraiAmount = materai.IsNull() ? "0" : materai.MateraiValue.ToString("N0");
            returnItem.TotalAmount = item.TotalAmount.ToString("N0");

            return returnItem;

        }

        private IfinsGLJournalD ConvertForm(GLJournalD item)
        {
            IfinsGLJournalD returnItem = new IfinsGLJournalD(Convert.ToInt32(item.Id));

            returnItem.CompanyID = item.CompanyID;
            returnItem.BranchID = item.BranchID;
            returnItem.SequenceNo = item.SequenceNo;
            returnItem.CoaCo = item.CoaCo;
            returnItem.CoaBranch = item.CoaBranch;
            returnItem.CoaId = item.CoaId;
            returnItem.TransactionID = item.TransactionID;
            returnItem.Tr_Desc = item.Tr_Desc;
            returnItem.Post = item.Post;
            returnItem.Amount = item.Amount;
            returnItem.CoaId_X = item.CoaId_X;
            returnItem.PaymentAllocationId = item.PaymentAllocationId;
            returnItem.ProductId = item.ProductId;
            returnItem.DtmUpd = item.DtmUpd;
            returnItem.UsrUpd = item.UsrUpd;

            return returnItem;
        }

        private IList<InvoiceAktivaView> CreateNewAktivaDetail()
        {
            IList<InvoiceAktivaView> returnDetail = new List<InvoiceAktivaView>();
            returnDetail.Add(AddNewAktivaRecord(returnDetail));
            return returnDetail;
        }
        private IList<InvoiceNonAktivaView> CreateNewNonAktivaDetail()
        {
            IList<InvoiceNonAktivaView> returnDetail = new List<InvoiceNonAktivaView>();
            returnDetail.Add(AddNewNonAktivaRecord(returnDetail));
            return returnDetail;
        }

        private IList<InvoiceServiceView> CreateNewserviceDetail()
        {
            IList<InvoiceServiceView> returnDetail = new List<InvoiceServiceView>();
            returnDetail.Add(AddNewServiceRecord(returnDetail));
            return returnDetail;
        }
        private InvoiceAktivaView AddNewAktivaRecord(IList<InvoiceAktivaView> FppdDetailList)
        {
            InvoiceAktivaView detail = new InvoiceAktivaView();
            detail.ID = FppdDetailList.IsNull() ? 1 : (FppdDetailList.Count > 0) ? FppdDetailList.LastOrDefault().ID + 1 : 1;
            detail.TanggalPerolehan = DateTime.Now.ToString("dd/MM/yyyy");
            detail.Quantity = 1;
            return detail;
        }
        private InvoiceNonAktivaView AddNewNonAktivaRecord(IList<InvoiceNonAktivaView> FppdDetailList)
        {
            InvoiceNonAktivaView detail = new InvoiceNonAktivaView();
            detail.ID = FppdDetailList.IsNull() ? 1 : (FppdDetailList.Count > 0) ? FppdDetailList.LastOrDefault().ID + 1 : 1;
            //detail.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy"); 
            //detail.EndDate = detail.StartDate.AddMonths(1).AddDays(-1);
            //detail.EndDateChanged = 0;

            return detail;
        }
        private InvoiceServiceView AddNewServiceRecord(IList<InvoiceServiceView> FppdDetailList)
        {
            InvoiceServiceView detail = new InvoiceServiceView();
            detail.ID = FppdDetailList.IsNull() ? 1 : (FppdDetailList.Count > 0) ? FppdDetailList.LastOrDefault().ID + 1 : 1;
            detail.ServiceDate = DateTime.Now.ToString("dd/MM/yyyy");
            detail.ServiceGuaranteeDate = DateTime.Now.ToString("dd/MM/yyyy");
            return detail;
        }
        private ViewResult CreateView(InvoiceHView data)
        {
            if (data.VendorID == null)
            {
                ViewData["SupAacc"] = createEmptySupAacc();
            }
            else
            {
                ViewData["SupAacc"] = VendorAccountSelect(data.VendorID.Trim(), data.VendorBankId);

                if (data.VendorBankId.IsNullOrEmpty() || data.VendorBankId == " ")
                {
                    if (!ViewData["SupAacc"].IsNull())
                    {
                        IList<SelectListItem> SupAaccList = (IList<SelectListItem>)ViewData["SupAacc"];
                        string AccID = SupAaccList.FindElement(s => s.Selected == true).Value.ToString();

                        data.VendorBankId = AccID.IsNullOrEmpty() ? "" : AccID;

                        VendorAccount VendorAccount = _VendorAccountRepository.getVendorAccount(AccID);
                        data.VendorAccountName = VendorAccount.IsNull() ? "" : VendorAccount.VendorAccountName;
                        data.VendorAccountNo = VendorAccount.IsNull() ? "" : VendorAccount.VendorAccountNo;
                        data.VendorBankBranch = VendorAccount.IsNull() ? "" : VendorAccount.VendorBankBranch;
                    }
                }
            }
            ViewData["InvoiceType"] = InvoiceTypeSelect(data.InvoiceTypeID == null ? "" : data.InvoiceTypeID);

            generateViewDataGroupAssetSelect(data.InvoiceNonAktivaList);
            generateViewDataExpenceTypeSelect(data.InvoiceNonAktivaList);
            generateViewDataPpnAktivaSelect(data.InvoiceAktivaList, 0);
            generateViewDataPphAktivaSelect(data.InvoiceAktivaList, 1);
            generateViewDataPpnNonAktivaSelect(data.InvoiceNonAktivaList, 0);
            generateViewDataPphNonAktivaSelect(data.InvoiceNonAktivaList, 1);
            generateViewDataPpnServiceSelect(data.InvoiceServiceList, 0);
            generateViewDataPphServiceSelect(data.InvoiceServiceList, 1);
            generateViewDataServiceMateraiSelect(data.InvoiceServiceList);

            return View("Detail", data);
        }

        private IList<SelectListItem> createEmptySupAacc()
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            return dataList;
        }

        private IList<SelectListItem> VendorAccountSelect(string suppId, string selected)
        {
            List<VendorAccount> itemList = _VendorAccountRepository.getBankByVendor(suppId).ToList();

            string defaultItem = "";

            if (itemList.Count > 0)
            {
                var selectedItem = itemList.FindElement(s => s.DefaultAccount == true).VendorAccID;// .Select(s => s.DefaultAccount == true).ToList();
                defaultItem = selectedItem.IsNullOrEmpty() ? "" : selectedItem.Trim();
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<VendorAccount, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _VendorAccID = itemList.FirstOrDefault().VendorAccID;

            if (!selected.IsNull())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
                _VendorAccID = selected;
            }
            else if (!defaultItem.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == defaultItem.ToString()).Selected = true;
                _VendorAccID = selected;
            }
            //else
            //{
            //    dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            //    _VendorAccID = selected;
            //}
            return dataList;
        }

        public SelectListItem ConvertFrom(VendorAccount item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.VendorAccID.Trim() + "-" + item.VendorAccountName.Trim();
            returnItem.Value = item.VendorAccID.ToString().Trim();
            return returnItem;
        }

        private IList<SelectListItem> InvoiceTypeSelect(string selected)
        {
            List<InvoiceType> itemList = Lookup.Get<List<InvoiceType>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _invoiceTypeRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<InvoiceType, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _invoiceTypeID = itemList.FirstOrDefault().InvoiceTypeID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString())).Selected = true;
                _invoiceTypeID = selected;
            }

            return dataList;
        }

        public SelectListItem ConvertForm(InvoiceType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.InvoiceTypeName.Trim();
            returnItem.Value = item.InvoiceTypeID.Trim();
            return returnItem;
        }
        private void generateViewDataServiceMateraiSelect(IList<InvoiceServiceView> InvoiceServiceList)
        {
            int index = 0;
            if (!InvoiceServiceList.IsNullOrEmpty())
            {
                foreach (InvoiceServiceView item in InvoiceServiceList)
                {
                    ViewData["InvoiceServiceList[" + (index).ToString() + "].MateraiID_List"] = MateraiSelect(item.MateraiID);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }
        private void generateViewDataExpenceTypeSelect(IList<InvoiceNonAktivaView> InvoiceNonActivaList)
        {
            int index = 0;
            if (!InvoiceNonActivaList.IsNullOrEmpty())
            {
                foreach (InvoiceNonAktivaView item in InvoiceNonActivaList)
                {
                    ViewData["InvoiceNonAktivaList[" + (index).ToString() + "].ExpenseTypeSelect"] = ExpenseTypeSelect(item.ExpenseType);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }

        private IList<SelectListItem> ExpenseTypeSelect(string selected)
        {
            List<ExpenceType> itemList = Lookup.Get<List<ExpenceType>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _expenceTypeRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<ExpenceType, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _expenceTypeID = itemList.FirstOrDefault().ExpenceTypeID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString())).Selected = true;
                _expenceTypeID = selected;
            }

            return dataList;
        }

        public SelectListItem ConvertForm(ExpenceType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.ExpenceTypeName.Trim();
            returnItem.Value = item.ExpenceTypeID.ToString().Trim();
            return returnItem;
        }

        private void generateViewDataGroupAssetSelect(IList<InvoiceNonAktivaView> InvoiceNonActivaList)
        {
            int index = 0;
            if (!InvoiceNonActivaList.IsNullOrEmpty())
            {
                foreach (InvoiceNonAktivaView item in InvoiceNonActivaList)
                {
                    ViewData["InvoiceNonAktivaList[" + (index).ToString() + "].GroupAssetIDSelect"] = createGroupAssetSelect(item.GroupAssetID);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }

        private void generateViewDataPpnAktivaSelect(IList<InvoiceAktivaView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceAktivaView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceAktivaList[" + (index).ToString() + "].PPnId_tax"] = createTaxSelect(item.PPnId, 0);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }
        private void generateViewDataPphAktivaSelect(IList<InvoiceAktivaView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceAktivaView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceAktivaList[" + (index).ToString() + "].PPhId_tax"] = createTaxSelect(item.PPhId, 1);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }


        private void generateViewDataPpnNonAktivaSelect(IList<InvoiceNonAktivaView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceNonAktivaView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceNonAktivaList[" + (index).ToString() + "].PPnId_tax"] = createTaxSelect(item.PPnId, 0);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }
        private void generateViewDataPphNonAktivaSelect(IList<InvoiceNonAktivaView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceNonAktivaView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceNonAktivaList[" + (index).ToString() + "].PPhId_tax"] = createTaxSelect(item.PPhId, 1);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }

        private void generateViewDataPpnServiceSelect(IList<InvoiceServiceView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceServiceView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceServiceList[" + (index).ToString() + "].PPnId_tax"] = createTaxSelect(item.PPnId, 0);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }
        private void generateViewDataPphServiceSelect(IList<InvoiceServiceView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceServiceView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceServiceList[" + (index).ToString() + "].PPhId_tax"] = createTaxSelect(item.PPhId, 1);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }

        private IList<SelectListItem> createTaxSelect(string selected, int param)
        {
            List<Tax> itemList = Lookup.Get<List<Tax>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _TaxRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }
            if (param == 0)
                itemList = itemList.Where(items => items.TaxCategory.Trim() == "PPN").ToList();
            else
                itemList = itemList.Where(items => items.TaxCategory.Trim() == "PPH").ToList();

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Tax, SelectListItem>(ConvertFromPPn));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString().Trim())).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFromPPn(Tax item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.TaxDescription.ToString().Trim() + " " + item.Rate.ToString().Trim() + " %";
            returnItem.Value = item.TaxID.ToString().Trim();
            return returnItem;
        }

        //===========================================================================================================================
        private IList<SelectListItem> MateraiSelect(string selected)
        {
            List<Materai> itemList = Lookup.Get<List<Materai>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _materaiRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Materai, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _expenceTypeID = itemList.FirstOrDefault().MateraiID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.Trim())).Selected = true;
                _expenceTypeID = selected;
            }

            return dataList;
        }

        public SelectListItem ConvertForm(Materai item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.MateraiName.Trim();
            returnItem.Value = item.MateraiID.ToString().Trim();
            return returnItem;
        }

        //=========================================================================================================================

        private IList<SelectListItem> createGroupAssetSelect(string selected)
        {
            List<GroupAsset> itemList = Lookup.Get<List<GroupAsset>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _groupAssetRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(GroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GroupAssetDescription;
            returnItem.Value = item.GroupAssetID.Trim();
            return returnItem;
        }
    }
}