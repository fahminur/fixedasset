﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Web.Resources;
using Treemas.FixedAsset.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Foundation.Model;
using Treemas.IFIN.Repository;
using System.Transactions;
using System.Globalization;

namespace Treemas.FixedAsset.Controllers
{
    public class InvoiceController : PageController
    {
        private IInvoiceRepository _InvoiceRepository;
        private IGroupAssetRepository _groupAssetRepository;
        private ISubGroupAssetRepository _subGroupAssetRepository;
        private INamaHartaRepository _namaHartaRepository;
        private IAssetCoaRepository _assetCoaRepository;
        private IAssetBranchRepository _branchRepository;
        private IAssetLocationRepository _assetLocationRepository;
        private IAssetAreaRepository _assetAreaRepository;
        private IAssetDeptRepository _assetDeptRepository;
        private IAssetUserRepository _assetUserRepository;
        private IAssetConditionRepository _assetConditionRepository;
        private ICurrencyRepository _currencyRepository;
        private IGolonganPajakRepository _golonganPajakRepository;
        private IVendorAccountRepository _VendorAccountRepository;
        private IVendorRepository _VendorRepository;
        private ITaxRepository _TaxRepository;
        private IInvoiceService _InvoiceService;
        private IInvoiceTypeRepository _invoiceTypeRepository;
        private IGeneralSettingRepository _generalSettingRepository;
        private IGLPeriodRepository _glPeriodRepository;
        private IExpenceTypeRepository _expenceTypeRepository;
        private IFixedAssetMasterRepository _fixedAssetMasterRepository;
        private IFixedAssetMasterSequence _fixedAssetMasterSequence;
        private IPenerimaanBarangRepository _penerimaanBarangRepository;
        private IMateraiRepository _materaiRepository;

        private string _invoiceTypeID;
        private string _VendorAccID;
        private string _expenceTypeID;
        //private string _materaiID;
        private INonAssetMasterRepository _nonAssetMasterRepository;

        private IAuditTrailLog _auditRepository;
        public InvoiceController(ISessionAuthentication sessionAuthentication, IInvoiceRepository InvoiceRepository, IGroupAssetRepository groupAssetRepository,
            ISubGroupAssetRepository subGroupAssetRepository, INamaHartaRepository namaHartaRepository, IAssetCoaRepository assetCoaRepository, IAssetBranchRepository branchRepository,
            IAssetLocationRepository assetLocationRepository, IAssetAreaRepository assetAreaRepository, IAssetDeptRepository assetDeptRepository, IAssetUserRepository assetUserRepository,
            ITaxRepository TaxRepository, IVendorAccountRepository VendorAccountRepository, IAssetConditionRepository assetConditionRepository, ICurrencyRepository currencyRepository,
            IInvoiceService InvoiceService, IGolonganPajakRepository golonganPajakRepository, IInvoiceTypeRepository invoiceTypeRepository, IGeneralSettingRepository generalSettingRepository,
            IGLPeriodRepository glPeriodRepository, IVendorRepository VendorRepository, IExpenceTypeRepository expenceTypeRepository, IAuditTrailLog auditRepository, IMateraiRepository materaiRepository,
            IFixedAssetMasterRepository fixedAssetMasterRepository, IFixedAssetMasterSequence fixedAssetMasterSequence, IPenerimaanBarangRepository penerimaanBarangRepository,
            INonAssetMasterRepository nonAssetMasterRepository
            ) : base(sessionAuthentication)
        {
            this._InvoiceRepository = InvoiceRepository;
            this._groupAssetRepository = groupAssetRepository;
            this._subGroupAssetRepository = subGroupAssetRepository;
            this._namaHartaRepository = namaHartaRepository;
            this._assetCoaRepository = assetCoaRepository;
            this._branchRepository = branchRepository;
            this._assetLocationRepository = assetLocationRepository;
            this._assetAreaRepository = assetAreaRepository;
            this._assetDeptRepository = assetDeptRepository;
            this._assetUserRepository = assetUserRepository;
            this._assetConditionRepository = assetConditionRepository;
            this._currencyRepository = currencyRepository;
            this._golonganPajakRepository = golonganPajakRepository;
            this._VendorAccountRepository = VendorAccountRepository;
            this._TaxRepository = TaxRepository;
            this._auditRepository = auditRepository;
            this._InvoiceService = InvoiceService;
            this._invoiceTypeRepository = invoiceTypeRepository;
            this._generalSettingRepository = generalSettingRepository;
            this._glPeriodRepository = glPeriodRepository;
            this._VendorRepository = VendorRepository;
            this._expenceTypeRepository = expenceTypeRepository;
            this._fixedAssetMasterRepository = fixedAssetMasterRepository;
            this._fixedAssetMasterSequence = fixedAssetMasterSequence;
            this._penerimaanBarangRepository = penerimaanBarangRepository;
            this._materaiRepository = materaiRepository;
            this._nonAssetMasterRepository = nonAssetMasterRepository;

            Settings.ModuleName = "Invoice";
            Settings.Title = "Invoice";
        }

        protected override void Startup()
        {
            ViewData["InvStatus"] = createInvoiceStatusSelect("N");
        }

        public ActionResult Search()
        {
            string Mode = Request.QueryString.GetValues("Mode")[0];
            string SearchType = Mode == "" ? "Invoice" : Mode;

            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            JsonResult result = new JsonResult();
            try
            {
                if (SearchType == "Invoice")
                {
                    InvoiceFilter filter = new InvoiceFilter();
                    string vendorid = Request.QueryString.GetValues("VendorID")[0];
                    string VendorInvoiceNo = Request.QueryString.GetValues("VendorInvoiceNo")[0];
                    string InvoiceStatus = Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];
                    filter.InvoiceDateFrom = !Request.QueryString["InvoiceDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("InvoiceDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.InvoiceDateTo = !Request.QueryString["InvoiceDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("InvoiceDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.VendorId = vendorid;
                    filter.VendorInvoiceNo = VendorInvoiceNo;
                    filter.InvoiceStatus = InvoiceStatus;

                    string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                    var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                    int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                    int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                    int pageNumber = (startRec + pageSize - 1) / pageSize;

                    // Loading.   
                    PagedResult<InvoiceH> data;
                    data = _InvoiceRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                    var items = data.Items.ToList().ConvertAll<InvoiceHView>(new Converter<InvoiceH, InvoiceHView>(ConvertFrom));

                    result = this.Json(new
                    {
                        draw = Convert.ToInt32(draw),
                        recordsTotal = data.TotalItems,
                        recordsFiltered = data.TotalItems,
                        data = items
                    }, JsonRequestBehavior.AllowGet);
                }
                else if (SearchType == "Generate")
                {
                    PenerimaanBarangFilter filter = new PenerimaanBarangFilter();
                    filter.GRDateFrom = !Request.QueryString["GRDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("GRDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.GRDateTo = !Request.QueryString["GRDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("GRDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.DeliveryOrderDateFrom = !Request.QueryString["DeliveryOrderDateFrom"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("DeliveryOrderDateFrom")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.DeliveryOrderDateTo = !Request.QueryString["DeliveryOrderDateTo"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("DeliveryOrderDateTo")[0], "d/M/yyyy", culture) : (DateTime?)null;
                    filter.VendorID = Request.QueryString.GetValues("VendorID")[0];
                    filter.DeliveryOrderNo = Request.QueryString.GetValues("DeliveryOrderNo")[0];
                    filter.Status = "A";//Request.QueryString["Status"].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Status")[0];

                    string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                    var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                    int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                    int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                    int pageNumber = (startRec + pageSize - 1) / pageSize;

                    // Loading.   
                    PagedResult<PenerimaanBarangHdr> data;
                    data = _penerimaanBarangRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                    var items = data.Items.ToList().ConvertAll<PenerimaanBarangHdrView>(new Converter<PenerimaanBarangHdr, PenerimaanBarangHdrView>(ConvertFrom));

                    result = this.Json(new
                    {
                        draw = Convert.ToInt32(draw),
                        recordsTotal = data.TotalItems,
                        recordsFiltered = data.TotalItems,
                        data = items
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }


        public InvoiceHView ConvertFrom(InvoiceH item)
        {
            InvoiceHView returnItem = new InvoiceHView();
            returnItem.InvoiceNo = item.InvoiceNo;
            returnItem.VendorInvoiceNo = item.VendorInvoiceNo;
            returnItem.VendorID = item.VendorID;
            returnItem.GRNo = item.GRNo;
            returnItem.DeliveryOrderNo = item.DeliveryOrderNo;
            returnItem.PONo = item.PONo;

            Vendor Vendor = _VendorRepository.getVendor(item.VendorID.Trim());
            returnItem.VendorName = Vendor == null ? "" : Vendor.VendorName.Trim();
            returnItem.VendorBankId = item.VendorBankId;
            returnItem.InvoiceDate = item.InvoiceDate.ToString("dd/MM/yyyy");
            returnItem.InvoiceDueDate = item.InvoiceDueDate.ToString("dd/MM/yyyy");
            returnItem.InvoiceOrderDate = item.InvoiceOrderDate.ToString("dd/MM/yyyy");
            returnItem.Status = item.Status;
            returnItem.StatusString = item.InvStatusEnum.ToDescription();
            returnItem.Notes = item.Notes;
            returnItem.InvoiceTypeID = item.InvoiceTypeID.Trim();
            returnItem.NoFakturPajak = item.NoFakturPajak;
            returnItem.TglFakturPajak = item.TglFakturPajak.ToString("dd/MM/yyyy");
            returnItem.InvoiceAmount = item.InvoiceAmount.ToString("N0");
            returnItem.AllowEdit = item.AllowEdit;
            return returnItem;
        }

        private PenerimaanBarangHdrView ConvertFrom(PenerimaanBarangHdr input)
        {
            PenerimaanBarangHdrView output = new PenerimaanBarangHdrView();
            Vendor Vendor = _VendorRepository.getVendor(input.VendorID.Trim());
            InvoiceType jenispenerimaan = _invoiceTypeRepository.GetInvoiceType(input.GRType);

            output.GRNo = input.GRNo.Trim();
            output.VendorID = input.VendorID.Trim();
            output.VendorName = Vendor == null ? "" : Vendor.VendorName.Trim();
            output.DeliveryOrderNo = input.DeliveryOrderNo.Trim();
            output.Notes = input.Notes.IsNull() ? "" : input.Notes.Trim();
            output.GRType = input.GRType.Trim();
            output.GRTypeDesc = jenispenerimaan.InvoiceTypeName.Trim();
            output.Status = input.Status.Trim();
            output.GRAmount = input.GRAmount.ToString("N0");
            output.StatusString = input.penerimaanStatusEnum.ToDescription();
            output.DeliveryOrderDate = input.DeliveryOrderDate.ToString("dd/MM/yyyy");
            output.GRDate = input.GRDate.ToString("dd/MM/yyyy");
            output.AllowEdit = input.AllowEdit;
            return output;
        }

        // GET: Function/Create
        public ActionResult Create(string GRNo)
        {
            ViewData["ActionName"] = "Create";
            InvoiceHView data = new InvoiceHView();

            if (GRNo.IsNullOrEmpty())
            {
                data.Mode = "Create";
                data.InvoiceAktivaList = CreateNewAktivaDetail();
                data.InvoiceNonAktivaList = CreateNewNonAktivaDetail();
                data.InvoiceServiceList = CreateNewserviceDetail();
                data.InvoiceDate = DateTime.Now.ToString("dd/MM/yyyy"); ;
                data.InvoiceDueDate = DateTime.Now.ToString("dd/MM/yyyy"); ;
                data.InvoiceOrderDate = DateTime.Now.ToString("dd/MM/yyyy"); ;
                data.TglFakturPajak = DateTime.Now.ToString("dd/MM/yyyy"); ;

                return CreateView(data);
            }
            else
            {
                PenerimaanBarangHdr pbHdr = _penerimaanBarangRepository.getPenerimaanBarang(GRNo);

                if (!pbHdr.IsNull())
                {
                    Vendor vendor = _VendorRepository.getVendor(pbHdr.VendorID);

                    data.GRNo = GRNo.Trim();
                    data.VendorID = pbHdr.VendorID;
                    data.DeliveryOrderNo = pbHdr.DeliveryOrderNo.Trim();
                    data.VendorName = vendor.IsNull() ? "" : vendor.VendorName.Trim();
                    data.InvoiceTypeID = pbHdr.GRType.Trim();
                    data.InvoiceAmount = pbHdr.GRAmount.ToString("N0");
                    data.Mode = "Generate";

                    data.InvoiceDate = DateTime.Now.ToString("dd/MM/yyyy");
                    data.InvoiceDueDate = DateTime.Now.ToString("dd/MM/yyyy");
                    data.InvoiceOrderDate = DateTime.Now.ToString("dd/MM/yyyy");
                    data.TglFakturPajak = DateTime.Now.ToString("dd/MM/yyyy");

                    IList<PenerimaanBarangAktiva> GRA = _penerimaanBarangRepository.getPenerimaanBarangAktiva(data.GRNo);
                    IList<PenerimaanBarangNonAktiva> GRNA = _penerimaanBarangRepository.getPenerimaanBarangNonAktiva(data.GRNo);
                    //IList<PenerimaanBarangService> GRS = _penerimaanBarangRepository.getPenerimaanBarangService(data.GRNo);

                    IList<InvoiceAktivaView> InvAView = ConvertFromA(GRA, pbHdr);
                    data.InvoiceAktivaList = InvAView.IsNullOrEmpty() ? CreateNewAktivaDetail() : InvAView;
                    IList<InvoiceNonAktivaView> InvNAView = ConvertFromNA(GRNA);
                    data.InvoiceNonAktivaList = InvNAView.IsNullOrEmpty() ? CreateNewNonAktivaDetail() : InvNAView;
                    //IList<InvoiceServiceView> InvSView = GRS.ToList().ConvertAll<InvoiceServiceView>(new Converter<PenerimaanBarangService, InvoiceServiceView>(ConvertFrom));
                    data.InvoiceServiceList = CreateNewserviceDetail();// InvSView.IsNullOrEmpty() ? CreateNewserviceDetail() : InvSView;
                }

                return CreateView(data);
            }
        }
        private IList<InvoiceAktivaView> CreateNewAktivaDetail()
        {
            IList<InvoiceAktivaView> returnDetail = new List<InvoiceAktivaView>();
            returnDetail.Add(AddNewAktivaRecord(returnDetail));
            return returnDetail;
        }
        private IList<InvoiceNonAktivaView> CreateNewNonAktivaDetail()
        {
            IList<InvoiceNonAktivaView> returnDetail = new List<InvoiceNonAktivaView>();
            returnDetail.Add(AddNewNonAktivaRecord(returnDetail));
            return returnDetail;
        }

        private IList<InvoiceServiceView> CreateNewserviceDetail()
        {
            IList<InvoiceServiceView> returnDetail = new List<InvoiceServiceView>();
            returnDetail.Add(AddNewServiceRecord(returnDetail));
            return returnDetail;
        }
        private InvoiceAktivaView AddNewAktivaRecord(IList<InvoiceAktivaView> FppdDetailList)
        {
            InvoiceAktivaView detail = new InvoiceAktivaView();
            detail.ID = FppdDetailList.IsNull() ? 1 : (FppdDetailList.Count > 0) ? FppdDetailList.LastOrDefault().ID + 1 : 1;
            detail.TanggalPerolehan = DateTime.Now.ToString("dd/MM/yyyy");
            detail.Quantity = 1;
            return detail;
        }
        private InvoiceNonAktivaView AddNewNonAktivaRecord(IList<InvoiceNonAktivaView> FppdDetailList)
        {
            InvoiceNonAktivaView detail = new InvoiceNonAktivaView();
            detail.ID = FppdDetailList.IsNull() ? 1 : (FppdDetailList.Count > 0) ? FppdDetailList.LastOrDefault().ID + 1 : 1;
            detail.StartDate = DateTime.Now.ToString("dd/MM/yyyy");
            detail.EndDate = DateTime.Now.ToString("dd/MM/yyyy");
            //detail.EndDateChanged = 0;

            return detail;
        }
        private InvoiceServiceView AddNewServiceRecord(IList<InvoiceServiceView> FppdDetailList)
        {
            InvoiceServiceView detail = new InvoiceServiceView();
            detail.ID = FppdDetailList.IsNull() ? 1 : (FppdDetailList.Count > 0) ? FppdDetailList.LastOrDefault().ID + 1 : 1;
            detail.ServiceDate = DateTime.Now.ToString("dd/MM/yyyy");
            detail.ServiceGuaranteeDate = DateTime.Now.ToString("dd/MM/yyyy");
            return detail;
        }
        private ViewResult CreateView(InvoiceHView data)
        {
            if (data.VendorID == null)
            {
                ViewData["SupAacc"] = createEmptySupAacc();
            }
            else
            {
                ViewData["SupAacc"] = VendorAccountSelect(data.VendorID.Trim(), data.VendorBankId);

                if (data.VendorBankId.IsNullOrEmpty() || data.VendorBankId == " ")
                {
                    if (!ViewData["SupAacc"].IsNull())
                    {
                        IList<SelectListItem> SupAaccList = (IList<SelectListItem>)ViewData["SupAacc"];
                        if (SupAaccList.Count > 1)
                        {
                            string AccID = SupAaccList.FindElement(s => s.Selected == true).Value.ToString();

                            data.VendorBankId = AccID.IsNullOrEmpty() ? "" : AccID;

                            VendorAccount VendorAccount = _VendorAccountRepository.getVendorAccount(AccID);
                            data.VendorAccountName = VendorAccount.IsNull() ? "" : VendorAccount.VendorAccountName;
                            data.VendorAccountNo = VendorAccount.IsNull() ? "" : VendorAccount.VendorAccountNo;
                            data.VendorBankBranch = VendorAccount.IsNull() ? "" : VendorAccount.VendorBankBranch;
                        }
                    }
                }
            }
            ViewData["InvoiceType"] = InvoiceTypeSelect(data.InvoiceTypeID == null ? "" : data.InvoiceTypeID);

            generateViewDataGroupAssetSelect(data.InvoiceNonAktivaList);
            generateViewDataExpenceTypeSelect(data.InvoiceNonAktivaList);
            generateViewDataPpnAktivaSelect(data.InvoiceAktivaList, 0);
            generateViewDataPphAktivaSelect(data.InvoiceAktivaList, 1);
            generateViewDataPpnNonAktivaSelect(data.InvoiceNonAktivaList, 0);
            generateViewDataPphNonAktivaSelect(data.InvoiceNonAktivaList, 1);
            generateViewDataPpnServiceSelect(data.InvoiceServiceList, 0);
            generateViewDataPphServiceSelect(data.InvoiceServiceList, 1);
            generateViewDataServiceMateraiSelect(data.InvoiceServiceList);

            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult Create(InvoiceHView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();
            string message = "";
            bool isActiva = false;
            bool isNonActiva = false;
            bool isService = false;
            int year = int.Parse(DateTime.Now.ToString("yyyy"));
            int month = int.Parse(DateTime.Now.ToString("MM"));

            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");


            switch (data.InvoiceTypeID)
            {
                case "AC":
                    isActiva = true;
                    break;
                case "ANA":
                    isNonActiva = true;
                    isActiva = true;
                    break;
                case "APA":
                    isActiva = true;
                    isService = true;
                    break;
                case "NA":
                    isNonActiva = true;
                    break;
                case "NPA":
                    isNonActiva = true;
                    isService = true;
                    break;
                case "PA":
                    isService = true;
                    break;
                case "ST":
                    isActiva = true;
                    isNonActiva = true;
                    isService = true;
                    break;
                default:
                    break;
            }


            try
            {
                message = validateData(data, "Create");

                if (message.IsNullOrEmpty())
                {
                    InvoiceH newData = new InvoiceH("");
                    List<FixedAssetMaster> famDatas = new List<FixedAssetMaster>();

                    newData.InvoiceNo = "";
                    newData.VendorInvoiceNo = data.VendorInvoiceNo;
                    newData.VendorID = data.VendorID;
                    newData.InvoiceDate = DateTime.ParseExact(data.InvoiceDate, "d/M/yyyy", culture);
                    newData.InvoiceDueDate = DateTime.ParseExact(data.InvoiceOrderDate, "d/M/yyyy", culture);
                    newData.InvoiceOrderDate = DateTime.ParseExact(data.InvoiceOrderDate, "d/M/yyyy", culture);
                    newData.DeliveryOrderNo = data.DeliveryOrderNo;
                    newData.GRNo = data.GRNo;
                    newData.PONo = data.PONo;
                    newData.Notes = data.Notes;
                    newData.VendorBankId = data.VendorBankId;
                    newData.InvoiceAmount = 0;// Convert.ToDouble(data.InvoiceAmount);
                    newData.InvoiceTypeID = data.InvoiceTypeID;
                    newData.NoFakturPajak = data.NoFakturPajak;
                    newData.TglFakturPajak = DateTime.ParseExact(data.TglFakturPajak, "d/M/yyyy", culture);
                    newData.Status = InvoiceStatusEnum.N.ToString();
                    //aktiva
                    if (isActiva)
                    {
                        newData.InvoiceAktivaList = new List<InvoiceAktiva>();
                        int a = 1;
                        foreach (var item in data.InvoiceAktivaList)
                        {
                            Tax taxpph = _TaxRepository.getTax(item.PPhId);
                            Tax taxppn = _TaxRepository.getTax(item.PPnId);
                            InvoiceAktiva newItem = new InvoiceAktiva(a);
                            newItem.ID = a;
                            newItem.InvoiceNo = data.InvoiceNo;
                            newItem.Seq = a;
                            //newItem.AssetSubGroupId = item.AssetSubGroupId;
                            newItem.StartingAktivaId = item.StartingAktivaId;
                            newItem.PurchaseOrderNo = item.PurchaseOrderNo;
                            //newItem.DeliveryOrderNo = item.DeliveryOrderNo;
                            //newItem.GRNo = item.GRNo;
                            newItem.TanggalPerolehan = DateTime.ParseExact(item.TanggalPerolehan, "d/M/yyyy", culture);
                            newItem.Quantity = 1;
                            newItem.Price = Convert.ToDouble(item.Price);
                            newItem.ExpedCost = Convert.ToDouble(item.ExpedCost);
                            newItem.OtherCost = Convert.ToDouble(item.OtherCost);
                            newItem.TotalPrice = Convert.ToDouble(item.TotalPrice);
                            //newItem.PPnId = item.PPnId;
                            //newItem.PPhId = item.PPhId;
                            newItem.Notes = item.Notes;
                            newItem.TotalPrice = Convert.ToDouble(item.Price) * 1;
                            newItem.PPnAmount = Convert.ToDouble(item.PPnAmount);//(newItem.TotalPrice * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate))) / 100;
                            newItem.PPhAmount = Convert.ToDouble(item.PPhAmount);//(newItem.TotalPrice * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate))) / 100;
                            newItem.TotalAmount = (newItem.TotalPrice + newItem.OtherCost + newItem.PPnAmount + newItem.ExpedCost) - newItem.PPhAmount;
                            newData.InvoiceAmount += newItem.TotalAmount;
                            newData.InvoiceAktivaList.Add(newItem);

                            FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(newItem.StartingAktivaId);
                            //tab perolehan
                            famData.VendorID = newData.VendorID;
                            famData.NoPO = newItem.PurchaseOrderNo;
                            famData.NoDO = newItem.DeliveryOrderNo;
                            famData.NoGSRN = newItem.GRNo;
                            famData.NoInvoice = newData.VendorInvoiceNo;
                            famData.TanggalPerolehan = DateTime.ParseExact(item.TanggalPerolehan, "d/M/yyyy", culture);
                            famData.NilaiOriginal = newItem.Price;
                            famData.HargaPerolehan = newItem.TotalAmount;
                            famData.KeteranganPerolehan = newItem.Notes;
                            famData.SisaNilaiBukuKomersial = newItem.TotalAmount;
                            famDatas.Add(famData);

                            a++;
                        }
                    }
                    if (isNonActiva)
                    {
                        //nonAktiva
                        newData.InvoiceNonAktivaList = new List<InvoiceNonAktiva>();
                        int n = 1;
                        foreach (var item in data.InvoiceNonAktivaList)
                        {
                            Tax taxpph = _TaxRepository.getTax(item.PPhId);
                            Tax taxppn = _TaxRepository.getTax(item.PPnId);
                            InvoiceNonAktiva newItem = new InvoiceNonAktiva(n);

                            newItem.ID = n;
                            newItem.InvoiceNo = data.InvoiceNo;
                            newItem.Seq = n;
                            newItem.NonAktivaId = item.NonAktivaId;
                            newItem.ItemName = item.ItemName;
                            newItem.Price = Convert.ToDouble(item.Price);
                            newItem.ExpedCost = Convert.ToDouble(item.ExpedCost);
                            newItem.OtherCost = Convert.ToDouble(item.OtherCost);
                            newItem.ExpenseType = item.ExpenseType;
                            newItem.TimePeriod = item.TimePeriod;
                            newItem.StartDate = DateTime.ParseExact(item.StartDate, "d/M/yyyy", culture);
                            newItem.EndDate = DateTime.ParseExact(item.EndDate, "d/M/yyyy", culture);
                            newItem.MonthlyExpense = Convert.ToDouble(item.MonthlyExpense);
                            newItem.ExpensePA = item.ExpensePA;
                            newItem.PrepaidPA = item.PrepaidPA;
                            newItem.Notes = item.Notes;
                            newItem.PPnAmount = Convert.ToDouble(item.PPnAmount);//(newItem.Price * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate))) / 100;
                            newItem.PPhAmount = Convert.ToDouble(item.PPhAmount);//(newItem.Price * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate))) / 100;
                            newItem.TotalAmount = (Convert.ToDouble(item.Price) + newItem.PPnAmount + newItem.OtherCost + newItem.ExpedCost) - newItem.PPhAmount;
                            newItem.DepreciatedTime = item.DepreciatedTime;
                            newData.InvoiceAmount += newItem.TotalAmount;
                            newData.InvoiceNonAktivaList.Add(newItem);
                            n++;
                        }
                    }
                    if (isService)
                    {
                        //Service
                        newData.InvoiceServiceList = new List<InvoiceService>();

                        int s = 1;
                        foreach (var item in data.InvoiceServiceList)
                        {
                            Tax taxpph = _TaxRepository.getTax(item.PPhId);
                            Tax taxppn = _TaxRepository.getTax(item.PPnId);
                            InvoiceService newItem = new InvoiceService(s);
                            Materai materai = _materaiRepository.GetMaterai(item.MateraiID);

                            newItem.ID = s;
                            newItem.InvoiceNo = data.InvoiceNo;
                            newItem.Seq = s;
                            newItem.AktivaId = item.AktivaId;
                            newItem.ExpensePA = item.ExpensePA;
                            newItem.OtherCost = Convert.ToDouble(item.OtherCost);
                            //newItem.PONo = item.PONo;
                            newItem.WorkOrderNo = item.WorkOrderNo;
                            newItem.ServiceDate = DateTime.ParseExact(item.ServiceDate, "d/M/yyyy", culture);
                            newItem.ServiceGuaranteeDate = DateTime.ParseExact(item.ServiceGuaranteeDate, "d/M/yyyy", culture);
                            newItem.ServiceAmount = Convert.ToDouble(item.ServiceAmount);
                            newItem.ItemAmount = Convert.ToDouble(item.ItemAmount);
                            //newItem.PPnId = item.PPnId;
                            //newItem.PPhId = item.PPhId;
                            newItem.Notes = item.Notes;
                            newItem.MateraiID = item.MateraiID.IsNull() ? "" : item.MateraiID.Trim();
                            newItem.MateraiAmount = materai.IsNull() ? 0 : materai.MateraiValue;
                            newItem.PPnAmount = Convert.ToDouble(item.PPnAmount);//(newItem.ItemAmount * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate))) / 100;
                            newItem.PPhAmount = Convert.ToDouble(item.PPhAmount);//(newItem.ServiceAmount * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate))) / 100;
                            newItem.TotalAmount = (newItem.ServiceAmount + newItem.PPnAmount + newItem.ItemAmount + newItem.MateraiAmount + newItem.OtherCost) - newItem.PPhAmount;
                            newData.InvoiceAmount += newItem.TotalAmount;
                            newData.InvoiceServiceList.Add(newItem);
                            s++;
                        }
                    }
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;

                    message = _InvoiceService.SaveInvoice(newData, user, company);
                    using (TransactionScope scope = new TransactionScope())
                    {
                        foreach (FixedAssetMaster famupd in famDatas)
                        {
                            _fixedAssetMasterRepository.Save(famupd);
                        }
                        scope.Complete();
                    }

                    PenerimaanBarangHdr pbHdr = _penerimaanBarangRepository.getPenerimaanBarang(newData.GRNo);
                    if (!pbHdr.IsNull())
                    {
                        pbHdr.Status = PenerimaanStatusEnum.I.ToString();
                        pbHdr.UpdateUser = user.Username;
                        pbHdr.UpdateDate = DateTime.Now;
                        _penerimaanBarangRepository.Save(pbHdr);
                    }

                    if (message == "")
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");


                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Save_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(InvoiceHView data, string mode)
        {
            bool isActiva = false;
            bool isNonActiva = false;
            bool isService = false;

            switch (data.InvoiceTypeID)
            {
                case "AC":
                    isActiva = true;
                    break;
                case "ANA":
                    isNonActiva = true;
                    isActiva = true;
                    break;
                case "APA":
                    isActiva = true;
                    isService = true;
                    break;
                case "NA":
                    isNonActiva = true;
                    break;
                case "NPA":
                    isNonActiva = true;
                    isService = true;
                    break;
                case "PA":
                    isService = true;
                    break;
                case "ST":
                    isActiva = true;
                    isNonActiva = true;
                    isService = true;
                    break;
                default:
                    break;
            }

            if (data.GRNo == "" || data.GRNo.IsNull())
                return "Nomor Penerimaan Barang di isi!";

            if (data.VendorName == "" || data.VendorName.IsNull())
                return "Vendor harus di pilih!";

            if (data.VendorInvoiceNo == "" || data.VendorInvoiceNo.IsNull())
                return "Nomor invoce vendor harus di isi!";

            if (data.PONo == "" || data.PONo.IsNull())
                return "Nomor PO Aktiva harus di isi!";

            if (data.DeliveryOrderNo == "" || data.DeliveryOrderNo.IsNull())
                return "Nomor Surat Jalan di isi!";

            if (data.InvoiceDate.IsNull())
                return "Tanggal invoce tidak valid!";

            if (data.InvoiceDueDate.IsNull())
                return "Tanggal jatuh tempo tidak valid!";

            if (data.InvoiceOrderDate.IsNull())
                return "Tanggal rencana bayar tidak valid!";

            if (data.InvoiceTypeID == " " || data.InvoiceTypeID.IsNull())
                return "Jenis invoice harus dipilih!";

            if (data.VendorBankId == " " || data.VendorBankId.IsNull())
                return "Nama Bank harus dipilih!";

            if (!data.NoFakturPajak.IsNull() || data.NoFakturPajak != "")
            {
                if (data.TglFakturPajak.IsNull())
                    return "Tanggal Faktur Pajak harus di isi!";
            }


            if (isActiva)
            {
                foreach (InvoiceAktivaView item in data.InvoiceAktivaList)
                {
                    if ((item.PPhId != " " || !item.PPhId.IsNull()) || (item.PPnId != " " || !item.PPnId.IsNull()))
                    {
                        if (data.NoFakturPajak.IsNull() || data.NoFakturPajak == "")
                        {
                            return "Nomor Faktur Pajak harus di isi!";
                        }
                    }

                    if (item.StartingAktivaId == "" || item.StartingAktivaId.IsNull())
                        return "Kode aktiva harus di isi!";

                    //if (item.PurchaseOrderNo == "" || item.PurchaseOrderNo.IsNull())
                    //    return "Nomor PO Aktiva harus di isi!";

                    //if (item.DeliveryOrderNo == "" || item.DeliveryOrderNo.IsNull())
                    //    return "Nomor DO Aktiva harus di isi!";

                    //if (item.GRNo == "" || item.GRNo.IsNull())
                    //    return "Nomor LPB Aktiva harus di isi!";

                    if (item.TanggalPerolehan.IsNull())
                        return "Tanggal perolehan Aktiva tidak valid!";

                    if (item.Price.IsNull() || Convert.ToDouble(item.Price) == 0)
                        return "Harga Aktiva harus di isi!";

                }
            }

            if (isNonActiva)
            {
                foreach (InvoiceNonAktivaView item in data.InvoiceNonAktivaList)
                {
                    if ((item.PPhId != " " || !item.PPhId.IsNull()) || (item.PPnId != " " || !item.PPnId.IsNull()))
                    {
                        if (data.NoFakturPajak.IsNull() || data.NoFakturPajak == "")
                        {
                            return "Nomor Faktur Pajak harus di isi!";
                        }
                    }

                    //if (item.RefferenceNo == "" || item.RefferenceNo.IsNull())
                    //    return "No Referensi non aktiva harus di isi!";
                    //if (item.ItemName == "" || item.ItemName.IsNull())
                    //    return "Nama Barang/Jasa non aktiva harus di isi!";
                    if (item.Price.IsNull() || Convert.ToDouble(item.Price) == 0)
                        return "Nilai non aktiva harus di isi!";
                    //if (item.Qty.IsNull() || item.Qty == 0)
                    //return "Jumlah non aktiva harus di isi!";
                    if (item.ExpenseType == "" || item.ExpenseType.IsNull())
                        return "Cara Pembebanan non aktiva harus di isi!";
                    if (item.ExpenseType == "A")
                    {
                        if (item.StartDate.IsNull())
                            return "Mulai Tanggal harus di pilih!";
                        if (item.EndDate.IsNull())
                            return "Sampai Tanggal harus di pilih!";
                        if (item.TimePeriod.IsNull() || item.TimePeriod == 0 || item.TimePeriod == 1)
                            return "Jangka Waktu tidak valid!";
                        if (item.PrepaidPA == "" || item.PrepaidPA.IsNull())
                            return "Akun Prepaid non aktiva harus di isi!";
                    }
                    else
                    {
                        if (item.ExpensePA == "" || item.ExpensePA.IsNull())
                            return "Akun Biaya non aktiva harus di isi!";
                    }
                    //if (item.PONo == "" || item.PONo.IsNull())
                    //    return "Nomor PO non aktiva harus di isi!";
                    //if (item.DeliveryOrderNo == "" || item.DeliveryOrderNo.IsNull())
                    //    return "Nomor Surat Jalan non aktiva harus di isi!";
                    //if (item.GRNo == "" || item.GRNo.IsNull())
                    //    return "Nomor LPB non aktiva harus di isi!";
                }
            }

            if (isService)
            {
                foreach (InvoiceServiceView item in data.InvoiceServiceList)
                {
                    if ((item.PPhId != " " || !item.PPhId.IsNull()) || (item.PPnId != " " || !item.PPnId.IsNull()))
                    {
                        if (data.NoFakturPajak.IsNull() || data.NoFakturPajak == "")
                        {
                            return "Nomor Faktur Pajak harus di isi!";
                        }
                    }

                    if (item.AktivaId == "" || item.AktivaId.IsNull())
                        return "Kode Aktiva perawatan aktiva harus di isi!";
                    //if (item.PONo == "" || item.PONo.IsNull())
                    //    return "Nomor PO perawatan aktiva harus di isi!";
                    if (item.WorkOrderNo == "" || item.WorkOrderNo.IsNull())
                        return "No SPK perawtan aktiva harus di isi!";
                    if (item.ExpensePA == "" || item.ItemAmount.IsNull())
                        return "Akun Biaya harus di pilih!";
                    if (item.ServiceDate.IsNull())
                        return "Tanggal perawatan aktiva harus di isi!";
                    if (item.ServiceGuaranteeDate.IsNull())
                        return "Tanggal akhir garansi perawatan aktiva harus di isi!";
                    if (item.ServiceAmount == "0" || item.ServiceAmount.IsNull())
                        return "Nilai Jasa aktiva harus di isi!";
                    if (item.ItemAmount == "0" || item.ItemAmount.IsNull())
                        return "Nilai Barang aktiva harus di isi!";

                }
            }

            if (mode == "Create")
            {
                if (_InvoiceRepository.IsDuplicate(data.InvoiceNo))
                    return "Invoice No Is Already Exist";
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            InvoiceH InvoiceItem = _InvoiceRepository.getInvoice(id);
            InvoiceHView data = ConvertFrom(InvoiceItem);
            VendorAccount VendorAccount = _VendorAccountRepository.getVendorAccount(data.VendorBankId.Trim());
            data.Mode = "Edit";

            if (VendorAccount != null)
            {
                data.VendorAccountName = VendorAccount.VendorAccountName;
                data.VendorAccountNo = VendorAccount.VendorAccountNo;
                data.VendorBankBranch = VendorAccount.VendorBankBranch;
            }

            IList<InvoiceAktiva> InvA = _InvoiceRepository.getInvoiceAktiva(data.InvoiceNo);
            IList<InvoiceAktivaView> InvAView = InvA.ToList().ConvertAll<InvoiceAktivaView>(new Converter<InvoiceAktiva, InvoiceAktivaView>(ConvertFrom));
            data.InvoiceAktivaList = InvAView.IsNullOrEmpty() ? CreateNewAktivaDetail() : InvAView;
            IList<InvoiceNonAktiva> InvNA = _InvoiceRepository.getInvoiceNonAktiva(data.InvoiceNo);
            IList<InvoiceNonAktivaView> InvNAView = InvNA.ToList().ConvertAll<InvoiceNonAktivaView>(new Converter<InvoiceNonAktiva, InvoiceNonAktivaView>(ConvertFrom));
            data.InvoiceNonAktivaList = InvNAView.IsNullOrEmpty() ? CreateNewNonAktivaDetail() : InvNAView;
            IList<InvoiceService> InvS = _InvoiceRepository.getInvoiceService(data.InvoiceNo);
            IList<InvoiceServiceView> InvSView = InvS.ToList().ConvertAll<InvoiceServiceView>(new Converter<InvoiceService, InvoiceServiceView>(ConvertFrom));
            data.InvoiceServiceList = InvSView.IsNullOrEmpty() ? CreateNewserviceDetail() : InvSView;
            return CreateView(data);

        }

        public InvoiceAktivaView ConvertFrom(InvoiceAktiva item)
        {
            InvoiceAktivaView returnItem = new InvoiceAktivaView();
            FixedAssetMaster FA = _fixedAssetMasterRepository.getFixedAssetMaster(item.StartingAktivaId);
            GroupAsset GA = _groupAssetRepository.getGroupAsset(FA.IsNull() ? "" : FA.GroupAssetID);

            returnItem.InvoiceNo = item.InvoiceNo.Trim();
            returnItem.Seq = item.Seq;
            returnItem.StartingAktivaId = item.StartingAktivaId.Trim();
            returnItem.AktivaName = FA.IsNull() ? "" : FA.NamaAktiva.Trim();
            returnItem.ID = item.ID;
            returnItem.TanggalPerolehan = item.TanggalPerolehan.ToString("dd/MM/yyyy");
            returnItem.Quantity = item.Quantity;
            returnItem.Price = item.Price.ToString("N0");
            returnItem.ExpedCost = item.ExpedCost.ToString("N0");
            returnItem.OtherCost = item.OtherCost.ToString("N0");
            returnItem.TotalPrice = item.TotalPrice.ToString("N0");
            returnItem.PPnAmount = item.PPnAmount.ToString("N0");
            returnItem.PPhAmount = item.PPhAmount.ToString("N0");
            returnItem.Notes = item.Notes;
            returnItem.TotalAmount = item.TotalAmount.ToString("N0");

            return returnItem;
        }
        public InvoiceNonAktivaView ConvertFrom(InvoiceNonAktiva item)
        {
            //GroupAsset GA = _groupAssetRepository.getGroupAsset(item.GroupAssetID);
            NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);

            InvoiceNonAktivaView returnItem = new InvoiceNonAktivaView();
            returnItem.ID = item.ID;
            returnItem.InvoiceNo = item.InvoiceNo;
            returnItem.Seq = item.Seq;
            returnItem.NonAktivaId = item.NonAktivaId;
            returnItem.NonAktivaName = nam.IsNull() ? "" : nam.NamaNonAktiva.Trim();
            returnItem.Price = item.Price.ToString("N0");
            returnItem.ExpedCost = item.ExpedCost.ToString("N0");
            returnItem.OtherCost = item.OtherCost.ToString("N0");
            returnItem.ExpenseType = item.ExpenseType;
            returnItem.TimePeriod = item.TimePeriod;
            returnItem.StartDate = item.StartDate.IsNull() ? null : Convert.ToDateTime(item.StartDate).ToString("dd/MM/yyyy");
            returnItem.EndDate = item.EndDate.IsNull() ? null : Convert.ToDateTime(item.EndDate).ToString("dd/MM/yyyy");
            returnItem.MonthlyExpense = item.MonthlyExpense.ToString("N0");
            returnItem.ExpensePA = item.ExpensePA;
            returnItem.PrepaidPA = item.PrepaidPA;
            returnItem.Notes = item.Notes;
            returnItem.DepreciatedTime = item.DepreciatedTime;
            returnItem.PPnAmount = item.PPnAmount.ToString("N0");
            returnItem.PPhAmount = item.PPhAmount.ToString("N0");
            returnItem.TotalAmount = item.TotalAmount.ToString("N0");
            return returnItem;
        }
        public InvoiceServiceView ConvertFrom(InvoiceService item)
        {
            InvoiceServiceView returnItem = new InvoiceServiceView();
            FixedAssetMaster FA = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);
            GroupAsset GA = _groupAssetRepository.getGroupAsset(FA.IsNull() ? "" : FA.GroupAssetID);
            Materai materai = _materaiRepository.GetMaterai(item.MateraiID);

            returnItem.ID = item.ID;
            returnItem.InvoiceNo = item.InvoiceNo;
            returnItem.Seq = item.Seq;
            returnItem.AktivaId = item.AktivaId;
            returnItem.COABeban = GA.IsNull() ? "" : GA.CoaBeban;
            returnItem.AktivaName = FA.IsNull() ? "" : FA.NamaAktiva.Trim();
            returnItem.OtherCost = item.OtherCost.ToString("N0");
            returnItem.WorkOrderNo = item.WorkOrderNo;
            returnItem.ExpensePA = item.ExpensePA;
            returnItem.ServiceDate = item.ServiceDate.ToString("dd/MM/yyyy");
            returnItem.ServiceGuaranteeDate = item.ServiceGuaranteeDate.ToString("dd/MM/yyyy");
            returnItem.ServiceAmount = item.ServiceAmount.ToString("N0");
            returnItem.ItemAmount = item.ItemAmount.ToString("N0");
            returnItem.Notes = item.Notes;
            returnItem.PPnAmount = item.PPnAmount.ToString("N0");
            returnItem.PPhAmount = item.PPhAmount.ToString("N0");
            returnItem.MateraiID = item.MateraiID;
            returnItem.MateraiAmount = materai.IsNull() ? "0" : materai.MateraiValue.ToString("N0");
            returnItem.TotalAmount = item.TotalAmount.ToString("N0");

            return returnItem;
        }


        public List<InvoiceAktivaView> ConvertFromA(IList<PenerimaanBarangAktiva> items, PenerimaanBarangHdr hdr)
        {
            List<InvoiceAktivaView> returnItems = new List<InvoiceAktivaView>();
            int i = 1;

            foreach (PenerimaanBarangAktiva item in items)
            {
                InvoiceAktivaView returnItem = new InvoiceAktivaView();
                FixedAssetMaster FA = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);
                GroupAsset GA = _groupAssetRepository.getGroupAsset(FA.IsNull() ? "" : FA.GroupAssetID);

                returnItem.Seq = i;
                returnItem.StartingAktivaId = item.AktivaId.Trim();
                returnItem.AktivaName = FA.IsNull() ? "" : FA.NamaAktiva.Trim();
                returnItem.ID = i;
                returnItem.COABeban = GA.IsNull() ? "" : GA.CoaBeban;
                returnItem.TanggalPerolehan = hdr.GRDate.ToString("dd/MM/yyyy");
                returnItem.Quantity = item.Quantity;
                returnItem.Price = item.Price.ToString("N0");
                returnItem.TotalPrice = item.Price.ToString("N0");
                returnItem.Notes = item.Notes;
                returnItem.TotalAmount = item.Price.ToString("N0");

                returnItems.Add(returnItem);
                i++;
            }
            return returnItems;
        }

        public List<InvoiceNonAktivaView> ConvertFromNA(IList<PenerimaanBarangNonAktiva> items)
        {
            List<InvoiceNonAktivaView> returnItems = new List<InvoiceNonAktivaView>();
            int i = 1;
            foreach (PenerimaanBarangNonAktiva item in items)
            {
                InvoiceNonAktivaView returnItem = new InvoiceNonAktivaView();
                //GroupAsset GA = _groupAssetRepository.getGroupAsset(item.GroupAssetID);
                NonAssetMaster nam = _nonAssetMasterRepository.getNonAssetMaster(item.NonAktivaId);

                returnItem.ID = i;
                //returnItem.InvoiceNo = item.InvoiceNo;
                returnItem.Seq = i;
                returnItem.NonAktivaId = item.NonAktivaId;
                returnItem.NonAktivaName = nam.IsNull() ? "" : nam.NamaNonAktiva.Trim();
                returnItem.Price = item.Price.ToString("N0");
                returnItem.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
                returnItem.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
                returnItem.Notes = item.Notes;
                returnItem.TotalAmount = item.Price.ToString("N0");
                returnItems.Add(returnItem);
                i++;
            }

            return returnItems;
        }

        //public InvoiceServiceView ConvertFrom(PenerimaanBarangService item)
        //{
        //    InvoiceServiceView returnItem = new InvoiceServiceView();
        //    FixedAssetMaster FA = _fixedAssetMasterRepository.getFixedAssetMaster(item.AktivaId);
        //    GroupAsset GA = _groupAssetRepository.getGroupAsset(FA.IsNull() ? "" : FA.GroupAssetID);

        //    returnItem.ID = item.Seq;
        //    //returnItem.InvoiceNo = item.InvoiceNo;
        //    returnItem.Seq = item.Seq;
        //    returnItem.AktivaId = item.AktivaId;
        //    returnItem.COABeban = GA.IsNull() ? "" : GA.CoaBeban;
        //    returnItem.AktivaName = FA.IsNull() ? "" : FA.NamaAktiva.Trim();
        //    //returnItem.PONo = item.PONo;
        //    //returnItem.WorkOrderNo = item.WorkOrderNo;
        //    //returnItem.ServiceDate = item.ServiceDate.ToString("dd/MM/yyyy");
        //    //returnItem.ServiceGuaranteeDate = item.ServiceGuaranteeDate.ToString("dd/MM/yyyy");
        //    returnItem.ServiceDate = DateTime.Now.ToString("dd/MM/yyyy");
        //    returnItem.ServiceGuaranteeDate = DateTime.Now.ToString("dd/MM/yyyy");
        //    returnItem.ItemAmount = item.Amount.ToString("N0");
        //    returnItem.Notes = item.Notes;
        //    //returnItem.PPnId = item.PPnId.ToString().Trim();
        //    //returnItem.PPnAmount = item.PPnAmount.ToString("N0");
        //    //returnItem.PPhId = item.PPhId.ToString().Trim();
        //    //returnItem.PPhAmount = item.PPhAmount.ToString("N0");
        //    returnItem.TotalAmount = item.Amount.ToString("N0");

        //    return returnItem;
        //}


        // POST: Role/Edit/5
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Edit")]
        public ActionResult Edit(InvoiceHView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            Company company = Lookup.Get<Company>();

            string message = "";
            bool isActiva = false;
            bool isNonActiva = false;
            bool isService = false;

            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            switch (data.InvoiceTypeID)
            {
                case "AC":
                    isActiva = true;
                    break;
                case "ANA":
                    isNonActiva = true;
                    isActiva = true;
                    break;
                case "APA":
                    isActiva = true;
                    isService = true;
                    break;
                case "NA":
                    isNonActiva = true;
                    break;
                case "NPA":
                    isNonActiva = true;
                    isService = true;
                    break;
                case "PA":
                    isService = true;
                    break;
                case "ST":
                    isActiva = true;
                    isNonActiva = true;
                    isService = true;
                    break;
                default:
                    break;
            }

            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    InvoiceH newData = _InvoiceRepository.getInvoice(data.InvoiceNo);
                    List<FixedAssetMaster> famDatas = new List<FixedAssetMaster>();

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    if (message.IsNullOrEmpty())
                    {
                        newData.VendorInvoiceNo = data.VendorInvoiceNo;
                        newData.VendorID = data.VendorID;
                        newData.InvoiceDate = DateTime.ParseExact(data.InvoiceDate, "d/M/yyyy", culture);
                        newData.InvoiceDueDate = DateTime.ParseExact(data.InvoiceOrderDate, "d/M/yyyy", culture);
                        newData.InvoiceOrderDate = DateTime.ParseExact(data.InvoiceOrderDate, "d/M/yyyy", culture);
                        newData.Notes = data.Notes;
                        newData.DeliveryOrderNo = data.DeliveryOrderNo;
                        newData.GRNo = data.GRNo;
                        newData.PONo = data.PONo;
                        newData.VendorBankId = data.VendorBankId;
                        newData.InvoiceAmount = 0;// Convert.ToDouble(data.InvoiceAmount);
                        newData.InvoiceTypeID = data.InvoiceTypeID;
                        newData.NoFakturPajak = data.NoFakturPajak;
                        newData.TglFakturPajak = DateTime.ParseExact(data.TglFakturPajak, "d/M/yyyy", culture);
                        newData.Status = InvoiceStatusEnum.N.ToString();
                        //aktiva
                        if (isActiva)
                        {
                            newData.InvoiceAktivaList = new List<InvoiceAktiva>();
                            int a = 1;
                            foreach (var item in data.InvoiceAktivaList)
                            {
                                Tax taxpph = _TaxRepository.getTax(item.PPhId);
                                Tax taxppn = _TaxRepository.getTax(item.PPnId);
                                InvoiceAktiva newItem = new InvoiceAktiva(a);
                                newItem.InvoiceNo = data.InvoiceNo;
                                newItem.ID = a;
                                newItem.Seq = a;
                                newItem.StartingAktivaId = item.StartingAktivaId;
                                newItem.PurchaseOrderNo = item.PurchaseOrderNo;
                                newItem.TanggalPerolehan = DateTime.ParseExact(item.TanggalPerolehan, "d/M/yyyy", culture);
                                newItem.Quantity = 1;
                                newItem.Price = Convert.ToDouble(item.Price);
                                newItem.ExpedCost = Convert.ToDouble(item.ExpedCost);
                                newItem.OtherCost = Convert.ToDouble(item.OtherCost);
                                newItem.TotalPrice = Convert.ToDouble(item.TotalPrice);
                                newItem.Notes = item.Notes;
                                newItem.TotalPrice = Convert.ToDouble(item.Price) * 1;
                                newItem.PPnAmount = Convert.ToDouble(item.PPnAmount);//(newItem.TotalPrice * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate))) / 100;
                                newItem.PPhAmount = Convert.ToDouble(item.PPhAmount);// (newItem.TotalPrice * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate))) / 100;
                                newItem.TotalAmount = (newItem.TotalPrice + newItem.PPnAmount + newItem.OtherCost + newItem.ExpedCost) - newItem.PPhAmount;
                                newData.InvoiceAmount += newItem.TotalAmount;
                                newData.InvoiceAktivaList.Add(newItem);

                                FixedAssetMaster famData = _fixedAssetMasterRepository.getFixedAssetMaster(newItem.StartingAktivaId);
                                //tab perolehan
                                famData.VendorID = newData.VendorID;
                                famData.NoPO = newItem.PurchaseOrderNo;
                                famData.NoDO = newItem.DeliveryOrderNo;
                                famData.NoGSRN = newItem.GRNo;
                                famData.NoInvoice = newData.VendorInvoiceNo;
                                famData.TanggalPerolehan = DateTime.ParseExact(item.TanggalPerolehan, "d/M/yyyy", culture);
                                famData.NilaiOriginal = newItem.Price;
                                famData.HargaPerolehan = newItem.Price;
                                famData.KeteranganPerolehan = newItem.Notes;
                                famDatas.Add(famData);
                                a++;
                            }
                        }
                        if (isNonActiva)
                        {
                            //nonAktiva
                            newData.InvoiceNonAktivaList = new List<InvoiceNonAktiva>();
                            int n = 1;

                            foreach (var item in data.InvoiceNonAktivaList)
                            {
                                Tax taxpph = _TaxRepository.getTax(item.PPhId);
                                Tax taxppn = _TaxRepository.getTax(item.PPnId);
                                InvoiceNonAktiva newItem = new InvoiceNonAktiva(n);
                                newItem.InvoiceNo = data.InvoiceNo;
                                newItem.ID = n;
                                newItem.Seq = n;
                                newItem.NonAktivaId = item.NonAktivaId;
                                newItem.ItemName = item.ItemName;
                                newItem.Price = Convert.ToDouble(item.Price);
                                newItem.ExpedCost = Convert.ToDouble(item.ExpedCost);
                                newItem.OtherCost = Convert.ToDouble(item.OtherCost);
                                newItem.ExpenseType = item.ExpenseType;
                                newItem.TimePeriod = item.TimePeriod;
                                newItem.StartDate = DateTime.ParseExact(item.StartDate, "d/M/yyyy", culture);
                                newItem.EndDate = DateTime.ParseExact(item.EndDate, "d/M/yyyy", culture);
                                newItem.MonthlyExpense = Convert.ToDouble(item.MonthlyExpense);
                                newItem.ExpensePA = item.ExpensePA;
                                newItem.PrepaidPA = item.PrepaidPA;
                                newItem.Notes = item.Notes;
                                newItem.PPnAmount = Convert.ToDouble(item.PPnAmount);//(newItem.Price * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate))) / 100;
                                newItem.PPhAmount = Convert.ToDouble(item.PPhAmount);//(newItem.Price * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate))) / 100;
                                newItem.TotalAmount = (Convert.ToDouble(item.Price) + newItem.PPnAmount + newItem.OtherCost + newItem.ExpedCost) - newItem.PPhAmount;
                                newItem.DepreciatedTime = item.DepreciatedTime;
                                newData.InvoiceAmount += newItem.TotalAmount;
                                newData.InvoiceNonAktivaList.Add(newItem);
                                n++;
                            }
                        }
                        if (isService)
                        {
                            //Service
                            newData.InvoiceServiceList = new List<InvoiceService>();
                            int s = 1;

                            foreach (var item in data.InvoiceServiceList)
                            {
                                Tax taxpph = _TaxRepository.getTax(item.PPhId);
                                Tax taxppn = _TaxRepository.getTax(item.PPnId);
                                InvoiceService newItem = new InvoiceService(s);
                                Materai materai = _materaiRepository.GetMaterai(item.MateraiID);


                                newItem.InvoiceNo = data.InvoiceNo;
                                newItem.ID = s;
                                newItem.Seq = s;
                                newItem.AktivaId = item.AktivaId;
                                newItem.ExpensePA = item.ExpensePA;
                                newItem.OtherCost = Convert.ToDouble(item.OtherCost);
                                newItem.WorkOrderNo = item.WorkOrderNo;
                                newItem.ServiceDate = DateTime.ParseExact(item.ServiceDate, "d/M/yyyy", culture);
                                newItem.ServiceGuaranteeDate = DateTime.ParseExact(item.ServiceGuaranteeDate, "d/M/yyyy", culture);
                                newItem.ServiceAmount = Convert.ToDouble(item.ServiceAmount);
                                newItem.ItemAmount = Convert.ToDouble(item.ItemAmount);
                                newItem.Notes = item.Notes;
                                newItem.MateraiID = item.MateraiID.IsNull() ? "" : item.MateraiID.Trim();
                                newItem.MateraiAmount = materai.IsNull() ? 0 : materai.MateraiValue;
                                newItem.PPnAmount = Convert.ToDouble(item.PPnAmount);//(newItem.ItemAmount * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate))) / 100;
                                newItem.PPhAmount = Convert.ToDouble(item.PPhAmount);//(newItem.ServiceAmount * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate))) / 100;
                                newItem.TotalAmount = (newItem.ServiceAmount + newItem.PPnAmount + newItem.ItemAmount + newItem.MateraiAmount + newItem.OtherCost) - newItem.PPhAmount;
                                newData.InvoiceAmount += newItem.TotalAmount;
                                newData.InvoiceServiceList.Add(newItem);
                                s++;
                            }
                        }
                        newData.UpdateUser = user.Username;
                        newData.UpdateDate = DateTime.Now;

                        message = _InvoiceService.UpdateInvoice(newData, user, company);
                        using (TransactionScope scope = new TransactionScope())
                        {
                            foreach (FixedAssetMaster famupd in famDatas)
                            {
                                _fixedAssetMasterRepository.Save(famupd);
                            }
                            scope.Complete();
                        }
                        if (message == "")
                            _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");

                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Update_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                InvoiceH newData = _InvoiceRepository.getInvoice(Id);
                _InvoiceRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckAddAccountAktiva")]
        public ActionResult NoAuthCheckAddAccountAktiva(InvoiceHView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.InvoiceAktivaList.Add(AddNewAktivaRecord(invH.InvoiceAktivaList));
            InvoiceHView InvH = CountAmount(invH);

            return CreateView(InvH);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckAddAccountNonAktiva")]
        public ActionResult NoAuthCheckAddAccountNonAktiva(InvoiceHView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.InvoiceNonAktivaList.Add(AddNewNonAktivaRecord(invH.InvoiceNonAktivaList));
            InvoiceHView InvH = CountAmount(invH);
            InvoiceHView invh = CountNonActivaAmortize(InvH);

            return CreateView(InvH);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckAddAccountService")]
        public ActionResult NoAuthCheckAddAccountService(InvoiceHView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.InvoiceServiceList.Add(AddNewServiceRecord(invH.InvoiceServiceList));
            InvoiceHView InvH = CountAmount(invH);
            return CreateView(InvH);
        }

        [HttpPost]
        public ActionResult NoAuthCheckDeleteAktivaRow(InvoiceHView invH, int RecordID)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.InvoiceAktivaList.Remove(invH.InvoiceAktivaList.FindElement(d => d.ID == RecordID));
            if (invH.InvoiceAktivaList.Count == 0)
            {
                invH.InvoiceAktivaList = CreateNewAktivaDetail();
            }

            InvoiceHView InvH = CountAmount(invH);
            return CreateView(InvH);
        }
        [HttpPost]
        public ActionResult NoAuthCheckDeleteNonAktivaRow(InvoiceHView invH, int RecordID)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.InvoiceNonAktivaList.Remove(invH.InvoiceNonAktivaList.FindElement(d => d.ID == RecordID));
            if (invH.InvoiceNonAktivaList.Count == 0)
            {
                invH.InvoiceNonAktivaList = CreateNewNonAktivaDetail();
            }

            InvoiceHView InvH = CountAmount(invH);
            InvoiceHView invh = CountNonActivaAmortize(InvH);
            return CreateView(InvH);
        }
        [HttpPost]
        public ActionResult NoAuthCheckDeleteServiceRow(InvoiceHView invH, int RecordID)
        {
            ViewData["ActionName"] = invH.Mode;
            invH.InvoiceServiceList.Remove(invH.InvoiceServiceList.FindElement(d => d.ID == RecordID));
            if (invH.InvoiceServiceList.Count == 0)
            {
                invH.InvoiceServiceList = CreateNewserviceDetail();
            }

            InvoiceHView InvH = CountAmount(invH);
            return CreateView(InvH);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckCalculateActivaAmount")]
        public ActionResult NoAuthCheckCalculateActivaAmount(InvoiceHView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            GeneralSetting GS = _generalSettingRepository.GetGeneralSetting("FANONACTIVA", "FA");
            if (GS.IsNull())
            {
                IFIN.Repository.SystemRepositoryBase sb = IFIN.Repository.SystemRepositoryBase.Instance;
                string dbname = sb.Connection.GetConnectionSetting().DBName;
                ScreenMessages.Submit(ScreenMessage.Error("General Setting ID \"FANONACTIVA\" not found in database " + dbname + "!"));
                CollectScreenMessages();
                return CreateView(invH);
            }

            string message = "";

            try
            {
                for (int i = 0; i <= invH.InvoiceAktivaList.Count - 1; i++)
                {
                    double CurValue = Convert.ToDouble(invH.InvoiceAktivaList[i].Price);
                    double CurLimit = Convert.ToDouble(GS.GSValue);

                    if (CurValue < CurLimit)
                    {
                        message = "Minimum nilai Aktiva adalah " + CurLimit.ToString("n") + "!";
                        invH.InvoiceAktivaList[i].Price = "0";
                    }
                }
                CountAmount(invH);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (!message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                CollectScreenMessages();
                return CreateView(invH);
            }
            else
            {
                return CreateView(invH);
            }
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckCalculateNonActivaAmount")]
        public ActionResult NoAuthCheckCalculateNonActivaAmount(InvoiceHView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            InvoiceHView InvH = CountAmount(invH);
            InvoiceHView invh = CountNonActivaAmortize(InvH);
            return CreateView(InvH);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckCalculateServiceAmount")]
        public ActionResult NoAuthCheckCalculateServiceAmount(InvoiceHView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            InvoiceHView InvH = CountAmount(invH);
            return CreateView(InvH);
        }

        private InvoiceHView CountAmount(InvoiceHView invH)
        {
            try
            {
                double InvoiceAmount = 0;
                foreach (var item in invH.InvoiceAktivaList)
                {
                    Tax taxppn = _TaxRepository.getTax(item.PPnId);
                    Tax taxpph = _TaxRepository.getTax(item.PPhId);

                    item.Quantity = 1;
                    item.TotalPrice = (Convert.ToDouble(item.Price) * Convert.ToDouble(item.Quantity)).ToString("N0");
                    item.OtherCost = Convert.ToDouble(item.OtherCost).ToString("N0");
                    item.ExpedCost = Convert.ToDouble(item.ExpedCost).ToString("N0");
                    item.PPnAmount = Convert.ToDouble(item.PPnAmount).ToString("N0"); //Math.Floor((Convert.ToDouble(item.TotalPrice) * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate))) / 100).ToString("N0");
                    item.PPhAmount = Convert.ToDouble(item.PPhAmount).ToString("N0"); //Math.Floor((Convert.ToDouble(item.TotalPrice) * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate))) / 100).ToString("N0");
                    item.TotalAmount = ((Convert.ToDouble(item.TotalPrice) + Convert.ToDouble(item.PPnAmount) + Convert.ToDouble(item.OtherCost) + Convert.ToDouble(item.ExpedCost)) - Convert.ToDouble(item.PPhAmount)).ToString("N0");
                    item.Price = Convert.ToDouble(item.Price).ToString("N0");
                    InvoiceAmount += Convert.ToDouble(item.TotalAmount);
                    invH.InvoiceAmount = InvoiceAmount.ToString("N0");
                }

                foreach (var item in invH.InvoiceNonAktivaList)
                {
                    Tax taxppn = _TaxRepository.getTax(item.PPnId);
                    Tax taxpph = _TaxRepository.getTax(item.PPhId);

                    item.Price = Convert.ToDouble(item.Price).ToString("N0");
                    item.OtherCost = Convert.ToDouble(item.OtherCost).ToString("N0");
                    item.ExpedCost = Convert.ToDouble(item.ExpedCost).ToString("N0");
                    //item.TotalPrice = (Convert.ToDouble(item.Price) * item.Qty).ToString("N0");
                    item.PPnAmount = Convert.ToDouble(item.PPnAmount).ToString("N0");//Math.Floor(((Convert.ToDouble(item.Price) * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate)))) / 100).ToString("N0");
                    item.PPhAmount = Convert.ToDouble(item.PPhAmount).ToString("N0");//Math.Floor(((Convert.ToDouble(item.Price) * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate)))) / 100).ToString("N0");
                    item.TotalAmount = ((Convert.ToDouble(item.Price) + Convert.ToDouble(item.PPnAmount) + Convert.ToDouble(item.OtherCost) + Convert.ToDouble(item.ExpedCost)) - Convert.ToDouble(item.PPhAmount)).ToString("N0");
                    InvoiceAmount += Convert.ToDouble(item.TotalAmount);
                    invH.InvoiceAmount = InvoiceAmount.ToString("N0");
                }

                foreach (var item in invH.InvoiceServiceList)
                {
                    Tax taxppn = _TaxRepository.getTax(item.PPnId);
                    Tax taxpph = _TaxRepository.getTax(item.PPhId);
                    Materai materai = _materaiRepository.GetMaterai(item.MateraiID);
                    item.ServiceAmount = Convert.ToDouble(item.ServiceAmount).ToString("N0");
                    item.OtherCost = Convert.ToDouble(item.OtherCost).ToString("N0");
                    item.ItemAmount = Convert.ToDouble(item.ItemAmount).ToString("N0");
                    item.PPnAmount = Convert.ToDouble(item.PPnAmount).ToString("N0");//Math.Floor(((Convert.ToDouble(item.ItemAmount) * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate)))) / 100).ToString("N0");
                    item.PPhAmount = Convert.ToDouble(item.PPhAmount).ToString("N0");//Math.Floor(((Convert.ToDouble(item.ServiceAmount) * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate)))) / 100).ToString("N0");
                    item.MateraiAmount = materai.IsNull() ? "0" : materai.MateraiValue.ToString("N0");
                    item.TotalAmount = ((Convert.ToDouble(item.ServiceAmount) + Convert.ToDouble(item.PPnAmount) + Convert.ToDouble(item.ItemAmount) + Convert.ToDouble(item.MateraiAmount) + Convert.ToDouble(item.OtherCost)) - Convert.ToDouble(item.PPhAmount)).ToString("N0");

                    InvoiceAmount += Convert.ToDouble(item.TotalAmount);
                    invH.InvoiceAmount = InvoiceAmount.ToString("N0");
                }
            }
            catch (Exception e)
            {
                ScreenMessages.Submit(ScreenMessage.Error(e.Message));
                CollectScreenMessages();
            }
            return invH;
        }

        //private InvoiceHView CountNonActivaAmount(InvoiceHView invH)
        //{
        //    try
        //    {
        //        double InvoiceAmount = 0;
        //        foreach (var item in invH.InvoiceNonAktivaList)
        //        {
        //            Tax taxppn = _TaxRepository.getTax(item.PPnId);
        //            Tax taxpph = _TaxRepository.getTax(item.PPhId);

        //            item.PPnAmount = ((Convert.ToDouble(item.Amount) * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate))) / 100).ToString("N0");
        //            item.PPhAmount = ((Convert.ToDouble(item.Amount) * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate))) / 100).ToString("N0");
        //            item.TotalAmount = (Convert.ToDouble(item.Amount) + Convert.ToDouble(item.PPnAmount) + Convert.ToDouble(item.PPhAmount)).ToString("N0");
        //            item.Amount = Convert.ToDouble(item.Amount).ToString("N0");
        //            InvoiceAmount += Convert.ToDouble(item.TotalAmount);
        //            invH.InvoiceAmount = InvoiceAmount.ToString("N0");
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        ScreenMessages.Submit(ScreenMessage.Error(e.Message));
        //        CollectScreenMessages();
        //    }
        //    return invH;
        //}

        //private InvoiceHView CountServiceAmount(InvoiceHView invH)
        //{
        //    try
        //    {
        //        double InvoiceAmount = 0;
        //        foreach (var item in invH.InvoiceServiceList)
        //        {
        //            Tax taxppn = _TaxRepository.getTax(item.PPnId);
        //            Tax taxpph = _TaxRepository.getTax(item.PPhId);

        //            item.PPnAmount = ((Convert.ToDouble(item.Amount) * (taxppn == null ? 0 : Convert.ToDouble(taxppn.Rate))) / 100).ToString("N0");
        //            item.PPhAmount = ((Convert.ToDouble(item.Amount) * (taxpph == null ? 0 : Convert.ToDouble(taxpph.Rate))) / 100).ToString("N0");
        //            item.TotalAmount = (Convert.ToDouble(item.Amount) + Convert.ToDouble(item.PPnAmount) + Convert.ToDouble(item.PPhAmount)).ToString("N0");
        //            item.Amount = Convert.ToDouble(item.Amount).ToString("N0");
        //            InvoiceAmount += Convert.ToDouble(item.TotalAmount);
        //            invH.InvoiceAmount = InvoiceAmount.ToString("N0");
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        ScreenMessages.Submit(ScreenMessage.Error(e.Message));
        //        CollectScreenMessages();
        //    }
        //    return invH;
        //}

        private InvoiceHView CountNonActivaAmortize(InvoiceHView invH)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            for (int i = 0; i <= invH.InvoiceNonAktivaList.Count - 1; i++)
            {
                if (!invH.InvoiceNonAktivaList[i].ExpenseType.IsNull())
                {
                    if (invH.InvoiceNonAktivaList[i].ExpenseType.Trim() == "A")
                    {
                        DateTime prdStart = DateTime.ParseExact(invH.InvoiceNonAktivaList[i].StartDate, "d/M/yyyy", culture);
                        DateTime prdFinish = DateTime.ParseExact(invH.InvoiceNonAktivaList[i].EndDate, "d/M/yyyy", culture);

                        int TimePeriod = 12 * (prdFinish.Year - prdStart.Year) + prdFinish.Month - prdStart.Month;
                        invH.InvoiceNonAktivaList[i].TimePeriod = TimePeriod;
                        invH.InvoiceNonAktivaList[i].MonthlyExpense = (Convert.ToDouble(invH.InvoiceNonAktivaList[i].Price) / Convert.ToDouble(invH.InvoiceNonAktivaList[i].TimePeriod)).ToString("N0");
                    }
                    else
                    {
                        invH.InvoiceNonAktivaList[i].StartDate = DateTime.Now.ToString("dd/MM/yyyy");
                        invH.InvoiceNonAktivaList[i].EndDate = DateTime.Now.ToString("dd/MM/yyyy");
                        invH.InvoiceNonAktivaList[i].TimePeriod = 0;
                        invH.InvoiceNonAktivaList[i].MonthlyExpense = "0";
                    }
                }
            }
            return invH;
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckValidateNonActivaAmount")]
        public ActionResult NoAuthCheckValidateNonActivaAmount(InvoiceHView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            //string message = "";
            //GeneralSetting GS = _generalSettingRepository.GetGeneralSetting("FANONACTIVA", "FA");

            //try
            //{
            //for (int i = 0; i <= invH.InvoiceNonAktivaList.Count - 1; i++)
            //{
            //    double CurValue = Convert.ToDouble(invH.InvoiceNonAktivaList[i].Price);
            //    double CurLimit = Convert.ToDouble(GS.GSValue);

            //    if ((CurValue > CurLimit))
            //    {
            //        message = "Maksimum nilai Non-Aktiva adalah " + CurLimit.ToString("N0") + "!";
            //        invH.InvoiceNonAktivaList[i].Price = "0";
            //    }
            //}
            invH = CountAmount(invH);
            invH = CountNonActivaAmortize(invH);
            //}
            //catch (Exception e)
            //{
            //    message = e.Message;
            //}

            //if (!message.IsNullOrEmpty())
            //{
            //    ScreenMessages.Submit(ScreenMessage.Error(message));
            //    CollectScreenMessages();
            //    return CreateView(invH);
            //}
            //else
            //{
            return CreateView(invH);
            //}
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckValidateNonActivaPeriod")]
        public ActionResult NoAuthCheckValidateNonActivaPeriod(InvoiceHView invH)
        {
            ViewData["ActionName"] = invH.Mode;
            string message = "";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");

            try
            {
                for (int i = 0; i <= invH.InvoiceNonAktivaList.Count - 1; i++)
                {
                    DateTime prdStart = DateTime.ParseExact(invH.InvoiceNonAktivaList[i].StartDate, "d/M/yyyy", culture);
                    DateTime prdFinish = DateTime.ParseExact(invH.InvoiceNonAktivaList[i].EndDate, "d/M/yyyy", culture);

                    if (prdFinish < prdStart)
                    {
                        message = "Periode yang di pilih tidak valid!";
                        invH.InvoiceNonAktivaList[i].StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
                        invH.InvoiceNonAktivaList[i].EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        invH = CountNonActivaAmortize(invH);

                        //if (invH.InvoiceNonAktivaList[i].ExpenseType == "A")
                        //{
                        //    int TimePeriod = 12 * (prdFinish.Year - prdStart.Year) + prdFinish.Month - prdStart.Month;
                        //    invH.InvoiceNonAktivaList[i].TimePeriod = TimePeriod + 1;
                        //    invH.InvoiceNonAktivaList[i].MonthlyExpense = (Convert.ToDouble(invH.InvoiceNonAktivaList[i].Amount) / Convert.ToDouble(invH.InvoiceNonAktivaList[i].TimePeriod)).ToString("N0");
                        //}
                        //else
                        //{
                        //    invH.InvoiceNonAktivaList[i].TimePeriod = 0;
                        //    invH.InvoiceNonAktivaList[i].MonthlyExpense = "0";
                        //    invH.InvoiceNonAktivaList[i].StartDate = DateTime.Now.ToString("dd/MM/yyyy");
                        //    invH.InvoiceNonAktivaList[i].EndDate = DateTime.Now.ToString("dd/MM/yyyy");
                        //}
                    }

                    //DateTime DtFrom = new DateTime(prdStart.Year, prdStart.Month, 1);
                    //DateTime DtTo = new DateTime(prdFinish.Year, prdFinish.AddMonths(1).Month, 1).AddDays(-1);

                    //GLPeriod GLP = _glPeriodRepository.GetGLPeriod(DtFrom, DtTo);

                    //if (GLP != null)
                    //{
                    //    switch (GLP.PeriodStatus)
                    //    {
                    //        case 0:
                    //            message = "Period " + prdStart.ToShortDateString() + "-" + prdFinish.ToShortDateString() + " belum dibuka!";
                    //            break;
                    //        case 2:
                    //            message = "Period " + prdStart.ToShortDateString() + "-" + prdFinish.ToShortDateString() + " sudah di tutup!";
                    //            break;
                    //    }
                    //}
                    //else
                    //{
                    //    message = "Period GL " + prdStart.ToShortDateString() + "-" + prdFinish.ToShortDateString() + " tidak ditemukan!";
                    //}
                    //}

                }
                invH = CountAmount(invH);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (!message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                CollectScreenMessages();
                return CreateView(invH);
            }
            else
            {
                return CreateView(invH);
            }
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "NoAuthCheckGetSuppAcc")]
        public ActionResult NoAuthCheckGetSuppAcc(InvoiceHView invH)
        {
            //ViewData["SupAacc"] = VendorAccountSelect(invH.VendorID.Trim(), "");
            ViewData["ActionName"] = invH.Mode;
            invH = CountAmount(invH);
            invH = CountNonActivaAmortize(invH);
            //invH = CountNonActivaAmount(invH);
            //invH = CountServiceAmount(invH);

            return CreateView(invH);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetCOA(string GroupAssetID)
        {
            GroupAsset groupAsset = _groupAssetRepository.getGroupAsset(GroupAssetID);

            List<object> objectList = new List<object>();

            objectList.Add(new { CoaBeban = groupAsset.CoaBeban });

            return Json(objectList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetAktivaCOA(string AssetID)
        {
            FixedAssetMaster asset = _fixedAssetMasterRepository.getFixedAssetMaster(AssetID);
            GroupAsset groupAsset = _groupAssetRepository.getGroupAsset(asset.GroupAssetID);

            List<object> objectList = new List<object>();

            objectList.Add(new { CoaBeban = groupAsset.CoaBeban });

            return Json(objectList, JsonRequestBehavior.AllowGet);
        }
        private IList<SelectListItem> createEmptySupAacc()
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            return dataList;
        }

        private IList<SelectListItem> VendorAccountSelect(string suppId, string selected)
        {
            List<VendorAccount> itemList = _VendorAccountRepository.getBankByVendor(suppId).ToList();
            string defaultItem = "";

            if (itemList.Count > 0)
            {
                var selectedItem = itemList.FindElement(s => s.DefaultAccount == true).VendorAccID;// .Select(s => s.DefaultAccount == true).ToList();
                defaultItem = selectedItem.IsNullOrEmpty() ? "" : selectedItem.Trim();
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<VendorAccount, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _VendorAccID = itemList.FirstOrDefault().VendorAccID;

            if (!selected.IsNull())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
                _VendorAccID = selected;
            }
            else if (!defaultItem.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == defaultItem.ToString()).Selected = true;
                _VendorAccID = selected;
            }
            //else
            //{
            //    dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            //    _VendorAccID = selected;
            //}
            return dataList;
        }

        private IList<SelectListItem> InvoiceTypeSelect(string selected)
        {
            List<InvoiceType> itemList = Lookup.Get<List<InvoiceType>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _invoiceTypeRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<InvoiceType, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _invoiceTypeID = itemList.FirstOrDefault().InvoiceTypeID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString())).Selected = true;
                _invoiceTypeID = selected;
            }

            return dataList;
        }

        private string GetNonActivaMaxValue(string gsid, string modulid)
        {
            GeneralSetting GS = _generalSettingRepository.GetGeneralSetting(gsid, modulid);
            return GS.GSValue;
        }

        public SelectListItem ConvertForm(InvoiceType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.InvoiceTypeName.Trim();
            returnItem.Value = item.InvoiceTypeID.Trim();
            return returnItem;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckSuppAcc(string suppId)
        {
            List<VendorAccount> itemList = _VendorAccountRepository.getBankByVendor(suppId).ToList();


            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<VendorAccount, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });


            //if (!selected.IsNullOrEmpty())
            //    dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public SelectListItem ConvertFrom(VendorAccount item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.VendorAccID.Trim() + "-" + item.VendorAccountName.Trim();
            returnItem.Value = item.VendorAccID.ToString().Trim();
            return returnItem;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult NoAuthCheckGetSuppAccDetail(string VendorAccID)
        {
            VendorAccount VendorAccount = _VendorAccountRepository.getVendorAccount(VendorAccID);

            return Json(VendorAccount, JsonRequestBehavior.AllowGet);
        }

        //====================================================Invice Role==================================================================

        private IList<SelectListItem> createInvoiceStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = Enum.GetValues(typeof(InvoiceStatusEnum)).Cast<InvoiceStatusEnum>()
                .Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            return dataList;
        }
        //===========================================================================================================================
        private IList<SelectListItem> ExpenseTypeSelect(string selected)
        {
            List<ExpenceType> itemList = Lookup.Get<List<ExpenceType>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _expenceTypeRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<ExpenceType, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _expenceTypeID = itemList.FirstOrDefault().ExpenceTypeID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString())).Selected = true;
                _expenceTypeID = selected;
            }

            return dataList;
        }

        public SelectListItem ConvertForm(ExpenceType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.ExpenceTypeName.Trim();
            returnItem.Value = item.ExpenceTypeID.ToString().Trim();
            return returnItem;
        }

        //=========================================================================================================================

        //===========================================================================================================================
        private IList<SelectListItem> MateraiSelect(string selected)
        {
            List<Materai> itemList = Lookup.Get<List<Materai>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _materaiRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Materai, SelectListItem>(ConvertForm));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });

            if (itemList.Count > 0)
                _expenceTypeID = itemList.FirstOrDefault().MateraiID;

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString().Trim())).Selected = true;
                _expenceTypeID = selected;
            }

            return dataList;
        }

        public SelectListItem ConvertForm(Materai item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.MateraiName.Trim();
            returnItem.Value = item.MateraiID.ToString().Trim();
            return returnItem;
        }

        //=========================================================================================================================
        private IList<SelectListItem> createGroupAssetSelect(string selected)
        {
            List<GroupAsset> itemList = Lookup.Get<List<GroupAsset>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _groupAssetRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GroupAsset, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem() { Value = "", Text = " " });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(GroupAsset item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GroupAssetDescription;
            returnItem.Value = item.GroupAssetID.Trim();
            return returnItem;
        }

        //=============================================================================

        private IList<SelectListItem> createTaxSelect(string selected, int param)
        {
            List<Tax> itemList = Lookup.Get<List<Tax>>();
            if (itemList.IsNullOrEmpty())
            {
                itemList = _TaxRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }
            if (param == 0)
                itemList = itemList.Where(items => items.TaxCategory.Trim() == "PPN").ToList();
            else
                itemList = itemList.Where(items => items.TaxCategory.Trim() == "PPH").ToList();

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<Tax, SelectListItem>(ConvertFromPPn));
            dataList.Insert(0, new SelectListItem() { Value = " ", Text = " " });
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == (selected.ToString().Trim() == "" ? " " : selected.ToString().Trim())).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFromPPn(Tax item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.TaxDescription.ToString().Trim() + " " + item.Rate.ToString().Trim() + " %";
            returnItem.Value = item.TaxID.ToString().Trim();
            return returnItem;
        }

        private void generateViewDataExpenceTypeSelect(IList<InvoiceNonAktivaView> InvoiceNonActivaList)
        {
            int index = 0;
            if (!InvoiceNonActivaList.IsNullOrEmpty())
            {
                foreach (InvoiceNonAktivaView item in InvoiceNonActivaList)
                {
                    ViewData["InvoiceNonAktivaList[" + (index).ToString() + "].ExpenseTypeSelect"] = ExpenseTypeSelect(item.ExpenseType);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }

        private void generateViewDataServiceMateraiSelect(IList<InvoiceServiceView> InvoiceServiceList)
        {
            int index = 0;
            if (!InvoiceServiceList.IsNullOrEmpty())
            {
                foreach (InvoiceServiceView item in InvoiceServiceList)
                {
                    ViewData["InvoiceServiceList[" + (index).ToString() + "].MateraiID_List"] = MateraiSelect(item.MateraiID);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }


        private void generateViewDataGroupAssetSelect(IList<InvoiceNonAktivaView> InvoiceNonActivaList)
        {
            int index = 0;
            if (!InvoiceNonActivaList.IsNullOrEmpty())
            {
                foreach (InvoiceNonAktivaView item in InvoiceNonActivaList)
                {
                    ViewData["InvoiceNonAktivaList[" + (index).ToString() + "].GroupAssetIDSelect"] = createGroupAssetSelect(item.GroupAssetID);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }

        private void generateViewDataPpnAktivaSelect(IList<InvoiceAktivaView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceAktivaView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceAktivaList[" + (index).ToString() + "].PPnId_tax"] = createTaxSelect(item.PPnId, 0);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }
        private void generateViewDataPphAktivaSelect(IList<InvoiceAktivaView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceAktivaView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceAktivaList[" + (index).ToString() + "].PPhId_tax"] = createTaxSelect(item.PPhId, 1);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }


        private void generateViewDataPpnNonAktivaSelect(IList<InvoiceNonAktivaView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceNonAktivaView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceNonAktivaList[" + (index).ToString() + "].PPnId_tax"] = createTaxSelect(item.PPnId, 0);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }
        private void generateViewDataPphNonAktivaSelect(IList<InvoiceNonAktivaView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceNonAktivaView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceNonAktivaList[" + (index).ToString() + "].PPhId_tax"] = createTaxSelect(item.PPhId, 1);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }

        private void generateViewDataPpnServiceSelect(IList<InvoiceServiceView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceServiceView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceServiceList[" + (index).ToString() + "].PPnId_tax"] = createTaxSelect(item.PPnId, 0);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }
        private void generateViewDataPphServiceSelect(IList<InvoiceServiceView> InvoiceAktivaList, int seq)
        {
            int index = 0;
            if (!InvoiceAktivaList.IsNullOrEmpty())
            {
                foreach (InvoiceServiceView item in InvoiceAktivaList)
                {
                    ViewData["InvoiceServiceList[" + (index).ToString() + "].PPhId_tax"] = createTaxSelect(item.PPhId, 1);
                    item.Seq = index + 1;
                    index++;
                }
            }
        }

        public ActionResult Generate()
        {
            CollectScreenMessages();
            return View("PenerimaanBarangList");
        }
    }
}