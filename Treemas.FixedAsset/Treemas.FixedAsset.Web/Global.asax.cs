﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Reflection;
using Treemas.Base.Web.Platform;
using Treemas.Base.Utilities;
using Treemas.Credential;
using Treemas.Credential.Interface;
using Treemas.Credential.Repository;
using Treemas.AuditTrail;
using Treemas.AuditTrail.Interface;
using Treemas.AuditTrail.Repository;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using Treemas.IFIN.Repository;
using Treemas.FixedAsset.Service;
using Treemas.OLS.Repository;

namespace Treemas.FixedAsset.Web
{
    public class MvcApplication : WebApplication
    {
        public MvcApplication() : base("TreemasFixedAsset")
        {
            SystemSettings.Instance.Name = "Treemas Fixed Asset";
            SystemSettings.Instance.Alias = "BNIMF-Asset";
            SystemSettings.Instance.Runtime.IsPortal = false;
            SystemSettings.Instance.Security.EnableAuthentication = true;
            SystemSettings.Instance.Security.IgnoreAuthorization = false;
            SystemSettings.Instance.Security.EnableSingleSignOn = false;
            SystemSettings.Instance.Logging.Enabled = false;
            SystemSettings.Instance.Security.EnableSiteSelection = false;
        }

        protected override void Startup()
        {
            Encryption.Instance.SetHashKey(SystemSettings.Instance.Security.EncryptionHalfKey);
            RouteCollection routes = RouteTable.Routes;
            RouteConfig.RegisterRoutes(routes);

            this.BindInterface();
        }

        private void BindInterface()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            container.Register<ISessionAuthentication, SessionAuthentication>(Lifestyle.Scoped);
            container.Register<ISSOSessionStorage, SSOSessionStorage>(Lifestyle.Scoped);
            // Register your types, for instance:
            container.Register<IUserRepository, UserRepository>(Lifestyle.Scoped);
            container.Register<IUserApplicationRepository, UserApplicationRepository>(Lifestyle.Scoped);
            container.Register<IRoleRepository, RoleRepository>(Lifestyle.Scoped);
            container.Register<IFunctionRepository, FunctionRepository>(Lifestyle.Scoped);
            container.Register<IFeatureRepository, FeatureRepository>(Lifestyle.Scoped);
            container.Register<IAuthorizationRepository, AuthorizationRepository>(Lifestyle.Scoped);
            container.Register<IApplicationRepository, ApplicationRepository>(Lifestyle.Scoped);
            container.Register<ILoginInfoRepository, LoginInfoRepository>(Lifestyle.Scoped);
            container.Register<IMenuRepository, MenuRepository>(Lifestyle.Scoped);
            container.Register<IUserAccount, UserAccount>(Lifestyle.Scoped);
            container.Register<IParameterRepository, ParameterRepository>(Lifestyle.Scoped);
            container.Register<IBusinessDateRepository, BusinessDateRepository>(Lifestyle.Scoped);
            container.Register<IUserSiteRepository, UserSiteRepository>(Lifestyle.Scoped);


            // Audit Trail Module
            container.Register<IAuditDataRepository, AuditDataRepository>(Lifestyle.Transient);
            container.Register<IAuditTrailLog, AuditTrailLog>(Lifestyle.Transient);

            // For Foundation Module
            container.Register<ICurrencyRepository, CurrencyRepository>(Lifestyle.Transient);
            container.Register<ICompanyRepository, CompanyRepository>(Lifestyle.Transient);
            container.Register<ISiteRepository, SiteRepository>(Lifestyle.Transient);
            container.Register<IUomRepository, UomRepository>(Lifestyle.Transient);
            container.Register<IUomCategoryRepository, UomCategoryRepository>(Lifestyle.Transient);
            container.Register<ISiteUserRepository, SiteUserRepository>(Lifestyle.Transient);
            container.Register<IKelurahanRepository, KelurahanRepository>(Lifestyle.Transient);
            container.Register<IModuleRepository, ModuleRepository>(Lifestyle.Transient);
            container.Register<IAssetTidakSusutRepository, AssetTidakSusutRepository>(Lifestyle.Transient);

            // For Fixed Asset Module
            container.Register<IAssetAreaRepository, AssetAreaRepository>(Lifestyle.Transient);
            container.Register<IAssetBranchRepository, AssetBranchRepository>(Lifestyle.Transient);
            container.Register<IAssetCoaRepository, AssetCoaRepository>(Lifestyle.Transient);
            container.Register<IAssetConditionRepository, AssetConditionRepository>(Lifestyle.Transient);
            container.Register<IAssetDeptRepository, AssetDeptRepository>(Lifestyle.Transient);
            container.Register<IAssetLocationRepository, AssetLocationRepository>(Lifestyle.Transient);
            container.Register<IAssetUserRepository, AssetUserRepository>(Lifestyle.Transient);
            container.Register<IGolonganPajakRepository, GolonganPajakRepository>(Lifestyle.Transient);
            container.Register<IGroupAssetRepository, GroupAssetRepository>(Lifestyle.Transient);
            container.Register<IJenisUsahaRepository, JenisUsahaRepository>(Lifestyle.Transient);
            container.Register<INamaHartaRepository, NamaHartaRepository>(Lifestyle.Transient);
            container.Register<ISubGroupAssetRepository, SubGroupAssetRepository>(Lifestyle.Transient);
            container.Register<IPaymentTypeRepository, PaymentTypeRepository>(Lifestyle.Transient);
            container.Register<IMetodeDepresiasiRepository, MetodeDepresiasiRepository>(Lifestyle.Transient);
            
            container.Register<IFixedAssetMasterRepository, FixedAssetMasterRepository>(Lifestyle.Transient);
            container.Register<IMutasiAktivaRepository, MutasiAktivaRepository>(Lifestyle.Transient);
            container.Register<IPerawatanAktivaRepository, PerawatanAktivaRepository>(Lifestyle.Transient);

            container.Register<IVendorRepository, VendorRepository>(Lifestyle.Transient);
            container.Register<IVendorAccountRepository, VendorAccountRepository>(Lifestyle.Transient);
            container.Register<IVendorSequence, VendorSequenceService>(Lifestyle.Transient);
            container.Register<IVendorService, VendorServiceRepository>(Lifestyle.Transient);
            container.Register<IVendorSequenceRepository, VendorSequenceRepository>(Lifestyle.Transient);

            container.Register<IInvoiceRepository, InvoiceRepository>(Lifestyle.Transient);
            container.Register<IPaymentAllocationRepository, PaymentAllocationRepository>(Lifestyle.Transient);
            container.Register<ITaxRepository, TaxRepository>(Lifestyle.Transient);
            container.Register<IInvoiceService, InvoiceServiceRepository>(Lifestyle.Transient);
            container.Register<IInvoiceSequenceRepository, InvoiceSequenceRepository>(Lifestyle.Transient);
            container.Register<IInvoiceSequence, InvoiceSequenceService>(Lifestyle.Transient);
            container.Register<IRequestReceiveRepository, RequestReceiveRepository>(Lifestyle.Transient);
            container.Register<IRequestPaymentRepository, RequestPaymentRepository>(Lifestyle.Transient);
            container.Register<IBankMasterRepository, BankMasterRepository>(Lifestyle.Transient);
            container.Register<IBankBranchMasterRepository, BankBranchMasterRepository>(Lifestyle.Transient);
            container.Register<IInvoiceTypeRepository, InvoiceTypeRepository>(Lifestyle.Transient);
            container.Register<IExpenceTypeRepository, ExpenceTypeRepository>(Lifestyle.Transient);

            container.Register<IPenerimaanBarangRepository, PenerimaanBarangRepository>(Lifestyle.Transient);
            container.Register<IPenerimaanBarangSequence, PenerimaanBarangSequenceService>(Lifestyle.Transient);
            container.Register<IPenerimaanBarangService, PenerimaanBarangServiceRepository>(Lifestyle.Transient);
            container.Register<IPenerimaanBarangSequenceRepository, PenerimaanBarangSequenceRepository>(Lifestyle.Transient);

            container.Register<IFixedAssetMasterSequence, FixedAssetMasterSequenceService>(Lifestyle.Transient);
            container.Register<IFixedAssetMasterSequenceRepository, FixedAssetSequenceRepository>(Lifestyle.Transient);
            container.Register<IFixedAssetMasterService, FixedAssetMasterServiceRepository>(Lifestyle.Transient);

            container.Register<IGeneralSettingRepository, GeneralSettingRepository>(Lifestyle.Transient);
            container.Register<IGLPeriodRepository, GLPeriodRepository>(Lifestyle.Transient);

            container.Register<IInsuranceComHORepository, InsuranceComHORepository>(Lifestyle.Transient);
            container.Register<IInsuranceComBranchRepository, InsuranceComBranchRepository>(Lifestyle.Transient);

            container.Register<IGLJournalRepository, GLJournalRepository>(Lifestyle.Transient);
            container.Register<IGLJournalService, GLJournalServiceRepository>(Lifestyle.Transient);
            container.Register<IGLJournalSequenceRepository, GLJournalSequenceRepository>(Lifestyle.Transient);
            container.Register<IGLJournalSequence, GLJournalSequenceService>(Lifestyle.Transient);
            container.Register<IGRTypeRepository, GRTypeRepository>(Lifestyle.Transient);
            container.Register<IMateraiRepository, MateraiRepository>(Lifestyle.Transient);

            container.Register<INonAssetMasterRepository, NonAssetMasterRepository>(Lifestyle.Transient);
            container.Register<INonAssetMasterService, NonAssetServiceRepository>(Lifestyle.Transient);
            container.Register<INonAssetMasterSequenceRepository, NonAssetMasterSequenceRepository>(Lifestyle.Transient);
            container.Register<INonAssetMasterSequence, NonAssetSequenceService>(Lifestyle.Transient);

            container.Register<IPenjualanAktivaRepository, PenjualanAktivaRepository>(Lifestyle.Transient);
            container.Register<IPenjualanAktivaService, PenjualanAktivaServiceRepository>(Lifestyle.Transient);
            container.Register<IPenjualanAktivaSequenceRepository, PenjualanAktivaSequenceRepository>(Lifestyle.Transient);
            container.Register<IPenjualanAktivaSequence, PenjualanAktivaSequenceService>(Lifestyle.Transient);

            container.Register<IPenghapusanAktivaRepository, PenghapusanAktivaRepository>(Lifestyle.Transient);
            container.Register<IPenghapusanAktivaService, PenghapusanAktivaServiceRepository>(Lifestyle.Transient);
            container.Register<IPenghapusanAktivaSequenceRepository, PenghapusanAktivaSequenceRepository>(Lifestyle.Transient);
            container.Register<IPenghapusanAktivaSequence, PenghapusanAktivaSequenceService>(Lifestyle.Transient);
            container.Register<IAssetTypeRepository, AssetTypeRepository>(Lifestyle.Transient);
            container.Register<IGLJournalTypeRepository, GlJournalTypeRepository>(Lifestyle.Transient);

            container.Register<IIfinsGLJournalRepository, IfinsGLJournalRepository>(Lifestyle.Transient);
            container.Register<IDepresiasiKomersialRepository, DepresiasiKomersialRepository>(Lifestyle.Transient);
            //ols
            container.Register<IInventoryRepository, InventoryRepository>(Lifestyle.Transient);
            container.Register<IAssetRepository, AssetRepository>(Lifestyle.Transient);


            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            // Override setting parameter
            var paramRepo = container.GetInstance<IParameterRepository>();
            var param = paramRepo.getParameter();
            SystemSettings.Instance.Security.SessionTimeout = param.SessionTimeout;
            SystemSettings.Instance.Security.LoginAttempt = param.LoginAttempt;
            SystemSettings.Instance.Security.MaximumConcurrentLogin = param.MaximumConcurrentLogin;
        }
    }
}
