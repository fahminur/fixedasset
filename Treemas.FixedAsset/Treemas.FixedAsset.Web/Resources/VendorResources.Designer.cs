﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Treemas.FixedAsset.Web.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class VendorResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal VendorResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Treemas.FixedAsset.Web.Resources.VendorResources", typeof(VendorResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pembeli.
        /// </summary>
        public static string Buyer {
            get {
                return ResourceManager.GetString("Buyer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nama Kontak Vendor.
        /// </summary>
        public static string ContactPersonName {
            get {
                return ResourceManager.GetString("ContactPersonName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mata Uang.
        /// </summary>
        public static string Currency {
            get {
                return ResourceManager.GetString("Currency", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Auto Update Resource.
        /// </summary>
        public static string FlagAutoUpdateResource {
            get {
                return ResourceManager.GetString("FlagAutoUpdateResource", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NPWP.
        /// </summary>
        public static string NPWP {
            get {
                return ResourceManager.GetString("NPWP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alamat NPWP .
        /// </summary>
        public static string NPWPAddress {
            get {
                return ResourceManager.GetString("NPWPAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kota NPWP.
        /// </summary>
        public static string NPWPCity {
            get {
                return ResourceManager.GetString("NPWPCity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NPWP Kecamatan.
        /// </summary>
        public static string NPWPKecamatan {
            get {
                return ResourceManager.GetString("NPWPKecamatan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NPWP Kelurahan.
        /// </summary>
        public static string NPWPKelurahan {
            get {
                return ResourceManager.GetString("NPWPKelurahan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kode Pos NPWP.
        /// </summary>
        public static string NPWPZipCode {
            get {
                return ResourceManager.GetString("NPWPZipCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vendor .
        /// </summary>
        public static string PageTitle {
            get {
                return ResourceManager.GetString("PageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Jangka Waktu Pembayaran.
        /// </summary>
        public static string PaymentTerm {
            get {
                return ResourceManager.GetString("PaymentTerm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Nama Pembeli.
        /// </summary>
        public static string Validation_Buyer {
            get {
                return ResourceManager.GetString("Validation_Buyer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Nama Kontak Vendor.
        /// </summary>
        public static string Validation_ContactPersonName {
            get {
                return ResourceManager.GetString("Validation_ContactPersonName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Mata Uang.
        /// </summary>
        public static string Validation_Currency {
            get {
                return ResourceManager.GetString("Validation_Currency", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data Sudah ada.
        /// </summary>
        public static string Validation_DuplicateData {
            get {
                return ResourceManager.GetString("Validation_DuplicateData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan FlagAutoUpdateResource.
        /// </summary>
        public static string Validation_FlagAutoUpdateResource {
            get {
                return ResourceManager.GetString("Validation_FlagAutoUpdateResource", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Nama.
        /// </summary>
        public static string Validation_Name {
            get {
                return ResourceManager.GetString("Validation_Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan NPWP.
        /// </summary>
        public static string Validation_NPWP {
            get {
                return ResourceManager.GetString("Validation_NPWP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Alamat NPWP.
        /// </summary>
        public static string Validation_NPWPAddress {
            get {
                return ResourceManager.GetString("Validation_NPWPAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Kota NPWP.
        /// </summary>
        public static string Validation_NPWPCity {
            get {
                return ResourceManager.GetString("Validation_NPWPCity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan NPWP Kecamatan.
        /// </summary>
        public static string Validation_NPWPKecamatan {
            get {
                return ResourceManager.GetString("Validation_NPWPKecamatan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan NPWP Kelurahan.
        /// </summary>
        public static string Validation_NPWPKelurahan {
            get {
                return ResourceManager.GetString("Validation_NPWPKelurahan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Kode Pos NPWP.
        /// </summary>
        public static string Validation_NPWPZipCode {
            get {
                return ResourceManager.GetString("Validation_NPWPZipCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Jangka Waktu Pembayaran.
        /// </summary>
        public static string Validation_PaymentTerm {
            get {
                return ResourceManager.GetString("Validation_PaymentTerm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Alamat Vendor.
        /// </summary>
        public static string Validation_VendorAddress {
            get {
                return ResourceManager.GetString("Validation_VendorAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Kota Vendor.
        /// </summary>
        public static string Validation_VendorCity {
            get {
                return ResourceManager.GetString("Validation_VendorCity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Fax Vendor.
        /// </summary>
        public static string Validation_VendorFax {
            get {
                return ResourceManager.GetString("Validation_VendorFax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Vendor Kecamatan.
        /// </summary>
        public static string Validation_VendorKecamatan {
            get {
                return ResourceManager.GetString("Validation_VendorKecamatan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Vendor Kelurahan.
        /// </summary>
        public static string Validation_VendorKelurahan {
            get {
                return ResourceManager.GetString("Validation_VendorKelurahan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Vendor Phone.
        /// </summary>
        public static string Validation_VendorPhone {
            get {
                return ResourceManager.GetString("Validation_VendorPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Jenis Vendor.
        /// </summary>
        public static string Validation_VendorTypeID {
            get {
                return ResourceManager.GetString("Validation_VendorTypeID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Vendor Website.
        /// </summary>
        public static string Validation_VendorWebsite {
            get {
                return ResourceManager.GetString("Validation_VendorWebsite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Kode Pos Vendor.
        /// </summary>
        public static string Validation_VendorZipCode {
            get {
                return ResourceManager.GetString("Validation_VendorZipCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alamat.
        /// </summary>
        public static string VendorAddress {
            get {
                return ResourceManager.GetString("VendorAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kota.
        /// </summary>
        public static string VendorCity {
            get {
                return ResourceManager.GetString("VendorCity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fax.
        /// </summary>
        public static string VendorFax {
            get {
                return ResourceManager.GetString("VendorFax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vendor ID.
        /// </summary>
        public static string VendorID {
            get {
                return ResourceManager.GetString("VendorID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kecamatan.
        /// </summary>
        public static string VendorKecamatan {
            get {
                return ResourceManager.GetString("VendorKecamatan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kelurahan.
        /// </summary>
        public static string VendorKelurahan {
            get {
                return ResourceManager.GetString("VendorKelurahan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nama Vendor.
        /// </summary>
        public static string VendorName {
            get {
                return ResourceManager.GetString("VendorName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nomot Telepon.
        /// </summary>
        public static string VendorPhone {
            get {
                return ResourceManager.GetString("VendorPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wajib diisi.
        /// </summary>
        public static string VendorRequired {
            get {
                return ResourceManager.GetString("VendorRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Jenis Vendor.
        /// </summary>
        public static string VendorTypeID {
            get {
                return ResourceManager.GetString("VendorTypeID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Website.
        /// </summary>
        public static string VendorWebsite {
            get {
                return ResourceManager.GetString("VendorWebsite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kode Pos.
        /// </summary>
        public static string VendorZipCode {
            get {
                return ResourceManager.GetString("VendorZipCode", resourceCulture);
            }
        }
    }
}
