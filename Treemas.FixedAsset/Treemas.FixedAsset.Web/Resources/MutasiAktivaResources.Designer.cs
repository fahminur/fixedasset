﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Treemas.FixedAsset.Web.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class MutasiAktivaResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal MutasiAktivaResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Treemas.FixedAsset.Web.Resources.MutasiAktivaResources", typeof(MutasiAktivaResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kode Aktiva.
        /// </summary>
        public static string AktivaID {
            get {
                return ResourceManager.GetString("AktivaID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Area.
        /// </summary>
        public static string DariAreaID {
            get {
                return ResourceManager.GetString("DariAreaID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cabang.
        /// </summary>
        public static string DariCabangID {
            get {
                return ResourceManager.GetString("DariCabangID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Departement.
        /// </summary>
        public static string DariDepartmentID {
            get {
                return ResourceManager.GetString("DariDepartmentID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kode Induk Aktiva.
        /// </summary>
        public static string DariKodeIndukAktivaID {
            get {
                return ResourceManager.GetString("DariKodeIndukAktivaID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kondisi.
        /// </summary>
        public static string DariKondisiID {
            get {
                return ResourceManager.GetString("DariKondisiID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lokasi.
        /// </summary>
        public static string DariLokasiID {
            get {
                return ResourceManager.GetString("DariLokasiID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pemakai.
        /// </summary>
        public static string DariPemakaiID {
            get {
                return ResourceManager.GetString("DariPemakaiID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kode Aktiva Baru.
        /// </summary>
        public static string KeAktivaID {
            get {
                return ResourceManager.GetString("KeAktivaID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Area.
        /// </summary>
        public static string KeAreaID {
            get {
                return ResourceManager.GetString("KeAreaID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cabang.
        /// </summary>
        public static string KeCabangID {
            get {
                return ResourceManager.GetString("KeCabangID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Departement.
        /// </summary>
        public static string KeDepartmentID {
            get {
                return ResourceManager.GetString("KeDepartmentID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kondisi.
        /// </summary>
        public static string KeKondisiID {
            get {
                return ResourceManager.GetString("KeKondisiID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lokasi.
        /// </summary>
        public static string KeLokasiID {
            get {
                return ResourceManager.GetString("KeLokasiID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pemakai.
        /// </summary>
        public static string KePemakaiID {
            get {
                return ResourceManager.GetString("KePemakaiID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Keterangan.
        /// </summary>
        public static string Keterangan {
            get {
                return ResourceManager.GetString("Keterangan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nama Aktiva.
        /// </summary>
        public static string NamaAktiva {
            get {
                return ResourceManager.GetString("NamaAktiva", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Referensi.
        /// </summary>
        public static string NoReferensi {
            get {
                return ResourceManager.GetString("NoReferensi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mutasi Aktiva.
        /// </summary>
        public static string PageTitle {
            get {
                return ResourceManager.GetString("PageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Periode.
        /// </summary>
        public static string Periode {
            get {
                return ResourceManager.GetString("Periode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        public static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tanggal Mutasi.
        /// </summary>
        public static string TanggalMutasi {
            get {
                return ResourceManager.GetString("TanggalMutasi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mutasi Aktiva Sudah Ada.
        /// </summary>
        public static string Validation_DuplicateData {
            get {
                return ResourceManager.GetString("Validation_DuplicateData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Kode Aktiva.
        /// </summary>
        public static string Validation_Id {
            get {
                return ResourceManager.GetString("Validation_Id", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Periode.
        /// </summary>
        public static string Validation_Periode {
            get {
                return ResourceManager.GetString("Validation_Periode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silahkan Masukkan Tanggal Mutasi.
        /// </summary>
        public static string Validation_TanggalMutasi {
            get {
                return ResourceManager.GetString("Validation_TanggalMutasi", resourceCulture);
            }
        }
    }
}
