﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Treemas.FixedAsset.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            string[] controllersString = { "AssetTidakSusut" };

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            foreach (string item in controllersString)
            {
                routes.MapRoute(
                    name: item,
                    url: item + "/{id}/{action}/{rid}",
                    defaults: new { controller = item, action = "Index", rid = UrlParameter.Optional },
                    constraints: new { id = @"^[a-zA-Z0-9._-]*$" }
                );
            }

            routes.MapRoute(
                name: "Default", 
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
