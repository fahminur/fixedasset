﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.FixedAsset.Models
{
    public class UserView
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string RegNo { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string OldPassword { get; set; }
        public string PasswordExpirationDate { get; set; }
        public string AccountValidityDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool InActiveDirectory { get; set; }
        public int SessionTimeout { get; set; }
        public int LockTimeout { get; set; }
        public int MaxConLogin { get; set; }
        public bool IsActive { get; set; }
        public bool IsPrinted { get; set; }
        public bool Approved { get; set; }
        public string Name { get; set; }

        public int passExp { get; set; }
        public int AccValidity { get; set; }
        public string Roles { get; set; }
    }
}