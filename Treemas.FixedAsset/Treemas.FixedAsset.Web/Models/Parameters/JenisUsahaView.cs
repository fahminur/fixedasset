﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class JenisUsahaView
    {

        public string JenisUsahaID { get; set; }
        public string NamaJenisUsaha { get; set; }
        //public string JenisHartaID { get; set; }
        //public string JenisHartaString { get; set; }
        public string JenisUsahaShortDesc { get; set; }
        public bool Status { get; set; }

    }
}
