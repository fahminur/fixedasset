﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class TerimaInvoiceNonAktivaView
    {
        public long InvoiceNonAktivaID { get; set; }
        public long TerimaInvoiceID { get; set; }
        public int NSeqNo { get; set; }
        public string NoRefference { get; set; }
        public string NamaBarangJasa { get; set; }
        public double Nilai { get; set; }
        public string CaraPembebanan { get; set; }
        public int JangkaWaktu { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int BebanPerBulan { get; set; }
        public string COABiaya { get; set; }
        public string COAPrepaid { get; set; }
        public string NoPO { get; set; }
        public string NoDO { get; set; }
        public string NoPenerimaan { get; set; }
        public string Keterangan { get; set; }
        public bool NonAktivaIsPPn { get; set; }
        public int NonAktivaPPn { get; set; }
        public bool NonAktivaIsPPh23 { get; set; }
        public int NonAktivaPPh23 { get; set; }
    }
}