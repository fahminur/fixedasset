﻿using System;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class GolonganPajakView
    {
        public string GolonganPajakID { get; set; }
        public string GolonganPajakDescription { get; set; }
        public double SLPercentage { get; set; }
        public double DDPercentage { get; set; }
        public int MasaManfaat { get; set; }
        public bool Status { get; set; }

    }
}
