﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class NamaHartaView
    {
        public string NamaHartaID { get; set; }
        public string NamaHartaDeskripsi { get; set; }
        public JenisHartaEnum JenisHartaID { get; set; }

        public string JenisHartaString { get; set; }
        public string JenisHartaValue
        {
            get
            {
                if (Enum.IsDefined(typeof(JenisHartaEnum), JenisHartaID))
                {
                    return JenisHartaID.ToString();
                }
                return "";
            }
        }
        public bool Status { get; set; }
    }
}