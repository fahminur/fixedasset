﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Web.Models
{
    public class PerawatanAktivaDetailView
    {
        public bool Selected { get; set; }
        public string AktivaID { get; set; }
        public string TanggalPerawatan { get; set; }
        public string TanggalPerawatanString { get; set; }
        public string NamaAktiva { get; set; }
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string NoInvoice { get; set; }
        public string COAPerawatan { get; set; }
        public string NilaiOriginal { get; set; }
        public string Biaya { get; set; }
        public string MasaGaransi { get; set; }
        public string MasaGaransiString { get; set; }
    }
}
