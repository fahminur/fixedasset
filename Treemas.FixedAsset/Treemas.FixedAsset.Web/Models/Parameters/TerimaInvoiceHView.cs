﻿using System;
using System.Collections.Generic;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class TerimaInvoiceHView
    {
        public TerimaInvoiceHView()
        {
            this.Detail = new List<TerimaInvoiceDView>();
            this.Aktiva = new List<TerimaInvoiceAktivaView>();
            this.NonAktiva = new List<TerimaInvoiceNonAktivaView>();
            this.Perawatan = new List<TerimaInvoicePerawatanView>();
        }
        public long Id { get; set; }
        public long VendorId { get; set; }
        public string VendorName { get; set; }
        public string NoInvoice { get; set; }
        public DateTime? TanggalInvoice { get; set; }
        public DateTime? TanggalJatuhTempo { get; set; }
        public DateTime? TanggalRencanaBayar { get; set; }
        public long VendorBankId { get; set; }
        public string BankId { get; set; }
        public string BankName { get; set; }
        public string BankAccountNo { get; set; }
        public string BankAccountName { get; set; }
        public string BankBranch { get; set; }
        public double TotalInvoice { get; set; }
        public double PPn { get; set; }
        public double PPh23 { get; set; }
        public double TotalBayar { get; set; }
        public string Catatan { get; set; }
        public bool Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public string TanggalInvoiceString { get; set; }
        public string TanggalJatuhTempoString { get; set; }
        public string CreateDateString { get; set; }

        public IList<TerimaInvoiceDView> Detail { get; set; }
        public IList<TerimaInvoiceAktivaView> Aktiva { get; set; }
        public IList<TerimaInvoiceNonAktivaView> NonAktiva { get; set; }
        public IList<TerimaInvoicePerawatanView> Perawatan { get; set; }
    }
}
