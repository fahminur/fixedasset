﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.FixedAsset.Web.Models
{
    public class PerawatanAktivaView
    {
        public string AktivaID { get; set; }
        public string NamaAktiva { get; set; }
        public string TanggalPerawatan { get; set; }
        public string TanggalPerawatanString { get; set; }
        public string CurrencyID { get; set; }
        public string CurrencyName { get; set; }
        public string NilaiOriginal { get; set; }
        public string Kurs { get; set; }
        public string Biaya { get; set; }
        public string Vendor { get; set; }
        public string NoReferensi { get; set; }
        public string Keterangan { get; set; }
        public string NoInvoice { get; set; }
        public string COA { get; set; }
        public string MasaGaransi { get; set; }
        public string MasaGaransiString { get; set; }
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string VendorInvoiceNo { get; set; }
        public string InvoiceDateFrom { get; set; }
        public string InvoiceDateTo { get; set; }

        public IList<PerawatanAktivaDetailView> PerawatanAktivaDetailList { get; set; }
    }
}