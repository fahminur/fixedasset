﻿using System;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class AssetAreaView 
    {
      
        public  string AssetAreaID { get; set; }
        public  string AssetAreaDescription { get; set; }
        public  string AssetAreaShortDesc { get; set; }
        public  string AssetBranchID { get; set; }
        public string AssetBranchString { get; set; }
        public  bool Status { get; set; } 
      
    }
}
