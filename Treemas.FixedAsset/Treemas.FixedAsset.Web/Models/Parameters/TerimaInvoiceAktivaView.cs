﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class TerimaInvoiceAktivaView
    {
        public long TerimaInvoiceAktivaID { get; set; }
        public long TerimaInvoiceID { get; set; }
        public int ASeqNo { get; set; }
        public string AktivaID { get; set; }
        public string AktivaName { get; set; }
        public string AktivaGroup { get; set; }
        public string AktivaSubGroup { get; set; }
        public string AktivaBranch { get; set; }
        public string AktivaLocation { get; set; }
        public string AktivaArea { get; set; }
        public string AktivaDepartment { get; set; }
        public string NoPO { get; set; }
        public string NoDO { get; set; }
        public string NoPenerimaan { get; set; }
        public DateTime? TanggalPerolehan { get; set; }
        public string MataUang { get; set; }
        public double NilaiOriginal { get; set; }
        public double Kurs { get; set; }
        public double HargaPerolehan { get; set; }
        public string Keterangan { get; set; }
        public string AktivaUOM { get; set; }
        public int AktivaQty { get; set; }
        public bool AktivaIsPPn { get; set; }
        public int AktivaPPn { get; set; }
        public bool AktivaIsPPh23 { get; set; }
        public int AktivaPPh23 { get; set; }
    }
}
