﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class FixedAssetMasterView
    {

        public string AktivaID { get; set; }
        public string AssetCode { get; set; }
        public string NamaAktiva { get; set; }
        public string GroupAssetID { get; set; }
        public string GroupAssetString { get; set; }
        public string SubGroupAssetID { get; set; }
        public string SubGroupAssetString { get; set; }
        public string NamaHartaID { get; set; }
        public string NamaHartaString { get; set; }
        public string COAAktivaID { get; set; }
        public string COAAkumulasiID { get; set; }
        public string COAPenyusutanID { get; set; }
        public string ParentAktivaID { get; set; }
        public string SiteID { get; set; }
        public string SiteString { get; set; }
        public string AssetLocationID { get; set; }
        public string Lokasi { get; set; }
        public string AssetLocationString { get; set; }
        public string AssetAreaID { get; set; }
        public string AssetAreaString { get; set; }
        public string AssetDepID { get; set; }
        public string AssetDepString { get; set; }
        public string AssetUserID { get; set; }
        public string AssetUserString { get; set; }
        public string AssetConditionID { get; set; }
        public string Merek { get; set; }
        public string Model { get; set; }
        public string SerialNo1 { get; set; }
        public string SerialNo2 { get; set; }
        public string MasaGaransi { get; set; }
        public string InsuranceComID { get; set; }
        public string InsuranceComBranchID { get; set; }
        public string KeteranganAktiva { get; set; }
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string NoPO { get; set; }
        public string NoDO { get; set; }
        public string NoGSRN { get; set; }
        public string NoInvoice { get; set; }
        public string NoPaymentVoucher { get; set; }
        public string TanggalPerolehan { get; set; }
        public string TanggalPerolehanString { get; set; }
        public string CurrencyID { get; set; }
        public string NilaiOriginal { get; set; }
        public string Kurs { get; set; }
        public string HargaPerolehan { get; set; }
        public string KeteranganPerolehan { get; set; }
        public string TanggalPerolehanKomersial { get; set; }
        public string HargaPerolehanKomersial { get; set; }
        public bool KalkulasiOtomatis { get; set; }
        public string TanggalCatat { get; set; }
        public string MetodeDepresiasiKomersial { get; set; }
        public string MasaManfaatKomersial { get; set; }
        public string PeriodeAwal { get; set; }
        public string PeriodeAkhir { get; set; }
        public string Tarif { get; set; }
        public string ResiduKomersial { get; set; }
        public string TanggalPenghapusan { get; set; }
        public string NomorReferensi { get; set; }
        public string HargaJual { get; set; }
        public bool Penyusutandihitung { get; set; }
        public string AlasanPengapusan { get; set; }
        public string TanggalPerolehanFiskal { get; set; }
        public string HargaPerolehanFiskal { get; set; }
        public bool KalkulasiOtomatisFiskal { get; set; }
        public string TanggalCatatFiskal { get; set; }
        public string GolonganPajakID { get; set; }
        public string GolonganPajakString { get; set; }
        public string MetodeDepresiasiFiskal { get; set; }
        public string MasaManfaatFiskal { get; set; }
        public string PeriodeMulaiFiskal { get; set; }
        public string PeriodeAkhirFiskal { get; set; }
        public string TarifFiskal { get; set; }
        public string Pembebanan { get; set; }
        public string ResiduFiskal { get; set; }
        public string TanggalPenghapusanFiskal { get; set; }
        public string NomorReferensiFiskal { get; set; }
        public string HargaJualFiskal { get; set; }
        public bool PenyusutanFiskal { get; set; }
        public string AlasanFiskal { get; set; }
        public string AkumulasiPenyusutanFiskal { get; set; }
        public string AkumulasiPenyusutanKomersial { get; set; }
        public string SisaNilaiBukuFiskal { get; set; }
        public string SisaNilaiBukuKomersial { get; set; }
        public string NoPol { get; set; }
        public string STNK { get; set; }
        public string BPKB { get; set; }
        public string NoRangka { get; set; }
        public string NoMesin { get; set; }
        public string NoPolis { get; set; }
        public string TglPolis { get; set; }
        public string NomorFaktur { get; set; }
        public string NomorNIKVIN { get; set; }
        public string FormANo { get; set; }
        public string TahunPembuatan { get; set; }
        public string TanggalTerimaAsset { get; set; }
        public string Status { get; set; }
        public string StatusString { get; set; }
        public bool IsOLS { get; set; }
        public string JenisAsset { get; set; }
        public AssetStatusEnum FAStatusEnum { get; set; }
        public string StatusValue
        {
            get
            {
                if (Enum.IsDefined(typeof(AssetStatusEnum), FAStatusEnum))
                {
                    return FAStatusEnum.ToString();
                }
                return "";
            }
        }
    }
}
