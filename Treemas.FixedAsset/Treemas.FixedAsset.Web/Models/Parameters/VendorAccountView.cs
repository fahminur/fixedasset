﻿using System;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class VendorAccountView
    {
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string VendorAccID { get; set; }
        public string VendorBankID { get; set; }
        public string VendorBankName { get; set; }
        public string VendorBankBranch { get; set; }
        public string VendorAccountNo { get; set; }
        public string VendorAccountName { get; set; }
        public int VendorBankBranchID { get; set; }
        public bool DefaultAccount { get; set; }
        public string UntukBayar { get; set; }
        public string TanggalEfektif { get; set; }
    }
}
