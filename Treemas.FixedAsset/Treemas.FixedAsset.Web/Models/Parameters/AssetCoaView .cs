﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{ 
    [Serializable]
    public class AssetCoaView
    {

        public string COAID { get; set; }
        public string COADescription { get; set; }       
         public CoaTypeEnum CoaType { get; set; }

        public string CoaTypeString { get; set; }
        public string CoaTypeValue
        {
            get
            {
                if (Enum.IsDefined(typeof(CoaTypeEnum), CoaType))
                {
                    return CoaType.ToString();
                }
                return "";
            }
        }
       
        public bool Status { get; set; }

    }
}
