﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class VendorBankView
    {
        public long Id { get; set; }
        public string BankMasterId { get; set; }
        public long VendorId { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string BankAccountNo { get; set; }
        public string BankAccountName { get; set; }
        public bool IsDefault { get; set; }

    }
}
