﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class TerimaInvoicePerawatanView
    {
        public long PerawatanAktivaID { get; set; }
        public long TerimaInvoiceID { get; set; }
        public int PSeqNo { get; set; }
        public string PAktivaID { get; set; }
        public string PAktivaName { get; set; }
        public string PAktivaGroup { get; set; }
        public string PAktivaSubGroup { get; set; }
        public string PAktivaBranch { get; set; }
        public string PAktivaLocation { get; set; }
        public string PAktivaArea { get; set; }
        public string PAktivaDepartment { get; set; }
        public string COABiaya { get; set; }
        public string NoPO { get; set; }
        public string NoSPK { get; set; }
        public DateTime? TanggalPerawatan { get; set; }
        public string MataUang { get; set; }
        public double NilaiOriginal { get; set; }
        public double Kurs { get; set; }
        public double NilaiPerawatan { get; set; }
        public string Keterangan { get; set; }
        public string PerawatanUOM { get; set; }
        public int PerawatanQty { get; set; }
        public bool PerawatanIsPPn { get; set; }
        public int PerawatanPPn { get; set; }
        public bool PerawatanIsPPh23 { get; set; }
        public int PerawatanPPh23 { get; set; }
    }
}