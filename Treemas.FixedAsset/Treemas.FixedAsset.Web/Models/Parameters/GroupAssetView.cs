﻿using System;
namespace Treemas.FixedAsset.Web.Models
{ 
    [Serializable]
    public class GroupAssetView
    {

        public string GroupAssetID { get; set; }
        public string GroupAssetDescription { get; set; }
        public string GroupAssetShortDesc { get; set; }
        public string CoaPerolehan { get; set; }
        public string CoaAkumulasi { get; set; }
        public string CoaBeban { get; set; }
        public string PaymentAllocation { get; set; }
        public bool Status { get; set; }

    }
}
