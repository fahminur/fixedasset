﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class NonAssetMasterView
    {
        public string NonAktivaID { get; set; }
        public string NamaNonAktiva { get; set; }
        public string GroupAssetID { get; set; }
        public string GroupAssetString { get; set; }
        public string SubGroupAssetID { get; set; }
        public string SubGroupAssetString { get; set; }
        public string SiteID { get; set; }
        public string SiteString { get; set; }
        public string Lokasi { get; set; }
        public string AssetAreaID { get; set; }
        public string AssetDepID { get; set; }
        public string AssetDepString { get; set; }
        public string AssetUserID { get; set; }
        public string AssetUserString { get; set; }
        public string AssetConditionID { get; set; }
        public string Merek { get; set; }
        public string Model { get; set; }
        public string SerialNo1 { get; set; }
        public string SerialNo2 { get; set; }
        public string MasaGaransi { get; set; }
        public string InsuranceComID { get; set; }
        public string InsuranceComBranchID { get; set; }
        public string KeteranganAktiva { get; set; }
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string NoPO { get; set; }
        public string NoDO { get; set; }
        public string NoGSRN { get; set; }
        public string NoInvoice { get; set; }
        public string TanggalTerimaAsset { get; set; }
        public string TanggalPerolehan { get; set; }
        public string NilaiOriginal { get; set; }
        public string Kurs { get; set; }
        public string HargaPerolehan { get; set; }
        public string KeteranganPerolehan { get; set; }
        public string NoPolis { get; set; }
        public string TglPolis { get; set; }
        public string CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Status { get; set; }
        public string StatusString { get; set; }
    }
}
