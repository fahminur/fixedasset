﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class AssetTidakSusutView
    {
        public long Id { get; set; }
        public long RowID { get; set; }
        public string AktivaId { get; set; }
        public string Tahun { get; set; }  
        public string Bulan { get; set; }
    }
}