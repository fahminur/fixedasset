﻿using System;
using Treemas.Procurement.Model;

namespace Treemas.Procurement.Web.Models
{
    [Serializable]
    public class VendorContactView
    {
        public long Id { get; set; }
        public long ContactID { get; set; }
        public string ContactName { get; set; }
        public string ContactJabatan { get; set; }
     //  public string ContactType { get; set; }
        public TypeContactEnum ContactType { get; set; }

        public string ContactTypeString { get; set; }
        public string ContactTypeValue
        {
            get
            {
                if (Enum.IsDefined(typeof(TypeContactEnum), ContactType))
                {
                    return ContactType.ToString();
                }
                return "";
            }
        }

        //  public string KirimDokumenVia { get; set; }
        //public DokumentEnum KirimDokumenVia { get; set; }

        public string KirimDokumenVia { get; set; }
        public string KirimDokumenViaString { get; set; }
        //public string KirimDokumenViaValue
        //{
        //    get
        //    {
        //        if (Enum.IsDefined(typeof(DokumentEnum), KirimDokumenVia))
        //        {
        //            return KirimDokumenVia.ToString();
        //        }
        //        return "";
        //    }
        //}
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Handphone1 { get; set; }
        public string Handphone2 { get; set; }
        public string Email { get; set; }
        public long VendorID { get; set; }

    }
}