﻿using System;
using System.Collections.Generic;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    public class RequestPaymentView
    {
        public RequestPaymentView()
        {
            
        }
        public string PlanDisburseDateFrom { get; set; }
        public string PlanDisburseDateTo { get; set; }
        public string VendorName { get; set; }
        public string VendorId { get; set; }
        public string VendorBankId { get; set; }
        public string VendorBankBranch { get; set; }
        public string VendorBankAccountNo { get; set; }
        public string VendorBankAccountName { get; set; }
        public string VendorInvoiceNo { get; set; }
        public string InvoiceNo { get; set; }
        public InvoiceStatusEnum InvoiceStatus { get; set; }
        public string StatusString { get; set; }
        public string StatusValue
        {
            get
            {
                if (Enum.IsDefined(typeof(InvoiceStatusEnum), InvoiceStatus))
                {
                    return InvoiceStatus.ToString();
                }
                return "";
            }
        }
        public string InvoiceDate { get; set; }
        public string InvoiceDateString { get; set; }
        public string PaymentOrderDate { get; set; }
        public string PaymentOrderDateString { get; set; }
        public string CaraBayar { get; set; }
        public string CaraBayarString { get; set; }
        public string Jumlah { get; set; }
        public string RefferenceNo { get; set; }
        public string Notes { get; set; }
        public string MemoNo { get; set; }
        public string PaymentAllocationId { get; set; }
        public string NoInvoice { get; set; }
        public string TanggalInvoice { get; set; }
        public string TanggalJatuhTempo { get; set; }
        public string TanggalRencanaBayar { get; set; }
        public string TanggalRencanaBayarString { get; set; }
        public string TotalInvoice { get; set; }
        public string PPn { get; set; }
        public string PPh23 { get; set; }
        public string TotalBayar { get; set; }
        public string Catatan { get; set; }
        public string Status { get; set; }
        public string CreateDate { get; set; }
        public string TanggalInvoiceString { get; set; }
        public string TanggalJatuhTempoString { get; set; }
        public string CreateDateString { get; set; }
        public string RequestDate { get; set; }
        public string RequestDateString { get; set; }
        public bool AllowEdit { get; set; }
        public bool AllowApprove { get; set; }
        //params
        public string VendorNames { get; set; }
        public string InvoiceNos { get; set; }

    }
}
