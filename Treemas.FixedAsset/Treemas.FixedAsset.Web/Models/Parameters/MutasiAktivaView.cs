﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.FixedAsset.Web.Models
{
    public class MutasiAktivaView
    {
        public string AktivaID { get; set; }
        public string NamaAktiva { get; set; }
        public string GroupAssetID { get; set; }
        public string SubGroupAssetID { get; set; }
        public string TanggalMutasi { get; set; }
        public string TanggalMutasiString { get; set; }
        public string Periode { get; set; }
        public string DariCabangID { get; set; }
        //public string DariLokasiID { get; set; }
        public string DariLokasi { get; set; }
        //public string DariAreaID { get; set; }
        public string DariDepartmentID { get; set; }
        public string DariPemakaiID { get; set; }
        public string DariKondisiID { get; set; }

        public string DariCabangName { get; set; }
        public string DariLokasiName { get; set; }
        public string DariAreaName { get; set; }
        public string DariDepartmentName { get; set; }
        public string DariPemakaiName { get; set; }
        public string DariKondisiName { get; set; }

        public string DariKodeIndukAktivaID { get; set; }

        public string KeCabangID { get; set; }
        public string KeLokasi { get; set; }
        //public string KeLokasiID { get; set; }
        //public string KeAreaID { get; set; }
        public string KeDepartmentID { get; set; }
        public string KePemakaiID { get; set; }
        public string KeKondisiID { get; set; }
        public string KeAktivaID { get; set; }
        public string KeCabangName { get; set; }
        public string KeLokasiName { get; set; }
        public string KeAreaName { get; set; }
        public string KeDepartmentName { get; set; }
        public string KePemakaiName { get; set; }
        public string KeKondisiName { get; set; }

        public string KeKodeIndukAktivaID { get; set; }
        public string NoReferensi { get; set; }
        public string Keterangan { get; set; }

    }
}