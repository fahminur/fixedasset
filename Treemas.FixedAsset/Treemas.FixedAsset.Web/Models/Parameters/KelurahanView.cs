﻿using System;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Web.Models
{
    [Serializable]
    public class KelurahanView
    {

        public string KelurahanID { get; set; }
        public string Kelurahan { get; set; }
        public string Kecamatan { get; set; }
        public string Kota { get; set; }
        public string ZipCode { get; set; }
        public string AreaCode { get; set; }
        public string Provinsi { get; set; }

    }
}