﻿using System;
using System.Collections.Generic;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class TerimaInvoiceDView
    {
        public long VendorId { get; set; }
        public string NoInvoice { get; set; }
        public int SeqNo { get; set; }
        public string JenisInvoice { get; set; }
        public string NamaBarangJasa { get; set; }
        public int Jumlah { get; set; }
        public string Satuan { get; set; }
        public string MataUang { get; set; }
        public double HargaSatuan { get; set; }
        public double Nilai { get; set; }
        public int PPn { get; set; }
        public int PPh23 { get; set; }
        public string AktivaIDNoReff { get; set; }
    }
}
