﻿using System;
namespace Treemas.FixedAsset.Web.Models
{ 
    [Serializable]
    public class SubGroupAssetView
    {

        public string SubGroupAssetID { get; set; }
        public string SubGroupAssetDescription { get; set; }
        public string GroupAssetID { get; set; }
        public string GroupAssetString { get; set; }
        public double MasaManfaat { get; set; }
        public string GolonganPajakID { get; set; }
        public string GolonganPajakString { get; set; }
        public bool Status { get; set; }

    }
}
