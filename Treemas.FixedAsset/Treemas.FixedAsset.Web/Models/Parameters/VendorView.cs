﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class VendorView
    {
        public string Id { get; set; }
        public string VendorId { get; set; }
        public string VendorName { get; set; }
        public string VendorGroupID { get; set; }
        public string VendorGroupName { get; set; }
        public string GroupCompanyID { get; set; }
        public string NPWP { get; set; }
        public string TDP { get; set; }
        public string SIUP { get; set; }
        public string VendorAddress { get; set; }
        public string VendorRT { get; set; }
        public string VendorRW { get; set; }
        public string VendorKelurahan { get; set; }
        public string VendorKecamatan { get; set; }
        public string VendorCity { get; set; }
        public string VendorZipCode { get; set; }
        public string VendorAreaPhone1 { get; set; }
        public string VendorPhone1 { get; set; }
        public string VendorAreaPhone2 { get; set; }
        public string VendorPhone2 { get; set; }
        public string VendorAreaFax { get; set; }
        public string VendorFax { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonJobTitle { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonHP { get; set; }
        public string VendorStatus { get; set; }
        public string VendorStartDate { get; set; }
        public string VendorBadStatus { get; set; }
        public string VendorLevelStatus { get; set; }
        public string LastCalculationDate { get; set; }
        public bool IsAutomotive { get; set; }
        public bool IsUrusBPKB { get; set; }
        public string VendorCategory { get; set; }
        public string VendorAssetStatus { get; set; }
        public string UsrUpd { get; set; }
        public string DtmUpd { get; set; }
        public string VendorIncentiveIDNew { get; set; }
        public string VendorIncentiveIDUsed { get; set; }
        public string VendorIncentiveIDRO { get; set; }
        public bool IsActive { get; set; }
        public string IsActiveString { get; set; }
        public string VendorPointId { get; set; }
        public bool PF { get; set; }
        public string NoPKS { get; set; }
        public bool PenerapanTVC { get; set; }
        public bool isPerseorangan { get; set; }
        public string PKSDate { get; set; }
        public string AssetTypeID { get; set; }
        public string AssetTypeDesc { get; set; }
        public string VendorInitialName { get; set; }
        public string AttachedFile { get; set; }
        public bool SKBP { get; set; }
        public string NPWPVendorAddress { get; set; }
        public string NPWPVendorRT { get; set; }
        public string NPWPVendorRW { get; set; }
        public string NPWPVendorKelurahan { get; set; }
        public string NPWPVendorKecamatan { get; set; }
        public string NPWPVendorCity { get; set; }
        public string NPWPVendorZipCode { get; set; }
        public string NPWPVendorAreaPhone1 { get; set; }
        public string NPWPVendorPhone1 { get; set; }
        public string NPWPVendorAreaPhone2 { get; set; }
        public string NPWPVendorPhone2 { get; set; }
        public string NPWPVendorAreaFax { get; set; }
        public string NPWPVendorFax { get; set; }

    }
}