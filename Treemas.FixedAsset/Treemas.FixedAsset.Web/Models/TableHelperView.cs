﻿using System;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class TableHelperView
    {
        public string Table { get; set; }

    }
}