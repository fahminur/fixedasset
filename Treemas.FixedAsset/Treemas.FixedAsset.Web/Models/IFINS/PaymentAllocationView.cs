﻿using System;
namespace Treemas.FixedAsset.Web.Models
{ 
    [Serializable]
    public class PaymentAllocationView
    {

        public string PaymentAllocationID { get; set; }
        public string Description { get; set; }
        public bool isPaymentRequest { get; set; }

    }
}
