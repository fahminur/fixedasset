﻿using System;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class GeneralReferenceView
    {
        public string DataId { get; set; }
        public string Description { get; set; }
        public string ShortDesc { get; set; }
        public bool Status { get; set; }
        public string Table { get; set; }

    }
}