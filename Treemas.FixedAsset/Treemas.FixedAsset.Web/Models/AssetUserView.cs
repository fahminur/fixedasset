﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.FixedAsset.Web.Models
{
    public class AssetUserView
    {
        public string AssetUserID { get; set; }
        public string AssetUserName { get; set; }
        public string AssetBranchID { get; set; }
        public string AssetDeptID { get; set; }
        public bool Status { get; set; }
        public string CreateDate { get; set; }
        public string CreateUser { get; set; }
        public string UpdateDate { get; set; }
        public string UpdateUser { get; set; }
    }
}