﻿using System;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class InvoiceServiceView
    {
        public int ID { get; set; }
        public string InvoiceNo { get; set; }
        public int Seq { get; set; }
        public string AktivaId { get; set; }
        public string AktivaName { get; set; }
        public string COABeban { get; set; }
        public string ExpensePA { get; set; }
        public string PONo { get; set; }
        public string WorkOrderNo { get; set; }
        public string ServiceDate { get; set; }
        public string ServiceGuaranteeDate { get; set; }
        public string ServiceAmount { get; set; }
        public string ItemAmount { get; set; }
        public string Notes { get; set; }
        public string PPnId { get; set; }
        public string PPnAmount { get; set; }
        public string PPhId { get; set; }
        public string PPhAmount { get; set; }
        public string MateraiID { get; set; }
        public string MateraiAmount { get; set; }
        public string TotalAmount { get; set; }
        public string OtherCost { get; set; }
        public string ExpedCost { get; set; }
    }
}
