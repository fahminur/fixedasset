﻿using System;
using System.Collections.Generic;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{ 
    [Serializable]
    public class InvoiceHView
    {
        public string GRNo { get; set; }
        public string InvoiceNo { get; set; }
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string VendorInvoiceNo { get; set; }
        public string DeliveryOrderNo { get; set; }
        public string PONo { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceDueDate { get; set; }
        public string InvoiceOrderDate { get; set; }
        //public string InvoiceDateString { get; set; }
        //public string InvoiceDueDateString { get; set; }
        //public string InvoiceOrderDateString { get; set; }
        public string Notes { get; set; }
        public string VendorBankId { get; set; }
        public string VendorBankBranch { get; set; }
        public string VendorAccountNo { get; set; }
        public string VendorAccountName { get; set; }
        public string InvoiceAmount { get; set; }
        public string Status { get; set; }
        public string InvoiceTypeID { get; set; }
        public string InvoiceTypeDesc { get; set; }
        public string NoFakturPajak { get; set; }
        public string TglFakturPajak { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public string Deviation { get; set; }
        public string DeviationNA { get; set; }
        public string DeviationS { get; set; }
        public string Mode { get; set; }
        public IList<InvoiceAktivaView> InvoiceAktivaList { get; set; }
        public IList<InvoiceNonAktivaView> InvoiceNonAktivaList { get; set; }
        public IList<InvoiceServiceView> InvoiceServiceList { get; set; }
        public bool AllowEdit { get; set; }
        public bool AllowApprove { get; set; }
        public bool AllowReject { get; set; }
        public bool AllowTaxIdEdit { get; set; }
        public InvoiceStatusEnum InvoiceStatus { get; set; }
        public string StatusString { get; set; }
        public string StatusValue
        {
            get
            {
                if (Enum.IsDefined(typeof(InvoiceStatusEnum), InvoiceStatus))
                {
                    return InvoiceStatus.ToString();
                }
                return "";
            }
        }

        //params
        public string VendorNames { get; set; }
        public string InvoiceNos { get; set; }
    }
}
