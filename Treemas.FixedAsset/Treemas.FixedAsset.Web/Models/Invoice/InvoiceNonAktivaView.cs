﻿using System;

namespace Treemas.FixedAsset.Web.Models
{ 
    [Serializable]
    public class InvoiceNonAktivaView
    {
        
        public int ID { get; set; }
        public string InvoiceNo { get; set; }
        public int Seq { get; set; }
        public string NonAktivaId { get; set; }
        public string NonAktivaName { get; set; }
        public string RefferenceNo { get; set; }
        public string GroupAssetID { get; set; }
        public string ItemName { get; set; }
        public string Price { get; set; }
        //public int Qty { get; set; }
        //public string TotalPrice { get; set; }
        public string ExpenseType { get; set; }
        public int TimePeriod { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string MonthlyExpense { get; set; }
        public string ExpensePA { get; set; }
        public string PrepaidPA { get; set; }
        public string COABeban { get; set; }
        public string PONo { get; set; }
        public string DeliveryOrderNo { get; set; }
        public string GRNo { get; set; }
        public string Notes { get; set; }
        public int DepreciatedTime { get; set; }
        public string PPnId { get; set; }
        public string PPnAmount { get; set; }
        public string PPhId { get; set; }
        public string PPhAmount { get; set; }
        public int EndDateChanged { get; set; }
        public string TotalAmount { get; set; }
        public string OtherCost { get; set; }
        public string ExpedCost { get; set; }
    }
}
