﻿using System;

namespace Treemas.FixedAsset.Web.Models
{ 
    [Serializable]
    public class InvoiceAktivaView
    {
        public int ID { get; set; }
        public string InvoiceNo { get; set; }
        public int Seq { get; set; }
        public string AssetSubGroupId { get; set; }
        public string StartingAktivaId { get; set; }
        public string AktivaName { get; set; }
        public string COABeban { get; set; }
        public string PurchaseOrderNo { get; set; }
        public string DeliveryOrderNo { get; set; }
        public string GRNo { get; set; }
        public string TanggalPerolehan { get; set; }
        public string TanggalPerolehanString { get; set; }
        public double Quantity { get; set; }
        public string Price { get; set; }
        public string TotalPrice { get; set; }
        public string PPnId { get; set; }
        public string PPnAmount { get; set; }
        public string PPhId { get; set; }
        public string PPhAmount { get; set; }
        public string Notes { get; set; }
        public string TotalAmount { get; set; }
        public string OtherCost { get; set; }
        public string ExpedCost { get; set; }

        public DateTime? CreateDate { get; set; }
        public string CreateUser { get; set; }

    }
}
