﻿using System;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Web.Models
{
    public class PenghapusanAktivaHeaderView
    {
        public string DeleteNo { get; set; }
        public string DeleteDate { get; set; }
        public string InternalMemoNo { get; set; }
        public string CreateUser { get; set; }
        public string CreateDate { get; set; }
        public string UpdateUser { get; set; }
        public string UpdateDate { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public string Mode { get; set; }
        public bool AllowEdit { get; set; }
        public bool AllowApprove { get; set; }
        public bool AllowReject { get; set; }
        public PenjualanStatusEnum PenghapusanStatus { get; set; }
        public IList<PenghapusanAktivaDetailView> PenghapusanAktivaDetailList { get; set; }

        public string StatusString { get; set; }
        public string StatusValue
        {
            get
            {
                if (Enum.IsDefined(typeof(PenjualanStatusEnum), PenghapusanStatus))
                {
                    return PenghapusanStatus.ToString();
                }
                return "";
            }
        }
    }
}
