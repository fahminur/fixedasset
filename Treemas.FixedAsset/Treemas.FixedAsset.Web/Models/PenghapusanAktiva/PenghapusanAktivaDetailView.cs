﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    public class PenghapusanAktivaDetailView
    {
        public string DeleteNo { get; set; }
        public int Seq { get; set; }
        public string AktivaID { get; set; }
        public string AktivaName { get; set; }
        public string SisaNilaiBukuKomersial { get; set; }
        public string DeleteReason { get; set; }
        public string Notes { get; set; }
    }
}
