﻿
using System;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class AssetView
    {
        public string AssetCode { get; set; }
        public string AssetName { get; set; }
        public string AssetTypeID { get; set; }

    }
}
