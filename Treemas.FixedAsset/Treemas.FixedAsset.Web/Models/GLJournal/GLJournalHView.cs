﻿using System;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class GLJournalHView
    {
        public string CompanyID { get; set; }
        public string BranchID { get; set; }
        public string BranchName { get; set; }
        public string Tr_Nomor { get; set; }
        public string PeriodYear { get; set; }
        public string PeriodMonth { get; set; }
        public string TransactionID { get; set; }
        public string tr_Date1 { get; set; }
        public string Tr_Date { get; set; }
        public string Reff_No { get; set; }
        public string Reff_Link { get; set; }
        public string Reff_Date { get; set; }
        public string Tr_Desc { get; set; }
        public string JournalAmount { get; set; }
        public string BatchID { get; set; }
        public string SubSystem { get; set; }
        public string Status_Tr { get; set; }
        public bool IsActive { get; set; }
        public string Flag { get; set; }
        public string UsrUpd { get; set; }
        public string DtmUpd { get; set; }
        public string Status { get; set; }
        public bool IsValid { get; set; }
        public int DepresiasiTenor { get; set; }
        public int SisaTenor { get; set; }
        public string PrintDate { get; set; }
        public string PrintBy { get; set; }
        public bool IsPrint { get; set; }
        public string Jenis_Reff { get; set; }
        public string PostingDate { get; set; }
        public string PostingBy { get; set; }
        public string LastRevisiPosting { get; set; }
        public Int32 CountRevisiPosting { get; set; }
        public IList<GLJournalDView> GLJournalDtlList { get; set; }
    }                  
}
