﻿using System;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class GLJournalDView
    {
        public string CompanyID { get; set; }            //CHAR (3) NOT NULL,
        public string BranchID { get; set; }            //CHAR (3) NOT NULL,
        public string Tr_Nomor { get; set; }            //CHAR (20) NOT NULL,
        public int SequenceNo { get; set; }            //INT NOT NULL,
        public string CoaCo { get; set; }            //CHAR (3) NOT NULL,
        public string CoaBranch { get; set; }            //CHAR (3) NOT NULL,
        public string CoaId { get; set; }            //VARCHAR(25) NOT NULL,
        public string TransactionID { get; set; }            //VARCHAR (8) NOT NULL,
        public string Tr_Desc { get; set; }            //VARCHAR (250) NULL,
        public string Post { get; set; }            //CHAR (1) NOT NULL,
        public string PostDesc { get; set; }
        public string Amount { get; set; }            //NUMERIC(18) NULL,
        public string CoaId_X { get; set; }            //VARCHAR(25) NULL,
        public string PaymentAllocationId { get; set; }            //CHAR (10) NULL,
        public string ProductId { get; set; }            //CHAR (10) NULL,
        public string UsrUpd { get; set; }            //VARCHAR(50) NOT NULL,
        public DateTime DtmUpd { get; set; }            //DATETIME NOT NULL,
    }
}
