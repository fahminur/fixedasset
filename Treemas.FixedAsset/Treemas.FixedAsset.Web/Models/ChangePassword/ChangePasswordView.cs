﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.FixedAsset.Models
{
    public class ChangePasswordView
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string OldPassword { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}