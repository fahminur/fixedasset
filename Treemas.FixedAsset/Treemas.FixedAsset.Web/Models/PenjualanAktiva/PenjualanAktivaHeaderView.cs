﻿using System;
using System.Collections.Generic;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    public class PenjualanAktivaHeaderView
    {
        public string SellingNo { get; set; }
        public string SellingDate { get; set; }
        public string SellingDateString { get; set; }
        public string ReferenceNo { get; set; }
        public string Notes { get; set; }
        public string InternalMemoNo { get; set; }
        public string Status { get; set; }
        public bool AllowEdit { get; set; }
        public bool AllowApprove { get; set; }
        public bool AllowReject { get; set; }
        public PenjualanStatusEnum PenjualanStatus { get; set; }
        public string StatusString { get; set; }
        public string StatusValue
        {
            get
            {
                if (Enum.IsDefined(typeof(PenjualanStatusEnum), PenjualanStatus))
                {
                    return PenjualanStatus.ToString();
                }
                return "";
            }
        }
        public IList<PenjualanAktivaDetailView> PenjualanDetailList { get; set; }

        public string Mode { get; set; }

    }
}