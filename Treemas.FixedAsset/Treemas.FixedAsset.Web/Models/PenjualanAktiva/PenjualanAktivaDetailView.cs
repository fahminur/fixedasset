﻿using System;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    public class PenjualanAktivaDetailView
    {
        public string SellingNo { get; set; }
        public int Seq { get; set; }
        public string AktivaID { get; set; }
        public string AktivaName { get; set; }
        public string Notes { get; set; }
        public string SellingPrice { get; set; }
        public string NilaiAktiva { get; set; }
        public string Buyer { get; set; }
        public string StopDepreciationDate { get; set; }
        public string StopDepreciationDateString { get; set; }
        public int DepreciationSeqNo { get; set; }
    }
}