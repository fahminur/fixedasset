﻿using System;
using System.Collections.Generic;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    public class PenerimaanBarangAktivaView
    {
        public string GRNo { get; set; }
        public int Seq { get; set; }
        public string AktivaId { get; set; }
        public string AktivaName { get; set; }
        public string Price { get; set; }
        public int Quantity { get; set; }
        public string TotalPrice { get; set; }
        public string Notes { get; set; }
    }
}
