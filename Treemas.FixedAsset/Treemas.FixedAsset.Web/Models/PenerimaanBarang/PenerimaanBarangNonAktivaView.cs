﻿using System;
using System.Collections.Generic;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    public class PenerimaanBarangNonAktivaView
    {
        public string GRNo { get; set; }
        public int Seq { get; set; }

        public string NonAktivaId { get; set; }
        public string NonAktivaName { get; set; }
        public string RefferenceNo { get; set; }
        public string GroupAssetID { get; set; }
        public string ItemName { get; set; }
        public string Price { get; set; }
        public int Quantity { get; set; }
        public string TotalPrice { get; set; }
        public string Notes { get; set; }
    }
}
