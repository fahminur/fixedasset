﻿using System;
using System.Collections.Generic;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    public class PenerimaanBarangServiceView
    {
        public string GRNo { get; set; }
        public int Seq { get; set; }
        public string AktivaId { get; set; }
        public string AktivaName { get; set; }
        public string Amount { get; set; }
        public string Notes { get; set; }
    }
}
