﻿using System;
using System.Collections.Generic;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Web.Models
{
    [Serializable]
    public class PenerimaanBarangHdrView
    {
        public string GRNo { get; set; }
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string DeliveryOrderNo { get; set; }
        public string Notes { get; set; }
        public string GRType { get; set; }
        public string GRTypeDesc { get; set; }
        public string Status { get; set; }
        public string DeliveryOrderDate { get; set; }
        public string GRDate { get; set; }
        public string GRAmount { get; set; }

        public IList<PenerimaanBarangAktivaView> PenerimaanAktivaList { get; set; }
        public IList<PenerimaanBarangNonAktivaView> PenerimaanNonAktivaList { get; set; }
        public IList<PenerimaanBarangServiceView> PenerimaanServiceList { get; set; }
        public bool AllowEdit { get; set; }
        public bool AllowApprove { get; set; }
        public bool AllowReject { get; set; }
        public PenerimaanStatusEnum PenerimaanStatus { get; set; }
        public string StatusString { get; set; }
        public string StatusValue
        {
            get
            {
                if (Enum.IsDefined(typeof(PenerimaanStatusEnum), PenerimaanStatus))
                {
                    return PenerimaanStatus.ToString();
                }
                return "";
            }
        }

        public DateTime? CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public string Mode { get; set; }
    }
}
