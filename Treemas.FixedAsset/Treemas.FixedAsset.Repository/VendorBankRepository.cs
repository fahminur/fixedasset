﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class VendorBankRepository : RepositoryController<VendorBank>, IVendorBankRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public VendorBankRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public VendorBank getVendorBank(long id)
        {
            return transact(() => session.QueryOver<VendorBank>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public bool IsDuplicate(string bankmasterid, string accountno)
        {
            int rowCount = transact(() => session.QueryOver<VendorBank>()
                                                .Where(f => f.BankMasterId == bankmasterid && f.BankAccountNo == accountno).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public IEnumerable<VendorBank> getBankByVendor(long vendorid)
        {
            Specification<VendorBank> specification;
            specification = new AdHocSpecification<VendorBank>(s => s.VendorId == vendorid);
            return FindAll(specification);
        }
        public PagedResult<VendorBank> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, long id)
        {
            PagedResult<VendorBank> paged = new PagedResult<VendorBank>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<VendorBank>()
                                                     .Where(x => x.VendorId == id)
                                                     .OrderByDescending(GetOrderByExpression<VendorBank>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<VendorBank>()
                                                     .Where(x => x.VendorId == id)
                                                     .OrderBy(GetOrderByExpression<VendorBank>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<VendorBank> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, long id)
        {
            Specification<VendorBank> specification;

            switch (searchColumn)
            {
                case "Id":
                    specification = new AdHocSpecification<VendorBank>(s => s.Id.Equals(searchValue) && s.VendorId == id);
                    break;
                case "ContactName":
                    specification = new AdHocSpecification<VendorBank>(s => s.BankAccountNo.Contains(searchValue) && s.VendorId == id);
                    break;
                case "ContactJabatan":
                    specification = new AdHocSpecification<VendorBank>(s => s.BankName.Contains(searchValue) && s.VendorId == id);
                    break;
                case "ContactType":
                    specification = new AdHocSpecification<VendorBank>(s => s.BankBranch.Equals(searchValue) && s.VendorId == id);
                    break;
                default:
                    specification = new AdHocSpecification<VendorBank>(s => s.Id.Equals(searchValue) && s.VendorId == id);
                    break;
            }

            PagedResult<VendorBank> paged = new PagedResult<VendorBank>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<VendorBank>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<VendorBank>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<VendorBank>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<VendorBank>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
