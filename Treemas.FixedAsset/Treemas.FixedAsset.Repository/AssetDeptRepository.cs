﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class AssetDeptRepository : RepositoryControllerString<AssetDept>, IAssetDeptRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AssetDeptRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public AssetDept getAssetDept(string id)
        {
            return transact(() => session.QueryOver<AssetDept>()
                                                .Where(f => f.AssetDeptID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string senddocid)
        {
            int rowCount = transact(() => session.QueryOver<AssetDept>()
                                                .Where(f => f.AssetDeptID == senddocid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public IEnumerable<AssetDept> findActive()
        {
            Specification<AssetDept> specification;
            specification = new AdHocSpecification<AssetDept>(s => s.Status == true);

            return FindAll(specification);
        }
        public PagedResult<AssetDept> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AssetDept> paged = new PagedResult<AssetDept>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AssetDept>()
                                                     .OrderByDescending(GetOrderByExpression<AssetDept>("Status"))
                                                     .ThenBy(GetOrderByExpression<AssetDept>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AssetDept>()
                                                     .OrderByDescending(GetOrderByExpression<AssetDept>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<AssetDept>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<AssetDept> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<AssetDept> specification;

            switch (searchColumn)
            {
                case "DataId":
                    specification = new AdHocSpecification<AssetDept>(s => s.AssetDeptID.Equals(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<AssetDept>(s => s.AssetDeptDescription.Contains(searchValue));
                    break;
                case "ShortDesc":
                    specification = new AdHocSpecification<AssetDept>(s => s.AssetDeptShortDesc.Contains(searchValue));
                    break;
              
                default:
                    specification = new AdHocSpecification<AssetDept>(s => s.AssetDeptID.Equals(searchValue));
                    break;
            }

            PagedResult<AssetDept> paged = new PagedResult<AssetDept>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AssetDept>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetDept>("Status"))
                                 .ThenBy(GetOrderByExpression<AssetDept>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AssetDept>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetDept>("Status"))
                                 .ThenByDescending(GetOrderByExpression<AssetDept>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
