﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class PerawatanAktivaRepository : RepositoryControllerString<PerawatanAktiva>, IPerawatanAktivaRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public PerawatanAktivaRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        //public IEnumerable<PerawatanAktiva> findActive()
        //{
        //    Specification<PerawatanAktiva> specification;
        //    specification = new AdHocSpecification<PerawatanAktiva>(s => s.Status == true);

        //    return FindAll(specification);
        //}

        public PerawatanAktiva getPerawatanAktiva(string id)
        {
            return transact(() => session.QueryOver<PerawatanAktiva>()
                                                .Where(f => f.AktivaID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string coaclassid)
        {
            int rowCount = transact(() => session.QueryOver<PerawatanAktiva>()
                                                .Where(f => f.AktivaID == coaclassid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<PerawatanAktiva> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, PerawatanAktivaFilter filter)
        {
            SimpleExpression aktivaID = Restrictions.Like("AktivaID", filter.AktivaID.Trim(), MatchMode.Anywhere);
            SimpleExpression vendor = Restrictions.Like("VendorID", filter.VendorID.Trim(), MatchMode.Anywhere);
            SimpleExpression noReferensi = Restrictions.Like("NoReferensi", filter.NoReferensi.Trim(), MatchMode.Anywhere);
            SimpleExpression tanggalPerawatanFrom = Restrictions.Ge("TanggalPerawatan", filter.TanggalPerawatanFrom);
            SimpleExpression tanggalPerawatanTo = Restrictions.Le("TanggalPerawatan", filter.TanggalPerawatanTo);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.AktivaID.Trim().IsNullOrEmpty()) conjuction.Add(aktivaID);
            if (!filter.VendorID.Trim().IsNullOrEmpty()) conjuction.Add(vendor);
            if (!filter.NoReferensi.Trim().IsNullOrEmpty()) conjuction.Add(noReferensi);
            if (!filter.TanggalPerawatanFrom.IsNull()) conjuction.Add(tanggalPerawatanFrom);
            if (!filter.TanggalPerawatanTo.IsNull()) conjuction.Add(tanggalPerawatanTo);

            PagedResult<PerawatanAktiva> paged = new PagedResult<PerawatanAktiva>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<PerawatanAktiva>()
                                 .Where(conjuction)
                                 //.OrderBy(Projections.Property("Status")).Desc
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<PerawatanAktiva>()
                                 .Where(conjuction)
                                 //.OrderBy(Projections.Property("Status")).Desc
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                          session.QueryOver<PerawatanAktiva>()
                                 .Where(conjuction).RowCount());

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
