﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using NHibernate.Criterion;

namespace Treemas.FixedAsset.Repository
{
    public class MetodeDepresiasiRepository : RepositoryControllerString<MetodeDepresiasi>, IMetodeDepresiasiRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public MetodeDepresiasiRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public MetodeDepresiasi GetMetodeDepresiasi(string MetodeDepresiasiID)
        {
            return transact(() => session.QueryOver<MetodeDepresiasi>().Where(f => f.MetodeDepresiasiID == MetodeDepresiasiID).SingleOrDefault());
        }
        public bool IsDuplicate(string MetodeDepresiasiID)
        {
            int rowCount = transact(() => session.QueryOver<MetodeDepresiasi>()
                                                .Where(f => f.MetodeDepresiasiID == MetodeDepresiasiID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<MetodeDepresiasi> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<MetodeDepresiasi> paged = new PagedResult<MetodeDepresiasi>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<MetodeDepresiasi>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<MetodeDepresiasi>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<MetodeDepresiasi> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {

            Conjunction conjuction = Restrictions.Conjunction();

            PagedResult<MetodeDepresiasi> paged = new PagedResult<MetodeDepresiasi>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<MetodeDepresiasi>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<MetodeDepresiasi>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<MetodeDepresiasi>()
                                .Where(conjuction).RowCount());
            return paged;
        }
    }
}
