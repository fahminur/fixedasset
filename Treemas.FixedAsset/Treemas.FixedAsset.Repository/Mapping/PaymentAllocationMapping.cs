﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;


namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PaymentAllocationMapping : ClassMapping<PaymentAllocation>
    {
        public PaymentAllocationMapping()
        {
            this.Table("PaymentAllocation");
            Id<string>(x => x.PaymentAllocationID,
               map => { map.Column("PaymentAllocationID"); });

            Property<string>(x => x.Description, map => { map.Column("Description"); });
            Property<string>(x => x.DescriptionToPrint, map => { map.Column("DescriptionToPrint"); });
            Property<bool>(x => x.IsAgreement, map => { map.Column("IsAgreement"); });
            Property<bool>(x => x.IsSystem, map => { map.Column("IsSystem"); });
            Property<bool>(x => x.IsScheme, map => { map.Column("IsScheme"); });
            Property<bool>(x => x.IsPettyCash, map => { map.Column("IsPettyCash"); });
            Property<bool>(x => x.IsPaymentReceive, map => { map.Column("IsPaymentReceive"); });
            Property<bool>(x => x.IsHOTransaction, map => { map.Column("IsHOTransaction"); });
            Property<bool>(x => x.IsHeader, map => { map.Column("IsHeader"); });
            Property<bool>(x => x.isPaymentRequest, map => { map.Column("isPaymentRequest"); });
            Property<string>(x => x.COA, map => { map.Column("COA"); });
            Property<string>(x => x.Group_Coa, map => { map.Column("Group_Coa"); });
            Property<string>(x => x.AddFree1, map => { map.Column("AddFree1"); });
            Property<string>(x => x.AddFree2, map => { map.Column("AddFree2"); });

            Property<bool>(x => x.IsActive, map => { map.Column("IsActive"); });
            Property<DateTime?>(x => x.DtmUpd, map => { map.Column("DtmUpd"); });
            Property<string>(x => x.UsrUpd, map => { map.Column("UsrUpd"); });
        }
    }
}
