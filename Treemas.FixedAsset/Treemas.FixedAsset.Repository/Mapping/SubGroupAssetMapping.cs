﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class SubGroupAssetMapping : ClassMapping<SubGroupAsset>
    {
        public SubGroupAssetMapping()
        {
            this.Table("Asset_ref_SubGroupAsset");
            Id<string>(x => x.SubGroupAssetID,
               map => { map.Column("SubGroupAssetID"); });

            Property<string>(x => x.SubGroupAssetDescription,
               map => { map.Column("SubGroupAssetDescription");});

            Property<string>(x => x.GroupAssetID,
               map => { map.Column("GroupAssetID"); });

            Property<double>(x => x.MasaManfaat,
               map => { map.Column("MasaManfaat"); });

            Property<string>(x => x.GolonganPajakID,
              map => { map.Column("GolonganPajakID"); });


            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
