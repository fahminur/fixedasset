﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class AssetUserMapping : ClassMapping<AssetUser>
    {
        public AssetUserMapping()
        {
            this.Table("Asset_ref_AssetUser");
            Id<string>(x => x.AssetUserID,
               map => { map.Column("AssetUserID"); });

            Property<string>(x => x.AssetUserName,
               map => { map.Column("AssetUserName"); ; });

            Property<string>(x => x.AssetBranchID,
               map => { map.Column("AssetBranchID"); });

            Property<string>(x => x.AssetDeptID,
               map => { map.Column("AssetDeptID"); });

            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
