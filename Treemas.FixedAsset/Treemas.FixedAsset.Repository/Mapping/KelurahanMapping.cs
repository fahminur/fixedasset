﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class KelurahanMapping : ClassMapping<Kelurahan>
    {
        public KelurahanMapping()
        {
            this.Table("Foundation_Kelurahan");

            Id<string>(x => x.ID, map => { map.Column("ID"); });
            Property<string>(x => x.ID, map => { map.Column("ID"); });
            Property<string>(x => x.NamaKelurahan, map => { map.Column("Kelurahan"); });
            Property<string>(x => x.Kecamatan, map => { map.Column("Kecamatan"); });
            Property<string>(x => x.Kota, map => { map.Column("Kota"); });
            Property<string>(x => x.ZipCode, map => { map.Column("ZipCode"); });
            Property<string>(x => x.AreaCode, map => { map.Column("AreaCode"); });
            Property<string>(x => x.Provinsi, map => { map.Column("Provinsi"); });
            Property<DateTime>(x => x.CreateDate, map => { map.Column("CreateDate"); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); });
            Property<DateTime>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
        }
    }
}
