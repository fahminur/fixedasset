﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class DepresiasiKomersialMapping : ClassMapping<DepresiasiKomersial>
    {
        public DepresiasiKomersialMapping()
        {
            this.Table("Asset_DepresiasiKomersial");
            Id<string>(x => x.AktivaId,
                map =>
                {
                    map.Column("AktivaId");
                });

            Property<string>(x => x.AktivaId, map => { map.Column("AktivaId"); });
            Property<string>(x => x.Tahun, map => { map.Column("Tahun"); });
            Property<string>(x => x.Bulan, map => { map.Column("Bulan"); });
            Property<int>(x => x.SeqNo, map => { map.Column("SeqNo"); });
            Property<int>(x => x.Nilai, map => { map.Column("Nilai"); });
            Property<int>(x => x.Saldo, map => { map.Column("Saldo"); });
            Property<int>(x => x.AkumulasiPenyusutan, map => { map.Column("AkumulasiPenyusutan"); });
            Property<string>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime>(x => x.StatusDate, map => { map.Column("StatusDate"); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); });
            Property<DateTime>(x => x.CreateDate, map => { map.Column("CreateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
        }
    }
}
