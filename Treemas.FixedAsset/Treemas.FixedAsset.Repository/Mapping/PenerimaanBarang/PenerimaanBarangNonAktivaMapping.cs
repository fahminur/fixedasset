﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PenerimaanBarangNonAktivaMapping : ClassMapping<PenerimaanBarangNonAktiva>
    {
        public PenerimaanBarangNonAktivaMapping()
        {
            this.Table("Asset_PenerimaanBarangNonAktiva");
            Id<int>(x => x.Seq, map => { map.Column("Seq"); });
            Property<string>(x => x.GRNo, map => { map.Column("GRNo"); });
            Property<int>(x => x.Seq, map => { map.Column("Seq"); });
            Property<string>(x => x.NonAktivaId, map => { map.Column("NonAktivaId"); });
            Property<string>(x => x.RefferenceNo, map => { map.Column("RefferenceNo"); });
            Property<string>(x => x.GroupAssetID, map => { map.Column("GroupAssetID"); });
            Property<string>(x => x.ItemName, map => { map.Column("ItemName"); });
            Property<double>(x => x.Price, map => { map.Column("Price"); });
            Property<int>(x => x.Quantity, map => { map.Column("Quantity"); });
            Property<double>(x => x.TotalPrice, map => { map.Column("TotalPrice"); });
            Property<string>(x => x.Notes, map => { map.Column("Notes"); });
        }
    }
}
