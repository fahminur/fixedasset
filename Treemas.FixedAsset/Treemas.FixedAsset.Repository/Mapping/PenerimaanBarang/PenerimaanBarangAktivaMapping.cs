﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;


namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PenerimaanBarangAktivaMapping : ClassMapping<PenerimaanBarangAktiva>
    {
        public PenerimaanBarangAktivaMapping()
        {
            this.Table("Asset_PenerimaanBarangAktiva");
            Id<int>(x => x.Seq, map => { map.Column("Seq"); });

            Property<string>(x => x.GRNo, map => { map.Column("GRNo"); });
            Property<int>(x => x.Seq, map => { map.Column("Seq"); });
            Property<string>(x => x.AktivaId, map => { map.Column("AktivaId"); });
            Property<double>(x => x.Price, map => { map.Column("Price"); });
            Property<int>(x => x.Quantity, map => { map.Column("Quantity"); });
            Property<double>(x => x.TotalPrice, map => { map.Column("TotalPrice"); });
            Property<string>(x => x.Notes, map => { map.Column("Notes"); });
        }
    }
}
