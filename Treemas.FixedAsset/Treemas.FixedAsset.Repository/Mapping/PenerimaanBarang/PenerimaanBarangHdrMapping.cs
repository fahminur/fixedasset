﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PenerimaanBarangHdrMapping : ClassMapping<PenerimaanBarangHdr>
    {
        public PenerimaanBarangHdrMapping()
        {
            this.Table("Asset_PenerimaanBarangHeader");
            Id<string>(x => x.GRNo, map => { map.Column("GRNo"); });

            //Property<string>(x => x.GRNo, map => { map.Column("GRNo"); });
            Property<DateTime?>(x => x.GRDate, map => { map.Column("GRDate"); });
            Property<string>(x => x.VendorID, map => { map.Column("VendorID"); });
            Property<string>(x => x.DeliveryOrderNo, map => { map.Column("DeliveryOrderNo"); });
            Property<DateTime?>(x => x.DeliveryOrderDate, map => { map.Column("DeliveryOrderDate"); });
            Property<string>(x => x.GRType, map => { map.Column("GRType"); });
            Property<string>(x => x.Notes, map => { map.Column("Notes"); });
            Property<string>(x => x.Status, map => { map.Column("Status"); });
            Property<double>(x => x.GRAmount, map => { map.Column("GRAmount"); });
            
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });

        }
    }
}
