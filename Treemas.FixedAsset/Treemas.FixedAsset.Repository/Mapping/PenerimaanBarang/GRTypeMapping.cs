﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;


namespace Treemas.FixedAsset.Repository.Mapping
{
    public class GRTypeMapping : ClassMapping<GRType>
    {
        public GRTypeMapping()
        {
            this.Table("Asset_Ref_GRType");
            Id<string>(x => x.GRTypeID, map => { map.Column("GRTypeID"); });
            Property<string>(x => x.GRTypeName, map => { map.Column("GRTypeName"); });
            Property<string>(x => x.GRTypeInitial, map => { map.Column("GRTypeInitial"); });
            Property<string>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
