﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PenerimaanBarangServiceMapping:ClassMapping<PenerimaanBarangService>
    {
        public PenerimaanBarangServiceMapping()
        {
            this.Table("Asset_PenerimaanBarangService");
            Id<int>(x => x.Seq, map => { map.Column("Seq"); });

            Property<string>(x => x.GRNo, map => { map.Column("GRNo"); });
            Property<int>(x => x.Seq, map => { map.Column("Seq"); });
            Property<string>(x => x.AktivaId, map => { map.Column("AktivaId"); });
            Property<double>(x => x.Amount, map => { map.Column("Amount"); });
            Property<string>(x => x.Notes, map => { map.Column("Notes"); });
        }
    }
}
