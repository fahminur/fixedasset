﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class AssetBranchMapping : ClassMapping<AssetBranch>
    {
        public AssetBranchMapping()
        {
            this.Table("Asset_ref_AssetBranch");
            Id<string>(x => x.AssetBranchID,
               map => { map.Column("AssetBranchID"); });

            Property<string>(x => x.AssetBranchDescription,
               map => { map.Column("AssetBranchDescription"); ; });

            Property<string>(x => x.AssetBranchShortDesc,
               map => { map.Column("AssetBranchShortDesc"); });

            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
