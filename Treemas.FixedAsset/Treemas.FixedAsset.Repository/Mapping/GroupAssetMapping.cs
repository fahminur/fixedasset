﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class GroupAssetMapping : ClassMapping<GroupAsset>
    {
        public GroupAssetMapping()
        {
            this.Table("Asset_ref_GroupAsset");
            Id<string>(x => x.GroupAssetID,
               map => { map.Column("GroupAssetID"); });

            Property<string>(x => x.GroupAssetDescription,
               map => { map.Column("GroupAssetDescription"); ; });

            Property<string>(x => x.GroupAssetShortDesc,
               map => { map.Column("GroupAssetShortDesc"); });

            Property<string>(x => x.CoaPerolehan,
               map => { map.Column("CoaPerolehan"); });

            Property<string>(x => x.CoaAkumulasi,
               map => { map.Column("CoaAkumulasi"); });

            Property<string>(x => x.CoaBeban,
               map => { map.Column("CoaBeban"); });

            Property<string>(x => x.PaymentAllocation,
               map => { map.Column("PaymentAllocationId"); });

            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
