﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class VendorBankMapping : ClassMapping<VendorBank>
    {
        public VendorBankMapping()
        {
            this.Table("Procurement_VendorBankAccount");
            Id<long>(x => x.Id,
               map => {
                   map.Column("VendorBankId");
                   map.Generator(Generators.Identity);
               });

            Property<string>(x => x.BankMasterId,
                map => { map.Column("BankId"); ; });
            Property<long>(x => x.VendorId,
               map => { map.Column("VendorId"); ; });

            Property<string>(x => x.BankName,
               map => { map.Column("BankName"); });

            Property<string>(x => x.BankAccountNo,
               map => { map.Column("BankAccountNo"); });

            Property<string>(x => x.BankAccountName,
               map => { map.Column("BankAccountName"); });

            Property<string>(x => x.BankBranch,
               map => { map.Column("BankBranch"); });

            Property<bool>(x => x.IsDefault,
               map => { map.Column("IsDefault"); ; });


            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
