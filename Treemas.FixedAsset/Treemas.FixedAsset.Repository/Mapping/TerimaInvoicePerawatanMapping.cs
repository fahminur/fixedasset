﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class TerimaInvoicePerawatanMapping : ClassMapping<TerimaInvoicePerawatan>
    {
        public TerimaInvoicePerawatanMapping()
        {
            this.Table("Asset_TerimaInvoicePerawatan");
            Id<long>(x => x.Id,
               map => { map.Column("ID"); map.Generator(Generators.Identity); });

            Property<long>(x => x.VendorId, map => { map.Column("VendorId"); });
            Property<string>(x => x.NoInvoice, map => { map.Column("NoInvoice"); });
            Property<int>(x => x.SeqNo, map => { map.Column("SeqNo"); });
            Property<string>(x => x.AktivaID, map => { map.Column("AktivaID"); });
            Property<string>(x => x.COABiaya, map => { map.Column("COABiaya"); });
            Property<string>(x => x.NoPO, map => { map.Column("NoPO"); });
            Property<string>(x => x.NoSPK, map => { map.Column("NoSPK"); });
            Property<DateTime?>(x => x.TanggalPerawatan, map => { map.Column("TanggalPerawatan"); });
            Property<string>(x => x.MataUang, map => { map.Column("MataUang"); });
            Property<double>(x => x.NilaiOriginal, map => { map.Column("NilaiOriginal"); });
            Property<double>(x => x.Kurs, map => { map.Column("Kurs"); });
            Property<double>(x => x.NilaiPerawatan, map => { map.Column("NilaiPerawatan"); });
            Property<string>(x => x.Keterangan, map => { map.Column("Keterangan"); });
            Property<string>(x => x.UOM, map => { map.Column("UOM"); });
            Property<int>(x => x.Qty, map => { map.Column("Qty"); });
            Property<int>(x => x.PPn, map => { map.Column("PPn"); });
            Property<int>(x => x.PPh23, map => { map.Column("PPh23"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
