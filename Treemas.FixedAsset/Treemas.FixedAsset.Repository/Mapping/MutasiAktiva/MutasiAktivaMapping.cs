﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class MutasiAktivaMapping : ClassMapping<MutasiAktiva>
    {
        public MutasiAktivaMapping()
        {
            this.Table("Asset_MutasiAktiva");
            Id<string>(x => x.AktivaID,
                map =>
                {
                    map.Column("AktivaID");
                });

            Property<string>(x => x.AktivaID,
                map => { map.Column("AktivaID"); });

            Property<string>(x => x.NamaAktiva,
                map => { map.Column("NamaAktiva"); });

            Property<string>(x => x.GroupAssetID,
                map => { map.Column("GroupAssetID"); });

            Property<string>(x => x.SubGroupAssetID,
                map => { map.Column("SubGroupAssetID"); });

            Property<DateTime?>(x => x.TanggalMutasi,
                map => { map.Column("TanggalMutasi"); });

            Property<string>(x => x.Periode,
                map => { map.Column("Periode"); });

            Property<string>(x => x.DariCabangID,
                map => { map.Column("DariCabangID"); });

            Property<string>(x => x.DariLokasi,
                map => { map.Column("DariLokasi"); });

            //Property<string>(x => x.DariLokasiID,
            //    map => { map.Column("DariLokasiID"); });

            //Property<string>(x => x.DariAreaID,
            //map => { map.Column("DariAreaID"); });

            Property<string>(x => x.DariDepartmentID,
                map => { map.Column("DariDepartmentID"); });

            Property<string>(x => x.DariPemakaiID,
                map => { map.Column("DariPemakaiID"); });

            Property<string>(x => x.DariKondisiID,
                map => { map.Column("DariKondisiID"); });

            //Property<string>(x => x.DariKodeIndukAktivaID,
            //    map => { map.Column("DariKodeIndukAktivaID"); });

            Property<string>(x => x.KeAktivaID,
                map => { map.Column("KeAktivaID"); });

            Property<string>(x => x.KeCabangID,
                map => { map.Column("KeCabangID"); });

            Property<string>(x => x.KeLokasi,
                map => { map.Column("KeLokasi"); });

            //Property<string>(x => x.KeLokasiID,
            //    map => { map.Column("KeLokasiID"); });

            //Property<string>(x => x.KeAreaID,
            //    map => { map.Column("KeAreaID"); });

            Property<string>(x => x.KeDepartmentID,
                map => { map.Column("KeDepartmentID"); });

            Property<string>(x => x.KePemakaiID,
                map => { map.Column("KePemakaiID"); });

            Property<string>(x => x.KeKondisiID,
                map => { map.Column("KeKondisiID"); });

            //Property<string>(x => x.KeKodeIndukAktivaID,
            //    map => { map.Column("KeKodeIndukAktivaID"); });

            Property<string>(x => x.NoReferensi,
                map => { map.Column("NoReferensi"); });

            Property<string>(x => x.Keterangan,
                map => { map.Column("Keterangan"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
