﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class AssetAreaMapping : ClassMapping<AssetArea>
    {
        public AssetAreaMapping()
        {
            this.Table("Asset_ref_AssetArea");
            Id<string>(x => x.AssetAreaID,
               map => { map.Column("AssetAreaID"); });

            Property<string>(x => x.AssetAreaDescription,
               map => { map.Column("AssetAreaDescription");});

            Property<string>(x => x.AssetAreaShortDesc,
               map => { map.Column("AssetAreaShortDesc"); });

            Property<string>(x => x.AssetBranchID,
              map => { map.Column("AssetBranchID"); });


            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
