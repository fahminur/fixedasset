﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PaymentTypeMapping : ClassMapping<PaymentType>
    {
        public PaymentTypeMapping()
        {
            this.Table("Asset_ref_PaymentType");
            Id<string>(x => x.PaymentTypeID, map => { map.Column("PaymentTypeID"); });
            Property<string>(x => x.PaymentTypeName, map => { map.Column("PaymentTypeName"); });
            Property<string>(x => x.PaymentTypeInitial, map => { map.Column("PaymentTypeInitial"); });
            Property<string>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
