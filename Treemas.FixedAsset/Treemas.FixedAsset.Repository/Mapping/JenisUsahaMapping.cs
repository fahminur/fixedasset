﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class JenisUsahaMapping : ClassMapping<JenisUsaha>
    {
        public JenisUsahaMapping()
        {
            this.Table("Asset_ref_JenisUsaha");
            Id<string>(x => x.JenisUsahaID,
               map => { map.Column("JenisUsahaID"); });

            Property<string>(x => x.NamaJenisUsaha,
               map => { map.Column("NamaJenisUsaha"); });

            Property<string>(x => x.JenisUsahaShortDesc,
                map => { map.Column("JenisUsahaShortDesc"); });

            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
