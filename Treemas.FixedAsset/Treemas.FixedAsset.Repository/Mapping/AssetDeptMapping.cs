﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class AssetDeptMapping : ClassMapping<AssetDept>
    {
        public AssetDeptMapping()
        {
            this.Table("Asset_ref_AssetDept");
            Id<string>(x => x.AssetDeptID,
               map => { map.Column("AssetDeptID"); });

            Property<string>(x => x.AssetDeptDescription,
               map => { map.Column("AssetDeptDescription"); ; });

            Property<string>(x => x.AssetDeptShortDesc,
               map => { map.Column("AssetDeptShortDesc"); });

            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
