﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class AssetCoaMapping : ClassMapping<AssetCoa>
    {
        public AssetCoaMapping()
        {
            this.Table("Asset_ref_AssetCoa");
            Id<string>(x => x.COAID,
               map => { map.Column("COAID"); });

            Property<string>(x => x.COADescription,
               map => { map.Column("COADescription");});

          
            Property<string>(x => x.CoaType,
              map => { map.Column("CoaType"); });                 


            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
