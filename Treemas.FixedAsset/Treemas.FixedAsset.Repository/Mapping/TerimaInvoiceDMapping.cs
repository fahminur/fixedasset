﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class TerimaInvoiceDMapping : ClassMapping<TerimaInvoiceD>
    {
        public TerimaInvoiceDMapping()
        {
            this.Table("Asset_TerimaInvoiceD");
            Id<long>(x => x.Id,
               map => { map.Column("ID"); map.Generator(Generators.Identity); });

            Property<long>(x => x.VendorId, map => { map.Column("VendorId"); });
            Property<string>(x => x.NoInvoice, map => { map.Column("NoInvoice"); });
            Property<int>(x => x.SeqNo, map => { map.Column("SeqNo"); });
            Property<string>(x => x.JenisInvoice, map => { map.Column("JenisInvoice"); });
            Property<string>(x => x.NamaBarangJasa, map => { map.Column("NamaBarangJasa"); });
            Property<int>(x => x.Jumlah, map => { map.Column("Jumlah"); });
            Property<string>(x => x.Satuan, map => { map.Column("Satuan"); });
            Property<string>(x => x.MataUang, map => { map.Column("MataUang"); });
            Property<double>(x => x.HargaSatuan, map => { map.Column("HargaSatuan"); });
            Property<double>(x => x.Nilai, map => { map.Column("Nilai"); });
            Property<int>(x => x.PPn, map => { map.Column("PPn"); });
            Property<int>(x => x.PPh23, map => { map.Column("PPh23"); });
            Property<string>(x => x.AktivaIDNoReff, map => { map.Column("AktivaIDNoReff"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
