﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PenghapusanAktivaHeaderMapping: ClassMapping<PenghapusanAktivaHeader>
    {
        public PenghapusanAktivaHeaderMapping()
        {
            this.Table("Asset_PenghapusanAktivaHeader");
            Id<string>(x => x.DeleteNo,
                map =>
                {
                    map.Column("DeleteNo");
                });

            Property<DateTime>(x => x.DeleteDate,
                map => { map.Column("DeleteDate"); });

            Property<string>(x => x.InternalMemoNo,
                map => { map.Column("InternalMemoNo"); });

            Property<string>(x => x.Notes,
                map => { map.Column("Notes"); });

            Property<string>(x => x.Status,
                map => { map.Column("Status"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
