﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PenghapusanAktivaDetailMapping : ClassMapping<PenghapusanAktivaDetail>
    {
        public PenghapusanAktivaDetailMapping()
        {
            this.Table("Asset_PenghapusanAktivaDetail");
            Id<int>(x => x.Seq,
                map =>
                {
                    map.Column("Seq");
                });

            Property<string>(x => x.DeleteNo,
                map => { map.Column("DeleteNo"); });

            Property<string>(x => x.AktivaID,
                map => { map.Column("AktivaID"); });

            Property<string>(x => x.DeleteReason,
                map => { map.Column("DeleteReason"); });

            Property<string>(x => x.Notes,
                map => { map.Column("Notes"); });

        }
    }
}
