﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PenjualanAktivaDetailMapping : ClassMapping<PenjualanAktivaDetail>
    {
        public PenjualanAktivaDetailMapping()
        {
            this.Table("Asset_PenjualanAktivaDetail");
            Id<int>(x => x.Seq,
                map =>
                {
                    map.Column("Seq");
                });

            Property<string>(x => x.SellingNo,
               map => { map.Column("SellingNo"); });

            Property<string>(x => x.AktivaID,
                map => { map.Column("AktivaID"); });

            Property<double>(x => x.SellingPrice,
                map => { map.Column("SellingPrice"); });

            Property<string>(x => x.Buyer,
                map => { map.Column("Buyer"); });

            Property<DateTime?>(x => x.StopDepresiationDate, map => { map.Column("StopDepreciationDate"); });

            Property<string>(x => x.Notes,
                map => { map.Column("Notes"); });

            Property<int>(x => x.DepreciationSeqNo,
                map => { map.Column("DepreciationSeqNo"); });
        }
    }
}
