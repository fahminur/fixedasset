﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PenjualanAktivaMapping : ClassMapping<PenjualanAktiva>
    {
        public PenjualanAktivaMapping()
        {
            this.Table("Asset_PenjualanAktiva");
            Id<string>(x => x.AktivaID,
                map =>
                {
                    map.Column("AktivaID");
                });

            Property<string>(x => x.AktivaName,
                map => { map.Column("AktivaName"); });

            Property<double>(x => x.SellingPrice,
                map => { map.Column("SellingPrice"); });

            Property<string>(x => x.Buyer,
                map => { map.Column("Buyer"); });

            Property<DateTime?>(x => x.SellingDate,
                map => { map.Column("SellingDate"); });

            Property<string>(x => x.ReferenceNo,
                map => { map.Column("ReferenceNo"); });

            Property<string>(x => x.InternalMemoNo,
                map => { map.Column("InternalMemoNo"); });

            Property<DateTime?>(x => x.StopDepresiationDate,
                map => { map.Column("StopDepreciationDate"); });

            Property<string>(x => x.Notes,
                map => { map.Column("Notes"); });
            Property<string>(x => x.Status,
                map => { map.Column("Status"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });

            Property<bool>(x => x.IsApproved1,
                map => { map.Column("IsApproved1"); });

            Property<DateTime?>(x => x.Approved1Date, map => { map.Column("Approved1Date"); });
            Property<string>(x => x.Approved1User, map => { map.Column("Approved1User"); });

            Property<bool>(x => x.IsApproved2,
                map => { map.Column("IsApproved2"); });

            Property<DateTime?>(x => x.Approved2Date, map => { map.Column("Approved2Date"); });
            Property<string>(x => x.Approved2User, map => { map.Column("Approved2User"); });

            Property<bool>(x => x.IsApproved3,
                map => { map.Column("IsApproved3"); });

            Property<DateTime?>(x => x.Approved3Date, map => { map.Column("Approved3Date"); });
            Property<string>(x => x.Approved3User, map => { map.Column("Approved3User"); });
        }
    }
}
