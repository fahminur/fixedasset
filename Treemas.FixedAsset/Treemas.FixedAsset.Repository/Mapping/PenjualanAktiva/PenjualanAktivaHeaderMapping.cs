﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PenjualanAktivaHeaderMapping : ClassMapping<PenjualanAktivaHeader>
    {
        public PenjualanAktivaHeaderMapping()
        {
            this.Table("Asset_PenjualanAktivaHeader");

            Id<string>(x => x.SellingNo,
                map =>
                {
                    map.Column("SellingNo");
                });

            Property<DateTime?>(x => x.SellingDate,
                map => { map.Column("SellingDate"); });

            Property<string>(x => x.ReferenceNo,
                map => { map.Column("ReferenceNo"); });

            Property<string>(x => x.InternalMemoNo,
                map => { map.Column("InternalMemoNo"); });

            Property<string>(x => x.Notes,
                map => { map.Column("Notes"); });
            Property<string>(x => x.Status,
                map => { map.Column("Status"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
