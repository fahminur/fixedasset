﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class AssetTidakSusutMapping : ClassMapping<AssetTidakSusut>
    {
        public AssetTidakSusutMapping()
        {
            this.Table("Asset_AssetTidakSusut");
            Id<long>(x => x.Id,
               map => {
                   map.Column("RowID");
                   map.Generator(Generators.Identity);
               });
            

            Property<string>(x => x.AktivaId,
               map => { map.Column("AktivaId"); });

            Property<string>(x => x.Tahun,
               map => { map.Column("Tahun"); });
            
            Property<string>(x => x.Bulan,
               map => { map.Column("Bulan"); });

           
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
