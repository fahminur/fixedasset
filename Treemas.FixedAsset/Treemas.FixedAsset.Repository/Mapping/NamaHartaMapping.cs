﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class NamaHartaMapping : ClassMapping<NamaHarta>
    {
        public NamaHartaMapping()
        {
            this.Table("Asset_ref_NamaHarta");
            Id<string>(x => x.NamaHartaID,
               map => { map.Column("NamaHartaID"); });

            Property<string>(x => x.NamaHartaDeskripsi,
               map => { map.Column("NamaHartaDeskripsi");});

            Property<string>(x => x.JenisHartaID,
               map => { map.Column("JenisHartaID"); });
            

            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
