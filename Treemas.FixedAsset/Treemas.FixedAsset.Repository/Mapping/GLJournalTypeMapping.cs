﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class GLJournalTypeMapping : ClassMapping<GLJournalType>
    {
        public GLJournalTypeMapping()
        {
            this.Table("Asset_Ref_JournalType");
            Id<string>(x => x.TransactionID, map => { map.Column("TransactionID"); });
            Property<string>(x => x.TransactionID, map => { map.Column("TransactionID"); });
            Property<string>(x => x.TransactionDesc, map => { map.Column("TransactionDesc"); });
        }
    }
}
