﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class ExpenceTypeMapping : ClassMapping<ExpenceType>
    {
        public ExpenceTypeMapping()
        {
            this.Table("Asset_ref_ExpenceType");
            Id<string>(x => x.ExpenceTypeID, map => { map.Column("ExpenceTypeID"); });
            Property<string>(x => x.ExpenceTypeName, map => { map.Column("ExpenceTypeName"); });
            Property<string>(x => x.ExpenceTypeInitial, map => { map.Column("ExpenceTypeInitial"); });
            Property<string>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
