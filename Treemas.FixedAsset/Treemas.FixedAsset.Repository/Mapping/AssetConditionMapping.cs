﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class AssetConditionMapping : ClassMapping<AssetCondition>
    {
        public AssetConditionMapping()
        {
            this.Table("Asset_ref_AssetCondition");
            Id<string>(x => x.AssetConditionID,
               map => { map.Column("AssetConditionID"); });

            Property<string>(x => x.AssetConditionDescription,
               map => { map.Column("AssetConditionDescription"); ; });

            Property<string>(x => x.AssetConditionShortDesc,
               map => { map.Column("AssetConditionShortDesc"); });

            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
