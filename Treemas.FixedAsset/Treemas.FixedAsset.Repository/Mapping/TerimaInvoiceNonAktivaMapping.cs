﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class TerimaInvoiceNonAktivaMapping : ClassMapping<TerimaInvoiceNonAktiva>
    {
        public TerimaInvoiceNonAktivaMapping()
        {
            this.Table("Asset_TerimaInvoiceNonAktiva");
            Id<long>(x => x.Id,
               map => { map.Column("ID"); map.Generator(Generators.Identity); });

            Property<long>(x => x.VendorId, map => { map.Column("VendorId"); });
            Property<string>(x => x.NoInvoice, map => { map.Column("NoInvoice"); });
            Property<int>(x => x.SeqNo, map => { map.Column("SeqNo"); });
            Property<string>(x => x.NoRefference, map => { map.Column("NoRefference"); });
            Property<string>(x => x.NamaBarangJasa, map => { map.Column("NamaBarangJasa"); });
            Property<double>(x => x.Nilai, map => { map.Column("Nilai"); });
            Property<string>(x => x.CaraPembebanan, map => { map.Column("CaraPembebanan"); });
            Property<int>(x => x.JangkaWaktu, map => { map.Column("JangkaWaktu"); });
            Property<DateTime?>(x => x.StartDate, map => { map.Column("StartDate"); });
            Property<DateTime?>(x => x.EndDate, map => { map.Column("EndDate"); });
            Property<double>(x => x.BebanPerBulan, map => { map.Column("BebanPerBulan"); });
            Property<string>(x => x.COABiaya, map => { map.Column("COABiaya"); });
            Property<string>(x => x.COAPrepaid, map => { map.Column("COAPrepaid"); });
            Property<string>(x => x.NoPO, map => { map.Column("NoPO"); });
            Property<string>(x => x.NoDO, map => { map.Column("NoDO"); });
            Property<string>(x => x.NoPenerimaan, map => { map.Column("NoPenerimaan"); });
            Property<string>(x => x.Keterangan, map => { map.Column("Keterangan"); });
            Property<int>(x => x.PPn, map => { map.Column("PPn"); });
            Property<int>(x => x.PPh23, map => { map.Column("PPh23"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
