﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class AssetLocationMapping : ClassMapping<AssetLocation>
    {
        public AssetLocationMapping()
        {
            this.Table("Asset_ref_AssetLocation");
            Id<string>(x => x.AssetLocationID,
               map => { map.Column("AssetLocationID"); });

            Property<string>(x => x.AssetLocationDescription,
               map => { map.Column("AssetLocationDescription"); ; });

            Property<string>(x => x.AssetLocationShortDesc,
               map => { map.Column("AssetLocationShortDesc"); });

            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
