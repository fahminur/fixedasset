﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class VendorMapping : ClassMapping<Vendor>
    {
        public VendorMapping()
        {
            this.Table("Asset_ref_Vendor");
            Id<string>(x => x.VendorId, map => { map.Column("VendorID"); });

            Property<string>(x => x.VendorGroupID, map => { map.Column("VendorGroupID"); });

            Property<string>(x => x.VendorName, map => { map.Column("VendorName"); });

            Property<string>(x => x.GroupCompanyID, map => { map.Column("GroupCompanyID "); });
            Property<string>(x => x.NPWP, map => { map.Column("NPWP "); });
            Property<string>(x => x.TDP, map => { map.Column("TDP"); });
            Property<string>(x => x.SIUP, map => { map.Column("SIUP "); });
            Property<string>(x => x.VendorAddress, map => { map.Column("VendorAddress"); });
            Property<string>(x => x.VendorRT, map => { map.Column("VendorRT "); });
            Property<string>(x => x.VendorRW, map => { map.Column("VendorRW "); });
            Property<string>(x => x.VendorKelurahan, map => { map.Column("VendorKelurahan"); });
            Property<string>(x => x.VendorKecamatan, map => { map.Column("VendorKecamatan"); });
            Property<string>(x => x.VendorCity, map => { map.Column("VendorCity "); });
            Property<string>(x => x.VendorZipCode, map => { map.Column("VendorZipCode"); });
            Property<string>(x => x.VendorAreaPhone1, map => { map.Column("VendorAreaPhone1 "); });
            Property<string>(x => x.VendorPhone1, map => { map.Column("VendorPhone1 "); });
            Property<string>(x => x.VendorAreaPhone2, map => { map.Column("VendorAreaPhone2 "); });
            Property<string>(x => x.VendorPhone2, map => { map.Column("VendorPhone2 "); });
            Property<string>(x => x.VendorAreaFax, map => { map.Column("VendorAreaFax"); });
            Property<string>(x => x.VendorFax, map => { map.Column("VendorFax"); });
            Property<string>(x => x.ContactPersonName, map => { map.Column("ContactPersonName"); });
            Property<string>(x => x.ContactPersonJobTitle, map => { map.Column("ContactPersonJobTitle"); });
            Property<string>(x => x.ContactPersonEmail, map => { map.Column("ContactPersonEmail "); });
            Property<string>(x => x.ContactPersonHP, map => { map.Column("ContactPersonHP"); });
            Property<string>(x => x.VendorStatus, map => { map.Column("VendorStatus "); });
            Property<DateTime?>(x => x.VendorStartDate, map => { map.Column("VendorStartDate"); });
            Property<DateTime?>(x => x.PKSDate, map => { map.Column("PKSDate"); });
            Property<DateTime?>(x => x.LastCalculationDate, map => { map.Column("LastCalculationDate"); });
            Property<bool>(x => x.IsAutomotive, map => { map.Column("IsAutomotive"); });
            Property<bool>(x => x.IsUrusBPKB, map => { map.Column("IsUrusBPKB"); });
            Property<bool>(x => x.IsActive, map => { map.Column("IsActive"); });
            Property<bool>(x => x.PF, map => { map.Column("PF"); });
            Property<bool>(x => x.SKBP, map => { map.Column("SKBP"); });
            Property<bool>(x => x.PenerapanTVC, map => { map.Column("PenerapanTVC"); });
            Property<bool>(x => x.isPerseorangan, map => { map.Column("isPerseorangan"); });
            Property<string>(x => x.VendorBadStatus, map => { map.Column("VendorBadStatus "); });
            Property<string>(x => x.VendorLevelStatus, map => { map.Column("VendorLevelStatus "); });
            Property<string>(x => x.VendorCategory, map => { map.Column("VendorCategory"); });
            Property<string>(x => x.VendorAssetStatus, map => { map.Column("VendorAssetStatus "); });
            Property<string>(x => x.UsrUpd, map => { map.Column("UsrUpd"); });
            Property<DateTime>(x => x.DtmUpd, map => { map.Column("DtmUpd"); });
            Property<string>(x => x.VendorIncentiveIDNew, map => { map.Column("VendorIncentiveIDNew"); });
            Property<string>(x => x.VendorIncentiveIDUsed, map => { map.Column("VendorIncentiveIDUsed "); });
            Property<string>(x => x.VendorIncentiveIDRO, map => { map.Column("VendorIncentiveIDRO "); });
            Property<string>(x => x.VendorPointId, map => { map.Column("VendorPointId "); });
            Property<string>(x => x.NoPKS, map => { map.Column("NoPKS "); });
            Property<string>(x => x.AssetTypeID, map => { map.Column("AssetTypeID "); });
            Property<string>(x => x.VendorInitialName, map => { map.Column("VendorInitialName "); });
            Property<string>(x => x.AttachedFile, map => { map.Column("AttachedFile"); });
            Property<string>(x => x.NPWPVendorAddress, map => { map.Column("NPWPVendorAddress "); });
            Property<string>(x => x.NPWPVendorRT, map => { map.Column("NPWPVendorRT"); });
            Property<string>(x => x.NPWPVendorRW, map => { map.Column("NPWPVendorRW"); });
            Property<string>(x => x.NPWPVendorKelurahan, map => { map.Column("NPWPVendorKelurahan "); });
            Property<string>(x => x.NPWPVendorKecamatan, map => { map.Column("NPWPVendorKecamatan "); });
            Property<string>(x => x.NPWPVendorCity, map => { map.Column("NPWPVendorCity"); });
            Property<string>(x => x.NPWPVendorZipCode, map => { map.Column("NPWPVendorZipCode "); });
            Property<string>(x => x.NPWPVendorAreaPhone1, map => { map.Column("NPWPVendorAreaPhone1"); });
            Property<string>(x => x.NPWPVendorPhone1, map => { map.Column("NPWPVendorPhone1"); });
            Property<string>(x => x.NPWPVendorAreaPhone2, map => { map.Column("NPWPVendorAreaPhone2"); });
            Property<string>(x => x.NPWPVendorPhone2, map => { map.Column("NPWPVendorPhone2"); });
            Property<string>(x => x.NPWPVendorAreaFax, map => { map.Column("NPWPVendorAreaFax "); });
            Property<string>(x => x.NPWPVendorFax, map => { map.Column("NPWPVendorFax "); });

            //Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            //Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            //Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            //Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
