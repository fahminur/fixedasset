﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class VendorAccountMapping : ClassMapping<VendorAccount>
    {
        public VendorAccountMapping()
        {
            this.Table("Asset_ref_VendorAccount");
            Id<string>(x => x.VendorAccID,
               map => { map.Column("VendorAccID"); });

            Property<string>(x => x.VendorAccID, map => { map.Column("VendorAccID"); });

            Property<string>(x => x.VendorID, map => { map.Column("VendorID"); });

            Property<string>(x => x.VendorBankID, map => { map.Column("VendorBankID"); });

            Property<string>(x => x.VendorBankBranch, map => { map.Column("VendorBankBranch"); });

            Property<int>(x => x.VendorBankBranchID, map => { map.Column("VendorBankBranchID"); });

            Property<string>(x => x.VendorAccountNo, map => { map.Column("VendorAccountNo"); });

            Property<string>(x => x.VendorAccountName, map => { map.Column("VendorAccountName"); });

            Property<bool>(x => x.DefaultAccount, map => { map.Column("DefaultAccount"); });

            Property<string>(x => x.UntukBayar, map => { map.Column("UntukBayar"); });

            Property<DateTime?>(x => x.TanggalEfektif, map => { map.Column("TanggalEfektif"); });

            Property<DateTime?>(x => x.DtmUpd, map => { map.Column("DtmUpd"); });

            Property<string>(x => x.UsrUpd, map => { map.Column("UsrUpd"); });

            //Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            //Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
