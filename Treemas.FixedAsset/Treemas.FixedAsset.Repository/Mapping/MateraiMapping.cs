﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;


namespace Treemas.FixedAsset.Repository.Mapping
{
    public class MateraiMapping : ClassMapping<Materai>
    {
        public MateraiMapping()
        {
            this.Table("Asset_Ref_Materai");
            Id<string>(x => x.MateraiID, map => { map.Column("MateraiID"); });
            Property<string>(x => x.MateraiName, map => { map.Column("MateraiName"); });
            Property<double>(x => x.MateraiValue, map => { map.Column("MateraiValue"); });
            Property<string>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
