﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class TerimaInvoiceHMapping : ClassMapping<TerimaInvoiceH>
    {
        public TerimaInvoiceHMapping()
        {
            this.Table("Asset_TerimaInvoiceH");
            Id<long>(x => x.Id,
               map => { map.Column("ID"); map.Generator(Generators.Identity); });

            Property<long>(x => x.VendorId, map => { map.Column("VendorId"); });
            Property<string>(x => x.NoInvoice, map => { map.Column("NoInvoice"); });
            Property<DateTime?>(x => x.TanggalInvoice, map => { map.Column("TanggalInvoice"); });
            Property<DateTime?>(x => x.TanggalJatuhTempo, map => { map.Column("TanggalJatuhTempo"); });
            Property<DateTime?>(x => x.TanggalRencanaBayar, map => { map.Column("TanggalRencanaBayar"); });
            Property<long>(x => x.VendorBankId, map => { map.Column("VendorBankId"); });
            Property<double>(x => x.TotalInvoice, map => { map.Column("TotalInvoice"); });
            Property<double>(x => x.PPn, map => { map.Column("PPn"); });
            Property<double>(x => x.PPh23, map => { map.Column("PPh23"); });
            Property<double>(x => x.TotalBayar, map => { map.Column("TotalBayar"); });
            Property<string>(x => x.Catatan, map => { map.Column("Catatan"); });
            Property<bool>(x => x.Status, map => { map.Column("Status"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
