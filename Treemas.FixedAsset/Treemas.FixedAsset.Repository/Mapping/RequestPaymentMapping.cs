﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;


namespace Treemas.FixedAsset.Repository.Mapping
{
    public class RequestPaymentMapping : ClassMapping<RequestPayment>
    {
        public RequestPaymentMapping()
        {
            this.Table("Asset_RequestPayment");
            Id<string>(x => x.Id,
               map => { map.Column("ID"); });

            Property<string>(x => x.SupplierId, map => { map.Column("SupplierId"); });
            Property<string>(x => x.SupplierBankId, map => { map.Column("SupplierBankId"); });
            Property<string>(x => x.SupplierBankBranch, map => { map.Column("SupplierBankBranch"); });
            Property<string>(x => x.SupplierBankAccountNo, map => { map.Column("SupplierBankAccountNo"); });
            Property<string>(x => x.SupplierBankAccountName, map => { map.Column("SupplierBankAccountName"); });
            Property<string>(x => x.InvoiceNo, map => { map.Column("InvoiceNo"); });
            Property<DateTime?>(x => x.InvoiceDate, map => { map.Column("InvoiceDate"); });
            Property<DateTime?>(x => x.PaymentOrderDate, map => { map.Column("PaymentOrderDate"); });
            Property<string>(x => x.CaraBayar, map => { map.Column("CaraBayar"); });
            Property<double>(x => x.Jumlah, map => { map.Column("Jumlah"); });
            Property<string>(x => x.RefferenceNo, map => { map.Column("RefferenceNo"); });
            Property<string>(x => x.Notes, map => { map.Column("Notes"); });
            Property<string>(x => x.PaymentAllocationId, map => { map.Column("PaymentAllocationId"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
