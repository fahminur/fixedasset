﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class InvoiceAktivaMapping : ClassMapping<InvoiceAktiva>
    {
        public InvoiceAktivaMapping()
        {
            this.Table("Asset_InvoiceAktiva");
            Id<long>(x => x.ID, map => { map.Column("Id"); });

            Property<string>(x => x.InvoiceNo,
               map => { map.Column("InvoiceNo"); });

            Property<int>(x => x.Seq,
               map => { map.Column("Seq"); });

            //Property<string>(x => x.AssetSubGroupId,
            //  map => { map.Column("AssetSubGroupId"); });

            Property<string>(x => x.StartingAktivaId,
              map => { map.Column("StartingAktivaId"); });

            //Property<string>(x => x.PurchaseOrderNo,
            //  map => { map.Column("PurchaseOrderNo"); });

            //Property<string>(x => x.DeliveryOrderNo,
            //  map => { map.Column("DeliveryOrderNo"); });

            //Property<string>(x => x.GRNo,
            //  map => { map.Column("GRNo"); });

            Property<DateTime>(x => x.TanggalPerolehan,
              map => { map.Column("TanggalPerolehan"); });

            Property<double>(x => x.Quantity,
              map => { map.Column("Quantity"); });

            Property<double>(x => x.Price,
              map => { map.Column("Price"); });

            Property<double>(x => x.TotalPrice,
              map => { map.Column("TotalPrice"); });

            Property<string>(x => x.PPnId,
              map => { map.Column("PPnId"); });

            Property<double>(x => x.PPnAmount,
              map => { map.Column("PPnAmount"); });

            Property<string>(x => x.PPhId,
              map => { map.Column("PPhId"); });

            Property<double>(x => x.PPhAmount,
              map => { map.Column("PPhAmount"); });

            Property<double>(x => x.TotalAmount,
              map => { map.Column("TotalAmount"); });

            Property<double>(x => x.OtherCost,
              map => { map.Column("OtherCost"); });

            Property<double>(x => x.ExpedCost,
                map => { map.Column("ExpedCost"); });

            Property<string>(x => x.Notes,
              map => { map.Column("Notes"); });

            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateBy"); map.Update(false); });
        }
    }
}
