﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class InvoiceMapping : ClassMapping<InvoiceH>
    {
        public InvoiceMapping()
        {
            this.Table("Asset_InvoiceHeader");
            Id<string>(x => x.InvoiceNo,
               map => { map.Column("InvoiceNo"); });

            Property<string>(x => x.VendorInvoiceNo,
                map => { map.Column("VendorInvoiceNo"); });
            Property<string>(x => x.GRNo,
                map => { map.Column("GRNo"); });

            Property<string>(x => x.DeliveryOrderNo,
                map => { map.Column("DeliveryOrderNo"); });

            Property<string>(x => x.PONo,
              map => { map.Column("PONo"); });

            Property<string>(x => x.VendorID,
               map => { map.Column("VendorID");});

            Property<DateTime>(x => x.InvoiceDate,
               map => { map.Column("InvoiceDate"); });

            Property<DateTime>(x => x.InvoiceOrderDate,
              map => { map.Column("PaymentOrderDate"); });

            Property<DateTime>(x => x.InvoiceDueDate,
              map => { map.Column("InvoiceDueDate"); });                                                                                                                                                                                                                                                                                               

            Property<string>(x => x.Notes,
              map => { map.Column("Notes"); });

            Property<string>(x => x.VendorBankId,
              map => { map.Column("VendorBankId"); });

            Property<double>(x => x.InvoiceAmount,
              map => { map.Column("InvoiceAmount"); });

            Property<string>(x => x.Status,
              map => { map.Column("Status"); });

            Property<string>(x => x.InvoiceTypeID,
              map => { map.Column("InvoiceTypeID"); });

            Property<string>(x => x.NoFakturPajak,
              map => { map.Column("NoFakturPajak"); });

            Property<DateTime>(x => x.TglFakturPajak,
              map => { map.Column("TglFakturPajak"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
