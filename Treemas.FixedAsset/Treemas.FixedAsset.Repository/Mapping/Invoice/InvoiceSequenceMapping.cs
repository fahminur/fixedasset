﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.Finance.Repository.Mapping
{
    public class InvoiceSequenceMapping : ClassMapping<InvoiceSequence>
    {
        public InvoiceSequenceMapping()
        {
            this.Table("Asset_Ref_Sequence");

            ComposedId(map =>
            {
                map.Property(x => x.SeqID);
                map.Property(x => x.Year);
            });
            Property<string>(x => x.SeqID,
              map => { map.Column("SeqID"); map.Update(false); });
            Property<int>(x => x.Year,
              map => { map.Column("Year"); map.Update(false); });

            Property<string>(x => x.SeqName, map => { map.Column("SeqName"); });

            Property<int>(x => x.Seq1, map => { map.Column("Seq1"); });
            Property<int>(x => x.Seq2, map => { map.Column("Seq2"); });
            Property<int>(x => x.Seq3, map => { map.Column("Seq3"); });
            Property<int>(x => x.Seq4, map => { map.Column("Seq4"); });
            Property<int>(x => x.Seq5, map => { map.Column("Seq5"); });
            Property<int>(x => x.Seq6, map => { map.Column("Seq6"); });
            Property<int>(x => x.Seq7, map => { map.Column("Seq7"); });
            Property<int>(x => x.Seq8, map => { map.Column("Seq8"); });
            Property<int>(x => x.Seq9, map => { map.Column("Seq9"); });
            Property<int>(x => x.Seq10, map => { map.Column("Seq10"); });
            Property<int>(x => x.Seq11, map => { map.Column("Seq11"); });
            Property<int>(x => x.Seq12, map => { map.Column("Seq12"); });

            Property<int>(x => x.LengthNo, map => { map.Column("LengthNo"); });
            Property<string>(x => x.ResetFlag, map => { map.Column("ResetFlag"); });
            Property<string>(x => x.Prefix, map => { map.Column("Prefix"); });
            Property<bool>(x => x.IsSite, map => { map.Column("IsSite"); });
            Property<bool>(x => x.IsYear, map => { map.Column("IsYear"); });
            Property<bool>(x => x.IsMonth, map => { map.Column("IsMonth"); });
            Property<string>(x => x.Suffix, map => { map.Column("Suffix"); });
            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
