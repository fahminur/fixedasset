﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class InvoiceNonAktivaMapping : ClassMapping<InvoiceNonAktiva>
    {
        public InvoiceNonAktivaMapping()
        {
            this.Table("Asset_InvoiceNonAktiva");
            Id<long>(x => x.ID,
                map =>
                {
                    map.Column("Id");
                });

            Property<string>(x => x.InvoiceNo,
               map => { map.Column("InvoiceNo"); });

            Property<int>(x => x.Seq,
               map => { map.Column("Seq"); });
            
            Property<string>(x => x.NonAktivaId,
               map => { map.Column("NonAktivaId"); });

            //Property<string>(x => x.RefferenceNo,
            //   map => { map.Column("RefferenceNo");});

            //Property<string>(x => x.GroupAssetID,
            //  map => { map.Column("GroupAssetID"); });

            //Property<string>(x => x.ItemName,
            //   map => { map.Column("ItemName"); });

            Property<Double>(x => x.Price,
              map => { map.Column("Price"); });

            //Property<Double>(x => x.TotalPrice,
              //map => { map.Column("TotalPrice"); });

            //Property<int>(x => x.Qty,
            //  map => { map.Column("Qty"); });

            Property<string>(x => x.ExpenseType,
              map => { map.Column("ExpenseType"); });

            Property<int>(x => x.TimePeriod,
              map => { map.Column("TimePeriod"); });

            Property<DateTime?>(x => x.StartDate,
              map => { map.Column("StartDate"); });

            Property<DateTime?>(x => x.EndDate,
              map => { map.Column("EndDate"); });

            Property<double>(x => x.MonthlyExpense,
              map => { map.Column("MonthlyExpense"); });

            Property<string>(x => x.ExpensePA,
              map => { map.Column("ExpensePA"); });

            Property<string>(x => x.PrepaidPA,
              map => { map.Column("PrepaidPA"); });

            //Property<string>(x => x.PONo,
            //  map => { map.Column("PONo"); });

            //Property<string>(x => x.DeliveryOrderNo,
            //  map => { map.Column("DeliveryOrderNo"); });

            //Property<string>(x => x.GRNo,
            //  map => { map.Column("GRNo"); });

            Property<string>(x => x.Notes,
              map => { map.Column("Notes"); });

            Property<string>(x => x.PPnId,
              map => { map.Column("PPnId"); });

            Property<double>(x => x.PPnAmount,
              map => { map.Column("PPnAmount"); });

            Property<string>(x => x.PPhId,
              map => { map.Column("PPhId"); });

            Property<double>(x => x.PPhAmount,
              map => { map.Column("PPhAmount"); });

            Property<double>(x => x.TotalAmount,
              map => { map.Column("TotalAmount"); });

            //Property<double>(x => x.OtherCost,
            //  map => { map.Column("OtherCost"); });
            Property<double>(x => x.ExpedCost,
                map => { map.Column("ExpedCost"); });
            Property<int>(x => x.DepreciatedTime,
              map => { map.Column("DepreciatedTime"); });

            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
