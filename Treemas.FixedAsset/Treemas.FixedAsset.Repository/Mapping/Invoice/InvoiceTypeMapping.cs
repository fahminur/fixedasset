﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;


namespace Treemas.FixedAsset.Repository.Mapping
{
    public class InvoiceTypeMapping : ClassMapping<InvoiceType>
    {
        public InvoiceTypeMapping()
        {
            this.Table("Asset_Ref_InvoiceType");
            Id<string>(x => x.InvoiceTypeID, map => { map.Column("InvoiceTypeID"); });
            Property<string>(x => x.InvoiceTypeName, map => { map.Column("InvoiceTypeName"); });
            Property<string>(x => x.InvoiceTypeInitial, map => { map.Column("InvoiceTypeInitial"); });
            Property<string>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
