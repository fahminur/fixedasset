﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class InvoiceServiceMapping : ClassMapping<InvoiceService>
    {
        public InvoiceServiceMapping()
        {
            this.Table("Asset_InvoiceService");
            Id<long>(x => x.ID,
                map =>
                {
                    map.Column("Id");
                });

            Property<string>(x => x.InvoiceNo,
               map => { map.Column("InvoiceNo"); });

            Property<int>(x => x.Seq,
               map => { map.Column("Seq"); });

            Property<string>(x => x.AktivaId,
               map => { map.Column("AktivaId");});

            Property<string>(x => x.ExpensePA,
               map => { map.Column("ExpensePA"); });

            //Property<string>(x => x.PONo,
            //  map => { map.Column("PONo"); });

            Property<string>(x => x.WorkOrderNo,
              map => { map.Column("WorkOrderNo"); });                                                                                                                                                                                                                                                                                               

            Property<DateTime>(x => x.ServiceDate,
              map => { map.Column("ServiceDate"); });

            Property<DateTime>(x => x.ServiceGuaranteeDate,
              map => { map.Column("ServiceGuaranteeDate"); });
            
            Property<double>(x => x.ServiceAmount,
              map => { map.Column("ServiceAmount"); });

            Property<double>(x => x.ItemAmount,
              map => { map.Column("ItemAmount"); });

            Property<string>(x => x.PPnId,
              map => { map.Column("PPnId"); });

            Property<double>(x => x.PPnAmount,
              map => { map.Column("PPnAmount"); });

            Property<string>(x => x.PPhId,
              map => { map.Column("PPhId"); });

            Property<double>(x => x.PPhAmount,
              map => { map.Column("PPhAmount"); });

            Property<string>(x => x.MateraiID,
              map => { map.Column("MateraiID"); });

            Property<double>(x => x.MateraiAmount,
              map => { map.Column("MateraiAmount"); });

            Property<double>(x => x.TotalAmount,
              map => { map.Column("TotalAmount"); });

            //Property<double>(x => x.OtherCost,
            //  map => { map.Column("OtherCost"); });
            //Property<double>(x => x.ExpedCost,
            //    map => { map.Column("ExpedCost"); });
            Property<string>(x => x.Notes,
              map => { map.Column("Notes"); });
            
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
