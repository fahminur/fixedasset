﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class NonAssetMasterMapping : ClassMapping<NonAssetMaster>
    {
        public NonAssetMasterMapping()
        {
            this.Table("Asset_NonAssetMaster");

            Id<string>(x => x.NonAktivaID,
               map => { map.Column("NonAktivaID"); });

            Property<string>(x => x.NamaNonAktiva,
               map => { map.Column("NamaNonAktiva"); });

            Property<string>(x => x.GroupAssetID,
               map => { map.Column("GroupAssetID"); });

            Property<string>(x => x.SubGroupAssetID,
              map => { map.Column("SubGroupAssetID"); });

            Property<string>(x => x.SiteID,
              map => { map.Column("SiteID"); });

            Property<string>(x => x.Lokasi,
              map => { map.Column("Lokasi"); });

            Property<string>(x => x.AssetAreaID,
              map => { map.Column("AssetAreaID"); });

            Property<string>(x => x.AssetDepID,
              map => { map.Column("AssetDepID"); });

            Property<string>(x => x.AssetUserID,
              map => { map.Column("AssetUserID"); });

            Property<string>(x => x.AssetConditionID,
              map => { map.Column("AssetConditionID"); });

            Property<string>(x => x.Merek,
              map => { map.Column("Merek"); });

            Property<string>(x => x.Model,
              map => { map.Column("Model"); });

            Property<string>(x => x.SerialNo1,
              map => { map.Column("SerialNo1"); });

            Property<string>(x => x.SerialNo2,
              map => { map.Column("SerialNo2"); });

            Property<string>(x => x.InsuranceComID,
              map => { map.Column("InsuranceComID"); });

            Property<string>(x => x.InsuranceComBranchID,
               map => { map.Column("InsuranceComBranchID"); });

            Property<DateTime?>(x => x.MasaGaransi,
              map => { map.Column("MasaGaransi"); });

            Property<string>(x => x.KeteranganAktiva,
             map => { map.Column("KeteranganAktiva"); });

            Property<string>(x => x.VendorID,
              map => { map.Column("VendorID"); });

            Property<string>(x => x.NoPO,
              map => { map.Column("NoPO"); });

            Property<string>(x => x.NoDO,
              map => { map.Column("NoDO"); });

            Property<string>(x => x.NoGSRN,
              map => { map.Column("NoGSRN"); });

            Property<string>(x => x.NoInvoice,
              map => { map.Column("NoInvoice"); });

            Property<DateTime?>(x => x.TanggalPerolehan,
              map => { map.Column("TanggalPerolehan"); });

            Property<DateTime?>(x => x.TanggalTerimaAsset,
              map => { map.Column("TanggalTerimaAsset"); });

            Property<double>(x => x.NilaiOriginal,
              map => { map.Column("NilaiOriginal"); });

            Property<double>(x => x.Kurs,
              map => { map.Column("Kurs"); });

            Property<double>(x => x.HargaPerolehan,
              map => { map.Column("HargaPerolehan"); });

            Property<string>(x => x.KeteranganPerolehan,
              map => { map.Column("KeteranganPerolehan"); });

            Property<string>(x => x.Status,
              map => { map.Column("Status"); });

            Property<string>(x => x.NoPolis, map => { map.Column("NoPolis"); });
            Property<DateTime?>(x => x.TglPolis, map => { map.Column("TglPolis"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
