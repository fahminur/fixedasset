﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class PerawatanAktivaMapping : ClassMapping<PerawatanAktiva>
    {
        public PerawatanAktivaMapping()
        {
            this.Table("Asset_PerawatanAktiva");
            Id<string>(x => x.AktivaID,
                map =>
                {
                    map.Column("AktivaID");
                });

            Property<string>(x => x.AktivaID,
                map => { map.Column("AktivaID"); });

            Property<string>(x => x.NamaAktiva,
                map => { map.Column("NamaAktiva"); });

            Property<DateTime?>(x => x.TanggalPerawatan,
                map => { map.Column("TanggalPerawatan"); });

            Property<string>(x => x.CurrencyID,
                map => { map.Column("CurrencyID"); });

            Property<double>(x => x.NilaiOriginal,
                map => { map.Column("NilaiOriginal"); });

            Property<int>(x => x.Kurs,
                map => { map.Column("Kurs"); });

            Property<double>(x => x.Biaya,
                map => { map.Column("Biaya"); });

            Property<string>(x => x.VendorID,
                map => { map.Column("VendorID"); });

            Property<string>(x => x.NoInvoice,
                map => { map.Column("NoInvoice"); });

            Property<string>(x => x.COA,
                map => { map.Column("COAPerawatan"); });

            Property<DateTime>(x => x.MasaGaransi,
                map => { map.Column("MasaGaransi"); });

            Property<string>(x => x.NoReferensi,
                map => { map.Column("NoReferensi"); });

            Property<string>(x => x.Keterangan,
                map => { map.Column("Keterangan"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
