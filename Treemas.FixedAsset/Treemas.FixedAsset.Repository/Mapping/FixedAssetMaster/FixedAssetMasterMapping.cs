﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository.Mapping
{
    public class FixedAssetMasterMapping : ClassMapping<FixedAssetMaster>
    {
        public FixedAssetMasterMapping()
        {
            this.Table("Asset_FixedAssetMaster");
            Id<string>(x => x.AktivaID,
               map => { map.Column("AktivaID"); });

            Property<string>(x => x.NamaAktiva,
               map => { map.Column("NamaAktiva");});

            Property<string>(x => x.GroupAssetID,
               map => { map.Column("GroupAssetID"); });

            Property<string>(x => x.SubGroupAssetID,
              map => { map.Column("SubGroupAssetID"); });

            Property<string>(x => x.NamaHartaID,
              map => { map.Column("NamaHartaID"); });                                                                                                                                                                                                                                                                                               

            Property<string>(x => x.COAAktivaID,
              map => { map.Column("COAAktivaID"); });

            Property<string>(x => x.COAAkumulasiID,
              map => { map.Column("COAAkumulasiID"); });

            Property<string>(x => x.COAPenyusutanID,
              map => { map.Column("COAPenyusutanID"); });

            Property<string>(x => x.ParentAktivaID,
              map => { map.Column("ParentAktivaID"); });

            Property<string>(x => x.SiteID,
              map => { map.Column("SiteID"); });

            Property<string>(x => x.Lokasi,
              map => { map.Column("Lokasi"); });                                                                                                                                                                                                                                        

            Property<string>(x => x.AssetAreaID,
              map => { map.Column("AssetAreaID"); });

            Property<string>(x => x.AssetDepID,
              map => { map.Column("AssetDepID"); });

            Property<string>(x => x.AssetUserID,
              map => { map.Column("AssetUserID"); });

            Property<string>(x => x.AssetConditionID,
              map => { map.Column("AssetConditionID"); });

            Property<string>(x => x.Merek,
              map => { map.Column("Merek"); }); 

            Property<string>(x => x.Model,
              map => { map.Column("Model"); });

            Property<string>(x => x.SerialNo1,
              map => { map.Column("SerialNo1"); });

            Property<string>(x => x.SerialNo2,
              map => { map.Column("SerialNo2"); });

            Property<string>(x => x.InsuranceComID,
              map => { map.Column("InsuranceComID"); });

            Property<string>(x => x.InsuranceComBranchID,
               map => { map.Column("InsuranceComBranchID"); });

            Property<DateTime?>(x => x.MasaGaransi, 
              map => { map.Column("MasaGaransi"); });

             Property<string>(x => x.KeteranganAktiva,
              map => { map.Column("KeteranganAktiva"); });

            Property<string>(x => x.VendorID,
              map => { map.Column("VendorID"); });

            Property<string>(x => x.NoPO,
              map => { map.Column("NoPO"); });

            Property<string>(x => x.NoDO,
              map => { map.Column("NoDO"); }); 

            Property<string>(x => x.NoGSRN,
              map => { map.Column("NoGSRN"); });

            Property<string>(x => x.NoInvoice,
              map => { map.Column("NoInvoice"); });

            Property<string>(x => x.NoPaymentVoucher,
              map => { map.Column("NoPaymentVoucher"); });

            Property<DateTime?>(x => x.TanggalPerolehan,
              map => { map.Column("TanggalPerolehan"); });

            Property<string>(x => x.CurrencyID,
              map => { map.Column("CurrencyID"); });

            Property<double>(x => x.NilaiOriginal,
              map => { map.Column("NilaiOriginal"); });

            Property<double>(x => x.Kurs,
              map => { map.Column("Kurs"); });

            Property<double>(x => x.HargaPerolehan,
              map => { map.Column("HargaPerolehan"); });

            Property<string>(x => x.KeteranganPerolehan,
              map => { map.Column("KeteranganPerolehan"); });

            Property<DateTime?>(x => x.TanggalPerolehanKomersial,
              map => { map.Column("TanggalPerolehanKomersial"); });

            Property<double>(x => x.HargaPerolehanKomersial,
              map => { map.Column("HargaPerolehanKomersial"); });

            Property<bool>(x => x.KalkulasiOtomatis, 
              map => { map.Column("KalkulasiOtomatis"); });

            Property<DateTime?>(x => x.TanggalCatat, 
              map => { map.Column("TanggalCatat"); });

            Property<string>(x => x.MetodeDepresiasiKomersial,
              map => { map.Column("MetodeDepresiasiKomersial"); });

            Property<double>(x => x.MasaManfaatKomersial,
              map => { map.Column("MasaManfaatKomersial"); });

            Property<string>(x => x.PeriodeAwal,
              map => { map.Column("PeriodeAwal"); });

            Property<string>(x => x.PeriodeAkhir,
              map => { map.Column("PeriodeAkhir"); });

            Property<double>(x => x.Tarif,
              map => { map.Column("Tarif"); });

            Property<double>(x => x.ResiduKomersial,
              map => { map.Column("ResiduKomersial"); });

            Property<DateTime?>(x => x.TanggalPenghapusan,
              map => { map.Column("TanggalPenghapusan"); });

            Property<string>(x => x.NomorReferensi,
              map => { map.Column("NomorReferensi"); });

            Property<double>(x => x.HargaJual,
              map => { map.Column("HargaJual"); });

            Property<bool>(x => x.Penyusutandihitung,
              map => { map.Column("Penyusutandihitung"); });

            Property<string>(x => x.AlasanPengapusan,
               map => { map.Column("AlasanPengapusan"); });

            Property<DateTime?>(x => x.TanggalPerolehanFiskal,
              map => { map.Column("TanggalPerolehanFiskal"); });

            Property<double>(x => x.HargaPerolehanFiskal,
              map => { map.Column("HargaPerolehanFiskal"); });

            Property<bool>(x => x.KalkulasiOtomatisFiskal,
              map => { map.Column("KalkulasiOtomatisFiskal"); });

            Property<DateTime?>(x => x.TanggalCatatFiskal,
              map => { map.Column("TanggalCatatFiskal"); });

            Property<DateTime?>(x => x.TanggalTerimaAsset,
              map => { map.Column("TanggalTerimaAsset"); });

            Property<string>(x => x.GolonganPajakID,
              map => { map.Column("GolonganPajakID"); });

            Property<string>(x => x.MetodeDepresiasiFiskal,
              map => { map.Column("MetodeDepresiasiFiskal"); });

            Property<double>(x => x.MasaManfaatFiskal,
              map => { map.Column("MasaManfaatFiskal"); });

            Property<string>(x => x.PeriodeMulaiFiskal,
              map => { map.Column("PeriodeMulaiFiskal"); });

            Property<string>(x => x.PeriodeAkhirFiskal,
              map => { map.Column("PeriodeAkhirFiskal"); });

            Property<double>(x => x.TarifFiskal,
              map => { map.Column("TarifFiskal"); });

            Property<double>(x => x.Pembebanan,
              map => { map.Column("Pembebanan"); });

            Property<double>(x => x.ResiduFiskal,
              map => { map.Column("ResiduFiskal"); });

            Property<DateTime?>(x => x.TanggalPenghapusanFiskal,
              map => { map.Column("TanggalPenghapusanFiskal"); });

            Property<string>(x => x.NomorReferensiFiskal,
              map => { map.Column("NomorReferensiFiskal"); });

            Property<double>(x => x.HargaJualFiskal,
              map => { map.Column("HargaJualFiskal"); });

            Property<bool>(x => x.PenyusutanFiskal,
              map => { map.Column("PenyusutanFiskal"); });

            Property<string>(x => x.AlasanFiskal,
              map => { map.Column("AlasanFiskal"); });

            Property<double>(x => x.AkumulasiPenyusutanFiskal,
              map => { map.Column("AkumulasiPenyusutanFiskal"); });

            Property<double>(x => x.AkumulasiPenyusutanKomersial,
              map => { map.Column("AkumulasiPenyusutanKomersial"); });

            Property<double>(x => x.SisaNilaiBukuFiskal,
              map => { map.Column("SisaNilaiBukuFiskal"); });

            Property<double>(x => x.SisaNilaiBukuKomersial,
              map => { map.Column("SisaNilaiBukuKomersial"); });

            Property<string>(x => x.Status,
              map => { map.Column("Status"); });

            Property<bool>(x => x.IsOLS,
              map => { map.Column("IsOLS"); });

            Property<string>(x => x.AssetCode,
              map => { map.Column("AssetCode"); });

            Property<string>(x => x.NoPol, map => { map.Column("NoPol"); });
            Property<string>(x => x.STNK, map => { map.Column("STNK"); });
            Property<string>(x => x.BPKB, map => { map.Column("BPKB"); });
            Property<string>(x => x.NoRangka, map => { map.Column("NoRangka"); });
            Property<string>(x => x.NoMesin, map => { map.Column("NoMesin"); });
            Property<string>(x => x.NoPolis, map => { map.Column("NoPolis"); });
            Property<DateTime?>(x => x.TglPolis, map => { map.Column("TglPolis"); });

            Property<string>(x => x.NomorFaktur, map => { map.Column("NomorFaktur"); });
            Property<string>(x => x.NomorNIKVIN, map => { map.Column("NomorNIKVIN"); });
            Property<string>(x => x.FormANo, map => { map.Column("FormANo"); });
            Property<int>(x => x.TahunPembuatan, map => { map.Column("TahunPembuatan"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
