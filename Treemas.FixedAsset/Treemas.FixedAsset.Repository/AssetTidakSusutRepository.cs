﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Interface;

namespace Treemas.FixedAsset.Repository
{
    public class AssetTidakSusutRepository : RepositoryController<AssetTidakSusut>, IAssetTidakSusutRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AssetTidakSusutRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public AssetTidakSusut getAssetTidakSusut(long id)
        {
            return transact(() => session.QueryOver<AssetTidakSusut>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public bool IsDuplicate(long vendortypeid)
        {
            int rowCount = transact(() => session.QueryOver<AssetTidakSusut>()
                                                .Where(f => f.Id == vendortypeid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<AssetTidakSusut> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string id)
        {
            PagedResult<AssetTidakSusut> paged = new PagedResult<AssetTidakSusut>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AssetTidakSusut>()
                                                     .Where(x => x.AktivaId ==id)
                                                     .OrderBy(GetOrderByExpression<AssetTidakSusut>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AssetTidakSusut>()
                                                     .Where(x => x.AktivaId == id)
                                                     .OrderByDescending(GetOrderByExpression<AssetTidakSusut>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<AssetTidakSusut> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string id)
        {
            Specification<AssetTidakSusut> specification;

            switch (searchColumn)
            {
                
                case "AktivaId":
                    specification = new AdHocSpecification<AssetTidakSusut>(s => s.AktivaId.Contains(searchValue) && s.RowID ==Convert.ToInt32(id));
                    break;
                default:
                    specification = new AdHocSpecification<AssetTidakSusut>(s => s.Id.Equals(searchValue) && s.RowID ==Convert.ToInt32(id));
                    break;
            }

            PagedResult<AssetTidakSusut> paged = new PagedResult<AssetTidakSusut>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AssetTidakSusut>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<AssetTidakSusut>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AssetTidakSusut>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetTidakSusut>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
        
    }
}
