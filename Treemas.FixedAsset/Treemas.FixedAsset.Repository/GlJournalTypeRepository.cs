﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using NHibernate.Criterion;

namespace Treemas.FixedAsset.Repository
{
    public class GlJournalTypeRepository : RepositoryControllerString<GLJournalType>, IGLJournalTypeRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public GlJournalTypeRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public PagedResult<GLJournalType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<GLJournalType> paged = new PagedResult<GLJournalType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<GLJournalType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<GLJournalType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<GLJournalType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Conjunction conjuction = Restrictions.Conjunction();

            PagedResult<GLJournalType> paged = new PagedResult<GLJournalType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<GLJournalType>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<GLJournalType>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<GLJournalType>()
                                .Where(conjuction).RowCount());
            return paged;
        }

        public GLJournalType GetGLJournalType(string GLJournalTypeID)
        {
            return transact(() => session.QueryOver<GLJournalType>().Where(f => f.TransactionID == GLJournalTypeID).SingleOrDefault());
        }

        public bool IsDuplicate(string GLJournalTypeID)
        {
            int rowCount = transact(() => session.QueryOver<GLJournalType>()
                                               .Where(f => f.TransactionID == GLJournalTypeID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
