﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using NHibernate.Criterion;

namespace Treemas.FixedAsset.Repository
{
    public class InvoiceTypeRepository : RepositoryControllerString<InvoiceType>, IInvoiceTypeRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public InvoiceTypeRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public InvoiceType GetInvoiceType(string invoiceTypeID)
        {
            return transact(() => session.QueryOver<InvoiceType>().Where(f => f.InvoiceTypeID == invoiceTypeID).SingleOrDefault());
        }
        public bool IsDuplicate(string invoiceTypeID)
        {
            int rowCount = transact(() => session.QueryOver<InvoiceType>()
                                                .Where(f => f.InvoiceTypeID == invoiceTypeID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<InvoiceType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<InvoiceType> paged = new PagedResult<InvoiceType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<InvoiceType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<InvoiceType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<InvoiceType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {

            Conjunction conjuction = Restrictions.Conjunction();

            PagedResult<InvoiceType> paged = new PagedResult<InvoiceType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<InvoiceType>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<InvoiceType>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<InvoiceType>()
                                .Where(conjuction).RowCount());
            return paged;
        }
    }
}
