﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class InvoiceSequenceRepository : RepositoryControllerString<InvoiceSequence>, IInvoiceSequenceRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public InvoiceSequenceRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public InvoiceSequence getInvSeq(string id, int year)
        {
            return transact(() => session.QueryOver<InvoiceSequence>()
                                                .Where(f => f.SeqID == id && f.Year == year).SingleOrDefault());
        }

        public bool IsDuplicate(string id, int year)
        {
            int rowCount = transact(() => session.QueryOver<InvoiceSequence>()
                                                .Where(f => f.SeqID == id && f.Year == year).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
