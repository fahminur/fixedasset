﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class InvoiceRepository : RepositoryControllerString<InvoiceH>, IInvoiceRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public InvoiceRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }


        public InvoiceH getInvoice(string id)
        {
            return transact(() => session.QueryOver<InvoiceH>()
                                                .Where(f => f.InvoiceNo == id).SingleOrDefault());
        }
        public bool IsDuplicate(string coaclassid)
        {
            int rowCount = transact(() => session.QueryOver<InvoiceH>()
                                                .Where(f => f.InvoiceNo == coaclassid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public PagedResult<InvoiceH> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<InvoiceH> specification;
            switch (searchColumn)
            {
                case "InvoiceNo":
                    specification = new AdHocSpecification<InvoiceH>(s => s.InvoiceNo.Contains(searchValue) && s.Status == "1");
                    break;
                case "VendorID":
                    specification = new AdHocSpecification<InvoiceH>(s => s.VendorID.Contains(searchValue) && s.Status == "1");
                    break;
                //case "AssetAreaShortDesc":
                //    specification = new AdHocSpecification<InvoiceH>(s => s.AssetAreaShortDesc.Contains(searchValue));
                //    break;
                default:
                    specification = new AdHocSpecification<InvoiceH>(s => s.InvoiceNo.Like(searchValue) && s.Status == "1");
                    break;
            }

            PagedResult<InvoiceH> paged = new PagedResult<InvoiceH>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<InvoiceH>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<InvoiceH>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<InvoiceH>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<InvoiceH>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }


        public IList<InvoiceH> getInvoiceList(InvoiceFilter filter)
        {
            SimpleExpression VendorId = Restrictions.Eq("VendorID", filter.VendorId);
            SimpleExpression InvoiceDateFrom = Restrictions.Ge("InvoiceDate", filter.InvoiceDateFrom);
            SimpleExpression InvoiceDateTo = Restrictions.Le("InvoiceDate", filter.InvoiceDateTo);
            SimpleExpression VendorInvoiceNo = Restrictions.Like("VendorInvoiceNo", filter.VendorInvoiceNo);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.VendorId.IsNullOrEmpty()) conjuction.Add(VendorId);
            if (!filter.InvoiceDateFrom.IsNull()) conjuction.Add(InvoiceDateFrom);
            if (!filter.InvoiceDateTo.IsNull()) conjuction.Add(InvoiceDateTo);
            if (!filter.VendorInvoiceNo.IsNullOrEmpty()) conjuction.Add(VendorInvoiceNo);

            IList<InvoiceH> Items =  transact(() => session.QueryOver<InvoiceH>().Where(conjuction).List());
           
            return Items;
        }

        public PagedResult<InvoiceH> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, InvoiceFilter filter)
        {
            SimpleExpression VendorId = Restrictions.Eq("VendorID", filter.VendorId);
            SimpleExpression VendorInvoiceNo = Restrictions.Like("VendorInvoiceNo", filter.VendorInvoiceNo);
            SimpleExpression InvoiceDateFrom = Restrictions.Ge("InvoiceDate", filter.InvoiceDateFrom);
            SimpleExpression InvoiceDateTo = Restrictions.Le("InvoiceDate", filter.InvoiceDateTo);
            SimpleExpression PlanDisburseDateFrom = Restrictions.Ge("InvoiceOrderDate", filter.PlanDisburseDateFrom);
            SimpleExpression PlanDisburseDateTo = Restrictions.Le("InvoiceOrderDate", filter.PlanDisburseDateTo);
            SimpleExpression Status = Restrictions.Eq("Status", filter.InvoiceStatus);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.VendorId.IsNullOrEmpty()) conjuction.Add(VendorId);
            if (!filter.VendorInvoiceNo.IsNullOrEmpty()) conjuction.Add(VendorInvoiceNo);
            if (!filter.InvoiceDateFrom.IsNull()) conjuction.Add(InvoiceDateFrom);
            if (!filter.InvoiceDateTo.IsNull()) conjuction.Add(InvoiceDateTo);
            if (!filter.PlanDisburseDateFrom.IsNull()) conjuction.Add(PlanDisburseDateFrom);
            if (!filter.PlanDisburseDateTo.IsNull()) conjuction.Add(PlanDisburseDateTo);
            if (!filter.InvoiceStatus.IsNullOrEmpty()) conjuction.Add(Status);

            PagedResult<InvoiceH> paged = new PagedResult<InvoiceH>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<InvoiceH>()
                                 .Where(conjuction)
                                 //.And(s => s.Status == "N")
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<InvoiceH>()
                                 .Where(conjuction)
                                 //.And(s => s.Status == "N")
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<InvoiceH>()
                               .Where(conjuction)
                               //.And(s => s.Status == "N")
                               .RowCount());
            return paged;
        }


        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }


        public void AddInvoiceAktiva(InvoiceAktiva item)
        {
            transact(() => session.Save(item));
        }

        public IList<InvoiceAktiva> getInvoiceAktiva(string invoiceNo)
        {
            IList<InvoiceAktiva> list = transact(() => session.QueryOver<InvoiceAktiva>().Where(f => f.InvoiceNo == invoiceNo).List());

            return list;
        }

        public void AddInvoiceNonAktiva(InvoiceNonAktiva item)
        {
            transact(() => session.Save(item));
        }

        public IList<InvoiceNonAktiva> getInvoiceNonAktiva(string invoiceNo)
        {
            IList<InvoiceNonAktiva> list = transact(() => session.QueryOver<InvoiceNonAktiva>().Where(f => f.InvoiceNo == invoiceNo).List());

            return list;
        }

        public void AddInvoiceService(InvoiceService item)
        {
            transact(() => session.Save(item));
        }

        public IList<InvoiceService> getInvoiceService(string invoiceNo)
        {
            IList<InvoiceService> list = transact(() => session.QueryOver<InvoiceService>().Where(f => f.InvoiceNo == invoiceNo).List());

            return list;
        }

        public void DeleteInvoiceActiva(string invoiceNo)
        {
            string hqlDelete = "delete InvoiceAktiva where InvoiceNo = :InvoiceNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("InvoiceNo", invoiceNo)
                    .ExecuteUpdate();
        }
        public void DeleteInvoiceNonActiva(string invoiceNo)
        {
            string hqlDelete = "delete InvoiceNonAktiva where InvoiceNo = :InvoiceNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("InvoiceNo", invoiceNo)
                    .ExecuteUpdate();
        }
        public void DeleteInvoiceService(string invoiceNo)
        {
            string hqlDelete = "delete InvoiceService where InvoiceNo = :InvoiceNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("InvoiceNo", invoiceNo)
                    .ExecuteUpdate();
        }

        public InvoiceAktiva getSingleInvoiceAktiva(string AktivaId)
        {
            return transact(() => session.QueryOver<InvoiceAktiva>()
                                                .Where(f => f.StartingAktivaId == AktivaId).SingleOrDefault());
        }
    }
}
