﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class NamaHartaRepository : RepositoryControllerString<NamaHarta>, INamaHartaRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public NamaHartaRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public IEnumerable<NamaHarta> findActive()
        {
            Specification<NamaHarta> specification;
            specification = new AdHocSpecification<NamaHarta>(s => s.Status == true);

            return FindAll(specification);
        }

        public NamaHarta getNamaHarta(string id)
        {
            return transact(() => session.QueryOver<NamaHarta>()
                                                .Where(f => f.NamaHartaID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string namahartaid)
        {
            int rowCount = transact(() => session.QueryOver<NamaHarta>()
                                                .Where(f => f.NamaHartaID == namahartaid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<NamaHarta> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<NamaHarta> paged = new PagedResult<NamaHarta>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<NamaHarta>()
                                                     .OrderByDescending(GetOrderByExpression<NamaHarta>("Status"))
                                                     .ThenBy(GetOrderByExpression<NamaHarta>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<NamaHarta>()
                                                     .OrderByDescending(GetOrderByExpression<NamaHarta>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<NamaHarta>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<NamaHarta> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<NamaHarta> specification;
            switch (searchColumn)
            {
                case "NamaHartaID":
                    specification = new AdHocSpecification<NamaHarta>(s => s.NamaHartaID.Equals(searchValue));
                    break;
                case "NamaHartaDeskripsi":
                    specification = new AdHocSpecification<NamaHarta>(s => s.NamaHartaDeskripsi.Contains(searchValue));
                    break;
                //case "AssetAreaShortDesc":
                //    specification = new AdHocSpecification<NamaHarta>(s => s.AssetAreaShortDesc.Contains(searchValue));
                //    break;
                default:
                    specification = new AdHocSpecification<NamaHarta>(s => s.NamaHartaID.Equals(searchValue));
                    break;
            }

            PagedResult<NamaHarta> paged = new PagedResult<NamaHarta>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<NamaHarta>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<NamaHarta>("Status"))
                                 .ThenBy(GetOrderByExpression<NamaHarta>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<NamaHarta>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<NamaHarta>("Status"))
                                 .ThenByDescending(GetOrderByExpression<NamaHarta>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
