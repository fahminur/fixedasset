﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class AssetCoaRepository : RepositoryControllerString<AssetCoa>, IAssetCoaRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AssetCoaRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public IEnumerable<AssetCoa> findActive()
        {
            Specification<AssetCoa> specification;
            specification = new AdHocSpecification<AssetCoa>(s => s.Status == true);

            return FindAll(specification);
        }
        public IEnumerable<AssetCoa> findByGroupAsset(string CoaId)
        {
            Specification<AssetCoa> specification;
            specification = new AdHocSpecification<AssetCoa>(s => s.COAID == CoaId);

            return FindAll(specification);
        }

        public AssetCoa getAssetCoa(string id)
        {
            return transact(() => session.QueryOver<AssetCoa>()
                                                .Where(f => f.COAID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string assetcoaid)
        {
            int rowCount = transact(() => session.QueryOver<AssetCoa>()
                                                .Where(f => f.COAID == assetcoaid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<AssetCoa> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<AssetCoa> paged = new PagedResult<AssetCoa>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AssetCoa>()
                                                     .OrderByDescending(GetOrderByExpression<AssetCoa>("Status"))
                                                     .ThenBy(GetOrderByExpression<AssetCoa>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AssetCoa>()
                                                     .OrderByDescending(GetOrderByExpression<AssetCoa>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<AssetCoa>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

           // paged.TotalItems = Count;
            paged.TotalItems = paged.Items.Count();
            return paged;
        }
        public PagedResult<AssetCoa> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<AssetCoa> specification;
            switch (searchColumn)
            {
                case "COAID":
                    specification = new AdHocSpecification<AssetCoa>(s => s.COAID.Equals(searchValue));
                    break;
                case "COADescription":
                    specification = new AdHocSpecification<AssetCoa>(s => s.COADescription.Contains(searchValue));
                    break;
                //case "JenisUsahaShortDesc":
                //    specification = new AdHocSpecification<AssetCoa>(s => s.JenisUsahaShortDesc.Contains(searchValue));
                //    break;
                default:
                    specification = new AdHocSpecification<AssetCoa>(s => s.COAID.Equals(searchValue));
                    break;
            }

            PagedResult<AssetCoa> paged = new PagedResult<AssetCoa>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AssetCoa>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetCoa>("Status"))
                                 .ThenBy(GetOrderByExpression<AssetCoa>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AssetCoa>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetCoa>("Status"))
                                 .ThenByDescending(GetOrderByExpression<AssetCoa>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
