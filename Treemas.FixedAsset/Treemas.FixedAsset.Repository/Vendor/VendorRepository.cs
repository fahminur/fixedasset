﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class VendorRepository : RepositoryControllerString<Vendor>, IVendorRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public VendorRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public IEnumerable<Vendor> findActive()
        {
            Specification<Vendor> specification;
            specification = new AdHocSpecification<Vendor>(s => s.VendorId == "true");

            return FindAll(specification);
        }

        public Vendor getVendor(string id)
        {
            return transact(() => session.QueryOver<Vendor>()
                                                .Where(f => f.VendorId == id).SingleOrDefault());
        }
        public bool IsDuplicate(string Vendorid)
        {
            int rowCount = transact(() => session.QueryOver<Vendor>()
                                                .Where(f => f.VendorId == Vendorid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Vendor> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, VendorFilter filter)
        {
            SimpleExpression vendorName = Restrictions.Like("VendorName", filter.VendorName.Trim(), MatchMode.Anywhere);
            SimpleExpression contactPersonName = Restrictions.Like("ContactPersonName", filter.ContactPersonName.Trim(), MatchMode.Anywhere);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.VendorName.Trim().IsNullOrEmpty()) conjuction.Add(vendorName);
            if (!filter.ContactPersonName.Trim().IsNullOrEmpty()) conjuction.Add(contactPersonName);

            PagedResult<Vendor> paged = new PagedResult<Vendor>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<Vendor>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<Vendor>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<Vendor>()
                               .Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<Vendor> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<Vendor> paged = new PagedResult<Vendor>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Vendor>()
                                                     .OrderByDescending(GetOrderByExpression<Vendor>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Vendor>()
                                                     .OrderBy(GetOrderByExpression<Vendor>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Vendor> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Vendor> specification;
            switch (searchColumn)
            {
                case "VendorID":
                    specification = new AdHocSpecification<Vendor>(s => s.VendorId.Equals(searchValue));
                    break;
                //case "VendorDeskripsi":
                //    specification = new AdHocSpecification<Vendor>(s => s.VendorDeskripsi.Contains(searchValue));
                //    break;
                //case "AssetAreaShortDesc":
                //    specification = new AdHocSpecification<Vendor>(s => s.AssetAreaShortDesc.Contains(searchValue));
                //    break;
                default:
                    specification = new AdHocSpecification<Vendor>(s => s.VendorName.Contains(searchValue));
                    break;
            }

            PagedResult<Vendor> paged = new PagedResult<Vendor>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Vendor>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Vendor>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Vendor>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Vendor>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = transact(() =>
                        //session.QueryOver<Vendor>()
                        //       .Where(specification.ToExpression()).RowCount());
            session.Query<Vendor>()
                                 .Where(specification.ToExpression()).Count());

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
