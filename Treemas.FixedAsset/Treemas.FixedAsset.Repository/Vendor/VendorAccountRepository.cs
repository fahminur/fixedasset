﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class VendorAccountRepository : RepositoryControllerString<VendorAccount>, IVendorAccountRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public VendorAccountRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public VendorAccount getVendorAccount(string id)
        {
            return transact(() => session.QueryOver<VendorAccount>()
                                                .Where(f => f.VendorAccID == id).SingleOrDefault());
        }
        public IList<VendorAccount> getVendorAccbyVendor(string id)
        {
            return transact(() => session.QueryOver<VendorAccount>()
                                                .Where(f => f.VendorID == id).List());
        }
        public bool IsDuplicate(string VendorId, string VendorAccId)
        {
            int rowCount = transact(() => session.QueryOver<VendorAccount>()
                                                .Where(f => f.VendorID == VendorId && f.VendorAccID == VendorAccId).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public IEnumerable<VendorAccount> getBankByVendor(string VendorId)
        {
            Specification<VendorAccount> specification;
            specification = new AdHocSpecification<VendorAccount>(s => s.VendorID == VendorId);
            return FindAll(specification);
        }

        public PagedResult<VendorAccount> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string Vendorid)
        {
            PagedResult<VendorAccount> paged = new PagedResult<VendorAccount>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<VendorAccount>()
                                                     .Where(x => x.VendorID == Vendorid)
                                                     .OrderByDescending(GetOrderByExpression<VendorAccount>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<VendorAccount>()
                                                     .Where(x => x.VendorID == Vendorid)
                                                     .OrderBy(GetOrderByExpression<VendorAccount>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<VendorAccount> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string id)
        {
            Specification<VendorAccount> specification;

            switch (searchColumn)
            {
                case "VendorBankID":
                    specification = new AdHocSpecification<VendorAccount>(s => s.VendorBankID.Equals(searchValue));
                    break;
                case "VendorAccountName":
                    specification = new AdHocSpecification<VendorAccount>(s => s.VendorAccountName.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<VendorAccount>(s => s.VendorID == id);
                    break;
            }

            PagedResult<VendorAccount> paged = new PagedResult<VendorAccount>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<VendorAccount>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<VendorAccount>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<VendorAccount>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<VendorAccount>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public PagedResult<VendorAccount> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, VendorBankFilter filter)
        {
            SimpleExpression VendorAccountName = Restrictions.Like("VendorAccountName", filter.VendorAccountName.Trim(), MatchMode.Anywhere);
            SimpleExpression VendorBankID = Restrictions.Like("VendorBankID", filter.VendorBankID.Trim(), MatchMode.Anywhere);
            //SimpleExpression VendorAccountNo = Restrictions.Like("VendorAccountNo", filter.VendorAccountNo.Trim(), MatchMode.Anywhere);
            SimpleExpression VendorBankBranchID = Restrictions.Like("VendorBankBranch", filter.VendorBankBranchID.Trim(), MatchMode.Anywhere);
            SimpleExpression VendorID = Restrictions.Like("VendorID", filter.VendorID.Trim(), MatchMode.Anywhere); 

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.VendorAccountName.Trim().IsNullOrEmpty()) conjuction.Add(VendorAccountName);
            if (!filter.VendorBankID.Trim().IsNullOrEmpty()) conjuction.Add(VendorBankID);
            //if (!filter.VendorAccountNo.Trim().IsNullOrEmpty()) conjuction.Add(VendorAccountNo);
            if (!filter.VendorBankBranchID.Trim().IsNullOrEmpty()) conjuction.Add(VendorBankBranchID);
            if (!filter.VendorID.Trim().IsNullOrEmpty()) conjuction.Add(VendorID);

            PagedResult<VendorAccount> paged = new PagedResult<VendorAccount>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<VendorAccount>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<VendorAccount>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<VendorAccount>()
                               .Where(conjuction).RowCount());
            return paged;
        }
    }
}
