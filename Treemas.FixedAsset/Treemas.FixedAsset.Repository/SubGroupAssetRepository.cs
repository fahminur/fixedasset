﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class SubGroupAssetRepository : RepositoryControllerString<SubGroupAsset>, ISubGroupAssetRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SubGroupAssetRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public IEnumerable<SubGroupAsset> findActive()
        {
            Specification<SubGroupAsset> specification;
            specification = new AdHocSpecification<SubGroupAsset>(s => s.Status == true);

            return FindAll(specification);
        }
        public IEnumerable<SubGroupAsset> FindByGroupAsset(string groupAssetID)
        {
            Specification<SubGroupAsset> specification;
            specification = new AdHocSpecification<SubGroupAsset>(s => s.GroupAssetID == groupAssetID);

            return FindAll(specification);
        }

        public SubGroupAsset getSubGroupAsset(string id)
        {
            return transact(() => session.QueryOver<SubGroupAsset>()
                                                .Where(f => f.SubGroupAssetID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string subGroupAssetid)
        {
            int rowCount = transact(() => session.QueryOver<SubGroupAsset>()
                                                .Where(f => f.SubGroupAssetID == subGroupAssetid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<SubGroupAsset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<SubGroupAsset> paged = new PagedResult<SubGroupAsset>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<SubGroupAsset>()
                                                     .OrderByDescending(GetOrderByExpression<SubGroupAsset>("Status"))
                                                     .ThenBy(GetOrderByExpression<SubGroupAsset>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<SubGroupAsset>()
                                                     .OrderByDescending(GetOrderByExpression<SubGroupAsset>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<SubGroupAsset>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<SubGroupAsset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<SubGroupAsset> specification;
            switch (searchColumn)
            {
                case "SubGroupAssetID":
                    specification = new AdHocSpecification<SubGroupAsset>(s => s.SubGroupAssetID.Equals(searchValue));
                    break;
                case "SubGroupAssetDescription":
                    specification = new AdHocSpecification<SubGroupAsset>(s => s.SubGroupAssetDescription.Contains(searchValue));
                    break;
                //case "AssetAreaShortDesc":
                //    specification = new AdHocSpecification<SubGroupAsset>(s => s.AssetAreaShortDesc.Contains(searchValue));
                //    break;
                default:
                    specification = new AdHocSpecification<SubGroupAsset>(s => s.SubGroupAssetID.Equals(searchValue));
                    break;
            }

            PagedResult<SubGroupAsset> paged = new PagedResult<SubGroupAsset>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<SubGroupAsset>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<SubGroupAsset>("Status"))
                                 .ThenBy(GetOrderByExpression<SubGroupAsset>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<SubGroupAsset>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<SubGroupAsset>("Status"))
                                 .ThenByDescending(GetOrderByExpression<SubGroupAsset>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
