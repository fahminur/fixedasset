﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class DepresiasiKomersialRepository : RepositoryControllerString<DepresiasiKomersial>, IDepresiasiKomersialRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public DepresiasiKomersialRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public DepresiasiKomersial getDepresiasiKomersial(string id)
        {
            return transact(() => session.QueryOver<DepresiasiKomersial>()
                                                .Where(f => f.AktivaId == id).SingleOrDefault());
        }

        public DepresiasiKomersial getSaldoDepresiasiKomersial(string id, int year, int month)
        {
            return transact(() => session.QueryOver<DepresiasiKomersial>()
                                                .Where(f => f.AktivaId == id && f.Tahun == year.ToString() && f.Bulan == month.ToString()).SingleOrDefault());
        }

    }
}
