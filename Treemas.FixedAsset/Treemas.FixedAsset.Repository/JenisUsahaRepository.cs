﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class JenisUsahaRepository : RepositoryControllerString<JenisUsaha>, IJenisUsahaRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public JenisUsahaRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public IEnumerable<JenisUsaha> findActive()
        {
            Specification<JenisUsaha> specification;
            specification = new AdHocSpecification<JenisUsaha>(s => s.Status == true);

            return FindAll(specification);
        }

        public JenisUsaha getJenisUsaha(string id)
        {
            return transact(() => session.QueryOver<JenisUsaha>()
                                                .Where(f => f.JenisUsahaID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string jenisusahid)
        {
            int rowCount = transact(() => session.QueryOver<JenisUsaha>()
                                                .Where(f => f.JenisUsahaID == jenisusahid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<JenisUsaha> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<JenisUsaha> paged = new PagedResult<JenisUsaha>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<JenisUsaha>()
                                                     .OrderByDescending(GetOrderByExpression<JenisUsaha>("Status"))
                                                     .ThenBy(GetOrderByExpression<JenisUsaha>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<JenisUsaha>()
                                                     .OrderByDescending(GetOrderByExpression<JenisUsaha>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<JenisUsaha>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

           // paged.TotalItems = Count;
            paged.TotalItems = paged.Items.Count();
            return paged;
        }
        public PagedResult<JenisUsaha> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<JenisUsaha> specification;
            switch (searchColumn)
            {
                case "JenisUsahaID":
                    specification = new AdHocSpecification<JenisUsaha>(s => s.JenisUsahaID.Equals(searchValue));
                    break;
                case "NamaJenisUsaha":
                    specification = new AdHocSpecification<JenisUsaha>(s => s.NamaJenisUsaha.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<JenisUsaha>(s => s.JenisUsahaID.Equals(searchValue));
                    break;
            }

            PagedResult<JenisUsaha> paged = new PagedResult<JenisUsaha>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<JenisUsaha>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<JenisUsaha>("Status"))
                                 .ThenBy(GetOrderByExpression<JenisUsaha>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<JenisUsaha>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<JenisUsaha>("Status"))
                                 .ThenByDescending(GetOrderByExpression<JenisUsaha>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
