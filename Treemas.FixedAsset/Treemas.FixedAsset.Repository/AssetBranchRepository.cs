﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class AssetBranchRepository : RepositoryControllerString<AssetBranch>, IAssetBranchRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AssetBranchRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public AssetBranch getAssetBranch(string id)
        {
            return transact(() => session.QueryOver<AssetBranch>()
                                                .Where(f => f.AssetBranchID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string senddocid)
        {
            int rowCount = transact(() => session.QueryOver<AssetBranch>()
                                                .Where(f => f.AssetBranchID == senddocid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public IEnumerable<AssetBranch> findActive()
        {
            Specification<AssetBranch> specification;
            specification = new AdHocSpecification<AssetBranch>(s => s.Status == true);

            return FindAll(specification);
        }
        public PagedResult<AssetBranch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AssetBranch> paged = new PagedResult<AssetBranch>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AssetBranch>()
                                                     .OrderByDescending(GetOrderByExpression<AssetBranch>("Status"))
                                                     .ThenBy(GetOrderByExpression<AssetBranch>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AssetBranch>()
                                                     .OrderByDescending(GetOrderByExpression<AssetBranch>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<AssetBranch>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<AssetBranch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<AssetBranch> specification;

            switch (searchColumn)
            {
                case "DataId":
                    specification = new AdHocSpecification<AssetBranch>(s => s.AssetBranchID.Equals(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<AssetBranch>(s => s.AssetBranchDescription.Contains(searchValue));
                    break;
                case "ShortDesc":
                    specification = new AdHocSpecification<AssetBranch>(s => s.AssetBranchShortDesc.Contains(searchValue));
                    break;
              
                default:
                    specification = new AdHocSpecification<AssetBranch>(s => s.AssetBranchID.Equals(searchValue));
                    break;
            }

            PagedResult<AssetBranch> paged = new PagedResult<AssetBranch>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AssetBranch>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetBranch>("Status"))
                                 .ThenBy(GetOrderByExpression<AssetBranch>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AssetBranch>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetBranch>("Status"))
                                 .ThenByDescending(GetOrderByExpression<AssetBranch>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
