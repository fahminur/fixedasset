﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class NonAssetMasterRepository : RepositoryControllerString<NonAssetMaster>, INonAssetMasterRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public NonAssetMasterRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public PagedResult<NonAssetMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, NonAssetFilter nonAssetFilter)
        {
            SimpleExpression aktivaID = Restrictions.Like("NonAktivaID", nonAssetFilter.NonAktivaID.Trim(), MatchMode.Anywhere);
            SimpleExpression namaAktiva = Restrictions.Like("NamaNonAktiva", nonAssetFilter.NamaNonAktiva.Trim(), MatchMode.Anywhere);
            SimpleExpression groupAssetID = Restrictions.Eq("GroupAssetID", nonAssetFilter.GroupAssetID.Trim());
            SimpleExpression subGroupAssetID = Restrictions.Eq("SubGroupAssetID", nonAssetFilter.SubGroupAssetID.Trim());
            SimpleExpression siteID = Restrictions.Eq("SiteID", nonAssetFilter.SiteID.Trim());
            //SimpleExpression assetAreaID = Restrictions.Eq("AssetAreaID", nonAssetFilter.AssetAreaID.Trim());
            SimpleExpression assetDepID = Restrictions.Eq("AssetDepID", nonAssetFilter.AssetDepID.Trim());
            SimpleExpression tanggalPerolehanStart = Restrictions.Ge("TanggalPerolehan", nonAssetFilter.TanggalPerolehanStart);
            SimpleExpression tanggalPerolehanEnd = Restrictions.Le("TanggalPerolehan", nonAssetFilter.TanggalPerolehanEnd);
            SimpleExpression Status = Restrictions.Like("Status", nonAssetFilter.Status.Trim(), MatchMode.Anywhere);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!nonAssetFilter.SiteID.Trim().IsNullOrEmpty()) conjuction.Add(siteID);
            //if (!nonAssetFilter.AssetAreaID.Trim().IsNullOrEmpty()) conjuction.Add(assetAreaID);
            if (!nonAssetFilter.AssetDepID.Trim().IsNullOrEmpty()) conjuction.Add(assetDepID);
            if (!nonAssetFilter.GroupAssetID.Trim().IsNullOrEmpty()) conjuction.Add(groupAssetID);
            if (!nonAssetFilter.SubGroupAssetID.Trim().IsNullOrEmpty()) conjuction.Add(subGroupAssetID);
            if (!nonAssetFilter.NonAktivaID.Trim().IsNullOrEmpty()) conjuction.Add(aktivaID);
            if (!nonAssetFilter.NamaNonAktiva.Trim().IsNullOrEmpty()) conjuction.Add(namaAktiva);
            if (!nonAssetFilter.TanggalPerolehanStart.IsNull()) conjuction.Add(tanggalPerolehanStart);
            if (!nonAssetFilter.TanggalPerolehanEnd.IsNull()) conjuction.Add(tanggalPerolehanEnd);
            if (!nonAssetFilter.Status.IsNull()) conjuction.Add(Status);

            PagedResult<NonAssetMaster> paged = new PagedResult<NonAssetMaster>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<NonAssetMaster>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<NonAssetMaster>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<NonAssetMaster>()
                               .Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<NonAssetMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<NonAssetMaster> specification;
            switch (searchColumn)
            {
                case "NonAktivaID":
                    specification = new AdHocSpecification<NonAssetMaster>(s => s.NonAktivaID.Equals(searchValue));
                    break;
                case "NamaNonAktiva":
                    specification = new AdHocSpecification<NonAssetMaster>(s => s.NamaNonAktiva.Contains(searchValue));
                    break;
                //case "AssetAreaShortDesc":
                //    specification = new AdHocSpecification<NonAssetMaster>(s => s.AssetAreaShortDesc.Contains(searchValue));
                //    break;
                default:
                    specification = new AdHocSpecification<NonAssetMaster>(s => s.NonAktivaID.Equals(searchValue));
                    break;
            }

            PagedResult<NonAssetMaster> paged = new PagedResult<NonAssetMaster>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<NonAssetMaster>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<NonAssetMaster>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<NonAssetMaster>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<NonAssetMaster>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = transact(() =>
                          session.Query<NonAssetMaster>()
                                 .Where(specification.ToExpression()).Count());

            return paged;
        }

        public PagedResult<NonAssetMaster> FindAllPagedLookup(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<NonAssetMaster> paged = new PagedResult<NonAssetMaster>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<NonAssetMaster>()
                                                     .OrderBy(GetOrderByExpression<NonAssetMaster>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<NonAssetMaster>()
                                                     .OrderByDescending(GetOrderByExpression<NonAssetMaster>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public NonAssetMaster getNonAssetMaster(string id)
        {
            return transact(() => session.QueryOver<NonAssetMaster>()
                                                 .Where(f => f.NonAktivaID == id).SingleOrDefault());
        }

        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<NonAssetMaster>()
                                                .Where(f => f.NonAktivaID == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
