﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class GLJournalRepository: RepositoryControllerString<GLJournalH>, IGLJournalRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public GLJournalRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void AddGlJournalDtl(GLJournalD item)
        {
            transact(() => session.Save(item));
        }

        public GLJournalH GetJournalHdr(string id)
        {
            return transact(() => session.QueryOver<GLJournalH>()
                                    .Where(f => f.Tr_Nomor == id).SingleOrDefault());

        }

        public bool IsDuplicate(string trno)
        {
            int rowCount = transact(() => session.QueryOver<GLJournalH>()
                                                .Where(f => f.Tr_Nomor == trno).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public PagedResult<GLJournalH> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, GLJFilter filter)
        {
            SimpleExpression Tr_Nomor = Restrictions.Like("Tr_Nomor", filter.Tr_Nomor);
            SimpleExpression TransactionID = Restrictions.Eq("TransactionID", filter.TransactionID);
            SimpleExpression Tr_DateFrom = Restrictions.Ge("Tr_Date", filter.Tr_DateFrom);
            SimpleExpression Tr_DateTo = Restrictions.Le("Tr_Date", filter.Tr_DateTo);
            SimpleExpression BranchID = Restrictions.Eq("BranchID", filter.BranchID);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.Tr_Nomor.IsNullOrEmpty()) conjuction.Add(Tr_Nomor);
            if (!filter.TransactionID.IsNullOrEmpty()) conjuction.Add(TransactionID);
            if (!filter.BranchID.IsNullOrEmpty()) conjuction.Add(BranchID);
            if (!filter.Tr_DateFrom.IsNull()) conjuction.Add(Tr_DateFrom);
            if (!filter.Tr_DateTo.IsNull()) conjuction.Add(Tr_DateTo);

            PagedResult<GLJournalH> paged = new PagedResult<GLJournalH>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<GLJournalH>()
                                 .Where(conjuction)
                                 //.And(s => s.Status == "N")
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<GLJournalH>()
                                 .Where(conjuction)
                                 //.And(s => s.Status == "N")
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<GLJournalH>()
                               .Where(conjuction)
                               //.And(s => s.Status == "N")
                               .RowCount());
            return paged;
        }

        public IList<GLJournalD> getGLJournalDtl(string trxNo)
        {
            IList<GLJournalD> list = transact(() => session.QueryOver<GLJournalD>().Where(f => f.Tr_Nomor == trxNo).List());

            return list;

        }
    }
}
