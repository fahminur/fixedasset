﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using NHibernate.Criterion;

namespace Treemas.FixedAsset.Repository
{
    public class PaymentTypeRepository : RepositoryControllerString<PaymentType>, IPaymentTypeRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public PaymentTypeRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public PaymentType GetPaymentType(string PaymentTypeID)
        {
            return transact(() => session.QueryOver<PaymentType>().Where(f => f.PaymentTypeID == PaymentTypeID).SingleOrDefault());
        }
        public bool IsDuplicate(string PaymentTypeID)
        {
            int rowCount = transact(() => session.QueryOver<PaymentType>()
                                                .Where(f => f.PaymentTypeID == PaymentTypeID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<PaymentType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<PaymentType> paged = new PagedResult<PaymentType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<PaymentType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<PaymentType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<PaymentType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {

            Conjunction conjuction = Restrictions.Conjunction();

            PagedResult<PaymentType> paged = new PagedResult<PaymentType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<PaymentType>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<PaymentType>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<PaymentType>()
                                .Where(conjuction).RowCount());
            return paged;
        }
    }
}
