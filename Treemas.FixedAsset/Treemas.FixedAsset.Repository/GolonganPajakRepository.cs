﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class GolonganPajakRepository : RepositoryControllerString<GolonganPajak>, IGolonganPajakRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public GolonganPajakRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public GolonganPajak getGolonganPajak(string id)
        {
            return transact(() => session.QueryOver<GolonganPajak>()
                                                .Where(f => f.GolonganPajakID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string golonganPajakId)
        {
            int rowCount = transact(() => session.QueryOver<GolonganPajak>()
                                                .Where(f => f.GolonganPajakID == golonganPajakId).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public IEnumerable<GolonganPajak> findActive()
        {
            Specification<GolonganPajak> specification;
            specification = new AdHocSpecification<GolonganPajak>(s => s.Status == true);

            return FindAll(specification);
        }
        public PagedResult<GolonganPajak> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<GolonganPajak> paged = new PagedResult<GolonganPajak>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<GolonganPajak>()
                                                     .OrderByDescending(GetOrderByExpression<GolonganPajak>("Status"))
                                                     .ThenBy(GetOrderByExpression<GolonganPajak>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<GolonganPajak>()
                                                     .OrderByDescending(GetOrderByExpression<GolonganPajak>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<GolonganPajak>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<GolonganPajak> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<GolonganPajak> specification;

            switch (searchColumn)
            {
                case "GolonganPajakID":
                    specification = new AdHocSpecification<GolonganPajak>(s => s.GolonganPajakID.Equals(searchValue));
                    break;
                case "GolonganPajakDescription":
                    specification = new AdHocSpecification<GolonganPajak>(s => s.GolonganPajakDescription.Contains(searchValue));
                    break;
                //case "ShortDesc":
                //    specification = new AdHocSpecification<GolonganPajak>(s => s.AssetDeptShortDesc.Contains(searchValue));
                //    break;
              
                default:
                    specification = new AdHocSpecification<GolonganPajak>(s => s.GolonganPajakID.Equals(searchValue));
                    break;
            }

            PagedResult<GolonganPajak> paged = new PagedResult<GolonganPajak>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<GolonganPajak>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<GolonganPajak>("Status"))
                                 .ThenBy(GetOrderByExpression<GolonganPajak>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<GolonganPajak>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<GolonganPajak>(orderColumn))
                                 .ThenByDescending(GetOrderByExpression<GolonganPajak>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
