﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class PenghapusanAktivaRepository : RepositoryControllerString<PenghapusanAktivaHeader>, IPenghapusanAktivaRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public PenghapusanAktivaRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;

        }

        public void AddPenghapusanDetail(PenghapusanAktivaDetail item)
        {
            transact(() => session.Save(item));
        }

        public void DeletePenghapusanDetail(string id)
        {
            string hqlDelete = "delete PenghapusanAktivaDetail where DeleteNo = :DeleteNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("DeleteNo", id)
                    .ExecuteUpdate();
        }

        public PagedResult<PenghapusanAktivaHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, PenghapusanAktivaFilter filter)
        {
            SimpleExpression InternalMemoNo = Restrictions.Like("InternalMemoNo", filter.InternalMemoNo.Trim(), MatchMode.Anywhere);
            SimpleExpression DeleteDateFrom = Restrictions.Ge("DeleteDate", filter.DeleteDateFrom);
            SimpleExpression DeleteDateTo = Restrictions.Le("DeleteDate", filter.DeleteDateTo);
            SimpleExpression status = Restrictions.Eq("Status", filter.Status);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.InternalMemoNo.Trim().IsNullOrEmpty()) conjuction.Add(InternalMemoNo);
            if (!filter.DeleteDateFrom.IsNull()) conjuction.Add(DeleteDateFrom);
            if (!filter.DeleteDateTo.IsNull()) conjuction.Add(DeleteDateTo);
            if (!filter.Status.IsNull()) conjuction.Add(status);


            PagedResult<PenghapusanAktivaHeader> paged = new PagedResult<PenghapusanAktivaHeader>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<PenghapusanAktivaHeader>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<PenghapusanAktivaHeader>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                          session.QueryOver<PenghapusanAktivaHeader>()
                                 .Where(conjuction).RowCount());

            return paged;
        }

        public PenghapusanAktivaHeader getPenghapusanAktiva(string id)
        {
            return transact(() => session.QueryOver<PenghapusanAktivaHeader>()
                                                .Where(f => f.DeleteNo == id).SingleOrDefault());
        }

        public IList<PenghapusanAktivaDetail> getPenghapusanDetail(string id)
        {
            IList<PenghapusanAktivaDetail> list = transact(() => session.QueryOver<PenghapusanAktivaDetail>().Where(f => f.DeleteNo == id).List());

            return list;

        }

        public PenghapusanAktivaDetail getSinglePenghapusanDetail(string AktivaId)
        {
            return transact(() => session.QueryOver<PenghapusanAktivaDetail>()
                                                .Where(f => f.AktivaID == AktivaId).SingleOrDefault());
        }

        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<PenghapusanAktivaHeader>()
                                                .Where(f => f.DeleteNo == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
