﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;
using Treemas.Credential.Model;

namespace Treemas.FixedAsset.Repository
{
    public class FixedAssetMasterRepository : RepositoryControllerString<FixedAssetMaster>, IFixedAssetMasterRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public FixedAssetMasterRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        //public IEnumerable<FixedAssetMaster> findActive()
        //{
        //    Specification<FixedAssetMaster> specification;
        //    specification = new AdHocSpecification<FixedAssetMaster>(s => s.Status == true);

        //    return FindAll(specification);
        //}

        public FixedAssetMaster getFixedAssetMaster(string id)
        {
            return transact(() => session.QueryOver<FixedAssetMaster>()
                                                .Where(f => f.AktivaID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string coaclassid)
        {
            int rowCount = transact(() => session.QueryOver<FixedAssetMaster>()
                                                .Where(f => f.AktivaID == coaclassid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<FixedAssetMaster> FindAllPagedLookup(User usr, int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            Specification<FixedAssetMaster> specification;
            bool isHo = false;
            if (!usr.Site.IsNull())
            {
                isHo = usr.Site.Is_HO;
                specification = new AdHocSpecification<FixedAssetMaster>(m => m.SiteID.Equals(usr.Site.SiteID.Trim()));
            }
            else
            {
                specification = new AdHocSpecification<FixedAssetMaster>(m => m.SiteID.Equals("099"));
            }

            PagedResult<FixedAssetMaster> paged = new PagedResult<FixedAssetMaster>(pageNumber, itemsPerPage);

            if (orderKey.ToLower() == "asc")
            {
                if (isHo)
                {
                    paged.Items = transact(() => session.Query<FixedAssetMaster>()
                    .OrderBy(GetOrderByExpression<FixedAssetMaster>(orderColumn))
                    .Skip((pageNumber) * itemsPerPage)
                    .Take(itemsPerPage).ToList());
                }
                else
                {
                    paged.Items = transact(() => session.Query<FixedAssetMaster>()
                    .Where(specification.ToExpression())
                    .OrderBy(GetOrderByExpression<FixedAssetMaster>(orderColumn))
                    .Skip((pageNumber) * itemsPerPage)
                    .Take(itemsPerPage).ToList());
                }
            }
            else
            {
                if (isHo)
                {
                    paged.Items = transact(() => session.Query<FixedAssetMaster>()
                    .OrderByDescending(GetOrderByExpression<FixedAssetMaster>(orderColumn))
                    .Skip((pageNumber) * itemsPerPage)
                    .Take(itemsPerPage).ToList());
                }
                else
                {
                    paged.Items = transact(() => session.Query<FixedAssetMaster>()
                    .Where(specification.ToExpression())
                    .OrderByDescending(GetOrderByExpression<FixedAssetMaster>(orderColumn))
                    .Skip((pageNumber) * itemsPerPage)
                    .Take(itemsPerPage).ToList());
                }
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<FixedAssetMaster> FindAllPaged(User usr, int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            bool isHo = false;
            SimpleExpression siteID;

            if (!usr.Site.IsNull())
            {
                isHo = usr.Site.Is_HO;
                siteID = Restrictions.Eq("SiteID", usr.Site.SiteID.Trim());
            }
            else
            {
                siteID = Restrictions.Eq("SiteID", "099");
            }

            SimpleExpression aktivaID = Restrictions.Like("AktivaID", searchValue, MatchMode.Anywhere);
            SimpleExpression namaAktiva = Restrictions.Like("NamaAktiva", searchValue, MatchMode.Anywhere);

            Conjunction conjuction = Restrictions.Conjunction();

            if (!isHo)
            {
                conjuction.Add(siteID);
            }

            switch (searchColumn)
            {
                case "AktivaID":
                    conjuction.Add(aktivaID);
                    //specification = new AdHocSpecification<FixedAssetMaster>(s => s.AktivaID.Equals(searchValue));
                    break;
                case "NamaAktiva":
                    conjuction.Add(namaAktiva);
                    //specification = new AdHocSpecification<FixedAssetMaster>(s => s.NamaAktiva.Contains(searchValue));
                    break;
                    //case "AssetAreaShortDesc":
                    //    specification = new AdHocSpecification<FixedAssetMaster>(s => s.AssetAreaShortDesc.Contains(searchValue));
                    //    break;
                    //default:
                    //    specification = new AdHocSpecification<FixedAssetMaster>(s => s.AktivaID.Equals(searchValue));
                    //break;
            }

            PagedResult<FixedAssetMaster> paged = new PagedResult<FixedAssetMaster>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<FixedAssetMaster>()
                                     .Where(conjuction)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<FixedAssetMaster>()
                                     .Where(conjuction)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<FixedAssetMaster>()
                               .Where(conjuction).RowCount());

            return paged;
        }


        public PagedResult<FixedAssetMaster> FindAllPaged(User usr, int pageNumber, int itemsPerPage, string orderColumn, string orderKey, FixedAssetFilter fixedAssetFilter)
        {
            SimpleExpression aktivaID = Restrictions.Like("AktivaID", fixedAssetFilter.AktivaID.Trim(), MatchMode.Anywhere);
            SimpleExpression namaAktiva = Restrictions.Like("NamaAktiva", fixedAssetFilter.NamaAktiva.Trim(), MatchMode.Anywhere);
            SimpleExpression groupAssetID = Restrictions.Eq("GroupAssetID", fixedAssetFilter.GroupAssetID.Trim());
            SimpleExpression subGroupAssetID = Restrictions.Eq("SubGroupAssetID", fixedAssetFilter.SubGroupAssetID.Trim());
            SimpleExpression siteID = Restrictions.Eq("SiteID", fixedAssetFilter.SiteID.Trim());
            //SimpleExpression assetAreaID = Restrictions.Eq("AssetAreaID", fixedAssetFilter.AssetAreaID.Trim());
            SimpleExpression assetDepID = Restrictions.Eq("AssetDepID", fixedAssetFilter.AssetDepID.Trim());
            SimpleExpression tanggalPerolehanStart = Restrictions.Ge("TanggalPerolehan", fixedAssetFilter.TanggalPerolehanStart);
            SimpleExpression tanggalPerolehanEnd = Restrictions.Le("TanggalPerolehan", fixedAssetFilter.TanggalPerolehanEnd);
            SimpleExpression Status = Restrictions.Like("Status", fixedAssetFilter.Status.Trim(), MatchMode.Anywhere);
            //SimpleExpression isOls = Restrictions.Eq("IsOLS", Convert.ToBoolean(fixedAssetFilter.IsOls));
            SimpleExpression noMesin = Restrictions.Like("NoMesin", fixedAssetFilter.NoMesin.Trim(), MatchMode.Anywhere);
            SimpleExpression noRangka = Restrictions.Like("NoRangka", fixedAssetFilter.NoRangka.Trim(), MatchMode.Anywhere);

            

            Conjunction conjuction = Restrictions.Conjunction();
            if (!fixedAssetFilter.SiteID.Trim().IsNullOrEmpty()) conjuction.Add(siteID);
            //if (!fixedAssetFilter.AssetAreaID.Trim().IsNullOrEmpty()) conjuction.Add(assetAreaID);
            if (!fixedAssetFilter.AssetDepID.Trim().IsNullOrEmpty()) conjuction.Add(assetDepID);
            if (!fixedAssetFilter.GroupAssetID.Trim().IsNullOrEmpty()) conjuction.Add(groupAssetID);
            if (!fixedAssetFilter.SubGroupAssetID.Trim().IsNullOrEmpty()) conjuction.Add(subGroupAssetID);
            if (!fixedAssetFilter.NamaAktiva.Trim().IsNullOrEmpty()) conjuction.Add(namaAktiva);
            if (!fixedAssetFilter.AktivaID.Trim().IsNullOrEmpty()) conjuction.Add(aktivaID);
            if (!fixedAssetFilter.TanggalPerolehanStart.IsNull()) conjuction.Add(tanggalPerolehanStart);
            if (!fixedAssetFilter.TanggalPerolehanEnd.IsNull()) conjuction.Add(tanggalPerolehanEnd);
            if (!fixedAssetFilter.Status.IsNull()) conjuction.Add(Status);
            //if (!fixedAssetFilter.IsOls.IsNullOrEmpty()) conjuction.Add(isOls);
            if (!fixedAssetFilter.NoMesin.IsNullOrEmpty()) conjuction.Add(noMesin);
            if (!fixedAssetFilter.NoRangka.IsNullOrEmpty()) conjuction.Add(noRangka);

            PagedResult<FixedAssetMaster> paged = new PagedResult<FixedAssetMaster>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                if (fixedAssetFilter.IsOls == "")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<FixedAssetMaster>()
                                     .Where(conjuction)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                             session.QueryOver<FixedAssetMaster>()
                                    .Where(conjuction)
                                    .And(a => a.IsOLS == (fixedAssetFilter.IsOls == "1" ? true : false))
                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                    .Skip((pageNumber) * itemsPerPage)
                                    .Take(itemsPerPage).List());
                }
            }
            else
            {
                if (fixedAssetFilter.IsOls == "")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<FixedAssetMaster>()
                                     .Where(conjuction)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                             session.QueryOver<FixedAssetMaster>()
                                    .Where(conjuction)
                                    .And(a => a.IsOLS == (fixedAssetFilter.IsOls == "1" ? true : false))
                                    .OrderBy(Projections.Property(orderColumn)).Desc
                                    .Skip((pageNumber) * itemsPerPage)
                                    .Take(itemsPerPage).List());
                }
            }
            if (fixedAssetFilter.IsOls == "")
            {
                paged.TotalItems = transact(() =>
                        session.QueryOver<FixedAssetMaster>()
                               .Where(conjuction).RowCount());
            }
            else
            {
                paged.TotalItems = transact(() =>
                        session.QueryOver<FixedAssetMaster>()
                               .Where(conjuction)
                               .And(a => a.IsOLS == (fixedAssetFilter.IsOls == "1" ? true : false)).RowCount());

            }
            return paged;
        }


        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        //public PagedResult<FixedAssetMaster> FindAllPagedLookupSales(User usr, int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        //{
        //    Specification<FixedAssetMaster> specification;
        //    bool isHo = false;
        //    if (!usr.Site.IsNull())
        //    {
        //        isHo = usr.Site.Is_HO;
        //        specification = new AdHocSpecification<FixedAssetMaster>(m => m.SiteID.Equals(usr.Site.SiteID.Trim()) && m.Status.Equals("A"));
        //    }
        //    else
        //    {
        //        specification = new AdHocSpecification<FixedAssetMaster>(m => m.SiteID.Equals("099") && m.Status.Equals("A"));
        //    }            

        //    PagedResult<FixedAssetMaster> paged = new PagedResult<FixedAssetMaster>(pageNumber, itemsPerPage);

        //    if (orderKey.ToLower() == "asc")
        //    {
        //        if (isHo)
        //        {
        //            paged.Items = transact(() => session.Query<FixedAssetMaster>()
        //            .OrderBy(GetOrderByExpression<FixedAssetMaster>(orderColumn))
        //            .Skip((pageNumber) * itemsPerPage)
        //            .Take(itemsPerPage).ToList());
        //        }
        //        else
        //        {
        //            paged.Items = transact(() => session.Query<FixedAssetMaster>()
        //            .Where(specification.ToExpression())
        //            .OrderBy(GetOrderByExpression<FixedAssetMaster>(orderColumn))
        //            .Skip((pageNumber) * itemsPerPage)
        //            .Take(itemsPerPage).ToList());
        //        }
        //    }
        //    else
        //    {
        //        if (isHo)
        //        {
        //            paged.Items = transact(() => session.Query<FixedAssetMaster>()
        //            .OrderByDescending(GetOrderByExpression<FixedAssetMaster>(orderColumn))
        //            .Skip((pageNumber) * itemsPerPage)
        //            .Take(itemsPerPage).ToList());
        //        }
        //        else
        //        {
        //            paged.Items = transact(() => session.Query<FixedAssetMaster>()
        //            .Where(specification.ToExpression())
        //            .OrderByDescending(GetOrderByExpression<FixedAssetMaster>(orderColumn))
        //            .Skip((pageNumber) * itemsPerPage)
        //            .Take(itemsPerPage).ToList());
        //        }
        //    }

        //    paged.TotalItems = Count;
        //    return paged;
        //}

        //public PagedResult<FixedAssetMaster> FindAllPagedLookupSales(User usr, int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        //{
        //    bool isHo = false;
        //    SimpleExpression siteID;

        //    if (!usr.Site.IsNull())
        //    {
        //        isHo = usr.Site.Is_HO;
        //        siteID = Restrictions.Eq("SiteID", usr.Site.SiteID.Trim());
        //    }
        //    else
        //    {
        //        siteID = Restrictions.Eq("SiteID", "099");
        //    }

        //    SimpleExpression aktivaID = Restrictions.Like("AktivaID", searchValue, MatchMode.Anywhere);
        //    SimpleExpression namaAktiva = Restrictions.Like("NamaAktiva", searchValue, MatchMode.Anywhere);
        //    SimpleExpression isSales = Restrictions.Eq("Status", "A");

        //    Conjunction conjuction = Restrictions.Conjunction();

        //    if (!isHo)
        //    {
        //        conjuction.Add(siteID);
        //    }

        //    switch (searchColumn)
        //    {
        //        case "AktivaID":
        //            conjuction.Add(aktivaID);
        //            break;
        //        case "NamaAktiva":
        //            conjuction.Add(namaAktiva);
        //            break;
        //    }

        //    conjuction.Add(isSales);

        //    PagedResult<FixedAssetMaster> paged = new PagedResult<FixedAssetMaster>(pageNumber, itemsPerPage);
        //    if (orderKey.ToLower() == "asc")
        //    {
        //        paged.Items = transact(() =>
        //                  session.QueryOver<FixedAssetMaster>()
        //                             .Where(conjuction)
        //                             .OrderBy(Projections.Property(orderColumn)).Asc
        //                             .Skip((pageNumber) * itemsPerPage)
        //                             .Take(itemsPerPage).List());
        //    }
        //    else
        //    {
        //        paged.Items = transact(() =>
        //                  session.QueryOver<FixedAssetMaster>()
        //                             .Where(conjuction)
        //                             .OrderBy(Projections.Property(orderColumn)).Desc
        //                             .Skip((pageNumber) * itemsPerPage)
        //                             .Take(itemsPerPage).List());
        //    }

        //    paged.TotalItems = transact(() =>
        //                session.QueryOver<FixedAssetMaster>()
        //                       .Where(conjuction).RowCount());

        //    return paged;
        //}
    }
}
