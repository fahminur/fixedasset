﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class PenjualanAktivaRepository : RepositoryControllerString<PenjualanAktivaHeader>, IPenjualanAktivaRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public PenjualanAktivaRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public PenjualanAktivaHeader getPenjualanAktiva(string id)
        {
            return transact(() => session.QueryOver<PenjualanAktivaHeader>()
                                                .Where(f => f.SellingNo == id).SingleOrDefault());
        }
        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<PenjualanAktivaHeader>()
                                                .Where(f => f.SellingNo == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<PenjualanAktivaHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, PenjualanAktivaFilter filter)
        {
            SimpleExpression tanggalPenjualanFrom = Restrictions.Ge("SellingDate", filter.TanggalPenjualanFrom);
            SimpleExpression tanggalPenjualanTo = Restrictions.Le("SellingDate", filter.TanggalPenjualanTo);
            SimpleExpression internalmemo = Restrictions.Like("InternalMemoNo", filter.InternalMemoNo);
            SimpleExpression reference = Restrictions.Like("ReferenceNo", filter.ReferenceNo);
            SimpleExpression status = Restrictions.Eq("Status", filter.Status);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.InternalMemoNo.Trim().IsNullOrEmpty()) conjuction.Add(internalmemo);
            if (!filter.ReferenceNo.Trim().IsNullOrEmpty()) conjuction.Add(reference);
            if (!filter.TanggalPenjualanFrom.IsNull()) conjuction.Add(tanggalPenjualanFrom);
            if (!filter.TanggalPenjualanTo.IsNull()) conjuction.Add(tanggalPenjualanTo);
            if (!filter.Status.IsNull()) conjuction.Add(status);

            PagedResult<PenjualanAktivaHeader> paged = new PagedResult<PenjualanAktivaHeader>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<PenjualanAktivaHeader>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<PenjualanAktivaHeader>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                          session.QueryOver<PenjualanAktivaHeader>()
                                 .Where(conjuction).RowCount());

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public IList<PenjualanAktivaDetail> getPenjualanDetail(string id)
        {
            IList<PenjualanAktivaDetail> list = transact(() => session.QueryOver<PenjualanAktivaDetail>().Where(f => f.SellingNo == id).List());

            return list;
        }

        public void AddPenjualanDetail(PenjualanAktivaDetail item)
        {
            transact(() => session.Save(item));
        }

        public void DeletePenjualanDetail(string sellingNo)
        {
            string hqlDelete = "delete PenjualanAktivaDetail where SellingNo = :SellingNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("SellingNo", sellingNo)
                    .ExecuteUpdate();
        }
        public void CreateJournalPenjualan(string sellingNo)
        {
            string hqlJournal = "EXEC uSP_GenerateGLJPenjualanAktiva @SellingNo = :SellingNo";
            IQuery query = (IQuery)session.CreateQuery("EXEC uSP_GenerateGLJPenjualanAktiva @SellingNo = :SellingNo");
            int deletedEntities = session.CreateQuery(hqlJournal)
            .SetString("SellingNo", sellingNo)
                    .ExecuteUpdate();
        }

        public PenjualanAktivaDetail getSinglePenjualanDetail(string AktivaId)
        {
            return transact(() => session.QueryOver<PenjualanAktivaDetail>().Where(f => f.AktivaID == AktivaId).SingleOrDefault());
        }
    }
}
