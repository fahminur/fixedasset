﻿using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.Base.Repository;
using System;

namespace Treemas.FixedAsset.Repository
{
    public class PenjualanAktivaSequenceRepository : RepositoryControllerString<PenjualanAktivaSequence>, IPenjualanAktivaSequenceRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public PenjualanAktivaSequenceRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public PenjualanAktivaSequence getPenjualanSeq(string id, int year)
        {
            return transact(() => session.QueryOver<PenjualanAktivaSequence>()
                                                .Where(f => f.SeqID == id && f.Year == year).SingleOrDefault());
        }

        public bool IsDuplicate(string id, int year)
        {
            int rowCount = transact(() => session.QueryOver<PenerimaanBarangSequence>()
                                                .Where(f => f.SeqID == id && f.Year == year).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
