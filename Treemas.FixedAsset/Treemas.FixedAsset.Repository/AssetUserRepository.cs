﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class AssetUserRepository : RepositoryControllerString<AssetUser>, IAssetUserRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AssetUserRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public AssetUser getAssetUser(string id)
        {
            return transact(() => session.QueryOver<AssetUser>()
                                                .Where(f => f.AssetUserID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string senddocid)
        {
            int rowCount = transact(() => session.QueryOver<AssetUser>()
                                                .Where(f => f.AssetUserID == senddocid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public IEnumerable<AssetUser> findActive()
        {
            Specification<AssetUser> specification;
            specification = new AdHocSpecification<AssetUser>(s => s.Status == true);

            return FindAll(specification);
        }
        public PagedResult<AssetUser> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AssetUser> paged = new PagedResult<AssetUser>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AssetUser>()
                                                     .OrderByDescending(GetOrderByExpression<AssetUser>("Status"))
                                                     .ThenBy(GetOrderByExpression<AssetUser>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AssetUser>()
                                                     .OrderByDescending(GetOrderByExpression<AssetUser>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<AssetUser>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<AssetUser> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<AssetUser> specification;

            switch (searchColumn)
            {
                case "DataId":
                    specification = new AdHocSpecification<AssetUser>(s => s.AssetUserID.Contains(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<AssetUser>(s => s.AssetUserName.Contains(searchValue));
                    break;
                //case "ShortDesc":
                //    specification = new AdHocSpecification<AssetUser>(s => s.AssetUserShortDesc.Contains(searchValue));
                //    break;
              
                default:
                    specification = new AdHocSpecification<AssetUser>(s => s.AssetUserID.Contains(searchValue));
                    break;
            }

            PagedResult<AssetUser> paged = new PagedResult<AssetUser>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AssetUser>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetUser>("Status"))
                                 .ThenBy(GetOrderByExpression<AssetUser>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AssetUser>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetUser>("Status"))
                                 .ThenByDescending(GetOrderByExpression<AssetUser>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public IEnumerable<AssetUser> FindByBranchID(string BranchID)
        {
            Specification<AssetUser> specification;
            specification = new AdHocSpecification<AssetUser>(s => s.AssetBranchID == BranchID);

            return FindAll(specification);
        }

        public IEnumerable<AssetUser> FindByDeptID(string BranchID, string DeptID)
        {
            Specification<AssetUser> specification;
            specification = new AdHocSpecification<AssetUser>(s => s.AssetBranchID == BranchID && s.AssetDeptID == DeptID);

            return FindAll(specification);
        }
    }
}
