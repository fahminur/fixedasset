﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using NHibernate.Criterion;

namespace Treemas.FixedAsset.Repository
{
    public class MateraiRepository : RepositoryControllerString<Materai>, IMateraiRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public MateraiRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public Materai GetMaterai(string MateraiID)
        {
            return transact(() => session.QueryOver<Materai>().Where(f => f.MateraiID == MateraiID).SingleOrDefault());
        }
        public bool IsDuplicate(string MateraiID)
        {
            int rowCount = transact(() => session.QueryOver<Materai>()
                                                .Where(f => f.MateraiID == MateraiID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Materai> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<Materai> paged = new PagedResult<Materai>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<Materai>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<Materai>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Materai> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {

            Conjunction conjuction = Restrictions.Conjunction();

            PagedResult<Materai> paged = new PagedResult<Materai>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<Materai>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<Materai>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<Materai>()
                                .Where(conjuction).RowCount());
            return paged;
        }
    }
}
