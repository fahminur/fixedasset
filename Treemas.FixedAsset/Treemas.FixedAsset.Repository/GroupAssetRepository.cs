﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class GroupAssetRepository : RepositoryControllerString<GroupAsset>, IGroupAssetRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public GroupAssetRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public GroupAsset getGroupAsset(string id)
        {
            return transact(() => session.QueryOver<GroupAsset>()
                                                .Where(f => f.GroupAssetID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string senddocid)
        {
            int rowCount = transact(() => session.QueryOver<GroupAsset>()
                                                .Where(f => f.GroupAssetID == senddocid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public IEnumerable<GroupAsset> findActive()
        {
            Specification<GroupAsset> specification;
            specification = new AdHocSpecification<GroupAsset>(s => s.Status == true);

            return FindAll(specification);
        }
        public PagedResult<GroupAsset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<GroupAsset> paged = new PagedResult<GroupAsset>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<GroupAsset>()
                                                     .OrderByDescending(GetOrderByExpression<GroupAsset>("Status"))
                                                     .ThenBy(GetOrderByExpression<GroupAsset>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<GroupAsset>()
                                                     .OrderByDescending(GetOrderByExpression<GroupAsset>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<GroupAsset>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<GroupAsset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<GroupAsset> specification;

            switch (searchColumn)
            {
                case "DataId":
                    specification = new AdHocSpecification<GroupAsset>(s => s.GroupAssetID.Equals(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<GroupAsset>(s => s.GroupAssetDescription.Contains(searchValue));
                    break;
                case "ShortDesc":
                    specification = new AdHocSpecification<GroupAsset>(s => s.GroupAssetShortDesc.Contains(searchValue));
                    break;
                case "CoaPerolehan":
                    specification = new AdHocSpecification<GroupAsset>(s => s.CoaPerolehan.Contains(searchValue));
                    break;
                case "CoaAkumulasi":
                    specification = new AdHocSpecification<GroupAsset>(s => s.CoaAkumulasi.Contains(searchValue));
                    break;
                case "CoaBeban":
                    specification = new AdHocSpecification<GroupAsset>(s => s.CoaBeban.Contains(searchValue));
                    break;

                default:
                    specification = new AdHocSpecification<GroupAsset>(s => s.GroupAssetID.Equals(searchValue));
                    break;
            }

            PagedResult<GroupAsset> paged = new PagedResult<GroupAsset>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<GroupAsset>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<GroupAsset>("Status"))
                                 .ThenBy(GetOrderByExpression<GroupAsset>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<GroupAsset>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<GroupAsset>("Status"))
                                 .ThenByDescending(GetOrderByExpression<GroupAsset>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
