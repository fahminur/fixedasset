﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class MutasiAktivaRepository : RepositoryControllerString<MutasiAktiva>, IMutasiAktivaRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public MutasiAktivaRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        //public IEnumerable<MutasiAktiva> findActive()
        //{
        //    Specification<MutasiAktiva> specification;
        //    specification = new AdHocSpecification<MutasiAktiva>(s => s.Status == true);

        //    return FindAll(specification);
        //}

        public MutasiAktiva getMutasiAktiva(string id)
        {
            return transact(() => session.QueryOver<MutasiAktiva>()
                                                .Where(f => f.AktivaID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string coaclassid)
        {
            int rowCount = transact(() => session.QueryOver<MutasiAktiva>()
                                                .Where(f => f.AktivaID == coaclassid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<MutasiAktiva> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, MutasiAktivaFilter filter)
        {
            SimpleExpression aktivaID = Restrictions.Like("AktivaID", filter.AktivaID.Trim(),MatchMode.Anywhere);
            //SimpleExpression namaAktiva = Restrictions.Like("NamaAktiva", filter.NamaAktiva.Trim(), MatchMode.Anywhere);
            SimpleExpression groupAssetID = Restrictions.Eq("GroupAssetID", filter.GroupAssetID.Trim());
            SimpleExpression subGroupAssetID = Restrictions.Eq("SubGroupAssetID", filter.SubGroupAssetID.Trim());
            SimpleExpression tanggalMutasiFrom = Restrictions.Ge("TanggalMutasi", filter.TanggalMutasiFrom);
            SimpleExpression tanggalMutasiTo = Restrictions.Le("TanggalMutasi", filter.TanggalMutasiTo);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.AktivaID.Trim().IsNullOrEmpty()) conjuction.Add(aktivaID);
            //if (!filter.NamaAktiva.Trim().IsNullOrEmpty()) conjuction.Add(namaAktiva);
            if (!filter.GroupAssetID.Trim().IsNullOrEmpty()) conjuction.Add(groupAssetID);
            if (!filter.SubGroupAssetID.Trim().IsNullOrEmpty()) conjuction.Add(subGroupAssetID);
            if (!filter.TanggalMutasiFrom.IsNull()) conjuction.Add(tanggalMutasiFrom);
            if (!filter.TanggalMutasiTo.IsNull()) conjuction.Add(tanggalMutasiTo);

            PagedResult<MutasiAktiva> paged = new PagedResult<MutasiAktiva>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<MutasiAktiva>()
                                 .Where(conjuction)
                                 //.OrderBy(Projections.Property("Status")).Desc
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<MutasiAktiva>()
                                 .Where(conjuction)
                                 //.OrderBy(Projections.Property("Status")).Desc
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                          session.QueryOver<MutasiAktiva>()
                                 .Where(conjuction).RowCount());

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
