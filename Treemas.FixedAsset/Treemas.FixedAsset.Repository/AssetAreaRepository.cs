﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class AssetAreaRepository : RepositoryControllerString<AssetArea>, IAssetAreaRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AssetAreaRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public IEnumerable<AssetArea> findActive()
        {
            Specification<AssetArea> specification;
            specification = new AdHocSpecification<AssetArea>(s => s.Status == true);

            return FindAll(specification);
        }

        public AssetArea getAssetArea(string id)
        {
            return transact(() => session.QueryOver<AssetArea>()
                                                .Where(f => f.AssetAreaID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string coaclassid)
        {
            int rowCount = transact(() => session.QueryOver<AssetArea>()
                                                .Where(f => f.AssetAreaID == coaclassid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<AssetArea> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<AssetArea> paged = new PagedResult<AssetArea>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AssetArea>()
                                                     .OrderByDescending(GetOrderByExpression<AssetArea>("Status"))
                                                     .ThenBy(GetOrderByExpression<AssetArea>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AssetArea>()
                                                     .OrderByDescending(GetOrderByExpression<AssetArea>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<AssetArea>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<AssetArea> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<AssetArea> specification;
            switch (searchColumn)
            {
                case "AssetAreaID":
                    specification = new AdHocSpecification<AssetArea>(s => s.AssetAreaID.Equals(searchValue));
                    break;
                case "AssetAreaDescription":
                    specification = new AdHocSpecification<AssetArea>(s => s.AssetAreaDescription.Contains(searchValue));
                    break;
                case "AssetAreaShortDesc":
                    specification = new AdHocSpecification<AssetArea>(s => s.AssetAreaShortDesc.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<AssetArea>(s => s.AssetAreaID.Equals(searchValue));
                    break;
            }

            PagedResult<AssetArea> paged = new PagedResult<AssetArea>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AssetArea>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetArea>("Status"))
                                 .ThenBy(GetOrderByExpression<AssetArea>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AssetArea>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetArea>("Status"))
                                 .ThenByDescending(GetOrderByExpression<AssetArea>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
