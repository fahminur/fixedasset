﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class AssetConditionRepository : RepositoryControllerString<AssetCondition>, IAssetConditionRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AssetConditionRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public AssetCondition getAssetCondition(string id)
        {
            return transact(() => session.QueryOver<AssetCondition>()
                                                .Where(f => f.AssetConditionID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string senddocid)
        {
            int rowCount = transact(() => session.QueryOver<AssetCondition>()
                                                .Where(f => f.AssetConditionID == senddocid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public IEnumerable<AssetCondition> findActive()
        {
            Specification<AssetCondition> specification;
            specification = new AdHocSpecification<AssetCondition>(s => s.Status == true);

            return FindAll(specification);
        }
        public PagedResult<AssetCondition> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AssetCondition> paged = new PagedResult<AssetCondition>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AssetCondition>()
                                                     .OrderByDescending(GetOrderByExpression<AssetCondition>("Status"))
                                                     .ThenBy(GetOrderByExpression<AssetCondition>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AssetCondition>()
                                                     .OrderByDescending(GetOrderByExpression<AssetCondition>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<AssetCondition>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<AssetCondition> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<AssetCondition> specification;

            switch (searchColumn)
            {
                case "DataId":
                    specification = new AdHocSpecification<AssetCondition>(s => s.AssetConditionID.Equals(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<AssetCondition>(s => s.AssetConditionDescription.Contains(searchValue));
                    break;
                case "ShortDesc":
                    specification = new AdHocSpecification<AssetCondition>(s => s.AssetConditionShortDesc.Contains(searchValue));
                    break;
              
                default:
                    specification = new AdHocSpecification<AssetCondition>(s => s.AssetConditionID.Equals(searchValue));
                    break;
            }

            PagedResult<AssetCondition> paged = new PagedResult<AssetCondition>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AssetCondition>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetCondition>("Status"))
                                 .ThenBy(GetOrderByExpression<AssetCondition>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AssetCondition>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetCondition>("Status"))
                                 .ThenByDescending(GetOrderByExpression<AssetCondition>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
