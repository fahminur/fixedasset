﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Repository
{
    public class AssetLocationRepository : RepositoryControllerString<AssetLocation>, IAssetLocationRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AssetLocationRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public AssetLocation getAssetLocation(string id)
        {
            return transact(() => session.QueryOver<AssetLocation>()
                                                .Where(f => f.AssetLocationID == id).SingleOrDefault());
        }
        public bool IsDuplicate(string senddocid)
        {
            int rowCount = transact(() => session.QueryOver<AssetLocation>()
                                                .Where(f => f.AssetLocationID == senddocid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public IEnumerable<AssetLocation> findActive()
        {
            Specification<AssetLocation> specification;
            specification = new AdHocSpecification<AssetLocation>(s => s.Status == true);

            return FindAll(specification);
        }
        public PagedResult<AssetLocation> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AssetLocation> paged = new PagedResult<AssetLocation>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AssetLocation>()
                                                     .OrderByDescending(GetOrderByExpression<AssetLocation>("Status"))
                                                     .ThenBy(GetOrderByExpression<AssetLocation>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AssetLocation>()
                                                     .OrderByDescending(GetOrderByExpression<AssetLocation>("Status"))
                                                     .ThenByDescending(GetOrderByExpression<AssetLocation>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<AssetLocation> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<AssetLocation> specification;

            switch (searchColumn)
            {
                case "DataId":
                    specification = new AdHocSpecification<AssetLocation>(s => s.AssetLocationID.Equals(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<AssetLocation>(s => s.AssetLocationDescription.Contains(searchValue));
                    break;
                case "ShortDesc":
                    specification = new AdHocSpecification<AssetLocation>(s => s.AssetLocationShortDesc.Contains(searchValue));
                    break;
              
                default:
                    specification = new AdHocSpecification<AssetLocation>(s => s.AssetLocationID.Equals(searchValue));
                    break;
            }

            PagedResult<AssetLocation> paged = new PagedResult<AssetLocation>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AssetLocation>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetLocation>("Status"))
                                 .ThenBy(GetOrderByExpression<AssetLocation>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AssetLocation>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AssetLocation>("Status"))
                                 .ThenByDescending(GetOrderByExpression<AssetLocation>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
