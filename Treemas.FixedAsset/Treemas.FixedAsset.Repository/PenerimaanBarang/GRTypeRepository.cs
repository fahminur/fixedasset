﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using NHibernate.Criterion;

namespace Treemas.FixedAsset.Repository
{
    public class GRTypeRepository : RepositoryControllerString<GRType>, IGRTypeRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public GRTypeRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public GRType GetGRType(string GRTypeID)
        {
            return transact(() => session.QueryOver<GRType>().Where(f => f.GRTypeID == GRTypeID).SingleOrDefault());
        }
        public bool IsDuplicate(string GRTypeID)
        {
            int rowCount = transact(() => session.QueryOver<GRType>()
                                                .Where(f => f.GRTypeID == GRTypeID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<GRType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<GRType> paged = new PagedResult<GRType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<GRType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<GRType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<GRType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {

            Conjunction conjuction = Restrictions.Conjunction();

            PagedResult<GRType> paged = new PagedResult<GRType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<GRType>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<GRType>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<GRType>()
                                .Where(conjuction).RowCount());
            return paged;
        }
    }
}
