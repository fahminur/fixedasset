﻿using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.Base.Repository;
using System;

namespace Treemas.FixedAsset.Repository
{
    public class PenerimaanBarangSequenceRepository : RepositoryControllerString<PenerimaanBarangSequence>, IPenerimaanBarangSequenceRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public PenerimaanBarangSequenceRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public PenerimaanBarangSequence getGRSeq(string id, int year)
        {
            return transact(() => session.QueryOver<PenerimaanBarangSequence>()
                                                .Where(f => f.SeqID == id && f.Year == year).SingleOrDefault());
        }

        public bool IsDuplicate(string id, int year)
        {
            int rowCount = transact(() => session.QueryOver<PenerimaanBarangSequence>()
                                                .Where(f => f.SeqID == id && f.Year == year).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
