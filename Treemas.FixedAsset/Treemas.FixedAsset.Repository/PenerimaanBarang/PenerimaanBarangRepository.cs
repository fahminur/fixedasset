﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class PenerimaanBarangRepository : RepositoryControllerString<PenerimaanBarangHdr>, IPenerimaanBarangRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public PenerimaanBarangRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void AddPenerimaanBarangAktiva(PenerimaanBarangAktiva item)
        {
            transact(() => session.Save(item));
        }

        public void AddPenerimaanBarangNonAktiva(PenerimaanBarangNonAktiva item)
        {
            transact(() => session.Save(item));
        }

        public void AddPenerimaanBarangService(PenerimaanBarangService item)
        {
            transact(() => session.Save(item));
        }

        public void DeletePenerimaanBarangActiva(string grno)
        {
            string hqlDelete = "delete PenerimaanBarangAktiva where GRNo = :GRNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("GRNo", grno)
                    .ExecuteUpdate();
        }

        public void DeletePenerimaanBarangNonActiva(string grno)
        {
            string hqlDelete = "delete PenerimaanBarangNonAktiva where GRNo = :GRNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("GRNo", grno)
                    .ExecuteUpdate();
        }

        public void DeletePenerimaanBarangService(string grno)
        {
            string hqlDelete = "delete PenerimaanBarangService where GRNo = :GRNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("GRNo", grno)
                    .ExecuteUpdate();
        }

        public PagedResult<PenerimaanBarangHdr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, PenerimaanBarangFilter filter)
        {
            //SimpleExpression GRNo = Restrictions.Like("GRNo", filter.GRNo);
            SimpleExpression VendorID = Restrictions.Eq("VendorID", filter.VendorID);
            SimpleExpression GRDateFrom = Restrictions.Ge("GRDate", filter.GRDateFrom);
            SimpleExpression GRDateTo = Restrictions.Le("GRDate", filter.GRDateTo);
            SimpleExpression DeliveryOrderNo = Restrictions.Like("DeliveryOrderNo", filter.DeliveryOrderNo);
            SimpleExpression DeliveryOrderDateFrom = Restrictions.Ge("DeliveryOrderDate", filter.DeliveryOrderDateFrom);
            SimpleExpression DeliveryOrderDateTo = Restrictions.Le("DeliveryOrderDate", filter.DeliveryOrderDateTo);
            SimpleExpression Status = Restrictions.Like("Status", filter.Status);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.VendorID.IsNullOrEmpty()) conjuction.Add(VendorID);
            //if (!filter.GRNo.IsNullOrEmpty()) conjuction.Add(GRNo);
            if (!filter.GRDateFrom.IsNull()) conjuction.Add(GRDateFrom);
            if (!filter.GRDateTo.IsNull()) conjuction.Add(GRDateTo);
            if (!filter.DeliveryOrderNo.IsNullOrEmpty()) conjuction.Add(DeliveryOrderNo);
            if (!filter.DeliveryOrderDateFrom.IsNull()) conjuction.Add(DeliveryOrderDateFrom);
            if (!filter.DeliveryOrderDateTo.IsNull()) conjuction.Add(DeliveryOrderDateTo);
            if (!filter.Status.IsNullOrEmpty()) conjuction.Add(Status);

            PagedResult<PenerimaanBarangHdr> paged = new PagedResult<PenerimaanBarangHdr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<PenerimaanBarangHdr>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<PenerimaanBarangHdr>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<PenerimaanBarangHdr>()
                               .Where(conjuction)
                               .RowCount());
            return paged;
        }

        public PagedResult<PenerimaanBarangHdr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<PenerimaanBarangHdr> specification;
            switch (searchColumn)
            {
                case "GRNo":
                    specification = new AdHocSpecification<PenerimaanBarangHdr>(s => s.GRNo.Contains(searchValue) && s.Status == "1");
                    break;
                case "VendorID":
                    specification = new AdHocSpecification<PenerimaanBarangHdr>(s => s.VendorID.Contains(searchValue) && s.Status == "1");
                    break;
                default:
                    specification = new AdHocSpecification<PenerimaanBarangHdr>(s => s.GRNo.Like(searchValue) && s.Status == "1");
                    break;
            }

            PagedResult<PenerimaanBarangHdr> paged = new PagedResult<PenerimaanBarangHdr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<PenerimaanBarangHdr>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<PenerimaanBarangHdr>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<PenerimaanBarangHdr>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<PenerimaanBarangHdr>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        public PenerimaanBarangHdr getPenerimaanBarang(string id)
        {
            return transact(() => session.QueryOver<PenerimaanBarangHdr>()
                                                .Where(f => f.GRNo == id).SingleOrDefault());
        }

        public IList<PenerimaanBarangAktiva> getPenerimaanBarangAktiva(string grno)
        {
            IList<PenerimaanBarangAktiva> list = transact(() => session.QueryOver<PenerimaanBarangAktiva>().Where(f => f.GRNo == grno).List());

            return list;
        }

        public IList<PenerimaanBarangHdr> getPenerimaanBarangList(PenerimaanBarangFilter filter)
        {
            SimpleExpression GRNo = Restrictions.Like("GRNo", filter.GRNo);
            SimpleExpression VendorID = Restrictions.Eq("VendorID", filter.VendorID);
            SimpleExpression GRDateFrom = Restrictions.Ge("GRDate", filter.GRDateFrom);
            SimpleExpression GRDateTo = Restrictions.Le("GRDate", filter.GRDateTo);
            SimpleExpression DeliveryOrderNo = Restrictions.Like("DeliveryOrderNo", filter.DeliveryOrderNo);
            SimpleExpression DeliveryOrderDateFrom = Restrictions.Ge("DeliveryOrderDate", filter.DeliveryOrderDateFrom);
            SimpleExpression DeliveryOrderDateTo = Restrictions.Le("DeliveryOrderDate", filter.DeliveryOrderDateTo);
            SimpleExpression Status = Restrictions.Eq("Status", filter.Status);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.VendorID.IsNullOrEmpty()) conjuction.Add(VendorID);
            if (!filter.GRNo.IsNullOrEmpty()) conjuction.Add(GRNo);
            if (!filter.GRDateFrom.IsNull()) conjuction.Add(GRDateFrom);
            if (!filter.GRDateTo.IsNull()) conjuction.Add(GRDateTo);
            if (!filter.DeliveryOrderNo.IsNull()) conjuction.Add(DeliveryOrderNo);
            if (!filter.DeliveryOrderDateFrom.IsNull()) conjuction.Add(DeliveryOrderDateFrom);
            if (!filter.DeliveryOrderDateTo.IsNull()) conjuction.Add(DeliveryOrderDateTo);
            if (!filter.Status.IsNullOrEmpty()) conjuction.Add(Status);

            IList<PenerimaanBarangHdr> Items = transact(() => session.QueryOver<PenerimaanBarangHdr>().Where(conjuction).List());

            return Items;
        }

        public IList<PenerimaanBarangNonAktiva> getPenerimaanBarangNonAktiva(string grno)
        {
            IList<PenerimaanBarangNonAktiva> list = transact(() => session.QueryOver<PenerimaanBarangNonAktiva>().Where(f => f.GRNo == grno).List());

            return list;
        }

        public IList<PenerimaanBarangService> getPenerimaanBarangService(string grno)
        {
            IList<PenerimaanBarangService> list = transact(() => session.QueryOver<PenerimaanBarangService>().Where(f => f.GRNo == grno).List());

            return list;
        }

        public PenerimaanBarangAktiva getSinglePenerimaanBarangAktiva(string AktivaID)
        {
            return transact(() => session.QueryOver<PenerimaanBarangAktiva>().Where(f => f.AktivaId == AktivaID).SingleOrDefault());
        }

        public PenerimaanBarangNonAktiva getSinglePenerimaanBarangNonAktiva(string NonAktivaID)
        {
            return transact(() => session.QueryOver<PenerimaanBarangNonAktiva>().Where(f => f.NonAktivaId == NonAktivaID).SingleOrDefault());
        }

        public PenerimaanBarangService getSinglePenerimaanBarangService(string AktivaID)
        {
            return transact(() => session.QueryOver<PenerimaanBarangService>().Where(f => f.AktivaId == AktivaID).SingleOrDefault());
        }

        public bool IsDuplicate(string grno)
        {
            int rowCount = transact(() => session.QueryOver<PenerimaanBarangHdr>()
                                                .Where(f => f.GRNo == grno).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
