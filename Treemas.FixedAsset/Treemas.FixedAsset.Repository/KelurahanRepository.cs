﻿using System;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using System.Collections.Generic;

namespace Treemas.FixedAsset.Repository
{
    public class KelurahanRepository : RepositoryControllerString<Kelurahan>, IKelurahanRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public KelurahanRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        //public IEnumerable<Kelurahan> findActive()
        //{
        //    Specification<Kelurahan> specification;
        //    specification = new AdHocSpecification<Kelurahan>(s => s.KelurahanId == "true");

        //    return FindAll(specification);
        //}

        public PagedResult<Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Kelurahan> paged = new PagedResult<Kelurahan>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Kelurahan>()
                                                     .OrderByDescending(GetOrderByExpression<Kelurahan>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Kelurahan>()
                                                     .OrderBy(GetOrderByExpression<Kelurahan>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, KelurahanFilter filter)
        {
            SimpleExpression NamaKelurahan = Restrictions.Like("NamaKelurahan", filter.NamaKelurahan.Trim(), MatchMode.Anywhere);
            SimpleExpression Provinsi = Restrictions.Like("Provinsi", filter.Provinsi.Trim(), MatchMode.Anywhere);
            SimpleExpression Kecamatan = Restrictions.Like("Kecamatan", filter.Kecamatan.Trim(), MatchMode.Anywhere);
            SimpleExpression Kota = Restrictions.Like("Kota", filter.Kota.Trim(), MatchMode.Anywhere);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.NamaKelurahan.Trim().IsNullOrEmpty()) conjuction.Add(NamaKelurahan);
            if (!filter.Provinsi.Trim().IsNullOrEmpty()) conjuction.Add(Provinsi);
            if (!filter.Kecamatan.Trim().IsNullOrEmpty()) conjuction.Add(Kecamatan);
            if (!filter.Kota.Trim().IsNullOrEmpty()) conjuction.Add(Kota);

            PagedResult<Kelurahan> paged = new PagedResult<Kelurahan>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<Kelurahan>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<Kelurahan>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<Kelurahan>()
                               .Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Kelurahan> specification;
            switch (searchColumn)
            {
                case "NamaKelurahan":
                    specification = new AdHocSpecification<Kelurahan>(s => s.NamaKelurahan.Equals(searchValue));
                    break;
                //case "KelurahanDeskripsi":
                //    specification = new AdHocSpecification<Kelurahan>(s => s.KelurahanDeskripsi.Contains(searchValue));
                //    break;
                //case "AssetAreaShortDesc":
                //    specification = new AdHocSpecification<Kelurahan>(s => s.AssetAreaShortDesc.Contains(searchValue));
                //    break;
                default:
                    specification = new AdHocSpecification<Kelurahan>(s => s.NamaKelurahan.Contains(searchValue));
                    break;
            }

            PagedResult<Kelurahan> paged = new PagedResult<Kelurahan>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Kelurahan>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Kelurahan>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Kelurahan>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Kelurahan>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();

            return paged;
        }

        public IEnumerable<Kelurahan> getKecamatan(string Kota)
        {
            Specification<Kelurahan> specification;
            specification = new AdHocSpecification<Kelurahan>(s => s.Kota == Kota);

            return FindAll(specification);
        }

        public IEnumerable<Kelurahan> getKelurahan(string Kota, string Kecamatan)
        {
            Specification<Kelurahan> specification;
            specification = new AdHocSpecification<Kelurahan>(s => s.Kota == Kota && s.Kecamatan == Kecamatan);

            return FindAll(specification);
        }

        public IEnumerable<Kelurahan> getKota()
        {
            Specification<Kelurahan> specification;
            specification = new AdHocSpecification<Kelurahan>(k => k.NamaKelurahan.Contains(""));

            return FindAll(specification);
        }

        public Kelurahan GetZipCode(string NamaKelurahan)
        {
            return transact(() => session.QueryOver<Kelurahan>()
                                                .Where(f => f.NamaKelurahan == NamaKelurahan).SingleOrDefault());
        }

        public Kelurahan getKelurahanSingle(string ID)
        {
            return transact(() => session.QueryOver<Kelurahan>()
                                                .Where(f => f.ID == ID).SingleOrDefault());
        }

        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<Kelurahan>()
                                                .Where(f => f.ID == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
