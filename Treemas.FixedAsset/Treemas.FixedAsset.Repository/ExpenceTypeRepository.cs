﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Repository;
using NHibernate.Criterion;

namespace Treemas.FixedAsset.Repository
{
    public class ExpenceTypeRepository : RepositoryControllerString<ExpenceType>, IExpenceTypeRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public ExpenceTypeRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public ExpenceType GetExpenceType(string expenceTypeID)
        {
            return transact(() => session.QueryOver<ExpenceType>().Where(f => f.ExpenceTypeID == expenceTypeID).SingleOrDefault());
        }
        public bool IsDuplicate(string invoiceTypeID)
        {
            int rowCount = transact(() => session.QueryOver<ExpenceType>()
                                                .Where(f => f.ExpenceTypeID == invoiceTypeID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<ExpenceType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {

            PagedResult<ExpenceType> paged = new PagedResult<ExpenceType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<ExpenceType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<ExpenceType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<ExpenceType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {

            Conjunction conjuction = Restrictions.Conjunction();

            PagedResult<ExpenceType> paged = new PagedResult<ExpenceType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<ExpenceType>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<ExpenceType>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<ExpenceType>()
                                .Where(conjuction).RowCount());
            return paged;
        }
    }
}
