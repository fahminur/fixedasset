﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IFixedAssetMasterService
    {
        string SaveFixedAssetMaster(FixedAssetMaster invH, User user, Company company);
        string UpdateFixedAssetMaster(FixedAssetMaster invH, User user, Company company);
    }
}
