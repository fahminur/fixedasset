﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IVendorService
    {
        string SaveVendor(Vendor vendor, User user, Company company);
        string UpdateVendor(Vendor vendor, User user, Company company);
    }
}
