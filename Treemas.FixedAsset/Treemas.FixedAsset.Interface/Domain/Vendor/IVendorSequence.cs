﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IVendorSequence
    {
        string createVendorID(string SeqID, string BranchID, int year, int month, User user, Company company);
    }
}
