﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPenjualanAktivaService 
    {
        string SavePenjualanAktiva(PenjualanAktivaHeader paH, User user, Company company);
        string UpdatePenjualanAktiva(PenjualanAktivaHeader paH, User user, Company company);
    }
}
