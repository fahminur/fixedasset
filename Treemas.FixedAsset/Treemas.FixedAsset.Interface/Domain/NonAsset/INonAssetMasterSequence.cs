﻿using Treemas.Credential.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface INonAssetMasterSequence
    {
        string createNonAssetMasterNo(string SeqID, string BranchID, string GroupAssetID, int year, int month, User user, Company company);
        //string createNonAktivaNo(string SeqID, int year, int month, User user, Company company);

    }
}
