﻿using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;


namespace Treemas.FixedAsset.Interface
{
    public interface INonAssetMasterService
    {
        string SaveNonAssetMaster(NonAssetMaster nam, User user, Company company);
        string UpdateNonAssetMaster(NonAssetMaster nam, User user, Company company);
    }
}
