﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IInvoiceService 
    {
        string SaveInvoice(InvoiceH invH, User user, Company company);
        string UpdateInvoice(InvoiceH invH, User user, Company company);
    }
}
