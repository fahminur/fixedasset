﻿using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IGLJournalService
    {
        string SaveJournal(GLJournalH journalHdr, User usr, Company comp);
    }
}
