﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPenerimaanBarangService
    {
        string SavePenerimaanBarang(PenerimaanBarangHdr invH, User user, Company company);
        string UpdatePenerimaanBarang(PenerimaanBarangHdr invH, User user, Company company);

    }
}
