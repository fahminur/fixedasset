﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPenghapusanAktivaSequence
    {
        string createTransactionNo(string SeqID, int year, int month, User user, Company company);

    }
}
