﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;
using Treemas.Foundation.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPenghapusanAktivaService 
    {
        string SavePenghapusanAktiva(PenghapusanAktivaHeader paH, User user, Company company);
        string UpdatePenghapusanAktiva(PenghapusanAktivaHeader paH, User user, Company company);
    }
}
