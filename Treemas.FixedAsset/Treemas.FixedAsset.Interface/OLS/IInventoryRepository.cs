﻿using Treemas.FixedAsset.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.FixedAsset.Interface
{
    public interface IInventoryRepository : IBaseRepository<Inventory>
    {
        bool IsDuplicate(string itemstatusid);
        Inventory getInventory(string id);

    }
}
