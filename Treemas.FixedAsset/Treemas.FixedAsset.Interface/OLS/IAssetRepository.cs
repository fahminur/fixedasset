﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IAssetRepository : IBaseRepository<Asset>
    {
        PagedResult<Asset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Asset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        Asset getAsset(string id);

    }
}
