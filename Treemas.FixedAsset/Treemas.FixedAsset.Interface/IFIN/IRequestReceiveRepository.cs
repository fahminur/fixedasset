﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IRequestReceiveRepository : IBaseRepository<RequestReceive>
    {
        bool IsDuplicate(string Aktivaid);
    }
}
