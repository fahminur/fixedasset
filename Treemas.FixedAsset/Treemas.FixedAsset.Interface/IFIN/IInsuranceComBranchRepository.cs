﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;


namespace Treemas.FixedAsset.Interface
{
    public interface IInsuranceComBranchRepository:IBaseRepository<InsuranceComBranch>
    {
        bool IsDuplicate(string id);
        PagedResult<InsuranceComBranch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<InsuranceComBranch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        InsuranceComBranch getInsuranceComBranch(string id);
        IEnumerable<InsuranceComBranch> FindByMaskAssID(string id);
    }
}
