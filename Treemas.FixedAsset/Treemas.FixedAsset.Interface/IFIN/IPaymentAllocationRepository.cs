﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPaymentAllocationRepository : IBaseRepository<PaymentAllocation>
    {
        bool IsDuplicate(string id);
        PagedResult<PaymentAllocation> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<PaymentAllocation> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PaymentAllocation getPaymentAllocation(string id);

    }
}
