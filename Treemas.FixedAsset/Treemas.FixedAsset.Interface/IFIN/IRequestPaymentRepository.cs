﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IRequestPaymentRepository : IBaseRepository<RequestPayment>
    {
        bool IsDuplicate(string invoiceNo);
        PagedResult<RequestPayment> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, RequestPaymentFilter filter);
        RequestPayment getRequest(string id);
    }
}
