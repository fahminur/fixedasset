﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IBankMasterRepository : IBaseRepository<BankMaster>
    {
        BankMaster getBankMaster(string id);
    }
}
