﻿using Treemas.FixedAsset.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.FixedAsset.Interface
{
    public interface IAssetTypeRepository : IBaseRepository<AssetType>
    {
        bool IsDuplicate(string id);
        PagedResult<AssetType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AssetType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        AssetType getAssetType(string id);

    }
}
