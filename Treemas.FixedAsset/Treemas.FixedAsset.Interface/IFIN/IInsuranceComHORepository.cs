﻿using Treemas.FixedAsset.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.FixedAsset.Interface
{
    public interface IInsuranceComHORepository : IBaseRepository<InsuranceComHO>
    {
        bool IsDuplicate(string id);
        PagedResult<InsuranceComHO> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<InsuranceComHO> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        InsuranceComHO getInsuranceComHO(string id);

    }
}
