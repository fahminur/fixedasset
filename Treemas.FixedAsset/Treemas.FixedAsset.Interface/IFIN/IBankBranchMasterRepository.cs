﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IBankBranchMasterRepository: IBaseRepository<BankBranchMaster>
    {
        bool IsDuplicate(string id);
        PagedResult<BankBranchMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<BankBranchMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        BankBranchMaster getBankBranchMaster(string id);
        IEnumerable<BankBranchMaster> getBankBranchByBankID(string id);
    }
}
