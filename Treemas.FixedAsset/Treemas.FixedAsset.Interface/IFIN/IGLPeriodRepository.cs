﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IGLPeriodRepository : IBaseRepository<GLPeriod>
    {
        GLPeriod GetGLPeriod(DateTime periodStart, DateTime periodEnd);
    }
}
