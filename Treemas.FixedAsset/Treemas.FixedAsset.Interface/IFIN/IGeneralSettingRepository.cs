﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IGeneralSettingRepository: IBaseRepository<GeneralSetting>
    {
        GeneralSetting GetGeneralSetting(string gsid, string modulid);
    }
}
