﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IAssetCoaRepository : IBaseRepository<AssetCoa>
    {
        bool IsDuplicate(string accClassid);
        PagedResult<AssetCoa> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AssetCoa> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        AssetCoa getAssetCoa(string id);
        IEnumerable<AssetCoa> findActive();
        IEnumerable<AssetCoa> findByGroupAsset(string coaId);
    }
}
