﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IVendorBankRepository : IBaseRepository<VendorBank>
    {
        bool IsDuplicate(string bankmasterid, string accountno);
        PagedResult<VendorBank> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, long Id);
        PagedResult<VendorBank> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, long Id);
        VendorBank getVendorBank(long id);
        IEnumerable<VendorBank> getBankByVendor(long vendorid);
    }
}
