﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface INamaHartaRepository : IBaseRepository<NamaHarta>
    {
        bool IsDuplicate(string accClassid);
        PagedResult<NamaHarta> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<NamaHarta> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        NamaHarta getNamaHarta(string id);
        IEnumerable<NamaHarta> findActive();
    }
}
