﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IGLJournalTypeRepository : IBaseRepository<GLJournalType>
    {
        bool IsDuplicate(string GLJournalTypeID);
        PagedResult<GLJournalType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<GLJournalType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        GLJournalType GetGLJournalType(string GLJournalTypeID);
    }
}
