﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IVendorAccountRepository : IBaseRepository<VendorAccount>
    {
        bool IsDuplicate(string bankmasterid, string accountno);
        PagedResult<VendorAccount> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string Id);
        PagedResult<VendorAccount> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string Id);
        PagedResult<VendorAccount> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, VendorBankFilter filter);

        IList<VendorAccount> getVendorAccbyVendor(string id);
        VendorAccount getVendorAccount(string id);
        IEnumerable<VendorAccount> getBankByVendor(string Vendorid);
    }
}
