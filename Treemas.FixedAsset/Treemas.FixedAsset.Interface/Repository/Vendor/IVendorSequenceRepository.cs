﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IVendorSequenceRepository : IBaseRepository<VendorSequence>
    {
        bool IsDuplicate(string id, int year);
        VendorSequence getVendorSeq(string id, int year);
    }
}
