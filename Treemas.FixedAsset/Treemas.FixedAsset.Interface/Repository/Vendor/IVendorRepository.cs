﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IVendorRepository : IBaseRepository<Vendor>
    {
        bool IsDuplicate(string accClassid);
        PagedResult<Vendor> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Vendor> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<Vendor> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, VendorFilter filter);
        Vendor getVendor(string id);
        IEnumerable<Vendor> findActive();
    }
}
