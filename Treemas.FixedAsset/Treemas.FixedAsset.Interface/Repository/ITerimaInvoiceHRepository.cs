﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface ITerimaInvoiceHRepository : IBaseRepository<TerimaInvoiceH>
    {
        bool IsDuplicate(long vendorId, string invoiceNo);
        PagedResult<TerimaInvoiceH> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<TerimaInvoiceH> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<TerimaInvoiceH> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, RequestPaymentFilter requestPaymentFilter);
        TerimaInvoiceH getTerimaInvoiceH(long id);
        void AddAktiva(TerimaInvoiceAktiva item);
        void AddNonAktiva(TerimaInvoiceNonAktiva item);
        void AddPerawatan(TerimaInvoicePerawatan item);

    }
}
