﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IDepresiasiKomersialRepository : IBaseRepository<DepresiasiKomersial>
    {
        DepresiasiKomersial getDepresiasiKomersial(string id);
        DepresiasiKomersial getSaldoDepresiasiKomersial(string id, int year, int month);
    }
}
