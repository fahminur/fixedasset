﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IAssetConditionRepository : IBaseRepository<AssetCondition>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<AssetCondition> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AssetCondition> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        AssetCondition getAssetCondition(string id);
        IEnumerable<AssetCondition> findActive();
    }
}
