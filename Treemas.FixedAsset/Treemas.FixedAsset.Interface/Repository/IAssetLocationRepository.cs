﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IAssetLocationRepository : IBaseRepository<AssetLocation>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<AssetLocation> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AssetLocation> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        AssetLocation getAssetLocation(string id);
        IEnumerable<AssetLocation> findActive();
    }
}
