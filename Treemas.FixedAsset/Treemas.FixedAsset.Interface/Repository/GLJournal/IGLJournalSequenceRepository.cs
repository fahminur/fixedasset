﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IGLJournalSequenceRepository:IBaseRepository<GLJournalSequence>
    {
        bool IsDuplicate(string id, int year);
        GLJournalSequence getJournalSeq(string id, int year);
    }
}
