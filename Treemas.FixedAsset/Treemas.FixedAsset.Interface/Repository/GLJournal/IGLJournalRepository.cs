﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IGLJournalRepository: IBaseRepository<GLJournalH>
    {
        bool IsDuplicate(string trno);
        PagedResult<GLJournalH> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, GLJFilter filter);

        IList<GLJournalD> getGLJournalDtl(string trxNo);

        GLJournalH GetJournalHdr(string trxNo);

        void AddGlJournalDtl(GLJournalD item);
    }
}
