﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IAssetBranchRepository : IBaseRepository<AssetBranch>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<AssetBranch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AssetBranch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        AssetBranch getAssetBranch(string id);
        IEnumerable<AssetBranch> findActive();
    }
}
