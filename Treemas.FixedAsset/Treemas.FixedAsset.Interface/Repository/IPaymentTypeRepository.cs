﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPaymentTypeRepository: IBaseRepository<PaymentType>
    {
        bool IsDuplicate(string PaymentTypeID);
        PagedResult<PaymentType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<PaymentType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PaymentType GetPaymentType(string PaymentTypeID);
    }
}
