﻿using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface INonAssetMasterSequenceRepository:IBaseRepository<NonAssetSequence>
    {
        bool IsDuplicate(string id, int year);
        NonAssetSequence getNAMSeq(string id, int year);
    }
}
