﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IBankMasterRepository : IBaseRepository<BankMaster>
    {
        bool IsDuplicate(string bankId);
        PagedResult<BankMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<BankMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        BankMaster getBankMaster(string id);
    }
}
