﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface ISubGroupAssetRepository : IBaseRepository<SubGroupAsset>
    {
        bool IsDuplicate(string accClassid);
        PagedResult<SubGroupAsset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<SubGroupAsset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        SubGroupAsset getSubGroupAsset(string id);
        IEnumerable<SubGroupAsset> findActive();
        IEnumerable<SubGroupAsset> FindByGroupAsset(string groupAssetID);
    }
}
