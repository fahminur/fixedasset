﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPerawatanAktivaRepository : IBaseRepository<PerawatanAktiva>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<PerawatanAktiva> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, PerawatanAktivaFilter filter);
        PerawatanAktiva getPerawatanAktiva(string id);
    }
}
