﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IMutasiAktivaRepository : IBaseRepository<MutasiAktiva>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<MutasiAktiva> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, MutasiAktivaFilter filter);
        MutasiAktiva getMutasiAktiva(string id);
    }
}
