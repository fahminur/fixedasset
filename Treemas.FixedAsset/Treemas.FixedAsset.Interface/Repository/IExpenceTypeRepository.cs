﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IExpenceTypeRepository : IBaseRepository<ExpenceType>
    {
        bool IsDuplicate(string expenceeID);
        PagedResult<ExpenceType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<ExpenceType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        ExpenceType GetExpenceType(string expenceTypeID);
    }
}
