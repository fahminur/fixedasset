﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IInvoiceRepository : IBaseRepository<InvoiceH>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<InvoiceH> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        //PagedResult<InvoiceH> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, RequestPaymentFilter filter);
        PagedResult<InvoiceH> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, InvoiceFilter filter);
        InvoiceH getInvoice(string id);
        IList<InvoiceH> getInvoiceList(InvoiceFilter filter);
        IList<InvoiceAktiva> getInvoiceAktiva(string InvoiceNo);
        void AddInvoiceAktiva(InvoiceAktiva item);
        IList<InvoiceNonAktiva> getInvoiceNonAktiva(string InvoiceNo);
        void AddInvoiceNonAktiva(InvoiceNonAktiva item);
        IList<InvoiceService> getInvoiceService(string InvoiceNo);
        void AddInvoiceService(InvoiceService item);

        void DeleteInvoiceActiva(string invoiceNo);
        void DeleteInvoiceNonActiva(string invoiceNo);
        void DeleteInvoiceService(string invoiceNo);

        InvoiceAktiva getSingleInvoiceAktiva(string AktivaId);
    }
}
