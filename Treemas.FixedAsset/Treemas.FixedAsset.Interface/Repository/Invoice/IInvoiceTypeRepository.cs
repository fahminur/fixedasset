﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;


namespace Treemas.FixedAsset.Interface
{
    public interface IInvoiceTypeRepository : IBaseRepository<InvoiceType>
    {
        bool IsDuplicate(string invoiceTypeID);
        PagedResult<InvoiceType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<InvoiceType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        InvoiceType GetInvoiceType(string invoiceTypeID);
    }
}
