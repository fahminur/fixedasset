﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IInvoiceSequenceRepository : IBaseRepository<InvoiceSequence>
    {
        bool IsDuplicate(string id, int year);
        InvoiceSequence getInvSeq(string id, int year);
    }
}
