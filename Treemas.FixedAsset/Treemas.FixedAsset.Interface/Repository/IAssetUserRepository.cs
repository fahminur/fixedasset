﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IAssetUserRepository : IBaseRepository<AssetUser>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<AssetUser> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AssetUser> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        AssetUser getAssetUser(string id);
        IEnumerable<AssetUser> findActive();
        IEnumerable<AssetUser> FindByBranchID(string BranchID);
        IEnumerable<AssetUser> FindByDeptID(string BranchID, string DeptID);
    }
}
