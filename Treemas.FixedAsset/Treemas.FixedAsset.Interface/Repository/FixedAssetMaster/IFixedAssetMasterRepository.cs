﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;
using Treemas.Credential.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IFixedAssetMasterRepository : IBaseRepository<FixedAssetMaster>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<FixedAssetMaster> FindAllPagedLookup(User usr, int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        //PagedResult<FixedAssetMaster> FindAllPagedLookupSales(User usr, int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        //PagedResult<FixedAssetMaster> FindAllPagedLookupSales(User usr, int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<FixedAssetMaster> FindAllPaged(User usr, int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<FixedAssetMaster> FindAllPaged(User usr, int pageNumber, int itemsPerPage, string orderColumn, string orderKey, FixedAssetFilter fixedAssetFilter);
        FixedAssetMaster getFixedAssetMaster(string id);

        //PagedResult<FixedAssetMaster> FindAllPagedLookup(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string companyId, string siteId);
        //PagedResult<FixedAssetMaster> FindAllPagedLookup(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string companyId, string siteId);
        //PagedResult<FixedAssetMaster> FindAllPagedLookupAll(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string companyId, string siteId);
        //PagedResult<FixedAssetMaster> FindAllPagedLookupAll(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string companyId, string siteId);
        //IList<FixedAssetMaster> FindAllFixedAssetMasterinList(IList<string> items);

        // IEnumerable<FixedAssetMaster> findActive();
    }
}
