﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IFixedAssetMasterSequenceRepository : IBaseRepository<FixedAssetSequence>
    {
        bool IsDuplicate(string id, int year);
        FixedAssetSequence getFAMSeq(string id, int year);
        FixedAssetSequence getNACSeq(string id, int year);
    }
}
