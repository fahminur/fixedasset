﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;


namespace Treemas.FixedAsset.Interface
{
    public interface IMateraiRepository : IBaseRepository<Materai>
    {
        bool IsDuplicate(string MateraiID);
        PagedResult<Materai> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Materai> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        Materai GetMaterai(string MateraiID);
    }
}
