﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IAssetAreaRepository : IBaseRepository<AssetArea>
    {
        bool IsDuplicate(string accClassid);
        PagedResult<AssetArea> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AssetArea> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        AssetArea getAssetArea(string id);
        IEnumerable<AssetArea> findActive();
    }
}
