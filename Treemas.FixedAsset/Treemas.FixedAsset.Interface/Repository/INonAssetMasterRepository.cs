﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface INonAssetMasterRepository:IBaseRepository<NonAssetMaster>
    {
        bool IsDuplicate(string id);
        PagedResult<NonAssetMaster> FindAllPagedLookup(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<NonAssetMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<NonAssetMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, NonAssetFilter nonAssetFilter);
        NonAssetMaster getNonAssetMaster(string id);
    }
}
