﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IMetodeDepresiasiRepository : IBaseRepository<MetodeDepresiasi>
    {
        bool IsDuplicate(string MetodeDepresiasiID);
        PagedResult<MetodeDepresiasi> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<MetodeDepresiasi> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        MetodeDepresiasi GetMetodeDepresiasi(string MetodeDepresiasiID);
    }
}
