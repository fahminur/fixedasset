﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IAssetTidakSusutRepository : IBaseRepository<AssetTidakSusut>
    {
        bool IsDuplicate(long ContactID);
        PagedResult<AssetTidakSusut> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string Id);
        PagedResult<AssetTidakSusut> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string Id);
        AssetTidakSusut getAssetTidakSusut(long id);
    }
}
