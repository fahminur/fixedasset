﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IGolonganPajakRepository : IBaseRepository<GolonganPajak>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<GolonganPajak> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<GolonganPajak> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        GolonganPajak getGolonganPajak(string id);
        IEnumerable<GolonganPajak> findActive();
    }
}
