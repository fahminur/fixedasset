﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IAssetDeptRepository : IBaseRepository<AssetDept>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<AssetDept> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AssetDept> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        AssetDept getAssetDept(string id);
        IEnumerable<AssetDept> findActive();
    }
}
