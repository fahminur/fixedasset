﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IGroupAssetRepository : IBaseRepository<GroupAsset>
    {
        bool IsDuplicate(string itemstatusid);
        PagedResult<GroupAsset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<GroupAsset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        GroupAsset getGroupAsset(string id);
        IEnumerable<GroupAsset> findActive();
    }
}
