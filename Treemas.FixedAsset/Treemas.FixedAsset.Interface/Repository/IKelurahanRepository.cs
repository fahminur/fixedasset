﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IKelurahanRepository : IBaseRepository<Kelurahan>
    {
        bool IsDuplicate(string accClassid);
        PagedResult<Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, KelurahanFilter filter);
        IEnumerable<Kelurahan> getKelurahan(string Kota, string Kecamatan);
        IEnumerable<Kelurahan> getKecamatan(string Kota);
        IEnumerable<Kelurahan> getKota();
        Kelurahan GetZipCode(string ID);
        Kelurahan getKelurahanSingle(string ID);
        //IEnumerable<Kelurahan> findActive();
    }
}
