﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPenerimaanBarangRepository : IBaseRepository<PenerimaanBarangHdr>
    {
        bool IsDuplicate(string grno);
        PagedResult<PenerimaanBarangHdr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        //PagedResult<PenerimaanBarangHdr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, RequestPaymentFilter filter);
        PagedResult<PenerimaanBarangHdr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, PenerimaanBarangFilter filter);
        PenerimaanBarangHdr getPenerimaanBarang(string id);
        IList<PenerimaanBarangHdr> getPenerimaanBarangList(PenerimaanBarangFilter filter);
        IList<PenerimaanBarangAktiva> getPenerimaanBarangAktiva(string PenerimaanBarangNo);
        void AddPenerimaanBarangAktiva(PenerimaanBarangAktiva item);
        IList<PenerimaanBarangNonAktiva> getPenerimaanBarangNonAktiva(string PenerimaanBarangNo);
        void AddPenerimaanBarangNonAktiva(PenerimaanBarangNonAktiva item);
        IList<PenerimaanBarangService> getPenerimaanBarangService(string PenerimaanBarangNo);
        void AddPenerimaanBarangService(PenerimaanBarangService item);

        void DeletePenerimaanBarangActiva(string PenerimaanBarangNo);
        void DeletePenerimaanBarangNonActiva(string PenerimaanBarangNo);
        void DeletePenerimaanBarangService(string PenerimaanBarangNo);

        PenerimaanBarangAktiva getSinglePenerimaanBarangAktiva(string AktivaID);
        PenerimaanBarangNonAktiva getSinglePenerimaanBarangNonAktiva(string NonAktivaID);
        PenerimaanBarangService getSinglePenerimaanBarangService(string AktivaID);
    }
}
