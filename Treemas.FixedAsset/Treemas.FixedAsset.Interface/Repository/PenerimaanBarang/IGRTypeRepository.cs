﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;


namespace Treemas.FixedAsset.Interface
{
    public interface IGRTypeRepository : IBaseRepository<GRType>
    {
        bool IsDuplicate(string GRTypeID);
        PagedResult<GRType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<GRType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        GRType GetGRType(string GRTypeID);
    }
}
