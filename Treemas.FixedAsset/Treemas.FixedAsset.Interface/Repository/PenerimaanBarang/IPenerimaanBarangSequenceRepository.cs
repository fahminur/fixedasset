﻿using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPenerimaanBarangSequenceRepository:IBaseRepository<PenerimaanBarangSequence>
    {
        bool IsDuplicate(string id, int year);
        PenerimaanBarangSequence getGRSeq(string id, int year);
    }
}
