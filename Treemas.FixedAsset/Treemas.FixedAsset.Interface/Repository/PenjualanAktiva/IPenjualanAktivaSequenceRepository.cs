﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPenjualanAktivaSequenceRepository : IBaseRepository<PenjualanAktivaSequence>
    {
        bool IsDuplicate(string id, int year);
        PenjualanAktivaSequence getPenjualanSeq(string id, int year);
    }
}
