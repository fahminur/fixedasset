﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPenjualanAktivaRepository : IBaseRepository<PenjualanAktivaHeader>
    {
        bool IsDuplicate(string id);
        PagedResult<PenjualanAktivaHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, PenjualanAktivaFilter filter);
        PenjualanAktivaHeader getPenjualanAktiva(string id);
        IList<PenjualanAktivaDetail> getPenjualanDetail(string id);
        void AddPenjualanDetail(PenjualanAktivaDetail item);
        void DeletePenjualanDetail(string id);
        void CreateJournalPenjualan(string id);
        PenjualanAktivaDetail getSinglePenjualanDetail(string AktivaId);
    }
}
