﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPenghapusanAktivaRepository : IBaseRepository<PenghapusanAktivaHeader>
    {
        bool IsDuplicate(string id);
        PagedResult<PenghapusanAktivaHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, PenghapusanAktivaFilter filter);
        PenghapusanAktivaHeader getPenghapusanAktiva(string id);
        IList<PenghapusanAktivaDetail> getPenghapusanDetail(string id);
        void AddPenghapusanDetail(PenghapusanAktivaDetail item);
        void DeletePenghapusanDetail(string id);
        PenghapusanAktivaDetail getSinglePenghapusanDetail(string AktivaId);
    }
}
