﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IPenghapusanAktivaSequenceRepository : IBaseRepository<PenghapusanAktivaSequence>
    {
        bool IsDuplicate(string id, int year);
        PenghapusanAktivaSequence getPenghapusanSeq(string id, int year);
    }
}
