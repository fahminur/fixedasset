﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Model;

namespace Treemas.FixedAsset.Interface
{
    public interface IJenisUsahaRepository : IBaseRepository<JenisUsaha>
    {
        bool IsDuplicate(string accClassid);
        PagedResult<JenisUsaha> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<JenisUsaha> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        JenisUsaha getJenisUsaha(string id);
        IEnumerable<JenisUsaha> findActive();
    }
}
