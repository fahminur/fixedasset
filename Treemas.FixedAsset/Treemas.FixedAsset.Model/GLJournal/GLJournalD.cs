﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class GLJournalD : Entity
    {
        protected GLJournalD()
        {

        }
        public GLJournalD(int id) : base(id)
        {
            SequenceNo = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string CompanyID { get; set; }            //CHAR (3) NOT NULL,
        public virtual string BranchID { get; set; }            //CHAR (3) NOT NULL,
        public virtual string Tr_Nomor { get; set; }            //CHAR (20) NOT NULL,
        public virtual int SequenceNo { get; set; }            //INT NOT NULL,
        public virtual string CoaCo { get; set; }            //CHAR (3) NOT NULL,
        public virtual string CoaBranch { get; set; }            //CHAR (3) NOT NULL,
        public virtual string CoaId { get; set; }            //VARCHAR(25) NOT NULL,
        public virtual string TransactionID { get; set; }            //VARCHAR (8) NOT NULL,
        public virtual string Tr_Desc { get; set; }            //VARCHAR (250) NULL,
        public virtual string Post { get; set; }            //CHAR (1) NOT NULL,
        public virtual double Amount { get; set; }            //NUMERIC(18) NULL,
        public virtual string CoaId_X { get; set; }            //VARCHAR(25) NULL,
        public virtual string PaymentAllocationId { get; set; }            //CHAR (10) NULL,
        public virtual string ProductId { get; set; }            //CHAR (10) NULL,
        public virtual string UsrUpd { get; set; }            //VARCHAR(50) NOT NULL,
        public virtual DateTime DtmUpd { get; set; }            //DATETIME NOT NULL,
    }
}
