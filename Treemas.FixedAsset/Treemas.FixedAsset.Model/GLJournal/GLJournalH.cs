﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class GLJournalH : EntityString
    {
        protected GLJournalH()
        {

        }
        public GLJournalH(string id) : base(id)
        {
            Tr_Nomor = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string CompanyID { get; set; }                                       
        public virtual string BranchID { get; set; }                                        
        public virtual string Tr_Nomor { get; set; }                                        
        public virtual string PeriodYear { get; set; }                                      
        public virtual string PeriodMonth { get; set; }                                     
        public virtual string TransactionID { get; set; }                                   
        public virtual DateTime? tr_Date1 { get; set; }                                      
        public virtual DateTime Tr_Date { get; set; }                                       
        public virtual string Reff_No { get; set; }                                         
        public virtual DateTime Reff_Date { get; set; }                                     
        public virtual string Tr_Desc { get; set; }                                         
        public virtual double JournalAmount { get; set; }                                   
        public virtual string BatchID { get; set; }                                         
        public virtual string SubSystem { get; set; }                                       
        public virtual string Status_Tr { get; set; }                                       
        public virtual bool IsActive { get; set; }                                          
        public virtual string Flag { get; set; }                                            
        public virtual string UsrUpd { get; set; }                                          
        public virtual DateTime DtmUpd { get; set; }                                        
        public virtual string Status { get; set; }                                          
        public virtual bool IsValid { get; set; }                                           
        public virtual int DepresiasiTenor { get; set; }                                    
        public virtual int SisaTenor { get; set; }                                          
        public virtual DateTime? PrintDate { get; set; }                                     
        public virtual string PrintBy { get; set; }                                         
        public virtual bool IsPrint { get; set; }                                           
        public virtual string Jenis_Reff { get; set; }                                      
        public virtual string PostingDate { get; set; }                                     
        public virtual string PostingBy { get; set; }                                       
        public virtual DateTime? LastRevisiPosting { get; set; }                             
        public virtual Int32 CountRevisiPosting { get; set; }
        public virtual IList<GLJournalD> GLJournalDtlList { get; set; }

    }
}                                                                                                                                        
