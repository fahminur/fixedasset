﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class GolonganPajak : EntityString
    {
        protected GolonganPajak()
        { }
        public GolonganPajak(string id) : base(id)
        {
            this.GolonganPajakID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string GolonganPajakID { get; set; }
        public virtual string GolonganPajakDescription { get; set; }        
        public virtual double SLPercentage { get; set; }
        public virtual double DDPercentage { get; set; }
        public virtual int MasaManfaat { get; set; }
        public virtual bool Status { get; set; } 
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
