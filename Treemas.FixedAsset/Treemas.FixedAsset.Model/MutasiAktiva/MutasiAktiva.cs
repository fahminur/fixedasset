﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class MutasiAktiva : EntityString
    {
        protected MutasiAktiva() { }
        public MutasiAktiva(string id) : base(id)
        {
            this.MutasiAktivaID = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string MutasiAktivaID { get; set; }
        public virtual string AktivaID { get; set; }
        public virtual string NamaAktiva { get; set; }
        public virtual DateTime TanggalMutasi { get; set; }
        public virtual string Periode { get; set; }
        public virtual string DariCabangID { get; set; }
        public virtual string DariLokasi { get; set; }
        //public virtual string DariLokasiID { get; set; }
        //public virtual string DariAreaID { get; set; }
        public virtual string DariDepartmentID { get; set; }
        public virtual string DariPemakaiID { get; set; }
        public virtual string DariKondisiID { get; set; }
        public virtual string DariKodeIndukAktivaID { get; set; }
        public virtual string KeAktivaID { get; set; }
        public virtual string KeCabangID { get; set; }
        public virtual string KeLokasi { get; set; }
        //public virtual string KeLokasiID { get; set; }
        //public virtual string KeAreaID { get; set; }
        public virtual string KeDepartmentID { get; set; }
        public virtual string KePemakaiID { get; set; }
        public virtual string KeKondisiID { get; set; }
        public virtual string KeKodeIndukAktivaID { get; set; }
        public virtual string NoReferensi { get; set; }
        public virtual string Keterangan { get; set; }

        public virtual string GroupAssetID { get; set; }
        public virtual string SubGroupAssetID { get; set; }

        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
