﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;


namespace Treemas.FixedAsset.Model
{
    public class DepresiasiKomersial : EntityString
    {
        public DepresiasiKomersial()
        {

        }
        public DepresiasiKomersial(string id) : base(id)
        {
            AktivaId = id;
        }


        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string AktivaId { get; set; }
        public virtual string Tahun { get; set; }
        public virtual string Bulan { get; set; }
        public virtual int SeqNo { get; set; }
        public virtual int Nilai { get; set; }
        public virtual int Saldo { get; set; }
        public virtual int AkumulasiPenyusutan { get; set; }
        public virtual string Status { get; set; }
        public virtual DateTime StatusDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual DateTime UpdateDate { get; set; }
    }
}
