﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class SubGroupAsset : EntityString
    {
        protected SubGroupAsset()
        { }
        public SubGroupAsset(string id) : base(id)
        {
            this.SubGroupAssetID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string SubGroupAssetID { get; set; }
        public virtual string SubGroupAssetDescription { get; set; }
        public virtual string GroupAssetID { get; set; }
        public virtual double MasaManfaat { get; set; }
        public virtual string GolonganPajakID { get; set; }
        public virtual bool Status { get; set; } 
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
