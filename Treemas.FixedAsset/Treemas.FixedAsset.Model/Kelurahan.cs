﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class Kelurahan : EntityString
    {
        protected Kelurahan() { }
        public Kelurahan(string id) : base(id)
        {
            ID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string ID { get; set; }
        public virtual string NamaKelurahan { get; set; }
        public virtual string Kecamatan { get; set; }
        public virtual string Kota { get; set; }
        public virtual string ZipCode { get; set; }
        public virtual string AreaCode { get; set; }
        public virtual string Provinsi { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
