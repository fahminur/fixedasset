﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class TerimaInvoiceD : Entity
    {
        protected TerimaInvoiceD()
        { }
        public TerimaInvoiceD(long id) : base(id)
        {

        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual long VendorId { get; set; }
        public virtual string NoInvoice { get; set; }
        public virtual int SeqNo { get; set; }
        public virtual string JenisInvoice { get; set; }
        public virtual string NamaBarangJasa { get; set; }
        public virtual int Jumlah { get; set; }
        public virtual string Satuan { get; set; }
        public virtual string MataUang { get; set; }
        public virtual double HargaSatuan { get; set; }
        public virtual double Nilai { get; set; }
        public virtual int PPn { get; set; }
        public virtual int PPh23 { get; set; }
        public virtual string AktivaIDNoReff { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
