﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class AssetUser : EntityString
    {
        protected AssetUser()
        { }
        public AssetUser(string id) : base(id)
        {
            this.AssetUserID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string AssetUserID { get; set; }
        public virtual string AssetUserName { get; set; }
        public virtual string AssetBranchID { get; set; }
        public virtual string AssetDeptID { get; set; }
        public virtual bool Status { get; set; } 
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
