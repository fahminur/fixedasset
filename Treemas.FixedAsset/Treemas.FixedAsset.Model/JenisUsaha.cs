﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class JenisUsaha : EntityString
    {
        protected JenisUsaha()
        { }
        public JenisUsaha(string id) : base(id)
        {
            this.JenisUsahaID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string JenisUsahaID { get; set; }
        public virtual string NamaJenisUsaha { get; set; }
        public virtual string JenisUsahaShortDesc { get; set; }
        public virtual bool Status { get; set; } 
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
