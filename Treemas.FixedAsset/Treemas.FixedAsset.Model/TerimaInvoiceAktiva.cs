﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class TerimaInvoiceAktiva : Entity
    {
        protected TerimaInvoiceAktiva()
        { }
        public TerimaInvoiceAktiva(long id) : base(id)
        {

        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual long VendorId { get; set; }
        public virtual string NoInvoice { get; set; }
        public virtual int SeqNo { get; set; }
        public virtual string AktivaID { get; set; }
        public virtual string NoPO { get; set; }
        public virtual string NoDO { get; set; }
        public virtual string NoPenerimaan { get; set; }
        public virtual DateTime? TanggalPerolehan { get; set; }
        public virtual string UOM { get; set; }
        public virtual int Qty { get; set; }
        public virtual string MataUang { get; set; }
        public virtual double NilaiOriginal { get; set; }
        public virtual double Kurs { get; set; }
        public virtual double HargaPerolehan { get; set; }
        public virtual int PPn { get; set; }
        public virtual int PPh23 { get; set; }
        public virtual string Keterangan { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
