﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PenerimaanBarangAktiva : Entity
    {
        protected PenerimaanBarangAktiva() { }
        public PenerimaanBarangAktiva(int id) : base(id)
        {
            this.Seq = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string GRNo { get; set; }
        public virtual int Seq { get; set; }
        public virtual string AktivaId { get; set; }
        public virtual double Price { get; set; }
        public virtual int Quantity { get; set; }
        public virtual double TotalPrice { get; set; }
        public virtual string Notes { get; set; }

    }
}
