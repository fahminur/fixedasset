﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PenerimaanBarangService : Entity
    {
        protected PenerimaanBarangService() { }
        public PenerimaanBarangService(int id) : base(id)
        {
            Seq = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string GRNo { get; set; }
        public virtual int Seq { get; set; }
        public virtual string AktivaId { get; set; }
        public virtual double Amount { get; set; }
        public virtual string Notes { get; set; }
    }
}
