﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PenerimaanBarangNonAktiva : Entity
    {
        protected PenerimaanBarangNonAktiva() { }
        public PenerimaanBarangNonAktiva(int id) : base(id)
        {
            Seq = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string GRNo { get; set; }
        public virtual int Seq { get; set; }
        public virtual string NonAktivaId { get; set; }
        public virtual string RefferenceNo { get; set; }
        public virtual string GroupAssetID { get; set; }
        public virtual string ItemName { get; set; }
        public virtual double Price { get; set; }
        public virtual int Quantity { get; set; }
        public virtual double TotalPrice { get; set; }

        public virtual string Notes { get; set; }
    }
}
