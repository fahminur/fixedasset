﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PenerimaanBarangHdr : EntityString
    {
        protected PenerimaanBarangHdr() { }
        public PenerimaanBarangHdr(string id) : base(id)
        {
            GRNo = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string GRNo { get; set; }
        public virtual string VendorID { get; set; }
        public virtual string DeliveryOrderNo { get; set; }
        public virtual string Notes { get; set; }
        public virtual string GRType { get; set; }
        public virtual string Status { get; set; }
        public virtual DateTime DeliveryOrderDate { get; set; }
        public virtual DateTime GRDate { get; set; }
        public virtual double GRAmount { get; set; }
        public virtual IList<PenerimaanBarangAktiva> PenerimaanAktivaList { get; set; }
        public virtual IList<PenerimaanBarangNonAktiva> PenerimaanNonAktivaList { get; set; }
        public virtual IList<PenerimaanBarangService> PenerimaanServiceList { get; set; }

        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual PenerimaanStatusEnum penerimaanStatusEnum
        {
            get
            {
                return (PenerimaanStatusEnum)Enum.Parse(typeof(PenerimaanStatusEnum), this.Status);
            }
        }
        public virtual bool AllowEdit
        {
            get
            {
                switch (penerimaanStatusEnum)
                {
                    case PenerimaanStatusEnum.R:
                    case PenerimaanStatusEnum.N:
                        return true;
                    //case InvoiceStatusEnum.P:
                    //    return false;
                    default:
                        return false;
                }
            }
        }

        public virtual bool AllowApprove
        {
            get
            {
                switch (penerimaanStatusEnum)
                {
                    //case PenerimaanStatusEnum.R:
                    case PenerimaanStatusEnum.N:
                        return true;
                    //case InvoiceStatusEnum.P:
                    //    return false;
                    default:
                        return false;
                }
            }
        }

        public virtual bool AllowReject
        {
            get
            {
                switch (penerimaanStatusEnum)
                {
                    //case PenerimaanStatusEnum.R:
                    case PenerimaanStatusEnum.N:
                        return true;
                    //case InvoiceStatusEnum.P:
                    //    return false;
                    default:
                        return false;
                }
            }
        }
    }
}
