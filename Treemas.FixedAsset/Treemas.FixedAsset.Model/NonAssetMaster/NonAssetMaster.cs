﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;


namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class NonAssetMaster : EntityString
    {
        protected NonAssetMaster()
        {

        }
        public NonAssetMaster(string id) : base(id)
        {
            this.NonAktivaID = id;

        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string NonAktivaID { get; set; }
        public virtual string NamaNonAktiva { get; set; }
        public virtual string GroupAssetID { get; set; }
        public virtual string SubGroupAssetID { get; set; }
        public virtual string SiteID { get; set; }
        public virtual string Lokasi { get; set; }
        public virtual string AssetAreaID { get; set; }
        public virtual string AssetDepID { get; set; }
        public virtual string AssetUserID { get; set; }
        public virtual string AssetConditionID { get; set; }
        public virtual string Merek { get; set; }
        public virtual string Model { get; set; }
        public virtual string SerialNo1 { get; set; }
        public virtual string SerialNo2 { get; set; }
        public virtual DateTime? MasaGaransi { get; set; }
        public virtual string InsuranceComID { get; set; }
        public virtual string InsuranceComBranchID { get; set; }
        public virtual string KeteranganAktiva { get; set; }
        public virtual string VendorID { get; set; }
        public virtual string NoPO { get; set; }
        public virtual string NoDO { get; set; }
        public virtual string NoGSRN { get; set; }
        public virtual string NoInvoice { get; set; }
        public virtual DateTime? TanggalPerolehan { get; set; }
        public virtual DateTime? TanggalTerimaAsset { get; set; }
        public virtual double NilaiOriginal { get; set; }
        public virtual double Kurs { get; set; }
        public virtual double HargaPerolehan { get; set; }
        public virtual string KeteranganPerolehan { get; set; }
        public virtual string NoPolis { get; set; }
        public virtual DateTime? TglPolis { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string Status { get; set; }
        public virtual AssetStatusEnum NAStatusEnum
        {
            get
            {
                return (AssetStatusEnum)Enum.Parse(typeof(AssetStatusEnum), this.Status.ToString());
            }
        }
    }
}
