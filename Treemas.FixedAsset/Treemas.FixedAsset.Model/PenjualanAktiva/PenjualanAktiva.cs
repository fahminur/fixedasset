﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PenjualanAktiva : EntityString
    {
        protected PenjualanAktiva() { }
        public PenjualanAktiva(string id) : base(id)
        {
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string AktivaID { get; set; }
        public virtual string AktivaName { get; set; }
        public virtual double SellingPrice { get; set; }
        public virtual string Buyer { get; set; }
        public virtual DateTime? SellingDate { get; set; }
        public virtual string ReferenceNo { get; set; }
        public virtual string InternalMemoNo { get; set; }
        public virtual DateTime? StopDepresiationDate { get; set; }
        public virtual string Notes { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual string Status { get; set; }
        public virtual bool IsApproved1 { get; set; }
        public virtual string Approved1User { get; set; }
        public virtual DateTime? Approved1Date { get; set; }
        public virtual bool IsApproved2 { get; set; }
        public virtual string Approved2User { get; set; }
        public virtual DateTime? Approved2Date { get; set; }
        public virtual bool IsApproved3 { get; set; }
        public virtual string Approved3User { get; set; }
        public virtual DateTime?Approved3Date { get; set; }

        public virtual PenjualanStatusEnum PenjualanStsEnum
        {
            get
            {
                return (PenjualanStatusEnum)Enum.Parse(typeof(PenjualanStatusEnum), this.Status);
            }
        }
        public virtual bool AllowEdit
        {
            get
            {
                switch (PenjualanStsEnum)
                {
                    case PenjualanStatusEnum.N:
                    //case PenjualanStatusEnum.R1:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowApprove1
        {
            get
            {
                switch (PenjualanStsEnum)
                {
                    case PenjualanStatusEnum.N:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowApprove2
        {
            get
            {
                switch (PenjualanStsEnum)
                {
                    //case PenjualanStatusEnum.A1:
                    //    return true;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowApprove3
        {
            get
            {
                switch (PenjualanStsEnum)
                {
                    //case PenjualanStatusEnum.A2:
                    //    return true;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowReject1
        {
            get
            {
                switch (PenjualanStsEnum)
                {
                    case PenjualanStatusEnum.N:
                    case PenjualanStatusEnum.A:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowReject2
        {
            get
            {
                switch (PenjualanStsEnum)
                {
                    //case PenjualanStatusEnum.R3:
                    //    return true;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowReject3
        {
            get
            {
                switch (PenjualanStsEnum)
                {
                    //case PenjualanStatusEnum.A2:
                    //case PenjualanStatusEnum.A3:
                    //    return false;
                    default:
                        return false;
                }
            }
        }
    }
}
