﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PenjualanAktivaHeader : EntityString
    {
        protected PenjualanAktivaHeader()
        {

        }
        public PenjualanAktivaHeader(string id): base(id)
        {
            SellingNo = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        public virtual string SellingNo { get; set; }
        public virtual DateTime? SellingDate { get; set; }
        public virtual string ReferenceNo { get; set; }
        public virtual string InternalMemoNo { get; set; }
        public virtual string Notes { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual string Status { get; set; }
        public virtual IList<PenjualanAktivaDetail> PenjualanAktivaDetailList { get; set; }

        public virtual PenjualanStatusEnum PenjualanStsEnum
        {
            get
            {
                return (PenjualanStatusEnum)Enum.Parse(typeof(PenjualanStatusEnum), this.Status);
            }
        }
        public virtual bool AllowEdit
        {
            get
            {
                switch (PenjualanStsEnum)
                {
                    case PenjualanStatusEnum.N:
                    case PenjualanStatusEnum.R:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowApprove
        {
            get
            {
                switch (PenjualanStsEnum)
                {
                    case PenjualanStatusEnum.N:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowAReject
        {
            get
            {
                switch (PenjualanStsEnum)
                {
                    case PenjualanStatusEnum.N:
                    case PenjualanStatusEnum.A:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}
