﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;


namespace Treemas.FixedAsset.Model
{
    public class PenjualanAktivaDetail : Entity
    {
        protected PenjualanAktivaDetail()
        {

        }
        public PenjualanAktivaDetail(int id):base(id)
        {
            Seq = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        public virtual string SellingNo { get; set; }
        public virtual int Seq { get; set; }
        public virtual string AktivaID { get; set; }
        public virtual double SellingPrice { get; set; }
        public virtual string Buyer { get; set; }
        public virtual DateTime? StopDepresiationDate { get; set; }
        public virtual string Notes { get; set; }
        public virtual int DepreciationSeqNo { get; set; }
    }
}
