﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class MetodeDepresiasi : EntityString
    {
        public MetodeDepresiasi()
        {

        }
        public MetodeDepresiasi(string id) : base(id)
        {
            this.MetodeDepresiasiID = id;
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string MetodeDepresiasiID { get; set; }
        public virtual string MetodeDepresiasiName { get; set; }
        public virtual string MetodeDepresiasiInitial { get; set; }
        public virtual string Status { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
