﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PerawatanAktiva : EntityString
    {
        protected PerawatanAktiva() { }
        public PerawatanAktiva(string id) : base(id)
        {
            this.PerawatanAktivaID = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string PerawatanAktivaID { get; set; }
        public virtual string AktivaID { get; set; }
        public virtual string NamaAktiva { get; set; }
        public virtual DateTime TanggalPerawatan { get; set; }
        public virtual string NoReferensi { get; set; }
        public virtual string Keterangan { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual double NilaiOriginal { get; set; }
        public virtual int Kurs { get; set; }
        public virtual double Biaya { get; set; }
        public virtual string VendorID { get; set; }
        public virtual string NoInvoice { get; set; }
        public virtual string COA { get; set; }
        public virtual DateTime MasaGaransi { get; set; }

        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }

        IList<PerawatanAktivaDetail> PerawatanAktivaDetailList { get; set; }
    }
}
