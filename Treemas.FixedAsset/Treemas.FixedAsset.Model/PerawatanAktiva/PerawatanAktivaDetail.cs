﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PerawatanAktivaDetail : EntityString
    {
        public PerawatanAktivaDetail()
        {

        }
        public PerawatanAktivaDetail(string id) : base(id)
        {
            this.AktivaID = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string AktivaID { get; set; }
        public virtual string NamaAktiva { get; set; }
        public virtual DateTime TanggalPerawatan { get; set; }
        public virtual string VendorID { get; set; }
        public virtual string VendorName { get; set; }
        public virtual string NoInvoice { get; set; }
        public virtual string COAPerawatan { get; set; }
        public virtual double NilaiOriginal { get; set; }
        public virtual double Biaya { get; set; }
        public virtual DateTime MasaGaransi { get; set; }
    }
}
