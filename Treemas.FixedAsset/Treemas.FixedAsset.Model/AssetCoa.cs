﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class AssetCoa : EntityString
    {
        protected AssetCoa()
        { }
        public AssetCoa(string id) : base(id)
        {
            this.COAID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string COAID { get; set; }
        public virtual string COADescription { get; set; }
        public virtual string CoaType { get; set; }
        public virtual CoaTypeEnum CoaTypeEnums
        {
            get
            {
                return (CoaTypeEnum)Enum.Parse(typeof(CoaTypeEnum), this.CoaType);
            }
        }
       
        public virtual bool Status { get; set; } 
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
