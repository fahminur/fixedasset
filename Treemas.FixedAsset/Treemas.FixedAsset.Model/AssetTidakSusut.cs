﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
   [Serializable]
    public class AssetTidakSusut : Entity
    {
        protected AssetTidakSusut()
        { }
        public AssetTidakSusut(long id) : base(id)
        {
            
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }


        public virtual long RowID { get; set; }
        public virtual string AktivaId { get; set; }
        public virtual string Bulan { get; set; }
        public virtual string Tahun { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
