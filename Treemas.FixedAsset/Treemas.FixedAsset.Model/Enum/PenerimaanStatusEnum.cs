﻿using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public enum PenerimaanStatusEnum:int
    {
        [StringValue("New")]
        N = 1,
        [StringValue("Approved")]
        A = 2,
        [StringValue("Rejected")]
        R = 3,
        [StringValue("Invoiced")]
        I = 4,
    }
}
