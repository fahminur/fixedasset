﻿using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public enum CoaTypeEnum : int
    {
        [StringValue("Aktiva")]
        A = 1,
        [StringValue("Akumulasi Penyusutan")]
        P = 2,
        [StringValue("Biaya Penyusutan")]
        B = 3
    }
}
