﻿using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public enum InvoiceStatusEnum : int
    {
        [StringValue("New")]
        N = 1,
        [StringValue("Approved")]
        A = 2,
        [StringValue("Requested")]
        Q = 3,
        [StringValue("Request Approved")]
        S = 4,
        [StringValue("Paid")]
        P = 5,
        [StringValue("Rejected")]
        X = 6,
    }
}
