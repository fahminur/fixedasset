﻿using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public enum JenisInvoiceEnum : int
    {
        [StringValue("ChooseTable")]
        Choose = 0,
        [StringValue("Aktiva")]
        Aktiva = 1,
        [StringValue("Non Aktiva")]
        NonAktiva = 2,
        [StringValue("Perawatan")]
        Perawatan = 3
    }
}
