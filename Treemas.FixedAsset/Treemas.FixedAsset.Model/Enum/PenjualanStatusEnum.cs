﻿using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public enum PenjualanStatusEnum:int
    {
        [StringValue("New")]
        N = 1,
        [StringValue("Approved")]
        A = 2,
        [StringValue("Rejected")]
        R = 3,
        //[StringValue("Supervisor Approved")]
        //A1 = 2,
        //[StringValue("Manager Approved")]
        //A2 = 3,
        //[StringValue("Director Approved")]
        //A3 = 4,
        //[StringValue("Supervisor Rejected")]
        //R1 = 5,
        //[StringValue("Manager Rejected")]
        //R2 = 6,
        //[StringValue("Director Rejected")]
        //R3 = 7,
    }
}
