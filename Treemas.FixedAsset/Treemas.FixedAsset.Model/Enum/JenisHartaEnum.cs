﻿using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public enum JenisHartaEnum : int
    {
        [StringValue("Tangible")]
        T = 1,
        [StringValue("Itangible")]
        IT = 2,
        [StringValue("Bangunan")]
        B = 3
    }
}
