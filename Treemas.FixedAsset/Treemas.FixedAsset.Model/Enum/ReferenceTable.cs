﻿using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public enum ReferenceTable : int
    {
        [StringValue("ChooseTable")]
        Choose = 0,
        [StringValue("Asset Branch")]
        InvoicingRule = 1,
        [StringValue("Asset Condition")]
        PurchaseCatalogClas = 2,
        [StringValue("Asset Dept")]
        PurchaseCatalogType = 3,
        [StringValue("Asset Location")]
        PurchaseOrderType = 4,
        [StringValue("Asset User")]
        RequisitionType = 5,
        [StringValue("Jenis Usaha")]
        JenisUsaha = 6
    }
}
