﻿using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public enum MetodeDepresiasi : int
    {
        [StringValue("Single Line/ Garis Lurus")]
        GL = 1
    }
}
