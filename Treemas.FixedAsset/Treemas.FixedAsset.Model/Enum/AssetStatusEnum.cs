﻿using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public enum AssetStatusEnum:int
    {
        [StringValue("Aktif")]
        A = 1,
        [StringValue("Non-Aktif")]
        N = 2,
        [StringValue("Dihapus")]
        D = 3,
        [StringValue("Dijual")]
        S = 4,
        [StringValue("Diterima")]
        R = 5,
    }
}
