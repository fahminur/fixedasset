﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class TerimaInvoiceH : Entity
    {
        protected TerimaInvoiceH()
        { }
        public TerimaInvoiceH(long id) : base(id)
        {

        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        
        public virtual long VendorId { get; set; }
        public virtual string NoInvoice { get; set; }
        public virtual DateTime? TanggalInvoice { get; set; }
        public virtual DateTime? TanggalJatuhTempo { get; set; }
        public virtual DateTime? TanggalRencanaBayar { get; set; }
        public virtual long VendorBankId { get; set; }
        public virtual double TotalInvoice { get; set; }
        public virtual double PPn { get; set; }
        public virtual double PPh23 { get; set; }
        public virtual double TotalBayar { get; set; }
        public virtual string Catatan { get; set; }
        public virtual bool Status { get; set; }

        public virtual IList<TerimaInvoiceD> Detail { get; set; }
        public virtual IList<TerimaInvoiceAktiva> Aktiva { get; set; }
        public virtual IList<TerimaInvoiceNonAktiva> NonAktiva { get; set; }
        public virtual IList<TerimaInvoicePerawatan> Perawatan { get; set; }

        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
