﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PenghapusanAktivaHeader : EntityString
    {
        protected PenghapusanAktivaHeader() { }
        public PenghapusanAktivaHeader(string id) : base(id)
        {
            this.DeleteNo = id;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        public virtual string DeleteNo { get; set; }
        public virtual DateTime DeleteDate { get; set; }
        public virtual string InternalMemoNo { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual DateTime UpdateDate { get; set; }
        public virtual string Status { get; set; }
        public virtual string Notes { get; set; }
        public virtual IList<PenghapusanAktivaDetail> PenghapusanAktivaDetailList { get; set; }

        public virtual PenjualanStatusEnum PenghapusanStsEnum
        {
            get
            {
                return (PenjualanStatusEnum)Enum.Parse(typeof(PenjualanStatusEnum), this.Status);
            }
        }
        public virtual bool AllowEdit
        {
            get
            {
                switch (PenghapusanStsEnum)
                {
                    case PenjualanStatusEnum.N:
                    case PenjualanStatusEnum.R:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowApprove
        {
            get
            {
                switch (PenghapusanStsEnum)
                {
                    case PenjualanStatusEnum.N:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowReject
        {
            get
            {
                switch (PenghapusanStsEnum)
                {
                    case PenjualanStatusEnum.N:
                    case PenjualanStatusEnum.A:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}
