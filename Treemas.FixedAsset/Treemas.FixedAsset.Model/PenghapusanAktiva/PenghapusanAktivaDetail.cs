﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PenghapusanAktivaDetail : Entity
    {
        protected PenghapusanAktivaDetail() { }
        public PenghapusanAktivaDetail(int id) : base(id)
        {
            this.Seq = id;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        public virtual string DeleteNo { get; set; }
        public virtual int Seq { get; set; }
        public virtual string AktivaID { get; set; }
        public virtual string DeleteReason { get; set; }
        public virtual string Notes { get; set; }
    }
}
