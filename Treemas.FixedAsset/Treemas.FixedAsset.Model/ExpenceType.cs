﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class ExpenceType: EntityString
    {
        public ExpenceType()
        {

        }
        public ExpenceType(string id):base(id)
        {
            this.ExpenceTypeID = id;
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string ExpenceTypeID { get; set; }
        public virtual string ExpenceTypeName { get; set; }
        public virtual string ExpenceTypeInitial { get; set; }
        public virtual string Status { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
