﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class FixedAssetMaster : EntityString
    {
        protected FixedAssetMaster()
        { }
        public FixedAssetMaster(string id) : base(id)
        {
            this.AktivaID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string AktivaID { get; set; }
        public virtual string AssetCode { get; set; }
        public virtual string NamaAktiva { get; set; }
        public virtual string GroupAssetID { get; set; }
        public virtual string SubGroupAssetID { get; set; }
        public virtual string NamaHartaID { get; set; }
        public virtual string COAAktivaID { get; set; }
        public virtual string COAAkumulasiID { get; set; }
        public virtual string COAPenyusutanID { get; set; }
        public virtual string ParentAktivaID { get; set; }
        public virtual string SiteID { get; set; }
        public virtual string AssetLocationID { get; set; }
        public virtual string Lokasi { get; set; }
        public virtual string AssetAreaID { get; set; }
        public virtual string AssetDepID { get; set; }
        public virtual string AssetUserID { get; set; }
        public virtual string AssetConditionID { get; set; }
        public virtual string Merek { get; set; }
        public virtual string Model { get; set; }
        public virtual string SerialNo1 { get; set; }
        public virtual string SerialNo2 { get; set; }
        public virtual DateTime? MasaGaransi { get; set; }
        public virtual string InsuranceComID { get; set; }
        public virtual string InsuranceComBranchID { get; set; }
        public virtual string KeteranganAktiva { get; set; }
        public virtual string VendorID { get; set; }
        public virtual string NoPO { get; set; }
        public virtual string NoDO { get; set; }
        public virtual string NoGSRN { get; set; }
        public virtual string NoInvoice { get; set; }
        public virtual string NoPaymentVoucher { get; set; }
        public virtual DateTime? TanggalPerolehan { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual double NilaiOriginal { get; set; }
        public virtual double Kurs { get; set; }
        public virtual double HargaPerolehan { get; set; }
        public virtual string KeteranganPerolehan { get; set; }
        public virtual DateTime? TanggalPerolehanKomersial { get; set; }
        public virtual double HargaPerolehanKomersial { get; set; }
        public virtual bool KalkulasiOtomatis { get; set; }
        public virtual DateTime? TanggalCatat { get; set; }
        public virtual string MetodeDepresiasiKomersial { get; set; }
        public virtual double MasaManfaatKomersial { get; set; }
        public virtual string PeriodeAwal { get; set; }
        public virtual string PeriodeAkhir { get; set; }
        public virtual double Tarif { get; set; }
        public virtual double ResiduKomersial { get; set; }
        public virtual DateTime? TanggalPenghapusan { get; set; }
        public virtual string NomorReferensi { get; set; }
        public virtual double HargaJual { get; set; }
        public virtual bool Penyusutandihitung { get; set; }
        public virtual string AlasanPengapusan { get; set; }
        public virtual DateTime? TanggalPerolehanFiskal { get; set; }
        public virtual double HargaPerolehanFiskal { get; set; }
        public virtual bool KalkulasiOtomatisFiskal { get; set; }
        public virtual DateTime? TanggalCatatFiskal { get; set; }
        public virtual string GolonganPajakID { get; set; }
        public virtual string MetodeDepresiasiFiskal { get; set; }
        public virtual double MasaManfaatFiskal { get; set; }
        public virtual string PeriodeMulaiFiskal { get; set; }
        public virtual string PeriodeAkhirFiskal { get; set; }
        public virtual double TarifFiskal { get; set; }
        public virtual double Pembebanan { get; set; }
        public virtual double ResiduFiskal { get; set; }
        public virtual DateTime? TanggalPenghapusanFiskal { get; set; }
        public virtual string NomorReferensiFiskal { get; set; }
        public virtual double HargaJualFiskal { get; set; }
        public virtual bool PenyusutanFiskal { get; set; }
        public virtual string AlasanFiskal { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual double AkumulasiPenyusutanFiskal { get; set; }
        public virtual double AkumulasiPenyusutanKomersial { get; set; }
        public virtual double SisaNilaiBukuFiskal { get; set; }
        public virtual double SisaNilaiBukuKomersial { get; set; }
        public virtual DateTime? TanggalTerimaAsset { get; set; }
        public virtual string Status { get; set; }
        public virtual bool IsOLS { get; set; }
        public virtual AssetStatusEnum FAStatusEnum
        {
            get
            {
                return (AssetStatusEnum)Enum.Parse(typeof(AssetStatusEnum), this.Status.ToString());
            }
        }
        public virtual string NoPol { get; set; }
        public virtual string STNK { get; set; }
        public virtual string BPKB { get; set; }
        public virtual string NoRangka { get; set; }
        public virtual string NoMesin { get; set; }
        public virtual string NoPolis { get; set; }
        public virtual string NomorFaktur { get; set; }
        public virtual string NomorNIKVIN { get; set; }
        public virtual string FormANo { get; set; }
        public virtual int TahunPembuatan { get; set; }
        public virtual DateTime? TglPolis { get; set; }
    }
}
