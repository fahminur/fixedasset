﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class AssetArea : EntityString
    {
        protected AssetArea()
        { }
        public AssetArea(string id) : base(id)
        {
            this.AssetAreaID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string AssetAreaID { get; set; }
        public virtual string AssetAreaDescription { get; set; }
        public virtual string AssetAreaShortDesc { get; set; }
        public virtual string AssetBranchID { get; set; }
        public virtual bool Status { get; set; } 
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
