﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class GLJournalType : EntityString
    {
        public GLJournalType()
        {

        }
        public GLJournalType(string id) : base(id)
        {
            TransactionID = id;
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string TransactionID { get; set; }
        public virtual string TransactionDesc { get; set; }
    }
}
