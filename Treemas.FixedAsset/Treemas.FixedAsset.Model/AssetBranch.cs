﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class AssetBranch : EntityString
    {
        protected AssetBranch()
        { }
        public AssetBranch(string id) : base(id)
        {
            this.AssetBranchID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string AssetBranchID { get; set; }
        public virtual string AssetBranchDescription { get; set; }
        public virtual string AssetBranchShortDesc { get; set; }
        public virtual bool Status { get; set; } 
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
