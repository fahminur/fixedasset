﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class TerimaInvoiceNonAktiva : Entity
    {
        protected TerimaInvoiceNonAktiva()
        { }
        public TerimaInvoiceNonAktiva(long id) : base(id)
        {

        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual long VendorId { get; set; }
        public virtual string NoInvoice { get; set; }
        public virtual int SeqNo { get; set; }
        public virtual string NoRefference { get; set; }
        public virtual string NamaBarangJasa { get; set; }
        public virtual double Nilai { get; set; }
        public virtual string CaraPembebanan { get; set; }
        public virtual int JangkaWaktu { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual int BebanPerBulan { get; set; }
        public virtual int PPn { get; set; }
        public virtual int PPh23 { get; set; }
        public virtual string COABiaya { get; set; }
        public virtual string COAPrepaid { get; set; }
        public virtual string NoPO { get; set; }
        public virtual string NoDO { get; set; }
        public virtual string NoPenerimaan { get; set; }
        public virtual string Keterangan { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
