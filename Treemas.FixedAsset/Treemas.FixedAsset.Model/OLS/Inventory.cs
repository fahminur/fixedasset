﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class Inventory : EntityString
    {
        protected Inventory() { }
        public Inventory(string id) : base(id)
        {
            UnitID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string UnitID { get; set; }
        public virtual string AssetCode { get; set; }
        public virtual string GRNCode { get; set; }
        public virtual DateTime? GRNDate { get; set; }
        public virtual string NoRangka { get; set; }
        public virtual string NoMesin { get; set; }
        public virtual string NoPolisi { get; set; }
        public virtual string NewUsed { get; set; }
        public virtual string Color { get; set; }
        public virtual int ManufacturingYear { get; set; }
        public virtual double TotalOTR { get; set; }
        public virtual string SupplierID { get; set; }
        public virtual string ReceiveAt { get; set; }
        public virtual string UnitStatus { get; set; }
        public virtual Int64 AgreementSeqNo { get; set; }
        public virtual string RentalType { get; set; }
        public virtual string InProcessStatus { get; set; }
        public virtual string AlocationUnitStatus { get; set; }
        public virtual string OperationalStatus { get; set; }
        public virtual string UnitCurrentStatus { get; set; }
        public virtual string PreDeliveryInspection { get; set; }
        public virtual string UserCreate { get; set; }
        public virtual DateTime? DateCreate { get; set; }
        public virtual string UserLastUpdate { get; set; }
        public virtual DateTime? DateLastUpdate { get; set; }
    }
}
