﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class Asset : EntityString
    {
        protected Asset() { }
        public Asset(string id) : base(id)
        {
            AssetCode = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string AssetCode { get; set; }
        //public virtual string AssetParent { get; set; }
        public virtual string AssetName { get; set; }
        public virtual string AssetTypeID { get; set; }
        public virtual string SiteID { get; set; }
        //public virtual string AssetCategoryID { get; set; }
        //public virtual int AssetLevel { get; set; }
        //public virtual bool IsFinalLevel { get; set; }
        //public virtual double ResaleValue { get; set; }
        //public virtual int CylinderCapacity { get; set; }
        //public virtual int SeatCapacity { get; set; }
        //public virtual string TransmissionType { get; set; }
        //public virtual int MaxTransmission { get; set; }
        //public virtual string FuelType { get; set; }
        //public virtual double AverageConsumption { get; set; }
        //public virtual string MfgType { get; set; }
        //public virtual string MfgCountry { get; set; }
        public virtual bool IsActive { get; set; }
        //public virtual string UserCreate { get; set; }
        //public virtual DateTime DateCreate { get; set; }
        //public virtual string UserlastUpdate { get; set; }
        //public virtual DateTime DateLastUpdate { get; set; }
    }
}
