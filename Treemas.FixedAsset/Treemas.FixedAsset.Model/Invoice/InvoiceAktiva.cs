﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class InvoiceAktiva : Entity
    {
        protected InvoiceAktiva() { }
        public InvoiceAktiva(int id) : base(id)
        {
            this.ID = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string InvoiceNo { get; set; }
        public virtual int Seq { get; set; }
        public virtual string AssetSubGroupId { get; set; }
        public virtual string StartingAktivaId { get; set; }
        public virtual int ID { get; set; }
        public virtual string PurchaseOrderNo { get; set; }
        public virtual string DeliveryOrderNo { get; set; }
        public virtual string GRNo { get; set; }
        public virtual DateTime TanggalPerolehan { get; set; }
        public virtual double Quantity { get; set; }
        public virtual double Price { get; set; }
        public virtual double TotalPrice { get; set; }
        public virtual string PPnId { get; set; }
        public virtual double PPnAmount { get; set; }
        public virtual string PPhId { get; set; }
        public virtual double PPhAmount { get; set; }
        public virtual string Notes { get; set; }
        public virtual double TotalAmount { get; set; }
        public virtual double OtherCost { get; set; }
        public virtual double ExpedCost { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual bool Journaled { get; set; }
    }
}
