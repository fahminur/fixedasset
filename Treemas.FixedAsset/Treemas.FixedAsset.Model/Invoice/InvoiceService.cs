﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class InvoiceService : Entity
    {
        protected InvoiceService() { }
        public InvoiceService(int id) : base(id)
        {
            this.ID = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual int ID { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual int Seq { get; set; }
        public virtual string AktivaId { get; set; }
        public virtual string ExpensePA { get; set; }
        public virtual string PONo { get; set; }
        public virtual string WorkOrderNo { get; set; }
        public virtual DateTime ServiceDate { get; set; }
        public virtual DateTime ServiceGuaranteeDate { get; set; }
        public virtual double ServiceAmount { get; set; }
        public virtual double ItemAmount { get; set; }
        public virtual string PPnId { get; set; }
        public virtual double PPnAmount { get; set; }
        public virtual string PPhId { get; set; }
        public virtual double PPhAmount { get; set; }
        public virtual string MateraiID { get; set; }
        public virtual double MateraiAmount { get; set; }
        public virtual string Notes { get; set; }
        public virtual double TotalAmount { get; set; }
        public virtual double OtherCost { get; set; }
        public virtual double ExpedCost { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual bool Journaled { get; set; }
    }
}
