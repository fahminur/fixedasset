﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class InvoiceType : EntityString
    {
        public InvoiceType()
        {

        }
        public InvoiceType(string id) : base(id)
        {
            this.InvoiceTypeID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string InvoiceTypeID { get; set; }
        public virtual string InvoiceTypeName { get; set; }
        public virtual string InvoiceTypeInitial { get; set; }
        public virtual string Status { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
