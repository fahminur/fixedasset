﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class InvoiceNonAktiva : Entity
    {
        protected InvoiceNonAktiva() { }
        public InvoiceNonAktiva(int id) : base(id)
        {
            this.ID = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual int ID { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual int Seq { get; set; }
        public virtual string NonAktivaId { get; set; }
        public virtual string RefferenceNo { get; set; }
        public virtual string GroupAssetID { get; set; }
        public virtual string ItemName { get; set; }
        //public virtual int Qty { get; set; }
        public virtual double Price { get; set; }
        //public virtual double TotalPrice { get; set; }
        public virtual string ExpenseType { get; set; }
        public virtual int TimePeriod { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual double MonthlyExpense { get; set; }
        public virtual string ExpensePA { get; set; }
        public virtual string PrepaidPA { get; set; }
        public virtual string PONo { get; set; }
        public virtual string DeliveryOrderNo { get; set; }
        public virtual string GRNo { get; set; }
        public virtual string PPnId { get; set; }
        public virtual double PPnAmount { get; set; }
        public virtual string PPhId { get; set; }
        public virtual double PPhAmount { get; set; }
        public virtual string Notes { get; set; }
        public virtual double TotalAmount { get; set; }
        public virtual int DepreciatedTime { get; set; }
        public virtual double OtherCost { get; set; }
        public virtual double ExpedCost { get; set; }

        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual bool Journaled { get; set; }
    }
}
