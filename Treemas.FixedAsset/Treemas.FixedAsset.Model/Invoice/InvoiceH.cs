﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class InvoiceH : EntityString
    {
        protected InvoiceH() { }
        public InvoiceH(string id) : base(id)
        {
            this.InvoiceNo = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        public virtual string GRNo { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual string VendorInvoiceNo { get; set; }
        public virtual string DeliveryOrderNo { get; set; }
        public virtual string PONo { get; set; }
        public virtual string VendorID { get; set; }
        public virtual DateTime InvoiceDate { get; set; }
        public virtual DateTime InvoiceDueDate { get; set; }
        public virtual DateTime InvoiceOrderDate { get; set; }
        public virtual string InvoiceDateString { get; set; }
        public virtual string InvoiceDueDateString { get; set; }
        public virtual string InvoiceOrderDateString { get; set; }
        public virtual string Notes { get; set; }
        public virtual string VendorBankId { get; set; }
        public virtual double InvoiceAmount { get; set; }
        public virtual string Status { get; set; }
        public virtual InvoiceStatusEnum InvStatusEnum
        {
            get
            {
                return (InvoiceStatusEnum)Enum.Parse(typeof(InvoiceStatusEnum), this.Status);
            }
        }
        public virtual string InvoiceTypeID { get; set; }
        public virtual string NoFakturPajak { get; set; }
        public virtual DateTime TglFakturPajak { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }

        public virtual IList<InvoiceAktiva> InvoiceAktivaList { get; set; }
        public virtual IList<InvoiceNonAktiva> InvoiceNonAktivaList { get; set; }
        public virtual IList<InvoiceService> InvoiceServiceList { get; set; }

        public virtual bool AllowEdit
        {
            get
            {
                switch (InvStatusEnum)
                {
                    case InvoiceStatusEnum.N:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public virtual bool AllowApprove
        {
            get
            {
                switch (InvStatusEnum)
                {
                    case InvoiceStatusEnum.N:
                    case InvoiceStatusEnum.X:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public virtual bool AllowReject
        {
            get
            {
                switch (InvStatusEnum)
                {
                    case InvoiceStatusEnum.N:
                    case InvoiceStatusEnum.A:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}
