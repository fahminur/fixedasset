﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class NamaHarta : EntityString
    {
        protected NamaHarta()
        { }
        public NamaHarta(string id) : base(id)
        {
            this.NamaHartaID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string NamaHartaID { get; set; }
        public virtual string NamaHartaDeskripsi { get; set; }
        public virtual string JenisHartaID { get; set; }
        public virtual JenisHartaEnum JenisHartaEnums
        {
            get
            {
                return (JenisHartaEnum)Enum.Parse(typeof(JenisHartaEnum), this.JenisHartaID);
            }
        }
        public virtual bool Status { get; set; } 
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
