﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class GLJFilter
    {
        public DateTime? Tr_DateFrom { get; set; }
        public DateTime? Tr_DateTo { get; set; }
        public string TransactionID { get; set; }
        public string BranchID { get; set; }
        public string Tr_Nomor { get; set; }
    }
}
