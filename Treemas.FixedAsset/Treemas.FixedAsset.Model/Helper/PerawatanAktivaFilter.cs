﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class PerawatanAktivaFilter
    {
        public string AktivaID { get; set; }
        public string NamaAktiva { get; set; }
        public string VendorID { get; set; }
        public virtual DateTime? TanggalPerawatanFrom { get; set; }
        public virtual DateTime? TanggalPerawatanTo { get; set; }
        public string NoReferensi { get; set; }
        public string Keterangan { get; set; }
    }
}
