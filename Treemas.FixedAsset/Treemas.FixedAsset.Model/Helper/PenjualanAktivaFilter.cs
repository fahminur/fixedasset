﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class PenjualanAktivaFilter
    {
        public string AktivaID { get; set; }
        public string AktivaName { get; set; }
        public virtual DateTime? TanggalPenjualanFrom { get; set; }
        public virtual DateTime? TanggalPenjualanTo { get; set; }
        public string ReferenceNo { get; set; }
        public string SellingPrice { get; set; }
        public string Buyer { get; set; }
        public string InternalMemoNo { get; set; }
        public string Status { get; set; }
    }
}
