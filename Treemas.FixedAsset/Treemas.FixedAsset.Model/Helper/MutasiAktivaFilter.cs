﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class MutasiAktivaFilter
    {
        public string AktivaID { get; set; }
        public string NamaAktiva { get; set; }
        public string GroupAssetID { get; set; }
        public string SubGroupAssetID { get; set; }
        public virtual DateTime? TanggalMutasiFrom { get; set; }
        public virtual DateTime? TanggalMutasiTo { get; set; }
        public string BranchID { get; set; }
        public string AssetLocationID { get; set; }
        public string AssetAreaID { get; set; }
        public string AssetDepID { get; set; }
        public string AssetUserID { get; set; }
    }
}
