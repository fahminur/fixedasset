﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class NonAssetFilter
    {
        public string NonAktivaID { get; set; }
        public string NamaNonAktiva { get; set; }
        public string GroupAssetID { get; set; }
        public string SubGroupAssetID { get; set; }
        public string SiteID { get; set; }
        public string AssetLocationID { get; set; }
        public string AssetAreaID { get; set; }
        public string AssetDepID { get; set; }
        public string AssetUserID { get; set; }
        public string AssetConditionID { get; set; }
        public string Status { get; set; }
        public virtual DateTime? TanggalPerolehanStart { get; set; }
        public virtual DateTime? TanggalPerolehanEnd { get; set; }
    }
}
