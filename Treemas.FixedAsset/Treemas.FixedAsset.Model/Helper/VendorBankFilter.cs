﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class VendorBankFilter
    {
        public string VendorID { get; set; }
        public string VendorAccID { get; set; }
        public string VendorBankID { get; set; }
        public string VendorBankBranchID { get; set; }
        public string VendorAccountNo { get; set; }
        public string VendorAccountName { get; set; }
    }
}
