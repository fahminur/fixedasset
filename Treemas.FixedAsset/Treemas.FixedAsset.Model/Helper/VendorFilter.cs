﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class VendorFilter
    {
        public string VendorName { get; set; }
        public string ContactPersonName { get; set; }
    }
}
