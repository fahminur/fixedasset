﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class InvoiceFilter
    {
        public DateTime? PlanDisburseDateFrom { get; set; }
        public DateTime? PlanDisburseDateTo { get; set; }
        public DateTime? InvoiceDateFrom { get; set; }
        public DateTime? InvoiceDateTo { get; set; }
        public string VendorId { get; set; }
        public string VendorInvoiceNo { get; set; }
        public string InvoiceStatus { get; set; }
        public string AktivaID { get; set; }
        public string AktivaName { get; set; }
    }
}
