﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class PenerimaanBarangFilter
    {
        public string GRNo { get; set; }
        public string VendorID { get; set; }
        public string DeliveryOrderNo { get; set; }
        public DateTime? DeliveryOrderDateFrom { get; set; }
        public DateTime? DeliveryOrderDateTo { get; set; }
        public DateTime? GRDateFrom { get; set; }
        public DateTime? GRDateTo { get; set; }
        public string Status { get; set; }
    }
}
