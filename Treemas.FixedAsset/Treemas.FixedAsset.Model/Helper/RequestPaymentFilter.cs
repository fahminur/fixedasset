﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class RequestPaymentFilter
    {
        public DateTime? RequestDateFrom { get; set; }
        public DateTime? RequestDateTo { get; set; }
        public string VendorId { get; set; }
        public string VendorInvoiceNo { get; set; }
        public string InvoiceStatus { get; set; }
    }
}
