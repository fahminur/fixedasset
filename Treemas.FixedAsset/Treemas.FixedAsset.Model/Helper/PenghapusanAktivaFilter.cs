﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.FixedAsset.Model
{
    public class PenghapusanAktivaFilter
    {
        public string InternalMemoNo { get; set; }
        public DateTime? DeleteDateFrom { get; set; }
        public DateTime? DeleteDateTo { get; set; }
        public string Status { get; set; }
    }
}
