﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class PaymentAllocation : EntityString
    {
        protected PaymentAllocation()
        { }
        public PaymentAllocation(string id) : base(id)
        {
            this.PaymentAllocationID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string PaymentAllocationID { get; set; }
        public virtual string Description { get; set; }
        public virtual string DescriptionToPrint { get; set; }
        public virtual bool IsAgreement { get; set; }
        public virtual bool IsSystem { get; set; }
        public virtual bool IsScheme { get; set; }
        public virtual bool IsPettyCash { get; set; }
        public virtual bool IsPaymentReceive { get; set; }
        public virtual bool IsHOTransaction { get; set; }
        public virtual bool IsHeader { get; set; }
        public virtual string COA { get; set; }
        public virtual string Group_Coa { get; set; }
        public virtual string AddFree1 { get; set; }
        public virtual string AddFree2 { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string UsrUpd { get; set; }
        public virtual DateTime? DtmUpd { get; set; }
        public virtual bool isPaymentRequest { get; set; }
    }
}
