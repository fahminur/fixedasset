﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class InsuranceComHO : EntityString
    {
        protected InsuranceComHO() { }
        public InsuranceComHO(string id) : base(id)
        {
            MaskAssID = id;
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string MaskAssID { get; set; }
        public virtual string Description { get; set; }
        public virtual string Address { get; set; }
    }
}
