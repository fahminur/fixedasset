﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class AssetType : EntityString
    {
        protected AssetType() { }
        public AssetType(string id) : base(id)
        {
            AssetTypeID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string AssetTypeID { get; set; }
        public virtual string Description { get; set; }
        public virtual string SerialNo1Label { get; set; }
        public virtual string SerialNo2Label { get; set; }
        public virtual string AgreementNoID { get; set; }
        public virtual string AssetCode { get; set; }
    }
}
