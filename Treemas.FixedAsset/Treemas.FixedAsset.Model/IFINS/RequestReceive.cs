﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class RequestReceive : EntityString
    {
        protected RequestReceive()
        { }
        public RequestReceive(string id) : base(id)
        {
            this.Id = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string AktivaId { get; set; }
        public virtual string COA { get; set; }
        public virtual string Buyer { get; set; }
        public virtual double Amount { get; set; }
        public virtual string ReffNo { get; set; }
        public virtual string InternalMemoNo { get; set; }
        public virtual string Notes { get; set; }
       
        public virtual string Status { get; set; }
        public virtual string BranchID { get; set; }
    }
}
