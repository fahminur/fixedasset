﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class GeneralSetting : EntityString
    {
        protected GeneralSetting()
        {

        }

        public GeneralSetting(string id) : base(id)
        {
            this.GSID = id;
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string GSID { get; set; }
        public virtual string ModuleID { get; set; }
        public virtual string GSName { get; set; }
        public virtual string GSValue { get; set; }
    }
}
