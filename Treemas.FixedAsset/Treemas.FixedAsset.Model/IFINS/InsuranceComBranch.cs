﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class InsuranceComBranch : EntityString
    {
        protected InsuranceComBranch() { }
        public InsuranceComBranch(string id) : base(id)
        {
            MaskAssBranchID = id;
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string MaskAssID { get; set; }
        public virtual string MaskAssBranchID { get; set; }
        public virtual string BranchId { get; set; }
        public virtual string MaskAssBranchName { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual bool isActive { get; set; }
    }
}
