﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class GLPeriod : EntityString
    {
        protected GLPeriod()
        {

        }

        public GLPeriod(string id) : base(id)
        {
            this.CompanyId = id;
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string CompanyId { get; set; }
        public virtual string BranchId { get; set; }
        public virtual string PeriodId { get; set; }
        public virtual string PeriodName { get; set; }
        public virtual DateTime PeriodStart { get; set; }
        public virtual DateTime PeriodEnd { get; set; }
        public virtual int Year { get; set; }
        public virtual int PeriodStatus { get; set; }
        public virtual DateTime PeriodClosedOn { get; set; }
        public virtual int PeriodEverClosed { get; set; }
        public virtual DateTime PeriodChangeActiveMonthOn { get; set; }
        public virtual int PeriodEverChangeActiveMonth { get; set; }
    }
}
