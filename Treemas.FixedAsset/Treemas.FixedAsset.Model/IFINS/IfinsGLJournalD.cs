﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class IfinsGLJournalD : Entity
    {
        protected IfinsGLJournalD()
        {

        }
        public IfinsGLJournalD(int id) : base(id)
        {
            SequenceNo = id;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }

        public virtual string CompanyID { get; set; }       
        public virtual string BranchID { get; set; }        
        public virtual string Tr_Nomor { get; set; }        
        public virtual int SequenceNo { get; set; }         
        public virtual string CoaCo { get; set; }           
        public virtual string CoaBranch { get; set; }       
        public virtual string CoaId { get; set; }           
        public virtual string TransactionID { get; set; }   
        public virtual string Tr_Desc { get; set; }         
        public virtual string Post { get; set; }            
        public virtual double Amount { get; set; }          
        public virtual string CoaId_X { get; set; }                       
        public virtual string PaymentAllocationId { get; set; } 
        public virtual string ProductId { get; set; }           
        public virtual string UsrUpd { get; set; }            
        public virtual DateTime DtmUpd { get; set; }                      
    }
}
