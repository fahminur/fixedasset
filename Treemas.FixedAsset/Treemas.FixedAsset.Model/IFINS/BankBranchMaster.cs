﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class BankBranchMaster : EntityString
    {
        protected BankBranchMaster() { }
        public BankBranchMaster(string ID):base(ID)
        {
            this.BankBranchid = ID;
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string BankBranchid { get; set; }
        public virtual string BankCode { get; set; }
        public virtual string BankBranchName { get; set; }
        public virtual string BankID { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string City { get; set; }
    }
}
