﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class BankMaster : EntityString
    {
        protected BankMaster()
        { }
        public BankMaster(string id) : base(id)
        {
            this.BankID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string BankID { get; set; }
        public virtual string BankName { get; set; }
        public virtual string ShortName { get; set; }

    }
}
