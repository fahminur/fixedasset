﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class RequestPayment : EntityString
    {
        protected RequestPayment()
        { }
        public RequestPayment(string id) : base(id)
        {
            this.Id = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string InvoiceNo { get; set; }
        public virtual string VendorId { get; set; }
        public virtual string VendorBankId { get; set; }
        public virtual string VendorBankBranch { get; set; }
        public virtual string VendorBankAccountNo { get; set; }
        public virtual string VendorBankAccountName { get; set; }
        public virtual string VendorInvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual DateTime? PaymentOrderDate { get; set; }
        public virtual string CaraBayar { get; set; }
        public virtual double Jumlah { get; set; }
        public virtual string RefferenceNo { get; set; }
        public virtual string Notes { get; set; }
        public virtual string MemoNo { get; set;}
        public virtual string PaymentAllocationId { get; set; }
        public virtual DateTime? RequestDate { get; set; }
        public virtual string Status { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        //public virtual InvoiceStatusEnum InvStatusEnum
        //{
        //    get
        //    {
        //        return (InvoiceStatusEnum)Enum.Parse(typeof(InvoiceStatusEnum), this.Status);
        //    }
        //}

        public virtual RequestPaymentEnum RequestStatusEnum
        {
            get
            {
                return (RequestPaymentEnum)Enum.Parse(typeof(RequestPaymentEnum), this.Status);
            }
        }

        public virtual bool AllowEdit
        {
            get
            {
                switch (RequestStatusEnum)
                {
                    case RequestPaymentEnum.N:
                    case RequestPaymentEnum.R:
                        return true;
                    case RequestPaymentEnum.A:
                        return false;
                    default:
                        return false;
                }
            }
        }
        public virtual bool AllowApprove
        {
            get
            {
                switch (RequestStatusEnum)
                {
                    case RequestPaymentEnum.N:
                        return true;
                    case RequestPaymentEnum.A:
                    case RequestPaymentEnum.R:
                        return false;
                    default:
                        return false;
                }
            }
        }
    }
}
