﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class GroupAsset : EntityString
    {
        protected GroupAsset()
        { }
        public GroupAsset(string id) : base(id)
        {
            this.GroupAssetID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string GroupAssetID { get; set; }
        public virtual string GroupAssetDescription { get; set; }
        public virtual string GroupAssetShortDesc { get; set; }
        public virtual string CoaPerolehan { get; set; }
        public virtual string CoaAkumulasi { get; set; }
        public virtual string CoaBeban { get; set; }
        public virtual string PaymentAllocation { get; set; }
        public virtual bool Status { get; set; } 
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
