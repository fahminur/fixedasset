﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class Vendor : EntityString
    {
        protected Vendor()
        { }
        public Vendor(string id) : base(id)
        {
            this.VendorId = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string VendorId { get; set; }
        public virtual string VendorName { get; set; }
        public virtual string VendorGroupID { get; set; }
        public virtual string GroupCompanyID { get; set; }
        public virtual string NPWP { get; set; }
        public virtual string TDP { get; set; }
        public virtual string SIUP { get; set; }
        public virtual string VendorAddress { get; set; }
        public virtual string VendorRT { get; set; }
        public virtual string VendorRW { get; set; }
        public virtual string VendorKelurahan { get; set; }
        public virtual string VendorKecamatan { get; set; }
        public virtual string VendorCity { get; set; }
        public virtual string VendorZipCode { get; set; }
        public virtual string VendorAreaPhone1 { get; set; }
        public virtual string VendorPhone1 { get; set; }
        public virtual string VendorAreaPhone2 { get; set; }
        public virtual string VendorPhone2 { get; set; }
        public virtual string VendorAreaFax { get; set; }
        public virtual string VendorFax { get; set; }
        public virtual string ContactPersonName { get; set; }
        public virtual string ContactPersonJobTitle { get; set; }
        public virtual string ContactPersonEmail { get; set; }
        public virtual string ContactPersonHP { get; set; }
        public virtual string VendorStatus { get; set; }
        public virtual DateTime? VendorStartDate { get; set; }
        public virtual string VendorBadStatus { get; set; }
        public virtual string VendorLevelStatus { get; set; }
        public virtual DateTime? LastCalculationDate { get; set; }
        public virtual bool IsAutomotive { get; set; }
        public virtual bool IsUrusBPKB { get; set; }
        public virtual string VendorCategory { get; set; }
        public virtual string VendorAssetStatus { get; set; }
        public virtual string UsrUpd { get; set; }
        public virtual DateTime DtmUpd { get; set; }
        public virtual string VendorIncentiveIDNew { get; set; }
        public virtual string VendorIncentiveIDUsed { get; set; }
        public virtual string VendorIncentiveIDRO { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string VendorPointId { get; set; }
        public virtual bool PF { get; set; }
        public virtual string NoPKS { get; set; }
        public virtual bool PenerapanTVC { get; set; }
        public virtual bool isPerseorangan { get; set; }
        public virtual DateTime? PKSDate { get; set; }
        public virtual string AssetTypeID { get; set; }
        public virtual string VendorInitialName { get; set; }
        public virtual string AttachedFile { get; set; }
        public virtual bool SKBP { get; set; }
        public virtual string NPWPVendorAddress { get; set; }
        public virtual string NPWPVendorRT { get; set; }
        public virtual string NPWPVendorRW { get; set; }
        public virtual string NPWPVendorKelurahan { get; set; }
        public virtual string NPWPVendorKecamatan { get; set; }
        public virtual string NPWPVendorCity { get; set; }
        public virtual string NPWPVendorZipCode { get; set; }
        public virtual string NPWPVendorAreaPhone1 { get; set; }
        public virtual string NPWPVendorPhone1 { get; set; }
        public virtual string NPWPVendorAreaPhone2 { get; set; }
        public virtual string NPWPVendorPhone2 { get; set; }
        public virtual string NPWPVendorAreaFax { get; set; }
        public virtual string NPWPVendorFax { get; set; }

        //public virtual DateTime? CreateDate { get; set; }
        //public virtual string CreateUser { get; set; }
        //public virtual DateTime? UpdateDate { get; set; }
        //public virtual string UpdateUser { get; set; }
    }
}
