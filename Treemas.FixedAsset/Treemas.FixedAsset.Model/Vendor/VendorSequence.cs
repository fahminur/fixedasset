﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class VendorSequence : EntityString
    {
        protected VendorSequence()
        { }

        public VendorSequence(string Id) :base(Id)
        {

        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual int Year { get; set; }
        public virtual string SeqID { get; set; }
        public virtual string SeqName { get; set; }
        public virtual int Seq1 { get; set; }
        public virtual int Seq2 { get; set; }
        public virtual int Seq3 { get; set; }
        public virtual int Seq4 { get; set; }
        public virtual int Seq5 { get; set; }
        public virtual int Seq6 { get; set; }
        public virtual int Seq7 { get; set; }
        public virtual int Seq8 { get; set; }
        public virtual int Seq9 { get; set; }
        public virtual int Seq10 { get; set; }
        public virtual int Seq11 { get; set; }
        public virtual int Seq12 { get; set; }
        public virtual int Seq13 { get; set; }
        public virtual int LengthNo { get; set; }
        public virtual string ResetFlag { get; set; }
        public virtual string Prefix { get; set; }
        public virtual bool IsSite { get; set; }
        public virtual bool IsYear { get; set; }
        public virtual bool IsMonth { get; set; }
        public virtual string Suffix { get; set; }
        public virtual bool Status { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
