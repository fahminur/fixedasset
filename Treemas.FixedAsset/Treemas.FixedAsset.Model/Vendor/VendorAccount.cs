﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    public class VendorAccount : EntityString
    {
        protected VendorAccount()
        { }
        public VendorAccount(string id) : base(id)
        {
            this.Id = VendorAccID;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }


        public virtual string VendorID { get; set; }
        public virtual string VendorAccID { get; set; }
        public virtual string VendorBankID { get; set; }
        public virtual string VendorBankBranch { get; set; }
        public virtual string VendorAccountNo { get; set; }
        public virtual string VendorAccountName { get; set; }
        public virtual string UsrUpd { get; set; }
        public virtual DateTime? DtmUpd { get; set; }
        public virtual int VendorBankBranchID { get; set; }
        public virtual bool DefaultAccount { get; set; }
        public virtual string UntukBayar { get; set; }
        public virtual DateTime? TanggalEfektif { get; set; }
    }
}
