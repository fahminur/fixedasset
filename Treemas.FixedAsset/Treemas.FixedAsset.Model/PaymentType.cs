﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.FixedAsset.Model
{
    [Serializable]
    public class PaymentType : EntityString
    {
        public PaymentType() { }
        public PaymentType(string id) : base(id)
        {
            this.PaymentTypeID = id;
        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string PaymentTypeID { get; set; }
        public virtual string PaymentTypeName { get; set; }
        public virtual string PaymentTypeInitial { get; set; }
        public virtual string Status { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }
    }
}
