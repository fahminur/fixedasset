﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;


namespace Treemas.OLS.Repository
{
    public class AssetRepository : RepositoryControllerString<Asset>, IAssetRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AssetRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;

        }
        public PagedResult<Asset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Asset> paged = new PagedResult<Asset>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Asset>()
                                                     .OrderBy(GetOrderByExpression<Asset>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Asset>()
                                                     .OrderByDescending(GetOrderByExpression<Asset>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<Asset> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Asset> specification;
            switch (searchColumn)
            {
                case "AssetCode":
                    specification = new AdHocSpecification<Asset>(s => s.AssetCode.Equals(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<Asset>(s => s.AssetName.Contains(searchValue));
                    break;
            }

            PagedResult<Asset> paged = new PagedResult<Asset>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Asset>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Asset>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Asset>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Asset>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = transact(() =>
                          session.Query<Asset>()
                                 .Where(specification.ToExpression()).Count());

            return paged;
        }

        public Asset getAsset(string id)
        {
            return transact(() => session.QueryOver<Asset>()
                                                          .Where(f => f.AssetCode == id).SingleOrDefault());
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
