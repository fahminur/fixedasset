﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.OLS.Repository.Mapping
{
    public class InventoryMapping : ClassMapping<Inventory>
    {
        public InventoryMapping()
        {
            this.Table("Inventory");

            Id<string>(x => x.UnitID, map => { map.Column("UnitID"); });
            Property<string>(x => x.UnitID, map => { map.Column("UnitID"); });
            Property<string>(x => x.AssetCode, map => { map.Column("AssetCode"); });
            Property<string>(x => x.GRNCode, map => { map.Column("GRNCode"); });
            Property<DateTime?>(x => x.GRNDate, map => { map.Column("GRNDate"); });
            Property<string>(x => x.NoRangka, map => { map.Column("NoRangka"); });
            Property<string>(x => x.NoMesin, map => { map.Column("NoMesin"); });
            Property<string>(x => x.NoPolisi, map => { map.Column("NoPolisi"); });
            Property<string>(x => x.NewUsed, map => { map.Column("NewUsed"); });
            Property<string>(x => x.Color, map => { map.Column("Color"); });
            Property<Int32>(x => x.ManufacturingYear, map => { map.Column("ManufacturingYear"); });
            Property<double>(x => x.TotalOTR, map => { map.Column("TotalOTR"); });
            Property<string>(x => x.SupplierID, map => { map.Column("SupplierID"); });
            Property<string>(x => x.ReceiveAt, map => { map.Column("ReceiveAt"); });
            Property<string>(x => x.UnitStatus, map => { map.Column("UnitStatus"); });
            Property<Int64>(x => x.AgreementSeqNo, map => { map.Column("AgreementSeqNo"); });
            Property<string>(x => x.RentalType, map => { map.Column("RentalType"); });
            Property<string>(x => x.InProcessStatus, map => { map.Column("InProcessStatus"); });
            Property<string>(x => x.AlocationUnitStatus, map => { map.Column("AlocationUnitStatus"); });
            Property<string>(x => x.OperationalStatus, map => { map.Column("OperationalStatus"); });
            Property<string>(x => x.UnitCurrentStatus, map => { map.Column("UnitCurrentStatus"); });
            Property<string>(x => x.PreDeliveryInspection, map => { map.Column("PreDeliveryInspection"); });
            Property<string>(x => x.UserCreate, map => { map.Column("UserCreate"); });
            Property<DateTime?>(x => x.DateCreate, map => { map.Column("DateCreate"); });
            Property<string>(x => x.UserLastUpdate, map => { map.Column("UserLastUpdate"); });
            Property<DateTime?>(x => x.DateLastUpdate, map => { map.Column("DateLastUpdate"); });
        }
    }
}
