﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.FixedAsset.Model;

namespace Treemas.OLS.Repository.Mapping
{
    public class AssetMapping : ClassMapping<Asset>
    {
        public AssetMapping()
        {
            this.Table("Asset");

            Id<string>(x => x.AssetCode, map => { map.Column("AssetCode"); });

            Property<string>(x => x.AssetCode, map => { map.Column("AssetCode"); });
            //Property<string>(x => x.AssetParent, map => { map.Column("AssetParent"); });
            Property<string>(x => x.AssetName, map => { map.Column("AssetName"); });
            Property<string>(x => x.AssetTypeID, map => { map.Column("AssetTypeID"); });
            //Property<string>(x => x.AssetCategoryID, map => { map.Column("AssetCategoryID"); });
            //Property<int>(x => x.AssetLevel, map => { map.Column("AssetLevel"); });
            //Property<bool>(x => x.IsFinalLevel, map => { map.Column("IsFinalLevel"); });
            //Property<double>(x => x.ResaleValue, map => { map.Column("ResaleValue"); });
            //Property<int>(x => x.CylinderCapacity, map => { map.Column("CylinderCapacity"); });
            //Property<int>(x => x.SeatCapacity, map => { map.Column("SeatCapacity"); });
            //Property<string>(x => x.TransmissionType, map => { map.Column("TransmissionType"); });
            //Property<int>(x => x.MaxTransmission, map => { map.Column("MaxTransmission"); });
            //Property<string>(x => x.FuelType, map => { map.Column("FuelType"); });
            //Property<double>(x => x.AverageConsumption, map => { map.Column("AverageConsumption"); });
            //Property<string>(x => x.MfgType, map => { map.Column("MfgType"); });
            //Property<string>(x => x.MfgCountry, map => { map.Column("MfgCountry"); });
            Property<bool>(x => x.IsActive, map => { map.Column("IsActive"); });
            //Property<string>(x => x.UserCreate, map => { map.Column("UserCreate"); });
            //Property<DateTime>(x => x.DateCreate, map => { map.Column("DateCreate"); });
            //Property<string>(x => x.UserlastUpdate, map => { map.Column("UserlastUpdate"); });
            //Property<DateTime>(x => x.DateLastUpdate, map => { map.Column("DateLastUpdate"); });
        }
    }
}
