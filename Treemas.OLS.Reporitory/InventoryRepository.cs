﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using Treemas.FixedAsset.Interface;
using Treemas.FixedAsset.Model;
using Treemas.OLS.Repository;

namespace Treemas.OLS.Repository
{
    public class InventoryRepository : RepositoryControllerString<Inventory>, IInventoryRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;

        public InventoryRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;

        }
        public Inventory getInventory(string id)
        {
            return transact(() => session.QueryOver<Inventory>()
                                                .Where(f => f.UnitID == id).SingleOrDefault());
        }

        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<Inventory>()
                                                 .Where(f => f.UnitID == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
