﻿using System;
using Treemas.Credential.Model;

namespace Treemas.SingleSignon
{
    public interface ISingleSignonPolicy
    {
        SignonPolicyState Evaluate(SignonLoginInfo info, DateTime time);
    }
}
