﻿using System;
using System.IO;
using System.Web.Hosting;
using System.ServiceModel.Activation;
using Treemas.Credential;
using Treemas.Credential.Interface;
using Treemas.Credential.Repository;
using Treemas.Base.Configuration;
using Treemas.Base.Configuration.Binder;
using Treemas.Base.Utilities;
using SimpleInjector;
using SimpleInjector.Integration.Wcf;

namespace Treemas.SingleSignon
{
    public class SingleSignon
    {
        private static Treemas.SingleSignon.SingleSignon instance;
        private Container container;

        private SingleSignon()
        {
            this._InitConfiguration();
            this._BindInterface();
        }

        private void _BindInterface()
        {
            this.container = new Container();
            container.Options.DefaultScopedLifestyle = new WcfOperationLifestyle();

            // Register your types, for instance:
            container.Register<IUserRepository, UserRepository>(Lifestyle.Scoped);
            container.Register<IUserApplicationRepository, UserApplicationRepository>(Lifestyle.Scoped);
            container.Register<IRoleRepository, RoleRepository>(Lifestyle.Scoped);
            container.Register<IFunctionRepository, FunctionRepository>(Lifestyle.Scoped);
            container.Register<IFeatureRepository, FeatureRepository>(Lifestyle.Scoped);
            container.Register<IAuthorizationRepository, AuthorizationRepository>(Lifestyle.Scoped);
            container.Register<IApplicationRepository, ApplicationRepository>(Lifestyle.Scoped);
            container.Register<ILoginInfoRepository, LoginInfoRepository>(Lifestyle.Scoped);
            container.Register<IUserAccount, UserAccount>(Lifestyle.Scoped);
            container.Register<IMenuRepository, MenuRepository>(Lifestyle.Scoped);
            container.Verify();
            SimpleInjectorServiceHostFactory.SetContainer(container);
        }

        public IUserAccount userAccount { get { return container.GetInstance<IUserAccount>(); } private set { } }
        public ILoginInfoRepository loginInfoRepository { get { return container.GetInstance<ILoginInfoRepository>(); } private set { } }

        public static Treemas.SingleSignon.SingleSignon Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Treemas.SingleSignon.SingleSignon();
                }
                return instance;
            }
        }

        private void _InitConfiguration()
        {
            string path = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "Config");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            ConfigurationBinder binder = new XmlFileConfigurationBinder("SingleSignon", path);
            binder.Load();

            CompositeConfigurationItem configurations = (CompositeConfigurationItem)binder.GetConfiguration("DBSingleSignon");
            ConfigurationItem configuration;

            if (configurations.IsNull())
            {
                configurations = new CompositeConfigurationItem("DBSingleSignon");
                configuration = new ConfigurationItem
                {
                    Key = "DBInstance",
                    Value = "DB-Instance-SSO-Service"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBUser",
                    Value = "DB-User-SSO-Service-Encrypted"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBPassword",
                    Value = "DB-Password-SSO-Service-Encrypted"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBName",
                    Value = "DB-Name-SSO-Service-Encrypted"
                };
                configurations.AddItem(configuration);
                binder.AddConfiguration(configurations);
            }

            configurations = (CompositeConfigurationItem)binder.GetConfiguration("DBUserHost");
            if (configurations.IsNull())
            {
                configurations = new CompositeConfigurationItem("DBUserHost");
                configuration = new ConfigurationItem
                {
                    Key = "DBInstance",
                    Value = "DB-Instance-User-Hosted"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBUser",
                    Value = "DB-User-User-Hosted-Encrypted"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBPassword",
                    Value = "DB-Password-User-Hosted-Encrypted"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBName",
                    Value = "DB-Name-User-Hosted-Encrypted"
                };
                configurations.AddItem(configuration);
                binder.AddConfiguration(configurations);
            }

            configuration = binder.GetConfiguration("WalkerWorkingPeriod");
            if (configuration.IsNull())
            {
                configuration = new ConfigurationItem
                {
                    Key = "WalkerWorkingPeriod",
                    Value = "60000",
                    Description = "Walker's working period"
                };
                binder.AddConfiguration(configuration);
            }
            Configurations.Instance.WalkerWorkingPeriod = Convert.ToInt32(configuration.Value);

            configuration = binder.GetConfiguration("EncryptionKey");
            if (configuration.IsNull())
            {
                configuration = new ConfigurationItem
                {
                    Key = "EncryptionKey",
                    Value = "Encryption_KEY_VALUE",
                    Description = "Encryption Key Value"
                };
                binder.AddConfiguration(configuration);
            }
            Configurations.Instance.EncryptionKey = configuration.Value;
            Encryption.Instance.SetHashKey(Configurations.Instance.EncryptionKey);

            binder.Save();
        }
    }
}
