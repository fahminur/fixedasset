﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Treemas.Base.Utilities;
using Treemas.Credential.Model;
using Treemas.Credential.Repository;
using Treemas.Credential.Interface;

namespace Treemas.SingleSignon
{
    internal class Walker : IDisposable
    {
        private static Walker instance = null;
        private ILoginInfoRepository _loginInfoRepository;

        private Walker()
        {
            this.Policy = new DefaultPolicy();
            this._loginInfoRepository = new LoginInfoRepository();
            this.Start();
        }

        public static Walker Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Walker();
                }
                return instance;
            }
        }

        public ISingleSignonPolicy Policy { get; set; }

        public void Dispose()
        {
            if (this._Timer != null)
            {
                this._Timer.Dispose();
            }
        }

        private void Start()
        {
            if (!this.Policy.IsNull())
            {
                this._Timer = new Timer((double)Configurations.Instance.WalkerWorkingPeriod);
                this._Timer.Elapsed += new ElapsedEventHandler(this.CheckLogins);
                this._Timer.Start();
            }
        }

        public void Stop()
        {
            if (this._Timer != null)
            {
                this._Timer.Stop();
            }
        }

        private Timer _Timer { get; set; }

        private void CheckLogins(object source, ElapsedEventArgs args)
        {
            try
            {
                DateTime now = DateTime.Now;
                IList<SignonLoginInfo> list = _loginInfoRepository.FindAll().ToList();
                
                if (!list.IsNullOrEmpty<SignonLoginInfo>())
                {
                    foreach (SignonLoginInfo info in list)
                    {
                        info.PolicyState = this.Policy.Evaluate(info, now);

                        if (info.PolicyState == SignonPolicyState.SessionExpired)
                        {
                            info.LoginTime = now;
                        }
                        if (info.PolicyState == SignonPolicyState.LockActive)
                        {
                            info.LockTime = now;
                        }
                    }
                    _loginInfoRepository.EvaluateLoginInfo(list);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    }
}
