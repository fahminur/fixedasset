﻿using System;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Web.Service;

namespace Treemas.SingleSignon
{
    internal class CommandIsLoggedIn : ServiceCommand 
    {
        private ILoginInfoRepository _loginInfoRepository;
        public CommandIsLoggedIn(ILoginInfoRepository loginInfoRepository) : base("IsLoggedIn")
        {
            this._loginInfoRepository = loginInfoRepository;
        }

        public override ServiceResult Execute(ServiceParameter parameter)
        {
            ServiceResult result = null;
            if ((((!parameter.IsNull() && parameter.Parameters.HasKey("username")) && (parameter.Parameters.HasKey("hostname") && parameter.Parameters.HasKey("host_ip"))) && (parameter.Parameters.HasKey("browser") && parameter.Parameters.HasKey("browser_version"))) && parameter.Parameters.HasKey("is_mobile"))
            {
                string username = parameter.Parameters.Get<string>("username");
                string str3 = parameter.Parameters.Get<string>("hostname");
                string str4 = parameter.Parameters.Get<string>("host_ip");
                string str5 = parameter.Parameters.Get<string>("browser");
                string str6 = parameter.Parameters.Get<string>("browser_version");
                bool flag = parameter.Parameters.Get<bool>("is_mobile");
                
                try
                {
                    SignonLoginInfo info = new SignonLoginInfo(0L);
                    info.Username = username;
                    info.Hostname = str3;
                    info.HostIP = str4;
                    info.Browser = str5;
                    info.BrowserVersion = str6;
                    info.IsMobile = flag;

                    SignonLoginInfo loginInfo = _loginInfoRepository.GetLoginInfoByData(info);
                    result = new ServiceResult();
                    if (!loginInfo.IsNull())
                    {
                        result.Data.Add<long>("id", loginInfo.Id);
                        result.Status = ServiceStatus.Confirmed;
                    }
                
                }
                catch (Exception ex)
                {
                    result.Status = ServiceStatus.Error;
                    result.Message = ex.Message;
                }
            }
            return result;
        }
    }
}
