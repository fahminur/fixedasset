﻿namespace Treemas.SingleSignon
{
    public class Configurations
    {
        private static Configurations instance = null;
        private Configurations()
        {
        }

        public string DevelopmentPhase { get; set; }

        public static Configurations Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Configurations();
                }
                return instance;
            }
        }

        public int WalkerWorkingPeriod { get; set; }
        public string EncryptionKey { get; set; }
    }
}
