﻿using System;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Web.Service;

namespace Treemas.SingleSignon
{
    internal class CommandLogin : ServiceCommand
    {
        private IUserAccount _userAccount;
        private ILoginInfoRepository _loginInfoRepository;
        public CommandLogin(IUserAccount userAccount, ILoginInfoRepository loginInfoRepository) : base("Login")
        {
            this._userAccount = userAccount;
            this._loginInfoRepository = loginInfoRepository;
        }

        public override ServiceResult Execute(ServiceParameter parameter)
        {
            ServiceResult result = null;
            if ((((!parameter.IsNull() && parameter.Parameters.HasKey("username")) && (parameter.Parameters.HasKey("password") && parameter.Parameters.HasKey("hostname"))) && ((parameter.Parameters.HasKey("host_ip") && parameter.Parameters.HasKey("browser")) && parameter.Parameters.HasKey("browser_version"))) && parameter.Parameters.HasKey("is_mobile"))
            {
                string username = parameter.Parameters.Get<string>("username");
                string password = parameter.Parameters.Get<string>("password");
                string str3 = parameter.Parameters.Get<string>("hostname");
                string str4 = parameter.Parameters.Get<string>("host_ip");
                string str5 = parameter.Parameters.Get<string>("browser");
                string str6 = parameter.Parameters.Get<string>("browser_version");
                bool flag = parameter.Parameters.Get<bool>("is_mobile");
                string str7 = parameter.Parameters.Get<string>("sessionid");
                
                try
                {
                    User user = _userAccount.IsUserAuthenticSSO(username, password);
                    result = new ServiceResult();
                    if (!user.IsNull())
                    {
                        DateTime now = DateTime.Now;
                        SignonLoginInfo info = new SignonLoginInfo(0L);
                        info.Username = username;
                        info.LoginTime = now;
                        info.LastActive = now;
                        info.SessionTimeout = user.SessionTimeout;
                        info.LockTimeout = user.LockTimeout;
                        info.MaximumLogin = user.MaximumConcurrentLogin;
                        info.Hostname = str3;
                        info.HostIP = str4;
                        info.Browser = str5;
                        info.BrowserVersion = str6;
                        info.IsMobile = flag;
                        info.SessionId = str7;

                        _loginInfoRepository.Add(info);

                        result.Data.Add<long>("id", info.Id);
                        result.Status = ServiceStatus.Success;
                    }
                
                }
                catch (Exception ex)
                {
                    result.Status = ServiceStatus.Error;
                    result.Message = ex.Message;
                }
            }
            return result;
        }
    }
}
