﻿using System;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Web.Service;

namespace Treemas.SingleSignon
{
    internal class CommandGetLoginCount : ServiceCommand 
    {
        private ILoginInfoRepository _loginInfoRepository;
        public CommandGetLoginCount(ILoginInfoRepository loginInfoRepository) : base("GetLoginCount")
        {
            this._loginInfoRepository = loginInfoRepository;
        }

        public override ServiceResult Execute(ServiceParameter parameter)
        {
            ServiceResult result = null;
            if (!parameter.IsNull() && parameter.Parameters.HasKey("username"))
            {
                string username = parameter.Parameters.Get<string>("username");
                DateTime now = DateTime.Now;
                result = new ServiceResult();
                try
                {
                    IList<SignonLoginInfo> loginInfos = _loginInfoRepository.GetLoginInfos(username);

                    result.Data.Add<int>("count", loginInfos.Count);
                    result.Status = ServiceStatus.Success;
                }
                catch (Exception ex)
                {
                    result.Status = ServiceStatus.Error;
                    result.Message = ex.Message;
                }
            }
            return result;
        }
    }
}
