﻿using System;
using System.IO;
using System.Web.Hosting;
using System.ServiceModel.Activation;
using Treemas.Base.Configuration;
using Treemas.Base.Configuration.Binder;
using Treemas.Credential;
using Treemas.Credential.Interface;
using Treemas.Credential.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Web.Service;


namespace Treemas.SingleSignon
{
    public class SingleSignonService : WebService
    {
        private ILoginInfoRepository _loginInfoRepository;
        private IUserAccount _userAccount;
        private IUserRepository _userRepository;
        private IUserApplicationRepository _userApplicationRepository;
        private IRoleRepository _roleRepository;
        private IFunctionRepository _functionRepository;
        private IFeatureRepository _featureRepository;
        private IAuthorizationRepository _authorizationRepository;
        private IApplicationRepository _applicationRepository;
        private IMenuRepository _menuRepository;
        private IUserSiteRepository _userSiteRepository;

        public SingleSignonService()
        {
            Walker instance_walker = Walker.Instance;

            _userRepository = new UserRepository();
            _userApplicationRepository = new UserApplicationRepository();
            _roleRepository = new RoleRepository();
            _functionRepository = new FunctionRepository();
            _featureRepository = new FeatureRepository();
            _authorizationRepository = new AuthorizationRepository();
            _applicationRepository = new ApplicationRepository();
            _loginInfoRepository = new LoginInfoRepository();
            _menuRepository = new MenuRepository();
            _userSiteRepository = new UserSiteRepository(); 

            _userAccount = new UserAccount(_userRepository, _applicationRepository,
                _userApplicationRepository, _functionRepository, _featureRepository, 
                _roleRepository, _authorizationRepository, _menuRepository, _loginInfoRepository, _userSiteRepository);

            base.Commands.AddCommand(new CommandLogin(_userAccount, _loginInfoRepository));
            base.Commands.AddCommand(new CommandLogout(_loginInfoRepository));
            base.Commands.AddCommand(new CommandUnlock(_loginInfoRepository));
            base.Commands.AddCommand(new CommandLock(_loginInfoRepository));
            base.Commands.AddCommand(new CommandIsLocked(_loginInfoRepository));
            base.Commands.AddCommand(new CommandIsAlive(_loginInfoRepository));
            base.Commands.AddCommand(new CommandGetLoggedInUser(_loginInfoRepository));
            base.Commands.AddCommand(new CommandMarkActive(_loginInfoRepository));
            base.Commands.AddCommand(new CommandIsLoggedIn(_loginInfoRepository));
            base.Commands.AddCommand(new CommandGetLoginCount(_loginInfoRepository));
            base.Commands.AddCommand(new CommandGetSession(_loginInfoRepository));
            //base.Commands.AddCommand(new CommandGetSession(_userSiteRepository));

        }


    }
}
