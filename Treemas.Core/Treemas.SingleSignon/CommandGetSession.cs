﻿using System;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Web.Service;

namespace Treemas.SingleSignon
{
    internal class CommandGetSession : ServiceCommand 
    {
        private ILoginInfoRepository _loginInfoRepository;
        public CommandGetSession(ILoginInfoRepository loginInfoRepository) : base("GetSession")
        {
            this._loginInfoRepository = loginInfoRepository;
        }
        
        public override ServiceResult Execute(ServiceParameter parameter)
        {
            ServiceResult result = null;
            if (!parameter.IsNull() && parameter.Parameters.HasKey("username"))
            {
                string username = parameter.Parameters.Get<string>("username");
                DateTime now = DateTime.Now;
                result = new ServiceResult();
                try
                {
                    IList<SignonLoginInfo> loginInfos = _loginInfoRepository.GetLoginInfos(username);

                    result.Data.Add<string>("session", JSON.ToString<IList<SignonLoginInfo>>(loginInfos));
                    result.Status = ServiceStatus.Success;
                }
                catch (Exception ex)
                {
                    result.Status = ServiceStatus.Error;
                    result.Message = ex.Message;
                }
            }
            return result;
        }
    }
}
