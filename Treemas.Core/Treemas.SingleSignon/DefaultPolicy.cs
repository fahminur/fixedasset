﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treemas.Base.Utilities;
using Treemas.Credential.Model;


namespace Treemas.SingleSignon
{
    internal class DefaultPolicy : ISingleSignonPolicy
    {
        public SignonPolicyState Evaluate(SignonLoginInfo info, DateTime time)
        {
            double minutes = -1;
            if (!(info.LastActive.IsNull() || (info.LastActive <= DateTime.MinValue)))
            {
                minutes = time.Subtract(info.LastActive).TotalMinutes;
            }
            else
            {
                minutes = time.Subtract(info.LoginTime).TotalMinutes;
            }
            if (minutes >= info.SessionTimeout)
            {
                return SignonPolicyState.SessionExpired;
            }
            if (!(info.LastActive.IsNull() || (info.LastActive <= DateTime.MinValue)))
            {
                minutes = time.Subtract(info.LastActive).TotalMinutes;
            }
            else
            {
                minutes = time.Subtract(info.UnlockTime.Value).TotalMinutes;
            }
            if (minutes >= info.LockTimeout)
            {
                return SignonPolicyState.LockActive;
            }
            return SignonPolicyState.Hold;
        }
    }
}
