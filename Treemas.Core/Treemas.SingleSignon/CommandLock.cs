﻿using System;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Web.Service;

namespace Treemas.SingleSignon
{
    internal class CommandLock : ServiceCommand 
    {
        private ILoginInfoRepository _loginInfoRepository;
        public CommandLock(ILoginInfoRepository loginInfoRepository) : base("Lock")
        {
            this._loginInfoRepository = loginInfoRepository;
        }

        public override ServiceResult Execute(ServiceParameter parameter)
        {
            ServiceResult result = null;
            if (!parameter.IsNull() && parameter.Parameters.HasKey("id"))
            {
                long id = parameter.Parameters.Get<long>("id");
                DateTime now = DateTime.Now;
                result = new ServiceResult();
                try
                {
                    SignonLoginInfo loginInfo = _loginInfoRepository.GetLoginInfo(id);
                    
                    if (!loginInfo.IsNull())
                    {
                        loginInfo.UnlockTime = DateTime.MinValue;
                        loginInfo.LockTime = now;
                        loginInfo.Locked = true;
                        _loginInfoRepository.Save(loginInfo);
                        result.Status = ServiceStatus.Success;
                    }
                }
                catch (Exception ex)
                {
                    result.Status = ServiceStatus.Error;
                    result.Message = ex.Message;
                }
            }
            return result;
        }
    }
}
