﻿using System;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Web.Service;

namespace Treemas.SingleSignon
{
    internal class CommandIsLocked : ServiceCommand 
    {
        private ILoginInfoRepository _loginInfoRepository;
        public CommandIsLocked(ILoginInfoRepository loginInfoRepository) : base("IsLocked")
        {
            this._loginInfoRepository = loginInfoRepository;
        }

        public override ServiceResult Execute(ServiceParameter parameter)
        {
            ServiceResult result = null;
            if (!parameter.IsNull() && parameter.Parameters.HasKey("id"))
            {
                long id = parameter.Parameters.Get<long>("id");
                DateTime now = DateTime.Now;
                result = new ServiceResult();
                try
                {
                    SignonLoginInfo loginInfo = _loginInfoRepository.GetLoginInfo(id);
                    
                    if (!loginInfo.IsNull())
                    {
                        result.Data.Add<bool>("is_locked", loginInfo.Locked);
                        result.Status = ServiceStatus.Success;
                    }
                }
                catch (Exception ex)
                {
                    result.Status = ServiceStatus.Error;
                    result.Message = ex.Message;
                }
            }
            return result;
        }
    }
}
