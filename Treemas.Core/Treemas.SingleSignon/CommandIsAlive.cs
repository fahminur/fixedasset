﻿using System;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Web.Service;

namespace Treemas.SingleSignon
{
    internal class CommandIsAlive : ServiceCommand
    { 
        private ILoginInfoRepository _loginInfoRepository;
        public CommandIsAlive(ILoginInfoRepository loginInfoRepository) : base("IsAlive")
        {
            this._loginInfoRepository = loginInfoRepository;
        }

        public override ServiceResult Execute(ServiceParameter parameter)
        {
            ServiceResult result = null;
            if (!parameter.IsNull() && parameter.Parameters.HasKey("id"))
            {
                long id = parameter.Parameters.Get<long>("id");
                DateTime now = DateTime.Now;
                result = new ServiceResult();
                try
                {
                    SignonLoginInfo loginInfo = _loginInfoRepository.GetLoginInfo(id);
                    result.Data.Add<bool>("is_alive", !loginInfo.IsNull());
                    result.Status = ServiceStatus.Confirmed;
                }
                catch (Exception ex)
                {
                    result.Status = ServiceStatus.Error;
                    result.Message = ex.Message;
                }
            }
            return result;
        }
    }
}
