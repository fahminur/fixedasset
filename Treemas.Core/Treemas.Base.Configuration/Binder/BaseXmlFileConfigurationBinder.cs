﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Treemas.Base.Configuration;

namespace Treemas.Base.Configuration.Binder
{
    public class BaseXmlFileConfigurationBinder : ConfigurationBinder
    {
        public BaseXmlFileConfigurationBinder(string label) : base(label)
        {
        }

        public override void Load()
        {
        }

        protected void Load(Stream stream)
        {
            if (stream != null)
            {
                XmlDocument document = new XmlDocument();
                document.Load(stream);
                try
                {
                    XmlNodeList elementsByTagName = document.GetElementsByTagName("configuration");
                    if ((elementsByTagName != null) && (elementsByTagName.Count > 0))
                    {
                        foreach (XmlNode node3 in elementsByTagName.Item(0).ChildNodes)
                        {
                            if ((node3.NodeType != XmlNodeType.Comment) && node3.Name.Equals("configuration-item"))
                            {
                                IList<ConfigurationItem> source = null;
                                int num = 0;
                                ConfigurationItem configuration = new ConfigurationItem();
                                XmlNodeList childNodes = node3.ChildNodes;
                                for (int i = childNodes.Count - 1; i >= 0; i--)
                                {
                                    XmlNode node = childNodes[i];
                                    if (node.NodeType != XmlNodeType.Comment)
                                    {
                                        if (node.Name.Equals("key"))
                                        {
                                            configuration.Key = node.InnerText.Trim();
                                            continue;
                                        }
                                        if (node.Name.Equals("value"))
                                        {
                                            XmlNodeList list4 = node.ChildNodes;
                                            foreach (XmlNode node4 in list4)
                                            {
                                                if (node4 is XmlElement)
                                                {
                                                    num++;
                                                }
                                                if (num > 0)
                                                {
                                                    break;
                                                }
                                            }
                                            if (num > 0)
                                            {
                                                source = new List<ConfigurationItem>();
                                                foreach (XmlNode node5 in list4)
                                                {
                                                    ConfigurationItem item = new ConfigurationItem {
                                                        Key = node5.Name.Trim(),
                                                        Value = node5.InnerText.Trim()
                                                    };
                                                    source.Add(item);
                                                }
                                            }
                                            else
                                            {
                                                configuration.Value = node.InnerText.Trim();
                                            }
                                            continue;
                                        }
                                        if (node.Name.Equals("description"))
                                        {
                                            configuration.Description = node.InnerText.Trim();
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(configuration.Key))
                                {
                                    if (source != null)
                                    {
                                        base.AddConfiguration(new CompositeConfigurationItem(configuration.Key, configuration.Description, source.ToArray<ConfigurationItem>()));
                                    }
                                    else
                                    {
                                        base.AddConfiguration(configuration);
                                    }
                                }
                            }
                        }
                    }
                }
                finally
                {
                    stream.Close();
                }
            }
        }

        public override void Save()
        {
        }

        protected void Save(Stream stream)
        {
            if (stream != null)
            {
                XmlWriterSettings settings = new XmlWriterSettings {
                    Indent = true,
                    IndentChars = "\t"
                };
                XmlWriter writer = XmlWriter.Create(stream, settings);
                try
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("configuration");
                    ConfigurationItem[] configurations = base.GetConfigurations();
                    if (configurations != null)
                    {
                        IList<ConfigurationItem> list = new List<ConfigurationItem>();
                        foreach (ConfigurationItem item2 in configurations)
                        {
                            if (item2.Transient)
                            {
                                list.Add(item2);
                            }
                            else
                            {
                                bool flag = item2 is CompositeConfigurationItem;
                                writer.WriteStartElement("configuration-item");
                                writer.WriteElementString("key", item2.Key);
                                if (flag)
                                {
                                    writer.WriteStartElement("value");
                                    ConfigurationItem[] items = ((CompositeConfigurationItem) item2).GetItems();
                                    if (items != null)
                                    {
                                        foreach (ConfigurationItem item3 in items)
                                        {
                                            writer.WriteElementString(item3.Key, item3.Value);
                                        }
                                    }
                                    writer.WriteEndElement();
                                }
                                else
                                {
                                    writer.WriteElementString("value", item2.Value);
                                }
                                writer.WriteElementString("description", item2.Description);
                                writer.WriteEndElement();
                            }
                        }
                        foreach (ConfigurationItem item4 in list)
                        {
                            base.RemoveConfiguration(item4.Key);
                        }
                    }
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
                finally
                {
                    writer.Close();
                    stream.Close();
                }
            }
        }
    }
}

