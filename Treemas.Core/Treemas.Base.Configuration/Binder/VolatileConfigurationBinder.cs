﻿using Treemas.Base.Configuration;

namespace Treemas.Base.Configuration.Binder
{
    public class VolatileConfigurationBinder : ConfigurationBinder
    {
        public VolatileConfigurationBinder(string label) : base(label)
        {
        }

        public override void Load()
        {
        }

        public override void Save()
        {
        }
    }
}

