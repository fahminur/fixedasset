﻿namespace Treemas.Base.Configuration
{
    public interface IConfigurationCabinet
    {
        void AddBinder(IConfigurationBinder binder);
        IConfigurationBinder GetBinder(string label);
        IConfigurationBinder[] GetBinderByConfigurationKey(string key);
        IConfigurationBinder[] GetBinders();
        string GetLabel();
    }
}

