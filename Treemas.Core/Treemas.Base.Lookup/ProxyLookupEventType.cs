﻿using System;

namespace Treemas.Base.Lookup
{
    public enum ProxyLookupEventType
    {
        Lookup_Added,
        Lookup_Removed
    }
}

