﻿using System;

namespace Treemas.Base.Lookup
{
    public enum LookupEventType
    {
        Instance_Added,
        Instance_Removed
    }
}

