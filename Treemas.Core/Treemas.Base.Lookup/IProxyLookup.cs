﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Lookup
{
    public interface IProxyLookup : IProxyLookupEventBroadcaster
    {
        void AddLookup(ILookup lookup);
        T Get<T>();
        IList<T> GetAll<T>();
        ILookup GetLookup(string name);
        string GetName();
        void Remove<T>();
        void Remove(object obj);
        void Remove<T>(Predicate<T> matchedCondition);
        void RemoveLookup(string name);
        void RemoveLookup(ILookup lookup);
    }
}

