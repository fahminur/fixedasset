﻿namespace Treemas.Base.Lookup
{
    public interface ILookupProvider
    {
        ILookup GetLookup();
    }
}

