﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Lookup
{
    public interface ILookup : ILookupEventBroadcaster
    {
        void Add(object obj);
        void Clear();
        T Get<T>();
        IList<object> GetAll();
        IList<T> GetAll<T>();
        string GetName();
        void Remove<T>();
        void Remove(object obj);
        void Remove<T>(Predicate<T> matchedCondition);
    }
}

