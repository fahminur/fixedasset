﻿using System;
using System.ServiceModel.Channels;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class StreamedServiceClient : IDisposable
    {
        public StreamedServiceClient(IStreamedWebService client)
        {
            this.Client = client;
        }

        public void Dispose()
        {
            if (!this.Client.IsNull())
            {
                ((IChannel) this.Client).Close();
            }
        }

        public virtual StreamedServiceResult Execute(StreamedServiceParameter parameter)
        {
            if (!this.Client.IsNull() && !parameter.IsNull())
            {
                StreamedServiceRuntimeResult result = this.Client.Execute(parameter.ToRuntime());
                if (!result.IsNull())
                {
                    return StreamedServiceResult.Create(result);
                }
            }
            return null;
        }

        public IStreamedWebService Client { get; set; }
    }
}

