﻿using System;

namespace Treemas.Base.Web.Service
{
    public class ActionServiceCommand : ServiceCommand
    {
        public ActionServiceCommand(string name, Func<ServiceParameter, ServiceResult> _Action) : base(name)
        {
            this._Action = _Action;
        }

        public override ServiceResult Execute(ServiceParameter parameter) => 
            this._Action(parameter);

        private Func<ServiceParameter, ServiceResult> _Action { get; set; }
    }
}

