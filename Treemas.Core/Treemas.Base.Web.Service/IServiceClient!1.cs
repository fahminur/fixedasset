﻿using System;

namespace Treemas.Base.Web.Service
{

    public interface IServiceClient<T>
    {
        void Close();

        T Client { get; }
    }
}

