﻿using System;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class BaseServiceParameter
    {
        public BaseServiceParameter() : this(null, delegate (JsonDataMap data) {
        })
        {
        }

        public BaseServiceParameter(string command) : this(command, delegate (JsonDataMap param) {
        })
        {
        }

        public BaseServiceParameter(string command, Action<JsonDataMap> paramAction)
        {
            this.Parameters = new JsonDataMap();
            if (paramAction != null)
            {
                paramAction(this.Parameters);
            }
            if (!string.IsNullOrEmpty(command))
            {
                this.Command = command;
            }
        }

        public virtual string Command { get; set; }

        public JsonDataMap Parameters { get; private set; }
    }
}

