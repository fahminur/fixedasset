﻿using System;
using System.Runtime.Serialization;

namespace Treemas.Base.Web.Service
{
    [DataContract(Namespace="Treemas.Base.Web.Service")]
    public class ServiceRuntimeResult
    {
        [DataMember]
        public string DataString { get; set; }
    }
}

