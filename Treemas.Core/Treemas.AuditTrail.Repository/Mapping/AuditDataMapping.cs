﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.AuditTrail.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class AuditTrailMapping : ClassMapping<AuditData>
    {
        public AuditTrailMapping()
        {
            this.Table("SEC_AUDIT_TRAIL");
            Id<long>(
                x => x.Id,
                map =>
                {
                    map.Column("ID");
                    //map.Generator(Generators.Identity) ;
                });

            Property<string>(x => x.FunctionName,
                map =>
                {
                    map.Column("FUNCTION_NAME");
                });

            Property<string>(x => x.ObjectName,
                map =>
                {
                    map.Column("OBJECT_NAME");
                });

            Property<string>(x => x.ObjectValue,
                map =>
                {
                    map.Column("OBJECT_VALUE");
                    map.Length(64000);
                });

            Property<string>(x => x.Action,
                map =>
                {
                    map.Column("ACTION");
                });

            Property<string>(x => x.ActionBy,
                map =>
                {
                    map.Column("ACTION_BY");
                });

            Property<DateTime>(x => x.ActionDateTime,
                map =>
                {
                    map.Column("ACTION_DATETIME");
                });

          

        }
    }
}
