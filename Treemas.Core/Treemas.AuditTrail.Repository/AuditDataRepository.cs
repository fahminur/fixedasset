﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.AuditTrail.Model;
using Treemas.AuditTrail.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.AuditTrail.Repository
{
    public class AuditDataRepository : RepositoryController<AuditData>, IAuditDataRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AuditDataRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
     
    }
}
