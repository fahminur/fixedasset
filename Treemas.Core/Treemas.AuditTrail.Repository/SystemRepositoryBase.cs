﻿using System.Data;
using System.Reflection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Connection;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Mapping.ByCode;
using Treemas.Base.Utilities;
using Treemas.Base.Repository.UnitOfWork;

namespace Treemas.AuditTrail.Repository
{
    public class SystemRepositoryBase
    {
        private volatile static SystemRepositoryBase uniqueInstance;
        private static object syncLock = new object();
        public static NHibernate.Cfg.Configuration configuration { get; set; }
        public static ISessionFactory sessionFactory { get; private set; }
        private static ISession _currentSession = null;
        private static IStatelessSession _statelessSession = null;
        private static HbmMapping _mapping;
        private static IUnitOfWorkFactory _unitOfWork = null;
        private static IConnection _connection = null;

        private SystemRepositoryBase()
        {
            var conn = Connection.GetConnectionSetting();
            configuration = new NHibernate.Cfg.Configuration();
            configuration.DataBaseIntegration(dbi =>
            {
                dbi.Dialect<MsSql2012Dialect>();
                dbi.Driver<SqlClientDriver>();
                dbi.ConnectionProvider<DriverConnectionProvider>();
                dbi.ConnectionString = string.Format(@"Data Source={0};Database={1};Uid={2};Pwd={3};", conn.DBInstance, conn.DBName, conn.DBUser, conn.DBPassword);
                dbi.IsolationLevel = IsolationLevel.ReadCommitted;
                dbi.Timeout = 15;
            });
            configuration.AddDeserializedMapping(Mapping, null);
            sessionFactory = configuration.BuildSessionFactory();
        }

        private static SystemRepositoryBase instance()
        {
            if (uniqueInstance == null)
            {
                // Lock area where instance is created
                lock (syncLock)
                {
                    if (uniqueInstance == null)
                    {
                        uniqueInstance = new SystemRepositoryBase();
                    }
                }
            }
            return uniqueInstance;
        }

        public static SystemRepositoryBase Instance => instance();

        public ISession currentSession
        {
            get
            {
                if (_currentSession == null)
                {
                    _currentSession = sessionFactory.OpenSession();
                }
                return _currentSession;
            }
        }

        public IStatelessSession statelessSession
        {
            get
            {
                if (_statelessSession == null)
                {
                    _statelessSession = sessionFactory.OpenStatelessSession();
                }
                return _statelessSession;
            }
        }
        public IUnitOfWorkFactory UnitOfWork
        {
            get
            {
                if (_unitOfWork == null)
                {
                    _unitOfWork = new UnitOfWorkFactory(configuration, sessionFactory, currentSession);
                }
                return _unitOfWork;
            }
        }
        public IConnection Connection
        {
            get
            {
                if (_connection == null)
                {
                    _connection = new SystemConnection();
                }
                return _connection;
            }
        }
        public HbmMapping Mapping
        {
            get
            {
                if (_mapping == null)
                {
                    _mapping = CreateMapping();
                }
                return _mapping;
            }
        }
        private HbmMapping CreateMapping()
        {
            var mapper = new ModelMapper();
            //Add mapping classes to the model mapper
            var types = Assembly.Load("Treemas.AuditTrail.Repository").GetTypes();
            mapper.AddMappings(types);

            //Create and return a HbmMapping of the model mapping in code
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }
}
