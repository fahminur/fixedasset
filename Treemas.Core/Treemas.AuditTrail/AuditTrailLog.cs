﻿using System;
using Treemas.AuditTrail.Model;
using Treemas.AuditTrail.Interface;
using Newtonsoft.Json;

namespace Treemas.AuditTrail
{
    public class AuditTrailLog : IAuditTrailLog
    {
        private IAuditDataRepository _auditRepository;
        public AuditTrailLog(IAuditDataRepository auditRepository)
        {
            this._auditRepository = auditRepository;
        }
        public void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action)
        {
            AuditData audit = new AuditData(0L);
            audit.FunctionName = functionName;
            audit.ObjectName = auditedObject.GetType().Name;
            audit.ObjectValue = JsonConvert.SerializeObject(auditedObject).ToString();
            audit.Action = action;
            audit.ActionBy = actionby;
            audit.ActionDateTime = DateTime.Now;
            _auditRepository.Add(audit);
        }
    }
}
