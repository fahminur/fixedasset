﻿using System;
using System.IO;
using System.Reflection;
using System.Web.Hosting;
using System.ServiceModel.Activation;
using Treemas.Base.Utilities;
using Treemas.Credential;
using Treemas.Credential.Interface;
using Treemas.Credential.Repository;
using Treemas.Base.Configuration;
using Treemas.Base.Configuration.Binder;


namespace Treemas.SingleSignon.Service
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            this._InitConfiguration();
        }

        private void _InitConfiguration()
        {
            string path = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "Config");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            ConfigurationBinder binder = new XmlFileConfigurationBinder("SingleSignon", path);
            binder.Load();

            CompositeConfigurationItem configurations = (CompositeConfigurationItem)binder.GetConfiguration("DBSingleSignon");
            ConfigurationItem configuration;

            if (configurations.IsNull())
            {
                configurations = new CompositeConfigurationItem("DBSingleSignon");
                configuration = new ConfigurationItem
                {
                    Key = "DBInstance",
                    Value = "DB-Instance-SSO-Service"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBUser",
                    Value = "DB-User-SSO-Service-Encrypted"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBPassword",
                    Value = "DB-Password-SSO-Service-Encrypted"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBName",
                    Value = "DB-Name-SSO-Service-Encrypted"
                };
                configurations.AddItem(configuration);
                binder.AddConfiguration(configurations);
            }

            configurations = (CompositeConfigurationItem)binder.GetConfiguration("DBUserHost");
            if (configurations.IsNull())
            {
                configurations = new CompositeConfigurationItem("DBUserHost");
                configuration = new ConfigurationItem
                {
                    Key = "DBInstance",
                    Value = "DB-Instance-User-Hosted"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBUser",
                    Value = "DB-User-User-Hosted-Encrypted"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBPassword",
                    Value = "DB-Password-User-Hosted-Encrypted"
                };
                configurations.AddItem(configuration);
                configuration = new ConfigurationItem
                {
                    Key = "DBName",
                    Value = "DB-Name-User-Hosted-Encrypted"
                };
                configurations.AddItem(configuration);
                binder.AddConfiguration(configurations);
            }

            configuration = binder.GetConfiguration("WalkerWorkingPeriod");
            if (configuration.IsNull())
            {
                configuration = new ConfigurationItem
                {
                    Key = "WalkerWorkingPeriod",
                    Value = "60000",
                    Description = "Walker's working period"
                };
                binder.AddConfiguration(configuration);
            }
            Configurations.Instance.WalkerWorkingPeriod = Convert.ToInt32(configuration.Value);

            configuration = binder.GetConfiguration("EncryptionKey");
            if (configuration.IsNull())
            {
                configuration = new ConfigurationItem
                {
                    Key = "EncryptionKey",
                    Value = "Encryption_KEY_VALUE",
                    Description = "Encryption Key Value"
                };
                binder.AddConfiguration(configuration);
            }
            Configurations.Instance.EncryptionKey = configuration.Value;
            Encryption.Instance.SetHashKey(Configurations.Instance.EncryptionKey);

            binder.Save();
        }
    }
}