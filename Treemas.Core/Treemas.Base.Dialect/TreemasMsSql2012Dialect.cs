﻿using NHibernate;
using NHibernate.Dialect;
using NHibernate.Dialect.Function;


namespace Treemas.Base.Dialect
{
    public class TreemasMsSql2012Dialect : MsSql2012Dialect
    {
        public TreemasMsSql2012Dialect()
        {
            this.RegisterFunction("age", new SQLFunctionTemplate(NHibernateUtil.Int32, "datediff(year, ?1, getdate())"));

        }
    }
}
