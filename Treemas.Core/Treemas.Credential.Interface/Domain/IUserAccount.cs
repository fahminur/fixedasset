﻿using System;
using Treemas.Credential.Model;
using System.Collections.Generic;

namespace Treemas.Credential.Interface
{
    public interface IUserAccount
    {
        User IsUserAuthenticSSO(string username, string password);
        User IsUserAuthentic(string username, string password, string currentApp = "");
        User GetUserAttributes(string username, string currentApp = "");
        void _FetchMenus(User user);
        void UpdateLoginAttempt(string username, int loginAttempt, bool active, DateTime? lastlogin);
        void SetUserToInactive(string username);
        IList<SignonLoginInfo> GetLoginInfos(string username);
        void SaveLoginInfo(User user, string hostname, string hostIP, string browser, string browserVersion, bool isMobile, string sessionid);
        void RemoveLoginInfo(SignonLoginInfo info);
        void setUpdateMenu(long id, bool update);
    }
}
