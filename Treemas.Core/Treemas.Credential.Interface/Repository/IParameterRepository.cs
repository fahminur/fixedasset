﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IParameterRepository : IBaseRepository<Parameter>
    {
        Parameter getParameter();
        bool IsDuplicate(string UserId);
    }
}
