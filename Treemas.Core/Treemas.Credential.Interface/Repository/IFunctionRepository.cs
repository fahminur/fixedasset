﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IFunctionRepository : IBaseRepository<AuthorizationFunction>
    {
        IList<AuthorizationFunction> getFunctions(IList<string> ApplicationIds);
        bool IsDuplicate(string application, string functionid);
        PagedResult<AuthorizationFunction> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AuthorizationFunction> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<AuthorizationFunction> FindAllPagedByApp(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string application);
        PagedResult<AuthorizationFunction> FindAllPagedByApp(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue ,string application);
        AuthorizationFunction getFunction(long id);
    }
}
