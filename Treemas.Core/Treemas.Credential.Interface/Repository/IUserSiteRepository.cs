﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IUserSiteRepository: IBaseRepository<UserSite>
    {
        bool IsDuplicate(string UserName);
        PagedResult<UserSite> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<UserSite> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        UserSite getSite(string UserName, string Application);
    }
}
