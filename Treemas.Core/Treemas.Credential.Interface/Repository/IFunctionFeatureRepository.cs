﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IFunctionFeatureRepository : IBaseRepository<FunctionFeature>
    {
        FunctionFeature getFunctionFeature(long id);
        IList<FunctionFeature> getFunctionFeatures(long functionid);
        int deleteSelected(long functionId, long featureid);
        PagedResult<FunctionFeature> FindAllPagedbyFunction(long functionId, int pageNumber, string orderColumn, int itemsPerPage, string orderKey);
    }
}
