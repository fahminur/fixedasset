﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IApplicationRepository : IBaseRepository<Application>
    {
        IList<Application> getApplications(IList<string> Ids);
        bool IsDuplicate(string applicationid);
        PagedResult<Application> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Application> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        Application getApplication(long id);
    }
}
