﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IFeatureRepository : IBaseRepository<AuthorizationFeature>
    {
        IList<AuthorizationFeature> getFeatures(IList<string> ApplicationIds);
        bool IsDuplicate(string application, string featureid);
        PagedResult<AuthorizationFeature> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AuthorizationFeature> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<AuthorizationFeature> FindAllPagedByApplication(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string application);
        PagedResult<AuthorizationFeature> FindAllPagedByApplication(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string application, string searchColumn, string searchValue);
        AuthorizationFeature getFeature(long id);
    }
}
