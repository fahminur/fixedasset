﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IRoleRepository : IBaseRepository<Role>
    {
        IList<Role> getApplicationRole(string application);
        bool IsDuplicate(string application, string roleid);
        PagedResult<Role> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Role> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<Role> FindAllPagedUserAppRole(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string application);
        PagedResult<Role> FindAllPagedUserAppRole(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string application);
        Role getRole(long id);
    }
}
