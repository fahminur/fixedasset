﻿using System.Collections.Generic;
using System;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User GetUser(string username);
        User GetUserById(long id);
        PagedResult<User> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<User> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<User> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
        PagedResult<User> FindAllPagedApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<User> FindAllPagedApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<User> FindAllPagedDateApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
        bool IsDuplicate(string username);
        void UpdateLoginAttempt(string username, int loginAttempt, bool disabled, DateTime? lastlogin);
        void resetPassword(long id, string password);
        void resetPassword(long id, string password, DateTime expiredDate);
        int deleteAll(string username);
        void SetUserToInactive(string username);
        IList<PasswordHistory> GetPasswordHistory(string username);
        void SavePasswordHistory(PasswordHistory passwordHistory, PasswordHistory deletedHistory);
        User GetUserForPrint(long id);
        void setApproved(long id);
        void setPrinted(long id);
        void setActive(long id, DateTime passexpdate, DateTime accvaldate);
        void setUpdateMenu(long id, bool update);
        void setUpdateMenu(string username, bool update);
        void setUpdateMenu(bool update);
    }
}
