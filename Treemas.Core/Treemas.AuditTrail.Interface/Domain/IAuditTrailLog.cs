﻿using System;
using Treemas.AuditTrail.Model;
using System.Collections.Generic;

namespace Treemas.AuditTrail.Interface
{
    public interface IAuditTrailLog
    {
        void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action);
    }
}
