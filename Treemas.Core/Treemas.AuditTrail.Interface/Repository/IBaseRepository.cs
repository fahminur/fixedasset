﻿using System;
using System.Collections.Generic;
using LinqSpecs;

namespace Treemas.AuditTrail.Interface
{
    public interface IBaseRepository<T>
    {
        void Add(T item);
        bool Contains(T item);
        int Count { get; }
        bool Remove(T item);
        void Save(T item);
        void SaveOrUpdateAll(IEnumerable<T> items);
        IEnumerable<T> FindAll();
        IEnumerable<T> FindAll(Specification<T> specification);
        T FindOne(Specification<T> specification);
        T GetOne(Specification<T> specification);
        Type GetSubjectType();
    }
}
