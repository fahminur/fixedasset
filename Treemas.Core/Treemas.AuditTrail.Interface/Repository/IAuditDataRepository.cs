﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Model;

namespace Treemas.AuditTrail.Interface
{
    public interface IAuditDataRepository : IBaseRepository<AuditData>
    {
        
    }
}
