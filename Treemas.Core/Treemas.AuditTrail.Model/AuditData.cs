﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.AuditTrail.Model
{
    public class AuditData : Entity
    {
        protected AuditData()
        {

        }
        public AuditData(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string FunctionName { get; set; }
        public virtual string ObjectName { get; set; }
        public virtual string ObjectValue { get; set; }
        public virtual string Action { get; set; }
        public virtual DateTime ActionDateTime { get; set; }
        public virtual string ActionBy { get; set; }
    }
}
