﻿using System;
using System.Data;
using System.Runtime.InteropServices;
using NHibernate;

namespace Treemas.Base.Repository.UnitOfWork
{
    public class UnitOfWorkImplementor : IUnitOfWorkImplementor
    {
        private readonly IUnitOfWorkFactory m_factory;
        private readonly ISession m_session;
        private IntPtr nativeResource = Marshal.AllocHGlobal(100);
        public UnitOfWorkImplementor(IUnitOfWorkFactory factory, ISession session)
        {
            m_factory = factory;
            m_session = session;
        }

        ~UnitOfWorkImplementor()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (m_session != null)
                {
                    m_session.Dispose();
                }
                if (m_factory != null)
                {
                    m_factory.DisposeUnitOfWork(this);
                }
            }
            // free native resources if there are any.
            if (nativeResource == IntPtr.Zero) return;
            Marshal.FreeHGlobal(nativeResource);
            nativeResource = IntPtr.Zero;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

            //m_factory.DisposeUnitOfWork(this);
            //m_session.Dispose();
        }

        public void IncrementUsages()
        {
            throw new NotImplementedException();
        }

        public void Flush()
        {
            m_session.Flush();
        }

        public bool IsInActiveTransaction
        {
            get
            {
                return m_session.Transaction.IsActive;
            }
        }

        public IUnitOfWorkFactory Factory
        {
            get { return m_factory; }
        }

        public ISession Session
        {
            get { return m_session; }
        }

        public IGenericTransaction BeginTransaction()
        {
            return new GenericTransaction(m_session.BeginTransaction());
        }

        public IGenericTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return new GenericTransaction(m_session.BeginTransaction(isolationLevel));
        }

        public void TransactionalFlush()
        {
            TransactionalFlush(IsolationLevel.ReadCommitted);
        }

        public void TransactionalFlush(IsolationLevel isolationLevel)
        {
            // $$$$$$$$$$$$$$$$ gns: take this, when making thread safe! $$$$$$$$$$$$$$
            //IUoWTransaction tx = UnitOfWork.Current.BeginTransaction(isolationLevel);   

            IGenericTransaction tx = BeginTransaction(isolationLevel);
            try
            {
                //forces a flush of the current unit of work
                tx.Commit();
            }
            catch
            {
                tx.Rollback();
                throw;
            }
            finally
            {
                tx.Dispose();
            }
        }
    }
}
