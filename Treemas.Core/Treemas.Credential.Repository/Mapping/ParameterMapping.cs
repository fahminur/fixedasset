﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;


namespace Treemas.Credential.Repository.Mapping
{
    public class ParameterMapping : ClassMapping<Parameter>
    {
        public ParameterMapping()
        {
            this.Table("SEC_PARAMETER");
            Id<string>(x => x.Id,
               map => { map.Column("PARAM_ID"); });

            Property<int>(x => x.LoginAttempt,
                map => { map.Column("LOGIN_ATTEMPT"); });

            Property<int>(x => x.SessionTimeout,
                map => { map.Column("SESSION_TIMEOUT"); });

            Property<int>(x => x.MaximumConcurrentLogin ,
                map => { map.Column("MAX_CONCURRENT_LOGIN"); });

            Property<DateTime?>(x => x.ChangedDate,
                map => { map.Column("CHANGE_DATE"); });
            
            Property<string>(x => x.ChangedBy,
                map => { map.Column("CHANGED_BY"); });

            Property<string>(x => x.PasswordExp,
                map => { map.Column("PASSWORD_EXPIRED_DATE"); });

            Property<string>(x => x.AccountValidity,
                map => { map.Column("ACCOUNT_VALIDITY_DATE"); });

        }
    }
}
