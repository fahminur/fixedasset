﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class LoginInfoHistoryMapping : ClassMapping<SignonLoginInfoHistory>
    {
        public LoginInfoHistoryMapping()
        {
            this.Table("SEC_LOGIN_INFO_HISTORY");
            Id<long>(x => x.Id, map => { map.Column("ID"); });
            Property<string>(x => x.Browser, map => { map.Column("BROWSER"); });
            Property<string>(x => x.BrowserVersion, map => { map.Column("BROWSER_VERSION"); });
            Property<string>(x => x.HostIP, map => { map.Column("HOST_IP"); });
            Property<string>(x => x.Hostname, map => { map.Column("HOST_NAME"); });
            Property<bool>(x => x.IsMobile, map => { map.Column("IS_MOBILE"); });
            Property<int>(x => x.LockTimeout, map => { map.Column("LOCK_TIMEOUT"); });
            Property<DateTime>(x => x.LoginTime, map => { map.Column("LOGIN_TIME"); });
            Property<int>(x => x.MaximumLogin, map => { map.Column("MAXIMUM_LOGIN"); });
            Property<int>(x => x.SessionTimeout, map => { map.Column("SESSION_TIMEOUT"); });
            Property<string>(x => x.Username, map => { map.Column("USERNAME"); });
            Property<string>(x => x.SessionId, map => { map.Column("SESSION_ID"); });
        }
    }
}
