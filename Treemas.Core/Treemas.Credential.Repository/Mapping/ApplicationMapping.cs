﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class ApplicationMapping : ClassMapping<Application>
    {
        public ApplicationMapping()
        {
            this.Table("SEC_APPLICATION");
            Id<long>(
                x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity) ;
                });

            Property<string>(x => x.ApplicationId ,
                map =>
                {
                    map.Column("APPLICATION_ID");
                    map.Update(false);
                });

            Property<string>(x => x.Name ,
                map =>
                {
                    map.Column("NAME");
                });

            Property<string>(x => x.Runtime ,
                map =>
                {
                    map.Column("RUNTIME");
                });

            Property<string>(x => x.Type,
                map =>
                {
                    map.Column("TYPE");
                });

            Property<string>(x => x.Description,
                map =>
                {
                    map.Column("DESCRIPTION");
                });

            Property<string>(x => x.CSSColor,
                map =>
                {
                    map.Column("CSS_COLOR");
                });

            Property<string>(x => x.Icon,
                map =>
                {
                    map.Column("ICON");
                });

            Property<DateTime?>(x => x.ChangedDate,
                map =>
                {
                    map.Column("CHANGED_DATE");
                });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });

        }
    }
}
