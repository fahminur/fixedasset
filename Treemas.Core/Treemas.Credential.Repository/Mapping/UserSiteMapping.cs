﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class UserSiteMapping : ClassMapping<UserSite>
    {
        public UserSiteMapping()
        {
            this.Table("SEC_USER_SITE");
            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });

            Property<string>(x => x.Username,
                map =>
                {
                    map.Column("USERNAME"); map.Update(false);
                });

            Property<string>(x => x.Application,
                map =>
                {
                    map.Column("APPLICATION");
                    map.Update(false);
                });

            Property<string>(x => x.SiteID,
                map =>
                {
                    map.Column("SITE_ID"); map.Update(false);
                });

            Property<bool>(x => x.Is_HO,
                map =>
                {
                    map.Column("IS_HO"); map.Update(false);
                });


            Property<DateTime?>(x => x.ChangedDate,
               map =>
               {
                   map.Column("CHANGED_DATE");
               });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });
        }
    }
}
