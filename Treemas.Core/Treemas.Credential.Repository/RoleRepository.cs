﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Repository
{
    public class RoleRepository : RepositoryController<Role>, IRoleRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RoleRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public Role getRole(long id)
        {
            return transact(() => session.QueryOver<Role>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public IList<Role> getApplicationRole(string application)
        {
            IEnumerable<Role> _applicationRole = this.FindAll(new AdHocSpecification<Role>(s => s._Application == application));
            return _applicationRole.ToList();
        }

        public bool IsDuplicate(string application, string Roleid)
        {
            int rowCount = transact(() => session.QueryOver<Role>()
                                                .Where(f => f.RoleId == Roleid && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Role> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Role> paged = new PagedResult<Role>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Role>()
                                                     .OrderByDescending(GetOrderByExpression<Role>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Role>()
                                                     .OrderBy(GetOrderByExpression<Role>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Role> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Role> specification;

            switch (searchColumn)
            {
                case "Application":
                    specification = new AdHocSpecification<Role>(s => s._Application.Contains(searchValue));
                    break;
                case "Name":
                    specification = new AdHocSpecification<Role>(s => s.Name.Contains(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<Role>(s => s.Description.Contains(searchValue));
                    break;
                case "RoleId":
                    specification = new AdHocSpecification<Role>(s => s.RoleId.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<Role>(s => s._Application.Contains(searchValue));
                    break;
            }

            PagedResult<Role> paged = new PagedResult<Role>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Role>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Role>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Role>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Role>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<Role>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<Role> FindAllPagedUserAppRole(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string application)
        {
            PagedResult<Role> paged = new PagedResult<Role>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Role>()
                                                     .Where(s => s._Application == application)
                                                     .OrderByDescending(GetOrderByExpression<Role>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Role>()
                                                     .Where(s => s._Application == application)
                                                     .OrderBy(GetOrderByExpression<Role>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = transact(() =>
                    session.Query<Role>()
                    .Where(s => s._Application == application).Count());
            return paged;
        }
        public PagedResult<Role> FindAllPagedUserAppRole(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string application)
        {
            Specification<Role> specification;

            switch (searchColumn)
            {
                case "RoleId":
                    specification = new AdHocSpecification<Role>(s => s.RoleId.Contains(searchValue) && s._Application == application);
                    break;
                default:
                    specification = new AdHocSpecification<Role>(s => s._Application.Contains(searchValue) && s._Application == application);
                    break;
            }

            PagedResult<Role> paged = new PagedResult<Role>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Role>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Role>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Role>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Role>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<Role>()
                                .Where(specification.ToExpression()).Count());

            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
