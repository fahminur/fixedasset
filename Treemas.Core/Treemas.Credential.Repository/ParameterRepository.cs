﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Repository
{
    public class ParameterRepository : RepositoryControllerString<Parameter>, IParameterRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public ParameterRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public Parameter getParameter()
        {
            return transact(() => session.QueryOver<Parameter>().Take(1).SingleOrDefault());
        }
        public bool IsDuplicate(string ID)
        {
            int rowCount = transact(() => session.QueryOver<Parameter>()
                                                .Where(f => f.ParamId == ID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
