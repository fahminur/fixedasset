﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Repository
{
    public class UserSiteRepository : RepositoryController<UserSite>, IUserSiteRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public UserSiteRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public PagedResult<UserSite> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<UserSite> paged = new PagedResult<UserSite>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<UserSite>()
                                                     .OrderByDescending(GetOrderByExpression<UserSite>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<UserSite>()
                                                     .OrderBy(GetOrderByExpression<UserSite>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<UserSite> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<UserSite> specification;

            switch (searchColumn)
            {
                case "Username":
                    specification = new AdHocSpecification<UserSite>(s => s.Username.Contains(searchValue));
                    break;
                case "Application":
                    specification = new AdHocSpecification<UserSite>(s => s.Application.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<UserSite>(s => s.SiteID.Contains(searchValue));
                    break;
            }

            PagedResult<UserSite> paged = new PagedResult<UserSite>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<UserSite>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<UserSite>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<UserSite>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<UserSite>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        public UserSite getSite(string UserName, string Application)
        {
            return transact(() => session.QueryOver<UserSite>()
                                                .Where(f => f.Username == UserName && f.Application == Application).SingleOrDefault());
        }

        public bool IsDuplicate(string UserName)
        {
            int rowCount = transact(() => session.QueryOver<UserSite>()
                                                .Where(f => f.Username == UserName).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
