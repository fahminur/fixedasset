﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;

namespace Treemas.Credential.Repository
{
    public class LoginInfoRepository : RepositoryController<SignonLoginInfo>, ILoginInfoRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public LoginInfoRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public SignonLoginInfo GetLoginInfo(long id)
        {
            SignonLoginInfo _loginInfo = this.FindOne(new AdHocSpecification<SignonLoginInfo>(s => s.Id == id));
            return _loginInfo;
        }
        public SignonLoginInfo GetLoginInfoByData(SignonLoginInfo loginInfo)
        {
            IEnumerable<SignonLoginInfo> _loginInfos = this.FindAll(new AdHocSpecification<SignonLoginInfo>(s => s.Username == loginInfo.Username &&
               s.Hostname == loginInfo.Hostname && s.HostIP == loginInfo.HostIP && s.Browser == loginInfo.Browser && s.SessionId == loginInfo.SessionId &&
               s.BrowserVersion == loginInfo.BrowserVersion && s.IsMobile == loginInfo.IsMobile));
            if (_loginInfos != null)
            {
                if (_loginInfos.Count<SignonLoginInfo>() > 0)
                {
                    return _loginInfos.First<SignonLoginInfo>();
                }
            }
            return null;
        }
        public IList<SignonLoginInfo> GetLoginInfos(string username)
        {
            IEnumerable<SignonLoginInfo> _loginInfos = this.FindAll(new AdHocSpecification<SignonLoginInfo>(s => s.Username == username));
            return _loginInfos.ToList();
        }

        public void EvaluateLoginInfo(IList<SignonLoginInfo> loginInfos)
        {
            transact(() => _EvaluateLoginInfo(loginInfos));
        }

        private void _EvaluateLoginInfo(IList<SignonLoginInfo> loginInfos)
        {
            foreach (SignonLoginInfo info in loginInfos)
            {
                switch (info.PolicyState)
                {
                    case SignonPolicyState.SessionExpired:
                        SignonLoginInfoHistory infoHist = new SignonLoginInfoHistory();
                        infoHist.Id = info.Id;
                        infoHist.Username = info.Username;
                        infoHist.LoginTime = info.LoginTime;
                        infoHist.SessionTimeout = info.SessionTimeout;
                        infoHist.LockTimeout = info.LockTimeout;
                        infoHist.MaximumLogin = info.MaximumLogin;
                        infoHist.Hostname = info.Hostname;
                        infoHist.HostIP = info.HostIP;
                        infoHist.Browser = info.Browser;
                        infoHist.BrowserVersion = info.BrowserVersion;
                        infoHist.IsMobile = info.IsMobile;
                        infoHist.SessionId = info.SessionId;
                        session.Save(infoHist);
                        session.Delete(info);

                        break;
                    case SignonPolicyState.LockActive:
                        info.LockTime = info.LoginTime;
                        break;
                }
            }

        }

        public void LogoutProcess(SignonLoginInfo info)
        {
            transact(() => _LogoutProcess(info));
        }

        private void _LogoutProcess(SignonLoginInfo info)
        {
            SignonLoginInfoHistory infoHist = new SignonLoginInfoHistory();
            infoHist.Id = info.Id;
            infoHist.Username = info.Username;
            infoHist.LoginTime = info.LoginTime;
            infoHist.SessionTimeout = info.SessionTimeout;
            infoHist.LockTimeout = info.LockTimeout;
            infoHist.MaximumLogin = info.MaximumLogin;
            infoHist.Hostname = info.Hostname;
            infoHist.HostIP = info.HostIP;
            infoHist.Browser = info.Browser;
            infoHist.BrowserVersion = info.BrowserVersion;
            infoHist.IsMobile = info.IsMobile;
            infoHist.SessionId = info.SessionId;

            session.Save(infoHist);
            session.Delete(info);
        }
    }
}
