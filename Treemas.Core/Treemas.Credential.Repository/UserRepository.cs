﻿using System;
using System.Linq;
using System.Collections.Generic;
using NHibernate.Criterion;
using LinqSpecs;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Globals;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;

namespace Treemas.Credential.Repository
{
    public class UserRepository : RepositoryController<User>, IUserRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public UserRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public void Delete(int id)
        {
            Remove(new User(id));
        }
        public User GetUser(string username)
        {
            User _user = this.FindOne(new AdHocSpecification<User>(s => s.Username == username));
            return _user;
        }
        public User GetUserById(long id)
        {
            return transact(() => session.QueryOver<User>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public User GetUserForPrint(long id)
        {
            return transact(() => session.QueryOver<User>()
                                                .Where(f => f.Id == id && f.IsPrinted == false).SingleOrDefault());
        }
        public IList<User> GetUsers()
        {
            return null;
        }
        public PagedResult<User> GetUsers(int pageNumber, int itemsPerPage)
        {
            return null;
        }
        public IList<User> SearchLikeName(string key)
        {
            return null;
        }
        public PagedResult<User> SearchLikeName(string key, int pageNumber, int itemsPerPage)
        {
            return null;
        }
        public long GetUserCount()
        {
            long num = 0L;
            num = Count;
            return num;
        }

        public bool IsDuplicate(string username)
        {
            int rowCount = transact(() => session.QueryOver<User>()
                                                .Where(f => f.Username == username).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public PagedResult<User> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<User> paged = new PagedResult<User>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<User>()
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<User>()
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<User> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<User> paged = new PagedResult<User>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<User>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<User>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<User>()
                                .Where(specification).RowCount());
            return paged;
        }

        public PagedResult<User> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue)
        {
            
            PagedResult<User> paged = new PagedResult<User>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("PasswordExpirationDate"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<User>()
                                     .Where(f => f.PasswordExpirationDate == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<User>()
                                     .Where(f => f.PasswordExpirationDate == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<User>()
                               .Where(f => f.PasswordExpirationDate == searchValue).RowCount());
            }else
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<User>()
                                     .Where(f => f.AccountValidityDate == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<User>()
                                     .Where(f => f.AccountValidityDate == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<User>()
                               .Where(f => f.AccountValidityDate == searchValue).RowCount());
            }
            return paged;
        }

        public void UpdateLoginAttempt(string username, int loginAttempt, bool active, DateTime? lastlogin)
        {
            if (loginAttempt == 0 && !lastlogin.IsNull())
            {
                string hqlSyntax = "Update User Set LastLogin=:LastLogin, LoginAttempt = :LoginAttempt, IsActive = :IsActive Where UserName = :UserName";
                session.CreateQuery(hqlSyntax)
                        .SetInt32("LoginAttempt", loginAttempt)
                        .SetString("UserName", username)
                        .SetBoolean("IsActive", active)
                        .SetDateTime("LastLogin", DateTime.Now)
                        .ExecuteUpdate();
            }
            else
            {
                string hqlSyntax = "Update User Set LoginAttempt = :LoginAttempt, IsActive = :IsActive Where UserName = :UserName";
                session.CreateQuery(hqlSyntax)
                        .SetInt32("LoginAttempt", loginAttempt)
                        .SetString("UserName", username)
                        .SetBoolean("IsActive", active)
                        .ExecuteUpdate();
            }
        }

        public void SetUserToInactive(string username)
        {
            string hqlSyntax = "Update User Set IsActive = :IsActive, ChangedDate = :ChangedDate Where UserName = :UserName";
            session.CreateQuery(hqlSyntax)
                    .SetBoolean("IsActive", false)
                    .SetDateTime("ChangedDate", DateTime.Now)
                    .SetString("UserName", username)
                    .ExecuteUpdate();
        }

        public int deleteAll(string username)
        {

            string hqlSyntax = "delete Authorization where Username = :Username";
            int deletedEntities = session.CreateQuery(hqlSyntax)
                    .SetString("Username", username)
                    .ExecuteUpdate();

            string hqlSyntaxUserApp = "delete UserApplication where Username = :Username";
            int deletedEntities2 = session.CreateQuery(hqlSyntaxUserApp)
                    .SetString("Username", username)
                    .ExecuteUpdate();

            return deletedEntities;
        }

        public void resetPassword(long id, string password)
        {
            string hqlSyntax = "update User set Password = :password, LoginAttempt = 0, IsPrinted = false where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetString("password", Encryption.Instance.EncryptText(password))
                .SetInt64("id", id)
                .ExecuteUpdate();
        }

        public void resetPassword(long id, string password, DateTime expiredDate)
        {
            string hqlSyntax = "update User set LastLogin=:LastLogin, Password = :password, PasswordExpirationDate = :PasswordExpirationDate , IsPrinted = false where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetString("password", Encryption.Instance.EncryptText(password))
                .SetDateTime("PasswordExpirationDate", expiredDate)
                .SetDateTime("LastLogin", DateTime.Now)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }

        public IList<PasswordHistory> GetPasswordHistory(string username)
        {
            return transact(() => session.QueryOver<PasswordHistory>()
                                    .Where(f => f.Username == username)
                                    .OrderBy(x => x.Id).Asc
                                    .List());
        }

        public void SavePasswordHistory(PasswordHistory passwordHistory, PasswordHistory deletedHistory)
        {
            transact(() => {
                session.Save(passwordHistory);
                if (!deletedHistory.IsNull())
                    session.Delete(deletedHistory); });
        }
        public void setApproved(long id)
        {
            string hqlSyntax = "update User set Approved = true where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }
        public void setActive(long id, DateTime passexpdate, DateTime accvaldate)
        {
            string hqlSyntax = "update User set IsActive = true, AccountValidityDate = :accvaldate, PasswordExpirationDate = :passexpdate, LoginAttempt = 0, LastLogin = null where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetInt64("id", id)
                .SetDateTime("passexpdate", passexpdate)
                .SetDateTime("accvaldate", accvaldate)
                .ExecuteUpdate();
        }
        public void setPrinted(long id)
        {
            string hqlSyntax = "update User set IsPrinted = true where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }
        public void setUpdateMenu(long id, bool update)
        {
            string hqlSyntax = "update User set UpdateMenu = :UpdateMenu where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetBoolean("UpdateMenu", update)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }
        public void setUpdateMenu(string username, bool update)
        {
            string hqlSyntax = "update User set UpdateMenu = :UpdateMenu where Username = :Username";
            session.CreateQuery(hqlSyntax)
                .SetBoolean("UpdateMenu", update)
                .SetString("Username", username)
                .ExecuteUpdate();
        }
        public void setUpdateMenu(bool update)
        {
            string hqlSyntax = "update User set UpdateMenu = :UpdateMenu ";
            session.CreateQuery(hqlSyntax)
                .SetBoolean("UpdateMenu", update)
                .ExecuteUpdate();
        }
        public PagedResult<User> FindAllPagedApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<User> paged = new PagedResult<User>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<User>()
                                                    .Where(x => x.Approved == false)
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<User>()
                                                     .Where(x => x.Approved == false)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<User>()
                               .Where(x => x.Approved == false)
                               .RowCount());
            return paged;
        }

        public PagedResult<User> FindAllPagedApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<User> paged = new PagedResult<User>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<User>()
                                 .Where(x => x.Approved == false)
                                 .And(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<User>()
                                 .Where(x => x.Approved == false)
                                 .And(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<User>()
                                .Where(f => f.Approved == false)
                                .And(specification)
                                .RowCount());
            return paged;
        }

        public PagedResult<User> FindAllPagedDateApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue)
        {

            PagedResult<User> paged = new PagedResult<User>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("PasswordExpirationDate"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<User>()
                                     .Where(f => f.PasswordExpirationDate == searchValue)
                                     .And(f => f.Approved == false)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<User>()
                                     .Where(f => f.PasswordExpirationDate == searchValue)
                                     .And(f => f.Approved == false)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<User>()
                               .Where(f => f.PasswordExpirationDate == searchValue)
                               .And(f => f.Approved == false).RowCount());
            }
            else
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<User>()
                                     .Where(f => f.AccountValidityDate == searchValue)
                                     .And(f => f.Approved == false)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<User>()
                                     .Where(f => f.AccountValidityDate == searchValue)
                                     .And(f => f.Approved == false)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<User>()
                               .Where(f => f.AccountValidityDate == searchValue)
                               .And(f => f.Approved == false).RowCount());
            }
            return paged;
        }

    }
}
