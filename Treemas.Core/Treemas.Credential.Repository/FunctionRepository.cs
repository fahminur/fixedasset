﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Repository
{
    public class FunctionRepository : RepositoryController<AuthorizationFunction>, IFunctionRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public FunctionRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public AuthorizationFunction getFunction(long id)
        {
            return transact(() => session.QueryOver<AuthorizationFunction>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<AuthorizationFunction> getFunctions(IList<string> ApplicationIds)
        {
            return transact(() => 
                   session.QueryOver<AuthorizationFunction>()
                          .WhereRestrictionOn(val => val.Application)
                          .IsIn(ApplicationIds.ToArray()).List());
        }
        public bool IsDuplicate(string application, string functionid)
        {
            int rowCount = transact(() => session.QueryOver<AuthorizationFunction>()
                                                .Where(f => f.FunctionId == functionid && f.Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<AuthorizationFunction> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AuthorizationFunction> paged = new PagedResult<AuthorizationFunction>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AuthorizationFunction>()
                                                     .OrderByDescending(GetOrderByExpression<AuthorizationFunction>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AuthorizationFunction>()
                                                     .OrderBy(GetOrderByExpression<AuthorizationFunction>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<AuthorizationFunction> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue )
        {
            Specification<AuthorizationFunction> specification;

            switch (searchColumn)
            {
                case "Application":
                    specification = new AdHocSpecification<AuthorizationFunction>(s => s.Application.Contains(searchValue));
                    break;
                case "Name":
                    specification = new AdHocSpecification<AuthorizationFunction>(s => s.Name.Contains(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<AuthorizationFunction>(s => s.Description.Contains(searchValue));
                    break;
                case "FunctionId":
                    specification = new AdHocSpecification<AuthorizationFunction>(s => s.FunctionId.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<AuthorizationFunction>(s => s.Application.Contains(searchValue));
                    break;
            }

            PagedResult<AuthorizationFunction> paged = new PagedResult<AuthorizationFunction>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AuthorizationFunction>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AuthorizationFunction>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AuthorizationFunction>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<AuthorizationFunction>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<AuthorizationFunction>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<AuthorizationFunction> FindAllPagedByApp(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string application)
        {
            Specification<AuthorizationFunction> specification;
            specification = new AdHocSpecification<AuthorizationFunction>(s => s.Application == application);

            PagedResult<AuthorizationFunction> paged = new PagedResult<AuthorizationFunction>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AuthorizationFunction>()
                                                     .Where(specification)
                                                     .OrderByDescending(GetOrderByExpression<AuthorizationFunction>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AuthorizationFunction>()
                                                     .Where(specification)
                                                     .OrderBy(GetOrderByExpression<AuthorizationFunction>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = transact(() =>
             session.Query<AuthorizationFunction>()
                    .Where(specification.ToExpression()).Count());

            return paged;
        }
        public PagedResult<AuthorizationFunction> FindAllPagedByApp(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string application)
        {
            Specification<AuthorizationFunction> specification;

            switch (searchColumn)
            {
                case "Name":
                    specification = new AdHocSpecification<AuthorizationFunction>(s => s.Name.Contains(searchValue) && s.Application == application);
                    break;
                case "Description":
                    specification = new AdHocSpecification<AuthorizationFunction>(s => s.Description.Contains(searchValue) && s.Application == application);
                    break;
                case "FunctionId":
                    specification = new AdHocSpecification<AuthorizationFunction>(s => s.FunctionId.Contains(searchValue) && s.Application == application);
                    break;
                default:
                    specification = new AdHocSpecification<AuthorizationFunction>(s => s.Application.Contains(searchValue) && s.Application == application);
                    break;
            }

            PagedResult<AuthorizationFunction> paged = new PagedResult<AuthorizationFunction>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AuthorizationFunction>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AuthorizationFunction>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AuthorizationFunction>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<AuthorizationFunction>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<AuthorizationFunction>()
                                .Where(specification.ToExpression()).Count());
            
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
