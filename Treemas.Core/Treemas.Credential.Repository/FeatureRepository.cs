﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Repository
{
    public class FeatureRepository : RepositoryController<AuthorizationFeature>, IFeatureRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public FeatureRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public AuthorizationFeature getFeature(long id)
        {
            return transact(() => session.QueryOver<AuthorizationFeature>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<AuthorizationFeature> getFeatures(IList<string> ApplicationIds)
        {
            return transact(() =>
                   session.QueryOver<AuthorizationFeature>()
                          .WhereRestrictionOn(val => val.Application)
                          .IsIn(ApplicationIds.ToArray()).List());
        }

        public bool IsDuplicate(string application, string featureid)
        {
            int rowCount = transact(() => session.QueryOver<AuthorizationFeature>()
                                                .Where(f => f.FeatureId == featureid && f.Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<AuthorizationFeature> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AuthorizationFeature> paged = new PagedResult<AuthorizationFeature>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AuthorizationFeature>()
                                                     .OrderByDescending(GetOrderByExpression<AuthorizationFeature>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AuthorizationFeature>()
                                                     .OrderBy(GetOrderByExpression<AuthorizationFeature>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<AuthorizationFeature> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<AuthorizationFeature> specification;

            switch (searchColumn)
            {
                case "Application":
                    specification = new AdHocSpecification<AuthorizationFeature>(s => s.Application.Contains(searchValue));
                    break;
                case "Name":
                    specification = new AdHocSpecification<AuthorizationFeature>(s => s.Name.Contains(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<AuthorizationFeature>(s => s.Description.Contains(searchValue));
                    break;
                case "FeatureId":
                    specification = new AdHocSpecification<AuthorizationFeature>(s => s.FeatureId.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<AuthorizationFeature>(s => s.Application.Contains(searchValue));
                    break;
            }

            PagedResult<AuthorizationFeature> paged = new PagedResult<AuthorizationFeature>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AuthorizationFeature>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<AuthorizationFeature>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AuthorizationFeature>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<AuthorizationFeature>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<AuthorizationFeature>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<AuthorizationFeature> FindAllPagedByApplication(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string application)
        {
            PagedResult<AuthorizationFeature> paged = new PagedResult<AuthorizationFeature>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AuthorizationFeature>()
                                                     .Where(x => x.Application == application)
                                                     .OrderByDescending(GetOrderByExpression<AuthorizationFeature>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AuthorizationFeature>()
                                                     .Where(x => x.Application == application)
                                                     .OrderBy(GetOrderByExpression<AuthorizationFeature>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = transact(() =>
                         session.Query<AuthorizationFeature>()
                                .Where(x => x.Application == application).Count());
            return paged;
        }
        public PagedResult<AuthorizationFeature> FindAllPagedByApplication(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string application, string searchColumn, string searchValue)
        {
            Specification<AuthorizationFeature> specification;

            switch (searchColumn)
            {
                case "Application":
                    specification = new AdHocSpecification<AuthorizationFeature>(s => s.Application.Contains(searchValue));
                    break;
                case "Name":
                    specification = new AdHocSpecification<AuthorizationFeature>(s => s.Name.Contains(searchValue) && s.Application == application);
                    break;
                case "Description":
                    specification = new AdHocSpecification<AuthorizationFeature>(s => s.Description.Contains(searchValue) && s.Application == application);
                    break;
                case "FeatureId":
                    specification = new AdHocSpecification<AuthorizationFeature>(s => s.FeatureId.Contains(searchValue) && s.Application == application);
                    break;
                default:
                    specification = new AdHocSpecification<AuthorizationFeature>(s => s.Application.Contains(searchValue) && s.Application == application);
                    break;
            }

            PagedResult<AuthorizationFeature> paged = new PagedResult<AuthorizationFeature>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<AuthorizationFeature>()
                                 .Where(x => x.Application == application)
                                 .OrderByDescending(GetOrderByExpression<AuthorizationFeature>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<AuthorizationFeature>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<AuthorizationFeature>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<AuthorizationFeature>()
                                .Where(specification.ToExpression()).Count());

            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }


    }
}
