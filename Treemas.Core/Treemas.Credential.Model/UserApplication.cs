﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace Treemas.Credential.Model
{
    public class UserApplication : Entity
    {
        protected UserApplication()
        { }
        public UserApplication(long id) : base(id)
        {
            
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string Application { get; set; }
        public virtual bool IsDefault { get; set; }
        public virtual string Username { get; set; }
        public virtual bool Selected { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
        public virtual Application app { get; set; }
    }
}
