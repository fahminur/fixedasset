﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public class AuthorizationFeature : Entity
    {
        protected AuthorizationFeature()
        {
            this.Qualifiers = new List<AuthorizationFeatureQualifier>();
        }
        public AuthorizationFeature(long id) : base(id)
        {

        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual IList<AuthorizationFeatureQualifier> Qualifiers { get; set; }
        public virtual string Application { get; set; }
        public virtual string Description { get; set; }
        public virtual string FeatureId { get; set; }
        public virtual string Name { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }
}
