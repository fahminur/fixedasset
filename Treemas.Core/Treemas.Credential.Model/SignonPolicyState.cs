﻿namespace Treemas.Credential.Model
{
    public enum SignonPolicyState
    {
        SessionExpired,
        LockActive,
        Hold
    }
}
