﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;
using Treemas.Foundation.Model;

namespace Treemas.Credential.Model
{
    public class User : Entity
    {
        protected User()
        {
            this.Roles = new List<Role>();
            this.Applications = new List<ApplicationInfo>();
            this.SessionTimeout = 20;
            this.LockTimeout = 10;
            this.MaximumConcurrentLogin = 3;
            this.AccountValidityDate = DateTime.MinValue;
            this.PasswordExpirationDate = DateTime.MinValue;
            this.Site = null;   
        }
        public User(long id) : base(id)
        {
        }
        public virtual int GetMaximumSessionTimeout()
        {
            int sessionTimeout = this.SessionTimeout;
            if (!this.Roles.IsNullOrEmpty<Role>())
            {
                foreach (Role role in this.Roles)
                {
                    if (role.SessionTimeout > sessionTimeout)
                    {
                        sessionTimeout = role.SessionTimeout;
                    }
                }
            }
            return sessionTimeout;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual DateTime AccountValidityDate { get; set; }
        public virtual DateTime PasswordExpirationDate { get; set; }
        public virtual IList<ApplicationInfo> Applications { get; set; }
        public virtual ApplicationInfo CurrentApplication { get; set; }
        public virtual IList<SignonLoginInfo> LoginInfos { get; set; }
        public virtual BusinessDate BusinessDate { get; set; }
        public virtual UserSite Site { get; set; }
        public virtual DateTime? LastLogin { get; set; }
        public virtual string Username { get; set; }
        public virtual bool InActiveDirectory { get; set; }
        public virtual bool IsPasswordNeedToBeReset
        {
            get
            {
                if (LastLogin.IsNull()) return true; else return false;
            }
        }
        public virtual int SessionTimeout { get; set; }
        public virtual IList<Role> Roles { get; set; }
        public virtual string Password { get; set; }
        public virtual int LoginAttempt { get; set; }
        public virtual bool UpdateMenu { get; set; }
        public virtual string Name
        {
            get
            {
                string str = (this.FirstName != null) ? this.FirstName : string.Empty;
                return (str + ((this.LastName != null) ? (" " + this.LastName) : string.Empty)).Trim();
            }
        }
        public virtual string ApplicationMenuPath()
        {
            string str = "Views/Menu/_" + this.Id.ToString() + this.Username + this.CurrentApplication.Id + ".cshtml";
            return str;
        }
        public virtual string ApplicationMenuView()
        {
            string str = "~/Views/Menu/_" + this.Id.ToString() + this.Username + this.CurrentApplication.Id + ".cshtml";
            return str;
        }
        public virtual int MaximumConcurrentLogin { get; set; }
        public virtual bool IsMaximumConcurrentLoginReached
        {
            get
            {
                if (!this.LoginInfos.IsNullOrEmpty())
                    if (this.MaximumConcurrentLogin <= this.LoginInfos.Count) return true; else return false;
                else return true;
            }
        }
        public virtual int LockTimeout { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string RegNo { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsPasswordExpired()
        {
            if (this.PasswordExpirationDate < DateTime.Now)
                return true;
            return false;
        }
        public virtual bool IsValidityExpired()
        {
            if (this.AccountValidityDate < DateTime.Now)
                return true;
            return false;
        }
        public virtual bool IsAccountNotUsed()
        {
            if (!LastLogin.IsNull())
            {
                if (LastLogin.Value.AddDays(30) <= DateTime.Now)
                    return true;
            }
            if (!ChangedDate.IsNull() && LastLogin.IsNull())
            {
                if (ChangedDate.Value.AddDays(30) <= DateTime.Now)
                    return true;
            }
            return false;
        }
        public virtual bool IsPrinted { get; set; }
        public virtual bool Approved { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }

    }
}
