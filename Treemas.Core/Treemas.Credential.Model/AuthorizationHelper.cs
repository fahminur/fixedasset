﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace Treemas.Credential.Model
{
    public class AuthorizationHelper
    {
        public virtual long RoleId { get; set; }
        public virtual bool Selected { get; set; }
        
    }
}
