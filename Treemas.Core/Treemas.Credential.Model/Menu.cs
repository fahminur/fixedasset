﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public class Menu : Entity
    {
        protected Menu()
        {
        }
        public Menu(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string MenuId { get; set; }
        public virtual string MenuName { get; set; }
        public virtual string Application { get; set; }
        public virtual string MenuUrl { get; set; }
        public virtual string MenuIcon { get; set; }
        public virtual int MenuLevel { get; set; }
        public virtual int MenuOrder { get; set; }
        public virtual bool Active { get; set; }
        public virtual IList<Menu> ChildMenu { get; set; }
        public virtual long ParentMenu { get; set; }
        public virtual string FunctionId { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }
}
