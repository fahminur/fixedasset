﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public class Parameter : EntityString
    {
        protected Parameter()
        {

        }
        public Parameter(string id) : base(id)
        {
            this.ParamId = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string ParamId { get; set; }
        public virtual int SessionTimeout { get; set; }
        public virtual int LoginAttempt { get; set; }
        public virtual int MaximumConcurrentLogin { get; set; }
        public virtual string PasswordExp { get; set; }
        public virtual string AccountValidity { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }
}
