﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Treemas.Base.Utilities
{
    [Serializable]
    public abstract class EntityString : EntityString<string>, IEntityString
    {
        public EntityString() : this("") { }
        public EntityString(string id)
            : base(id)
        { }
    }

    [Serializable]
    public abstract class EntityString<TId>
    {
        protected abstract IEnumerable<object>
           GetAttributesToIncludeInEqualityCheck();

        private TId id;
        public EntityString(TId id)
        {
            this.id = id;
        }
        protected static readonly DateTime NullDate = new DateTime(0x76c, 1, 1);
        public virtual TId Id { get { return id; } protected set { id = value; } }

        public virtual int RecordVersion { get; protected set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as EntityString<TId>);
        }

        private static bool isTransient(EntityString<TId> obj)
        {
            return obj != null &&
            Equals(obj.Id, default(TId));
        }

        private Type getUnproxiedType()
        {
            return GetType();
        }

        public virtual bool Equals(EntityString<TId> other)
        {
            if (other == null) return false;

            if (ReferenceEquals(this, other)) return true;

            if (isTransient(this) || isTransient(other) || !Equals(Id, other.Id)) return false;

            var otherType = other.getUnproxiedType();
            var thisType = getUnproxiedType();

            if (!(thisType.IsAssignableFrom(otherType) || otherType.IsAssignableFrom(thisType))) return false;

            return GetAttributesToIncludeInEqualityCheck().SequenceEqual(
           other.GetAttributesToIncludeInEqualityCheck());
        }

        public override int GetHashCode()
        {
            return Equals(Id, default(TId)) ? base.GetHashCode() : Id.GetHashCode();
        }
        public static bool operator ==(EntityString<TId> entity1, EntityString<TId> entity2)
        {
            if ((object)entity1 == null && (object)entity2 == null) { return true; }
            if ((object)entity1 == null || (object)entity2 == null) { return false; }
            return entity1.Id.ToString() == entity2.Id.ToString();
        }

        public static bool operator !=(EntityString<TId> entity1, EntityString<TId> entity2)
        {
            return (!(entity1 == entity2));
        }
        protected abstract void validate();

    }
}