﻿namespace Treemas.Base.Utilities.Pattern
{
    public interface ICommandHandler<TCommand>
    {
        void Execute(TCommand command);
    }
}
