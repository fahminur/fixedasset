﻿namespace Treemas.Base.Utilities
{
    public interface IBootable
    {
        void Boot(object param);
    }
}

