﻿namespace Treemas.Base.Utilities
{
    public interface IRefreshAction : IAction
    {
        void Refresh(object param);
    }
}

