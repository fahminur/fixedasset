﻿
namespace Treemas.Base.Utilities.Queries
{
    public interface IQueryCommandHandler<TQuery, TResult> where TQuery : IQueryCommand<TResult>
    {
        TResult Execute(TQuery query);
    }
}
