﻿namespace Treemas.Base.Utilities
{
    public class ConnectionParam
    {
        public string DBInstance { get;  set; }
        public string DBName { get;  set; }
        public string DBUser { get;  set; }
        public string DBPassword { get;  set; }
    }
}
