﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Utilities
{
    public class JsonDataMap
    {
        public JsonDataMap() : this(null)
        {
        }

        public JsonDataMap(string dataString)
        {
            this.FromString(dataString);
        }

        public void Add<T>(string key, T data)
        {
            this.Add(key, data, typeof(T));
        }

        public void Add(string key, object data, Type type)
        {
            if (this.DataMap.ContainsKey(key))
            {
                this.DataMap[key] = JSON.ToString(data, type);
            }
            else
            {
                this.DataMap.Add(key, JSON.ToString(data, type));
            }
        }

        public void AddRaw(string key, string json)
        {
            if (this.DataMap.ContainsKey(key))
            {
                this.DataMap[key] = json;
            }
            else
            {
                this.DataMap.Add(key, json);
            }
        }

        public void Clear()
        {
            this.DataMap.Clear();
        }

        public void FromString(string dataString)
        {
            if (string.IsNullOrEmpty(dataString))
            {
                this.DataMap = new Dictionary<string, string>();
            }
            else
            {
                this.DataMap = JSON.ToObject<IDictionary<string, string>>(dataString);
            }
        }

        public T Get<T>(string key) => 
            ((T) this.Get(key, typeof(T)));

        public object Get(string key, Type type)
        {
            if (this.DataMap.ContainsKey(key))
            {
                return JSON.ToObject(this.DataMap[key], type);
            }
            return null;
        }

        public string GetRaw(string key)
        {
            if (this.DataMap.ContainsKey(key))
            {
                return this.DataMap[key];
            }
            return null;
        }

        public bool HasKey(string key) => 
            this.DataMap.ContainsKey(key);

        public void Remove(string key)
        {
            this.DataMap.Remove(key);
        }

        public override string ToString() => 
            JSON.ToString<IDictionary<string, string>>(this.DataMap);

        public int Count =>
            this.DataMap.Count;

        protected IDictionary<string, string> DataMap { get; set; }

        public ICollection<string> Keys =>
            this.DataMap.Keys;
    }
}

