﻿namespace Treemas.Base.Utilities
{
    public class NormalizedData
    {
        public string Description { get; set; }

        public string Id { get; set; }

        public bool IsDefault { get; set; }

        public string Name { get; set; }
    }
}

