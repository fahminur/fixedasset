﻿using System;

namespace Treemas.Base.Utilities
{
    public static class NumberExtensions
    {
        public static int IncrementIfAllowed(this int number, bool allowed)
        {
            if (allowed)
            {
                number++;
            }
            return number;
        }

        public static string Terbilang(this double number)
        {
            double AngkaDepan = Math.Floor(number);
            string strAngka = AngkaDepan.ToString();
            int X = 0;
            int Y = 0;
            int Z = 0;
            bool se = false;
            string Urai = "";
            string Bil1 = "";
            string Bil2 = "";
            string strTot;

            while (X < strAngka.Length)
            {
                X += 1;
                strTot = strAngka.Substring(X - 1, 1);
                Y += int.Parse(strTot);
                Z = strAngka.Length - X + 1;

                if (Z==5 || Z==6)
                {
                    if (strTot != "0")
                    {
                        se = true;
                    }
                }

                if (strTot == "1")
                {
                    if (Z == 1 || Z == 7 || Z == 10 || Z == 13)
                    {
                        Bil1 = "satu ";
                    } else
                    {
                        if (Z == 4)
                        {
                            if (se == false)Bil1 = "se";else Bil1 = "satu ";
                        }
                        else
                        {
                            if (Z == 2 || Z == 5 || Z == 8 || Z == 11 || Z == 14)
                            {
                                X += 1;
                                strTot = strAngka.Substring(X - 1, 1);
                                Z = strAngka.Length - X + 1;
                                Bil2 = "";
                                switch (strTot)
                                {
                                    case "0":
                                        Bil1 = "sepuluh ";
                                        break;
                                    case "1":
                                        Bil1 = "sebelas ";
                                        break;
                                    case "2":
                                        Bil1 = "dua belas ";
                                        break;
                                    case "3":
                                        Bil1 = "tiga belas ";
                                        break;
                                    case "4":
                                        Bil1 = "empat belas ";
                                        break;
                                    case "5":
                                        Bil1 = "lima belas ";
                                        break;
                                    case "6":
                                        Bil1 = "enam belas ";
                                        break;
                                    case "7":
                                        Bil1 = "tujuh belas ";
                                        break;
                                    case "8":
                                        Bil1 = "delapan belas ";
                                        break;
                                    case "9":
                                        Bil1 = "sembilan belas ";
                                        break;
                                }
                            }
                            else
                            {
                                Bil1 = "se";
                            }
                        }
                    }
                }
                else
                {
                    switch (strTot)
                    {
                        case "2":
                            Bil1 = "dua ";
                            break;
                        case "3":
                            Bil1 = "tiga ";
                            break;
                        case "4":
                            Bil1 = "empat ";
                            break;
                        case "5":
                            Bil1 = "lima ";
                            break;
                        case "6":
                            Bil1 = "enam ";
                            break;
                        case "7":
                            Bil1 = "tujuh ";
                            break;
                        case "8":
                            Bil1 = "delapan ";
                            break;
                        case "9":
                            Bil1 = "sembilan ";
                            break;
                        default:
                            Bil1 = "";
                            break;

                    }
                }

                if (int.Parse(strTot) > 0)
                {
                    if (Z == 2 || Z == 5 || Z == 8 || Z == 11 || Z == 14)
                        Bil2 = "puluh ";
                    else if (Z == 3 || Z == 6 || Z == 9 || Z == 12 || Z == 15)
                        Bil2 = "ratus ";
                    else
                        Bil2 = "";
                }
                else
                {
                    Bil2 = "";
                }

                if (Y > 0)
                {

                    if (Z == 4)
                    {
                        Bil2 = Bil2 + "ribu ";
                        Y = 0;
                    }
                    else
                    {
                        if (Z == 7)
                        {
                            Bil2 = Bil2 + "juta ";
                            Y = 0;
                        }
                        else
                        {
                            if (Z == 10)
                            {
                                Bil2 = Bil2 + "miliar ";
                                Y = 0;
                            }
                            else
                            {
                                if (Z == 13)
                                {
                                    Bil2 = Bil2 + "trilyun ";
                                    Y = 0;
                                }
                            }
                        }
                    }
                }

                Urai = Urai + Bil1 + Bil2;
            }
            
            if (number - AngkaDepan > 0)
            {
                strAngka = Math.Round((number - AngkaDepan), 2).ToString();
                strAngka = strAngka.Substring(2); //RIGHT(strAngka, LEN(@strAngka) - 2)

                while(strAngka.Substring(1) == "0")
                {
                    strAngka = strAngka.Substring(0, strAngka.Length - 1); //LEFT(@strAngka, LEN(@strAngka) - 1)
                }
                Urai = Urai + "koma ";

                X = 0;
                //WHILE @X < LEN(@strAngka) 
                while (X < strAngka.Length)
                {
                    X += 1;
                    strTot = strAngka.Substring(X - 1, 1); //SUBSTRING(@strAngka, @X, 1)
                    switch (strTot)
                    {
                        case "0":
                            Urai = Urai + "nol ";
                            break;
                        case "1":
                            Urai = Urai + "satu ";
                            break;
                        case "2":
                            Urai = Urai + "dua ";
                            break;
                        case "3":
                            Urai = Urai + "tiga ";
                            break;
                        case "4":
                            Urai = Urai + "empat ";
                            break;
                        case "5":
                            Urai = Urai + "lima ";
                            break;
                        case "6":
                            Urai = Urai + "enam ";
                            break;
                        case "7":
                            Urai = Urai + "tujuh ";
                            break;
                        case "8":
                            Urai = Urai + "delapan ";
                            break;
                        case "9":
                            Urai = Urai + "sembilan ";
                            break;
                    }
                }
            }

            if (Urai.PadLeft(4) == "koma")
                Urai = "nol " + Urai;

            return Urai;
        }
    }
}

