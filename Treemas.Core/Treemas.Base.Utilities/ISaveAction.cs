﻿namespace Treemas.Base.Utilities
{
    public interface ISaveAction : IAction
    {
        void Save(object param);
    }
}

