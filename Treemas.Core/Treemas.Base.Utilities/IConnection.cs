﻿namespace Treemas.Base.Utilities
{
    public interface IConnection
    {
        ConnectionParam GetConnectionSetting();
    }
}
