﻿using System;

namespace Treemas.Base.Utilities
{
    public class SaveAction : BaseAction, ISaveAction, IAction
    {
        public SaveAction(string name, Action<object> action) : base(name)
        {
            this._Action = action;
        }

        public void Save(object param)
        {
            if (this._Action != null)
            {
                this._Action(param);
            }
        }

        protected Action<object> _Action { get; private set; }
    }
}

