﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class RoleFunctionController : PageController
    {
        private IRoleRepository _roleRepository;
        private IFunctionRepository _funcRepository;
        private IRoleFunctionRepository _rolefuncRepository;
        private IFeatureRepository _featRepository;
        private IFunctionFeatureRepository _funcfeatRepository;
        private IRoleFunctionFeatureRepository _rolefuncfeatRepository;
        private IAuditTrailLog _auditRepository;
        public IList<RoleFunction> roleFuncList;
        int i = 0;

        public RoleFunctionController(ISessionAuthentication sessionAuthentication, IRoleRepository roleRepository, 
            IRoleFunctionRepository rolefuncRepository, IFunctionRepository funcRepository,
            IFeatureRepository featRepository, IFunctionFeatureRepository funcfeatRepository
            , IRoleFunctionFeatureRepository rolefuncfeatRepository, IAuditTrailLog auditRepository)
            : base(sessionAuthentication)
        {
            this._roleRepository = roleRepository;
            this._rolefuncRepository = rolefuncRepository;
            this._funcRepository = funcRepository;
            this._featRepository = featRepository;
            this._funcfeatRepository = funcfeatRepository;
            this._rolefuncfeatRepository = rolefuncfeatRepository;
            this._auditRepository = auditRepository;
            Settings.Title = RoleResources.PageTitleRoleFunction;
            Settings.ModuleName = "Role";
        }
        protected override void Startup()
        {
            var returnpage = new { controller = "Role", action = "Index" };
            try
            {
                int Roleid = Convert.ToInt32(Request.RequestContext.RouteData.Values["Id"]);
                Role role = null;
                if (Roleid < 1)
                    Response.RedirectToRoute(returnpage);
                role = _roleRepository.getRole(Roleid);
                if (role.IsNull())
                    Response.RedirectToRoute(returnpage);

                RoleSerializeableView roleView = ConvertFromViewData(role);
                Lookup.Remove<RoleSerializeableView>();
                Lookup.Add(roleView);
                ViewData["Role"] = roleView;
            }
            catch
            {
                Response.RedirectToRoute(returnpage);
            }

        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                RoleSerializeableView roleView = Lookup.Get<RoleSerializeableView>();
                if (roleView.IsNull())
                {
                    if (Request.QueryString.GetValues("Id").IsNullOrEmpty())
                        return null;
                    long Id = long.Parse(Request.QueryString.GetValues("Id")[0]);
                    Role role = _roleRepository.getRole(Id);

                    roleView = ConvertFromViewData(role);
                    Lookup.Remove<RoleSerializeableView>();
                    Lookup.Add(roleView);
                }
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                roleFuncList = _rolefuncRepository.getFunctions(roleView.Id);

                // Loading.   
                PagedResult<AuthorizationFunction> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _funcRepository.FindAllPagedByApp(pageNumber, pageSize, sortColumn, sortColumnDir, roleView.Application);
                }
                else
                {
                    data = _funcRepository.FindAllPagedByApp(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, roleView.Application);
                }

                var items = data.Items.ToList().ConvertAll<RoleFunctionView>(new Converter<AuthorizationFunction, RoleFunctionView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        public RoleFunctionView ConvertFrom(AuthorizationFunction item)
        {
            RoleFunctionView returnItem = new RoleFunctionView();
            var found = roleFuncList.FindElement(x => x.FunctionId == item.Id);
            if (found.IsNull())
            {
                returnItem.selected = false;
            }
            else
            {
                returnItem.selected = true;
                returnItem.Id = found.Id;
                returnItem.RoleId = found.RoleId;
            }
            returnItem.FunctionId = item.Id;
            returnItem.Name = item.Name;
            return returnItem;
        }

        public RoleSerializeableView ConvertFromViewData(Role item)
        {
            RoleSerializeableView returnItem = new RoleSerializeableView();

            returnItem.Id = item.Id;
            returnItem.RoleId = item.RoleId;
            returnItem.Name = item.Name;
            returnItem.Application = item._Application;
            return returnItem;
        }

        public ActionResult Create(RoleFunctionSelectHelper select)
        {
            JsonResult result = new JsonResult();
            User user = Lookup.Get<User>();
            string message = "";
            string messageRFF = "";
            long Id = 0;
            long IdRoleFuncFeat = 0;
            try
            {
                RoleSerializeableView role = Lookup.Get<RoleSerializeableView>();
                
                AuthorizationFunction func = null;
                RoleFunction rolefunc = null;
                IList<FunctionFeature> funcFeatlist = null;
                if (select.selected == true)
                {
                    func = _funcRepository.getFunction(select.FunctionId);
                    Id = Insert(func, role.Id);
                    funcFeatlist = _funcfeatRepository.getFunctionFeatures(select.FunctionId);
                    rolefunc.RoleId = role.Id;
                    rolefunc.FunctionId = select.FunctionId;
                    foreach(var a in funcFeatlist)
                    {
                        IdRoleFuncFeat = InsertRoleFuncFeat(Id,role.Id,select.FunctionId,a.FeatureId);
                    }
                }
                else
                {
                    rolefunc = _rolefuncRepository.getRoleFunc(role.Id, select.FunctionId);
                    messageRFF = DeleteRoleFuncFeat(rolefunc.Id);
                    message = Delete(role.Id, select.FunctionId);

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, rolefunc, user.Username, "Delete");
                }
                
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            result = this.Json(new
            {
                message = message,
                Id = Id
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }

        
        private long Insert(AuthorizationFunction data, long roleId)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                RoleFunction newData = new RoleFunction(0L);
                newData.FunctionId = data.Id;
                newData.RoleId = roleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                _rolefuncRepository.Add(newData);
                returnId = newData.Id;
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
            }
            catch (Exception e)
            {
                returnId = -1;
            }
            return returnId;
        }

        private long InsertRoleFuncFeat(long rfId, long roleId, long funcId, long featId)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                RoleFunctionFeature newData = new RoleFunctionFeature(0L);
                newData.FeatureId = featId;
                newData.RoleFunctionId = rfId;
                newData.FunctionId = funcId;
                newData.RoleId = roleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                _rolefuncfeatRepository.Add(newData);
                returnId = newData.Id;
                
            }
            catch (Exception e)
            {
                returnId = -1;
            }
            return returnId;
        }

        public string Delete(long roleId, long functionId)
        {
            string message = "";
            try
            {
                _rolefuncRepository.deleteSelected(roleId, functionId);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteRoleFuncFeat(long rolefunctionId)
        {
            string message = "";
            try
            {
                _rolefuncfeatRepository.deleteAllbyRoleFuncId(rolefunctionId);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }
    }
}