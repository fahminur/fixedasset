﻿using System;
using System.Linq;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class SiteController : PageController
    {
        private ISiteRepository _siteRepository;
        private IAuditTrailLog _auditRepository;
        public SiteController(ISessionAuthentication sessionAuthentication, ISiteRepository siteRepository,
            IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._siteRepository = siteRepository;
            this._auditRepository = auditRepository;
            Settings.ModuleName = "Site";
            Settings.Title = SiteResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Site> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _siteRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _siteRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<SiteView>(new Converter<Site, SiteView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SiteView ConvertFrom(Site item)
        {
            SiteView returnItem = new SiteView();
            returnItem.SiteId = item.SiteId;
            returnItem.SiteName = item.SiteName;
            returnItem.SiteAddress = item.SiteAddress;
            returnItem.SiteKecamatan = item.SiteKecamatan;
            returnItem.SiteKelurahan = item.SiteKelurahan;
            returnItem.SiteCity = item.SiteCity;
            returnItem.SiteZipCode = item.SiteZipCode;
            returnItem.Status = item.Status;
            return returnItem;
        }

        public SelectListItem ConvertFrom(Application item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.ApplicationId;
            return returnItem;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            SiteView data = new SiteView();
            return CreateView(data);
        }

        private ViewResult CreateView(SiteView data)
        {
            return View("Detail", data);
        }

        // POST: Feature/Create
        [HttpPost]
        public ActionResult Create(SiteView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Site newData = new Site(data.SiteId);
                    newData.SiteName = data.SiteName;
                    newData.SiteAddress = data.SiteAddress;
                    newData.SiteCity = data.SiteCity;
                    newData.SiteKecamatan = data.SiteKecamatan;
                    newData.SiteKelurahan = data.SiteKelurahan;
                    newData.SiteZipCode = data.SiteZipCode;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _siteRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SiteResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(SiteView data, string mode)
        {
            if (data.SiteName == "")
                return SiteResources.Validation_FillSiteName;

            if (data.SiteId == "")
                return SiteResources.Validation_FillSiteId;

            if (mode == "Create")
            {
                if (_siteRepository.IsDuplicate(data.SiteId))
                    return SiteResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Feature/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            Site autoFeat = _siteRepository.getSite(id);
            SiteView data = ConvertFrom(autoFeat);

            return CreateView(data);
        }

        // POST: Feature/Edit/5
        [HttpPost]
        public ActionResult Edit(SiteView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Site newData = _siteRepository.getSite(data.SiteId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.SiteName = data.SiteName;
                    newData.SiteAddress = data.SiteAddress;
                    newData.SiteCity = data.SiteCity;
                    newData.SiteKecamatan = data.SiteKecamatan;
                    newData.SiteKelurahan = data.SiteKelurahan;
                    newData.SiteZipCode = data.SiteZipCode;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _siteRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SiteResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Feature/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Site newData = _siteRepository.getSite(Id);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
                _siteRepository.Remove(newData);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SiteResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}