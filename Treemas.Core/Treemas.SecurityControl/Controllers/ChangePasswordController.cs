﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using System.Text.RegularExpressions;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class ChangePasswordController : PageController
    {
        private IUserRepository _userRepository;
        private IAuditTrailLog _auditRepository;

        public ChangePasswordController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._auditRepository = auditRepository;
            Settings.Title = UserResources.PageTitleChangePassword;
            Settings.ModuleName = "ChangePassword";
        }
        // GET: Home
        protected override void Startup()
        {
            User user = Lookup.Get<User>();

            ViewData["User"] = user;
            ViewData["ActionName"] = "Edit";
        }

        private string validateData(ChangePasswordView data, User user, IList<PasswordHistory> passwordHistory)
        {
            if (data.Password == "")
                return UserResources.Validation_FillPassword;

            if (Encryption.Instance.EncryptText(data.OldPassword) != user.Password)
                return UserResources.Validation_OldPassNotMatch;

            if (data.Password == data.OldPassword)
                return UserResources.Validation_PassNewOld;

            if (data.Password != data.ConfirmPassword)
                return UserResources.Validation_ConfirmPass;

            Regex regex = new Regex("^(?!.*[\\s])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-zA-Z]).{0,}|(?!.*[\\s])(?=.*[0-9])(?=.*[a-zA-Z]).{0,}|(?!.*[\\s])(?=.*[!@#$%^&*])(?=.*[a-zA-Z]).{0,}");
            var match = regex.Match(data.Password).Success;
            if (match == false)
                return UserResources.Validation_Pass;

            string encPassword = Encryption.Instance.EncryptText(data.Password);
            if (passwordHistory.FindIndex(x => x.Password == encPassword) > 0)
            {
                return UserResources.Validation_PasswordHistory;
            }

            return "";
        }

        [HttpPost]
        public ActionResult Edit(ChangePasswordView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            IList<PasswordHistory> passwordHistory = _userRepository.GetPasswordHistory(user.Username);

            string message = "";
            try
            {
                message = validateData(data, user, passwordHistory);
                if (message.IsNullOrEmpty())
                {
                    _userRepository.resetPassword(user.Id, data.Password, DateTime.Now.AddYears(1));
                   
                    PasswordHistory deletedHistory = null;

                    if (!passwordHistory.IsNullOrEmpty())
                    {    
                        if (passwordHistory.Count > 4)
                        {
                            deletedHistory = passwordHistory[0];
                        }
                    }

                    PasswordHistory oldPassHistory = new PasswordHistory();
                    oldPassHistory.Id = 0L;
                    oldPassHistory.Username = user.Username;
                    oldPassHistory.Password = user.Password;
                    oldPassHistory.ChangePasswordDate = DateTime.Now;
                    _userRepository.SavePasswordHistory(oldPassHistory, deletedHistory);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, oldPassHistory, user.Username, "ChangePassword");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_PassSuccess));
                return RedirectToRoute(new { controller = "Login", action = "Logout" });
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();

            return View("ChangePassword");
        }



    }
}