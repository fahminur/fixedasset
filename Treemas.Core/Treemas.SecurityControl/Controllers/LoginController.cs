﻿using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;

namespace Treemas.SecurityControl.Controllers
{
    public class LoginController : LoginPageController
    {
        public LoginController(IUserAccount userAccount, ISessionAuthentication sessionAuthentication,
            ISSOSessionStorage ssoSessionStorage) : base(userAccount, sessionAuthentication, ssoSessionStorage)
        {
        }

        protected override void Startup()
        {
        }


    }
}