﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class UserApplicationRoleController : PageController
    {
        private IUserApplicationRepository _userAppRepository;
        private IRoleFunctionFeatureRepository _roleFuncFeatRepository;
        private IFunctionRepository _funcRepository;
        private IRoleRepository _roleRepository;
        private IFeatureRepository _featRepository;
        private IAuthorizationRepository _authRepository;
        private IList<Authorization> authList;
        private IUserRepository _userRepository;
        private IAuditTrailLog _auditRepository;

        public UserApplicationRoleController(ISessionAuthentication sessionAuthentication, IUserApplicationRepository userAppRepository, IFeatureRepository featRepository, 
            IRoleFunctionFeatureRepository roleFuncFeatRepository, IFunctionRepository funcRepository, IRoleRepository roleRepository, IAuthorizationRepository authRepository,
            IUserRepository userRepository, IAuditTrailLog auditRepository)
            : base(sessionAuthentication)
        {
            this._userAppRepository = userAppRepository;
            this._roleFuncFeatRepository = roleFuncFeatRepository;
            this._funcRepository = funcRepository;
            this._featRepository = featRepository;
            this._roleRepository = roleRepository;
            this._authRepository = authRepository;
            this._userRepository = userRepository;
            this._auditRepository = auditRepository;
            Settings.Title = "UserApplication";
            Settings.ModuleName = UserResources.PageTitle;
        }
        protected override void Startup()
        {
            var returnpage = new { controller = "UserApplication", action = "Index" };
            try
            {
                long rid = Convert.ToInt64(Request.RequestContext.RouteData.Values["Id"]);

                UserApplication userapp = null;
                if (rid.IsNull())
                    Response.RedirectToRoute(returnpage);
                if (rid<0)
                    Response.RedirectToRoute(returnpage);

                userapp = _userAppRepository.getUserApplication(rid);
                UserApplicationSerializeabileView userappView = ConvertFromViewData(userapp);
                if (userapp.IsNull())
                    Response.RedirectToRoute(returnpage);
                

                Lookup.Remove<UserApplicationSerializeabileView>();
                Lookup.Add(userappView);
                ViewData["UserApp"] = userappView;

            }
            catch
            {
                Response.RedirectToRoute(returnpage);
            }

        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                UserApplicationSerializeabileView userappView = Lookup.Get<UserApplicationSerializeabileView>();
                if (userappView.IsNull())
                {
                    if (Request.QueryString.GetValues("Id").IsNullOrEmpty())
                        return null;
                    long Id = long.Parse(Request.QueryString.GetValues("Id")[0]);
                    UserApplication userapp = _userAppRepository.getUserApplication(Id);
                    userappView = ConvertFromViewData(userapp);
                    Lookup.Remove<UserApplicationSerializeabileView>();
                    Lookup.Add(userappView);
                }

                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                authList = _authRepository.getUserAuthorization(userappView.Username);

                // Loading.   
                PagedResult<Role> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _roleRepository.FindAllPagedUserAppRole(pageNumber, pageSize, sortColumn, sortColumnDir, userappView.Application);
                }
                else
                {
                    data = _roleRepository.FindAllPagedUserAppRole(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, userappView.Application);
                }

                var items = data.Items.ToList().ConvertAll<AuthorizationView>(new Converter<Role, AuthorizationView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        public AuthorizationView ConvertFrom(Role item)
        {
            AuthorizationView returnItem = new AuthorizationView();
            int index = authList.FindIndex(x => x.Role.Equals(item.RoleId));
            if (index == -1)
            {
                returnItem.Selected = false;
                returnItem.Id = item.Id;
            }
            else
            {
                returnItem.Id = item.Id;
                returnItem.Selected = true;
            }
            returnItem.Role = item.RoleId;
            
            return returnItem;
        }

        public UserApplicationSerializeabileView ConvertFromViewData(UserApplication item)
        {
            UserApplicationSerializeabileView returnItem = new UserApplicationSerializeabileView();

            returnItem.Id = item.Id;
            returnItem.Application = item.Application;
            returnItem.Username = item.Username;
            returnItem.UserApplicationId = item.Id;
            return returnItem;
        }

        [HttpPost]
        public ActionResult Create(AuthorizationHelper data)
        {
            User user = Lookup.Get<User>();
            JsonResult result = new JsonResult();
            try
            {

                if (data.Selected == true)
                {
                    Insert(data);
                }
                else
                {
                    Delete(data);
                }
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        [HttpPost]
        public ActionResult Insert(AuthorizationHelper data)
        {
            User user = Lookup.Get<User>();
            UserApplicationSerializeabileView userappView = Lookup.Get<UserApplicationSerializeabileView>();
            Role role = _roleRepository.getRole(data.RoleId);

            IList<RoleFunctionFeature> roleFuncFeat = _roleFuncFeatRepository.getRoleFuncFeaturebyRole(data.RoleId);

            foreach(var item in roleFuncFeat)
            {
                AuthorizationFeature feat = _featRepository.getFeature(item.FeatureId);
                AuthorizationFunction func = _funcRepository.getFunction(item.FunctionId);
                try
                {
                    Authorization newData = new Authorization(0L);
                    newData.Username = userappView.Username;
                    newData.Application = userappView.Application;
                    newData.Role = role.RoleId;
                    newData.Function = func.FunctionId;
                    newData.Feature = feat.Name;
                    newData.QualifierKey = "";
                    newData.QualifierValue = "";
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    _authRepository.Add(newData);
                    _userRepository.setUpdateMenu(userappView.Username, true);
                }
                catch (Exception e)
                {

                }
            }
            _auditRepository.SaveAuditTrail(Settings.ModuleName, new {user.Username, role.RoleId }, user.Username, "Create");
            return RedirectToAction("Search");
        }

        [HttpPost]
        public ActionResult Delete(AuthorizationHelper data)
        {
            User user = Lookup.Get<User>();
            Role role = _roleRepository.getRole(data.RoleId);
            UserApplicationSerializeabileView userappView = Lookup.Get<UserApplicationSerializeabileView>();
            string message = "";
            try
            {
                _authRepository.delete(userappView.Username, userappView.Application, role.RoleId);
                _userRepository.setUpdateMenu(userappView.Username, true);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, new { userappView.Username, role.RoleId }, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            
            return RedirectToAction("Search");
        }

    }
}