﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class RoleFunctionFeatureController : PageController
    {
        private IRoleRepository _roleRepository;
        private IFunctionRepository _funcRepository;
        private IRoleFunctionRepository _rolefuncRepository;
        private IFeatureRepository _featRepository;
        private IRoleFunctionFeatureRepository _rolefuncfeatRepository;
        private IFunctionFeatureRepository _funcfeatRepository;
        private IAuditTrailLog _auditRepository;
        public IList<RoleFunctionFeature> roleFuncFeatList;

        public RoleFunctionFeatureController(ISessionAuthentication sessionAuthentication, IRoleRepository roleRepository,
            IRoleFunctionRepository rolefuncRepository, IFunctionRepository funcRepository, IFeatureRepository featRepository,
            IRoleFunctionFeatureRepository rolefuncfeatRepository, IFunctionFeatureRepository funcfeatRepository, IAuditTrailLog auditRepository)
            : base(sessionAuthentication)
        {
            this._roleRepository = roleRepository;
            this._rolefuncRepository = rolefuncRepository;
            this._funcRepository = funcRepository;
            this._featRepository = featRepository;
            this._rolefuncfeatRepository = rolefuncfeatRepository;
            this._funcfeatRepository = funcfeatRepository;
            this._auditRepository = auditRepository;
            Settings.Title = RoleResources.PageTitleRoleFunctionFeature;
            Settings.ModuleName = "Role Function";
        }
        protected override void Startup()
        {
            var returnpage = new { controller = "RoleFunction", action = "Index" };
            try
            {
                int RoleFuncid = Convert.ToInt32(Request.RequestContext.RouteData.Values["Id"]);
                RoleFunction roleFunc = null;
                AuthorizationFunction Func = null;
                if (RoleFuncid < 1)
                    Response.RedirectToRoute(returnpage);
                roleFunc = _rolefuncRepository.getRoleFunction(RoleFuncid);

                Func = _funcRepository.getFunction(roleFunc.FunctionId);
                if (roleFunc.IsNull())
                    Response.RedirectToRoute(returnpage);

                RoleFunctionSerializeableView roleFuncView = ConvertFromViewDataRoleFunc(roleFunc);
                AuthorizationFunctionView funcView = ConvertFromViewDataFunc(Func);

                Lookup.Remove<RoleFunctionSerializeableView>();
                Lookup.Add(roleFuncView);
                ViewData["RoleFunction"] = roleFuncView;
                ViewData["Function"] = funcView;
                Role role = _roleRepository.getRole(roleFuncView.RoleId);
            }
            catch
            {
                Response.RedirectToRoute(returnpage);
            }

        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                RoleFunctionSerializeableView roleFuncView = Lookup.Get<RoleFunctionSerializeableView>();
                if (roleFuncView.IsNull())
                {

                    if (Request.QueryString.GetValues("Id").IsNullOrEmpty())
                        return null;
                    long Id = long.Parse(Request.QueryString.GetValues("Id")[0]);
                    RoleFunction roleFunc = _rolefuncRepository.getRoleFunction(Id);

                    roleFuncView = ConvertFromViewDataRoleFunc(roleFunc);
                    Lookup.Remove<RoleFunctionSerializeableView>();
                    Lookup.Add(roleFunc);
                }

                Role role = _roleRepository.getRole(roleFuncView.RoleId);
                // Initialization.   
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                roleFuncFeatList = _rolefuncfeatRepository.getRoleFuncFeatures(roleFuncView.Id);

                // Loading.   
                PagedResult<FunctionFeature> data;

                data = _funcfeatRepository.FindAllPagedbyFunction(roleFuncView.FunctionId, pageNumber, sortColumn, pageSize, sortColumnDir);

                var items = data.Items.ToList().ConvertAll<RoleFunctionFeatureView>(new Converter<FunctionFeature, RoleFunctionFeatureView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        public RoleFunctionFeatureView ConvertFrom(FunctionFeature item)
        {
            RoleFunctionSerializeableView roleFunc = Lookup.Get<RoleFunctionSerializeableView>();
            AuthorizationFeature feat = null;
            feat = _featRepository.getFeature(item.FeatureId);
            RoleFunctionFeatureView returnItem = new RoleFunctionFeatureView();
            var found = roleFuncFeatList.FindElement(x => x.FeatureId == item.FeatureId);
            if (found.IsNull())
            {
                returnItem.selected = false;
            }
            else
            {
                returnItem.selected = true;
                returnItem.Id = found.Id;
                returnItem.RoleId = found.RoleId;
                returnItem.FunctionId = found.FunctionId;
                returnItem.RoleFunctionId = found.RoleFunctionId;
            }
            returnItem.FeatureId = item.FeatureId;
            returnItem.Name = feat.Name;
            return returnItem;
        }

        public RoleFunctionSerializeableView ConvertFromViewDataRoleFunc(RoleFunction item)
        {
            RoleFunctionSerializeableView returnItem = new RoleFunctionSerializeableView();

            returnItem.Id = item.Id;
            returnItem.RoleId = item.RoleId;
            returnItem.FunctionId = item.FunctionId;
            return returnItem;
        }

        public AuthorizationFunctionView ConvertFromViewDataFunc(AuthorizationFunction item)
        {
            AuthorizationFunctionView returnItem = new AuthorizationFunctionView();

            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            returnItem.FunctionId = item.FunctionId;
            return returnItem;
        }

        public ActionResult Create(RoleFunctionFeatSelectHelper select)
        {
            JsonResult result = new JsonResult();
            string message = "";
            long Id = 0;
            try
            {
                RoleFunctionSerializeableView roleFunc = Lookup.Get<RoleFunctionSerializeableView>();
                
                if (select.selected == true)
                {
                    Id = Insert(select.FeatureId, roleFunc);
                }
                else
                {
                    message = Delete(roleFunc.Id, select.FeatureId);
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            result = this.Json(new
            {
                message = message,
                Id = Id
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }


        private long Insert(long featureId, RoleFunctionSerializeableView roleFunc)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                RoleFunctionFeature newData = new RoleFunctionFeature(0L);
                newData.FeatureId = featureId;
                newData.RoleFunctionId = roleFunc.Id;
                newData.FunctionId = roleFunc.FunctionId;
                newData.RoleId = roleFunc.RoleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                _rolefuncfeatRepository.Add(newData);
                returnId = newData.Id;
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
            }
            catch (Exception e)
            {
                returnId = -1;
            }
            return returnId;
        }

        public string Delete(long roleFuncId, long featureId)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                _rolefuncfeatRepository.deleteSelected(roleFuncId, featureId);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, new { roleFuncId, featureId}, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }
    }
}