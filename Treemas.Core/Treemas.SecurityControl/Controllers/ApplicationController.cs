﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class ApplicationController : PageController
    {
        private IApplicationRepository _appRepository;
        private IAuditTrailLog _auditRepository;
        public ApplicationController(ISessionAuthentication sessionAuthentication,
            IApplicationRepository appRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            Settings.ModuleName = "Application";
            Settings.Title = ApplicationResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Application> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _appRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _appRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<ApplicationView>(new Converter<Application, ApplicationView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public ApplicationView ConvertFrom(Application item)
        {
            ApplicationView returnItem = new ApplicationView();
            returnItem.ApplicationId = item.ApplicationId;
            returnItem.Type = item.Type;
            returnItem.Runtime = item.Runtime;
            returnItem.Description = item.Description;
            returnItem.CSSColor = item.CSSColor;
            returnItem.Icon = item.Icon;
            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            return returnItem;
        }
        
        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            ApplicationView data = new ApplicationView();
            return CreateView(data);
        }

        private ViewResult CreateView(ApplicationView data)
        {
            return View("Detail", data);
        }
        
        // POST: application/Create
        [HttpPost]
        public ActionResult Create(ApplicationView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Application newData = new Application(0L);
                    newData.ApplicationId = data.ApplicationId;
                    newData.Name = data.Name;
                    newData.Type = data.Type;
                    newData.Runtime = data.Runtime;
                    newData.Description = data.Description;
                    newData.CSSColor = data.CSSColor;
                    newData.Icon = data.Icon;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    _appRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(ApplicationResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(ApplicationView data, string mode)
        {
            if (data.ApplicationId == "")
                return ApplicationResources.Validation_FillApplicationId;

            if (data.Name == "")
                return ApplicationResources.Validation_FillApplicationName;

            if (data.Type == "")
                return ApplicationResources.Validation_FillApplicationType;

            if (data.Runtime == "")
                return ApplicationResources.Validation_FillApplicationRuntime;

            if (mode == "Create")
            {
                if (_appRepository.IsDuplicate(data.ApplicationId))
                    return ApplicationResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: application/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            Application autoFeat = _appRepository.getApplication(id);
            ApplicationView data = ConvertFrom(autoFeat);

            return CreateView(data);
        }

        // POST: application/Edit/5
        [HttpPost]
        public ActionResult Edit(ApplicationView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Application newData = new Application(data.Id);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.ApplicationId = data.ApplicationId;
                    newData.Name = data.Name;
                    newData.Type = data.Type;
                    newData.Runtime = data.Runtime;
                    newData.Description = data.Description;
                    newData.CSSColor = data.CSSColor;
                    newData.Icon = data.Icon;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _appRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(ApplicationResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: application/Delete/5
        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Application newData = new Application(Id);
                _appRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(ApplicationResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}