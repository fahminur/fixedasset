﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class MenuController : PageController
    {
        private IMenuRepository _menuRepository;
        private IFunctionRepository _funcRepository;
        private IApplicationRepository _appRepository;
        private IUserRepository _userRepository;
        private IAuditTrailLog _auditRepository;
        public MenuController(ISessionAuthentication sessionAuthentication, IMenuRepository menuRepository,
            IFunctionRepository funcRepository, IUserRepository userRepository,
            IApplicationRepository appRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._menuRepository = menuRepository;
            this._funcRepository = funcRepository;
            this._appRepository = appRepository;
            this._userRepository = userRepository;
            this._auditRepository = auditRepository;
            Settings.ModuleName = "Menu";
            Settings.Title = MenuResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Menu> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _menuRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _menuRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<MenuView>(new Converter<Menu, MenuView>(ConvertFrom));
            
                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public MenuView ConvertFrom(Menu item)
        {
            MenuView returnItem = new MenuView();
            string spaces = createSpaces(item.MenuLevel);                
            returnItem.Id = item.Id;
            returnItem.MenuId = spaces + item.MenuId;
            returnItem.MenuName = spaces + item.MenuName;
            returnItem.ParentMenu = item.ParentMenu;
            returnItem.FunctionId = item.FunctionId;
            returnItem.MenuUrl = item.MenuUrl;
            returnItem.MenuLevel = item.MenuLevel;
            returnItem.MenuOrder = item.MenuOrder;
            returnItem.MenuIcon = item.MenuIcon;
            returnItem.Active = item.Active;
            returnItem.Application = item.Application;
            return returnItem;
        }

        public MenuView ConvertFromNoSpaces(Menu item)
        {
            MenuView returnItem = new MenuView();
            returnItem.Id = item.Id;
            returnItem.MenuId = item.MenuId;
            returnItem.MenuName = item.MenuName;
            returnItem.ParentMenu = item.ParentMenu;
            returnItem.FunctionId = item.FunctionId;
            returnItem.MenuUrl = item.MenuUrl;
            returnItem.MenuLevel = item.MenuLevel;
            returnItem.MenuOrder = item.MenuOrder;
            returnItem.MenuIcon = item.MenuIcon;
            returnItem.Active = item.Active;
            returnItem.Application = item.Application;
            return returnItem;
        }

        private string createSpaces(int level)
        {
            string spaces = "";
            for (int i = 0; i < level; i++ )
            {
                spaces += "&nbsp;&nbsp;&nbsp;";
            }
            return spaces;
        }

        public SelectListItem ConvertFrom(Application item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.ApplicationId;
            return returnItem;
        }

        public SelectListItem ConvertFromMenu(Menu item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.MenuName;
            returnItem.Value = item.Id.ToString();
            return returnItem;
        }

        public SelectListItem ConvertFrom(AuthorizationFunction item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.FunctionId;
            return returnItem;
        }

        

        private ViewResult CreateView(MenuView data)
        {
            ViewData["ApplicationMaster"] = createApplicationSelect(data.Application);
            //ViewData["ParentMenuList"] = createParentMenuSelect(data.ParentMenu);
            ViewData["FunctionList"] = createFunctionSelect(data.FunctionId);
            if ((string)ViewData["ActionName"] != "Create")
            {

                if (data.ParentMenu > 0)
                {
                    ViewData["ParentMenu"] = _menuRepository.GetRootMenuNameByParentMenuId(data.ParentMenu);
                }
                else
                {
                    ViewData["ParentMenu"] = new Menu(0L);
                }
            }

            return View("Detail", data);
        }

        private IList<SelectListItem> createApplicationSelect(string selected)
        {
            IList<SelectListItem> dataList = _appRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Application, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createParentMenuSelect(long selected)
        {
            //  IList<Menu> menuSpecific = _menuRepository.FindOnlyParentMenu(selected);
            IList<SelectListItem> dataList = _menuRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Menu, SelectListItem>(ConvertFromMenu));
            dataList.Insert(0, new SelectListItem { Text = "", Value = "" });
            if (selected > 0)
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createFunctionSelect(string selected)
        {
            IList<SelectListItem> dataList = _funcRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<AuthorizationFunction, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem { Text = "", Value = "" });
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            MenuView data = new MenuView();
            return CreateView(data);
        }

        // POST: Function/Create
        [HttpPost]
        public ActionResult Create(MenuView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create","");
                if (message.IsNullOrEmpty())
                {
                    Menu newData = new Menu(0L);
                    newData.MenuId = data.MenuId;
                    newData.MenuName = data.MenuName;
                    newData.ParentMenu = data.ParentMenu;
                    newData.Application = data.Application;
                    newData.FunctionId = data.FunctionId;
                    newData.MenuUrl = data.MenuUrl;
                    newData.MenuOrder = data.MenuOrder;
                    newData.MenuIcon = data.MenuIcon;
                    newData.Active = data.Active;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    _menuRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                    _userRepository.setUpdateMenu(true);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(FunctionResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(MenuView data, string mode, string joinId)
        {
            if (data.MenuId == "")
                return MenuResources.Validation_SelectApplication;

            if (data.MenuName == "")
                return MenuResources.Validation_FillMenuName;

            if (data.FunctionId == "")
                return MenuResources.Validation_FillFunction;

            if (mode == "Create")
            {
                if (_menuRepository.IsDuplicate(data.Application, data.MenuId))
                    return MenuResources.Validation_DuplicateData;
            }

            if (mode == "Add")
            {
                if (_menuRepository.IsDuplicateForAdd(data.Application, joinId))
                    return MenuResources.Validation_DuplicateData;
            }


            return "";
        }


        // GET: Function/Add/5
        public ActionResult Add(long id)
        {
            ViewData["ActionName"] = "Add";
            Menu autoFunc = _menuRepository.getForEditMode(id);
            ViewData["ParentMenu"] = autoFunc.MenuId;
            ViewData["Application"] = autoFunc.Application;

            MenuView data = new MenuView();
            data.ParentMenu = autoFunc.Id;
            data.Application = autoFunc.Application;

            return CreateView(data);
        }

        // POST: Function/Add/5
        [HttpPost]
        public ActionResult Add(MenuView data)
        {
            ViewData["ActionName"] = "Add";
            User user = Lookup.Get<User>();
            Menu pMenu = _menuRepository.getForEditMode(data.Id);
            string message = "";
            //string Application = ;
            string joinId = pMenu.MenuId + "." + data.MenuId;
            var count = joinId.Count(f => f == '.');
            try
            {
                message = validateData(data, "Add",joinId);
                if (message.IsNullOrEmpty())
                {
                    Menu newData = new Menu(0L);
                    newData.Application = data.Application;
                    newData.ParentMenu = pMenu.Id;
                    newData.MenuId = joinId;
                    newData.MenuName = data.MenuName;
                    newData.FunctionId = data.FunctionId;
                    newData.MenuUrl = data.MenuUrl;
                    newData.MenuLevel = count;
                    newData.MenuOrder = data.MenuOrder;
                    newData.MenuIcon = data.MenuIcon;
                    newData.Active = data.Active;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    _menuRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Add");
                    _userRepository.setUpdateMenu(true);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(MenuResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        ///

        // GET: Function/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            Menu autoFunc = _menuRepository.getForEditMode(id);
            MenuView data = ConvertFromNoSpaces(autoFunc);
            ViewData["ParentMenu"] = data.ParentMenu;
            //ViewData["Application"] = data.Application;
            return CreateView(data);
        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Edit(MenuView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            string parentMenuId = Request.Form["MenuId"];
            try
            {
                message = validateData(data, "Edit","");
                if (message.IsNullOrEmpty())
                {
                    Menu newData  = _menuRepository.getForEditMode(data.Id);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");
                    newData.MenuId = data.MenuId;
                    newData.MenuName = data.MenuName;
                    newData.ParentMenu = data.ParentMenu;
                    newData.Application = data.Application;
                    newData.FunctionId = data.FunctionId;
                    newData.MenuUrl = data.MenuUrl;
                    newData.MenuOrder = data.MenuOrder;
                    newData.MenuIcon = data.MenuIcon;
                    newData.Active = data.Active;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    _menuRepository.Save(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _userRepository.setUpdateMenu(true);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(MenuResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        // POST: Function/Delete/5
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Menu newData = new Menu(Id);
                _menuRepository.Remove(newData);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
                _userRepository.setUpdateMenu(true);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(MenuResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}