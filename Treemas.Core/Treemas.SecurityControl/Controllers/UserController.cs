﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using System.IO;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class UserController : PageController
    {
        private IUserRepository _userRepository;
        private IParameterRepository _parameterRepository;
        private IApplicationRepository _appRepository;
        private IAuditTrailLog _auditRepository;
        public UserController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IApplicationRepository appRepository, IAuditTrailLog auditRepository, IParameterRepository parameterRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._parameterRepository = parameterRepository;
            Settings.ModuleName = "User";
            Settings.Title = UserResources.PageTitle;
        }

        protected override void Startup()
        {
            long pid = Convert.ToInt64(Request.RequestContext.RouteData.Values["Id"]);
            if (pid > 0)
            {
                User printUser = _userRepository.GetUserForPrint(pid);
                ViewData["PrintId"] = printUser.Id;
                if(printUser.IsPrinted == true)
                {
                    ViewData["PrintStatus"] = (long)1;
                }
                else
                {
                    ViewData["PrintStatus"] = (long)0;
                }
            }
            else
            {
                ViewData["PrintId"] = (long)0;
                ViewData["PrintStatus"] = (long)1;
            }
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<User> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _userRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    if (searchColumn.ToLower().Contains("date"))
                    {
                        DateTime searchDate = DateTime.ParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data = _userRepository.FindAllPagedDate(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchDate);
                    }
                    else
                    {
                        data = _userRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                    }

                }

                var items = data.Items.ToList().ConvertAll<UserView>(new Converter<User, UserView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public UserView ConvertFrom(User item)
        {
            UserView returnItem = new UserView();
            returnItem.Id = item.Id;
            returnItem.Username = item.Username;
            returnItem.Password = Encryption.Instance.DecryptText(item.Password);
            returnItem.PasswordExpirationDate = item.PasswordExpirationDate.ToString("dd/MM/yyyy");
            returnItem.AccountValidityDate = item.AccountValidityDate.ToString("dd/MM/yyyy");
            returnItem.FirstName = item.FirstName;
            returnItem.LastName = item.LastName;
            returnItem.RegNo = item.RegNo;
            returnItem.SessionTimeout = SystemSettings.Instance.Security.SessionTimeout; //item.SessionTimeout;
            returnItem.LockTimeout = 10;//item.LockTimeout;
            returnItem.MaxConLogin = 1; //item.MaximumConcurrentLogin;
            returnItem.InActiveDirectory = item.InActiveDirectory;
            returnItem.Name = item.Name;
            returnItem.IsActive = item.IsActive;
            returnItem.IsPrinted = item.IsPrinted;
            returnItem.Approved = item.Approved;
            
            return returnItem;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            UserView data = new UserView();

            Parameter Param = _parameterRepository.getParameter();
            data.AccountValidityDate = Param.AccountValidity;
            data.PasswordExpirationDate = Param.PasswordExp;

            return CreateView(data);
        }

        private ViewResult CreateView(UserView data)
        {
            return View("Detail", data);
        }


        // POST: Feature/Create
        [HttpPost]
        public ActionResult Create(UserView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Parameter Param = _parameterRepository.getParameter();

            string message = "";
            long printId = 0;
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    User newData = new User(0L);
                    newData.Username = data.Username;
                    newData.Password = Encryption.Instance.EncryptText(data.Password);
                    newData.PasswordExpirationDate = DateTime.Now.AddDays(double.Parse(Param.PasswordExp));
                    newData.AccountValidityDate = DateTime.Now.AddDays(double.Parse(Param.AccountValidity));
                    newData.FirstName = data.FirstName;
                    newData.LastName = data.LastName;
                    newData.RegNo = data.RegNo;
                    newData.SessionTimeout = SystemSettings.Instance.Security.SessionTimeout; //item.SessionTimeout;
                    newData.LockTimeout = 10; //item.LockTimeout;
                    newData.MaximumConcurrentLogin = 1; //item.MaximumConcurrentLogin;
                    newData.IsActive = false;
                    newData.InActiveDirectory = data.InActiveDirectory;
                    newData.IsPrinted = false;
                    newData.Approved = false;

                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    _userRepository.Add(newData);

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");

                    printId = newData.Id;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(UserView data, string mode)
        {
            if (mode != "Reset")
            {
                if (data.Username == "")
                    return UserResources.Validation_FillUsername;

                //if (data.RegNo.IsNull())
                //    return UserResources.Validation_FillRegNo;

                if (data.FirstName.IsNull())
                    return UserResources.Validation_FillFirstName;

                if (data.LastName.IsNull())
                    return UserResources.Validation_FillLastName;

                if (data.SessionTimeout.IsNull())
                    return UserResources.Validation_FillSessionTimeout;

                if (data.LockTimeout.IsNull())
                    return UserResources.Validation_FillLockTimeout;

                if (data.LastName.IsNull())
                    return UserResources.Validation_FillMaxConLogin;
            }

            if (mode != "Edit")
            {
                if (data.Password == "")
                    return UserResources.Validation_FillPassword;

                if (data.Password != data.ConfirmPassword)
                    return UserResources.Validation_ConfirmPass;

                Regex regex = new Regex("^(?!.*[\\s])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-zA-Z]).{0,}|(?!.*[\\s])(?=.*[0-9])(?=.*[a-zA-Z]).{0,}|(?!.*[\\s])(?=.*[!@#$%^&*])(?=.*[a-zA-Z]).{0,}");
                var match = regex.Match(data.Password).Success;
                if (match == false)
                    return UserResources.Validation_Pass;
            }

            if (mode == "Create")
            {
                if (_userRepository.IsDuplicate(data.Username))
                    return UserResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Feature/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            User user = _userRepository.GetUserById(id);
            UserView data = ConvertFrom(user);

            return CreateView(data);
        }

        // POST: Feature/Edit/5
        [HttpPost]
        public ActionResult Edit(UserView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    User newData = _userRepository.GetUserById(data.Id);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");
                    
                    newData.FirstName = data.FirstName;
                    newData.LastName = data.LastName;
                    newData.RegNo = data.RegNo;
                    newData.SessionTimeout = SystemSettings.Instance.Security.SessionTimeout; //item.SessionTimeout;
                    newData.LockTimeout = 10;//item.LockTimeout;
                    newData.MaximumConcurrentLogin = 1; //item.MaximumConcurrentLogin;
                    newData.InActiveDirectory = data.InActiveDirectory;

                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");

                    _userRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public ActionResult Reset(long id)
        {
            ViewData["ActionName"] = "Reset";
            User userdata = _userRepository.GetUserById(id);
            UserView data = ConvertFrom(userdata);
            return ResetView(data);
        }

        private ViewResult ResetView(UserView data)
        {
            return View("Reset", data);
        }

        [HttpPost]
        public ActionResult Reset(UserView data)
        {
            ViewData["ActionName"] = "Reset";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Reset");
                if (message.IsNullOrEmpty())
                {
                    User newData = _userRepository.GetUserById(data.Id);
                    _userRepository.resetPassword(data.Id, data.Password);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "ResetPassword");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_Reset));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return ResetView(data);
        }

        // POST: Feature/Delete/5
        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                User userdata = _userRepository.GetUserById(Id);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, userdata, user.Username, "Delete");
                _userRepository.deleteAll(userdata.Username);
                _userRepository.Remove(userdata);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

        public ActionResult Activate(long id)
        {
            ViewData["ActionName"] = "Activate";
            User user = _userRepository.GetUserById(id);
            UserView data = ConvertFrom(user);
            ViewData["UserData"] = data;

            return View("Activate", data);
        }

        [HttpPost]
        public ActionResult Activate(UserView data)
        {
            ViewData["ActionName"] = "Activate";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (message.IsNullOrEmpty())
                {
                    Parameter Param = _parameterRepository.getParameter();
                    DateTime passexpdate = DateTime.Now.AddDays(double.Parse(Param.PasswordExp));
                    DateTime accvaldate = DateTime.Now.AddDays(double.Parse(Param.AccountValidity));

                    User userdata = _userRepository.GetUserById(data.Id);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, userdata, user.Username, "Activate");
                    _userRepository.setActive(data.Id, passexpdate , accvaldate);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_SuccessActivate));
                return RedirectToRoute(new { controller = "User", action = "Index" });
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return RedirectToRoute(new { controller = "User", action = "Index" });
        }

        //public ActionResult Print(long id)
        //{
        //    LocalReport lr = new LocalReport();
        //    string path = Path.Combine(Server.MapPath("~/Reports/PasswordPrint"), "PasswordPrint.rdlc");
        //    if (System.IO.File.Exists(path))
        //    {
        //        lr.ReportPath = path;
        //    }
        //    else
        //    {
        //        return View("Index");
        //    }

        //    User user = _userRepository.GetUserForPrint(id);
        //    if (user.IsNull())
        //    {
        //        ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_Print));
        //        return RedirectToRoute(new { controller = "User", action = "Index" });
        //    }
        //    user.Password = Encryption.Instance.DecryptText(user.Password);

        //    List<User> dataModelList = new List<User>();
        //    dataModelList.Add(user);
        //    //IEnumerable<User> dataModelList = (IEnumerable<User>)user;

        //    ReportDataSource rd = new ReportDataSource("PasswordPrint", dataModelList);
        //    lr.DataSources.Add(rd);
        //    string reportType = "pdf";
        //    string mimeType;
        //    string encoding;
        //    string fileNameExtension;

        //    string deviceInfo =

        //    "<DeviceInfo>" +
        //    "  <OutputFormat>pdf</OutputFormat>" +
        //    "  <PageWidth>4in</PageWidth>" +
        //    "  <PageHeight>2in</PageHeight>" +
        //    "  <MarginTop>1cm</MarginTop>" +
        //    "  <MarginLeft>1cm</MarginLeft>" +
        //    "  <MarginRight>1cm</MarginRight>" +
        //    "  <MarginBottom>1cm</MarginBottom>" +
        //    "</DeviceInfo>";

        //    Warning[] warnings;
        //    string[] streams;
        //    byte[] renderedBytes;

        //    renderedBytes = lr.Render(
        //        reportType,
        //        deviceInfo,
        //        out mimeType,
        //        out encoding,
        //        out fileNameExtension,
        //        out streams,
        //        out warnings);

        //    _userRepository.setPrinted(id);
        //    ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_Print));
        //    return File(renderedBytes, mimeType);
        //}

    }
}