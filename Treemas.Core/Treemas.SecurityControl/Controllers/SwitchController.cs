﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace Treemas.SecurityControl.Controllers
{
    public class SwitchController : PageController
    {
        private IUserApplicationRepository _userAppRepository;
        private IApplicationRepository _appRepository;
        public SwitchController(ISessionAuthentication sessionAuthentication, IUserApplicationRepository userAppRepository,
            IApplicationRepository appRepository) : base(sessionAuthentication)
        {
            this._userAppRepository = userAppRepository;
            this._appRepository = appRepository;
        }
        protected override void Startup()
        {
            base.Settings.Title = "Switch Application";
            User user = this.Lookup.Get<User>();

            IList<UserApplication> userApp = _userAppRepository.getUserApplication(user.Username);
            IList<string> listIds = new List<string>();

            foreach (UserApplication model in userApp)
            {
                listIds.Add(model.Application);
            }

            ViewData["Applications"] = _appRepository.getApplications(listIds);
        }
    }
}
