﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.SecurityControl.Resources;

namespace Treemas.SecurityControl.Controllers
{
    public class ParameterController : PageController
    {
        private IParameterRepository _parameterRepository;
        private IAuditTrailLog _auditRepository;
        public ParameterController(ISessionAuthentication sessionAuthentication, IParameterRepository parameterRepository, IAuditTrailLog auditRepository) 
            :base(sessionAuthentication)
        {
            this._parameterRepository = parameterRepository;
            this._auditRepository = auditRepository;
            Settings.Title = ParameterResources.PageTitle;

        }
        protected override void Startup()
        {
            Parameter param = _parameterRepository.getParameter();
            ViewData["Parameter"] = param;
        }

        private string validateData(ParameterView data, string mode)
        {

            return "";
        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Edit(ParameterView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {

                    Parameter newData = new Parameter(data.Id);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.SessionTimeout = (data.SessionTimeout <= 0) ? 30 : data.SessionTimeout;
                    newData.LoginAttempt= (data.LoginAttempt <= 0) ? 3 : data.LoginAttempt;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.PasswordExp = data.PasswordExp;
                    newData.AccountValidity = data.AccountValidity;
                    _parameterRepository.Save(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    ViewData["Parameter"] = newData;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(ParameterResources.Validation_Success));
                return RedirectToRoute(new { controller = "Parameter", action="Index" });

            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);

        }
        
        private ViewResult CreateView(ParameterView data)
        {
            Parameter param = _parameterRepository.getParameter();
           
            ViewData["Parameter"] = param;
            return View("Parameter", data);
        }

    }
}