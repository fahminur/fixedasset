﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class UserApplicationController : PageController
    {
        private IUserRepository _userRepository;
        private IUserApplicationRepository _userAppRepository;
        private IApplicationRepository _appRepository;
        private IList<UserApplication> userAppList;
        private IAuditTrailLog _auditRepository;

        public UserApplicationController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IUserApplicationRepository userAppRepository, IApplicationRepository appRepository, IAuditTrailLog auditRepository)
            : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._userAppRepository = userAppRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            Settings.Title = "UserApplication";
            Settings.ModuleName = UserResources.PageTitle;
        }
        protected override void Startup()
        {
            var returnpage = new { controller = "User", action = "Index" };
            try
            {
                string usernameString = Convert.ToString(Request.RequestContext.RouteData.Values["Id"]);
                UserHelper userhelper = new UserHelper();
                if (usernameString.IsNullOrEmpty())
                    Response.RedirectToRoute(returnpage);

                User userData = _userRepository.GetUser(usernameString);
                if (userData.IsNull())
                    Response.RedirectToRoute(returnpage);

                userhelper.Username = userData.Username.ToString();
                if (UserHelper != null)
                {
                    Lookup.Remove<UserHelper>();
                }
                Lookup.Add(userhelper);
                ViewData["Userhelper"] = userhelper;

            }
            catch(Exception ex)
            {
                Response.RedirectToRoute(returnpage);
            }

        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                UserHelper userhelper = Lookup.Get<UserHelper>();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                userAppList = _userAppRepository.getUserApplication(userhelper.Username);

                // Loading.   
                PagedResult<Application> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _appRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _appRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<UserApplicationView>(new Converter<Application, UserApplicationView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        public UserApplicationView ConvertFrom(Application item)
        {
            UserApplicationView returnItem = new UserApplicationView();
            UserHelper userhelper = Lookup.Get<UserHelper>();
            int index = userAppList.FindIndex(x => x.Application == item.ApplicationId);
            if (index == -1)
            {
                returnItem.Selected = false;
                returnItem.Username = userhelper.Username;
            }
            else
            {
                returnItem.Id = userAppList[index-1].Id;
                returnItem.Selected = true;
                returnItem.Username = userhelper.Username;
            }
            returnItem.Application = item.ApplicationId;


            return returnItem;
        }

        [HttpPost]
        public ActionResult Create(UserHelper data)
        {
            JsonResult result = new JsonResult();
            string message = "";
            long Id = 0;
            try
            {

                if (data.Selected == true)
                {
                    Id=Insert(data);
                    
                }
                else
                {
                    Delete(data);
                }
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }

            result = this.Json(new
            {
                message = message,
                Id = Id
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }

        [HttpPost]
        public long Insert(UserHelper data)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                UserApplication newData = new UserApplication(0L);
                newData.Username = data.Username;
                newData.Application = data.Application;
                newData.IsDefault = false;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                _userAppRepository.Add(newData);
                returnId = newData.Id;
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
            }
            catch (Exception e)
            {
                returnId = -1;
            }

            return returnId;
        }

        [HttpPost]
        public ActionResult Delete(UserHelper data)
        {
            User user = Lookup.Get<User>();
            UserApplication userApplication = _userAppRepository.getUserApplication(data.Username, data.Application);
            string message = "";
            try
            {
                UserApplication newData = new UserApplication(userApplication.Id);
                _userAppRepository.deleteSelected(data.Username, data.Application);
                _userAppRepository.Remove(newData);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            
            return RedirectToAction("Search");
        }

    }
}