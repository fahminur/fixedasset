﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class RoleController : PageController
    {
        private IRoleRepository _roleRepository;
        private IApplicationRepository _appRepository;
        private IAuditTrailLog _auditRepository;
        public RoleController(ISessionAuthentication sessionAuthentication, IRoleRepository roleRepository,
            IApplicationRepository appRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._roleRepository = roleRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            Settings.ModuleName = "Role";
            Settings.Title = RoleResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Role> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _roleRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _roleRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<RoleView>(new Converter<Role, RoleView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public RoleView ConvertFrom(Role item)
        {
            RoleView returnItem = new RoleView();
            returnItem.Application = item._Application;
            returnItem.Description = item.Description;
            returnItem.SessionTimeout = item.SessionTimeout;
            returnItem.LockTimeout = item.LockTimeout;
            returnItem.RoleId = item.RoleId;
            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            return returnItem;
        }

        public SelectListItem ConvertFrom(Application item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.ApplicationId;
            return returnItem;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            RoleView data = new RoleView();
            return CreateView(data);
        }

        private ViewResult CreateView(RoleView data)
        {
            ViewData["ApplicationMaster"] = createApplicationSelect(data.Application);
            return View("Detail", data);
        }

        private IList<SelectListItem> createApplicationSelect(string selected)
        {
            IList<SelectListItem> dataList = _appRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Application, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(RoleView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Role newData = new Role(0L);
                    newData._Application = data.Application;
                    newData.RoleId = data.RoleId;
                    newData.Name = data.Name;
                    newData.Description = data.Description;
                    newData.SessionTimeout = data.SessionTimeout;
                    newData.LockTimeout = data.LockTimeout;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    _roleRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(RoleResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(RoleView data, string mode)
        {
            if (data.Application == "")
                return RoleResources.Validation_SelectApplication;

            if (data.Name == "")
                return RoleResources.Validation_FillRoleName;

            if (data.RoleId == "")
                return RoleResources.Validation_FillRoleId;

            if (data.SessionTimeout <= 0)
                return RoleResources.Validation_SessionTimeout;

            if (data.LockTimeout <= 0)
                return RoleResources.Validation_LockTimeout;

            if (mode == "Create")
            {
                if (_roleRepository.IsDuplicate(data.Application, data.RoleId))
                    return RoleResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            Role autoRole = _roleRepository.getRole(id);
            RoleView data = ConvertFrom(autoRole);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(RoleView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Role newData = new Role(data.Id);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData._Application = data.Application;
                    newData.RoleId = data.RoleId;
                    newData.Name = data.Name;
                    newData.Description = data.Description;
                    newData.SessionTimeout = data.SessionTimeout;
                    newData.LockTimeout = data.LockTimeout;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _roleRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(RoleResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Role newData = new Role(Id);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
                _roleRepository.Remove(newData);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(RoleResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}