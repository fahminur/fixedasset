﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using System.IO;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class UserApprovalController : PageController
    {
        private IUserRepository _userRepository;
        private IApplicationRepository _appRepository;
        private IAuditTrailLog _auditRepository;
        private IAuthorizationRepository _authRepository;
        public UserApprovalController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IApplicationRepository appRepository, IAuditTrailLog auditRepository, IAuthorizationRepository authRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._authRepository = authRepository;
            Settings.ModuleName = "UserApproval";
            Settings.Title = UserResources.PageTitle_UserApproval;
        }
        
        protected override void Startup()
        {
            //long uid;
            //long.TryParse(Request.RequestContext.RouteData.Values["id"].ToString(), out uid);
            //UserView userData = null;
            //if (uid.ToString().IsNullOrEmpty())
            //    Response.RedirectToRoute(new { controller = "User", action = "Index" });


            //userData = ConvertFrom(_userRepository.GetUserById(uid));

            //if (userData.IsNull())
            //    Response.RedirectToRoute(new { controller = "User", action = "Index" });

            //Lookup.Remove<UserView>();
            //Lookup.Add(userData);
            //ViewData["UserData"] = userData;
            //ViewData["ActionName"] = "Activate";
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<User> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _userRepository.FindAllPagedApproval(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    if (searchColumn.ToLower().Contains("date"))
                    {
                        DateTime searchDate = DateTime.ParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data = _userRepository.FindAllPagedDateApproval(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchDate);
                    }
                    else
                    {
                        data = _userRepository.FindAllPagedApproval(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                    }

                }

                var items = data.Items.ToList().ConvertAll<UserView>(new Converter<User, UserView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public UserView ConvertFrom(User item)
        {
            UserView returnItem = new UserView();
            returnItem.Id = item.Id;
            returnItem.Username = item.Username;
            returnItem.Password = Encryption.Instance.DecryptText(item.Password);
            returnItem.PasswordExpirationDate = item.PasswordExpirationDate.ToString("dd/MM/yyyy");
            returnItem.AccountValidityDate = item.AccountValidityDate.ToString("dd/MM/yyyy");
            returnItem.FirstName = item.FirstName;
            returnItem.LastName = item.LastName;
            returnItem.RegNo = item.RegNo;
            returnItem.SessionTimeout = item.SessionTimeout;
            returnItem.LockTimeout = item.LockTimeout;
            returnItem.MaxConLogin = item.MaximumConcurrentLogin;
            returnItem.InActiveDirectory = item.InActiveDirectory;
            returnItem.Name = item.Name;
            returnItem.IsActive = item.IsActive;

            return returnItem;
        }


        public ActionResult Approve(long id)
        {
            ViewData["ActionName"] = "Approve";
            User user = _userRepository.GetUserById(id);
            string auth = _authRepository.getDistinctUserAuthorizationByUsername(user.Username);
            
            UserView data = ConvertFrom(user);
            data.Roles = auth;
            ViewData["UserData"] = data;
            
            return View("Detail", data);
        }

        [HttpPost]
        public ActionResult Approve(UserView data)
        {
            ViewData["ActionName"] = "Activate";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (message.IsNullOrEmpty())
                {
                    User userdata = _userRepository.GetUserById(data.Id);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, userdata, user.Username, "Approve");
                    _userRepository.setApproved(data.Id);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_SuccessApprove));
                return RedirectToRoute(new { controller = "UserApproval", action = "Index" });
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return RedirectToRoute(new { controller = "UserApproval", action = "Index" });
        }

        // POST: Feature/Delete/5
        [HttpPost]
        public ActionResult Reject(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                User userdata = _userRepository.GetUserById(Id);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, userdata, user.Username, "Reject");
                _userRepository.deleteAll(userdata.Username);
                _userRepository.Remove(userdata);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Approve(Id);
        }

    }
}