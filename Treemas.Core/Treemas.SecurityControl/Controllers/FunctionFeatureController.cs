﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.SecurityControl.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.SecurityControl.Resources;
using Treemas.Base.Globals;
using Treemas.AuditTrail.Interface;

namespace Treemas.SecurityControl.Controllers
{
    public class FunctionFeatureController : PageController
    {
        private IFunctionFeatureRepository _funcfeatRepo;
        private IFunctionRepository _funcRepo;
        private IFeatureRepository _featRepo;
        private IApplicationRepository _appRepo;
        private IAuditTrailLog _auditRepository;


        private IList<FunctionFeature> funcFeatureList;
        public FunctionFeatureController(ISessionAuthentication sessionAuthentication, IFunctionRepository funcRepo,
            IFeatureRepository featRepo, IFunctionFeatureRepository funcfeatRepo, IApplicationRepository appRepo, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._funcRepo = funcRepo;
            this._featRepo = featRepo;
            this._funcfeatRepo = funcfeatRepo;
            this._appRepo = appRepo;
            this._auditRepository = auditRepository;
            Settings.ModuleName = "Function";
            Settings.Title = FunctionResources.PageTitleFunctionFeature;
        }

        protected override void Startup()
        {
            var returnpage = new { controller = "Function", action = "Index" };
            try
            {
                int funcId = Convert.ToInt32(Request.RequestContext.RouteData.Values["Id"]);
                AuthorizationFunction func = null;
                if (funcId < 1)
                    Response.RedirectToRoute(returnpage);
                func = _funcRepo.getFunction(funcId);
                if (func.IsNull())
                    Response.RedirectToRoute(returnpage);
                AuthorizationFunctionView funcView = ConvertFromViewData(func);
                Lookup.Remove<AuthorizationFunctionView>();
                Lookup.Add(funcView);
                ViewData["AuthorizationFunctionView"] = funcView;
            }
            catch (Exception ex)
            {
                string aa = ex.Message;
                Response.RedirectToRoute(returnpage);
            }

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                AuthorizationFunctionView funcView = Lookup.Get<AuthorizationFunctionView>();
                if (funcView.IsNull())
                {
                    if (Request.QueryString.GetValues("Id").IsNullOrEmpty())
                        return null;
                    long Id = long.Parse(Request.QueryString.GetValues("Id")[0]);
                    AuthorizationFunction func = _funcRepo.getFunction(Id);
                    funcView = ConvertFromViewData(func);
                    Lookup.Remove<AuthorizationFunctionView>();
                    Lookup.Add(funcView);
                }
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                funcFeatureList = _funcfeatRepo.getFunctionFeatures(funcView.Id);

                // Loading.   
                PagedResult<AuthorizationFeature> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _featRepo.FindAllPagedByApplication(pageNumber, pageSize, sortColumn, sortColumnDir, funcView.Application);
                }
                else
                {
                    data = _featRepo.FindAllPagedByApplication(pageNumber, pageSize, sortColumn, sortColumnDir, funcView.Application, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<FunctionFeatureView>(new Converter<AuthorizationFeature, FunctionFeatureView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public FunctionFeatureView ConvertFrom(AuthorizationFeature item)
        {
            FunctionFeatureView returnItem = new FunctionFeatureView();
            var found = funcFeatureList.FindElement(x => x.FeatureId == item.Id);
            if (found.IsNull())
            {
                returnItem.Selected = false;
            }
            else
            {
                returnItem.Selected = true;
                returnItem.Id = found.Id;
                returnItem.FunctionId = found.FunctionId;
            }
            returnItem.FeatureId = item.Id;
            returnItem.FeatureName = item.Name;
            returnItem.Description = item.Description;
            return returnItem;
        }

        public AuthorizationFunctionView ConvertFromViewData(AuthorizationFunction item)
        {
            AuthorizationFunctionView returnItem = new AuthorizationFunctionView();
            
            returnItem.Id = item.Id;
            returnItem.Application = item.Application ;
            returnItem.FunctionId = item.FunctionId;
            returnItem.Description = item.Description;
            returnItem.Name = item.Name;
            return returnItem;
        }
        

        public ActionResult Create(FunctionFeatureSelectHelper select)
        {
            JsonResult result = new JsonResult();
            string message = "";
            long Id = 0;
            try
            {
                AuthorizationFunctionView func = Lookup.Get<AuthorizationFunctionView>();

                AuthorizationFeature feat = null;
                if (select.selected == true)
                {
                    feat = _featRepo.getFeature(select.FeatureId); 
                    Id = Insert(feat, func.Id);
                }
                else
                {
                    message = Delete(func.Id, select.FeatureId); 
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            result = this.Json(new
            {
                message = message,
                Id = Id
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }


        private long Insert(AuthorizationFeature data, long functionId)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                FunctionFeature newData = new FunctionFeature(0L);
                newData.FeatureId = data.Id;
                newData.FunctionId = functionId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                _funcfeatRepo.Add(newData);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                returnId = newData.Id;
            }
            catch (Exception e)
            {
                returnId = -1;
            }
            return returnId;
        }

        public string Delete(long functionid, long featureid)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                _funcfeatRepo.deleteSelected(functionid, featureid);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, new { functionid, featureid}, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

    }
}