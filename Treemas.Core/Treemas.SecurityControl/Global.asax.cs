﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using System.Reflection;
using Treemas.Base.Web.Platform;
using Treemas.Credential;
using Treemas.Credential.Repository;
using Treemas.Credential.Interface;
using Treemas.AuditTrail;
using Treemas.AuditTrail.Interface;
using Treemas.AuditTrail.Repository;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;
using Treemas.Base.Utilities;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;

namespace Treemas.SecurityControl
{
    public class SecurityControl : WebApplication
    {
        public SecurityControl() : base("SecurityControl")
        {
            SystemSettings.Instance.Name = "Security Control";
            SystemSettings.Instance.Alias = "TSC";
            SystemSettings.Instance.Security.EnableAuthentication = true;
            SystemSettings.Instance.Security.IgnoreAuthorization = false;
            SystemSettings.Instance.Security.EnableSingleSignOn = false;
            SystemSettings.Instance.Logging.Enabled = false;
            SystemSettings.Instance.Security.EnableSiteSelection = false;
        }

        protected override void Startup()
        {
            Encryption.Instance.SetHashKey(SystemSettings.Instance.Security.EncryptionHalfKey);
            RouteCollection routes = RouteTable.Routes;
            RouteConfig.RegisterRoutes(routes);

            this.BindInterface();
        }

        private void BindInterface()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            container.Register<ISessionAuthentication, SessionAuthentication>(Lifestyle.Transient);
            container.Register<ISSOSessionStorage, SSOSessionStorage>(Lifestyle.Transient);
            // Register your types, for instance:
            container.Register<IUserRepository, UserRepository>(Lifestyle.Transient);
            container.Register<IUserApplicationRepository, UserApplicationRepository>(Lifestyle.Transient);
            container.Register<IRoleRepository, RoleRepository>(Lifestyle.Transient);
            container.Register<IFunctionRepository, FunctionRepository>(Lifestyle.Transient);
            container.Register<IFeatureRepository, FeatureRepository>(Lifestyle.Transient);
            container.Register<IAuthorizationRepository, AuthorizationRepository>(Lifestyle.Transient);
            container.Register<IMenuRepository, MenuRepository>(Lifestyle.Transient);
            container.Register<IApplicationRepository, ApplicationRepository>(Lifestyle.Transient);
            container.Register<IUserAccount, UserAccount>(Lifestyle.Transient);

            container.Register<IFunctionFeatureRepository, FunctionFeatureRepository>(Lifestyle.Transient);
            container.Register<IRoleFunctionRepository, RoleFunctionRepository>(Lifestyle.Transient);
            container.Register<IRoleFunctionFeatureRepository, RoleFunctionFeatureRepository>(Lifestyle.Transient);
            container.Register<IParameterRepository, ParameterRepository>(Lifestyle.Transient);
            container.Register<ILoginInfoRepository, LoginInfoRepository>(Lifestyle.Transient);

            container.Register<IAuditDataRepository, AuditDataRepository>(Lifestyle.Transient);
            container.Register<IAuditTrailLog, AuditTrailLog>(Lifestyle.Transient);

            container.Register<ISiteRepository, SiteRepository>(Lifestyle.Transient);
            container.Register<IUserSiteRepository, UserSiteRepository>(Lifestyle.Transient);
            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            // Override setting parameter
            var paramRepo = container.GetInstance<IParameterRepository>();
            var param = paramRepo.getParameter();
            SystemSettings.Instance.Security.SessionTimeout = param.SessionTimeout;
            SystemSettings.Instance.Security.LoginAttempt = param.LoginAttempt;
            SystemSettings.Instance.Security.MaximumConcurrentLogin = param.MaximumConcurrentLogin;
        }
    }
}
