﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Treemas.SecurityControl.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class UserResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UserResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Treemas.SecurityControl.Resources.UserResources", typeof(UserResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account Validity Date.
        /// </summary>
        public static string AccValidDate {
            get {
                return ResourceManager.GetString("AccValidDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Active.
        /// </summary>
        public static string Active {
            get {
                return ResourceManager.GetString("Active", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm Password.
        /// </summary>
        public static string ConfirmPass {
            get {
                return ResourceManager.GetString("ConfirmPass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application has been deleted.
        /// </summary>
        public static string Delete_Success {
            get {
                return ResourceManager.GetString("Delete_Success", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Name.
        /// </summary>
        public static string FirstName {
            get {
                return ResourceManager.GetString("FirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Icon.
        /// </summary>
        public static string Icon {
            get {
                return ResourceManager.GetString("Icon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In Active Directory.
        /// </summary>
        public static string InActiveDirectory {
            get {
                return ResourceManager.GetString("InActiveDirectory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Name.
        /// </summary>
        public static string LastName {
            get {
                return ResourceManager.GetString("LastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lock Timeout.
        /// </summary>
        public static string LockTimeout {
            get {
                return ResourceManager.GetString("LockTimeout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maximum Concurrent Login.
        /// </summary>
        public static string MaxConLogin {
            get {
                return ResourceManager.GetString("MaxConLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Old Password.
        /// </summary>
        public static string OldPassword {
            get {
                return ResourceManager.GetString("OldPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Master User.
        /// </summary>
        public static string PageTitle {
            get {
                return ResourceManager.GetString("PageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Creation Approval.
        /// </summary>
        public static string PageTitle_UserApproval {
            get {
                return ResourceManager.GetString("PageTitle_UserApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Password.
        /// </summary>
        public static string PageTitleChangePassword {
            get {
                return ResourceManager.GetString("PageTitleChangePassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password Expired Date.
        /// </summary>
        public static string PassExpDate {
            get {
                return ResourceManager.GetString("PassExpDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registration No.
        /// </summary>
        public static string RegNo {
            get {
                return ResourceManager.GetString("RegNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reset Password.
        /// </summary>
        public static string Reset {
            get {
                return ResourceManager.GetString("Reset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Role.
        /// </summary>
        public static string Role {
            get {
                return ResourceManager.GetString("Role", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session Time Out.
        /// </summary>
        public static string SessionTimeout {
            get {
                return ResourceManager.GetString("SessionTimeout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username.
        /// </summary>
        public static string Username {
            get {
                return ResourceManager.GetString("Username", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User has been Activated.
        /// </summary>
        public static string Validation_Activate {
            get {
                return ResourceManager.GetString("Validation_Activate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirmed password doesn&apos;t match.
        /// </summary>
        public static string Validation_ConfirmPass {
            get {
                return ResourceManager.GetString("Validation_ConfirmPass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Duplicate Username.
        /// </summary>
        public static string Validation_DuplicateData {
            get {
                return ResourceManager.GetString("Validation_DuplicateData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill account validity date.
        /// </summary>
        public static string Validation_FillAccValidDate {
            get {
                return ResourceManager.GetString("Validation_FillAccValidDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill active.
        /// </summary>
        public static string Validation_FillActive {
            get {
                return ResourceManager.GetString("Validation_FillActive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill first name.
        /// </summary>
        public static string Validation_FillFirstName {
            get {
                return ResourceManager.GetString("Validation_FillFirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill last name.
        /// </summary>
        public static string Validation_FillLastName {
            get {
                return ResourceManager.GetString("Validation_FillLastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill lock timeout.
        /// </summary>
        public static string Validation_FillLockTimeout {
            get {
                return ResourceManager.GetString("Validation_FillLockTimeout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill maximum concurrent login.
        /// </summary>
        public static string Validation_FillMaxConLogin {
            get {
                return ResourceManager.GetString("Validation_FillMaxConLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill password expired date.
        /// </summary>
        public static string Validation_FillPassExp {
            get {
                return ResourceManager.GetString("Validation_FillPassExp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill password.
        /// </summary>
        public static string Validation_FillPassword {
            get {
                return ResourceManager.GetString("Validation_FillPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill reg no.
        /// </summary>
        public static string Validation_FillRegNo {
            get {
                return ResourceManager.GetString("Validation_FillRegNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill session timeout.
        /// </summary>
        public static string Validation_FillSessionTimeout {
            get {
                return ResourceManager.GetString("Validation_FillSessionTimeout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill username.
        /// </summary>
        public static string Validation_FillUsername {
            get {
                return ResourceManager.GetString("Validation_FillUsername", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Old password do not match.
        /// </summary>
        public static string Validation_OldPassNotMatch {
            get {
                return ResourceManager.GetString("Validation_OldPassNotMatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password must contain alphabet with 1 numeric or special character.
        /// </summary>
        public static string Validation_Pass {
            get {
                return ResourceManager.GetString("Validation_Pass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please set password different from previous.
        /// </summary>
        public static string Validation_PassNewOld {
            get {
                return ResourceManager.GetString("Validation_PassNewOld", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password has been changed.
        /// </summary>
        public static string Validation_PassSuccess {
            get {
                return ResourceManager.GetString("Validation_PassSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your password is already used before, please use another password.
        /// </summary>
        public static string Validation_PasswordHistory {
            get {
                return ResourceManager.GetString("Validation_PasswordHistory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User has been printed.
        /// </summary>
        public static string Validation_Print {
            get {
                return ResourceManager.GetString("Validation_Print", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password has been reset.
        /// </summary>
        public static string Validation_Reset {
            get {
                return ResourceManager.GetString("Validation_Reset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User has been saved.
        /// </summary>
        public static string Validation_Success {
            get {
                return ResourceManager.GetString("Validation_Success", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User has been activated.
        /// </summary>
        public static string Validation_SuccessActivate {
            get {
                return ResourceManager.GetString("Validation_SuccessActivate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User has been approved.
        /// </summary>
        public static string Validation_SuccessApprove {
            get {
                return ResourceManager.GetString("Validation_SuccessApprove", resourceCulture);
            }
        }
    }
}
