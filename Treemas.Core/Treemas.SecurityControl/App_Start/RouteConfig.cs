﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Treemas.SecurityControl
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            string[] controllers = { "FunctionFeature", "RoleFunction", "UserApplicationRole" };
            string[] controllersString = { "UserApplication" };

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            foreach (string item in controllers)
            {
                routes.MapRoute(
                    name: item,
                    url: "{controller}/{id}/{action}/{rid}",
                    defaults: new { controller = item, action = "Index", rid = UrlParameter.Optional },
                    constraints: new { id = @"\d+" }
                );
            }


            foreach (string item in controllersString)
            {
                routes.MapRoute(
                    name: item,
                    url: item + "/{id}/{action}/{rid}",
                    defaults: new { controller = item, action = "Index", rid = UrlParameter.Optional },
                    constraints: new { id = @"^[a-zA-Z0-9]*$" }
                );
            }

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
