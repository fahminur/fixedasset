﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.SecurityControl.Models
{
    public class UserApplicationView
    {
        public long Id { get; set; }
        public virtual string Application { get; set; }
        public virtual bool IsDefault { get; set; }
        public virtual string Username { get; set; }
        public virtual bool Selected { get; set; }
        
    }
}