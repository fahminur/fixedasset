﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.SecurityControl.Models
{
    [Serializable]
    public class RoleSerializeableView
    {
        public long Id { get; set; }
        public string RoleId { get; set; }
        public string Name { get; set; }
        public string Application { get; set; }
    }
}