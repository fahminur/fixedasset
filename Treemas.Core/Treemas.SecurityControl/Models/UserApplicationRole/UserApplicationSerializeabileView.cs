﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.SecurityControl.Models
{
    [Serializable]
    public class UserApplicationSerializeabileView
    {
        public long Id { get; set; }
        public long UserApplicationId { get; set; }
        public string Application { get; set; }
        public string Username { get; set; }
        
    }
}