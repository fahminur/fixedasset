﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.SecurityControl.Models
{
    public class RoleFunctionFeatSelectHelper
    {
        public bool selected { get; set; }
        public long FeatureId { get; set; }
    }
}