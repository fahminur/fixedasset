﻿
namespace Treemas.SecurityControl.Models
{
    public class FunctionView
    {
        public long Id { get; set; }
        public string Application { get; set; }
        public string FunctionId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
}