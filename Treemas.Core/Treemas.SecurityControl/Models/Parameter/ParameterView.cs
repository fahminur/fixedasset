﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.SecurityControl.Models
{
    public class ParameterView
    {
        public string Id { get; set; }
        public string PasswordExp { get; set; }
        public string AccountValidity { get; set; }
        public int SessionTimeout { get; set; }
        public int LoginAttempt { get; set; }
    }
}