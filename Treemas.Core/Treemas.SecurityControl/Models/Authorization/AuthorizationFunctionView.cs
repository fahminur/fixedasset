﻿using System;
using System.Collections.Generic;
using Treemas.Credential.Model;
using System.Linq;
using System.Web;

namespace Treemas.SecurityControl.Models
{
    [Serializable]
    public class AuthorizationFunctionView
    {
        public long Id { get; set; }
        public string Application { get; set; }
        public string FunctionId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
}