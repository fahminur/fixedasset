﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treemas.SecurityControl.Models
{
    [Serializable]
    public class RoleFunctionSerializeableView
    {
        public long Id { get; set; }
        public long RoleId { get; set; }
        public long FunctionId { get; set; }
        public string Name { get; set; }
        public bool selected { get; set; }
    }
}