﻿using Treemas.Base.Configuration;

namespace Treemas.Base.Web.Platform
{
    internal class ApplicationConstants
    {
        private static readonly ApplicationConstants instance = new ApplicationConstants();

        private ApplicationConstants()
        {
            this.Location = new LocationConstants();
            this.RequestParameter = new RequestParameterConstants();
            this.Configuration = new ConfigurationConstants();
        }
        public string DataUploadName => "DataUpload";
        public string DataUploadRootFolder => "DataUpload";
        public string DataUploadValidationResultFolder => "DataUploadValidationResult";
        public ConfigurationConstants Configuration { get; private set; }

        public string CONFIGURATION_SINGLE_SIGN_ON_BINDER =>
            "SingleSignOn";

        public string CONFIGURATION_SINGLE_SIGN_ON_URL =>
            "Url";

        public string CONFIGURATION_SYSTEM_ALIAS =>
            "Application-Alias";

        public string CONFIGURATION_SYSTEM_NAME =>
            "Application-Name";

        public static ApplicationConstants Instance =>
            instance;

        public LocationConstants Location { get; private set; }

        public RequestParameterConstants RequestParameter { get; private set; }

        public string SECURITY_COOKIE_SESSIONID =>
            "_tms_session_id";

        public string SECURITY_SALT_SESSION_ID
        {
            get
            {
                IConfigurationBinder binder = ApplicationConfigurationCabinet.Instance.GetBinder("Encryption");
                if (binder != null)
                {
                    ConfigurationItem configuration = binder.GetConfiguration("SessionId");
                    if (configuration != null)
                    {
                        return configuration.Value;
                    }
                }
                return "HFz127EDVNL5t9K7";
            }
        }
    }
}
