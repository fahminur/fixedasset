﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Treemas.Base.Lookup;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Platform
{
    public class SSOSessionStorage : ISSOSessionStorage
    {
        private static SSOSessionStorage instance;
        private IUserAccount _userAccount;
        public SSOSessionStorage(IUserAccount userAccount)
        {
            this._userAccount = userAccount;
        }

        public void Delete(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                string path = Path.Combine(SystemSettings.Instance.Security.SSOSessionStoragePath, id + ".sso");
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
        }

        public ILookup Load(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                string path = Path.Combine(SystemSettings.Instance.Security.SSOSessionStoragePath, id + ".sso");
                if (File.Exists(path))
                {
                    FileStream serializationStream = File.OpenRead(path);
                    BinaryFormatter formatter = new BinaryFormatter();
                    SSOSessionPersistence persistence = (SSOSessionPersistence)formatter.Deserialize(serializationStream);
                    serializationStream.Close();
                    if (persistence != null)
                    {
                        User user = _userAccount.GetUserAttributes(persistence.Username, SystemSettings.Instance.Name);
                        ILookup data = persistence.Data;
                        data.Add(user);
                        return data;
                    }
                }
            }
            return null;
        }

        public void Save(string id, ILookup lookup)
        {
            if (!lookup.IsNull())
            {
                User user = lookup.Get<User>();
                if (!user.IsNull())
                {
                    string path = Path.Combine(SystemSettings.Instance.Security.SSOSessionStoragePath, id + ".sso");
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    IList<object> all = lookup.GetAll();
                    ILookup lookup2 = new SimpleLookup();
                    foreach (object obj2 in all)
                    {
                        lookup2.Add(obj2);
                    }
                    lookup2.Remove<User>();
                    SSOSessionPersistence graph = new SSOSessionPersistence
                    {
                        Username = user.Username,
                        Password = user.Password,
                        Data = lookup2
                    };
                    FileStream serializationStream = File.Create(path, 0x200);
                    new BinaryFormatter().Serialize(serializationStream, graph);
                    serializationStream.Close();
                }
            }
        }
    }
}
