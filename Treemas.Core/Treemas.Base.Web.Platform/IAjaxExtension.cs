﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Treemas.Base.Web.Platform
{
    public interface IAjaxExtension
    {
        ActionResult Execute(HttpRequestBase request, HttpResponseBase response, HttpSessionStateBase session);
        string GetName();
    }
}
