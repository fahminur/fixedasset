﻿using System;
using System.Text;
using System.Web;
using System.Web.Routing;
using Treemas.Base.Utilities;
using Treemas.Base.Lookup;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;


namespace Treemas.Base.Web.Platform
{
    public class SessionAuthentication : ISessionAuthentication
    {
        private IUserAccount _userAccount;
        private ISSOSessionStorage _ssoSessionStorage;

        public SessionAuthentication(IUserAccount userAccount, ISSOSessionStorage ssoSessionStorage)
        {
            this.Enabled = true;
            this._userAccount = userAccount;
            this._ssoSessionStorage = ssoSessionStorage;
        }

        public PageDescriptor descriptor { get; set; }
        public string screenID { get; set; }
        public bool Enabled { get; set; }
        public bool IsAuthorized { get; set; }
        public bool IsValid { get; set; }
        public string RedirectUrl { get; set; }
        public void Authenticate(RequestContext requestContext)
        {
            HttpContextBase httpContext = requestContext.HttpContext;
            HttpSessionStateBase session = httpContext.Session;

            object ActionName = requestContext.RouteData.Values["action"];

            ILookup lookup = session.Lookup();
            SecuritySettings security = SystemSettings.Instance.Security;
            if (!(security.EnableAuthentication && this.Enabled))
            {
                this.IsValid = true;
                this.IsAuthorized = true;
            }
            else
            {
                User AuthenticatedUser = lookup.Get<User>(); 
                if (security.EnableSingleSignOn && AuthenticatedUser.IsNull())
                {
                    AuthenticatedUser = this._GetSSOUser(httpContext);
                } 
                if (AuthenticatedUser.IsNull())
                {
                    if (security.LoginController.Equals(this.screenID))
                    {
                        this.IsValid = true;
                        this.IsAuthorized = true;
                        return;
                    }
                    if (security.SimulateAuthenticatedSession && session.IsNewSession)
                    {
                        lookup.Remove<User>();
                        AuthenticatedUser = security.SimulatedAuthenticatedUser;
                        lookup.Add(AuthenticatedUser);
                    }
                    if (AuthenticatedUser.IsNull())
                    {
                        lookup.Remove<User>();
                        this.IsValid = false;
                        this.IsAuthorized = false;
                        return;
                    }
                }
                else
                {
                    //AuthenticatedUser.LoginInfos = _userAccount.GetLoginInfos(AuthenticatedUser.Username);
                    this.IsValid = true;
                }

                if (security.LoginController.Equals(this.screenID))
                {
//                    this.RedirectUrl = this.descriptor.BaseUrl + "/" + SystemSettings.Instance.Runtime.HomeController;
                    this.IsAuthorized = true;
                }
                else if (this.screenID == "Error")
                {
                    this.IsAuthorized = true;
                }
                else if (security.IgnoreAuthorization)
                {
                    this.IsAuthorized = true;
                }
                else if (security.ForgotPasswordController.Equals(this.screenID))
                {
                    this.IsAuthorized = true;
                }
                else if (SystemSettings.Instance.Runtime.SwitchController.Equals(this.screenID))
                {
                    this.IsAuthorized = true;
                }
                else if (!(string.IsNullOrEmpty(security.UnauthorizedController) || !security.UnauthorizedController.Equals(this.screenID)))
                {
                    this.IsAuthorized = true;
                }
                else if (this.screenID.Equals(SystemSettings.Instance.Runtime.HomeController) && this.screenID.Equals("Home"))
                {
                    this.IsAuthorized = true;
                }
                else if (this.screenID.Equals(SystemSettings.Instance.Security.LoginController))
                {
                    this.IsAuthorized = true;
                }
                else if (SystemSettings.Instance.Runtime.SwitchSessionController.Equals(this.screenID))
                {
                    this.IsAuthorized = true;
                }
                else if (SystemSettings.Instance.Security.SiteSelectionController.Equals(this.screenID))
                {
                    this.IsAuthorized = true;
                }
                else if (ActionName.ToString().Contains("NoAuthCheck"))
                {
                    this.IsAuthorized = true;
                }
                else
                {
                    AuthorizationFunction authFunc;
                    AuthorizationFeature authFeat;
                    AuthenticatedUser.Roles.IterateByAction<Role>(delegate (Role role) {
                        authFunc = role.Functions.FindElement<AuthorizationFunction>(func => this.screenID.Equals(func.FunctionId, StringComparison.OrdinalIgnoreCase));
                        authFeat = (!authFunc.IsNull()) ? authFunc.Features.FindElement<AuthorizationFeature>(feat => ActionName.ToString().Equals(feat.FeatureId, StringComparison.OrdinalIgnoreCase)) : null;

                        this.IsAuthorized = !authFunc.IsNull() && !authFeat.IsNull();
                        return !this.IsAuthorized;
                    });
                }

                if (AuthenticatedUser.Site.IsNull() && SystemSettings.Instance.Security.EnableSiteSelection && this.IsAuthorized && !SystemSettings.Instance.Security.SiteSelectionController.Equals(this.screenID))
                {
                    this.RedirectUrl = this.descriptor.BaseUrl + "/" + SystemSettings.Instance.Security.SiteSelectionController;
                }

            }
        }

        private User _GetSSOUser(HttpContextBase httpContext)
        {
            HttpSessionStateBase session = httpContext.Session;
            HttpCookie cookie = httpContext.Request.Cookies[ApplicationConstants.Instance.SECURITY_COOKIE_SESSIONID];
            if (!cookie.IsNull() && !string.IsNullOrEmpty(cookie.Value))
            {
                string id = Encoding.UTF8.GetString(HttpServerUtility.UrlTokenDecode(cookie.Value));
                if (SSOClient.Instance.IsAlive(id))
                {
                    User user = null;

                    ILookup lookup = session.Lookup();
                    
                    string loggedInUser = SSOClient.Instance.GetLoggedInUser(id);

                    if (lookup != null)
                    {
                        user = lookup.Get<User>();
                    }
                    if (user == null)
                    {
                        ILookup lookup2 = _ssoSessionStorage.Load(id);
                        if (lookup == null || lookup2 == null)
                        {
                            lookup2 = new SimpleLookup();
                            if (!string.IsNullOrEmpty(loggedInUser))
                            {
                                user = _userAccount.GetUserAttributes(loggedInUser, SystemSettings.Instance.Name);
                                lookup2.Add(user);
                                _ssoSessionStorage.Save(id, lookup2);
                            }
                        }
                        if (!string.IsNullOrEmpty(loggedInUser))
                            SSOClient.Instance.MarkActive(id);
                        session["__tms_Lookup__"] = lookup2;
                        SSOSessionLookupListener.RemoveExistingInstance(lookup);
                        lookup2.AddEventListener(new SSOSessionLookupListener(id, _ssoSessionStorage));
                        lookup = lookup2;
                    }
                    return user;
                }
                _ssoSessionStorage.Delete(id);
                cookie.Expires = DateTime.Now.AddDays(-1.0);
                httpContext.Response.Cookies.Add(cookie);
            }
            return null;
        }

    }
}
