﻿using System;
using System.Collections.Generic;
using Treemas.Base.Lookup;

namespace Treemas.Base.Web.Platform
{
    internal class SSOSessionLookupListener : ILookupEventListener
    {
        private string id;
        private ISSOSessionStorage _ssoSessionStorage;

        public SSOSessionLookupListener(string id, ISSOSessionStorage ssoSessionStorage)
        {
            this.id = id;
            this._ssoSessionStorage = ssoSessionStorage;
        }

        public void LookupChanged(LookupEvent evt)
        {
            if ((evt.Type == LookupEventType.Instance_Added) || (evt.Type == LookupEventType.Instance_Removed))
            {
                _ssoSessionStorage.Save(this.id, evt.Broadcaster);
            }
        }

        public static void RemoveExistingInstance(ILookup lookup)
        {
            if (lookup != null)
            {
                ILookupEventListener listener = null;
                IList<ILookupEventListener> listeners = lookup.GetListeners();
                if (listeners != null)
                {
                    foreach (ILookupEventListener listener2 in listeners)
                    {
                        if (listener2 is SSOSessionLookupListener)
                        {
                            listener = listener2;
                            break;
                        }
                    }
                    if (listener != null)
                    {
                        lookup.RemoveEventListener(listener);
                    }
                }
            }
        }
    }
}
