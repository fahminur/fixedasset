﻿using System;
using System.Web.Mvc;
using System.Globalization;

namespace Treemas.Base.Web.Platform
{
    public class CustomDateModelBinder : DefaultModelBinder
    {
        public override object BindModel
        (ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var displayFormat = "dd/MM/yyyy";
            var displayFormatTime = "dd/MM/yyyy hh:mm tt";
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (!string.IsNullOrEmpty(displayFormat) && value != null)
            {
                DateTime date;
                //displayFormat = displayFormat.Replace
                //("{0:", string.Empty).Replace("}", string.Empty);
                if (DateTime.TryParseExact(value.AttemptedValue, displayFormat,
                CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    return date;
                }
                else if (DateTime.TryParseExact(value.AttemptedValue, displayFormatTime,
                CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    return date;
                }
                else
                {
                    return null;
                    //bindingContext.ModelState.AddModelError(
                    //    bindingContext.ModelName,
                    //    string.Format("{0} is an invalid date format", value.AttemptedValue)
                    //);
                }
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}
