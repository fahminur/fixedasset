﻿using System.Web;

namespace Treemas.Base.Web.Platform
{
    public class BrowserAgent
    {
        public static BrowserAgent Chrome(string version) =>
            new BrowserAgent
            {
                Code = "chrome",
                Name = "Google Chrome",
                Version = version
            };

        public static BrowserAgent Firefox(string version) =>
            new BrowserAgent
            {
                Code = "firefox",
                Name = "Mozilla Firefox",
                Version = version
            };

        public static BrowserAgent InternetExplorer(string version) =>
            new BrowserAgent
            {
                Code = "ie",
                Name = "Internet Explorer",
                Version = version
            };

        public bool IsAgentEquals(HttpBrowserCapabilitiesBase browser)
        {
            string str = browser.Type.ToLower();
            string str2 = browser.Version.ToLower();
            return (((!string.IsNullOrEmpty(this.Code) && !string.IsNullOrEmpty(this.Version)) && str.Contains(this.Code.ToLower())) && str2.Contains(this.Version.ToLower()));
        }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Version { get; set; }
    }
}
