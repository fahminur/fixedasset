﻿using System.Web.Mvc;

namespace Treemas.Base.Web.Platform
{
    public class BaseRazorExtension
    {
        public virtual HtmlHelper Helper { get; set; }
    }
}
