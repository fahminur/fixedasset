﻿
namespace Treemas.Base.Web.Platform
{
    public class ConfigurationConstants
    {
        public ConfigurationConstants()
        {
            this.System = new SystemConfigurationConstants();
            this.Database = new DatabaseConfigurationConstants();
            this.Session = new SessionConfigurationConstants();
        }

        public DatabaseConfigurationConstants Database { get; private set; }

        public SessionConfigurationConstants Session { get; private set; }

        public SystemConfigurationConstants System { get; private set; }
    }
}
