﻿using System.Web.Mvc;

namespace Treemas.Base.Web.Platform
{
    public static class RazorHelperExtension
    {
        private static readonly PlatformUtilities extension = new PlatformUtilities();

        public static PlatformUtilities Treemas(this HtmlHelper helper)
        {
            extension.Helper = helper;
            return extension;
        }
    }
}
