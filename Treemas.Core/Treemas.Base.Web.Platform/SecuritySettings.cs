﻿using Treemas.Credential.Model;

namespace Treemas.Base.Web.Platform
{
    public class SecuritySettings
    {
        public SecuritySettings()
        {
            this.LoginController = "Login";
            this.ForgotPasswordController = "ForgotPassword";
            this.SiteSelectionController = "SiteSelection";
            this.IgnoreAuthorization = true;
        }

        public bool EnableAuthentication { get; set; }
        public bool EnableSingleSignOn { get; set; }
        public string ForgotPasswordController { get; set; }
        public bool IgnoreAuthorization { get; set; }
        public string LoginController { get; set; }
        public int LoginAttempt { get; set; }
        public int SessionTimeout { get; set; }
        public int MaximumConcurrentLogin { get; set; }
        public bool SimulateAuthenticatedSession { get; set; }
        public User SimulatedAuthenticatedUser { get; set; }
        public string SSOServiceUrl { get; set; }
        public string SSOSessionStoragePath { get; set; }
        public string UnauthorizedController { get; set; }
        public bool UseCustomAuthenticationRule { get; set; }
        public bool UseCustomAuthorizationRule { get; set; }
        public string EncryptionHalfKey { get; set; }
        public string SiteSelectionController { get; set; }
        public bool EnableSiteSelection { get; set; }
    }
}
