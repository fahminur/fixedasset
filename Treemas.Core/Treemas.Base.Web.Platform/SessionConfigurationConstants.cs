﻿namespace Treemas.Base.Web.Platform
{
    public class SessionConfigurationConstants
    {
        public string HomeFolder =>
            "Home-Session";

        public string Name =>
            "Sessions";
    }
}
