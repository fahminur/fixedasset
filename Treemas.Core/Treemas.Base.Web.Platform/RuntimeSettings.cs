﻿namespace Treemas.Base.Web.Platform
{
    public class RuntimeSettings
    {
        public RuntimeSettings()
        {
            this.Browser = new BrowserSettings();
            this.Mode = RuntimeMode.Online;
            this.HomeController = "Home";
            this.SwitchController = "Switch";
            this.ChangePasswordController = "ChangePassword";
            this.SwitchSessionController = "SwitchSession";
            this.EnableMobileSupport = false;
            this.EnabledMenu = false;
            this.IsPortal = false;
        }

        public BrowserSettings Browser { get; private set; }
        public bool EnableMobileSupport { get; set; }
        public bool EnabledMenu { get; set; }
        public string HomeController { get; set; }
        public string SwitchController { get; set; }
        public string ChangePasswordController { get; set; }
        public string SwitchSessionController { get; set; }
        public string PortalUrl { get; set; }
        public bool IsPortal { get; set; }
        public string MaintenanceModeController { get; set; }
        public RuntimeMode Mode { get; set; }
        public string OfflineModeController { get; set; }
    }
}
