﻿using System;


namespace Treemas.Base.Web.Platform
{
    public class DeploymentContextSettings
    {
        public DeploymentContextSettings()
        {
            this.Name = string.Empty;
            this.EmulateSecureProtocol = false;
        }

        public bool EmulateSecureProtocol { get; set; }

        public string Name { get; set; }
    }
}
