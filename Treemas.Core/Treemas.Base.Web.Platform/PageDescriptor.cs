﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Treemas.Base.Web.Platform
{
    public class PageDescriptor
    {
        private static string _baseUrl;
        private HttpRequestBase request;

        public void AttachToRequest(ViewDataDictionary viewData)
        {
            viewData["_tmsBaseUrl"] = this.BaseUrl;
        }
        public void Initialize(RequestContext requestContext)
        {
            HttpContextBase httpContext = requestContext.HttpContext;
            this.request = httpContext.Request;
        }
        public string BaseUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_baseUrl))
                {
                    string scheme = this.request.Url.Scheme;
                    DeploymentContextSettings context = SystemSettings.Instance.Deployment.Context;
                    if (context.EmulateSecureProtocol)
                    {
                        scheme = "https";
                    }
                    string name = context.Name;
                    if (!string.IsNullOrEmpty(name))
                    {
                        _baseUrl = $"{scheme}://{this.request.ServerVariables["HTTP_HOST"]}/{name}";
                    }
                    else
                    {
                        _baseUrl = $"{scheme}://{this.request.ServerVariables["HTTP_HOST"]}";
                    }
                }
                return _baseUrl;
            }
        }
    }
}
