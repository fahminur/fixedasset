﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
namespace Treemas.Foundation.Interface
{
    public interface IKelurahanRepository : IBaseRepository<Foundation_Kelurahan>
    {
        bool IsDuplicate(string siteid);
        PagedResult<Foundation_Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Foundation_Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<Foundation_Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, KelurahanFilter filter);
        IEnumerable<Foundation_Kelurahan> getKelurahan(string Kota, string Kecamatan);
        IEnumerable<Foundation_Kelurahan> getKecamatan(string Kota);
        IEnumerable<Foundation_Kelurahan> getKota();
        Foundation_Kelurahan GetZipCode(string ID);
        Foundation_Kelurahan getKelurahanSingle(string ID);
    }
}
