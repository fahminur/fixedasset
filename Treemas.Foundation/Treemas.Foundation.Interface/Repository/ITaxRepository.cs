﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Interface
{
    public interface ITaxRepository : IBaseRepository<Tax>
    {
        bool IsDuplicate(string siteid);
        PagedResult<Tax> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Tax> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        Tax getTax(string id);
        IList<Tax> getTaxByCategory(string category);
    }
}
