﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Interface
{
    public interface ISiteRepository : IBaseRepository<Site>
    {
        bool IsDuplicate(string siteid);
        PagedResult<Site> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Site> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        Site getSite(string id);
        IEnumerable<Site> findActive();
        IEnumerable<Site> FindBySite(string siteId);
    }
}
