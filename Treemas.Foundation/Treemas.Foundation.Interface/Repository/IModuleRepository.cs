﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Interface
{
    public interface IModuleRepository : IBaseRepository<Module>
    {
        bool IsDuplicate(string moduleid);
        PagedResult<Module> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Module> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        Module getModule(string id);
    }
}
