﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Interface
{
    public interface IUomCategoryRepository : IBaseRepository<UomCategory>
    {
        bool IsDuplicate(string UomCategoryId);
        PagedResult<UomCategory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<UomCategory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        UomCategory getUomCategory(string id);
    }
}
