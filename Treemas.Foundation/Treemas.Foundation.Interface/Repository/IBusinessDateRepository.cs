﻿using Treemas.Credential.Model;

namespace Treemas.Foundation.Interface
{
    public interface IBusinessDateRepository : IBaseRepository<BusinessDate>
    {
        BusinessDate getBusinessDate();
    }
}
