﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Interface
{
    public interface IUomRepository : IBaseRepository<Uom>
    {
        bool IsDuplicate(string currencyid);
        PagedResult<Uom> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Uom> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        Uom getUom(string id);
    }
}
