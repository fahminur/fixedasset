﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Interface
{
    public interface ISiteUserRepository : IBaseRepository<SiteUser>
    {
        SiteUser getSiteUser(long id);
        SiteUser getSiteUserBySite(string userName, string siteId);
        IList<SiteUser> getSitesbyUser(string userName);
        bool IsDuplicate(long userId, string siteId);
        PagedResult<SiteUser> FindAllPaged(string SiteId, int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<SiteUser> FindAllPaged(string SiteId, int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
    }
}
