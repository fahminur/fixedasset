﻿
namespace Treemas.Foundation.Web.Models
{
    public class CurrencyView
    {
        public string CurrencyId { get; set; }
        public string CurrencyDescription { get; set; }
        public string CurrencyShortDesc { get; set; }
        public bool Status { get; set; }

    }
}