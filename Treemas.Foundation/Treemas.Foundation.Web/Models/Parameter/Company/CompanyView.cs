﻿using System;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Web.Models
{
    [Serializable]
    public class CompanyView
    {
        public string Id { get; set; }
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyShortName { get; set; }
        public string CompanyInitialName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyRT { get; set; }
        public string CompanyRW { get; set; }
        public string CompanyKelurahan { get; set; }
        public string CompanyKecamatan { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyZipCode { get; set; }
        public string CompanyAreaPhone1 { get; set; }
        public string CompanyPhone1 { get; set; }
        public string CompanyAreaPhone2 { get; set; }
        public string CompanyPhone2 { get; set; }
        public string CompanyAreaFax { get; set; }
        public string CompanyFax { get; set; }
        public string NPWP { get; set; }
        public string TDP { get; set; }
        public string SIUP { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonJobTitle { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonHP { get; set; }
        public virtual bool Status { get; set; }

    }
}