﻿
namespace Treemas.Foundation.Web.Models
{
    public class SiteView
    {
        public string SiteId { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public string SiteKelurahan { get; set; }
        public string SiteKecamatan { get; set; }
        public string SiteCity { get; set; }
        public string SiteZipCode { get; set; }
        public bool Status { get; set; }

    }
}