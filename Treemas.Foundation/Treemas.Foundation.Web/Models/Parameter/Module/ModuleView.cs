﻿
namespace Treemas.Foundation.Web.Models
{
    public class ModuleView
    {
        public string ModuleId { get; set; }
        public string ModuleDescription { get; set; }       
        public bool Status { get; set; }

    }
}