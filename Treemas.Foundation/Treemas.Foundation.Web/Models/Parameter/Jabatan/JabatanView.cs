﻿
namespace Treemas.Foundation.Web.Models
{
    public class JabatanView
    {
        public string JabatanID { get; set; }
        public string JabatanDescription { get; set; }
        public string JabatanShortDesc { get; set; }
        public bool Status { get; set; }

    }
}