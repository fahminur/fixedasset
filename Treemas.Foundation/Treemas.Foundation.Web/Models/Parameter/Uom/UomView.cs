﻿
namespace Treemas.Foundation.Web.Models
{
    public class UomView
    {
        public string UomID { get; set; }
        public string UomFullDescription { get; set; }
        public string UomShorDesc { get; set; }
        public string UomDecimal { get; set; }
        public string UomCategoryID { get; set; }
        public bool Status { get; set; }

    }
}