﻿
namespace Treemas.Foundation.Web.Models
{
    public class UomCategoryView
    {
        public string UomCatID { get; set; }
        public string UomCatDescription { get; set; }
        public string UomCatShortDesc { get; set; }
        public bool Status { get; set; }

    }
}