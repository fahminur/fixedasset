﻿using System;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Web.Models
{
    [Serializable]
    public class TaxView
    {
        public string TaxID { get; set; }
        public string TaxDescription { get; set; }
        public string TaxShortDesc { get; set; }
        public string Rate { get; set; }
        public CalcRuleEnum CalcRule { get; set; }
        public string CalcRuleString { get; set; }
        public string CalcRuleValue {
            get
            {
                if (Enum.IsDefined(typeof(CalcRuleEnum), CalcRule))
                {
                    return CalcRule.ToString();
                }
                return "";
            }
        }
        public string GlSegment { get; set; }
        public string GlSegmentValue { get; set; }
        public string TaxCategory { get; set; }
        public bool Status { get; set; }

    }
}