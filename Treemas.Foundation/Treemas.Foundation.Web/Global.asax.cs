﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Reflection;
using Treemas.Base.Web.Platform;
using Treemas.Base.Utilities;
using Treemas.Credential;
using Treemas.Credential.Interface;
using Treemas.Credential.Repository;
using Treemas.AuditTrail;
using Treemas.AuditTrail.Interface;
using Treemas.AuditTrail.Repository;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Repository;

using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;

namespace Treemas.Foundation.Web
{
    public class MvcApplication : WebApplication
    {
        public MvcApplication() : base("TreemasFND")
        {
            SystemSettings.Instance.Name = "Treemas FND";
            SystemSettings.Instance.Alias = "TMS-Foundation";
            SystemSettings.Instance.Runtime.IsPortal = false;
            SystemSettings.Instance.Security.EnableAuthentication = true;
            SystemSettings.Instance.Security.IgnoreAuthorization = false;
            SystemSettings.Instance.Security.EnableSingleSignOn = false;
            SystemSettings.Instance.Logging.Enabled = false;
            SystemSettings.Instance.Security.EnableSiteSelection = false;
        }

        protected override void Startup()
        {
            Encryption.Instance.SetHashKey(SystemSettings.Instance.Security.EncryptionHalfKey);
            RouteCollection routes = RouteTable.Routes;
            RouteConfig.RegisterRoutes(routes);

            this.BindInterface();
        }

        private void BindInterface()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            container.Register<ISessionAuthentication, SessionAuthentication>(Lifestyle.Scoped);
            container.Register<ISSOSessionStorage, SSOSessionStorage>(Lifestyle.Scoped);
            // Register your types, for instance:
            container.Register<IUserRepository, UserRepository>(Lifestyle.Scoped);
            container.Register<IUserApplicationRepository, UserApplicationRepository>(Lifestyle.Scoped);
            container.Register<IRoleRepository, RoleRepository>(Lifestyle.Scoped);
            container.Register<IFunctionRepository, FunctionRepository>(Lifestyle.Scoped);
            container.Register<IFeatureRepository, FeatureRepository>(Lifestyle.Scoped);
            container.Register<IAuthorizationRepository, AuthorizationRepository>(Lifestyle.Scoped);
            container.Register<IApplicationRepository, ApplicationRepository>(Lifestyle.Scoped);
            container.Register<ILoginInfoRepository, LoginInfoRepository>(Lifestyle.Scoped);
            container.Register<IMenuRepository, MenuRepository>(Lifestyle.Scoped);
            container.Register<IUserAccount, UserAccount>(Lifestyle.Scoped);
            container.Register<IParameterRepository, ParameterRepository>(Lifestyle.Scoped);
            container.Register<IBusinessDateRepository, BusinessDateRepository>(Lifestyle.Scoped);


            // Audit Trail Module
            container.Register<IAuditDataRepository, AuditDataRepository>(Lifestyle.Transient);
            container.Register<IAuditTrailLog, AuditTrailLog>(Lifestyle.Transient);
            
            // For Foundation Module
            container.Register<ICurrencyRepository, CurrencyRepository>(Lifestyle.Transient);
            container.Register<ICompanyRepository, CompanyRepository>(Lifestyle.Transient);
            container.Register<ISiteRepository, SiteRepository>(Lifestyle.Transient);
            container.Register<IUomRepository, UomRepository>(Lifestyle.Transient);
            container.Register<IUomCategoryRepository, UomCategoryRepository>(Lifestyle.Transient);
            container.Register<ISiteUserRepository, SiteUserRepository>(Lifestyle.Transient);
            container.Register<IKelurahanRepository, KelurahanRepository>(Lifestyle.Transient);
            container.Register<IModuleRepository, ModuleRepository>(Lifestyle.Transient);



            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            // Override setting parameter
            var paramRepo = container.GetInstance<IParameterRepository>();
            var param = paramRepo.getParameter();
            SystemSettings.Instance.Security.SessionTimeout = param.SessionTimeout;
            SystemSettings.Instance.Security.LoginAttempt = param.LoginAttempt;
            SystemSettings.Instance.Security.MaximumConcurrentLogin = param.MaximumConcurrentLogin;
        }
    }
}
