﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using Treemas.Foundation.Web.Resources;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Web.Controllers
{
    public class SiteSelectionController : PageController
    {
        private IUserRepository _userRepository;
        private ISiteUserRepository _siteUserRepository;
        private ISiteRepository _siteRepository;
        public SiteSelectionController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            ISiteUserRepository siteUserRepository, ISiteRepository siteRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._siteUserRepository = siteUserRepository;
            this._siteRepository = siteRepository;
        }

        // GET: Home
        protected override void Startup()
        {
            User userInfo = Lookup.Get<User>();
            ViewData["User"] = userInfo;
            List<SiteUser> sites = new List<SiteUser>(_siteUserRepository.getSitesbyUser(userInfo.Username));
            if (sites.Count < 1)
            {
                ScreenMessages.Submit(ScreenMessage.Error(GeneralResources.Validation_UserSite));
                this.Authentication.RedirectUrl = this.Authentication.descriptor.BaseUrl + "/" + SystemSettings.Instance.Security.LoginController + "/Logout";
            }
            else if (sites.Count == 1)
            {
                userInfo.Site = _siteRepository.getSite(sites[0].SiteId.Trim());
                Lookup.Remove<User>();
                Lookup.Add(userInfo);
                this.Authentication.RedirectUrl = this.Authentication.descriptor.BaseUrl + "/" + SystemSettings.Instance.Runtime.HomeController;
            }
            else
            {
                ViewData["SiteId"] = sites.ConvertAll<SelectListItem>(new Converter<SiteUser, SelectListItem>(ConvertFrom)); ;
            }
            CollectScreenMessages();
        }

        public SelectListItem ConvertFrom(SiteUser item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Site.SiteName;
            returnItem.Value = item.SiteId;
            return returnItem;
        }

        [HttpPost]
        public ActionResult Select(string SiteId)
        {
            User userInfo = Lookup.Get<User>();
            try
            {
                userInfo.Site = _siteRepository.getSite(SiteId.Trim());
                Lookup.Remove<User>();
                Lookup.Add(userInfo);
                return this.Redirect(this.Authentication.descriptor.BaseUrl + "/" + SystemSettings.Instance.Runtime.HomeController);
            }
            catch (Exception e)
            {
                ScreenMessages.Submit(ScreenMessage.Error(e.Message));
                return this.RedirectToAction("Index");
            }
        }
    }
}