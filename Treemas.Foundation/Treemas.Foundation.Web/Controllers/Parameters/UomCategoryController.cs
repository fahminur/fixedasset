﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;
using Treemas.Foundation.Web.Resources;
using Treemas.Foundation.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.Foundation.Controllers
{
    public class UomCategoryController : PageController
    {
        private IUomCategoryRepository _uomCategoryRepository;
        private IAuditTrailLog _auditRepository;
        public UomCategoryController(ISessionAuthentication sessionAuthentication, IUomCategoryRepository uomCategoryRepository,
               IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._uomCategoryRepository = uomCategoryRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Uom Category";
            Settings.Title = UomCategoryResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<UomCategory> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _uomCategoryRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _uomCategoryRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<UomCategoryView>(new Converter<UomCategory, UomCategoryView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public UomCategoryView ConvertFrom(UomCategory item)
        {
            UomCategoryView returnItem = new UomCategoryView();
            returnItem.UomCatID = item.UomCatID;
            returnItem.UomCatDescription = item.UomCatDescription;
            returnItem.UomCatShortDesc = item.UomCatShortDesc;
            returnItem.Status = item.Status;
            return returnItem;
        }
        

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            UomCategoryView data = new UomCategoryView();
            return CreateView(data);
        }

        private ViewResult CreateView(UomCategoryView data)
        {
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(UomCategoryView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    UomCategory newData = new UomCategory("");
                    newData.UomCatID = data.UomCatID;
                    newData.UomCatDescription = data.UomCatDescription;
                    newData.UomCatShortDesc = data.UomCatShortDesc;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _uomCategoryRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(UomCategoryView data, string mode)
        {
            if (data.UomCatID == "")
                return UomCategoryResources.Validation_Id;

            if (data.UomCatDescription == "")
                return UomCategoryResources.Validation_Description;

            if (data.UomCatShortDesc == "")
                return UomCategoryResources.Validation_ShortDesc;

            if (data.Status.IsNull())
                return UomCategoryResources.Validation_Status;

            if (mode == "Create")
            {
                if (_uomCategoryRepository.IsDuplicate(data.UomCatID))
                    return UomCategoryResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            UomCategory coaClass = _uomCategoryRepository.getUomCategory(id);
            UomCategoryView data = ConvertFrom(coaClass);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(UomCategoryView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    UomCategory newData = _uomCategoryRepository.getUomCategory(data.UomCatID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.UomCatID = data.UomCatID;
                    newData.UomCatDescription = data.UomCatDescription;
                    newData.UomCatShortDesc = data.UomCatShortDesc;
                    newData.Status = data.Status;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _uomCategoryRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                UomCategory newData = _uomCategoryRepository.getUomCategory(Id);
                _uomCategoryRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}