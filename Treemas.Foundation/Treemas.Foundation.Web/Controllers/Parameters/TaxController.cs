﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;
using Treemas.Foundation.Web.Resources;
using Treemas.Foundation.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;
using Treemas.Procurement.Model;
using Treemas.Procurement.Interface;
using Treemas.Procurement.Repository;

namespace Treemas.Foundation.Controllers
{
    public class TaxController : PageController
    {
        private ITaxRepository _TaxRepository;
        private IAuditTrailLog _auditRepository;
        private IGLSegmentRepository _gLSegmentRepository;

        public TaxController(ISessionAuthentication sessionAuthentication, ITaxRepository TaxRepository, IGLSegmentRepository gLSegmentRepository,
               IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._TaxRepository = TaxRepository;
            this._gLSegmentRepository = gLSegmentRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Tax";
            Settings.Title = TaxResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Tax> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _TaxRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _TaxRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<TaxView>(new Converter<Tax, TaxView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        //public ActionResult NoAuthCheckSearch()
        //{
        //    return Search();
        //}

        public TaxView ConvertFrom(Tax item)
        {
            GLSegment gLSegment = _gLSegmentRepository.getGLSegment(item.GlSegment);

            TaxView returnItem = new TaxView();
            returnItem.TaxID = item.TaxID;
            returnItem.TaxDescription = item.TaxDescription;
            returnItem.TaxShortDesc = item.TaxShortDesc;
            returnItem.Rate = item.Rate;
            returnItem.CalcRule = item.CalcRuleEnum;
            returnItem.CalcRuleString = item.CalcRuleEnum.ToDescription();
            returnItem.GlSegment = item.GlSegment;
            returnItem.GlSegmentValue = gLSegment.GLSegmentDescription;
            returnItem.TaxCategory = item.TaxCategory;
            returnItem.Status = item.Status;
            return returnItem;
        }
        private IList<SelectListItem> createCalcRuleSelect(string selected)
        {
            List<SelectListItem> dataList = Enum.GetValues(typeof(CalcRuleEnum)).Cast<CalcRuleEnum>().Select(x => new SelectListItem { Text = x.ToDescription(), Value = x.ToString() }).ToList();
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }

            return dataList;
        }
        public SelectListItem Convert1(Tax item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.CalcRule;
            returnItem.Value = item.CalcRule;
            return returnItem;
        }

        private IList<SelectListItem> createGlSegmentSelect(string selected)
        {
            List<GLSegment> itemList = Lookup.Get<List<GLSegment>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _gLSegmentRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<GLSegment, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                //dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(GLSegment item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.GLSegmentDescription;
            returnItem.Value = item.GLSegmentId;
            return returnItem;
        }




        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            TaxView data = new TaxView();
            return CreateView(data);
        }

        private ViewResult CreateView(TaxView data)
        {
            ViewData["GLSegment"] = createGlSegmentSelect(data.GlSegment);
            ViewData["CalcRuleSelect"] = createCalcRuleSelect(data.CalcRuleValue);
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(TaxView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Tax newData = new Tax("");
                    newData.TaxID = data.TaxID;
                    newData.TaxDescription = data.TaxDescription;
                    newData.TaxShortDesc = data.TaxShortDesc;
                    newData.Rate = data.Rate;
                    newData.CalcRule = data.CalcRuleValue;
                    newData.GlSegment = data.GlSegment;
                    newData.TaxCategory = data.TaxCategory;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _TaxRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(TaxView data, string mode)
        {
            if (data.TaxID == "")
                return TaxResources.Validation_ID;

            if (data.TaxDescription == "")
                return TaxResources.Validation_TaxDescription;

            if (data.TaxShortDesc == "")
                return TaxResources.Validation_TaxShortDesc;
            if (data.Rate == "")
                return TaxResources.Validation_Rate;
            if (data.CalcRuleValue == "")
                return TaxResources.Validation_CalRule;

            if (mode == "Create")
            {
                if (_TaxRepository.IsDuplicate(data.TaxID))
                    return GeneralResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            Tax Tax = _TaxRepository.getTax(id);
            TaxView data = ConvertFrom(Tax);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(TaxView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Tax newData = _TaxRepository.getTax(data.TaxID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.TaxID = data.TaxID;
                    newData.TaxDescription = data.TaxDescription;
                    newData.TaxShortDesc = data.TaxShortDesc;
                    newData.Rate = data.Rate;
                    newData.CalcRule = data.CalcRuleValue;
                    newData.GlSegment = data.GlSegment;
                    newData.TaxCategory = data.TaxCategory;
                    newData.Status = data.Status;
                    newData.CreateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _TaxRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Tax newData = _TaxRepository.getTax(Id);
                _TaxRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}