﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;
using Treemas.Foundation.Web.Resources;
using Treemas.Foundation.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.Foundation.Controllers
{
    public class CompanyController : PageController
    {
        private ICompanyRepository _companyRepository;
        private IAuditTrailLog _auditRepository;

        public CompanyController(ISessionAuthentication sessionAuthentication, ICompanyRepository companyRepository,
               IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._companyRepository = companyRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Company";
            Settings.Title = CompanyResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Company> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _companyRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _companyRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<CompanyView>(new Converter<Company, CompanyView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public CompanyView ConvertFrom(Company item)
        {
            CompanyView returnItem = new CompanyView();
            returnItem.CompanyId = item.CompanyId;
            returnItem.CompanyName = item.CompanyName;
            returnItem.CompanyShortName = item.CompanyShortName;
            returnItem.CompanyInitialName = item.CompanyInitialName;
            returnItem.CompanyAddress = item.CompanyAddress;
            returnItem.CompanyRT = item.CompanyRT;
            returnItem.CompanyRW = item.CompanyRW;
            returnItem.CompanyKelurahan = item.CompanyKelurahan;
            returnItem.CompanyKecamatan = item.CompanyKecamatan;
            returnItem.CompanyCity = item.CompanyCity;
            returnItem.CompanyZipCode = item.CompanyZipCode;
            returnItem.CompanyAreaPhone1 = item.CompanyAreaPhone1;
            returnItem.CompanyPhone1 = item.CompanyPhone1;
            returnItem.CompanyAreaPhone2 = item.CompanyAreaPhone2;
            returnItem.CompanyPhone2 = item.CompanyPhone2;
            returnItem.CompanyAreaFax = item.CompanyAreaFax;
            returnItem.CompanyFax = item.CompanyFax;
            returnItem.NPWP = item.NPWP;
            returnItem.TDP = item.TDP;
            returnItem.SIUP = item.SIUP;
            returnItem.ContactPersonName = item.ContactPersonName;
            returnItem.ContactPersonJobTitle = item.ContactPersonJobTitle;
            returnItem.ContactPersonEmail = item.ContactPersonEmail;
            returnItem.ContactPersonHP = item.ContactPersonHP;   
            return returnItem;
        }
        

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            CompanyView data = new CompanyView();
            return CreateView(data);
        }

        private ViewResult CreateView(CompanyView data)
        {
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(CompanyView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Company newData = new Company("");
                    newData.CompanyId = data.CompanyId;
                    newData.CompanyName = data.CompanyName;
                    newData.CompanyShortName = data.CompanyShortName;
                    newData.CompanyInitialName = data.CompanyInitialName;
                    newData.CompanyAddress = data.CompanyAddress;
                    newData.CompanyRT = data.CompanyRT;
                    newData.CompanyRW = data.CompanyRW;
                    newData.CompanyKelurahan = data.CompanyKelurahan;
                    newData.CompanyKecamatan = data.CompanyKecamatan;
                    newData.CompanyCity = data.CompanyCity;
                    newData.CompanyZipCode = data.CompanyZipCode;
                    newData.CompanyAreaPhone1 = data.CompanyAreaPhone1;
                    newData.CompanyPhone1 = data.CompanyPhone1;
                    newData.CompanyAreaPhone2 = data.CompanyPhone2;
                    newData.CompanyPhone2 = data.CompanyPhone2;
                    newData.CompanyAreaFax = data.CompanyAreaFax;
                    newData.CompanyFax = data.CompanyFax;
                    newData.NPWP = data.NPWP;
                    newData.TDP = data.TDP;
                    newData.SIUP = data.SIUP;
                    newData.ContactPersonName = data.ContactPersonName;
                    newData.ContactPersonJobTitle = data.ContactPersonJobTitle;
                    newData.ContactPersonEmail = data.ContactPersonEmail;
                    newData.ContactPersonHP = data.ContactPersonHP;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _companyRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(CompanyView data, string mode)
        {
            if (data.CompanyId == "")
                return CompanyResources.Validation_Id;

            if (data.CompanyName == "")
                return CompanyResources.Validation_Name;

            if (data.CompanyShortName == "")
                return CompanyResources.Validation_ShortName;

            if (data.CompanyInitialName== "")
                return CompanyResources.Validation_InitialName;

            if (data.CompanyAddress == "")
                return CompanyResources.Validation_Address;

            if (data.CompanyRT == "")
                return CompanyResources.Validation_RT;

            if (data.CompanyRW == "")
                return CompanyResources.Validation_RW;

            if (data.CompanyKelurahan=="")
                return CompanyResources.Validation_Kelurahan;

            if (data.CompanyKecamatan == "")
                return CompanyResources.Validation_Kecamatan;

            if (data.CompanyCity == "")
                return CompanyResources.Validation_City;

            if (data.CompanyZipCode == "")
                return CompanyResources.Validation_ZipCode;           

            if (data.CompanyAreaPhone1 == "")
                return CompanyResources.Validation_AreaPhone1;

            if (data.CompanyPhone1 == "")
                return CompanyResources.Validation_Phone1;

            if (data.CompanyAreaPhone2 == "")
                return CompanyResources.Validation_AreaPhone2;

            if (data.CompanyPhone2 == "")
                return CompanyResources.Validation_Phone2;

            if (data.CompanyAreaFax == "")
                return CompanyResources.Validation_AreaFax;

            if (data.CompanyFax == "")
                return CompanyResources.Validation_Fax;

            if (data.NPWP == "")
                return CompanyResources.Validation_NPWP;

            if (data.TDP== "")
                return CompanyResources.Validation_TDP;

            if (data.SIUP == "")
                return CompanyResources.Validation_SIUP;

            if (data.ContactPersonName == "")
                return CompanyResources.Validation_PersonName;

            if (data.ContactPersonJobTitle == "")
                return CompanyResources.Validation_PersonJobTitle;

            if (data.ContactPersonEmail== "")
                return CompanyResources.Validation_PersonEmail;

            if (data.ContactPersonHP == "")
                return CompanyResources.Validation_PersonHP;
         

            if (mode == "Create")
            {
                if (_companyRepository.IsDuplicate(data.CompanyId))
                    return CompanyResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            Company coaClass = _companyRepository.getCompany(id);
            CompanyView data = ConvertFrom(coaClass);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(CompanyView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Company newData = _companyRepository.getCompany(data.CompanyId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");
                    newData.CompanyId = data.CompanyId;
                    newData.CompanyName = data.CompanyName;
                    newData.CompanyShortName = data.CompanyShortName;
                    newData.CompanyInitialName = data.CompanyInitialName;
                    newData.CompanyAddress = data.CompanyAddress;
                    newData.CompanyRT = data.CompanyRT;
                    newData.CompanyRW = data.CompanyRW;
                    newData.CompanyKelurahan = data.CompanyKelurahan;
                    newData.CompanyKecamatan = data.CompanyKecamatan;
                    newData.CompanyCity = data.CompanyCity;
                    newData.CompanyZipCode = data.CompanyZipCode;
                    newData.CompanyAreaPhone1 = data.CompanyAreaPhone1;
                    newData.CompanyPhone1 = data.CompanyPhone1;
                    newData.CompanyAreaPhone2 = data.CompanyAreaPhone2;
                    newData.CompanyPhone2 = data.CompanyPhone2;
                    newData.CompanyAreaFax = data.CompanyAreaFax;
                    newData.CompanyFax = data.CompanyFax;
                    newData.NPWP = data.NPWP;
                    newData.TDP = data.TDP;
                    newData.SIUP = data.SIUP;
                    newData.ContactPersonName = data.ContactPersonName;
                    newData.ContactPersonJobTitle = data.ContactPersonJobTitle;
                    newData.ContactPersonEmail = data.ContactPersonEmail;
                    newData.ContactPersonHP = data.ContactPersonHP;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _companyRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Company newData = _companyRepository.getCompany((Id));
                _companyRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}