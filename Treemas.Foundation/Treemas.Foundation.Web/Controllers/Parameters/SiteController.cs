﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;
using Treemas.Foundation.Web.Resources;
using Treemas.Foundation.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.Foundation.Controllers
{
    public class SiteController : PageController
    {
        private ISiteRepository _siteRepository;
        private IAuditTrailLog _auditRepository;
        public SiteController(ISessionAuthentication sessionAuthentication, ISiteRepository siteRepository,
               IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._siteRepository = siteRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Site";
            Settings.Title = SiteResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Site> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _siteRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _siteRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<SiteView>(new Converter<Site, SiteView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SiteView ConvertFrom(Site item)
        {
            SiteView returnItem = new SiteView();
            returnItem.SiteId = item.SiteId;
            returnItem.SiteName = item.SiteName;
            returnItem.SiteCity = item.SiteCity;
            returnItem.SiteKecamatan = item.SiteKecamatan;
            returnItem.SiteKelurahan = item.SiteKelurahan;
            returnItem.SiteAddress = item.SiteAddress;
            returnItem.SiteZipCode = item.SiteZipCode;
            returnItem.Status = item.Status;
            return returnItem;
        }
        

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            SiteView data = new SiteView();
            return CreateView(data);
        }

        private ViewResult CreateView(SiteView data)
        {
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(SiteView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Site newData = new Site("");
                    newData.SiteId = data.SiteId;
                    newData.SiteAddress = data.SiteAddress;
                    newData.SiteCity = data.SiteCity;
                    newData.SiteKecamatan = data.SiteKecamatan;
                    newData.SiteKelurahan = data.SiteKelurahan;
                    newData.SiteName = data.SiteName;
                    newData.SiteZipCode = data.SiteZipCode;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _siteRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(SiteView data, string mode)
        {
            if (data.SiteId == "")
                return SiteResources.Validation_Id;

            if (data.SiteAddress == "")
                return SiteResources.Validation_Address;

            if (data.SiteCity == "")
                return SiteResources.Validation_City;

            if (data.SiteKecamatan.IsNull())
                return SiteResources.Validation_Kecamatan;

            if (data.SiteKelurahan == "")
                return SiteResources.Validation_Kelurahan;

            if (data.SiteName == "")
                return SiteResources.Validation_Name;

            if (data.SiteZipCode == "")
                return SiteResources.Validation_Zipcode;

            if (data.Status.IsNull())
                return SiteResources.Validation_Status;

            if (mode == "Create")
            {
                if (_siteRepository.IsDuplicate(data.SiteId))
                    return SiteResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            Site site = _siteRepository.getSite(id);
            SiteView data = ConvertFrom(site);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(SiteView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Site newData = _siteRepository.getSite(data.SiteId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.SiteId = data.SiteId;
                    newData.SiteAddress = data.SiteAddress;
                    newData.SiteCity = data.SiteCity;
                    newData.SiteKecamatan = data.SiteKecamatan;
                    newData.SiteKelurahan = data.SiteKelurahan;
                    newData.SiteName = data.SiteName;
                    newData.SiteZipCode = data.SiteZipCode;
                    newData.Status = data.Status;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _siteRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Site newData = _siteRepository.getSite(Id);
                _siteRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}