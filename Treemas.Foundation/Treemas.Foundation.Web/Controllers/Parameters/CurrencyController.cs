﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;
using Treemas.Foundation.Web.Resources;
using Treemas.Foundation.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.Foundation.Controllers
{
    public class CurrencyController : PageController
    {
        private ICurrencyRepository _currencyRepository;
        private IAuditTrailLog _auditRepository;
        public CurrencyController(ISessionAuthentication sessionAuthentication, ICurrencyRepository currencyRepository,
               IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._currencyRepository = currencyRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Currency";
            Settings.Title = CurrencyResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Currency> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _currencyRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _currencyRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<CurrencyView>(new Converter<Currency, CurrencyView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public CurrencyView ConvertFrom(Currency item)
        {
            CurrencyView returnItem = new CurrencyView();
            returnItem.CurrencyId = item.CurrencyId;
            returnItem.CurrencyDescription = item.CurrencyDescription;
            returnItem.CurrencyShortDesc = item.CurrencyShortDesc;
            returnItem.Status = item.Status;
            return returnItem;
        }
        

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            CurrencyView data = new CurrencyView();
            return CreateView(data);
        }

        private ViewResult CreateView(CurrencyView data)
        {
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(CurrencyView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Currency newData = new Currency("");
                    newData.CurrencyId = data.CurrencyId;
                    newData.CurrencyDescription = data.CurrencyDescription;
                    newData.CurrencyShortDesc = data.CurrencyShortDesc;
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _currencyRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(CurrencyView data, string mode)
        {
            if (data.CurrencyId == "")
                return CurrencyResources.Validation_Id;

            if (data.CurrencyDescription == "")
                return CurrencyResources.Validation_Description;

            if (data.CurrencyShortDesc == "")
                return CurrencyResources.Validation_ShortDesc;

            if (data.Status.IsNull())
                return CurrencyResources.Validation_Status;

            if (mode == "Create")
            {
                if (_currencyRepository.IsDuplicate(data.CurrencyId))
                    return CurrencyResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            Currency coaClass = _currencyRepository.getCurrency(id);
            CurrencyView data = ConvertFrom(coaClass);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(CurrencyView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Currency newData = _currencyRepository.getCurrency(data.CurrencyId);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.CurrencyId = data.CurrencyId;
                    newData.CurrencyShortDesc = data.CurrencyShortDesc;
                    newData.CurrencyDescription = data.CurrencyDescription;
                    newData.Status = data.Status;

                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _currencyRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Currency newData = _currencyRepository.getCurrency(Id);
                _currencyRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}