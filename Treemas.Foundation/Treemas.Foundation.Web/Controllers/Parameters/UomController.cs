﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Model;
using Treemas.Foundation.Interface;
using Treemas.Foundation.Model;
using Treemas.Foundation.Web.Resources;
using Treemas.Foundation.Web.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.AuditTrail.Interface;

namespace Treemas.Foundation.Controllers
{
    public class UomController : PageController
    {
        private IUomRepository _uomRepository;
        private IUomCategoryRepository _uomCategoryRepository;


        private IAuditTrailLog _auditRepository;
        public UomController(ISessionAuthentication sessionAuthentication, IUomRepository uomRepository, IUomCategoryRepository uomCategoryRepository,
               IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._uomRepository = uomRepository;
            this._uomCategoryRepository = uomCategoryRepository;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Unit Of Measurement";
            Settings.Title = UomResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Uom> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _uomRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _uomRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<UomView>(new Converter<Uom, UomView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public UomView ConvertFrom(Uom item)
        {
            UomCategory uomCategory = _uomCategoryRepository.getUomCategory(item.UomCategoryID);

            UomView returnItem = new UomView();
            returnItem.UomID = item.UomID;
            returnItem.UomFullDescription = item.UomFullDescription;
            returnItem.UomShorDesc = item.UomShorDesc;
            returnItem.UomDecimal = item.UomDecimal;
            returnItem.UomCategoryID = uomCategory.UomCatDescription;         
            returnItem.Status = item.Status;
            return returnItem;
        }

        private IList<SelectListItem> createUomSelect(string selected)
        {
            List<UomCategory> itemList = Lookup.Get<List<UomCategory>>(); ;
            if (itemList.IsNullOrEmpty())
            {
                itemList = _uomCategoryRepository.FindAll().ToList();
                Lookup.Remove(itemList);
                Lookup.Add(itemList);
            }

            IList<SelectListItem> dataList = itemList.ConvertAll<SelectListItem>(new Converter<UomCategory, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Text == selected.ToString()).Selected = true;
            }
            return dataList;
        }
        public SelectListItem ConvertFrom(UomCategory item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.UomCatDescription;
            returnItem.Value = item.UomCatID;
            return returnItem;
        }


        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            UomView data = new UomView();
            return CreateView(data);
        }

        private ViewResult CreateView(UomView data)
        {
            ViewData["UomList"] = createUomSelect(data.UomCategoryID);
            return View("Detail", data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(UomView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Uom newData = new Uom("");
                    newData.UomID = data.UomID;
                    newData.UomFullDescription = data.UomFullDescription;
                    newData.UomShorDesc = data.UomShorDesc;
                    newData.UomDecimal = data.UomDecimal;
                    newData.UomCategoryID = data.UomCategoryID;               
                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;
                    newData.CreateUser = user.Username;
                    newData.CreateDate = DateTime.Now;
                    _uomRepository.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(UomView data, string mode)
        {
            if (data.UomID == "")
                return UomResources.Validation_Id;

            if (data.UomFullDescription == "")
                return UomResources.Validation_Description;

            if (data.UomShorDesc == "")
                return UomResources.Validation_ShorDesc;

            if (data.UomDecimal == "")
                return UomResources.Validation_Decimal;

            if (data.UomCategoryID == "")
                return UomResources.Validation_CategoryID;
         
            if (data.Status.IsNull())
                return UomResources.Validation_Status;

            if (mode == "Create")
            {
                if (_uomRepository.IsDuplicate(data.UomID))
                    return UomResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(string id)
        {
            ViewData["ActionName"] = "Edit";
            Uom uom = _uomRepository.getUom(id);
            UomView data = ConvertFrom(uom);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(UomView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Uom newData = _uomRepository.getUom(data.UomID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.UomID = data.UomID;
                    newData.UomFullDescription = data.UomFullDescription;
                    newData.UomShorDesc = data.UomShorDesc;
                    newData.UomDecimal = data.UomDecimal;
                    newData.UomCategoryID = data.UomCategoryID;
                 

                    newData.Status = data.Status;
                    newData.UpdateUser = user.Username;
                    newData.UpdateDate = DateTime.Now;

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _uomRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Uom newData = _uomRepository.getUom(Id);
                _uomRepository.Remove(newData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(GeneralResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

    }
}