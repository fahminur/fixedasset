﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;


namespace Treemas.Foundation.Model
{
    [Serializable]
    public class Foundation_Kelurahan : EntityString
    {
        protected Foundation_Kelurahan()
        {
        }
        public Foundation_Kelurahan (string id) : base(id)
        {
            this.KelurahanID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string KelurahanID { get; set; }
        public virtual string Kelurahan { get; set; }
        public virtual string Kecamatan { get; set; }
        public virtual string Kota      { get; set; }
        public virtual string ZipCode   { get; set; }
        public virtual string AreaCode  { get; set; }
        public virtual string Provinsi  { get; set; }
        public virtual DateTime? CreateDate{ get; set; }
        public virtual string CreateUser{ get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual string UpdateUser { get; set; }


       
    }
}
