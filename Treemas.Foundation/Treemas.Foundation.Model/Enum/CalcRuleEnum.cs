﻿using Treemas.Base.Utilities;

namespace Treemas.Foundation.Model
{
    public enum CalcRuleEnum : int
    {
        [StringValue("+")]
        P = 1,
        [StringValue("-")]
        M = 2
    }
}
