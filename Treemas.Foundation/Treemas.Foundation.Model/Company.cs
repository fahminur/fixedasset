﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;


namespace Treemas.Foundation.Model
{
    [Serializable]
    public class Company : EntityString
    {
        protected Company()
        {
        }
        public Company(string id) : base(id)
        {
            this.CompanyId = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string CompanyId { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual string CompanyShortName { get; set; }
        public virtual string CompanyInitialName { get; set; }
        public virtual string CompanyAddress { get; set; }
        public virtual string CompanyRT { get; set; }
        public virtual string CompanyRW { get; set; }
        public virtual string CompanyKelurahan { get; set; }
        public virtual string CompanyKecamatan { get; set; }
        public virtual string CompanyCity { get; set; }
        public virtual string CompanyZipCode { get; set; }
        public virtual string CompanyAreaPhone1 { get; set; }
        public virtual string CompanyPhone1 { get; set; }
        public virtual string CompanyAreaPhone2 { get; set; }
        public virtual string CompanyPhone2 { get; set; }
        public virtual string CompanyAreaFax { get; set; }
        public virtual string CompanyFax { get; set; }
        public virtual string NPWP { get; set; }
        public virtual string TDP { get; set; }
        public virtual string SIUP { get; set; }
        public virtual string ContactPersonName { get; set; }
        public virtual string ContactPersonJobTitle { get; set; }
        public virtual string ContactPersonEmail { get; set; }
        public virtual string ContactPersonHP { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
    }
}
