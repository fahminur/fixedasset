﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;


namespace Treemas.Foundation.Model
{
    [Serializable]
    public class UomCategory : EntityString
    {
        protected UomCategory()
        {
        }
        public UomCategory(string id) : base(id)
        {
            this.UomCatID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string UomCatID { get; set; }
        public virtual string UomCatDescription { get; set; }
        public virtual string UomCatShortDesc { get; set; }
        public virtual bool Status { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
    }
}
