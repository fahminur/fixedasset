﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;


namespace Treemas.Foundation.Model
{
    [Serializable]
    public class Tax : EntityString
    {
        protected Tax()
        {
        }
        public Tax(string id) : base(id)
        {
            this.TaxID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string TaxID { get; set; }
        public virtual string TaxDescription { get; set; }
        public virtual string TaxShortDesc { get; set; }
        public virtual string Rate { get; set; }
        public virtual string CalcRule { get; set; }
        public virtual string TaxCategory { get; set; }
        public virtual CalcRuleEnum CalcRuleEnum {
            get
            {
                return (CalcRuleEnum)Enum.Parse(typeof(CalcRuleEnum), this.CalcRule);
            }
        }
        public virtual string GlSegment { get; set; }
        public virtual bool   Status { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }

       
    }
}
