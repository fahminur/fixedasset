﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;


namespace Treemas.Foundation.Model
{
    [Serializable]
    public class SiteUser : Entity
    {
        protected SiteUser()
        {
        }
        public SiteUser(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string SiteId { get; set; }
        public virtual Site Site { get; set; }
        public virtual long UserId { get; set; }
        public virtual string Username { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Name
        {
            get
            {
                string str = (this.FirstName != null) ? this.FirstName : string.Empty;
                return (str + ((this.LastName != null) ? (" " + this.LastName) : string.Empty)).Trim();
            }
        }
        public virtual bool IsActive { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
    }
}
