﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;


namespace Treemas.Foundation.Model
{
    [Serializable]
    public class Uom : EntityString
    {
        protected Uom()
        {
        }
        public Uom(string id) : base(id)
        {
            this.UomID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string UomID { get; set; }
        public virtual string UomFullDescription { get; set; }
        public virtual string UomShorDesc { get; set; }
        public virtual string UomDecimal { get; set; }
        public virtual string UomCategoryID { get; set; }
        public virtual bool Status { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
    }
}
