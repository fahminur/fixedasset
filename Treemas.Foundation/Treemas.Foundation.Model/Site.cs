﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;


namespace Treemas.Foundation.Model
{
    [Serializable]
    public class Site : EntityString
    {
        protected Site()
        {
        }
        public Site(string id) : base(id)
        {
            this.SiteId = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string SiteId { get; set; }
        public virtual string SiteName { get; set; }
        public virtual string SiteAddress { get; set; }
        public virtual string SiteKelurahan { get; set; }
        public virtual string SiteKecamatan { get; set; }
        public virtual string SiteCity { get; set; }
        public virtual string SiteZipCode { get; set; }
        public virtual bool Status { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
    }
}
