﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;


namespace Treemas.Foundation.Model
{
    [Serializable]
    public class Currency : EntityString
    {
        protected Currency()
        {
        }
        public Currency(string id) : base(id)
        {
            this.CurrencyId = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string CurrencyId { get; set; }
        public virtual string CurrencyDescription { get; set; }
        public virtual string CurrencyShortDesc { get; set; }
        public virtual bool Status { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual string UpdateUser { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
    }
}
