﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Foundation.Repository
{
    public class SiteRepository : RepositoryControllerString<Site>, ISiteRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SiteRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public IEnumerable<Site> findActive()
        {
            Specification<Site> specification;
            specification = new AdHocSpecification<Site>(s => s.Status == true);

            return FindAll(specification);
        }
        public Site getSite(string id)
        {
            return transact(() => session.QueryOver<Site>()
                                                .Where(f => f.SiteId == id).SingleOrDefault());
        }
     
        public bool IsDuplicate(string siteid)
        {
            int rowCount = transact(() => session.QueryOver<Site>()
                                                .Where(f => f.SiteId == siteid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        
        public IEnumerable<Site> FindBySite(string siteId)
        {
            return transact(() => session.Query<Site>()
                                        .Where(f => f.SiteId == siteId)
                                        .ToList());
        }

        public PagedResult<Site> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Site> paged = new PagedResult<Site>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Site>()
                                                     .OrderByDescending(GetOrderByExpression<Site>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Site>()
                                                     .OrderBy(GetOrderByExpression<Site>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Site> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Site> specification;

            switch (searchColumn)
            {
                case "SiteName":
                    specification = new AdHocSpecification<Site>(s => s.SiteName.Contains(searchValue));
                    break;
                case "SiteId":
                    specification = new AdHocSpecification<Site>(s => s.SiteId.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<Site>(s => s.SiteId.Contains(searchValue));
                    break;
            }

            PagedResult<Site> paged = new PagedResult<Site>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Site>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Site>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Site>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Site>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<Site>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
