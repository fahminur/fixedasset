﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Foundation.Repository
{
    public class UomCategoryRepository : RepositoryControllerString<UomCategory>, IUomCategoryRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public UomCategoryRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public UomCategory getUomCategory(string id)
        {
            return transact(() => session.QueryOver<UomCategory>()
                                                .Where(f => f.UomCatID == id).SingleOrDefault());
        }
     
        public bool IsDuplicate(string currencyid)
        {
            int rowCount = transact(() => session.QueryOver<UomCategory>()
                                                .Where(f => f.UomCatID == currencyid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<UomCategory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<UomCategory> paged = new PagedResult<UomCategory>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<UomCategory>()
                                                     .OrderByDescending(GetOrderByExpression<UomCategory>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<UomCategory>()
                                                     .OrderBy(GetOrderByExpression<UomCategory>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<UomCategory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<UomCategory> specification;

            switch (searchColumn)
            {
                case "UomCatID":
                    specification = new AdHocSpecification<UomCategory>(s => s.UomCatID.Contains(searchValue));
                    break;
                case "UomCatDescription":
                    specification = new AdHocSpecification<UomCategory>(s => s.UomCatDescription.Contains(searchValue));
                    break;
                case "UomCatShortDesc":
                    specification = new AdHocSpecification<UomCategory>(s => s.UomCatShortDesc.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<UomCategory>(s => s.UomCatID.Contains(searchValue));
                    break;
            }

            PagedResult<UomCategory> paged = new PagedResult<UomCategory>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<UomCategory>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<UomCategory>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<UomCategory>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<UomCategory>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<UomCategory>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
