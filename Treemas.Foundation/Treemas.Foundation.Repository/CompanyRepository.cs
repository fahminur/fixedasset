﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Foundation.Repository
{
    public class CompanyRepository : RepositoryControllerString<Company>, ICompanyRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public CompanyRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public Company getCompany(string id)
        {
            return transact(() => session.QueryOver<Company>()
                                                .Where(f => f.CompanyId == id).SingleOrDefault());
        }
     
        public bool IsDuplicate(string companyid)
        {
            int rowCount = transact(() => session.QueryOver<Company>()
                                                .Where(f => f.Id == companyid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Company> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Company> paged = new PagedResult<Company>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Company>()
                                                     .OrderByDescending(GetOrderByExpression<Company>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Company>()
                                                     .OrderBy(GetOrderByExpression<Company>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Company> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Company> specification;

            switch (searchColumn)
            {
                case "CompanyId":
                    specification = new AdHocSpecification<Company>(s => s.CompanyId.Contains(searchValue));
                    break;
                case "CompanyName":
                    specification = new AdHocSpecification<Company>(s => s.CompanyName.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<Company>(s => s.CompanyId.Contains(searchValue));
                    break;
            }

            PagedResult<Company> paged = new PagedResult<Company>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Company>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Company>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Company>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Company>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<Company>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
