﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Credential.Model;
using Treemas.Base.Repository;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Foundation.Repository
{
    public class BusinessDateRepository : RepositoryController<BusinessDate>, IBusinessDateRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public BusinessDateRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public BusinessDate getBusinessDate()
        {
            return FindOne(new AdHocSpecification<BusinessDate>(s => s.Id.Equals(1)));
        }

    }
}
