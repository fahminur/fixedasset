﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Foundation.Repository
{
    public class ModuleRepository : RepositoryControllerString<Module>, IModuleRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public ModuleRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public Module getModule(string id)
        {
            return transact(() => session.QueryOver<Module>()
                                                .Where(f => f.ModuleId == id).SingleOrDefault());
        }
     
        public bool IsDuplicate(string moduleid)
        {
            int rowCount = transact(() => session.QueryOver<Module>()
                                                .Where(f => f.Id == moduleid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Module> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Module> paged = new PagedResult<Module>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Module>()
                                                     .OrderByDescending(GetOrderByExpression<Module>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Module>()
                                                     .OrderBy(GetOrderByExpression<Module>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Module> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Module> specification;

            switch (searchColumn)
            {
                case "ModuleId":
                    specification = new AdHocSpecification<Module>(s => s.ModuleId.Contains(searchValue));
                    break;
                case "ModuleDescription":
                    specification = new AdHocSpecification<Module>(s => s.ModuleDescription.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<Module>(s => s.ModuleId.Contains(searchValue));
                    break;
            }

            PagedResult<Module> paged = new PagedResult<Module>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Module>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Module>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Module>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Module>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<Module>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
