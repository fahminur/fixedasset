﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Repository.Mapping
{
    public class SiteUserMapping : ClassMapping<SiteUser>
    {
        public SiteUserMapping()
        {
            this.Table("Foundation_Site_User");
            Id<long>(x => x.Id,
               map => { map.Column("RecordID");   });

            Property<string>(x => x.SiteId, map => { map.Column("SiteID"); map.Insert(false); });
            ManyToOne(x => x.Site, map =>
            {
                map.Column("SiteID");
                map.Fetch(FetchKind.Join);
                map.Cascade(Cascade.None);
                map.Lazy(LazyRelation.NoLazy);
                map.Update(false);
                map.Insert(false);
            });
            Property<long>(x => x.UserId, map => { map.Column("UserID");});
            Property<string>(x => x.Username, map => { map.Column("UserName"); });
            Property<string>(x => x.FirstName, map => { map.Column("FirstName"); });
            Property<string>(x => x.LastName, map => { map.Column("LastName"); });
            Property<bool>(x => x.IsActive, map => { map.Column("IsActive"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
