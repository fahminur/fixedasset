﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Repository.Mapping
{
    public class TaxMapping : ClassMapping<Tax>
    {
        public TaxMapping()
        {
            this.Table("Foundation_Tax");
            Id<string>(x => x.TaxID,
               map => { map.Column("TaxID");   });
            
            Property<string>    (x => x.TaxDescription, map => { map.Column("TaxDescription"); });
            Property<string>    (x => x.TaxShortDesc, map => { map.Column("TaxShortDesc"); });
            Property<string>    (x => x.Rate, map => { map.Column("Rate"); });
            Property<string>    (x => x.CalcRule, map => { map.Column("CalcRule"); });
            Property<string>    (x => x.GlSegment, map => { map.Column("GlSegment"); });
            Property<string>(x => x.TaxCategory, map => { map.Column("TaxCategory"); });
            Property<bool>      (x => x.Status, map => { map.Column("Status"); });
            Property<string>    (x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
            Property<DateTime?> (x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false);});
            Property<string>    (x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?> (x => x.UpdateDate, map => { map.Column("UpdateDate"); });

        }
    }
}
