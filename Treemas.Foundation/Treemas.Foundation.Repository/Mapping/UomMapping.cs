﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Repository.Mapping
{
    public class UomMapping : ClassMapping<Uom>
    {
        public UomMapping()
        {
            this.Table("Foundation_Uom");
            Id<string>(x => x.UomID,
               map => { map.Column("UomID");   });

            Property<string>(x => x.UomFullDescription, map => { map.Column("UomFullDescription");});
            Property<string>(x => x.UomShorDesc, map => { map.Column("UomShorDesc"); });
            Property<string>(x => x.UomDecimal, map => { map.Column("UomDecimal"); });
            Property<string>(x => x.UomCategoryID, map => { map.Column("UomCategoryID"); });
           
            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
