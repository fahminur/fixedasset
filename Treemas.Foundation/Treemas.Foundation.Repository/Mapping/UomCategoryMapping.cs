﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Repository.Mapping
{
    public class UomCategoryMapping : ClassMapping<UomCategory>
    {
        public UomCategoryMapping()
        {
            this.Table("Foundation_UomCategory");
            Id<string>(x => x.UomCatID,
               map => { map.Column("UomCatID");   });

            Property<string>(x => x.UomCatDescription, map => { map.Column("UomCatDescription");});
            Property<string>(x => x.UomCatShortDesc, map => { map.Column("UomCatShortDesc"); });

            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
