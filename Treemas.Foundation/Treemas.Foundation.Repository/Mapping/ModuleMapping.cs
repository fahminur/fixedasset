﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Repository.Mapping
{
    public class ModuleMapping : ClassMapping<Module>
    {
        public ModuleMapping()
        {
            this.Table("Foundation_Module");
            Id<string>(x => x.Id,
               map => { map.Column("ModuleID");   });

            Property<string>(x => x.ModuleId, map => { map.Column("ModuleID"); map.Update(false); map.Insert(false); });
            Property<bool>(x => x.Status, map => { map.Column("Status");});
            Property<string>(x => x.ModuleDescription, map => { map.Column("ModuleDescription"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
