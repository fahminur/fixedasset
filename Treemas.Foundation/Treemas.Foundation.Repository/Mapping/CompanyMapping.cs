﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Repository.Mapping
{
    public class CompanyMapping : ClassMapping<Company>
    {
        public CompanyMapping()
        {
            this.Table("Foundation_Company");
            Id<string>(x => x.CompanyId,
               map => { map.Column("CompanyID");   });

            //Property<string>(x => x.CompanyId, map => { map.Column("CompanyId"); map.Update(false); map.Insert(false); });
            Property<string>(x => x.CompanyName, map => { map.Column("CompanyName");  });
            Property<string>(x => x.CompanyShortName, map => { map.Column("CompanyShortName");});
            Property<string>(x => x.CompanyInitialName, map => { map.Column("CompanyInitialName"); });
            Property<string>(x => x.CompanyAddress, map => { map.Column("CompanyAddress"); });
            Property<string>(x => x.CompanyRT, map => { map.Column("CompanyRT"); });
            Property<string>(x => x.CompanyRW, map => { map.Column("CompanyRW"); });
            Property<string>(x => x.CompanyKelurahan, map => { map.Column("CompanyKelurahan"); });
            Property<string>(x => x.CompanyKecamatan, map => { map.Column("CompanyKecamatan"); });
            Property<string>(x => x.CompanyCity, map => { map.Column("CompanyCity"); });
            Property<string>(x => x.CompanyZipCode, map => { map.Column("CompanyZipCode"); });
            Property<string>(x => x.CompanyPhone1, map => { map.Column("CompanyPhone1"); });
            Property<string>(x => x.CompanyAreaPhone1, map => { map.Column("CompanyAreaPhone1"); });
            Property<string>(x => x.CompanyPhone2, map => { map.Column("CompanyPhone2"); });
            Property<string>(x => x.CompanyAreaPhone2, map => { map.Column("CompanyAreaPhone2"); });
            Property<string>(x => x.CompanyAreaFax, map => { map.Column("CompanyAreaFax"); });
            Property<string>(x => x.CompanyFax, map => { map.Column("CompanyFax"); });
            Property<string>(x => x.NPWP, map => { map.Column("NPWP"); });
            Property<string>(x => x.TDP, map => { map.Column("TDP"); });
            Property<string>(x => x.SIUP, map => { map.Column("SIUP"); });
            Property<string>(x => x.ContactPersonName, map => { map.Column("ContactPersonName"); });
            Property<string>(x => x.ContactPersonJobTitle, map => { map.Column("ContactPersonJobTitle"); });
            Property<string>(x => x.ContactPersonEmail, map => { map.Column("ContactPersonEmail"); });
            Property<string>(x => x.ContactPersonHP, map => { map.Column("ContactPersonHP"); });

            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
