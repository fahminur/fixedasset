﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Foundation.Model;
using Treemas.Credential.Model;

namespace Treemas.Foundation.Repository.Mapping
{
    public class BusinessDateMapping : ClassMapping<BusinessDate>
    {
        public BusinessDateMapping()
        {
            this.Table("Foundation_SystemControl");
            Id<long>(x => x.Id,
               map => { map.Column("ID"); });

            Property<DateTime>(x => x.BeginningOfDay,
                map => { map.Column("BODDate"); });
            Property<DateTime>(x => x.CurrentDate,
                map => { map.Column("BDCurrent"); });
            Property<DateTime>(x => x.EndOfDay,
                map => { map.Column("EODDate"); });
            Property<DateTime>(x => x.BeginningOfDayPrevious,
                map => { map.Column("BDPrevious"); });
            Property<DateTime>(x => x.BeginningOfDayNext,
                map => { map.Column("BDNext"); });
            Property<DateTime>(x => x.EndOfMonthPrevious,
                map => { map.Column("EOMPrevious"); });
            Property<DateTime>(x => x.EndOfMonthNext,
                map => { map.Column("EOMNext"); });
            Property<bool>(x => x.IsBranchClosed,
                map => { map.Column("IsSiteClosed"); });
            Property<bool>(x => x.IsCashierClosed,
                map => { map.Column("IsCashierClosed"); });
            Property<string>(x => x.ChangedBy,
               map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.ChangedDate,
               map => { map.Column("UpdateDate"); });
        }
    }
}
