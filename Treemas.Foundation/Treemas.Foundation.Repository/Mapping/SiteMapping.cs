﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Repository.Mapping
{
    public class SiteMapping : ClassMapping<Site>
    {
        public SiteMapping()
        {
            this.Table("Foundation_Site");
            Id<string>(x => x.SiteId,
               map => { map.Column("SiteID");   });

         //  Property<string>(x => x.SiteId, map => { map.Column("SiteID"); map.Update(false); map.Insert(false); });
            Property<string>(x => x.SiteName, map => { map.Column("SiteName");});
            Property<string>(x => x.SiteAddress, map => { map.Column("SiteAddress"); });
            Property<string>(x => x.SiteKelurahan, map => { map.Column("SiteKelurahan"); });
            Property<string>(x => x.SiteKecamatan, map => { map.Column("SiteKecamatan"); });
            Property<string>(x => x.SiteCity, map => { map.Column("SiteCity"); });
            Property<string>(x => x.SiteZipCode, map => { map.Column("SiteZipCode"); });
            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
