﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Foundation.Model;

namespace Treemas.Foundation.Repository.Mapping
{
    public class CurrencyMapping : ClassMapping<Currency>
    {
        public CurrencyMapping()
        {
            this.Table("Foundation_Currency");
            Id<string>(x => x.CurrencyId,
               map => { map.Column("CurrencyID");   });

            Property<string>(x => x.CurrencyDescription, map => { map.Column("CurrencyDescription");});
            Property<string>(x => x.CurrencyShortDesc, map => { map.Column("CurrencyShortDesc"); });

            Property<bool>(x => x.Status, map => { map.Column("Status"); });
            Property<DateTime?>(x => x.UpdateDate, map => { map.Column("UpdateDate"); });
            Property<string>(x => x.UpdateUser, map => { map.Column("UpdateUser"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("CreateDate"); map.Update(false); });
            Property<string>(x => x.CreateUser, map => { map.Column("CreateUser"); map.Update(false); });
        }
    }
}
