﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Foundation.Repository
{
    public class SiteUserRepository : RepositoryController<SiteUser>, ISiteUserRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SiteUserRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public SiteUser getSiteUser(long id)
        {
            return FindOne(new AdHocSpecification<SiteUser>(s => s.Id.Equals(id)));
        }
        public SiteUser getSiteUserBySite(string userName, string siteId)
        {
            return FindOne(new AdHocSpecification<SiteUser>(s => s.SiteId.Equals(siteId) && s.Username == userName));
        }
        public IList<SiteUser> getSitesbyUser(string userName)
        {
            try
            {
                return transact(() => session.QueryOver<SiteUser>()
                                                .Where(f => f.Username == userName).List());
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool IsDuplicate(long UserId, string SiteId)
        {
            int rowCount = transact(() => session.QueryOver<SiteUser>()
                                                .Where(f => f.SiteId == SiteId &&
                                                            f.UserId == UserId).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<SiteUser> FindAllPaged(string SiteId, int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<SiteUser> paged = new PagedResult<SiteUser>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<SiteUser>()
                                                    .Where(x => x.SiteId == SiteId)
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<SiteUser>()
                                                     .Where(x => x.SiteId == SiteId)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<SiteUser>()
                                .Where(x => x.SiteId == SiteId)
                                .RowCount());
            return paged;
        }

        public PagedResult<SiteUser> FindAllPaged(string SiteId, int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<SiteUser> paged = new PagedResult<SiteUser>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<SiteUser>()
                                 .Where(x => x.SiteId == SiteId)
                                 .And(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<SiteUser>()
                                 .Where(x => x.SiteId == SiteId)
                                 .And(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<SiteUser>()
                                .Where(x => x.SiteId == SiteId)
                                .And(specification)
                                .RowCount());
            return paged;
        }


    }
}
