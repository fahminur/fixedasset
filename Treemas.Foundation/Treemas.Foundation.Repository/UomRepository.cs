﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Foundation.Repository
{
    public class UomRepository : RepositoryControllerString<Uom>, IUomRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public UomRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public Uom getUom(string id)
        {
            return transact(() => session.QueryOver<Uom>()
                                                .Where(f => f.UomID == id).SingleOrDefault());
        }
     
        public bool IsDuplicate(string currencyid)
        {
            int rowCount = transact(() => session.QueryOver<Uom>()
                                                .Where(f => f.UomID == currencyid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Uom> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Uom> paged = new PagedResult<Uom>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Uom>()
                                                     .OrderByDescending(GetOrderByExpression<Uom>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Uom>()
                                                     .OrderBy(GetOrderByExpression<Uom>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Uom> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Uom> specification;

            switch (searchColumn)
            {
                case "UomID":
                    specification = new AdHocSpecification<Uom>(s => s.UomID.Contains(searchValue));
                    break;
                case "UomFullDescription":
                    specification = new AdHocSpecification<Uom>(s => s.UomFullDescription.Contains(searchValue));
                    break;
                case "UomShorDesc":
                    specification = new AdHocSpecification<Uom>(s => s.UomShorDesc.Contains(searchValue));
                    break;              
                case "UomCategoryID":
                    specification = new AdHocSpecification<Uom>(s => s.UomCategoryID.Contains(searchValue));
                    break;               
                default:             
                    specification = new AdHocSpecification<Uom>(s => s.UomID.Contains(searchValue));
                    break;
            }

            PagedResult<Uom> paged = new PagedResult<Uom>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Uom>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Uom>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Uom>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Uom>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<Uom>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
