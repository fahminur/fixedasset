﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using NHibernate.Criterion;

namespace Treemas.Foundation.Repository
{
    public class KelurahanRepository : RepositoryControllerString<Foundation_Kelurahan>, IKelurahanRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public KelurahanRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public Foundation_Kelurahan getFoundation_kelurahan(string id)
        {
            return transact(() => session.QueryOver<Foundation_Kelurahan>()
                                                .Where(f => f.KelurahanID == id).SingleOrDefault());
        }

        public bool IsDuplicate(string id)
        {
            int rowCount = transact(() => session.QueryOver<Foundation_Kelurahan>()
                                                .Where(f => f.KelurahanID == id).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Foundation_Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, KelurahanFilter filter)
        {
            SimpleExpression NamaKelurahan = Restrictions.Like("NamaKelurahan", filter.NamaKelurahan.Trim(), MatchMode.Anywhere);
            SimpleExpression Provinsi = Restrictions.Like("Provinsi", filter.Provinsi.Trim(), MatchMode.Anywhere);
            SimpleExpression Kecamatan = Restrictions.Like("Kecamatan", filter.Kecamatan.Trim(), MatchMode.Anywhere);
            SimpleExpression Kota = Restrictions.Like("Kota", filter.Kota.Trim(), MatchMode.Anywhere);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.NamaKelurahan.Trim().IsNullOrEmpty()) conjuction.Add(NamaKelurahan);
            if (!filter.Provinsi.Trim().IsNullOrEmpty()) conjuction.Add(Provinsi);
            if (!filter.Kecamatan.Trim().IsNullOrEmpty()) conjuction.Add(Kecamatan);
            if (!filter.Kota.Trim().IsNullOrEmpty()) conjuction.Add(Kota);

            PagedResult<Foundation_Kelurahan> paged = new PagedResult<Foundation_Kelurahan>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<Foundation_Kelurahan>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<Foundation_Kelurahan>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<Foundation_Kelurahan>()
                               .Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<Foundation_Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Foundation_Kelurahan> paged = new PagedResult<Foundation_Kelurahan>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Foundation_Kelurahan>()
                                                     .OrderByDescending(GetOrderByExpression<Foundation_Kelurahan>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Foundation_Kelurahan>()
                                                     .OrderBy(GetOrderByExpression<Foundation_Kelurahan>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Foundation_Kelurahan> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Foundation_Kelurahan> specification;

            switch (searchColumn)
            {
                case "KelurahanID":
                    specification = new AdHocSpecification<Foundation_Kelurahan>(s => s.KelurahanID.Contains(searchValue));
                    break;
                case "Kelurahan":
                    specification = new AdHocSpecification<Foundation_Kelurahan>(s => s.Kelurahan.Contains(searchValue));
                    break;
                case "Kecamatan":
                    specification = new AdHocSpecification<Foundation_Kelurahan>(s => s.Kecamatan.Contains(searchValue));
                    break;
                case "Kota":
                    specification = new AdHocSpecification<Foundation_Kelurahan>(s => s.Kota.Contains(searchValue));
                    break;
                case "ZipCode":
                    specification = new AdHocSpecification<Foundation_Kelurahan>(s => s.ZipCode.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<Foundation_Kelurahan>(s => s.KelurahanID.Contains(searchValue));
                    break;
            }

            PagedResult<Foundation_Kelurahan> paged = new PagedResult<Foundation_Kelurahan>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Foundation_Kelurahan>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Foundation_Kelurahan>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Foundation_Kelurahan>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Foundation_Kelurahan>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<Foundation_Kelurahan>()
                                .Where(specification.ToExpression()).Count());
            return paged;
        }

        public IEnumerable<Foundation_Kelurahan> getKecamatan(string Kota)
        {
            Specification<Foundation_Kelurahan> specification;
            specification = new AdHocSpecification<Foundation_Kelurahan>(s => s.Kota == Kota);

            return FindAll(specification).Distinct();
        }

        public IEnumerable<Foundation_Kelurahan> getKelurahan(string Kota, string Kecamatan)
        {
            Specification<Foundation_Kelurahan> specification;
            specification = new AdHocSpecification<Foundation_Kelurahan>(s => s.Kota == Kota && s.Kecamatan == Kecamatan);

            return FindAll(specification).Distinct();
        }

        public IEnumerable<Foundation_Kelurahan> getKota()
        {
            IEnumerable<Foundation_Kelurahan> kota = FindAll();
            return kota.Distinct();
        }

        public Foundation_Kelurahan GetZipCode(string NamaKelurahan)
        {
            return transact(() => session.QueryOver<Foundation_Kelurahan>()
                                                .Where(f => f.Kelurahan == NamaKelurahan).SingleOrDefault());
        }

        public Foundation_Kelurahan getKelurahanSingle(string ID)
        {
            return transact(() => session.QueryOver<Foundation_Kelurahan>()
                                                .Where(f => f.KelurahanID == ID).SingleOrDefault());
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
