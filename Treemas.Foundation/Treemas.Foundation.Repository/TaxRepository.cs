﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Foundation.Repository
{
    public class TaxRepository : RepositoryControllerString<Tax>, ITaxRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public TaxRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public Tax getTax(string id)
        {
            return transact(() => session.QueryOver<Tax>()
                                                .Where(x => x.TaxID == id).SingleOrDefault());
        }
        public IList<Tax> getTaxByCategory(string category)
        {
            IList<Tax> list = transact(() => session.QueryOver<Tax>().Where(f => f.TaxCategory == category).List());

            return list;
        }

        public bool IsDuplicate(string taxid)
        {
            int rowCount = transact(() => session.QueryOver<Tax>()
                                                .Where(f => f.TaxID == taxid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Tax> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Tax> paged = new PagedResult<Tax>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Tax>()
                                                     .OrderByDescending(GetOrderByExpression<Tax>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Tax>()
                                                     .OrderBy(GetOrderByExpression<Tax>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Tax> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Tax> specification;

            switch (searchColumn)
            {
                case "TaxID":
                    specification = new AdHocSpecification<Tax>(s => s.TaxID.Contains(searchValue));
                    break;
                case "TaxDescription":
                    specification = new AdHocSpecification<Tax>(s => s.TaxDescription.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<Tax>(s => s.TaxID.Contains(searchValue));
                    break;
            }

            PagedResult<Tax> paged = new PagedResult<Tax>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Tax>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Tax>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Tax>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Tax>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<Tax>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
