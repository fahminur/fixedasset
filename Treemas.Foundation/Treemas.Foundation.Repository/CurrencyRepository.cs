﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Foundation.Model;
using Treemas.Foundation.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Foundation.Repository
{
    public class CurrencyRepository : RepositoryControllerString<Currency>, ICurrencyRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public CurrencyRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public Currency getCurrency(string id)
        {
            return transact(() => session.QueryOver<Currency>()
                                                .Where(f => f.CurrencyId == id).SingleOrDefault());
        }
     
        public bool IsDuplicate(string currencyid)
        {
            int rowCount = transact(() => session.QueryOver<Currency>()
                                                .Where(f => f.CurrencyId == currencyid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Currency> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Currency> paged = new PagedResult<Currency>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Currency>()
                                                     .OrderByDescending(GetOrderByExpression<Currency>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Currency>()
                                                     .OrderBy(GetOrderByExpression<Currency>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Currency> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Currency> specification;

            switch (searchColumn)
            {
                case "CurrencyId":
                    specification = new AdHocSpecification<Currency>(s => s.CurrencyId.Contains(searchValue));
                    break;
                case "CurrencyDescription":
                    specification = new AdHocSpecification<Currency>(s => s.CurrencyDescription.Contains(searchValue));
                    break;
                case "CurrencyShortDesc":
                    specification = new AdHocSpecification<Currency>(s => s.CurrencyShortDesc.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<Currency>(s => s.CurrencyId.Contains(searchValue));
                    break;
            }

            PagedResult<Currency> paged = new PagedResult<Currency>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Currency>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Currency>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Currency>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Currency>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<Currency>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
