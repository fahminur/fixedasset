﻿using System.Web;
using System.IO;
using Treemas.Base.Configuration;
using Treemas.Base.Configuration.Binder;

namespace Treemas.Foundation.Repository
{
    public class SystemConfigurationCabinet : ConfigurationCabinet
    {
        private static SystemConfigurationCabinet instance = new SystemConfigurationCabinet();

        private SystemConfigurationCabinet() : base("Application Configuration")
        {
            string path = HttpContext.Current.Server.MapPath("~/Config");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            IConfigurationBinder binder = new XmlFileConfigurationBinder("Database", path);
            this.AddBinder(binder);
            binder.Load();
        }

        public static SystemConfigurationCabinet Instance =>
            instance;
    }
}
